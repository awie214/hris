$(document).ready(function () {
   remIconDL();
   
   $("[name='btnTab_1']").click(function() {
      tabClick(1);
      $("#PolicyType").html("Leave");
      $("#hTable").val("leavepolicygroup");
      $("#hEntryScreen").val("EntryScrn");
      $("#hModalName").val("modalFieldEntry");
   });
   $("[name='btnTab_2']").click(function() {
      tabClick(2);
      $("#hTable").val("leavepolicy");
      $("#hEntryScreen").val("EntryScrn_LeavePolicy");
      $("#hModalName").val("modalFieldEntry_LeavePolicy");
   });
   $("[name='btnTab_3']").click(function() {
      tabClick(3);
      $("#PolicyType").html("Overtime");
      $("#hTable").val("overtimepolicygroup");
      $("#hEntryScreen").val("EntryScrn");
      $("#hModalName").val("modalFieldEntry");
   });
   $("[name='btnTab_4']").click(function() {
      tabClick(4);
      $("#hTable").val("overtimepolicy");
      $("#hEntryScreen").val("EntryScrn_OvertimePolicy");
      $("#hModalName").val("modalFieldEntry_OvertimePolicy");
   });

   $("#btnINSERTLPG, #btnINSERTOTG").click(function () {
      $(".saveFields--").val("");
      $(".saveFields--").prop("disabled",false);
      $(".saveFields--").prop("checked",false);
      $("#EntryScrn .saveFields--").not("input[type='hidden'], input[name='date_FiledDate'], .tmPicker--").val("");
      $("#EntryScrn .saveFields--").not("input[type='hidden'], input[name='date_FiledDate'], .tmPicker--").prop("disabled",false);
      $("#btnLocCancel, #btnLocSave").show();
      $("#modalFieldEntry").modal();

   });
   $("#btnINSERTLP").click(function () {
      $("#btnLocCancel, #btnLocSave").show();
      $(".saveFields--").val("");
      $(".saveFields--").prop("disabled",false);
      $(".saveFields--").prop("checked",false);
      $("#EntryScrn .saveFields--").not("input[type='hidden'], input[name='date_FiledDate'], .tmPicker--").val("");
      $("#EntryScrn .saveFields--").not("input[type='hidden'], input[name='date_FiledDate'], .tmPicker--").prop("disabled",false);
      $("#modalFieldEntry_LeavePolicy").modal();
   });
   $("#btnINSERTOT").click(function () {
      $("#btnLocCancel, #btnLocSave").show();
      $(".saveFields--").prop("disabled",false);
      $(".saveFields--").prop("checked",false);
      $("#EntryScrn .saveFields--").not("input[type='hidden'], input[name='date_FiledDate'], .tmPicker--").val("");
      $("#EntryScrn .saveFields--").not("input[type='hidden'], input[name='date_FiledDate'], .tmPicker--").prop("disabled",false);
      $(".saveFields--").val("");
      $("#modalFieldEntry_OvertimePolicy").modal();
      /*var name = "";
      $("#EntryScrn_OvertimePolicy .saveFields--").each(function () {
         name += $(this).attr("name") + ",";
      });
      alert(name);*/
   });
   $("[id*='btnLocCancel']").click(function () {
      $(".modalFieldEntry--").modal("hide");
   });


   /*$("#Late").change(function(){
      if ("#Late :checked") {
         alert(32);
         $("[name='sint_ForfeitedIfLate']").val(1);
      }else {
         alert(22);
         $("[name='sint_ForfeitedIfLate']").val(0);
      }
   });*/
   /*$("#Late").change(function () {
      if ($("#Late").attr('checked')){
         alert(32);
         $("[name='sint_ForfeitedIfLate']").val("1");
      }
   })*/

   $("[id*='btnLocSave']").each(function () {
      $(this).click(function () {
         /*
         if ($("[name='sint_LeavePolicyGroupRefId']").val() == "") {
            return false;
         }
         if ($("#hMode").val() == "EDIT" && parseInt($("#hRefId").val()) < 0){
            alert("Err. hRefId is Required!!!");
         }
         if ($("#hTable").val() == "") {
            alert("No Assigned Table");
         } else {
            $.ajax({
               url: "amsTrn.php",
               type: "POST",
               data: new FormData($('form#xForm')[0]),
               success : function(responseTxt){
                  if (responseTxt.indexOf("Error") < 0) {
                     if ($("#hTable").val() == "leavepolicygroup") {
                       var gridID = "spGridTable_lpgroup";
                     } else if ($("#hTable").val() == "overtimepolicygroup") {
                       var gridID = "spGridTable_opgroup";
                     } else if ($("#hTable").val() == "leavepolicy") {
                       var gridID = "spGridTable_lp";
                     } else if ($("#hTable").val() == "overtimepolicy") {
                       var gridID = "spGridTable_op";
                     }
                     alert("New Record Inserted");
                     $("#" + gridID).html(responseTxt);
                     $(".modalFieldEntry--").modal("hide");
                  } else {
                     console.log(responseTxt);
                  }
               },
               error : function (xhr,status,error) {
                  console.log(error);
               },
               processData: false,
               contentType: false,
               cache: false
            });
         }
         */
         if ($("#hTable").val() == "") {
            alert("No Assigned Table");
         } else {
            $.ajax({
               url: "PolicyTrn.e2e.php",
               type: "POST",
               data: new FormData($('form#xForm')[0]),
               success : function(responseTxt){
                  if (responseTxt.indexOf("Error") < 0) {
                     //alert(responseTxt);
                     eval(responseTxt);
                  } else {
                     console.log(responseTxt);
                  }
               },
               error : function (xhr,status,error) {
                  console.log(error);
               },
               processData: false,
               contentType: false,
               cache: false
            });
         }
      });
   });
   $("#Accumulating").click(function () {
      $("[name='sint_Accumulating']").val(1);
   });
   $("#ResetYearly").click(function () {
      $("[name='sint_Accumulating']").val(0);
   });
   $("[name='drpLeavePolicyGroupRefId']").change(function () {
      var data;
      data = {
         "table":"leavepolicy",
         "gridHDR":"Name|Leave|Accumulating|Value",
         "gridDTL":"LeavePolicyGroupRefId|LeavesRefId|Accumulating|Value",
         "gridTable":"gridTable_leavepolicy",
         "whereClause":"LeavePolicyGroupRefId = " + $(this).val(),
         "HolderId":"spGridTable_lp",
         "fn":"UpdateGridTable"
      }
      UpdateGrid(data);
   });
   $("#char_MaxForceLeave").prop("readonly",true);
   $("[name='sint_LeavesRefId']").change(function () {
      getLeaveCode($("[name='sint_LeavesRefId']").val());
   });
   /*
   $("#sint_LeavePolicyGroupRefId").change(function () {
      getPolicyName($(this).val(),"char_LPName","leavepolicygroup");
   });
   */
   
   $("#sint_OvertimePolicyGroupRefId").change(function () {
      /*var lp = $("#sint_OvertimePolicyGroupRefId option:selected").text().split("-");
      $("[name='char_OTName']").val(lp[1]);*/
      //getPolicyName($(this).val(),"char_OTName","overtimepolicygroup");
      $("[name='sint_MaxOTWorkingDays']").val(3);
      $("[name='sint_MaxOTSaturday']").val(8);
      $("[name='sint_MaxOTHolidays']").val(8);
      $("[name='sint_MaxOTMonth']").val(40);
      $("[name='sint_MaxUnusedCOCYear']").val(120);
      $("[name='sint_ValidYears']").val(1);
      $("[name='sint_COCRatesWorkingDays']").val("1.25");
      $("[name='sint_COCRatesSaturday']").val("1.50");
      $("[name='sint_COCRatesHolidays']").val("1.50");
   });
   
   /*$("#sint_LeavesRefId").change(function () {
      if ($(this).val() == 3 || $(this).val() == 2){
         $("[name='deci_Value']").val(1.25);
      }else {
         $("[name='deci_Value']").val("");
      } 
      
   });*/
});

function UpdateGrid(data) {
   /*$.ajax({
      url: "trn.e2e.php",
      type: "GET",
      data: {
         table:data.table,
         gridHDR:data.gridHDR,
         gridDTL:data.gridDTL,
         gridTable:data.gridTable,
         whereClause:data.whereClause,
         fn:data.fn
      },
      success : function(responseTxt){
         if (responseTxt.indexOf("Error") < 0) {
            $("#" + data.HolderId).html(responseTxt);
         } else {
            console.log(responseTxt);
         }
      },
      error : function (xhr,status,error) {
         console.log(error);
      },
      processData: false,
      contentType: false,
      cache: false
   });*/

   $.get("fnCaller.e2e.php",
   {
      table:data.table,
      gridHDR:data.gridHDR,
      gridDTL:data.gridDTL,
      gridTable:data.gridTable,
      whereClause:data.whereClause,
      fn:data.fn
   },
   function(responseTxt,status) {
      if (status == "success") {
         try {
            if (responseTxt.indexOf("Error") < 0) {
               $("#" + data.HolderId).html(responseTxt);
            } else {
               console.log(responseTxt);
            }
         } catch (e) {
             if (e instanceof SyntaxError) {
                 alert(e.message);
             }
         }
      }
   });

}
/*
function tabClick(tabidx) {
   var prevIDX = $("[name='hTabIdx']").val();
   $("[name='hTabIdx']").val(tabidx);
   $("#tab_" + prevIDX).hide();
   $("#tab_" + tabidx).show();
   if (tabidx == 1) {
      $("#PolicyType").html("Leave");
      $("#hTable").val("leavepolicygroup");
      $("#hEntryScreen").val("EntryScrn");
      $("#hModalName").val("modalFieldEntry");
   } 
}
*/

function selectMe() {

}
function afterDelete(refid){
   alert("Record Deleted!!!");
   gotoscrn("amsPolicy","");
}

function getLeaveCode(leaverefid){
   $.get("trn.e2e.php",
   {
      fn:"getLeaveCode",
      refid:leaverefid,
   },
   function(data,status) {
      if (status == "success") {
         eval(data);
      } else {
         alert(status);
      }
   });
}
function getPolicyName(refid,objname,table){
   $.get("trn.e2e.php",
   {
      fn:"getPolicyName",
      refid:refid,
      objname:objname,
      tbl:table
   },
   function(data,status){
      if (status == 'success') {
         eval(data);
      } else {
         console.log(status + " - " + data);
      }
   });
}