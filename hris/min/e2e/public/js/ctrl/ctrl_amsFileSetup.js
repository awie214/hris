$(document).ready(function() {
   $("#btn8Hr").click(function (){
      $("#tbl4Modal").modal();
   });
   $("[name='chk_isLegal']").click(function () {
      if ($(this).is(':checked')){
         $("[name='sint_isLegal']").val(1);
      } else {
         $("[name='sint_isLegal']").val(0);
      }
   });
   $("[name='chk_isApplyEveryYr']").click(function () {
      if ($(this).is(':checked')){
         $("[name='sint_isApplyEveryYr']").val(1);
      } else {
         $("[name='sint_isApplyEveryYr']").val(0);
      }
   });
});

function afterNewSave(LastRefId) {
   alert("New Record " + LastRefId + " Succesfully Inserted");
   gotoscrn ("amsFileSetup", setAddURL());
   return false;
}
function afterDelete() {
   alert("Record Succesfully Deleted");
   gotoscrn ("amsFileSetup", setAddURL());
   return false;
}
function afterEditSave(RefId){
   alert("Record " + RefId + " Succesfully Updated");
   gotoscrn ("amsFileSetup", setAddURL());
   return false;
}
