var tm = 500;
var isSavePDS = false;
var tabIDX = 1;
$(document).ready(function() {
   /*$("#dummy").click(function () {
      alert("Modal Na");
      $("#createNewAcct").modal();
   });*/
   $("#btn_complete").click(function () {
      if (confirm("Are you complete with editing your PDS?")) {
         completion_sent();
      }
   });
   $("#btnUPDATE, #btnSAVE").hide();
   $("#chkPersonalInfo").click(function () {
      if ($(this).is(":checked")) {
         $("#btnSAVE").show();
      } else {
         $("#btnSAVE").hide();
      }
   });

   $("[name*='sint_SalaryGradeRefId_']").each(function () {
      $(this).focus(function () {
         validateGrade2($(this).attr("partner"));
      });
      $(this).change(function () {
         validateGrade2($(this).attr("partner"));
      });
   });
   $("[name*='sint_JobGradeRefId_']").each(function () {
      $(this).focus(function () {
         validateGrade2($(this).attr("partner"));
      });
      $(this).change(function () {
         validateGrade2($(this).attr("partner"));
      });
   });

   $("[name='char_CivilStatus']").change(function () {
      if ($(this).val() == "Si") {
         $("#EntrySpouseInfo input[class*='saveFields--']").val("N/A");
         $("#EntrySpouseInfo select[class*='saveFields--']").val("0");
         $("#EntrySpouseInfo").hide();
      } else {
         $("#EntrySpouseInfo input[class*='saveFields--']").val("");
         $("#EntrySpouseInfo").show();
      }
      if ($(this).val() == "Ot") {
         $("#other_cs").show();
      } else {
         $("#other_cs").hide();
         $("#char_Others").val();
      }
   });
   $("#btnATTACH").click(function() {
      gotoscrn('DocAttachUpload','&paramTitle=Attach Document');
   });
   $("#sexMale").click(function() {
      $("[name='char_Sex']").val("M");
   });
   $("#sexFemale").click(function() {
      $("[name='char_Sex']").val("F");
   });
   $("#isFilipino").click(function() {
      $("[name='sint_isFilipino']").val(1);
      $("#CountryCitizen").slideUp(300);
   });
   $("#isDual").click(function() {
      $("[name='sint_isFilipino']").val(0);
      $("#CountryCitizen").slideDown(300);
   });
   $("#isByBirth").click(function() {
      $("[name='sint_isByBirthOrNatural']").val(1);
   });
   $("#isNaturalization").click(function() {
      $("[name='sint_isByBirthOrNatural']").val(2);
   });
   $("[name*='btnTab']").each(function() {
      $(this).click(function() {
         tabClick($(this).attr("idx"));
      });
   });
   /*$("#chkPDSQ").click(function () {
      $("[name*='Ans_']").prop("disabled",false);
      showBtnUpd();
   });*/
   $(".enabler--").each(function () {
      $(this).click(function () {
         $idx = $(this).attr("idx");
         if (getValueByName("hUserGroup") == "COMPEMP") {
            $("#"+$(this).attr("for") + " .form-input").not("[group='1']").prop("disabled",!($(this).is(":checked")))
            showBtnUpd();
            var objName = $(this).attr("name");
            var idx = 0;
            var lvl = 0;
            if (objName == "chkPDSQ") setObjBhvr($("#hEmpRefId").val());
            if (
                  objName.indexOf("chkVocCourse_3_") >= 0 ||
                  objName.indexOf("chkCollege_4_") >= 0 ||
                  objName.indexOf("chkGradStudies_5_") >= 0
               ) {
               idx = objName.split("_")[2];
               lvl = objName.split("_")[1];
               if ($("[name='sint_YearGraduated_"+lvl+"_"+idx+"']").val() > 0) {
                  $("[name='sint_Present_"+lvl+"_"+idx+"']").prop("disabled",true);
                  $("[name='char_HighestGrade_"+lvl+"_"+idx+"']").val("");
               } else {
                  $("[name='sint_Present_"+lvl+"_"+idx+"']").prop("disabled",false);
               }
               if ($("[name='char_HighestGrade_"+lvl+"_"+idx+"']").val() != "") {
                  $("[name='sint_YearGraduated_"+lvl+"_"+idx+"']").prop("disabled",true);
                  $("[name='sint_YearGraduated_"+lvl+"_"+idx+"']").val(0);
               }
               else
                  $("[name='sint_YearGraduated_"+lvl+"_"+idx+"']").prop("disabled",false);
            }
         } else {
            if ($("#chkPersonalInfo").is(":checked")) {
               $("#"+$(this).attr("for") + " .form-input")
               .prop("disabled",!($(this).is(":checked")))
               showBtnUpd();
            } else {
               //$.notify("You must fill up first the Personal Information Tab","warn",{position:"top center"});
               $("[type='checkbox']").prop("checked",false);
            }
         }
            /*if ($(this).hasClass("workexp_fpreview")) {
                  idx = $(this).attr("idx");
                  if ($("[name='chkWorkExp_" + idx + "']").is(":checked")) {
                        
                  }
            }*/

         
      });
   });

   $("[name*='chkWorkExp_']").click(function () {
      ctrlObjs(
            $(this).attr("id"),
            $(this).is(":checked"),
            "EntryWorkExp_"
      );
      $(this).val($(this).is(":checked"));
      chkBoxID = $(this).attr("id");
      var idx = chkBoxID.split("_")[1];
      var resid = $(this).attr("refid");
      if (!$(this).is(":checked")) {
            var fldName = $(this).attr("fldName");
            rtrnVal("employeesworkexperience", resid, idx, fldName, 1);
      } else {
         if ($("#sint_isGovtService_" + idx).val() == 0) {
               $("[name='sint_StepIncrementRefId_" + idx + "'], [name='sint_JobGradeRefId_" + idx + "'], [name='sint_SalaryGradeRefId_" + idx + "']").prop("disabled", true);
         }
      }
   });

   $("[name*='sint_YearGraduated']").each(function () {
      $(this).change(function () {
         idx = $(this).attr("name").split("_")[3];
         lvl = $(this).attr("name").split("_")[2];
         if ($(this).val() == 0) {
            $("[name='char_HighestGrade_" + lvl + "_" + idx + "']").prop("disabled", false);
         } else {
            $("[name='char_HighestGrade_" + lvl + "_" + idx + "']").prop("disabled", true);
            $("[name='char_HighestGrade_" + lvl + "_" + idx + "']").val("");
         }
      });
   });

   $("#chkPersonalInfo").click(function () {
      if (!$(this).is(":checked")){
         var fld = "";
         $("#PInfo .saveFields--").each(function () {
            var name = $(this).attr("name");
            fld += name + ",";
         });
         $.get("trn.e2e.php",
         {
            fn:"rtrnEmpVal",
            refid:$(this).attr("refid"),
            fldName:fld,
            table:"employees"
         },
         function(data,status) {
            if (status == "success") {
               try {
                  eval(data);
               } catch (e) {
                  if (e instanceof SyntaxError) {
                     alert(e.message);
                  }
               }
            }
         });
      }
   });
   $("#FamilyBG").click(function () {
      if (!$(this).is(":checked")){
         var fld = "";
         $("#famBG .saveFields--").each(function () {
            var name = $(this).attr("name");
            fld += name + ",";
         });
         $.get("trn.e2e.php",
         {
            fn:"rtrnEmpVal",
            refid:$(this).attr("refid"),
            fldName:fld,
            table:"employeesfamily"
         },
         function(data,status) {
            if (status == "success") {
               try {
                  eval(data);
               } catch (e) {
                  if (e instanceof SyntaxError) {
                     alert(e.message);
                  }
               }
            }
         });
      }
   });

   $("#chkPDSQ").click(function () {
      if (!$(this).is(":checked")){
         var fld = "";
         $("#EntryPDSQ .saveFields--").each(function () {
            var name = $(this).attr("name");
            fld += name + ",";
         });
         $.get("trn.e2e.php",
         {
            fn:"rtrnEmpVal",
            refid:$("[name='pdsqRefId']").val(),
            fldName:fld,
            table:"employeespdsq"
         },
         function(data,status) {
            if (status == "success") {
               try {
                  eval(data);
               } catch (e) {
                  if (e instanceof SyntaxError) {
                     alert(e.message);
                  }
               }
            }
         });
         $("[name*='date_'], [name*='char_']").prop("disabled",true);
      }
   });
   $("[name='drpDBTable']").change(function(){
      $("#hTable").val($(this).val());
   });
   $("[name='drpListOf']").change(function(){
      $("#setList").html(($(this).val() + " LIST").toUpperCase());
      $.post("changeSessionValue.e2e.php",
      {
         hGridTblHdr:"Last Name|First Name|Contact No|Birth Date|Civil Status",
         hGridTblFld:"LastName|FirstName|ContactNo|BirthDate|CivilStatus",
         hGridTblId:"gridTable",
         hGridDBTable:$(this).val(),
         sql:"SELECT * FROM "+$(this).val()+" ORDER BY RefId Desc LIMIT 500",
         listAction:0
      },
      function(data,status){
         if (status=='success') {
            refreshList("");
         }
      });
   });
   $("#btnCREATE").click(function() {
      gotoscrn("scrnNewEmployees","");
   });
   $("#btnCANCEL").click(function() {
      $("#201List").show();
      $("#DataEntry").hide();
      $("[name='hmode']").val("VIEW");
      $("#btnSAVE").hide();
      $("#chkPersonalInfo").prop("checked",false);
   });
   $("#btnSAVE").click(function() {
      saveEmployees201();
   });
   
   $("[name='chkSameAsAbove']").click(function(){
      fAddressCopy($(this).is(":checked"));
   });

   $("[name*='chkEligib_']").click(function(){
      ctrlObjs($(this).attr("id"),
               $(this).is(":checked"),
               "EntryEligib_");
      $(this).val($(this).is(":checked"));
      chkBoxID = $(this).attr("id");
      var idx = chkBoxID.split("_")[1];
      var resid = $(this).attr("refid");
      if (!$(this).is(":checked")){
         var fldName = $(this).attr("fldName");
         rtrnVal("employeeselegibility",resid,idx,fldName,1);
      }
   });
   $("[name*='chkOtherInfo_']").click(function(){
      ctrlObjs($(this).attr("id"),
               $(this).is(":checked"),
               "EntryOtherInfo_");
      $(this).val($(this).is(":checked"));
      chkBoxID = $(this).attr("id");
      var idx = chkBoxID.split("_")[1];
      var resid = $(this).attr("refid");
      if (!$(this).is(":checked")){
         var fldName = $(this).attr("fldName");
         rtrnVal("employeesotherinfo",resid,idx,fldName,1);
      }
   });
   $("[name*='chkRef_']").click(function(){
      ctrlObjs($(this).attr("id"),
               $(this).is(":checked"),
               "EntryRef_");
      $(this).val($(this).is(":checked"));
      chkBoxID = $(this).attr("id");
      var idx = chkBoxID.split("_")[1];
      var resid = $(this).attr("refid");
      if (!$(this).is(":checked")){
         var fldName = $(this).attr("fldName");
         rtrnVal("employeesreference",resid,idx,fldName,1);
      }
   });
   $("[name*='chkTrainProg_']").click(function(){
      ctrlObjs($(this).attr("id"),
               $(this).is(":checked"),
               "EntryTrainingProg_");
      $(this).val($(this).is(":checked"));
      chkBoxID = $(this).attr("id");
      var idx = chkBoxID.split("_")[1];
      var resid = $(this).attr("refid");
      if (!$(this).is(":checked")) {
         var fldName = $(this).attr("fldName");
         fldName = fldName.replace(/\s{2,10}/g, '');
         fldName.trim();
         rtrnVal("employeestraining",resid,idx,fldName,1);
      }
   });
   $("[name*='chkVolWork_']").click(function(){
      ctrlObjs($(this).attr("id"),
               $(this).is(":checked"),
               "EntryVolWork_");
      $(this).val($(this).is(":checked"));
      chkBoxID = $(this).attr("id");
      var idx = chkBoxID.split("_")[1];
      var resid = $(this).attr("refid");
      if (!$(this).is(":checked")){
         var fldName = $(this).attr("fldName");
         rtrnVal("employeesvoluntary",resid,idx,fldName,1);
      }
   });
   $("[name*='chkChild_']").each(function(){
      $(this).click(function(){
         chkBoxID = $(this).attr("id");
         var idx = chkBoxID.split("_")[1];
         ctrlObjs(chkBoxID,
                  $(this).is(":checked"),
                  "EntryChildrens_");
         $(this).val($(this).is(":checked"));
         var resid = $(this).attr("refid");
         if (!$(this).is(":checked")){
            var fldName = $(this).attr("fldName");
            rtrnVal("employeeschild",resid,idx,fldName,1);
         }
      });
   });

   $("[name*='chkElementary_']").each(function(){
      $(this).click(function(){
         ctrlObjs($(this).attr("id"),
               $(this).is(":checked"),
               "EntryElementary_");
         $(this).val($(this).is(":checked"));
         chkBoxID = $(this).attr("id");
         var idx = chkBoxID.split("_")[1];
         var resid = $(this).attr("refid");
         if (!$(this).is(":checked")){
            var fldName = $(this).attr("fldName");
            rtrnVal("employeeseduc",resid,idx,fldName,1);
         }
      });
   });
   $("[name*='chkSecondary_']").click(function(){
      ctrlObjs($(this).attr("id"),
               $(this).is(":checked"),
               "EntrySecondary_");
      $(this).val($(this).is(":checked"));
      chkBoxID = $(this).attr("id");
      var idx = chkBoxID.split("_")[1];
      var resid = $(this).attr("refid");
      if (!$(this).is(":checked")){
         var fldName = $(this).attr("fldName");
         rtrnVal("employeeseduc",resid,idx,fldName,1);
      }
   });
   $("[name*='chkVocCourse_']").click(function(){
      chkBoxID = $(this).attr("id");
      var idx = chkBoxID.split("_")[2];
      var resid = $(this).attr("refid");
      ctrlObjs($(this).attr("id"),
               $(this).is(":checked"),
               "EntryVocCourse_");
      $(this).val($(this).is(":checked"));
      if (!$(this).is(":checked")) {
         var fldName = $(this).attr("fldName");
         rtrnVal("employeeseduc",resid,idx,fldName,1);
      }
   });
   $("[name*='chkCollege_']").click(function(){
      ctrlObjs($(this).attr("id"),
               $(this).is(":checked"),
               "EntryCollege_");
      $(this).val($(this).is(":checked"));
      chkBoxID = $(this).attr("id");
      var idx = chkBoxID.split("_")[2];
      var resid = $(this).attr("refid");
      if (!$(this).is(":checked")){
         var fldName = $(this).attr("fldName");
         rtrnVal("employeeseduc",resid,idx,fldName,1);
      }
   });
   $("[name*='chkGradStudies_']").click(function(){
      ctrlObjs($(this).attr("id"),
               $(this).is(":checked"),
               "EntryGradStudies_");
      $(this).val($(this).is(":checked"));
      chkBoxID = $(this).attr("id");
      var idx = chkBoxID.split("_")[2];
      var resid = $(this).attr("refid");
      if (!$(this).is(":checked")){
         var fldName = $(this).attr("fldName");
         rtrnVal("employeeseduc",resid,idx,fldName,1);
      }
   });

   $("#chkPersonalInfo").click(function () {
      $(this).val($(this).is(":checked"));
   });
   $("#FamilyBG").click(function () {
      $(this).val($(this).is(":checked"));
   });

   /*Add Rows*/
   $("#addRowChildren").click(function() {
      AddRows("EntryChildrens_","contentChildrens","ChildrenRefId_");
   });
   $("#addRowVocCourse").click(function() {
      AddRows("EntryVocCourse_","VocCourse","VocationalRefId_");
   });
   $("#addRowGradStudies").click(function() {
      AddRows("EntryGradStudies_","GradStudies","GradStudiesRefId_");
   });
   $("#addRowEligibility").click(function() {
      AddRows("EntryEligib_","Eligibility","eligibilityRefId_");
   });
   $("#addRowWorkExp").click(function() {
      AddRows("EntryWorkExp_","WorkExp","workexperienceRefId_");
   });
   $("#addRowVolWork").click(function() {
      AddRows("EntryVolWork_","VoluntaryWork","voluntaryRefId_");
   });
   $("#addRowOtherInfo").click(function() {
      AddRows("EntryOtherInfo_","otherinfo","otherinfoRefId_");
   });
   $("#addRowCollege").click(function() {
      AddRows("EntryCollege_","College","collegeRefId_");
   });

   /*$("#addRowElementary").click(function() {
      AddRows("EntryElementary_","Elementary");
   });
   $("#addRowSecondary").click(function() {
      AddRows("EntrySecondary_","Secondary");
   });*/

   $("#addRowReference").click(function() {
      AddRows("EntryRef_","Reference","referenceRefId_");
   });
   $("#addRowTraining").click(function() {
      AddRows("EntryTrainingProg_","TrainProg","trainingRefId_");
   });

   if ($("#hUserGroup").val() == "COMPEMP") {
      //$(".form-input, input[name*='YAns_'], input[name*='NAns_']").prop("disabled",true);
      $(".form-input, input[name*='YAns_'], input[name*='NAns_']").prop("disabled",true);
      //$("").prop("readonly",true);

      /*$(".addRow, input[id*='childrens_'], input[id*='Elementary_'], input[id*='Secondary_'], input[id*='VocCourse_'], input[id*='College_'], input[id*='GradStudies_']").hide();
      $("input[id*='VolWork_'], input[id*='Eligib_'], input[id*='exp_'], input[id*='TrainProg_']").hide();
      $("input[id*='OtherInfo_'], input[id*='Ref_']").hide();*/
      //$(".addRow").hide();
      tabClick(1);
      load201();
   } else {
      $("#DataEntry, #CountryCitizen").hide();
   }

   $("[id*='sint_Present_']").each(function () {
      $(this).click(function () {
         sint_Present_Click($(this).attr("id"));
      });
   });

   $("#btnUPDATE").click(function () {
      saveEmployees201();
   });


   $("#btnSAVE201").click(function () {
      save201();
   });
   $("#btnSAVE201Admin").click(function () {
      save201();
   });


   $("#btnPREVIEW").click(function () {
      filepreview();
      $("#btnSAVE201").hide();
   });
   $("[name='sint_ResiAddProvinceRefId']").change(function () {
      setValueByName("char_PermanentAddZipCode","");
      setValueByName("char_ResiAddZipCode","");   
      loadCity("sint_ResiAddCityRefId",$(this).val());
      loadCity("sint_PermanentAddCityRefId",$(this).val());
   });
   $("[name='sint_PermanentAddProvinceRefId']").change(function () {
      setValueByName("char_PermanentAddZipCode","");
      setValueByName("char_ResiAddZipCode","");   
      loadCity("sint_ResiAddCityRefId",$(this).val());
      loadCity("sint_PermanentAddCityRefId",$(this).val());
   });
   $(".mypanel > .panel-top").each(function () {
      if ($(this).attr("for") != undefined) {
         $(this).click(function () {
            $("#"+$(this).attr("for")).slideToggle(1000);

         });
      }
   });
   /*$(".enabler--").each(function () {
      $(this).click(function () {
         $("#"+$(this).attr("for") + " .form-input")
         .not("[name='char_FirstName'], [name='char_LastName'], [name='char_MiddleName'], [name='char_AgencyId'], [name='char_EmailAddress'], [name='char_SexOpt'], [name='char_ExtName']")
         .prop("disabled",!($(this).is(":checked")));
      });
   });*/
   $("[name='sint_ResiAddCityRefId']").change(function () {
      getZipCode($(this).val(),'char_ResiAddZipCode');
   });
   $("[name='sint_PermanentAddCityRefId']").change(function () {
      getZipCode($(this).val(),'char_PermanentAddZipCode');
   });

   $("[name*='char_HighestGrade']").each(function () {
      HighestGrade_validation($(this).attr("name"));
   });
   $(".split--").prop("disabled",false);
   $("#aSplitSettings").click(function () {
      $("#split").modal();
   });
   //$(".mypanel > .panel-mid-litebg").slideUp();
});
      
function setObjBhvr(EmpRefId) {
   $.get("trn.e2e.php",
   {
      fn:"setObjEnabled",
      emprefid:EmpRefId
   },
   function(data,status) {
      if (status == "success") {
         try {
            eval(data);
         } catch (e) {
            if (e instanceof SyntaxError) {
               alert(e.message);
            }
         }
      }
   });

}

function saveEmployees201() {
      $("#btnSAVE201").show();
      var i = 0;
      var j = 0;
      if ($("#chkPersonalInfo").is(":checked")) {
         var bdate = $("#BirthDate").val();
         var age = calcAge(bdate);
         if (age <= 18) {
            $("#BirthDate").val("");
            $("#BirthDate").focus();
            alert("Employee Must Above 18 years old.");
            return false;
         }
      }
      var err = saveProceed();
      if (err == 0) {
         if ($("#chkPersonalInfo").is(":checked")) {
            if ($("[name='char_TelNo']").val() == "" && $("[name='char_MobileNo']").val() == "") {
               alert("Missing Entry!!!");
               $("[name='char_MobileNo']").notify("You need to supply atleast one","warn",{position:"bottom"});
               $("[name='char_TelNo']").notify("You need to supply atleast one","warn",{position:"bottom"});
               $("[name='char_MobileNo']").focus();
               j++;
            }
         }
         i = 0;
         $("[name*='chkWorkExp_']").each(function () {
            i++;
            if ($(this).is(":checked")) {
               var salgrade = $("[name='sint_SalaryGradeRefId_"+i+"']").val();
               var jobgrade = $("[name='sint_JobGradeRefId_"+i+"']").val();
               if ($("[name='sint_isGovtService_"+i+"']").val() == 1) {
                  if (salgrade == 0 && jobgrade == 0) {
                     alert("Missing Entry!!!");
                     $("[name='sint_JobGradeRefId_"+i+"']").notify("You need to supply atleast one","warn",{position:"bottom"});
                     $("[name='sint_SalaryGradeRefId_"+i+"']").notify("You need to supply atleast one","warn",{position:"bottom"});
                     $("[name='sint_SalaryGradeRefId_"+i+"']").focus();
                     j++;
                  }
               }

            }
         });
         /*
         if ($("[name='char_GovtIssuedIDDate']").val() == "") {
            char_GovtIssuedIDPlace
            char_GovtIssuedIDNo
            char_GovtIssuedID"
         }
         */

         // Date Range Validation
         i = 0;
         $("[name*='chkVolWork_']").each(function () {
            i++;
            if ($(this).is(":checked")) {
               if (chkDateRange("date_StartDate_"+i,"date_EndDate_"+i)) j++;
            }
         });

         i = 0;
         $("[name*='chkElementary_']").each(function () {
            i++;
            if ($(this).is(":checked")) {
               if (chkYearRange("sint_DateFrom_1_"+i,"sint_DateTo_1_"+i)) j++;
            }
         });

         i = 0;
         $("[name*='chkSecondary_']").each(function () {
            i++;
            if ($(this).is(":checked")) {
               if (chkYearRange("sint_DateFrom_2_"+i,"sint_DateTo_2_"+i)) j++;
            }
         });

         i = 0;
         $("[name*='chkVocCourse_3_']").each(function () {
            i++;
            if ($(this).is(":checked")) {
               if (chkYearRange("sint_DateFrom_3_"+i,"sint_DateTo_3_"+i)) j++;
               if (chkEducData(i,3)) j++;
            }
         });

         i = 0;
         $("[name*='chkCollege_4_']").each(function () {
            i++;
            if ($(this).is(":checked")) {
               if (chkYearRange("sint_DateFrom_4_"+i,"sint_DateTo_4_"+i)) j++;
               if (chkEducData(i,4)) j++;
            }
         });

         i = 0;
         $("[name*='chkGradStudies_5_']").each(function () {
            i++;
            if ($(this).is(":checked")) {
               if (chkYearRange("sint_DateFrom_5_"+i,"sint_DateTo_5_"+i)) j++;
               if (chkEducData(i,5)) j++;
            }
         });

         i = 0;
         $("[name*='chkWorkExp_']").each(function () {
            i++;
            if ($(this).is(":checked")) {
               if (chkDateRange("date_WorkStartDate_"+i,"date_WorkEndDate_"+i)) j++;
               if (
                     $("[name='sint_JobGradeRefId_"+i+"']").val() > 0 &&
                     $("[name='sint_SalaryGradeRefId_"+i+"']").val() > 0 &&
                     $("[name='sint_isGovtService_"+i+"']").val() == 1
                  )
               {
                  $("[name='sint_JobGradeRefId_"+i+"']").notify("You can select only 1 Grade !!!","error",{position:"bottom"});
                  $("[name='sint_SalaryGradeRefId_"+i+"']").notify("You can select only 1 Grade !!!","error",{position:"bottom"});
                  j++;
               }
            }
         });

         i = 0;
         $("[name*='chkTrainProg_']").each(function () {
            i++;
            if ($(this).is(":checked")) {
               if (chkDateRange("date_StartDate_"+i+"_TrainProg","date_EndDate_"+i+"_TrainProg")) j++;
            }
         });

         i = 0;
         $("[name*='chkEligib_']").each(function () {
            i++;
            if ($(this).is(":checked")) {
               if (chkDateRange("date_ExamDate_"+i,"date_LicenseReleasedDate_"+i)) j++;
            }
         });

         i = 0;
         $("[name*='chkOtherInfo_']").each(function () {
            i++;
            if ($(this).is(":checked")) {
               if ($("[name='char_Skills_"+i+"']").val() == "" && $("[name='char_Recognition_"+i+"']").val() == "" && $("[name='char_Affiliates_"+i+"']").val() == "") {
                  $.notify("You need to supply atleast one in Other Info","warn",{position:"top center"});
                  $("[name*='char_Skills_']").focus();
                  j++;
               }
            }
         });
         if (j == 0) {
            /*filepreview();*/
            save201();
         }
      } else {
         $.notify(err + " Error Encountered" ,{position:"top center"});
      }
}

function chkEducData(idx,lvl) {
   var obj = "";
   if ($("[name='sint_Present_"+lvl+"_"+idx+"']").is(":checked")) {
      if ($("[name='sint_YearGraduated_"+lvl+"_"+idx+"']").val() > 0) {
         obj = "sint_YearGraduated_" + lvl + "_" + idx;
         $("[name='" + obj + "']").focus();
         $("[name='" + obj + "']").addClass("innerRedShadow");
         $("[name='" + obj + "']").notify("Please remove Year Graduate if you are still studying","warn",{position:"bottom"});
         return true;
      }
   }
   if (
         $("[name='char_HighestGrade_"+lvl+"_"+idx+"']").val() != "" &&
         $("[name='sint_YearGraduated_"+lvl+"_"+idx+"']").val() > 0
      ) {
      obj = "char_HighestGrade_" + lvl + "_" + idx;   
      $("[name='" + obj + "']").focus();
      $("[name='" + obj + "']").addClass("innerRedShadow");
      $("[name='" + obj + "']").notify("Atleast 1 Entry only","warn",{position:"bottom"});
      $("[name='sint_YearGraduated_"+lvl+"_"+idx+"']").notify("Atleast 1 Entry only","warn",{position:"bottom"});
      return true;
   } else if (
      $("[name='char_HighestGrade_"+lvl+"_"+idx+"']").val() == "" &&
      $("[name='sint_YearGraduated_"+lvl+"_"+idx+"']").val() == 0
   ) {
      obj = "char_HighestGrade_" + lvl + "_" + idx;   
      $("[name='" + obj + "']").focus();
      $("[name='" + obj + "']").addClass("innerRedShadow");
      $("[name='" + obj + "']").notify("You need to supply atleast one","warn",{position:"bottom"});
      $("[name='sint_YearGraduated_"+lvl+"_"+idx+"']").notify("You need to supply atleast one","warn",{position:"bottom"});
      return true;
   }
   if (
      $("[name='sint_YearGraduated_" + lvl + "_" + idx + "']").val() > 0 &&
      $("[name='sint_DateTo_" + lvl + "_" + idx + "']").val() == 9999
   ) {
      obj = "sint_DateTo_" + lvl + "_" + idx;  
      $("[name='" + obj + "']").focus();
      $("[name='" + obj + "']").addClass("innerRedShadow");
      $("[name='" + obj + "']").notify("On going value with Year Graduated is not allowed", "warn", { position: "bottom" });
      return true;
   }
   return false;
}

function save201() {
   //alert($("#hRefId").val());
   //return false;
   if (saveProceed() == 0) {
      if (confirm("Continue to Save?")) {
         $("[group='1']").prop("disabled",false);
         $("#hEmpRefId").append('<input type="hidden" name="hCount_Childrens" value="' + $("#contentChildrens .enabler--").length + '">');
         $("#hEmpRefId").append('<input type="hidden" name="hCount_WorkExp" value="' + $("#WorkExp .enabler--").length + '">');
         $("#hEmpRefId").append('<input type="hidden" name="hCount_Eligibility" value="' + $("#Eligibility .enabler--").length + '">');
         $("#hEmpRefId").append('<input type="hidden" name="hCount_VoluntaryWork" value="' + $("#VoluntaryWork .enabler--").length + '">');
         $("#hEmpRefId").append('<input type="hidden" name="hCount_TrainProg" value="' + $("#TrainProg .enabler--").length + '">');
         $("#hEmpRefId").append('<input type="hidden" name="hCount_otherinfo" value="' + $("#otherinfo .enabler--").length + '">');
         $("#hEmpRefId").append('<input type="hidden" name="hCount_VocCourse" value="' + $("#VocCourse .enabler--").length + '">');
         $("#hEmpRefId").append('<input type="hidden" name="hCount_GradStudies" value="' + $("#GradStudies .enabler--").length + '">');
         $("#hEmpRefId").append('<input type="hidden" name="hCount_Reference" value="' + $("#refer .enabler--").length + '">');
         $("#hEmpRefId").append('<input type="hidden" name="hCount_College" value="' + $("#College .enabler--").length + '">');
         $.ajax({
            url: "trn.e2e.php",
            type: "POST",
            data: new FormData($("[name='xForm']")[0]),
            success : function(responseTxt){
               eval(responseTxt);
            },
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false
         });
      }
   }
}

function validateGrade2(objPartner) {
   var idx = objPartner.split("_")[2];
   if (objPartner.indexOf("JobGrade") > 0) {
      var objCaller = "sint_SalaryGradeRefId_" + idx;

   } else {
      var objCaller = "sint_JobGradeRefId_" + idx;
   }
   validateGrade(objPartner,idx);
}

function chkYearRange(dtObjFrom,dtObjTo) {
   var StartDate = $("[name='"+dtObjFrom+"']").val();
   var EndDate = $("[name='"+dtObjTo+"']").val();
   var j = 0;
   if (StartDate > EndDate) {
      $.notify("Invalid Year Range !!!","error",{position:"top center"});
      $("[name='"+dtObjFrom+"']").focus();
      j = 1;
      return j; 
   } else {
      return j; 
   }
}

function chkDateRange(dtObjFrom,dtObjTo) {
   var StartDate = $("[name='"+dtObjFrom+"']").val();
   var EndDate = $("[name='"+dtObjTo+"']").val();
   if (EndDate == "Present") EndDate = "2999-12-31";
   if (StartDate != "" && EndDate != "") {
      if (dateUTC(StartDate) > dateUTC(EndDate)) {
         var objName = $("[name='"+dtObjTo+"']").attr("name");
         if (objName.indexOf("LicenseReleasedDate") > 0) {
            $("[name='"+dtObjTo+"']").notify("LICENSE Date Not be lesser than to Exam Date!!!","error",{position:"top"});
         } else {
            $.notify("Invalid Date Range !!!","error",{position:"top center"});
         }
         $("[name='"+dtObjFrom+"']").focus();

         return true;
      } else return false;
   } else return false;
}

function sint_Present_Click(objId) {
   var idx = objId.split("_")[2];
   var ltype = objId.split("_")[1];
   $("[name='"+$("#" + objId).attr("yrGrad")+"']").prop("disabled",false);
   if ($("#" + objId).is(":checked")) {
      $("#" + objId).val("1");
      $("#" + objId).attr("currValue",$("[name='"+$("#" + objId).attr("for")+"']").val());
      $("[name='"+$("#" + objId).attr("for")+"']").val("Present");
      $("[name='"+$("#" + objId).attr("for")+"']").prop("disabled",true);
   } else {
      $("#" + objId).val("0");
      $("[name='"+$("#" + objId).attr("for")+"']").val($("#" + objId).attr("currValue"));
      $("[name='"+$("#" + objId).attr("for")+"']").prop("disabled",false);
   }
}

function clickAnsYN(val,idx) {
   $("[name='sint_"+idx+"']").val(val);
   if ($("[name='sint_"+idx+"']").val() == 1){
      $("[name='char_"+idx+"exp']").prop("disabled",false);
      if (idx == "Q2b") idx = "Q2";
      $("[name='date_"+idx+"DateFiled']").prop("disabled",false);
      $("[name='char_"+idx+"CaseStatus']").prop("disabled",false);
   } else {
      $("[name='char_"+idx+"exp']").prop("disabled",true);
      $("[name='char_"+idx+"exp']").val("");
      if (idx == "Q2b") idx = "Q2";
      $("[name='date_"+idx+"DateFiled']").val("");
      $("[name='char_"+idx+"CaseStatus']").val("");
      $("[name='date_"+idx+"DateFiled']").prop("disabled",true);
      $("[name='char_"+idx+"CaseStatus']").prop("disabled",true);
   }
}

function AddRows(parentId,gparentId,refid) {
   // $.notify(parentId + "1");
   // $.notify(gparentId + "2");
   // $.notify(refid + "3");
   var appends = "";
   var rowLength = $("[id*='"+parentId+"']").length;
   appends = $("#"+parentId+rowLength).html();
   var rowHTML = "", newAppends = "";

   if (appends.indexOf("_3_" + rowLength) > 0) {
     newAppends = appends.split("_3_" + rowLength).join("_3_" + (rowLength + 1));
   } else if (appends.indexOf("_5_" + rowLength) > 0) {
      newAppends = appends.split("_5_" + rowLength).join("_5_" + (rowLength + 1));
   } else if (appends.indexOf("idx_" + rowLength) > 0) {
      newAppends = appends.split("idx_" + rowLength).join("idx_" + (rowLength + 1));
   } else {
      newAppends = appends.split("_"+rowLength).join("_"+(rowLength+1));
   }

   //var btnCan = '<button type="button" class="btn-cls4-red trnbtn margin-top" id="bAddRowCancel_'+(rowLength + 1)+'" onClick=removeRow(this.id,"'+parentId+(rowLength+1)+'");><i class="fa fa-undo" aria-hidden="true"></i>&nbsp;Cancel Add Row</button>';
   newAppends = newAppends.split("#"+rowLength).join("#"+(rowLength+1));
   newAppends = newAppends.split('idx="' + rowLength + '"').join('idx="'+ (rowLength + 1) +'"');
   //newAppends = newAppends.split('class="enabler--"').join('type="checkbox" class="enabler--" onClick=removeRow("'+parentId+(rowLength+1)+'");');
   newAppends = newAppends.split('unclick="CancelAddRow"').join('onClick=removeRow("'+parentId+(rowLength+1)+'");');
   newAppends = newAppends.split('focus="validateGrade"').join('onChange=validateGrade2($(this).attr(\'partner\')); onFocus=validateGrade2($(this).attr(\'partner\'));');
   newAppends = newAppends.split('click="HighestGrade_validation"').join('onClick=HighestGrade_validation($(this).attr(\'name\'));');
   rowHTML += '<div id="'+parentId+(rowLength+1)+'" class="entry201">';
   rowHTML += newAppends;
   rowHTML += '</div>';
   $("#"+gparentId).append(rowHTML);
   $("[name='"+refid+(rowLength + 1)+"']").val("");
   if (rowHTML.indexOf("date--") > 0) {
      $("#" + parentId+(rowLength+1) + " input[class*='date--']").each(function () {
         $(this).keypress(fnDateClass($(this).attr("name")));
      });
   }
   if (rowHTML.indexOf("alpha--") > 0) {
      $("#" + parentId+(rowLength+1) + " input[class*='alpha--']").each(function () {
         var e = arguments[0];
         fnAlphaClass($(this).attr("name"),e);
      });
   }
   if (rowHTML.indexOf("number--") > 0) {
      $("#" + parentId+(rowLength+1) + " input[class*='number--']").each(function () {
         var e = arguments[0];
         fnNumberClass($(this).attr("name"),e);
      });
   }
   if (rowHTML.indexOf("valDate--") > 0) {
      $("#" + parentId+(rowLength+1) + " input[class*='valDate--']").each(function () {
         $(this).keypress(fnValDate($(this).attr("name")));
      });
   }
   if (rowHTML.indexOf('rowid="') > 0) {
      $("#" + parentId+(rowLength+1) + " input[class*='enabler--']").each(function () {
         $(this).attr("refid","");
         $("[name='"+$(this).attr("rowid")+"']").val("");
      });
   }
   if (rowHTML.indexOf('presentbox--') > 0) {
      $("#" + parentId+(rowLength+1) + " input[class*='presentbox--']").each(function () {
         $(this).attr("checked",false);
         $(this).click(function () {
            sint_Present_Click($(this).attr("id"));
         });
      });
   }
   // After Add Row
   //$("#"+parentId+(rowLength+1)).append(btnCan);
   $("#" +parentId+(rowLength+1)+ " .saveFields--").prop("disabled",false);
   $("#" +parentId+(rowLength+1)+ "> .enabler--").prop("checked",true);
   showBtnUpd();
}

function validateGrade(objPartner,idx) {
   if (objPartner.indexOf("JobGrade") > 0) {
      objCaller = "sint_SalaryGradeRefId_" + idx;
   } else {
      objCaller = "sint_JobGradeRefId_" + idx;
   }
   if (
         $("[name='sint_isGovtService_"+idx+"']").val() == 1 &&
         $("[name='chkWorkExp_"+idx+"']").is(":checked") &&
         $("[name='"+objPartner+"']").val() > 0
      )
   {
      $("[name='"+objCaller +"']").val(0);
      $("[name='"+objPartner+"']").focus();
      $("[name='"+objPartner+"']").notify("Ooops !!! You already have value here","info",{position:"bottom"});
   }
}

function removeRow(objID) {
   $("#"+objID).remove();
   //$("#"+id).remove();
}

function ctrlObjs(chkBoxID,isCHK,parent) {
   var idx = chkBoxID.split("_")[(chkBoxID.split("_").length - 1)];
   var parentID = parent+idx;
   $("#"+parentID+" .saveFields--").each(function() {
      var objname = $(this).attr("name");
      if (isCHK){
         $(this).attr("disabled",false);
      } else {
         if (objname.indexOf("RefId") >= 0) {
            $(this).val(0);
         } else {
            $(this).val("");
         }
         $(this).attr("disabled",true);
      }
   });
}

function afterNewSave(newRefId) {
   $.notify("New Record Inserted\nRef. ID: " + newRefId,"success",{position:"top center"});
}
function afterDelete() {
   alert("Successfully Deleted.");
   gotoscrn($("#hProg").val(),"");
}

function checkEntry(parentId) {
   var objvalue = "";
   var errCount = 0;
   $("#"+parentId+" .saveFields--").each(function()
   {
      ObjClassName = $(this).attr("class");
      Elem_Value = $(this).val();
      Elem_Name  = $(this).attr("name");

      if ($(this).is("select")) {
         Elem_Type  = "select";
      } else if ($(this).is("textarea")) {
         Elem_Type  = "textarea";
      } else {
         Elem_Type  = $(this).attr("type");
      }
      objvalue += Elem_Type + "|" + Elem_Name + "|" + Elem_Value + '!';
   });
   $.post("SystemAjax.e2e.php",
   {
      task  :"checkEntry",
      entry :objvalue,
      table :$("#hTable").val(),
      user  :$("#hUser").val(),
      prog  :$("#hProg").val()

   },
   function(data,status) {
      errCount = data;
   });
   return errCount;
}

function fAddressCopy(chk) {
   if (chk) {
      setValueByName("char_PermanentHouseNo",$("[name='char_ResiHouseNo']").val());
      setValueByName("char_PermanentStreet",$("[name='char_ResiStreet']").val());
      setValueByName("char_PermanentBrgy",$("[name='char_ResiBrgy']").val());
      setValueByName("char_PermanentAddZipCode",$("[name='char_ResiAddZipCode']").val());
      setValueByName("sint_PermanentCountryRefId",$("[name='sint_ResiCountryRefId']").val());
      setValueByName("sint_PermanentAddProvinceRefId",$("[name='sint_ResiAddProvinceRefId']").val());
      setValueByName("sint_PermanentAddCityRefId",$("[name='sint_ResiAddCityRefId']").val());
      setValueByName("char_PermanentSubd",$("[name='char_ResiSubd']").val());

   } else {
      setValueByName("char_PermanentHouseNo","");
      setValueByName("char_PermanentStreet","");
      setValueByName("char_PermanentBrgy","");
      setValueByName("char_PermanentAddZipCode","");
      setValueByName("sint_PermanentCountryRefId",0);
      setValueByName("sint_PermanentAddProvinceRefId",0);
      setValueByName("sint_PermanentAddCityRefId",0);
      setValueByName("char_PermanentSubd","");
   }
}

function load201() {
   if ($("#hUserGroup").val() == "COMPEMP") {
      var emprefid = $("#hEmpRefId").val();
   } else {
      var emprefid = $("#hRefId").val();
   }
   //document.getElementById("overlay").style.display = "block";
   $.get("trn.e2e.php",
   {
      fn:"LoadEmp201",
      emprefid: emprefid
   },
   function(data,status) {
      if (status == "success") {
         try {
            //alert(emprefid);
            eval(data);
         } catch (e) {
            if (e instanceof SyntaxError) {
               alert(e.message);
            }
         }
      }
   });
}

function dateUTC(dateString) {
   var dtArr = dateString.split("-");
   if (dtArr.length == 3) {
      return Date.UTC(dtArr[0],dtArr[1] - 1,dtArr[2]);
   } else {
      alert("Invalid Format !!!");
      return false;
   }
}

function rtrnVal(tbl,refid,idx,fldName,isMultiple){
   $.get("trn.e2e.php",
   {
      fn:"rtrnVal",
      table:tbl,
      refid:refid,
      idx:idx,
      fldName:fldName,
      isMultiple:isMultiple,
      hCompanyID:$("#hCompanyID").val(),
      hBranchID:$("#hBranchID").val(),
      hEmpRefId:$("#hEmpRefId").val(),
      hUserRefId:$("#hUserRefId").val()

   },
   function(data,status) {
      if (status == "success") {
         try {
            //console.log(data);
            eval(data);
         } catch (e) {
            if (e instanceof SyntaxError) {
               alert(e.message);
            }
         }
      }
   });
}
function filepreview() {
   var xname = "";
   var xval = "";
   var namenval = "";
   var refid = "";
   var tbl = "";
   var row = "";
   var i = 0;
   $("select, input").each(function () {
      var objname = $(this).attr("name");
      var objval = $(this).val();
      switch (objname) {
         case "sint_SchoolsRefId_1_1":
            objname = getName(objval,"schools",objname);
         break;
         case "sint_SchoolsRefId_2_1":
            objname = getName(objval,"schools",objname);
         break;
         case "sint_SchoolsRefId_4_1":
            objname = getName(objval,"schools",objname);
         break;
         case "sint_CourseRefId_4_1":
            objname = getName(objval,"course",objname);
         break;
         case "sint_BloodTypeRefId":
            objname = getName(objval,"BloodType",objname);
         break;
         case "sint_ResiCountryRefId":
            objname = getName(objval,"country",objname);
         break;
         case "sint_ResiAddProvinceRefId":
            objname = getName(objval,"province",objname);
         break;
         case "sint_ResiAddCityRefId":
            objname = getName(objval,"city",objname);
         break;
         case "sint_PermanentCountryRefId":
            objname = getName(objval,"country",objname);
         break;
         case "sint_PermanentAddProvinceRefId":
            objname = getName(objval,"province",objname);
         break;
         case "sint_PermanentAddCityRefId":
            objname = getName(objval,"city",objname);
         break;
         case "sint_OccupationsRefId":
            objname = getName(objval,"occupations",objname);
         break;
         default:
            objname = objname;
         break;
      }
      if (objname == "char_CivilStatus") {
         switch (objval) {
            case "Si":
               objval = "Single";
            break;
            case "Ma":
               objval = "Married";
            break;
            case "An":
               objval = "Annuled";
            break;
            case "Wi":
               objval = "Widowed";
            break;
            case "Se":
               objval = "Separated";
            break;
            case "Ot":
               objval = "Others";
            break;
         }
      }
      
      //xval += objval+"\n";
      //xname += objname+"\n";
      //namenval += objname + "------" + objval+"\n";

      if (objval == "" || objval == null) objval = "N/A";
      if (objname != undefined) {
         if ("val_" + objname != "val_char_EmailAdd") {
            $("#val_" + objname).html(objval.toUpperCase());
         } else {
            $("#val_" + objname).html(objval);
         }
      }
   });
   if ($("[name='sint_CountryCitizenRefId']").val() != 0) {
      getName($("[name='sint_CountryCitizenRefId']").val(),"country","sint_CountryRefId");   
   } else {
      $("#val_sint_CountryRefId").html("N/A");
   }
   if ($("#isFilipino").prop("checked")) {
      $("#val_sint_isFilipino").html("FILIPINO");
   } else {
      if ($("#isByBirth").prop("checked")) {
         $("#val_sint_isFilipino").html("DUAL BY BIRTH");
      } else if ($("#isNaturalization").prop("checked")) {
         $("#val_sint_isFilipino").html("DUAL BY NATURALIZATION");
      } else {
         $("#val_sint_isFilipino").html("DUAL");   
      }
   }
   //alert(xval);
   //alert(xname);
   //return false;
   //alert(namenval);

   //START OF CHILD
   i = 0;
   row = "";
   row += '<div class="row padd5">';
   row += '<table style="width:100%;border-collapse:collapse;" border=1>';
   row += '<thead><tr>';
   row += '<th class="fieldValueHDR--">FULLNAME</th>';
   row += '<th class="fieldValueHDR--">GENDER</th>';
   row += '<th class="fieldValueHDR--">DATE OF BIRTH</th>';
   row += '</tr></thead><tbody>';
   $("[id*='EntryChildrens_']").each(function () {
      i++;
      var name = $("[name='char_FullName_"+i+"']").val();
      var bday = $("[name='date_BirthDate_"+i+"']").val();
      var sex = $("[name='char_Gender_"+i+"']").val();
      if (bday == "") bday = "N/A";
      if (name == "") name = "N/A";
      if (sex == "M") {
         sex = "MALE";
      } else if (sex == "F") {
         sex = "FEMALE";
      } else {
         sex = "N/A";
      }
      row += '<tr>';
      row += '    <td class="fieldValue--">'+name+'</td>';
      row += '    <td class="fieldValue--">'+sex+'</td>';
      row += '    <td class="fieldValue--">'+bday+'</td>';
      row += '</tr>';
   });
   row += '</tbody></table></div>';
   $("#contentChild").html(row);
   //END OF CHILD

   //START OF ELEMENTARY
   i = 0;
   row = "";
   row += '<div class="row padd5">';
   row += '<table style="width:100%;border-collapse:collapse;" border=1>';
   row += '<thead>';
   row += '<tr>';
   row += '<th class="fieldValueHDR--" style="width:40%">SCHOOL</th>';
   row += '<th class="fieldValueHDR--" style="width:10%">YEAR GRADUATED</th>';
   row += '<th class="fieldValueHDR--" style="width:15%">HIGHEST GRADE/LEVEL/UNITS EARNED</th>';
   row += '<th class="fieldValueHDR--" style="width:10%">INCLUSIVE DATE FROM</th>';
   row += '<th class="fieldValueHDR--" style="width:10%">INCLUSIVE DATE TO</th>';
   row += '<th class="fieldValueHDR--" style="width:15%">SCHOLARSHIP/ACADEMIC HONORS RECEIVED</th>';
   row += '</tr>';
   row += '</thead>';
   row += '<tbody>';
      $("[id*='EntryElementary_']").each(function () {
         i++;
         var year         = $("[name='sint_YearGraduated_1_"+i+"']").val();
         var grade        = $("[name='char_HighestGrade_1_"+i+"']").val();
         var datefrom     = $("[name='sint_DateFrom_1_"+i+"']").val();
         var dateto       = $("[name='sint_DateTo_1_"+i+"']").val();
         var honor        = $("[name='char_Honors_1_"+i+"']").val();
         var school       = $("[name='sint_SchoolsRefId_1_"+i+"']").val();
         var school_objId = "sint_SchoolsRefId_1_"+i;
         school           = getName(school,"schools",school_objId);
         if (year == "" || year == 0) year = "N/A";
         if (grade == "") grade = "N/A";
         if (datefrom == "" || datefrom == 0 || datefrom == null) datefrom = "N/A";
         if (dateto == "" || dateto == 0 || dateto == null) dateto = "N/A";
         if (honor == "") honor = "N/A";
         if (dateto == 9999) dateto = "ON GOING";
         if (year == 9999) year = "ON GOING";
         row += '<tr>';
         row += '<td class="fieldValue--" id="val_'+school_objId+'">'+school+'</td>';
         row += '<td class="fieldValue--">'+year+'</td>';
         row += '<td class="fieldValue--">'+grade+'</td>';
         row += '<td class="fieldValue--">'+datefrom+'</td>';
         row += '<td class="fieldValue--">'+dateto+'</td>';
         row += '<td class="fieldValue--">'+honor+'</td>';
         row += '</tr>';
      });
   row += '</tbody>';
   row += '</table>';
   row += '</div>';
   $("#elem_content").html(row);
   //END OF ELEMENTARY

   //START OF SECONDARY
   i = 0;
   row = "";
   row += '<div class="row padd5">';
   row += '<table style="width:100%;border-collapse:collapse;" border=1>';
   row += '<thead>';
   row += '<tr>';
   row += '<th class="fieldValueHDR--" style="width:40%">SCHOOL</th>';
   row += '<th class="fieldValueHDR--" style="width:10%">YEAR GRADUATED</th>';
   row += '<th class="fieldValueHDR--" style="width:15%">HIGHEST GRADE/LEVEL/UNITS EARNED</th>';
   row += '<th class="fieldValueHDR--" style="width:10%">INCLUSIVE DATE FROM</th>';
   row += '<th class="fieldValueHDR--" style="width:10%">INCLUSIVE DATE TO</th>';
   row += '<th class="fieldValueHDR--" style="width:15%">SCHOLARSHIP/ACADEMIC HONORS RECEIVED</th>';
   row += '</tr>';
   row += '</thead>';
   row += '<tbody>';
      $("[id*='EntrySecondary_']").each(function () {
         i++;
         var year         = $("[name='sint_YearGraduated_2_"+i+"']").val();
         var grade        = $("[name='char_HighestGrade_2_"+i+"']").val();
         var datefrom     = $("[name='sint_DateFrom_2_"+i+"']").val();
         var dateto       = $("[name='sint_DateTo_2_"+i+"']").val();
         var honor        = $("[name='char_Honors_2_"+i+"']").val();
         var school       = $("[name='sint_SchoolsRefId_2_"+i+"']").val();
         var school_objId = "sint_SchoolsRefId_2_"+i;
         school           = getName(school,"schools",school_objId);
         if (year == "" || year == 0) year = "N/A";
         if (grade == "") grade = "N/A";
         if (datefrom == "" || datefrom == 0 || datefrom == null) datefrom = "N/A";
         if (dateto == "" || dateto == 0 || dateto == null) dateto = "N/A";
         if (dateto == 9999) dateto = "ON GOING";
         if (year == 9999) year = "ON GOING";
         if (honor == "") honor = "N/A";
         row += '<tr>';
         row += '<td class="fieldValue--" id="val_'+school_objId+'">'+school+'</td>';
         row += '<td class="fieldValue--">'+year+'</td>';
         row += '<td class="fieldValue--">'+grade+'</td>';
         row += '<td class="fieldValue--">'+datefrom+'</td>';
         row += '<td class="fieldValue--">'+dateto+'</td>';
         row += '<td class="fieldValue--">'+honor+'</td>';
         row += '</tr>';
      });
   row += '</tbody>';
   row += '</table>';
   row += '</div>';
   $("#secondary_content").html(row);
   //END OF SECONDARY

   //START OF VOCATIONAL
   i = 0;
   row = "";
   row += '<div class="row padd5">';
   row += '<table style="width:100%;border-collapse:collapse;" border=1>';
   row += '<thead>';
   row += '<tr>';
   row += '<th class="fieldValueHDR--" style="width:20%">SCHOOL</th>';
   row += '<th class="fieldValueHDR--" style="width:20%">DEGREE COURSE</th>';
   row += '<th class="fieldValueHDR--" style="width:10%">YEAR GRADUATED</th>';
   row += '<th class="fieldValueHDR--" style="width:15%">HIGHEST GRADE/LEVEL/UNITS EARNED</th>';
   row += '<th class="fieldValueHDR--" style="width:10%">INCLUSIVE DATE FROM</th>';
   row += '<th class="fieldValueHDR--" style="width:10%">INCLUSIVE DATE TO</th>';
   row += '<th class="fieldValueHDR--" style="width:15%">SCHOLARSHIP/ACADEMIC HONORS RECEIVED</th>';
   row += '</tr>';
   row += '</thead>';
   row += '<tbody>';
      $("[id*='EntryVocCourse_']").each(function () {
         i++;
         var year         = $("[name='sint_YearGraduated_3_"+i+"']").val();
         var grade        = $("[name='char_HighestGrade_3_"+i+"']").val();
         var datefrom     = $("[name='sint_DateFrom_3_"+i+"']").val();
         var dateto       = $("[name='sint_DateTo_3_"+i+"']").val();
         var honor        = $("[name='char_Honors_3_"+i+"']").val();
         var school       = $("[name='sint_SchoolsRefId_3_"+i+"']").val();
         var course       = $("[name='sint_CourseRefId_3_"+i+"']").val();
         var school_objId = "sint_SchoolsRefId_3_"+i;
         var course_objId = "sint_CourseRefId_3_"+i;
         school           = getName(school,"schools",school_objId);
         course           = getName(course,"course",course_objId);
         if (year == "" || year == 0) year = "N/A";
         if (grade == "") grade = "N/A";
         if (datefrom == "" || datefrom == 0 || datefrom == null) datefrom = "N/A";
         if (dateto == "" || dateto == 0 || dateto == null) dateto = "N/A";
         if (dateto == 9999) dateto = "ON GOING";
         if (year == 9999) year = "ON GOING";
         if (honor == "") honor = "N/A";

         row += '<tr>';
         row += '<td class="fieldValue--" id="val_'+school_objId+'">'+school+'</td>';
         row += '<td class="fieldValue--" id="val_'+course_objId+'">'+course+'</td>';
         row += '<td class="fieldValue--">'+year+'</td>';
         row += '<td class="fieldValue--">'+grade+'</td>';
         row += '<td class="fieldValue--">'+datefrom+'</td>';
         row += '<td class="fieldValue--">'+dateto+'</td>';
         row += '<td class="fieldValue--">'+honor+'</td>';
         row += '</tr>';
      });
   row += '</tbody>';
   row += '</table>';
   row += '</div>';
   $("#Vocationalcontent").html(row);
   //END OF VOCATIONAL

   //START OF COLLEGE
   i = 0;
   row = "";
   row += '<div class="row padd5">';
   row += '<table style="width:100%;border-collapse:collapse;" border=1>';
   row += '<thead>';
   row += '<tr>';
   row += '<th class="fieldValueHDR--" style="width:20%">SCHOOL</th>';
   row += '<th class="fieldValueHDR--" style="width:20%">DEGREE COURSE</th>';
   row += '<th class="fieldValueHDR--" style="width:10%">YEAR GRADUATED</th>';
   row += '<th class="fieldValueHDR--" style="width:15%">HIGHEST GRADE/LEVEL/UNITS EARNED</th>';
   row += '<th class="fieldValueHDR--" style="width:10%">INCLUSIVE DATE FROM</th>';
   row += '<th class="fieldValueHDR--" style="width:10%">INCLUSIVE DATE TO</th>';
   row += '<th class="fieldValueHDR--" style="width:15%">SCHOLARSHIP/ACADEMIC HONORS RECEIVED</th>';
   row += '</tr>';
   row += '</thead>';
   row += '<tbody>';
      $("[id*='EntryCollege_']").each(function () {
         i++;
         var year         = $("[name='sint_YearGraduated_4_"+i+"']").val();
         var grade        = $("[name='char_HighestGrade_4_"+i+"']").val();
         var datefrom     = $("[name='sint_DateFrom_4_"+i+"']").val();
         var dateto       = $("[name='sint_DateTo_4_"+i+"']").val();
         var honor        = $("[name='char_Honors_4_"+i+"']").val();
         var school       = $("[name='sint_SchoolsRefId_4_"+i+"']").val();
         var course       = $("[name='sint_CourseRefId_4_"+i+"']").val();
         var school_objId = "sint_SchoolsRefId_4_"+i;
         var course_objId = "sint_CourseRefId_4_"+i;
         school           = getName(school,"schools",school_objId);
         course           = getName(course,"course",course_objId);
         if (year == "" || year == 0) year = "N/A";
         if (grade == "") grade = "N/A";
         if (datefrom == "" || datefrom == 0 || datefrom == null) datefrom = "N/A";
         if (dateto == "" || dateto == 0 || dateto == null) dateto = "N/A";
         if (dateto == 9999) dateto = "ON GOING";
         if (year == 9999) year = "ON GOING";
         if (honor == "") honor = "N/A";

         row += '<tr>';
         row += '<td class="fieldValue--" id="val_'+school_objId+'">'+school+'</td>';
         row += '<td class="fieldValue--" id="val_'+course_objId+'">'+course+'</td>';
         row += '<td class="fieldValue--">'+year+'</td>';
         row += '<td class="fieldValue--">'+grade+'</td>';
         row += '<td class="fieldValue--">'+datefrom+'</td>';
         row += '<td class="fieldValue--">'+dateto+'</td>';
         row += '<td class="fieldValue--">'+honor+'</td>';
         row += '</tr>';
      });
   row += '</tbody>';
   row += '</table>';
   row += '</div>';
   $("#college_content").html(row);
   //END OF COLLEGE

   //START OF GRADUATE STUDIES
   i = 0;
   row = "";
   row += '<div class="row padd5">';
   row += '<table style="width:100%;border-collapse:collapse;" border=1>';
   row += '<thead>';
   row += '<tr>';
   row += '<th class="fieldValueHDR--" style="width:20%">SCHOOL</th>';
   row += '<th class="fieldValueHDR--" style="width:20%">DEGREE COURSE</th>';
   row += '<th class="fieldValueHDR--" style="width:10%">YEAR GRADUATED</th>';
   row += '<th class="fieldValueHDR--" style="width:15%">HIGHEST GRADE/LEVEL/UNITS EARNED</th>';
   row += '<th class="fieldValueHDR--" style="width:10%">INCLUSIVE DATE FROM</th>';
   row += '<th class="fieldValueHDR--" style="width:10%">INCLUSIVE DATE TO</th>';
   row += '<th class="fieldValueHDR--" style="width:15%">SCHOLARSHIP/ACADEMIC HONORS RECEIVED</th>';
   row += '</tr>';
   row += '</thead>';
   row += '<tbody>';
      $("[id*='EntryGradStudies_']").each(function () {
         i++;
         var year         = $("[name='sint_YearGraduated_5_"+i+"']").val();
         var grade        = $("[name='char_HighestGrade_5_"+i+"']").val();
         var datefrom     = $("[name='sint_DateFrom_5_"+i+"']").val();
         var dateto       = $("[name='sint_DateTo_5_"+i+"']").val();
         var honor        = $("[name='char_Honors_5_"+i+"']").val();
         var school       = $("[name='sint_SchoolsRefId_5_"+i+"']").val();
         var course       = $("[name='sint_CourseRefId_5_"+i+"']").val();
         var school_objId = "sint_SchoolsRefId_5_"+i;
         var course_objId = "sint_CourseRefId_5_"+i;
         school           = getName(school,"schools",school_objId);
         course           = getName(course,"course",course_objId);
         if (year == "" || year == 0) year = "N/A";
         if (grade == "") grade = "N/A";
         if (datefrom == "" || datefrom == 0 || datefrom == null) datefrom = "N/A";
         if (dateto == "" || dateto == 0 || dateto == null) dateto = "N/A";
         if (dateto == 9999) dateto = "ON GOING";
         if (year == 9999) year = "ON GOING";
         if (honor == "") honor = "N/A";

         row += '<tr>';
         row += '<td class="fieldValue--" id="val_'+school_objId+'">'+school+'</td>';
         row += '<td class="fieldValue--" id="val_'+course_objId+'">'+course+'</td>';
         row += '<td class="fieldValue--">'+year+'</td>';
         row += '<td class="fieldValue--">'+grade+'</td>';
         row += '<td class="fieldValue--">'+datefrom+'</td>';
         row += '<td class="fieldValue--">'+dateto+'</td>';
         row += '<td class="fieldValue--">'+honor+'</td>';
         row += '</tr>';
      });
   row += '</tbody>';
   row += '</table>';
   row += '</div>';
   $("#GradStudiescontent").html(row);
   //END OF GRADUATE STUDIES


   //START OF ELIGIBILITY
   i = 0;
   var row = "";
   row += '<div class="row padd5">';
   row += '<table style="width:100%;border-collapse:collapse;" border=1>';
   row += '<thead>';
   row += '<tr>';
   row += '<th class="fieldValueHDR--" style="width:30%">CAREER SERVICE/RA 1080 (BOARD/BAR) UNDER SPECIAL LAWS/CES/CSEE</th>';
   row += '<th class="fieldValueHDR--" style="width:10%">RATING</th>';
   row += '<th class="fieldValueHDR--" style="width:15%">DATE OF EXAMINATION/CONFERMENT</th>';
   row += '<th class="fieldValueHDR--" style="width:25%">PLACE OF EXAMINATION/CONFERMENT</th>';
   row += '<th class="fieldValueHDR--" style="width:10%">LICENSE NUMBER</th>';
   row += '<th class="fieldValueHDR--" style="width:10%">LICENSE RELEASED DATE</th>';
   row += '</tr>';
   row += '</thead>';
   row += '<tbody>';
      $("[id*='EntryEligib_']").each(function () {
         i++;
         var rating      = $("[name='sint_Rating_"+i+"']").val();
         var examdate    = $("[name='date_ExamDate_"+i+"']").val();
         var examplace   = $("[name='char_ExamPlace_"+i+"']").val();
         var licenseno   = $("[name='char_LicenseNo_"+i+"']").val();
         var licensedate = $("[name='date_LicenseReleasedDate_"+i+"']").val()
         var obj         = "sint_CareerServiceRefId_" + i;
         var val         = $("[name='sint_CareerServiceRefId_"+i+"']").val();
         val             = getName(val,"careerservice",obj);

         if (rating == "") rating = "N/A";
         if (examdate == "") examdate = "N/A";
         if (examplace == "") examplace = "N/A";
         if (licenseno == "") licenseno = "N/A";
         if (licensedate == "") licensedate = "N/A";

         row += '<tr>';
         row += '<td class="fieldValue--" id="val_'+obj+'"></td>';
         row += '<td class="fieldValue--">'+rating+'</td>';
         row += '<td class="fieldValue--">'+examdate+'</td>';
         row += '<td class="fieldValue--">'+examplace+'</td>';
         row += '<td class="fieldValue--">'+licenseno.toUpperCase()+'</td>';
         row += '<td class="fieldValue--">'+licensedate+'</td>';
         row += '</tr>';
      });
   row += '</tbody>';
   row += '</table>';
   row += '</div>';
   $("#eligibilitycontent").html(row);
   //END OF ELIGIBILITY

   //START OF WORK EXPERIENCE
   i = 0;
   row = "";
   row += '<div class="row padd5">';
   row += '<table style="width:100%;border-collapse:collapse;" border=1>';
   row += '<thead>';
   row += '<tr>';
   row += '<th class="fieldValueHDR--"> INCLUSIVE DATE FROM</th>';
   row += '<th class="fieldValueHDR--"> INCLUSIVE DATE TO</th>';
   row += '<th class="fieldValueHDR--"> POSITION TITLE</th>';
   row += '<th class="fieldValueHDR--"> AGENCY</th>';
   row += '<th class="fieldValueHDR--"> MONTHLY SALARY</th>';
   row += '<th class="fieldValueHDR--"> SALARY GRADE</th>';
   row += '<th class="fieldValueHDR--"> JOB GRADE</th>';
   row += '<th class="fieldValueHDR--"> STEP INC.</th>';
   row += '<th class="fieldValueHDR--"> EMPLOYMENT STATUS</th>';
   row += '<th class="fieldValueHDR--"> IS GOV\'T SERVICE</th>';
   row += '</tr>';
   row += '</thead>';
   row += '<tbody>';
      $("[id*='EntryWorkExp_']").each(function () {
         i++;
         var datefrom = $("[name='date_WorkStartDate_"+i+"']").val();
         var dateto = $("[name='date_WorkEndDate_"+i+"']").val();
         var salary = $("[name='deci_SalaryAmount_"+i+"']").val();
         var isgovtservice = $("[name='sint_isGovtService_"+i+"']").val();

         var pos_val      = $("[name='sint_PositionRefId_"+i+"']").val();
         var agency_val   = $("[name='sint_AgencyRefId_"+i+"']").val();
         var salgrade_val = $("[name='sint_SalaryGradeRefId_"+i+"']").val();
         var job_val      = $("[name='sint_JobGradeRefId_"+i+"']").val();
         var empstat_val  = $("[name='sint_EmpStatusRefId_"+i+"']").val();
         var stepinc_val  = $("[name='sint_StepIncrementRefId_"+i+"']").val();

         //alert(stepinc_val);
         var pos_obj      = "sint_PositionRefId_"+i;
         var agency_obj   = "sint_AgencyRefId_"+i;
         var salgrade_obj = "sint_SalaryGradeRefId_"+i;
         var job_obj      = "sint_JobGradeRefId_"+i;
         var empstat_obj  = "sint_EmpStatusRefId_"+i;
         var stepinc_obj  = "sint_StepIncrementRefId_"+i;

         pos_val          = getName(pos_val,"position",pos_obj);
         agency_val       = getName(agency_val,"agency",agency_obj);
         salgrade_val     = getName(salgrade_val,"salarygrade",salgrade_obj);
         job_val          = getName(job_val,"jobgrade",job_obj);
         empstat_val      = getName(empstat_val,"empstatus",empstat_obj);
         stepinc_val      = getName(stepinc_val,"stepincrement",stepinc_obj);

         if (datefrom == "") datefrom = "N/A";
         if (dateto == "") dateto = "N/A";
         if (salary == "") salary = "N/A";
         if (isgovtservice == "") {
            isgovtservice = "N/A";
         } else if (isgovtservice == 0){
            isgovtservice = "NO";
         } else {
            isgovtservice = "YES";
         }
         row += '<tr>';
         row += '<td class="fieldValue--">'+datefrom+'</td>';
         row += '<td class="fieldValue--">'+dateto+'</td>';
         row += '<td class="fieldValue--" id="val_'+pos_obj+'">'+pos_val+'</td>';
         row += '<td class="fieldValue--" id="val_'+agency_obj+'">'+agency_val+'</td>';
         row += '<td class="fieldValue--">'+salary+'</td>';
         row += '<td class="fieldValue--" id="val_'+salgrade_obj+'">'+salgrade_val+'</td>';
         row += '<td class="fieldValue--" id="val_'+job_obj+'">'+job_val+'</td>';
         row += '<td class="fieldValue--" id="val_'+stepinc_obj+'">'+stepinc_val+'yyy</td>';
         row += '<td class="fieldValue--" id="val_'+empstat_obj+'">'+empstat_val+'xxx</td>';
         row += '<td class="fieldValue--">'+isgovtservice+'</td>';
         row += '</tr>';
      });
   row += '</tbody>';
   row += '</table';
   row += '</div>';
   $("#workexpcontent").html(row);
   //END OF WORK EXPERIENCE


   //START OF VOLUNTARY WORK
   i = 0;
   row = "";
   row += '<div class="row padd5">';
   row += '<table style="width:100%;border-collapse:collapse;" border=1>';
   row += '<thead>';
   row += '<tr>';
   row += '<th class="fieldValueHDR--" style="width:20%">ORGANIZATION</th>';
   row += '<th class="fieldValueHDR--" style="width:15%">INCLUSIVE DATE FROM</th>';
   row += '<th class="fieldValueHDR--" style="width:15%">INCLUSIVE DATE TO</th>';
   row += '<th class="fieldValueHDR--" style="width:15%">NUMBER OF HOURS</th>';
   row += '<th class="fieldValueHDR--" style="width:35%">POSITION NATURE OF WORK</th>';
   row += '</tr>';
   row += '</thead>';
   row += '<tbody>';
      $("[id*='EntryVolWork_']").each(function () {
         i++;
         var org_val    = $("[name='bint_OrganizationRefId_"+i+"']").val();
         var org_obj    = "bint_OrganizationRefId_"+i;
         var org_val    = getName(org_val,"organization",org_obj);
         var startdate  = $("[name='date_StartDate_"+i+"']").val();
         var enddate    = $("[name='date_EndDate_"+i+"']").val();
         var numofhrs   = $("[name='sint_NumofHrs_"+i+"']").val();
         var worknature = $("[name='char_WorksNature_"+i+"']").val();

         if (startdate == "") startdate = "N/A";
         if (enddate == "") enddate = "N/A";
         if (numofhrs == "") numofhrs = "N/A";
         if (worknature == "") worknature = "N/A";
         row += '<tr>';
         row += '<td class="fieldValue--" id="val_'+org_obj+'">'+org_val+'</td>';
         row += '<td class="fieldValue--">'+startdate+'</td>';
         row += '<td class="fieldValue--">'+enddate+'</td>';
         row += '<td class="fieldValue--">'+numofhrs+'</td>';
         row += '<td class="fieldValue--">'+worknature+'</td>';
         row += '</tr>';
      });
   row += '</tbody>';
   row += '</table';
   row += '</div>';
   $("#voluntarycontent").html(row);
   //END OF VOLUNTARY WORK


   //START OF TRAINING PROGRAMS
   i = 0;
   row = "";
   row += '<div class="row padd5">';
   row += '<table style="width:100%;border-collapse:collapse;" border=1>';
   row += '<thead>';
   row += '<tr>';
   row += '<th class="fieldValueHDR--">TITLE OF SEMINAR/CONFERENCE/WORKSHOP/SHORTCOURSES</th>';
   row += '<th class="fieldValueHDR--">INCLUSIVE DATE FROM</th>';
   row += '<th class="fieldValueHDR--">INCLUSIVE DATE TO</th>';
   row += '<th class="fieldValueHDR--">NUMBER OF HOURS</th>';
   row += '<th class="fieldValueHDR--">CONDUCTED/SPONSORED BY</th>';
   if ($("#hCompanyID").val() > 0) {
      row += '<th class="fieldValueHDR--">SEMINAR PLACE</th>';
   }
   row += '<th class="fieldValueHDR--">SEMINAR TYPE</th>';
   row += '</tr>';
   row += '</thead>';
   row += '<tbody>';
      $("[id*='EntryTrainingProg_']").each(function () {
         i++;
         var seminar_val  = $("[name='bint_SeminarsRefId_"+i+"']").val();
         var seminar_obj  = "bint_SeminarsRefId_"+i;
         var semclass_val = $("[name='sint_SeminarClassRefId_"+i+"']").val();
         var semclass_obj = "sint_SeminarClassRefId_"+i;
         var semtype_val  = $("[name='bint_SeminarTypeRefId_"+i+"']").val();
         var semtype_obj  = "bint_SeminarTypeRefId_"+i;
         var sponsor_val  = $("[name='bint_SponsorRefId_"+i+"']").val();
         var sponsor_obj  = "bint_SponsorRefId_"+i;
         var startdate    = $("[name='date_StartDate_"+i+"_TrainProg']").val();
         var endate       = $("[name='date_EndDate_"+i+"_TrainProg']").val();
         var numofhrs     = $("[name='sint_NumofHrs_"+i+"_TrainProg']").val();
         var desc         = $("[name='char_Description_"+i+"']").val();
         var semplace     = $("[name='char_SeminarPlace_"+i+"']").val();
         seminar_val      = getName(seminar_val,"seminars",seminar_obj);
         semclass_val     = getName(semclass_val,"seminarclass",semclass_obj);
         semtype_val      = getName(semtype_val,"seminartype",semtype_obj);
         sponsor_val      = getName(sponsor_val,"sponsor",sponsor_obj);

         if (startdate == "") startdate = "N/A";
         if (endate == "") endate = "N/A";
         if (numofhrs == "") numofhrs = "N/A";
         if (desc == "") desc = "N/A";

         row += '<tr>';
         row += '<td class="fieldValue--" id="val_'+seminar_obj+'">'+seminar_val+'</td>';
         row += '<td class="fieldValue--">'+startdate+'</td>';
         row += '<td class="fieldValue--">'+endate+'</td>';
         row += '<td class="fieldValue--">'+numofhrs+'</td>';
         row += '<td class="fieldValue--" id="val_'+sponsor_obj+'">'+sponsor_val+'</td>';
         if ($("#hCompanyID").val() != 2) {
            row += '<td class="fieldValue--">'+semplace+'</td>';
         }
         row += '<td class="fieldValue--" id="val_'+semtype_obj+'">'+semtype_val+'</td>';
         row += '</tr>';

         row += '<tr>';
         row += '<td class="fieldValue--" id="val_'+semclass_obj+'">'+semclass_val+'</td>';
         row += '<td class="fieldValueHDR--">DESCRIPTION</td>';
         row += '<td class="fieldValue--" colspan=5>'+desc+'</td>';
         row += '</tr>';

         row += '<tr>';
         row += '<td colspan=7 style="background-color: #adbbcc;">&nbsp;</td>';
         row += '</tr>';


      });
   row += '</tbody>';
   row += '</table';
   row += '</div>';
   $("#trainingcontent").html(row);
   //END OF TRAINING PROGRAMS

   //START OF OTHER INFO
   i = 0;
   row = "";
   row += '<div class="row padd5">';
   row += '<table style="width:100%;border-collapse:collapse;" border=1>';
   row += '<thead>';
   row += '<tr>';
   row += '<th class="fieldValueHDR--">SKILLS/HOBBIES</th>';
   row += '<th class="fieldValueHDR--">NON-ACADEMIC DISTINCTION/RECOGNITION</th>';
   row += '<th class="fieldValueHDR--">MEMBERSHIP IN ASSOCIATION/ORGANIZATION</th>';
   row += '</tr>';
   row += '</thead>';
   row += '<tbody>';
      $("[id*='EntryOtherInfo_']").each(function () {
         i++;
         var skills      = $("[name='char_Skills_"+i+"']").val();
         var recognition = $("[name='char_Recognition_"+i+"']").val();
         var affiliates  = $("[name='char_Affiliates_"+i+"']").val();
         if (skills == "") skills = "N/A";
         if (recognition == "") recognition = "N/A";
         if (affiliates == "") affiliates = "N/A";
         row += '<tr>';
         row += '<td class="fieldValue--">'+skills+'</td>';
         row += '<td class="fieldValue--">'+recognition+'</td>';
         row += '<td class="fieldValue--">'+affiliates+'</td>';
         row += '</tr>';
      });
   row += '</tbody>';
   row += '</table';
   row += '</div>';
   $("#otherinfocontent").html(row);
   //END OF OTHER INFO


   //START OF REFERENCE
   i = 0;
   row = "";
   row += '<div class="row padd5">';
   row += '<table style="width:100%;border-collapse:collapse;" border=1>';
   row += '<thead>';
   row += '<tr>';
   row += '<th class="fieldValueHDR--">NAME</th>';
   row += '<th class="fieldValueHDR--">ADDRESS</th>';
   row += '<th class="fieldValueHDR--">CONTACT NUMBER</th>';
   row += '</tr>';
   row += '</thead>';
   row += '<tbody>';
      $("[id*='EntryRef_']").each(function () {
         i++;
         var name     = $("[name='char_Name_"+i+"']").val();
         var address  = $("[name='char_Address_"+i+"']").val();
         var contact  = $("[name='char_ContactNo_"+i+"']").val();
         if (name == "") name = "N/A";
         if (address == "") address = "N/A";
         if (contact == "") contact = "N/A";
         row += '<tr>';
         row += '<td class="fieldValue--" style="text-transform: uppercase;">'+name+'</td>';
         row += '<td class="fieldValue--" style="text-transform: uppercase;">'+address+'</td>';
         row += '<td class="fieldValue--">'+contact+'</td>';
         row += '</tr>';
      });
   row += '</tbody>';
   row += '</table';
   row += '</div>';
   $("#refcontent").html(row);
   //END OF REFERENCE
   $("#filereview").modal();
}

function getName(refid,tbl,objname){
   $.get("trn.e2e.php",
   {
      fn:"getName",
      refid:refid,
      tbl:tbl,
      objname:objname
   },
   function(data,status){
      if (status == 'success') {
         eval(data);
      } else {
         console.log(status + " - " + data);
      }
   });
}
function getZipCode(cityrefid,objcontent) {
   $.get("trn.e2e.php",
   {
      fn:"getZipCode",
      cityrefid:cityrefid,
      objcontent:objcontent
   },
   function(data,status){
      if (status == 'success') {
         eval(data);
      }
   });
}
function HighestGrade_validation(objname) {
   $("[name='"+objname+"']").keyup(function () {
      var idx = $("[name='"+objname+"']").attr("name").split("_")[3];
      var levelType = $("[name='"+objname+"']").attr("name").split("_")[2];
      $("[name='sint_YearGraduated_"+levelType+"_"+idx+"']").prop("disabled",true);
      $("[name='sint_YearGraduated_"+levelType+"_"+idx+"']").val(0);
      if ($("[name='"+objname+"']").val() == "") {
         $("[name='sint_YearGraduated_"+levelType+"_"+idx+"']").prop("disabled",false);
      }
   });
}

function completion_sent(){
   $.post("trn.e2e.php",
   {
      fn:"completion",
      emprefid:$("#hEmpRefId").val()
   },
   function(data,status){
      if (status == 'success') {
         data = data.trim();
         if (data == 1) {
            alert("PDS Complete.");
            gotoscrn("scrn201File","");
         } else {
            alert(data);
         }
      }
   });
}