$(document).ready(function() {
    t = 500;
    var cid = $("#hCompanyID").val();
    $("#PIS_menu").hide();
    $("#AMS_menu").hide();
    $("#PMS_menu").hide();
    $("#SPMS_menu").hide();
    $("#myrequestView").hide();
    $("#PIS_Header").click(function () {
        $("#PIS_menu").toggle();
        $("#AMS_menu").hide();
        $("#PMS_menu").hide();
        $("#SPMS_menu").hide();
        $("#myrequestView").hide();
    });
    $("#AMS_Header").click(function () {
        $("#AMS_menu").toggle();
        $("#PIS_menu").hide();
        $("#PMS_menu").hide();
        $("#SPMS_menu").hide();
        $("#myrequestView").hide();
    });
    $("#PMS_Header").click(function () {
        $("#PMS_menu").toggle();
        $("#AMS_menu").hide();
        $("#PIS_menu").hide();
        $("#SPMS_menu").hide();
        $("#myrequestView").hide();
    });
    $("#SPMS_Header").click(function () {
        $("#SPMS_menu").toggle();
        $("#AMS_menu").hide();
        $("#PIS_menu").hide();
        $("#PMS_menu").hide();
        $("#myrequestView").hide();
    });
    $("#myrequest").click(function () {
        $("#myrequestView").toggle();
        $("#AMS_menu").hide();
        $("#PIS_menu").hide();
        $("#PMS_menu").hide();
        $("#SPMS_menu").hide();
    });
    $("#btnChangePW").click(function () {
       $("#panelChangePW").slideDown(t);
       $("#panelReminders, #panelRequest").slideUp(t);
       $(this).prop("disabled",true);
       $("#newToken, #currenToken, #reToken").val("");
    });
    $("#btnChangeCancel").click(function () {
       $("#panelChangePW").slideUp(t);
       $("#panelReminders, #panelRequest").slideDown(t);
       $("#btnChangePW").prop("disabled",false);
    });
    $("#currentToken").click(function () {
       $("[for=\'cuPW\']").hide(100)
    });
    $("[id*='cancel_']").each(function () {
        $(this).click(function () {
            var refid = $(this).attr("id").split("_")[1];
            if (confirm("Do you want to cancel this request?")) {
                cancelRequest(refid);
            }
        });
    });
    $("[class*='govt_logo']").each(function () {
        $(this).click(function () {
            var value = $(this).attr("id");
            var url = "";
            switch (value) {
                case "bir":
                    url = "//www.bir.gov.ph";
                    break;
                case "gsis":
                    url = "//www.gsis.gov.ph";
                    break;
                case "phic":
                    url = "//www.philhealth.gov.ph";
                    break;
                case "pagibig":
                    url = "//www.pagibigfund.gov.ph";
                    break;
                case "sss":
                    url = "//www.sss.gov.ph";
                    break;
            }
            window.open(url,"_blank");
        });
    });
    var module_selected = $("#hModule").val();
    if (cid == 28 || cid == 21) {
        switch (module_selected) {
            case "pis":
                gotoscrn ("scrnDashboard","&paramTitle=DASHBOARD");
                break;
            case "ams":
                gotoscrn ("amsDashboard","&paramTitle=DASHBOARD");
                break;
            case "ldms":
                gotoscrn ("ldmsIndividualDevtPlan","&paramTitle=INDIVIDUAL DEVELOPMENT PLAN");
                break;
            case "spms":
                gotoscrn ("spmsDashboard", "&paramTitle=DASHBOARD");
        }
    } else {
        switch (module_selected) {
            case "pis":
                gotoscrn ("scrnMasterFile","&paramTitle=MASTERFILE");
                break;
            case "ams":
                if ($("#hUserGroup").val() == "AA" && cid == 35) {
                  gotoscrn ("amsDashboard2","&paramTitle=DASHBOARD");
                } else {
                  gotoscrn ("amsTrnDTR","&paramTitle=Daily Time Record");  
                }
                
                break;
            case "ldms":
                gotoscrn ("ldmsIndividualDevtPlan","&paramTitle=INDIVIDUAL DEVELOPMENT PLAN");
                break;
            case "spms":
                gotoscrn ("spmsDashboard", "&paramTitle=DASHBOARD");
        }  
    }
    
    
});
function cancelRequest(refid){
    $.get("trn.e2e.php",
    {
       fn:"cancelRequest",
       refid:refid,
    },
    function(data,status) {
       if (status == "success") {
          try {
             eval(data);
             $("#myrequestView").show();
          } catch (e) {
             if (e instanceof SyntaxError) {
                alert(e.message);
             }
          }
       }
    });
 }

function showEmp(fld,rptName,isDocType) {
    $("#prnModal").modal();
    $("#rptContent").attr("src","blank.e2e.php");
    var rptFile = $("#hRptFile").val();
    var url = "ReportCaller.e2e.php?file=" + rptFile + "&";
    url += "&fld=" + fld;
    url += "&rptName=" + rptName;
    url += "&isdoctype=" + isDocType;
    url += "&hgParam=" + $("#hgParam").val();
    $("#prnModal").modal();
    $("#rptContent").attr("src",url);
}
function clickRpt(file,where){
  $("#rptContent").attr("src","blank.htm");
  var rptFile = file;
  var url = "ReportCaller.e2e.php?file=" + rptFile;
  url += "&where=" + where;
  url += "&" + $("[name='hgParam']").val();
  $("#prnModal").modal();
  $("#rptContent").attr("src",url);
}
function printAvail(refid,file){
  $("#rptContent").attr("src","blank.htm");
  var rptFile = file;
  var url = "ReportCaller.e2e.php?file=" + rptFile;
  url += "&refid=" + refid;
  url += "&" + $("[name='hgParam']").val();
  $("#prnModal").modal();
  $("#rptContent").attr("src",url);
}