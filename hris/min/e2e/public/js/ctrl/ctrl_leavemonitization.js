$(document).ready(function () {
   $("#selected_emp_canvas").hide();
   $("#fifty_monetization").hide();
   $(".chkvalue--").each(function () {
      $(this).blur(function () {
         var val = $(this).val();
         var bal = $("[name='"+$(this).attr("leave")+"']").val();
         if (bal < val) {
            $.notify("No Enough Earnings");
            $(this).val("");
            return false;
         }
         var salary = $("#hSalary").val();
         var value = $(this).attr("for");
         //compute();
      });
   });
   $("#sint_IsHalf").change(function () {
      var value = $(this).val();
      if (value == 0) {
         $("#char_Remarks").val("");
         $("#regular_monetization").show();
         $("#fifty_monetization").hide();
      } else {
         $("#deci_VLValue, #deci_SLValue, #deci_VLAmount, #deci_SLAmount").val("");
         $("#regular_monetization").hide();
         $("#fifty_monetization").show();
         
      }
   });
   $("#deci_VLValue").change(function () {
      var value = $(this).val();
      var salary = $("#hSalary").val();
      if (value != "") {
         compute(value,"deci_VLAmount",salary);      
      }
   });
   $("#deci_SLValue").change(function () {
      var value = $(this).val();
      var salary = $("#hSalary").val();
      if (value != "") {
         compute(value,"deci_SLAmount",salary);      
      }
   });
});
/*function viewInfo(refid,mode,dum){
   if (mode == 3) {
      $("#rptContent").attr("src","blank.htm");
      var rptFile = "rpt_Leave_Availment_Monetization";
      var url = "ReportCaller.e2e.php?file=" + rptFile;
      url += "&refid=" + refid;
      url += "&" + $("[name='hgParam']").val();
      $("#prnModal").modal();
      $("#rptContent").attr("src",url);
   }
}*/
function compute(value,obj,salary){
   $.get("trn.e2e.php",
   {
      fn:"ComputeLeaveAmount",
      objname_value:value,
      objname_amount:obj,
      salary:salary
      /*params:{
         objname_value:$(this).val(),
         objname_amount:$(this).attr("for"),
         tokens:"none"
      }*/
   },
   function(data,status) {
      if(status == "error") return false;
      if(status == "success"){
         try {
            eval(data);
         }
         catch (e)
         {
            if (e instanceof SyntaxError) {
                alert(e.message);
            }
         }
      }
   });
}
function selectMe(emprefid) {
   $('[name="sint_EmployeesRefId"]').val(emprefid);
   $.get(
      "EmpQuery.e2e.php",
      {
         emprefid: emprefid,
         hCompanyID: $("#hCompanyID").val(),
         hBranchID: $("#hBranchID").val(),
         hEmpRefId: $("#hEmpRefId").val(),
         hUserRefId: $("#hUserRefId").val()
      },
      function (data, status) {
         setValueById("hSalary",0);
         setValueById("hRefIdSelected", 0); /* important */
         setHTMLById("lblRefIdSelected", "&nbsp;");
         setHTMLById("lblEmpLastName", "&nbsp;");
         setHTMLById("lblEmpFirstName", "&nbsp;");
         setHTMLById("lblEmpMiddleName", "&nbsp;");
         setHTMLById("lblEmpPosition", "&nbsp;");
         if (status == "error") {
            $.notify("System Encounter Error !!! fn(selectMe)", "warn");
            return false;
         }
         if (status == "success") {
            if (data != "") {
               var data = JSON.parse(data);
               try {
                  setValueById("hRefIdSelected", data.RefId);
                  setHTMLById("lblRefIdSelected", data.RefId);
                  setHTMLById("lblEmpLastName", data.LastName);
                  setHTMLById("lblEmpFirstName", data.FirstName);
                  setHTMLById("lblEmpMiddleName", data.MiddleName);
                  setHTMLById("lblEmpPosition", data.PositionRefId);
                  setValueById("hSalary",data.SalaryAmount);
                  setValueById("agencyid",data.AgencyId);
                  getCreditBalance(data.RefId,data.CompanyRefId,data.BranchRefId);
                  getSalary(data.RefId,"hSalary",data.CompanyRefId,data.BranchRefId);
               }
               catch (e) {
                  if (e instanceof SyntaxError) {
                     alert(e.message);
                  }
               }
            } 
         }
      }
   );
}
function getCreditBalance(emprefid,CompanyId,BranchId) {
   $.get("trn.e2e.php",
   {
      fn:"getCreditBalance",
      emprefid:emprefid,
      CompanyId:CompanyId,
      BranchId:BranchId,
      vlobj:"deci_VLBalance",
      slobj:"deci_SLBalance"

   },
   function(data,status){
      if (status == 'success') {
         eval(data);
      } else {
         console.log(status + " - " + data);
      }
   });
}
function getSalary(emprefid,obj,CompanyId,BranchId) {
   $.get("trn.e2e.php",
   {
      fn:"getSalary",
      emprefid:emprefid,
      CompanyId:CompanyId,
      BranchId:BranchId,
      obj:obj

   },
   function(data,status){
      if (status == 'success') {
         eval(data);
      } else {
         console.log(status + " - " + data);
      }
   });
}
function afterDelete() {
   alert("Successfully Deleted");
   gotoscrn($("#hProg").val(),"");
}