$(document).ready(function() {
   /*$(function () {
      remIconDL();
   });*/
   $("#btnSaveRecord").click(function () {
      delCookie("cookies_Object_Value");
      var objnval = ""
      var fldnval_add = "";
      if (parseInt($("#hEmployeesRefId").val()) > 0) {
         erefid = $("#hEmployeesRefId").val();

         /* New Record (Tabs) */
         objnval = getFieldEntry("entryAddNewEntry","ADD");
         objnval += "hidden|sint_EmployeesRefId|" + erefid + "!";
         objnval += "hidden|char_Remarks|System:Added From 201 Updates...!";

         /*Updates 201*/
         fldnval_add += "hidden|sint_EmployeesRefId|"+erefid+"!";
         fldnval_add += "hidden|char_TableName|"+$("#201Tabs").val()+"!";
         fldnval_add += "hidden|char_TabsName|"+$("#201Tabs").val()+"!";
         fldnval_add += "hidden|char_FieldsEdit|ALL NEW!";
         fldnval_add += "hidden|char_OldValue|!";
         fldnval_add += "hidden|char_NewValue|!";

         var dbtable = $("#dummyTable").val();
         var Proceed = 0;
         var criteria = " AND EmployeesRefId = " + erefid;
         switch (dbtable) {
            case "employeeschild":
               var chldname = $("[name='char_FullName']").val();
               var chldbdate = $("[name='date_BirthDate']").val();
               if (chldname == "" || chldbdate == "") {
                  alert("Blank Entry is not allowed");
                  return false;
               } else {
                  criteria += " AND FullName = '"+ chldname +"' AND BirthDate = '"+chldbdate+"'";
               }
            break;
            /*case "employeeselegibility":
               var carService = $("[name='sint_CareerServiceRefId']").val();
               var examDate = $("[name='date_ExamDate']").val();
               var licenseNo =  $("[name='char_LicenseNo']").val();
               if (carService == 0) {
                  alert("Career Service is Required");
                  return false;
               } else {
                  criteria += " AND CareerServiceRefId = " + carService + " AND ExamDate = '" + examDate + "' AND LicenseNo = '"  + licenseNo + "'";
               }
            break;*/
            case "employeeselegibility":
               var carService = $("[name='sint_CareerServiceRefId']").val();
               var LicenseDate = $("[name='date_LicenseReleasedDate']").val();
               var licenseNo =  $("[name='char_LicenseNo']").val();
               if (carService == 0 || LicenseDate == "" || licenseNo == "") {
                  alert("Blank Entry is not allowed");
                  return false;
               } else {
                  criteria += " AND CareerServiceRefId = " + carService + " AND LicenseReleasedDate = '" + LicenseDate + "' AND LicenseNo = '"  + licenseNo + "'";
               }
            break;
            case "employeesvoluntary":
               var organization = $("[name='sint_OrganizationRefId']").val();
               var startDate = $("[name='date_StartDate']").val();
               var endDate = $("[name='date_EndDate']").val();
               var worknature = $("[name='char_WorksNature']").val();
               if (organization == "" || startDate == "" || endDate == "" || worknature == "") {
                  alert("Blank Entry is not allowed");
                  return false;
               } else {
                  criteria += " AND OrganizationRefId = '" + organization + "' AND StartDate ='" + startDate + "'";
                  criteria += " AND EndDate = '" + endDate + "' AND WorksNature = '" + worknature + "'";
               }
            break;
            case "employeesworkexperience":
               var agency = $("[name='sint_AgencyRefId']").val();
               var workstartdate = $("[name='date_WorkStartDate']").val();
               if (agency == 0) {
                  alert("Agency is Required");
                  $("[name='sint_AgencyRefId']").focus();
                  return false;
               } else {
                  criteria += " AND AgencyRefId = '" + agency + "' AND WorkStartDate = '" + workstartdate + "'";
               }
            break;
            case "employeestraining":
               var seminar = $("[name='sint_SeminarsRefId']").val();
               var startDate = $("[name='date_StartDate']").val();
               if (seminar == 0) {
                  alert("Seminar is Required");
                  $("[name='sint_SeminarsRefId']").focus();
                  return false;
               } else {
                  criteria += " AND SeminarsRefId = '" + seminar + "' AND StartDate = '" + startDate + "' ";
               }
            break;
            case "employeesotherinfo":
               var skills = $("[name='char_Skills']").val();
               var recog = $("[name='char_Recognition']").val();
               var affiliates = $("[name='char_Affiliates']").val();
               if (skills == "" || recog == "" || affiliates == "") {
                  alert("Blank Entry is not allowed");
                  return false;
               } else {
                  criteria += " AND Skills = '" + skills + "' AND Recognition = '" + recog + "' AND Affiliates = '" + affiliates + "'";
               }
            break;
            case "employeesreference":
               var name = $("[name='char_Name']").val();
               var address = $("[name='char_Address']").val();
               var contact = $("[name='char_ContactNo']").val();
               if (name == "" || address == "" || contact == "") {
                  alert("Blank Entry is not allowed");
                  return false;
               } else {
                  criteria += " AND Name = '" + name + "' AND Address = '" + address + "' AND ContactNo = '" + contact + "'";
               }
            break;
         }
         chkRecord(dbtable,criteria,fldnval_add,objnval);
      } else {
         alert("No Emp. RefId Assigned");
      }
   });
   $("#btnAddNewRecord").click(function() {
      var url = "ctrl_201Updates.e2e.php?";
      url += "task=AddNewEntry";
      url += "&dbtable="+$("#201Tabs").val();
      url += "&empRefId="+$("#hEmployeesRefId").val();
      url += "&hCompanyID="+$("#hCompanyID").val();
      url += "&hBranchID="+$("#hBranchID").val();
      url += "&user=" + $("#hUser").val();
      $("#addNewEntry").html("");
      $("#addNewEntry").load(url, function(responseTxt, statusTxt, xhr){
         if(statusTxt == "error") {
            alert("Ooops Error: " + xhr.status + " : " + xhr.statusText);
            return false;
         }
         if(statusTxt == "success") {
            $("#dbtbl").html($("#201Tabs").val());
            $("#modalAddNew").modal();
            $(".focus--").focus();

         }
      });
   });
   $("#chkAll").click(function() {
      var isChk = $(this).is(":checked");
      $(".fldname--").prop("checked",isChk);
      $(".saveFields--").prop("disabled",!isChk);
      $("[id*='btnSAVE_']").prop("disabled",!isChk);
      $("#201Tabs").prop("disabled",false);

   });

   $(".fldname--").click(function() {
      var objid = $(this).attr("id");
      var isChk = $(this).is(":checked");
      $("[name='btn_"+objid+"']").prop("disabled",!isChk);
      $("[name='NewValue_"+objid+"']").prop("disabled",!isChk);
      if ($(this).is(":checked") == false) {
         $("[name='NewValue_" + objid + "']").val($("#CurrentValue_" + objid).val());
      }
   });
   /*----SAVINGS OF 201 UPDATES----*/
   $("[id*='btnSAVE_']").click(function() {
      var objname = $(this).attr("name");
      var idx = objname.split("_")[1];
      var emprefid = $("#hEmployeesRefId").val();
      var newValue = "";
      if ($(this).attr("id") == "btnSAVE_" + idx) {
         newValue = $("#NewValue_"+idx).val();
         /*if (newValue == "") {
            alert("Ooppss No Value Assigned.");
            $("#NewValue_"+idx).focus();
            return false;
         }*/
         if ($("#NewValue_"+idx).hasClass("date--") && $("#NewValue_"+idx).val() == "") {
            if (confirm("Modifying with date Class, Blank Value is not allowed, continuing this transaction date will be 1901-01-01")) {
               newValue = "1901-01-01";
            } else {
               return false;
            }
         }
         if ($("#NewValue_"+idx).hasClass("number--") && $("#NewValue_"+idx).val() == "") {
            if (confirm("Modifying with number Class, Blank Value is not allowed, continuing this transaction value will be 0")) {
               newValue = "0";
            } else {
               return false;
            }
         }

         $("#hCurrentIdx").val(idx);
         if ((emprefid!="" && emprefid>0) && $("#hTable").val() != "") {
            var fldnval_add = "";
            var fldnval_edit = "";
            fldnval_add += "hidden|sint_EmployeesRefId|"+emprefid+"!";
            fldnval_add += "hidden|char_TableName|"+$("#201Tabs").val()+"!";
            fldnval_add += "hidden|char_TabsName|"+$("#201Tabs option:selected").text()+"!";
            fldnval_add += "hidden|char_OldValue|"+$("#CurrentValue_"+idx).val()+"!";
            fldnval_add += "hidden|char_NewValue|"+$("#NewValue_"+idx).val()+"!";
            fldnval_add += "hidden|char_FieldsEdit|"+$("#FldName_"+idx).val()+"!";
            /*alert($("#hTable").val());
            alert(fldnval_add);
            return false;*/
            $("[id*='NewValue_"+idx+"']").prop("disabled",true);
            $("[name*='chk_"+idx+"']").prop("checked",false);
            $(this).prop("disabled",true);
            $.post("trn.e2e.php",
            {
               fn:"Save201Updates",
               dbtable:$("#dummyTable").val(),
               dbfield:$("#FldName_"+idx).val(),
               newval:newValue,
               refid:$("#RefId_"+idx).val(),
               user:$("#hUser").val(),
               idx:idx
            },
            function(data,status) {
               if (status == "success") {
                  try {
                     //alert(data);
                     eval(data);
                     $("[name='btn_"+idx+"'], #"+idx).prop("disabled",false);
                     gSaveRecord(fldnval_add,$("#hTable").val());
                  } catch (e) {
                     if (e instanceof SyntaxError) {
                        alert(e.message);
                     }
                  }
               }
            });
            // if (getValueByName("hUserGroup") == "COMPEMP") {
            //    gSaveRecord(fldnval_add,$("#hTable").val());
            // } else {
            //    $.post("trn.e2e.php",
            //    {
            //       fn:"Save201Updates",
            //       dbtable:$("#dummyTable").val(),
            //       dbfield:$("#FldName_"+idx).val(),
            //       newval:newValue,
            //       refid:$("#RefId_"+idx).val(),
            //       user:$("#hUser").val(),
            //       idx:idx
            //    },
            //    function(data,status) {
            //       if (status == "success") {
            //          try {
            //             //alert(data);
            //             eval(data);
            //             $("[name='btn_"+idx+"'], #"+idx).prop("disabled",false);
            //             gSaveRecord(fldnval_add,$("#hTable").val());
            //          } catch (e) {
            //             if (e instanceof SyntaxError) {
            //                alert(e.message);
            //             }
            //          }
            //       }
            //    });
            // }
            


         } else {
            alert("No Employees/Table Selected");
         }
      }
   });
   /*----*/
});
function chkRecord(dbtable,criteria,fldnval_add,objnval) {
   /*alert(dbtable);
   alert(criteria);
   return false;*/
   $.get("trn.e2e.php",
   {
      fn:"chkRecord",
      criteria:criteria,
      dbtable:dbtable,
      hCompanyID:$("#hCompanyID").val(),
      hBranchID:$("#hBranchID").val(),
      hEmpRefId:$("#hEmpRefId").val(),
      hUserRefId:$("#hUserRefId").val()
   },
   function (data,status) {
      if (status == "success") {
         try {
            Proceed = data;
            if (parseInt(Proceed) == 0) {
               //alert (fldnval_add);
               $.post("ctrl_201Updates.e2e.php",
               {
                  task:"SaveAllNew201Updates",
                  fv:fldnval_add,
                  ov:objnval,
                  table2:dbtable,
                  refid:0,
               },
               function(data,status) {
                  if (status == "success") {
                     try {
                        $("#modalAddNew").modal("hide");
                        eval(data);
                     } catch (e) {
                        if (e instanceof SyntaxError) {
                           alert(e.message);
                        }
                     }
                  }
               });
            } else {
               alert ("The Record is Already Exist!!!");
               //$("#modalAddNew").modal("hide");
               return false;
            }
         } catch (e) {
            if (e instanceof SyntaxError) {
               alert(e.message);
            }
         }
      }
   });
}

function afterNewSave(refid){
   // if (getValueByName("hUserGroup") == "COMPEMP") {
   //    alert("Successfully Submitted!");   
   // }
}