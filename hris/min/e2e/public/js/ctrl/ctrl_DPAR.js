$(document).ready (function () {
   var selected_emp = $("#SelectedEMP").val();
   if (selected_emp != "") {
      selectMe(selected_emp);
   } else {
      if (getValueByName("hUserGroup") == "COMPEMP") {
         selectMe($("#hEmpRefId").val());
      }
   }
   $("#insert_spms, #btnEDITIPCR, #btnBACKIPCR").hide();
   $("#btnINSERTSPMS").click(function () {
      var emprefid = $("#sint_EmployeesRefId").val();
      if (emprefid == "" || emprefid == 0){
         $.notify("No Employee Selected.");
      } else {
         $("#spmsList").hide();
         $("#insert_spms").show();
      }
   });
   $("#btnCANCELIPCR, #btnBACKIPCR").click(function () {
      var prog = $("#hProg").val();
      gotoscrn(prog,"");
   });
   
   $("#btnPRINT").click (function () {
      var rptFile = "prnIPCR";
      var url = "ReportCaller.e2e.php?file=" + rptFile + "&";
      $("#prnModal").modal();
      $("#rptContent").attr("src",url);
   });
   $("[id*='delete_']").each(function () {
      $(this).click(function () {
         var type = $(this).attr("id").split("_")[1];
         var last = "";
         $("[id*='" + type + "row_']").each(function () {
            last = $(this).attr("id").split("_")[1];
         });
         $("#" + type + "row_" + last).remove();
      });
   });
   $("[type*='checkbox']").each(function () {
   	$(this).click(function () {
   		var id = $(this).attr("id");
	   	var idx = $(this).attr("id").split("_")[1];
	   	if ($(this).is(":checked")) {
	   		$(this).val(1);	
	   	} else {
	   		$(this).val(0);
	   	}
   	});
   });
   $("[id*='addrow_']").each(function () {
      $(this).click(function () {
         var type = $(this).attr("id").split("_")[1];
         var last = "";
         $("[id*='" + type + "row_']").each(function () {
            last = $(this).attr("id").split("_")[1];
         });
         var idx = parseInt(last) + 1;
         var str = "";
         str += '<tr id="'+type+'row_' + idx + '">';
         str += '<td class="td-input">';
         str += '<input type="hidden" name="'+type+'_refid_'+ idx +'" id="'+type+'_refid_'+ idx +'" value="0">';
         str += '<input type="text" class="form-input" name="'+type+'_objectives_'+ idx +'" id="'+type+'_objectives_'+ idx +'">';
         str += '</td>';
         str += '<td class="td-input">';
         str += '<input type="text" class="form-input" name="'+type+'_percent_'+ idx +'" id="'+type+'_percent_'+ idx +'">';
         str += '</td>';
         str += '<td class="td-input">';
         str += '<input type="text" class="form-input" name="'+type+'_si_'+ idx +'" id="'+type+'_si_'+ idx +'">';
         str += '</td>';
         str += '<td class="td-input">';
         str += '<input type="text" class="form-input" name="'+type+'_sitype_'+ idx +'" id="'+type+'_sitype_'+ idx +'">';
         str += '</td>';
         str += '<td class="td-input">';
         str += '<input type="text" class="form-input" name="'+type+'_accomplishment_'+ idx +'" id="'+type+'_accomplishment_'+ idx +'">';
         str += '</td>';
         str += '<td class="td-input">';
         str += '<input type="text" class="form-input" name="'+type+'_numerical_'+ idx +'" id="'+type+'_numerical_'+ idx +'">';
         str += '</td>';
         str += '<td class="td-input">';
         str += '<input type="text" class="form-input" name="'+type+'_adjectival_'+ idx +'" id="'+type+'_adjectival_'+ idx +'">';
         str += '</td>';
         str += '<td class="td-input">';
         str += '<input type="text" class="form-input" name="'+type+'_remarks_'+ idx +'" id="'+type+'_remarks_'+ idx +'">';
         str += '</td>';
         str += '</tr>';
         $("#" + type + "row_" + last +":last").after(str);
         $("[type*='checkbox']").each(function () {
		   	$(this).click(function () {
		   		var id = $(this).attr("id");
			   	var idx = $(this).attr("id").split("_")[1];
			   	if ($(this).is(":checked")) {
			   		$(this).val(1);	
			   	} else {
			   		$(this).val(0);
			   	}
		   	});
		   });
      });
   });
   $("#btnSAVEIPCR, #btnEDITIPCR").click(function () {
      $(this).prop("disabled",true);
      $("#btnBACKIPCR, #btnCANCELIPCR").prop("disabled",true);
      $(this).html("Processing..");
   	var emprefid = $("#sint_EmployeesRefId").val();
   	if ($("#sint_Quarter").val() == "") {
   		$.notify("Quarter is Required");
   		return false;
   	}
   	if (emprefid == 0) {
   		$.notify("No Employee Selected");
   	} else {
   		var count_sp = 0;
	   	var count_cf = 0;
	   	var count_sf = 0;
	      $("[id*='sprow_']").each(function () {
	         count_sp = $(this).attr("id").split("_")[1];
	      });
	      $("[id*='cfrow_']").each(function () {
	         count_cf = $(this).attr("id").split("_")[1];
	      });
	      $("[id*='sfrow_']").each(function () {
	         count_sf = $(this).attr("id").split("_")[1];
	      });
	      $("#count_sp").val(count_sp);
	      $("#count_cf").val(count_cf);
	      $("#count_sf").val(count_sf);

	      $.ajax({
		      url: "ajax.e2e.php",
		      type: "POST",
		      data: new FormData($("[name='xForm']")[0]),
		      success: function (responseTxt) {
		         eval(responseTxt);
		      },
		      enctype: 'multipart/form-data',
		      processData: false,
		      contentType: false,
		      cache: false
		   });	
   	}
   });
});
function afterSave(){
	alert("Successfully Save");
	gotoscrn("spmsIPCR","");
}
function afterEdit() {
   alert("Successfully Update");
   gotoscrn("spmsIPCR","");   
}
function viewInfo(refid,mode,dum){
   if (mode == 2) {
      getIPAR(refid);   
   } else if (mode == 3) {
      printIPCR(refid);
   }
   
}
function selectedItems(emprefid,lname,fname) {
   $(".list-group-item").removeClass("active");
   $("#" + emprefid).addClass("active");
   $("[name='sint_EmployeesRefId']").val(emprefid);
   $.get("changeSessionValue.e2e.php",
   {
      hGridTblHdr:"Employee ID|Quarter|Year|Numerical Ave.|Adjectival Ave.",
      hGridTblFld:"AgencyId|Quarter|Year|AverageNumerical|AverageAdjectival",
      hGridTblId:"gridTable",
      hGridDBTable:"spms_ipar",
      sql:"SELECT * FROM `spms_ipar` WHERE `EmployeesRefId` = " + emprefid + " ORDER BY RefId Desc LIMIT 100",
      listAction:[true,true,true,""],
      empselected:emprefid
   },
   function(data,status){
      if (status=='success') {
         refreshTable(emprefid);
         tr_Click(emprefid);
      }
   });
}
function getIPAR(refid){
   $("#sint_IPARRefId").val(refid);
   $.get("ajax.e2e.php",
   {
      fn:"getIPAR",
      refid:refid
   },
   function(data,status){
      if (status=='success') {
         eval(data);
         $("[id*='delete_']").prop("disabled",true);
         $("#spmsList, #btnSAVEIPCR, #btnCANCELIPCR").hide();
         $("#insert_spms, #btnEDITIPCR, #btnBACKIPCR").show();
      }
   });
}
function refreshTable(emprefid) {
   $("#spGridTable").html("");
   $("#spGridTable").load("listRefresh.e2e.php",
   {
      EmployeesRefid : emprefid
   },
   function(responseTxt, statusTxt, xhr){
      if(statusTxt == "error")
         alert("Ooops Error: " + xhr.status + ": " + xhr.statusText);
         return false;
   });
}
function selectMe(emprefid) {
   $("#sint_EmployeesRefId").val(emprefid);
   $.get("EmpQuery.e2e.php",
   {
      emprefid:emprefid,
      hCompanyID:$("#hCompanyID").val(),
      hBranchID:$("#hBranchID").val(),
      hEmpRefId:$("#hEmpRefId").val(),
      hUserRefId:$("#hUserRefId").val()
   },
   function(data,status) {
      $('#selectedEmployees').html("&nbsp;");
      if(status == "error") return false;
      if(status == "success"){
         var data = JSON.parse(data);
         try
         {
            setHTMLById("RefIdSelected",data.RefId);
            setValueById("hRefIdSelected",data.RefId);
            setValueByName("txtEmpId",data.AgencyId);
            setHTMLById("selectedEmployees",data.LastName + ", " + data.FirstName + " " + data.MiddleName);
            selectedItems(data.RefId,
                          data.LastName,
                          data.FirstName);
         }
         catch (e)
         {
            if (e instanceof SyntaxError) {
                alert(e.message);
            }
         }
      }
   });
}

function printIPCR(refid){
   $("#rptContent").attr("src","blank.htm");
   var rptFile = "rpt_DPAR_28";
   var url = "ReportCaller.e2e.php?file=" + rptFile;
   url += "&refid=" + refid;
   url += "&" + $("[name='hgParam']").val();
   $("#prnModal").modal();
   $("#rptContent").attr("src",url);
}