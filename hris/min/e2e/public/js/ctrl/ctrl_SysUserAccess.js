$(document).ready(function() {
   var SysUserRefId = 0;
   $("[name='btnSysAccessUpdate']").click(function () {
      $.ajax({
         url: "trn.e2e.php",
         type: "POST",
         data: new FormData($("[name='xForm']")[0]),
         success : function(responseTxt) {
            eval(responseTxt);
         },
         enctype: 'multipart/form-data',
         processData: false,
         contentType: false,
         cache: false
      });
   });
});

function loadSysUserRecord(sysuserrefid) {
   $("#divSysAccess").html("Please Wait ...");
   $.get("trn.e2e.php",
   {
      fn:"loadSysUserAccess",
      sysuserrefid:sysuserrefid
   },
   function(data,status) {
      try {
         if (status=="success") {
            $("#divSysAccess").html(data);
            $("[name='btnSysAccessUpdate']").prop("disabled",false);
            $(document).ready(function() {
               $(".systemChk--").each(function () {
                  $(this).click(function () {
                     if ($(this).is(":checked")) {
                        $(this).val(1);
                     } else {
                        $(this).val(0);
                     }
                  });
               });
            });
         } else {
            alert(status);
         }
      } catch (e) {
          if (e instanceof SyntaxError) {
              alert(e.message);
          }
      }
   });

}

function selectSysUser(username,sysuserrefid) {
   $(".list-group-item").removeClass("active");
   $("#" + username).addClass("active");
   setValueByName("hSelectedSysUSer",sysuserrefid);
   $("#idSysUserName").html("User Profile Selected : [" + sysuserrefid + "] " + username);
   loadSysUserRecord(sysuserrefid);
}