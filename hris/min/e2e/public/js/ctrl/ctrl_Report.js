$(document).ready(function() {
   var addParam = "";
   $("#mEmpList").click(function(){
      addParam += "&hRptFile=EmployeesListRpt";
      addParam += "&paramTitle=Employees List Reports";
      addParam += "&hRptScrn=modelEmpReport";
      gotoscrn("scrnReports",addParam);
   });
   $("#mApplicantList").click(function(){
      addParam += "&hRptFile=EmployeesListRpt";
      addParam += "&paramTitle=Reports";
      addParam += "&hRptScrn=modelApplicantReport";
      gotoscrn("scrnReports",addParam);
   });
   $("#mEmpChildList").click(function(){
      addParam += "&hRptFile=EmpChildRpt";
      addParam += "&paramTitle=Empployees w/ Children Reports";
      addParam += "&hRptScrn=modelEmpChildList";
      gotoscrn("scrnReports",addParam);
   });
   $("#mEmpTrainingEduc").click(function(){
      addParam += "&hRptFile=EmpTrainingEducRpt";
      addParam += "&paramTitle=Employees W/ Training And Education Reports";
      addParam += "&hRptScrn=modelEmpTrainingEduc";
      gotoscrn("scrnReports",addParam);
   });
   $("#mNOSA").click(function(){
      addParam += "&hRptFile=NOSARpt";
      addParam += "&paramTitle=NOSA Reports";
      addParam += "&hRptScrn=modelReportScrn";
      gotoscrn("scrnReports",addParam);
   });
   $("#mNOSI").click(function(){
      addParam += "&hRptFile=NOSIRpt";
      addParam += "&paramTitle=NOSI Reports";
      addParam += "&hRptScrn=modelReportScrn";
      gotoscrn("scrnReports",addParam);
   });
   $("#mServiceRecords").click(function(){
      addParam += "&hRptFile=ServiceRecordsRpt";
      addParam += "&paramTitle=Service Record Reports";
      addParam += "&hRptScrn=modelReportScrn";
      gotoscrn("scrnReports",addParam);
   });


   $("#closeRpt").click(function(){
      addParam += "&paramTitle=Reports";
      gotoscrn("scrnReports",addParam);
   });

   $("#btnGENERATE").click(function(){
      $("#rptContent").attr("src","blank.htm");
      var rptFile = $("#ReportKind").val();
      var url = "ReportCaller.e2e.php?file=" + rptFile;
      var ext = $("#ReportKind").find(':selected').attr('ext');
      if (ext == undefined) ext = "php";
      url += "&for=" + $("#ReportKind").find(':selected').attr('for');
      url += "&ext=" + ext;
      url += "&hOwner=" + $("#ReportKind").find(':selected').attr('own');
      url += "&RptName=" + $("#ReportKind").find(':selected').attr('rptname');
      url += "&sys=" + $("#ReportKind").val();
      url += "&" + $("[name='hgParam']").val();
      url += "&" + getRPTCriteria("rptCriteria");
      $("#prnModal").modal();
      $("#rptContent").attr("src",url);
   });
   $("#ReportKind").change(function () {
      $("#column").hide();
      if ($(this).attr('sys') == "pis") {
         $.ajax({ // Send request
         type: "GET",
            url: $(this).find(':selected').attr('for') + '.e2e.php',
            data: {
            },
            cache: false,
            success: function(response) {
               $("#RptMainHolder").html(response);

            },
         });

      } else {
         var idDateCrit = $(this).find(':selected').attr('for');
         if (idDateCrit == "") {
            $(".dateCrit").hide();
         } else {
            $(".dateCrit").not("#" + idDateCrit).hide();
            $("#" + idDateCrit).show(300);
            $("#hRptFile").val($(this).val());
         }
      }
   });

   $(function() {
      $(".dateCrit, .newDataLibrary").hide();
      $("#AttendanceDateCriterias").show();
      remIconDL();
   });

   /*$("#btnShowCrit").click(function(){
      $("#rptCriteria").show();
      $("#btnShowCrit").prop("disabled",true);
      $("#btnHideCrit").prop("disabled",false);
   });
   $("#btnHideCrit").click(function(){
      $("#rptCriteria").hide();
      $("#btnHideCrit").prop("disabled",true);
      $("#btnShowCrit").prop("disabled",false);
   });*/

   $("#btnPRINT").click(function(){

   });
   $("#btnEXIT").click(function(){
      gotoscrn("scrnReports",addParam);
   });
   $("#checkAll").change(function(){
      if ($(this).is(':checked')) {
         $(".showCol--").prop("checked",true);
      } else {
         $(".showCol--").prop("checked",false);
      }
      $("[name='chkEmpName']").prop("checked",true);
   });


});
function selectMe(emprefid){
   $.get("EmpQuery.e2e.php",
   {
      emprefid:emprefid,
      hCompanyID:$("#hCompanyID").val(),
      hBranchID:$("#hBranchID").val(),
      hEmpRefId:$("#hEmpRefId").val(),
      hUserRefId:$("#hUserRefId").val()
   },
   function(data,status) {
      if (status == "error")return false;
      if (status == "success") {
         var data = JSON.parse(data);
         try
         {
         //$('[name="txtRefId"]').val(data.EmpRefId);
         $('[name="txtLName"]').val(data.LastName);
         $('[name="txtFName"]').val(data.FirstName);
         $('[name="txtMidName"]').val(data.MiddleName);
         $("#modalEmpLookUp").modal("hide");
         }
         catch (e)
         {
            if (e instanceof SyntaxError) {
                alert(e.message);
            }
         }
      }
   });

}

//$(document).ready(function(){
//  $('#btnGENERATE').click(function(){
//    alert($('#drpCvlStats :selected').text());
//  });
//});

function closeSCRN(scrntype) {
   addParam += "&paramTitle=Reports";
   gotoscrn("scrnReports",addParam);
}

function genReport(parentId) {
   var url = getRPTCriteria(parentId);
   url = "EmployeesListRpt.e2e.php?" + url;
   $("#rptHolder").html("");
   $("#rptHolder").load(url, function(responseTxt, statusTxt, xhr){
      if(statusTxt == "error")
         alert("Ooops Error: " + xhr.status + " : " + xhr.statusText);
         return false;
   });
}

function getRPTCriteria(parentId){
   var Elem_Value = "";
   var Elem_Name = "";
   var Elem_Type = "";
   var objvalue = "";
   var ObjClassName = "";
   var isInclude = false;
   $("#"+parentId+" .rptCriteria--").each(function()
   {
      ObjClassName = $(this).attr("class");
      Elem_Value = $(this).val();
      Elem_Name  = $(this).attr("name");
      isInclude = false;
      if ($(this).is("select")) {
         Elem_Type  = "select";
      } else if ($(this).is("textarea")) {
         Elem_Type  = "textarea";
      } else {
         Elem_Type  = $(this).attr("type");
      }
      if (Elem_Value != "") {
         objvalue += Elem_Name+"="+Elem_Value+"&";
      }
   });

   $("#"+parentId+" .showCol--").each(function()
   {
      ObjClassName = $(this).attr("class");
      Elem_Value = $(this).is(":checked");
      Elem_Name  = $(this).attr("name");
      isInclude = false;

      if ($(this).is("select")) {
         Elem_Type  = "select";
      } else if ($(this).is("textarea")) {
         Elem_Type  = "textarea";
      } else {
         Elem_Type  = $(this).attr("type");
      }

      if (Elem_Value == true) {
         objvalue += Elem_Name+"="+Elem_Value+"&";
      }
   });
   return objvalue;
}