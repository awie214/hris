$(document).ready(function(){
   if ($("#hUserGroup").val() == "COMPEMP") {
      selectMe($("#hEmpRefId").val());
   }
   $("hTable").val("employeesmovement");
   $("[name='sint_ApptType']").attr("onchange","changeApptStat(this.value)");
   $(".newDataLibrary").hide();
   $(".createSelect--").css("width","100%");
   var selected = $("#SelectedEMP").val();
   if (selected != "" && selected != undefined) {
      selectMe(selected);
   }
   $("#divView, #spSAVECANCEL").hide();
   $("#sint_Present").click(function () {
      if($(this).is(":checked")) {
         $("[name='date_ExpiryDate']").prop("disabled",true);
         $("[name='date_ExpiryDate']").val("PRESENT");
      } else {
         $("[name='date_ExpiryDate']").prop("disabled",false);
         $("[name='date_ExpiryDate']").val("");
      }
   });
   $("#btnADD").click(function(){
      $("[name='hRefId']").val(0);
      $("[name='hmode']").val("ADD");
      $("#btnSAVE, #btnCANCEL").show();
      if ($("[name='sint_EmployeesRefId']").val() > 0) {
         $("#divView").show();
         $("#divList").hide();

         $.get("trn.e2e.php",
         {
            fn:"AddEmpMovement",
            emprefid:$("[name='sint_EmployeesRefId']").val(),
            hCompanyID:$("#hCompanyID").val(),
            hBranchID:$("#hBranchID").val(),
            hEmpRefId:$("#hEmpRefId").val(),
            hUserRefId:$("#hUserRefId").val()
         },
         function(data,status) {
            $('#selectedEmployees').html("&nbsp;");
            if(status == "error") return false;
            if(status == "success"){
               var data = JSON.parse(data);
               try
               {

                  setValueByName("sint_AgencyRefId",setValue(data.AgencyRefId));
                  setValueByName("sint_PositionRefId",setValue(data.PositionRefId));
                  setValueByName("sint_PositionItemRefId",setValue(data.PositionItemRefId));
                  setValueByName("sint_DepartmentRefId",setValue(data.DepartmentRefId));
                  setValueByName("sint_OfficeRefId",setValue(data.OfficeRefId));
                  setValueByName("sint_DivisionRefId",setValue(data.DivisionRefId));
                  setValueByName("sint_EmpStatusRefId",setValue(data.EmpStatusRefId));
                  setValueByName("sint_DesignationRefId",setValue(data.DesignationRefId));
                  setValueByName("sint_SalaryGradeRefId",setValue(data.SalaryGradeRefId));
                  setValueByName("sint_JobGradeRefId",setValue(data.JobGradeRefId));
                  setValueByName("sint_StepIncrementRefId",setValue(data.StepIncrementRefId));
                  setValueByName("deci_SalaryAmount",setValue(data.SalaryAmount));
                  setValueByName("date_EffectivityDate",setValue(data.HiredDate));
                  setValueByName("date_ExpiryDate",setValue("PRESENT"));
                  $("#sint_Present").prop("checked",true);
                  $("#sint_Present").val(1);
                  //setValueByName("sint_isGovtService",1);
                  /*<div class="col-xs-5 mylabel">
                     Government Service
                  </div>
                  <div class="col-xs-6">
                     <div class="row margin-top">
                        <input type="hidden" class="saveFields--" name="sint_isGovtService" value="1" >
                        <div class="col-xs-6">
                           <input id="GovtServiceYes" type="radio" name="GovernmentService" checked>&nbsp;
                           <label for="GovtServiceYes">Yes</label>
                        </div>
                        <div class="col-xs-6">
                           <input id="GovtServiceNo" type="radio" name="GovernmentService">&nbsp;
                           <label for="GovtServiceNo">No</label>
                        </div>
                     </div>
                  </div>*/
               }
               catch (e)
               {
                  if (e instanceof SyntaxError) {
                      alert(e.message);
                  }
               }
            }
         });
      } else {
         alert("Ooops!!! You need to select first employee\nThank you.")
      }
   });

   $("#btnCANCEL").click(function(){
      $("#divView").hide();
      $("#divList").show();
   });

   $("#btnSAVE").click(function(){
      saveServiceRecord();
      /*
      fldnval_add = getFieldEntry("EntryEmpMovement","ADD");
      $.post("SystemAjax.e2e.php",
      {
         task:"InsertWorkExp",
         hCompanyID:$("#hCompanyID").val(),
         hBranchID:$("#hBranchID").val(),
         hUser:$("[name='hUser']").val(),
         emprefid:$("[name='sint_EmployeesRefId']").val()
      },
      function(data,status){
         if (status == "success") {
            if (data == 'success') {
               $.notify("New Work Experience Inserted","info");
               //fldnval_add = fldnval_add + "hidden|sint_EmployeesRefId|" + $("[name='sint_EmployeesRefId']").val() + "!";
               gSaveRecord(fldnval_add,"employeesmovement");
            }
         }
      });*/
      /*var end_date = $("[name='date_ExpiryDate']").val();
      if (end_date == "PRESENT") {
         $.notify("End Date Cannot be present");
         return false;
      }
      $("#hBtnValue").val($(this).val());
      $.ajax({
         url: "trn.e2e.php",
         type: "POST",
         data: new FormData($("[name='xForm']")[0]),
         success : function(responseTxt){
            if (responseTxt.trim() == "Save Success") {
               gotoscrn("scrnEmpMovement","&savesuccess=yes");
            } else if (responseTxt.trim() == "Update Success") {
               gotoscrn("scrnEmpMovement","&updatesuccess=yes");
            } else {
               alert(responseTxt);
            }
         },
         enctype: 'multipart/form-data',
         processData: false,
         contentType: false,
         cache: false
      });*/
   });
   $(function() {
      $("#hEmpRefId").append('<input type="hidden" name="fn" value="POST">');
      $("#hEmpRefId").append('<input type="hidden" name="json" value="EmpMovement">');
      $("#hEmpRefId").append('<input type="hidden" name="elem1" value="Module.PIS.EmployeesMovement">');
   });
   $("#btnPRINT").click(function () {
      var emprefid = $("[name='sint_EmployeesRefId']").val();
      var company  = $("#hCompanyID").val();
      if (emprefid == "") {
         alert("Select Employee First");
         return false;
      } else {
         if (company == "1000") {
            var url = "ReportCaller.e2e.php?file=" + "rpt_EmpMovement_1000";   
         } else {
            var url = "ReportCaller.e2e.php?file=" + "Form_EmpMovement";   
         }
         
         url += "&" + $("[name='hgParam']").val();
         url += "&ServiceRecordEmpRefId=" + emprefid;
         $("#rptContent").attr("src",url);
         $("#modalEmpMovement").modal();
      }

   });
   
   $("[type*='radio']").each(function () {
      $(this).click(function () {
         var val = $(this).val();
         //alert(val);
         switch ($(this).attr("name")) {
            case "GovernmentService":
               $("[name='sint_isGovtService']").val(val);
               break;
            case "IsServiceRecord":
               $("[name='sint_isServiceRecord']").val(val);
               break;
         }
      });
   });
   $("#btn_SAVE").click(function () {
      var new_empid = $("[name='char_NewEmployeesId']").val(); 
      var end_date = $("[name='date_ExpiryDate']").val();
      saveServiceRecord(end_date,new_empid);
   });
   $("[name='sint_SalaryGradeRefId']").change(function () {
      var sg = $(this).val();
      var jg = $("[name='sint_JobGradeRefId']").val(); 
      var si = $("[name='sint_StepIncrementRefId']").val(); 
      getSalary(sg,jg,si);
   });
   $("[name='sint_StepIncrementRefId']").change(function () {
      var si = $(this).val();
      var jg = $("[name='sint_JobGradeRefId']").val(); 
      var sg = $("[name='sint_SalaryGradeRefId']").val(); 
      getSalary(sg,jg,si);
   });
   $("[name='sint_JobGradeRefId']").change(function () {
      var jg = $(this).val();
      var sg = $("[name='sint_SalaryGradeRefId']").val(); 
      var si = $("[name='sint_StepIncrementRefId']").val(); 
      getSalary(sg,jg,si);
   });
});

function selectedItems(emprefid,lname,fname) {
   var action = "";
   if ($("#hUserGroup").val() == "COMPEMP") {
      action = "false,false,false,false";
   } else {
      action = "true,true,false,false";
   }
   $(".list-group-item").removeClass("active");
   $("[name='txtEmpName'").val(lname + ", " + fname);
   $("#" + emprefid).addClass("active");
   $("[name='sint_EmployeesRefId']").val(emprefid);
   $.get("changeSessionValue.e2e.php",
   {
      hGridTblHdr:"Start Date|End Date|Position|Emp.Status",
      hGridTblFld:"EffectivityDate|ExpiryDate|PositionRefId|EmpStatusRefId",
      hGridTblId:"gridTable",
      hGridDBTable:"employeesmovement",
      sql:"SELECT * FROM `employeesmovement` WHERE `EmployeesRefId` = " + emprefid + " ORDER BY EffectivityDate Desc LIMIT 100",
      listAction:action,
      json: "EmpMovement",
      Module: "Module.PIS.EmployeesMovement",
      empselected:emprefid
   },
   function(data,status){
      if (status=='success') {
         refreshTable(emprefid);
         tr_Click(emprefid);
      }
   });
}

function afterNewSave(newRefId) {
   $("#divView").hide();
   $("#divList").show();
   var lname = $("[name='txtEmpName'").val().split(",")[0];
   var fname = $("[name='txtEmpName'").val().split(",")[1];
   selectedItems($("[name='sint_EmployeesRefId']").val(),lname,fname);
   alert("Record Succesffuly Inserted\nRef. ID: " + newRefId);
}

function refreshTable(emprefid) {
   $("#spGridTable").html("");
   $("#spGridTable").load("listRefresh.e2e.php",
   {
      EmployeesRefid : emprefid
   },
   function(responseTxt, statusTxt, xhr){
      if(statusTxt == "error")
         alert("Ooops Error: " + xhr.status + ": " + xhr.statusText);
         return false;
   });
}

function selectMe(emprefid) {
   $.get("EmpQuery.e2e.php",
   {
      emprefid:emprefid,
      hCompanyID:$("#hCompanyID").val(),
      hBranchID:$("#hBranchID").val(),
      hEmpRefId:$("#hEmpRefId").val(),
      hUserRefId:$("#hUserRefId").val()
   },
   function(data,status) {
      $('#selectedEmployees').html("&nbsp;");
      if(status == "error") return false;
      if(status == "success"){
         var data = JSON.parse(data);
         try
         {
            setHTMLById("RefIdSelected",data.RefId);
            setValueById("hRefIdSelected",data.RefId);
            setValueByName("txtEmpId",data.AgencyId);
            setValueByName("sint_IncumbentEmployeesRefId",data.RefId);
            //setValueByName("txtFullName",data.LastName + ", " + data.FirstName + " " + data.MiddleName);
            setHTMLById("selectedEmployees",data.LastName + ", " + data.FirstName + " " + data.MiddleName);
            $("#modalEmpLookUp").modal("hide");
            selectedItems(data.RefId,
                          data.LastName,
                          data.FirstName);
         }
         catch (e)
         {
            if (e instanceof SyntaxError) {
                alert(e.message);
            }
         }
      }
   });
}



function afterDelete() {
   $("#divView").hide();
   $("#divList").show();
   refreshTable($("[name='sint_EmployeesRefId']").val());
}
function afterEditSave(refid) {
   $("#divView").hide();
   $("#divList").show();
   refreshTable($("[name='sint_EmployeesRefId']").val());
   alert("Record Updated");
}
function deleteRecord(refid) {
   if (refid > 0)
   {
      $("[name='hRefId']").val(refid);
      var table = $("[name='hTable']").val();
      if (confirm("Are you sure you want to delete this record " + refid + "?")) {
         if (table!="") {
            gDeleteRecord(table,refid);
         }
         else {
            alert("Err : No db Table assigned");
            return false;
         }
      }
   }
   else {
      alert("Err : No Ref. Id assigned");
      return false;
   }
}

function viewInfo(refid,mode) {
   $("#divView").show();
   $("#divList").hide();
   $("[name='hRefId']").val(refid);
   if (mode == 3) {
      var lgDis = true;
      $("[name='hmode']").val("VIEW");
      $("#btnSAVE").hide();
      $("#btnCANCEL").show();
   }
   else {
      var lgDis = false;
      $("[name='hmode']").val("EDIT");
      $("#btnSAVE").show();
      $("#btnCANCEL").show();
   }
   $("#EntryEmpMovement .saveFields--, [name='txtFullName']").prop("disabled",lgDis);
   loadRecord(refid,$("[name='sint_EmployeesRefId']").val());
}

function loadRecord(tbl_refid,emprefid) {
   $.get("ctrl_EmpMovement.e2e.php",
   {
      t:"viewRecord",
      tbl_refid:tbl_refid,
      emprefid:emprefid
   },
   function(data,status) {
      var EmpInformation = 0;
      if (data == false) {
         $("#EmpMovementRefId").html("");
         $("[name='txtFullName']").val("");
         $("#EntryEmpMovement .saveFields--").each(function(){
            $(this).val("");
         });
      }
      else {
         try {
            data = JSON.parse(data);
            if (parseInt(data.RefId) > 0) {
               $("#EmpMovementRefId").html(data.RefId);
               $("[name='txtFullName']").val(data.LastName + ", " + data.FirstName);
               $("#EntryEmpMovement .saveFields--").each(function(){
                  var flds = $(this).attr("name").split("_")[1];
                  $(this).val(data[flds]);
               });
               if (data.ExpiryDate == null) {                  
                  $("[name='date_ExpiryDate']").val("PRESENT");
                  $("#sint_Present").prop("checked",true);
                  $("#sint_Present").val(1);
               }
            } else {
               alert("Ooops!!! No RefId");
            }
         } catch (e) {
             if (e instanceof SyntaxError) {
                 alert(e.message);
             }
         }
      }
   });
}

function changeApptStat(refid) {
   $.get("trn.e2e.php",
   {
      fn:"getApptStat",
      refid:refid
   },
   function(data,status){
      if (status == 'success') {
         $("[name='sint_ApptStatusRefId']").empty();
         eval(data);
      }
   });
}

function saveServiceRecord() {
   $("[name='fn']").val("saveServiceRecord");
   $.ajax({
      url: "trn.e2e.php",
      type: "POST",
      data: new FormData($("[name='xForm']")[0]),
      success : function(responseTxt){
         responseTxt = responseTxt.trim();
         if (responseTxt == "Save Success") {
            gotoscrn("scrnEmpMovement","&savesuccess=yes");
         } else if (responseTxt == "Update Success") {
            gotoscrn("scrnEmpMovement","&updatesuccess=yes");
         } else {
            alert(responseTxt);
         }
      },
      enctype: 'multipart/form-data',
      processData: false,
      contentType: false,
      cache: false
   });
}