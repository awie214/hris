$(document).ready(function () {
});
function refreshTable(emprefid) {
   $("#spGridTable").html("");
   $("#spGridTable").load("listRefresh.e2e.php",
   {
      EmployeesRefid : emprefid
   },
   function(responseTxt, statusTxt, xhr){
      if(statusTxt == "error")
         alert("Ooops Error: " + xhr.status + ": " + xhr.statusText);
         return false;
   });
}
function selectedItems(emprefid,lname,fname) {
   $(".list-group-item").removeClass("active");
   $("[name='txtEmpName'").val(lname + ", " + fname);
   $("#" + emprefid).addClass("active");
   $("[name='sint_EmployeesRefId']").val(emprefid);
   $.get("changeSessionValue.e2e.php",
   {
      hGridTblHdr:"Employee ID|Award|Date",
      hGridTblFld:"EmployeesId|AwardsRefId|YearReceived",
      hGridTblId:"gridTable",
      hGridDBTable:"employeesrecognitionandreward",
      sql:"SELECT * FROM `employeesrecognitionandreward` WHERE `EmployeesRefId` = " + emprefid + " ORDER BY RefId Desc LIMIT 100",
      listAction:[true,true,false]
   },
   function(data,status){
      if (status=='success') {
         refreshTable(emprefid);
         tr_Click(emprefid);
      }
   });
}
function selectMe(emprefid) {
   $.get("EmpQuery.e2e.php",
   {
      emprefid:emprefid,
      hCompanyID:$("#hCompanyID").val(),
      hBranchID:$("#hBranchID").val(),
      hEmpRefId:$("#hEmpRefId").val(),
      hUserRefId:$("#hUserRefId").val()
   },
   function(data,status) {
      $('#selectedEmployees').html("&nbsp;");
      if(status == "error") return false;
      if(status == "success"){
         var data = JSON.parse(data);
         try
         {  
            $('[name="sint_EmployeesRefId"]').val(data.RefId);
            $("[name='sint_EmployeesId']").val(data.AgencyId);
            $('#selectedEmployees').html(data.LastName + ", " + data.FirstName + " " + data.MiddleName);
            $("#divList").show(500);
            $("#modalEmpLookUp").modal("hide");
            selectedItems(data.RefId,
                          data.LastName,
                          data.FirstName);
         }
         catch (e)
         {
            if (e instanceof SyntaxError) {
                alert(e.message);
            }
         }
      }
   });
}

function afterNewSave(SaveSuccessfull){
   $("#divView").hide();
   $("#divList").show();
   alert("Record Save !!!");
   refreshTable($("[name='sint_EmployeesRefId']").val());
}
function afterDelete() {
   $("#divView").hide();
   $("#divList").show();
   alert("Record Deleted !!!");
   refreshTable($("[name='sint_EmployeesRefId']").val());
}
function afterEditSave(refid) {
   $("#divView").hide();
   $("#divList").show();
   alert("Record Updated !!!");
   refreshTable($("[name='sint_EmployeesRefId']").val());
}