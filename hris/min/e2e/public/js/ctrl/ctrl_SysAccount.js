$(document).ready(function() {
   $("#radCanAddYes").click(function(){
      $("#hCanAdd").val(1);
   });
   $("#radCanAddNo").click(function(){
      $("#hCanAdd").val(0);
   });
   /*=============================*/

   $("#radCanEditYes").click(function(){
      $("#hCanEdit").val(1);
   });
   $("#radCanEditNo").click(function(){
      $("#hCanEdit").val(0);
   });
   /*=============================*/

   $("#radCanDeleteYes").click(function(){
      $("#hCanDelete").val(1);
   });
   $("#radCanDeleteNo").click(function(){
      $("#hCanDelete").val(0);
   });
   /*=============================*/

   $("#radCanPrintYes").click(function(){
      $("#hCanPrint").val(1);
   });
   $("#radCanPrintNo").click(function(){
      $("#hCanPrint").val(0);
   });
   /*=============================*/

   $("#radCanViewYes").click(function(){
      $("#hCanView").val(1);
   });
   $("#radCanViewNo").click(function(){
      $("#hCanView").val(0);
   });
   /*=============================*/

   $("#btnADD").click(function(){
      $("[name='hRefId']").val(0);
      if ($("#hSelectedGroup").val() != "")
      {
         $("#subSCRN_DataEntry").modal();
         $("#entry-modal-content").html("");
         $("#entry-modal-content").load("incSysAccount.e2e.php",
         {
            task:"modal",
            mode:"add",
            group:$("#hSelectedGroup").val(),
            SysGroupRefid:$("[name='hSysGroupRefId']").val(),
            RefId:0
         },
         function(responseTxt, statusTxt, xhr){
            if(statusTxt == "error")
               alert("Ooops Error: " + xhr.status + ": " + xhr.statusText);
               return false;
         });
      } else {
         alert("Select Group First !!!");
      }
   });

   $("#btnSAVE").click(function() {
      var objvalue = "";
      var err = false;
      var msg = "";
      objvalue += "select-one" + "|" + "sint_SysModulesRefId" + "|" + $("[name='sint_SysModulesRefId']").val();
      objvalue += "!hidden" + "|" + "sint_SysGroupRefId" + "|" + $("[name='hSysGroupRefId']").val();
      objvalue += "!hidden" + "|" + "sint_CanAdd" + "|" + $("#hCanAdd").val();
      objvalue += "!hidden" + "|" + "sint_CanEdit" + "|" + $("#hCanEdit").val();
      objvalue += "!hidden" + "|" + "sint_CanDelete" + "|" + $("#hCanDelete").val();
      objvalue += "!hidden" + "|" + "sint_CanPrint" + "|" + $("#hCanPrint").val();
      objvalue += "!hidden" + "|" + "sint_CanView" + "|" + $("#hCanView").val();
      objvalue += "!";
      if ($("[name='sint_SysModulesRefId']").val() == "") {
         msg = "Please Select System Modules\n";
         err = true;
      }
      if ($("[name='hSysGroupRefId']").val() == "") {
         msg = "Please Select System Group\n";
         err = true;
      }

      if(err) {
         alert(msg);
         return false;
      } else {
         if ($("#hTable").val() == "") {
            alert("No Table Assigned");
            return false;
         } else {
            $("#subSCRN_DataEntry").modal('hide');
            gSaveRecord(objvalue,$("#hTable").val());
         }
      }
   });
});

function afterEditSave(refid) {
   alert("Record Updated");
   refreshListGroup($("#hSysGroupRefId").val());
}
function afterNewSave(newRefId) {
   alert("New Record Inserted\nRef. ID: " + newRefId);
   refreshListGroup($("#hSysGroupRefId").val());
}
function afterDelete() {
   alert("Record Deleted");
   refreshListGroup($("#hSysGroupRefId").val());
}
function EditGroupAccess(refid) {
   if (refid > 0) {
      $("[name='hRefId']").val(refid);
      $("#subSCRN_DataEntry").modal();
      $("#entry-modal-content").html("");
      $("#entry-modal-content").load("incSysAccount.e2e.php",
      {
         task  : "modal",
         mode  : "edit",
         group : $("#hSelectedGroup").val(),
         RefId : refid
      },
      function(responseTxt, statusTxt, xhr){
         if(statusTxt == "error")
            alert("Ooops Error: " + xhr.status + ": " + xhr.statusText);
            return false;
      });
   }
}
function DelGroupAccess(refid) {
   if (refid > 0)
   {
      $("[name='hRefId']").val(refid);
      var table = $("[name='hTable']").val();
      if (confirm("Are you sure you want to delete this record " + refid + "?")) {
         if (table!="") {
            gDeleteRecord(table,refid);
         }
         else {
            alert("Err : No db Table assigned");
            return false;
         }
      }
   }
   else {
      alert("Err : No Ref. Id assigned");
      return false;
   }
}

function selectSysGroup(sysgroupcode,sysgrouprefid) {
   var groupSelected = sysgroupcode;
   $(".list-group-item").removeClass("active");
   $("#" + groupSelected).addClass("active");
   $("#hSelectedGroup").val(groupSelected);
   $("#hSysGroupRefId").val(sysgrouprefid);
   $("#SysGroupCode").html(groupSelected);
   refreshListGroup(sysgrouprefid);
}

function refreshListGroup(sysgrouprefid) {
   $("#SysGroupDetailsAccess").html("");
   $("#SysGroupDetailsAccess").load("incSysAccount.e2e.php",
   {
         task : "list",
         mode : "list",
         SysGroupRefid : sysgrouprefid
   },
   function(responseTxt, statusTxt, xhr){
      if(statusTxt == "error")
         alert("Ooops Error: " + xhr.status + ": " + xhr.statusText);
         return false;
   });

}
