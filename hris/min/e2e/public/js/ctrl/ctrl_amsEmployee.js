$(document).ready(function () {
   remIconDL();    
   tabClick(1);
   $("#updateRow").hide();
   $("[class*='saveFields--']").attr("disabled",true);
   $("[name='btnTab_1']").click(function() {
      tabClick(1);
   });
   $("[name='btnTab_2']").click(function() {
      tabClick(2);
   });
   $("[name='btnTab_3']").click(function() {
      tabClick(3);
      
   });
   $("#chkAttRef").click(function () {
      var emprefid = $("[name='hEmpRefId']").val();
      if (emprefid != "" && emprefid != 0) {
         if ($(this).is(":checked")) {
            $("[class*='saveFields--']").attr("disabled",false);
            $("#updateRow").show();
         } else {
            $("[class*='saveFields--']").attr("disabled",true);
            $("#updateRow").hide();
         }
      } else {
         alert("No Employee Selected");
         return false;
      }
   });
   $("#btnUpdate").click(function () {
      $.ajax({
         url: "amsTrn.php",
         type: "POST",
         data: new FormData($("[name='xForm']")[0]),
         success : function(responseTxt){
            eval(responseTxt);
         },
         enctype: 'multipart/form-data',
         processData: false,
         contentType: false,
         cache: false
      });
   });
});
function selectedItems(emprefid) {
   getEmpInfo(emprefid);
}
function selectMe(emprefid) {
   getEmpInfo(emprefid);
}

/*function getEmpInfo(emprefid) {
   $("[name='hEmpRefId']").val(emprefid);
   $("[name='RefId']").val(emprefid);
   $.get("amsEmpDetail.e2e.php",
   {
      EmpRefId:emprefid,
      hCompanyID:$("#hCompanyID").val(),
      hBranchID:$("#hBranchID").val(),
      hEmpRefId:$("#hEmpRefId").val(),
      hUserRefId:$("#hUserRefId").val()
   },
   function(data,status) {
      if (status == "success") {
         try {
            eval(data);
            var data = JSON.parse(data);
            $("[name='char_LastName']").val(data["LastName"]);
            $("[name='char_FirstName']").val(data["FirstName"]);
            $("[name='char_MiddleName']").val(data["MiddleName"]);
            $("[name='char_ExtName']").val(data["ExtName"]);
            $("[name='date_BirthDate']").val(data["BirthDate"]);
            $("[name='sint_AgencyId']").val(data["AgencyId"]);
            $("[name='sint_PositionRefId']").val(data["PositionRefId"]);
            $("[name='date_AssumptionDate']").val(data["AssumptionDate"]);
            $("[name='date_ResignedDate']").val(data["ResignedDate"]);
            $("[name='date_RehiredDate']").val(data["RehiredDate"]);
            $("[name='date_HiredDate']").val(data["HiredDate"]);
            $("[name='sint_PositionItemRefId']").val(data["PositionItemRefId"]);
            $("[name='sint_OfficeRefId']").val(data["OfficeRefId"]);
            $("[name='sint_DepartmentRefId']").val(data["DepartmentRefId"]);
            $("[name='sint_DivisionRefId']").val(data["DivisionRefId"]);
            $("[name='sint_EmpStatusRefId']").val(data["EmpStatusRefId"]);
            $("[name='sint_SalaryGradeRefId']").val(data["SalaryGradeRefId"]);
            $("[name='sint_StepIncrementRefId']").val(data["StepIncrementRefId"]);
            $("[name='deci_SalaryAmount']").val(data["SalaryAmount"]);
            $("[name='char_PayPeriod']").val(data["PayPeriod"]);
            $("[name='sint_PayrateRefId']").val(data["PayrateRefId"]);
            $("[name='sint_workscheduleRefId']").val(data["WorkScheduleRefId"]);
         } catch (e) {
             if (e instanceof SyntaxError) {
                 alert(e.message);
             }
         }
      }
   });
}*/

function getEmpInfo(emprefid) {
   $("[name='hEmpRefId']").val(emprefid);
   $("[name='RefId']").val(emprefid);
   $.get("amsTrn.php",
   {
      fn:"getEmpRecord",
      EmpRefId:emprefid,
      hCompanyID:$("#hCompanyID").val(),
      hBranchID:$("#hBranchID").val(),
      hEmpRefId:$("#hEmpRefId").val(),
      hUserRefId:$("#hUserRefId").val(),

   },
   function(data,status) {
      if (status == "success") {
         try {
            eval(data);
         } catch (e) {
             if (e instanceof SyntaxError) {
                 alert(e.message);
             }
         }
      }
   });
}



