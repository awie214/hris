$(document).ready(function () {
   $("input[class*='number--']").each(function () {
      var e = arguments[0];
      fnNumberClass($(this).attr("name"), e);
   });

   $("input[class*='alpha--']").each(function () {
      var e = arguments[0];
      fnAlphaClass($(this).attr("name"), e);
   });

   $("input[class*='alphanum--']").each(function () {
      var e = arguments[0];
      fnAlphaNumClass($(this).attr("name"), e);
   });

   $("input[class*='valDate--']").each(function () {
      fnValDate($(this).attr("name"))
   });

   $(function () {
      $("input[class*='date--']").each(function () {
         fnDateClass($(this).attr("name"));
      });
   });

   $("input[class*='uCase--']").each(function () {
      $(this).blur(function () {
         var objvalue = $(this).val();
         $(this).val(objvalue.toUpperCase());
      });
   });
   $("[class*='dateto']").each(function () {
      $(this).blur(function () {
         var objid = $(this).attr("for");
         var from = $("#" + objid).val();
         var to = $(this).val();
         if (to != "") {
            if (from > to) {
               $(this).notify("Invalid Date Range.", { position: "top center" });
               $(this).val("");
               return false;
            }   
         }
      });
   });
   $("[class*='nonzero']").each(function () {
      $(this).blur(function () {
         if ($(this).val() != "") {
            if ($(this).val() <= 0) {
               $.notify("Field Cannot Be Zero.", { position: "top center" });
               $(this).val("");
               return false;
            }
         }

      });
   });

});

function fnDateClass(objname) {
   $("[name='" + objname + "']").attr("data-date-format", "yyyy-mm-dd");
   $("[name='" + objname + "']").datepicker();
  /* $("[name='" + objname + "']").click(function () {
      $(".datepicker").css({ "z-index":"9999"});
   });*/
}

function fnAlphaClass(objname, e) {
   $("[name='" + objname + "']").keypress(function (e) {
      var isLetter = false;
      if (e.which >= 97 && e.which <= 122) {
         isLetter = true;
      } else if (e.which >= 65 && e.which <= 90) {
         isLetter = true;
      } else if (e.which == 8 ||
         e.which == 127 ||
         e.which == 0 ||
         e.which == 46 ||
         e.which == 32) {
         isLetter = true;
      }
      if (!isLetter) e.preventDefault();
   });
}
function fnNumberClass(objname, e) {
   $("[name='" + objname + "']").keypress(function (e) {
      var isNumber = false;
      if (e.which >= 48 && e.which <= 57) {
         isNumber = true;
      } else if (e.which == 8 || e.which == 127 || e.which == 0 || e.which == 250) {
         isNumber = true;
      } else if ($("[name='" + objname + "']").val().split(".").length <= 1 && e.which == 46) {
         isNumber = true;
      }
      if (!isNumber) e.preventDefault();
   });
}

function fnAlphaNumClass(objname, e) {
   $("[name='" + objname + "']").keypress(function (e) {
      var isAlphaNumber = false;
      if (e.which >= 48 && e.which <= 57) {
         isAlphaNumber = true;
      } else if (e.which >= 97 && e.which <= 122) {
         isAlphaNumber = true;
      } else if (e.which >= 65 && e.which <= 90) {
         isAlphaNumber = true;
      } else if (e.which == 8 || e.which == 127 || e.which == 0 || e.which == 46 || e.which == 32 || e.which == 250) {
         isAlphaNumber = true;
      }
      if (!isAlphaNumber) e.preventDefault();
   });
   /*$("[name='"+objname+"']").blur(function () {
      alert(3);
      if ($(this).val() <= 0) {
         alert("Cannot Enter Negative or Zero number.");
         $(this).focus();
         return false;
      }
   });*/
}

function fnValDate(objname) {
   var d = new Date();
   var n = d.toLocaleDateString();
   $("[name='" + objname + "']").blur(function () {
      var valDate = $("[name='" + objname + "']").val();
      var val = Date.parse(valDate);
      var now = Date.parse(n);
      if (val > (now + 86400000)) {
         $("[name='" + objname + "']").val("");
         $("[name='" + objname + "']").select();
         $("[name='" + objname + "']").focus();
         $.notify("Cannot Enter Future Date.", { position: "top center" });
         return false;
      } else {
         if (valDate != "") {
            if (!isValidDate(valDate)) {
               $.notify("Date Format yyyy-mm-dd", { position: "top center" });
               $("[name='" + objname + "']").val("");
               $("[name='" + objname + "']").select();
               $("[name='" + objname + "']").focus();
               return false;
            }   
         }
      }
   });
}