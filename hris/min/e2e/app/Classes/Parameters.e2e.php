<?php 

	class sysParam {

		public $EmployeesRefId         = "";
   		public $EmployeesRefId_Login   = "";
   		public $UserCode               = "";
   		public $CompanyId              = "";
   		public $BranchId               = "";
   		public $userSESSION            = "";
   		public $user                   = "";

        public function setEmployeesRefId($newData) {
        	$this->EmployeesRefId = $newData;
        }
        public function getEmployeesRefId() {
        	return $this->EmployeesRefId;
        }

        public function setCompanyId($newData) {
        	$this->CompanyId = $newData;
        }
        public function getCompanyId() {
        	return $this->CompanyId;
        }
        public function setBranchId($newData) {
        	$this->BranchId = $newData;
        }
        public function getBranchId() {
        	return $this->BranchId;
        }
        public function setEmployeesRefId_Login($newData) {
        	$this->EmployeesRefId_Login = $newData;
        }
        public function getEmployeesRefId_Login() {
        	return $this->EmployeesRefId_Login;
        }
        public function setUserCode($newData) {
        	$this->UserCode = $newData;
        }
        public function getUserCode() {
        	return $this->UserCode;
        }
        public function setUserSESSION($newData) {
        	$this->UserSESSION = $newData;
        }
        public function getUserSESSION() {
        	return $this->UserSESSION;
        }	
        public function setUser($newData) {
        	$this->user = $newData;
        }
        public function getUser() {
        	return $this->user;
        }
	}

?>