<?php
	function fullDTR($emprefid,$month,$year,$workschedule) {		
		$dtr_process        = [
			"EmployeesRefId"=>"",
			"WorkScheduleRefId"=>"",
			"Month"=>"",
			"Year"=>"",
			"Total_Regular_Hr"=>"0",
			"Total_Excess_Hr"=>"0",
			"Total_COC_Hr"=>"0",
			"Total_OT_Hr"=>"0",
			"Total_Tardy_Hr"=>"0",
			"Total_Undertime_Hr"=>"0",
			"Total_Absent_Count"=>"0",
			"Total_Present_Count"=>"0",
			"Total_Tardy_Count"=>"0",
			"Total_Undertime_Count"=>"0",
			"Days_Absent"=>"",
			"Days_Tardy"=>"",
			"Days_Undertime"=>"",
			"VL_Earned"=>"0",
			"SL_Earned"=>"0",
			"SPL_Used"=>"0",
			"COC_Used"=>"0",
			"VL_Used"=>"0",
			"SL_Used"=>"0",
			"VL_Days"=>"",
			"FL_Days"=>"",
			"SL_Days"=>"",
			"SPL_Days"=>"",
			"Tardy_Deduction_EQ"=>"0",
			"Undertime_Deduction_EQ"=>"0",
			"Regular_Hr_EQ"=>"0",
			"Total_Days_EQ"=>"0",
			"Total_Absent_EQ"=>"0",
			"01_Late"=>"0",
			"01_UT"=>"0",
			"01_Absent"=>"0",
			"02_Late"=>"0",
			"02_UT"=>"0",
			"02_Absent"=>"0",
			"03_Late"=>"0",
			"03_UT"=>"0",
			"03_Absent"=>"0",
			"04_Late"=>"0",
			"04_UT"=>"0",
			"04_Absent"=>"0",
			"05_Late"=>"0",
			"05_UT"=>"0",
			"05_Absent"=>"0",
			"06_Late"=>"0",
			"06_UT"=>"0",
			"06_Absent"=>"0",
			"07_Late"=>"0",
			"07_UT"=>"0",
			"07_Absent"=>"0",
			"08_Late"=>"0",
			"08_UT"=>"0",
			"08_Absent"=>"0",
			"09_Late"=>"0",
			"09_UT"=>"0",
			"09_Absent"=>"0",
			"10_Late"=>"0",
			"10_UT"=>"0",
			"10_Absent"=>"0",
			"11_Late"=>"0",
			"11_UT"=>"0",
			"11_Absent"=>"0",
			"12_Late"=>"0",
			"12_UT"=>"0",
			"12_Absent"=>"0",
			"13_Late"=>"0",
			"13_UT"=>"0",
			"13_Absent"=>"0",
			"14_Late"=>"0",
			"14_UT"=>"0",
			"14_Absent"=>"0",
			"15_Late"=>"0",
			"15_UT"=>"0",
			"15_Absent"=>"0",
			"16_Late"=>"0",
			"16_UT"=>"0",
			"16_Absent"=>"0",
			"17_Late"=>"0",
			"17_UT"=>"0",
			"17_Absent"=>"0",
			"18_Late"=>"0",
			"18_UT"=>"0",
			"18_Absent"=>"0",
			"19_Late"=>"0",
			"19_UT"=>"0",
			"19_Absent"=>"0",
			"20_Late"=>"0",
			"20_UT"=>"0",
			"20_Absent"=>"0",
			"21_Late"=>"0",
			"21_UT"=>"0",
			"21_Absent"=>"0",
			"22_Late"=>"0",
			"22_UT"=>"0",
			"22_Absent"=>"0",
			"23_Late"=>"0",
			"23_UT"=>"0",
			"23_Absent"=>"0",
			"24_Late"=>"0",
			"24_UT"=>"0",
			"24_Absent"=>"0",
			"25_Late"=>"0",
			"25_UT"=>"0",
			"25_Absent"=>"0",
			"26_Late"=>"0",
			"26_UT"=>"0",
			"26_Absent"=>"0",
			"27_Late"=>"0",
			"27_UT"=>"0",
			"27_Absent"=>"0",
			"28_Late"=>"0",
			"28_UT"=>"0",
			"28_Absent"=>"0",
			"29_Late"=>"0",
			"29_UT"=>"0",
			"29_Absent"=>"0",
			"30_Late"=>"0",
			"30_UT"=>"0",
			"30_Absent"=>"0",
			"31_Late"=>"0",
			"31_UT"=>"0",
			"31_Absent"=>"0",
			"Overtime_Remarks"=>""
		];
		$TimeIn 					= "";
		$LunchOut 					= "";
		$LunchIn 					= "";
		$TimeOut 					= "";
		$RegHour 					= "";
		$ExcessHour 				= "";
		$TardyEQ					= "";
		$UndertimeEQ				= "";
		$AbsentEQ					= "";
		$Days_Absent				= "";
		$Days_Tardy					= "";
		$Days_VL 					= "";
		$Days_FL 					= "";
		$Days_SL 					= "";
		$Days_SPL 					= "";
		$Days_Undertime				= "";
		$COC_Str 					= "";
		$First_Monday 				= "";
		$WorkSchedule 				= 0;
		$day_count_diff				= 0;
		$PresentDays				= 0;
		$VL_Count 					= 0;
		$SL_Count 					= 0;
		$SPL_Count 					= 0;
		$FL_count					= 0;
		$TotalRegHour 				= 0;
		$TotalTardyHour 			= 0;
		$TotalUndertimeHour			= 0;
		$TotalExcessHour			= 0;
		$TotalAbsentCount			= 0;
		$TotalCOCHour 				= 0;
		$TotalTardyCount 			= 0;
		$TotalUndertimeCount		= 0;
		$TotalDeduct				= 0;
		$TotalOTHour 				= 0;
		$Total_Days_EQ				= 0;
		$Total_Absent_EQ			= 0;
		$Used_COC 					= 0;
		$Half_SL 					= 0;
		$Late_Employee 				= 0;
		$arr 						= array();
		$RegHour_PerWeek 			= Array(0,0,0,0,0,0,0);
		$TardyHour_PerWeek 			= Array(0,0,0,0,0,0,0);
		$UndertimeHour_PerWeek 		= Array(0,0,0,0,0,0,0);
		$COCHours_PerWeek 			= Array(0,0,0,0,0,0,0);
		$ExcessHour_PerWeek 		= Array(0,0,0,0,0,0,0);
		$OTHours_PerWeek 			= Array(0,0,0,0,0,0,0);
		$UndertimeCount_PerWeek 	= Array(0,0,0,0,0,0,0);
		$AbsentCount_PerWeek 		= Array(0,0,0,0,0,0,0);
		$OTCount_PerWeek 			= Array(0,0,0,0,0,0,0);
		$TardyCount_PerWeek 		= Array(0,0,0,0,0,0,0);
		$CutOff_PerWeek 			= Array(0,0,0,0,0,0,0);
		$Friday_Holiday_PerWeek 	= Array(0,0,0,0,0,0,0);
		$WorkDayConversion 			= file_get_contents(json."WorkDayConversion.json");
		$WorkDayEQ   				= json_decode($WorkDayConversion, true);
		$Overtime_Remarks 			= "";

		if (getvalue("hucode") != "COMPEMP") {
	    	$emprefid = getvalue("emprefid");	
	    } else {
	    	$emprefid = getvalue("hEmpRefId");
	    }
	    $AssumptionDate = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","AssumptionDate");


		$new_week 					= "";
		$week_count 				= 0;
		$userID 					= 0;
		$NewMonth 					= intval($month);
		$NewYear 					= intval($year);
		if ($NewMonth <= 9) {
			$NewMonth = "0".$NewMonth;
		}
		$curr_date					= date("Y-m-d",time());
		$num_of_days 				= cal_days_in_month(CAL_GREGORIAN,$NewMonth,$NewYear);
		$exact_end_days 			= $num_of_days;
		$month_start 				= $NewYear."-".$NewMonth."-01";
		$month_end 					= $NewYear."-".$NewMonth."-".$num_of_days;
		if (strtotime($AssumptionDate) > strtotime($month_start)) {
			$month_start = $AssumptionDate;
			$Late_Employee = 1;
		}
		if (strtotime($curr_date) <= strtotime($month_end)) {
			$month_end = $curr_date;
		} else {
			$month_end = $month_end;
		}
		$map["[[StartMonth]]"]		= date("F d, Y",strtotime($month_start));
		$map["[[EndMonth]]"]		= date("F d, Y",strtotime($month_end));
		$num_of_days 				= date("d",strtotime($month_end));
		$Period 					= $NewMonth."/01/".$NewYear." - ".$NewMonth."/".$num_of_days."/".$NewYear;
		for($d=1;$d<=$num_of_days;$d++) {
			$y = $NewYear."-".$NewMonth."-".$d;
			$day = date("D",strtotime($y));	
			if ($d <= 9) $d = "0".$d;
			$y = $NewYear."-".$NewMonth."-".$d;
			$week = date("W",strtotime($y));
			if ($week != $new_week) {
				$week_count++;
			}
			$dtr = [
		   		"AttendanceDate" => "$y",
		   		"AttendanceTime" => "",
		   		"UTC" => "",
		   		"TimeIn" => "",
		   		"LunchOut" => "",
		   		"LunchIn" => "",
		   		"TimeOut" => "",
		   		"OBOut" => "",
		   		"OBIn" => "",
		   		"Day" => "$d",
		   		"Week" => "$week_count",
		   		"KEntry" => "",
		   		"Holiday" => "",
		   		"CTO"=>"",
		   		"Leave"=> "",
		   		"FL"=>"",
		   		"OffSus"=>"",
		   		"OffSusName"=>"",
		   		"HasOT"=>"",
		   		"OBName"=>"",
		   		"OBTimeIn"=>"",
		   		"OBTimeOut"=>"",
		   		"OTTime"=>"",
		   		"ChangesRem"=>""
		   	];
		   	$arr["ARR"][$y] = $dtr;
			$new_week = $week;
		}

		$row = FindFirst("employees","WHERE RefId = '$emprefid'","*");
		if ($row){ 
			$emprefid 			= $row["RefId"];
	    	$EmployeesName		= $row["LastName"].", ".$row["FirstName"]." ".$row["ExtName"]." ".$row["MiddleName"];	
			$AgencyId			= $row["AgencyId"];
			$biometricsID 		= $row["BiometricsID"];
			$CompanyID 			= $row["CompanyRefId"];
			$BranchID			= $row["BranchRefId"];
			$KEntryContent 		= file_get_contents(json."Settings_".$CompanyID.".json");
	      	$KEntry_json   		= json_decode($KEntryContent, true);
	      	$OTMinTime 			= $KEntry_json["OTMinTime"];
	      	$COCMinTime 		= $KEntry_json["COCMinTime"];
	      	$IsAuto 			= FindFirst("ams_employees","WHERE EmployeesRefId = '$emprefid'","IsAutoDTR");
			/*==============================================================================================================*/      	
	      	$Default_qry		= "WHERE CompanyRefId = ".$CompanyID." AND BranchRefId = ".$BranchID;
	      	/*==============================================================================================================*/
	      	/*==========================================================================================================*/
			//GETTING OF ATTENDANCE IN EMPLOYEE ATTENDANCE
			/*==========================================================================================================*/
			if ($CompanyID == 14) {
				include 'inc/inc_biometrics_14.e2e.php';
			}
			$where_empAtt 			= $Default_qry;
			$where_empAtt 			.= " AND EmployeesRefId = '".$emprefid."'";
			$where_empAtt       	.= " AND AttendanceDate BETWEEN '".$month_start."' AND '".$month_end."'";
			$rs_empAtt 				= SelectEach("employeesattendance",$where_empAtt);
			if ($rs_empAtt) {
				while ($row = mysqli_fetch_assoc($rs_empAtt)) {
					$AttendanceDate 	= $row["AttendanceDate"];
					$AttendanceTime 	= $row["AttendanceTime"];
					$UTC 				= $row["CheckTime"];
					$KEntry         	= $row["KindOfEntry"];
					$fld 				= "";
					$val 				= "";
					switch ($KEntry) {
						case 1:
							$fld 		= "TimeIn";
							$val     	= get_today_minute($UTC);
							break;
						case 2:
							$fld 		= "LunchOut";
							$val     	= get_today_minute($UTC);
							break;
						case 3:
							$fld 		= "LunchIn";
							$val     	= get_today_minute($UTC);
							break;
						case 4:
							$fld 		= "TimeOut";
							$val     	= get_today_minute($UTC);
							break;
					}
					$arr["ARR"][$AttendanceDate][$fld] = $val;
					$arr["ARR"][$AttendanceDate]["UTC"] = $UTC;
				}
			}
			/*==========================================================================================================*/
			/*==========================================================================================================*/
			//GETTING ATTENDANCE IN BIOMETRICS
			/*==========================================================================================================*/
			switch ($CompanyID) {
				case '2':
					include 'inc/inc_biometrics_2.e2e.php';
					break;
				case '28':
					include 'inc/inc_biometrics_28.e2e.php';
					break;
				case '21':
					include 'inc/inc_biometrics_21.e2e.php';
					break;
				default:
					include 'inc/inc_bio_default.e2e.php';
					break;
			}
			/*==========================================================================================================*/
			/*==========================================================================================================*/
			//CHECKING HOLIDAY TABLE
			/*==========================================================================================================*/
			$holiday = SelectEach("holiday","");
		    if ($holiday) {
		        while ($row = mysqli_fetch_assoc($holiday)) {
		            $StartDate        = $row["StartDate"];
		            $EndDate          = $row["EndDate"];
		            $holiday_date_diff   = dateDifference($StartDate,$EndDate);
		            $Name             = $row["Name"];
		            $EveryYr          = $row["isApplyEveryYr"];
		            $Legal            = $row["isLegal"];
		            $temp_arr         = explode("-", $StartDate);
		            $temp_date        = $NewYear."-".$temp_arr[1]."-".$temp_arr[2];
		            for ($H=0; $H <= $holiday_date_diff; $H++) { 
	                  	$holiday_NewDate = date('Y-m-d', strtotime($temp_date. ' + '.$H.' days'));
                  		if ($EveryYr == 1) {
	                     	if(isset($arr["ARR"][$holiday_NewDate])) {
		                        $arr["ARR"][$holiday_NewDate]["Holiday"] = $Name;
	                    	}   
	                  	} else {
	                     	if(isset($arr["ARR"][$StartDate])) {
	                        	$arr["ARR"][$StartDate]["Holiday"] = $Name;
	                     	}
	                  	}
	               	}
		        }
		    }
		    /*==========================================================================================================*/
			/*==========================================================================================================*/
			//CHECKING OFFICE SUSPENSION TABLE
			/*==========================================================================================================*/
			$officesuspension = SelectEach("officesuspension","");
	      	if ($officesuspension) {
	         	while ($row = mysqli_fetch_assoc($officesuspension)) {
		            $StartDate        = $row["StartDate"];
	            	$EndDate          = $row["EndDate"];
	            	$OffSus_date_diff = dateDifference($StartDate,$EndDate);
	            	$Name             = $row["Name"];
	            	$OffSusTime       = $row["StartTime"];
	            	if ($OffSusTime == "") $OffSusTime = 420;
	            	$temp_arr         = explode("-", $StartDate);
	            	for ($OS=0; $OS <= $OffSus_date_diff; $OS++) { 
                  		$OffSus_NewDate = date('Y-m-d', strtotime($StartDate. ' + '.$OS.' days'));
                  		if(isset($arr["ARR"][$OffSus_NewDate])) {
                     		$arr["ARR"][$OffSus_NewDate]["OffSusName"] 	= $Name;
	                    	$arr["ARR"][$OffSus_NewDate]["OffSus"] 		= $OffSusTime;
                  		}   
               		}
	         	}
	      	}
	      	/*==========================================================================================================*/
			/*==========================================================================================================*/
			//GETTING EMPLOYEE LEAVE
			/*==========================================================================================================*/
			$where_leave 		= $Default_qry;
			$where_leave		.= " AND EmployeesRefId = ".$emprefid;
			$where_leave 		.= " AND Status = 'Approved'";
			$rs_leave			= SelectEach("employeesleave",$where_leave);
			if ($rs_leave) {
				while ($row = mysqli_fetch_assoc($rs_leave)) {
					$diff = dateDifference($row["ApplicationDateFrom"],$row["ApplicationDateTo"]);
					for ($i=0; $i <= $diff ; $i++) { 
						$temp_date = date('Y-m-d', strtotime($row["ApplicationDateFrom"] . ' +'.$i.' day'));
						if(isset($arr["ARR"][$temp_date])) {
	                     	$arr["ARR"][$temp_date]["Leave"] = $row["LeavesRefId"];
	                     	if ($row["isForceLeave"] == 1) {
	                     		$arr["ARR"][$temp_date]["FL"] = 1;
	                     	}
	                  	}
					}
				}
			}
			/*==========================================================================================================*/
			/*==========================================================================================================*/
			//GETTING OFFICE AUTHORITY NAME
			/*==========================================================================================================*/
			$where_OB 			= $Default_qry;
			$where_OB			.= " AND EmployeesRefId = ".$emprefid;
			$where_OB 			.= " AND Status = 'Approved'";
			$rs_OB				= SelectEach("employeesauthority",$where_OB);
			if ($rs_OB) {
				while ($row = mysqli_fetch_assoc($rs_OB)) {
					$diff = dateDifference($row["ApplicationDateFrom"],$row["ApplicationDateTo"]);
					$OBName = getRecord("absences",$row["AbsencesRefId"],"Name");
					$whole = $row["Whole"];
					$time_in = $row["FromTime"];
					$time_out = $row["ToTime"];
					for ($i=0; $i <= $diff ; $i++) { 
						$temp_date = date('Y-m-d', strtotime($row["ApplicationDateFrom"] . ' +'.$i.' day'));
						if(isset($arr["ARR"][$temp_date])) {
	                     	$arr["ARR"][$temp_date]["OBName"] = $OBName;
	                     	if ($whole == 1) {
	                     		$arr["ARR"][$temp_date]["OBTimeIn"] = "Whole";
	                     		$arr["ARR"][$temp_date]["OBTimeOut"] = "Whole";	
	                     	} else {
	                     		$arr["ARR"][$temp_date]["OBTimeIn"] = $time_in;
	                     		$arr["ARR"][$temp_date]["OBTimeOut"] = $time_out;	
	                     	}
	                     	
	                  	}
					}
				}
			}
			/*==========================================================================================================*/
			/*==========================================================================================================*/
			//GETTING EMPLOYEE CTO AVAILMENT
			/*==========================================================================================================*/
			$where_cto 		= $Default_qry;
			$where_cto		.= " AND EmployeesRefId = ".$emprefid;
			$where_cto 		.= " AND Status = 'Approved'";
			$rs_cto			= SelectEach("employeescto",$where_cto);
			if ($rs_cto) {
				while ($row = mysqli_fetch_assoc($rs_cto)) {
					$diff = dateDifference($row["ApplicationDateFrom"],$row["ApplicationDateTo"]);
					for ($i=0; $i <= $diff; $i++) { 
						$temp_date = date('Y-m-d', strtotime($row["ApplicationDateFrom"] . ' +'.$i.' day'));
						if(isset($arr["ARR"][$temp_date])) {
	                     	$arr["ARR"][$temp_date]["CTO"] = $row["Hours"]*60;
	                     	$dtr_process["COC_Used"] += intval($row["Hours"]*60);
	                  	}
					}
				}
			}
			/*==========================================================================================================*/
			/*==========================================================================================================*/
			//GETTING EMPLOYEE OVERTIME REQUEST
			/*==========================================================================================================*/
			$where_OT    = $Default_qry;
			$where_OT   .= " AND EmployeesRefId = ".$emprefid;
			$where_OT   .= " AND Status = 'Approved'";
			$rs_OT 		 = SelectEach("overtime_request",$where_OT);
			if ($rs_OT) {
				while ($row = mysqli_fetch_assoc($rs_OT)) {
					$StartDate 		= $row["StartDate"];
					$EndDate 		= $row["EndDate"];
					$WithPay        = $row["WithPay"];
					$OTFrom			= $row["FromTime"];
					$OTTo 			= $row["ToTime"];
					$OTClass 		= $row["OTClass"];
					$OTTime 		= "";
					if ($OTClass == 2) {
						if ($OTTo != "") {
							if ($OTTo > $OTFrom) {
								if ($OTTo >= 720 && $OTTo <= 780) {
									// $deduct_ot = $OTTo - 720;
									// $OTTo = $OTTo - $deduct_ot;
									$OTTo -= 60;
								}
								$OTTime = $OTTo - $OTFrom;
								if ($OTTime > 480) {
									$excess_ot_time = $OTTime - 480;
									for ($y=60; $y <= $excess_ot_time ; $y+=60) {
										if ($y == 180) {
											$dummy = $excess_ot_time - 180;
											if ($dummy > 60) {
												$excess_ot_time -= 60;	
											} else {
												$excess_ot_time -= $dummy;
											}
										}
									}
									$OTTime += $excess_ot_time;
								} else {
									if ($OTFrom < 720 && $OTTo > 780) {
										$OTTime -= 60;
									}	
								}
							} else {
								$OTTime = (1440 - $OTFrom) + $OTTo;
								for ($x=60; $x < $OTTime ; $x+=60) { 
									if ($x == 180) {
										$dummy = $OTTime - 180;
										if ($dummy > 60) {
											$OTTime -= 60;	
										} else {
											$OTTime -= $dummy;
										}
									}
								}
							}	
						}
					}

					$OT_DateDiff 	= dateDifference($StartDate,$EndDate);
					for ($O=0; $O <= $OT_DateDiff; $O++) { 
	               		$OT_NewDate = date('Y-m-d', strtotime($StartDate. ' + '.$O.' days'));
	               		if ($WithPay == 1) {
	               			$Pay = 1;
	               		} else {
	               			$Pay = 0;
	               		}
	               		if(isset($arr["ARR"][$OT_NewDate])) {
	                     	$arr["ARR"][$OT_NewDate]["HasOT"] = $Pay;
	                     	$arr["ARR"][$OT_NewDate]["OTTime"] = $OTTime;
	                  	}
	               	}
				}
			}




			$Emp_WorkSched 		= FindFirst("workschedule"," WHERE RefId = ".$workschedule,"*");

			if (intval($IsAuto) == 1) {
				$Monday_in 		= $Emp_WorkSched["MondayIn"];
				$Monday_lout 	= $Emp_WorkSched["MondayLBOut"];
				$Monday_lin 	= $Emp_WorkSched["MondayLBIn"];
				$Monday_out 	= $Emp_WorkSched["MondayOut"];
				for ($j=1; $j <= $num_of_days ; $j++) { 
					if ($j < 10) $j = "0".$j;
					$AutoDate = $NewYear."-".$NewMonth."-".$j;
					$Auto_day = date("D",strtotime($AutoDate));
					switch ($Auto_day) {
						case 'Mon':
							$Auto_day = "Monday";
							break;
						case 'Tue':
							$Auto_day = "Tuesday";
							break;
						case 'Wed':
							$Auto_day = "Wednesday";
							break;
						case 'Thu':
							$Auto_day = "Thursday";
							break;
						case 'Fri':
							$Auto_day = "Friday";
							break;
						case 'Sat':
							$Auto_day = "Saturday";
							break;
						case 'Sun':
							$Auto_day = "Sunday";
							break;
					}
					$auto_RestDay = $Auto_day."isRestDay";
					if ($Emp_WorkSched[$auto_RestDay] != 1) {
						$arr["ARR"][$AutoDate]["TimeIn"] = $Monday_in;
						$arr["ARR"][$AutoDate]["LunchOut"] = $Monday_lout;
						$arr["ARR"][$AutoDate]["LunchIn"] = $Monday_lin;
						$arr["ARR"][$AutoDate]["TimeOut"] = $Monday_out;	
					}
				}
			}

			foreach ($arr as $xvalue) {
				foreach ($xvalue as $xkey => $yvalue) {
					$xday_name = date("D",strtotime($yvalue['AttendanceDate']));
					
					if ($xday_name == "Fri") {
						if ($yvalue["Holiday"] != "") {
							$Friday_Holiday_PerWeek[$yvalue["Week"]] = 1;
						}
					}
				}
			}
			foreach ($arr as $nvalue) {
				foreach ($nvalue as $zvalue) {
					if (date("D",strtotime($zvalue['AttendanceDate'])) == "Mon") {
						$First_Monday = date("Y-m-d",strtotime($zvalue['AttendanceDate']));
						break;
					}
				}
			}
			foreach ($arr as $value) {
				foreach ($value as $key => $row) {
					if (strtotime($row["AttendanceDate"]) >= strtotime($month_start)) {
						$Remarks 					= "";
					   	$TardyHour 					= "";
					   	$UndertimeHour 				= "";
					   	$arr_TI						= "";
						$arr_OBO					= "";
						$arr_TO						= "";
						$arr_OBI					= "";
						$arr_Holiday				= "";
						$arr_leave 					= "";
						$arr_COC					= "";
						$arr_OffSus					= "";
						$arr_HasOT 					= "";
						$arr_OBName 				= "";
						$new_exc 					= 0;
						$invisible_time 			= 0;
						$MidUnderTime 				= 0;
						$day_name 					= date("D",strtotime($row['AttendanceDate']));
						$day_number 				= date("d",strtotime($row['AttendanceDate']));
						if ($day_name != "") {
							switch ($day_name) {
								case 'Mon':
									$day_name = "Monday";
									break;
								case 'Tue':
									$day_name = "Tuesday";
									break;
								case 'Wed':
									$day_name = "Wednesday";
									break;
								case 'Thu':
									$day_name = "Thursday";
									break;
								case 'Fri':
									$day_name = "Friday";
									break;
								case 'Sat':
									$day_name = "Saturday";
									break;
								case 'Sun':
									$day_name = "Sunday";
									break;
							}
						}	
						$day 						= $row["Day"]; 
						$day_in             	   	= $day_name."In";
						$day_out            	   	= $day_name."Out";
						$day_flexi          	   	= $day_name."FlexiTime";
						$day_LBOut					= $day_name."LBOut";
						$day_LBIn					= $day_name."LBIn";
						$day_RestDay				= $day_name."isRestDay";
						$day_isflexi          		= $day_name."isFlexi";


						$data_flexi             	= $Emp_WorkSched[$day_flexi];
						$data_isflexi           	= $Emp_WorkSched[$day_isflexi];
						$data_timein            	= $Emp_WorkSched[$day_in];
						$data_timeout 				= $Emp_WorkSched[$day_out];
						$data_LunchOut 				= $Emp_WorkSched[$day_LBOut];
						$data_LunchIn 				= $Emp_WorkSched[$day_LBIn];
						$data_RestDay      			= $Emp_WorkSched[$day_RestDay];
						$data_LunchTime 			= $data_LunchIn - $data_LunchOut;
						$day_work_hours_count   	= ($data_timeout - $data_timein) - ($data_LunchIn - $data_LunchOut);
						$Week 						= $row["Week"];	
						$AutoLB						= $Emp_WorkSched["AutoLB"];
						$PerDayHours            	= $day_work_hours_count / 60;
						
						
						

						$arr_TI						= $row["TimeIn"];
						$arr_OBO					= $row["OBOut"];
						$arr_TO						= $row["TimeOut"];
						$arr_OBI					= $row["OBIn"];
						$arr_Holiday				= $row["Holiday"];
						$arr_leave 					= $row["Leave"];
						$arr_COC					= $row["CTO"];
						$arr_OffSus					= $row["OffSus"];
						$arr_HasOT 					= $row["HasOT"];
						$arr_OBName					= $row["OBName"];
						$arr_OT						= $row["OTTime"];
						$arr_OBTimeIn 				= $row["OBTimeIn"];
						$arr_OBTimeOUt 				= $row["OBTimeOut"];
						if ($CompanyID == 35) {
							if ($First_Monday != "") {
								if ($row['AttendanceDate'] == $First_Monday) {
									$data_flexi 	= "510";
								}
							}
						}

						$core_time 					= $data_flexi + ($data_timeout - $data_timein);

						if ($Emp_WorkSched["ScheduleType"] == "Fi") {
							$data_flexi = $data_timein;
						} else {
							$data_flexi = $data_flexi;
						}

						

						if ($CompanyID == 1000) {
							if ($Friday_Holiday_PerWeek[$Week] == 1) {
								$data_LunchTime = 60;;
								$day_work_hours_count = 480;
								$data_timeout = 960;
							}							
						}

						if ($arr_COC != "") {
							$Used_COC = $Used_COC + $arr_COC;
						}


						if ($arr_TI >= $data_LunchIn && $data_RestDay != 1) {
							$arr_TI = $arr_TI - ($data_LunchIn - $data_LunchOut);
						}
						if ($arr_leave != "") {
							$arr_TI = $data_timein;
							$arr_TO = $data_timeout;
						}

						if ($arr_OT != "") {
							if ($arr_TI != "") {
								if ($arr_TI < $data_timein) $arr_TI = $data_timein;
								$arr_OT = ($arr_TO - $arr_TI) - 60;
							}
						}


						if ($arr_Holiday != "" && $arr_TI == "" && $arr_OT == "") {
							$arr_TI = $data_timein;
							$arr_TO = $data_timeout;
						}
						if ($arr_COC != "") {
							if ($arr_TI != "") {
								if ($arr_TI < $data_flexi) {

									if ($arr_COC >= $day_work_hours_count) {
										$arr_TI = $data_timein;
										$arr_TO = $data_timeout;
									} else {
										if ($arr_TI == "") {
											$arr_TI = $data_timein;
											$Lunch_Out = $data_LunchOut;
										} else {
											$arr_TI = $arr_TI - $arr_COC;
										}
										if ($arr_TO == "") {
											//$arr_TO = $data_timein + $arr_COC;
											$arr_TO = $data_LunchIn + $arr_COC;
										} else {
											$arr_TO = $arr_TO + $arr_COC;
											
										}
									}
								} else {
									$arr_TI = $arr_TI - $arr_COC;
									if ($CompanyID == 1000) {
										$arr_TI = $data_timein;
									}
								}	
							} else {
								if ($arr_COC == $day_work_hours_count) {
									$arr_TI = $data_timein;
									$arr_TO = $data_timeout;	
								} else {
									$arr_TI 	= $data_timein;
									$arr_TO 	= $arr_TI + $arr_COC;
									$Lunch_Out 	= $data_LunchOut;
									$Lunch_In 	= $data_LunchIn;
								}
							}
						}


						if ($arr_TI != "" && $data_RestDay != 1) {
							$PresentDays++;
						}
						/*===================================================================================================*/
						/*===================================================================================================*/
						//AUTO LUNCH BREAK
						/*===================================================================================================*/
						if ($AutoLB == 1) {
							if ($arr_TI != "") {
								$Lunch_Out = $data_LunchOut;
								$Lunch_In = $data_LunchIn;
							} else {
								$Lunch_Out = "";
								$Lunch_In = "";
							}
						} else {
							$Lunch_Out = $row["LunchOut"];
							$Lunch_In = $row["LunchIn"];
						}
						/*===================================================================================================*/
						/*===================================================================================================*/
						//MAXIMUM TIME FROM FOR EMPLOYEE TO ENTER WITHOUT LATE
						/*===================================================================================================*/

						if ($arr_OBName != "") {
							if (is_numeric($arr_OBTimeIn)) {
								if ($arr_TI != "") {
									if ($arr_TI >= $arr_OBTimeIn) {
										$arr_TI = $arr_OBTimeIn;
									}	
								} else {
									$arr_TI = $data_timein;
								}
								if ($arr_TO != "") {
									if ($arr_TO <= $arr_OBTimeOUt) {
										$arr_TO = $arr_OBTimeOUt;
									}	
								} else {
									$arr_TO = $data_timeout;
								}
							} else {
								if ($TardyHour != "") {
									$arr_TO = $data_timeout;
								} else {
									$arr_TI = $data_timein;
									$arr_TO = $data_timeout;
								}	
							}
						}
						
						/*===================================================================================================*/
						/*===================================================================================================*/
						//GETTING LATE
						/*===================================================================================================*/
						if ($data_RestDay != 1 && $arr_Holiday == "") {
							if ($arr_OT != "" && $arr_OT != "0") {
								$arr_TO = $arr_TO + $arr_OT;
							}
							if ($arr_TI != "") {
								if (($arr_TI + $data_LunchTime) > $data_LunchIn) {
									if ($arr_TO > $core_time) {
										$AfternoonIN 				= $core_time - $arr_TI;
									} else {
										$AfternoonIN 				= intval($arr_TO) - intval($arr_TI);	
									}
									if ($arr_TI > $data_LunchOut) {
										$TardyHour 						= $day_work_hours_count - $AfternoonIN;
										$TardyHour 						= $data_LunchOut - $data_flexi;
									} else {
										$TardyHour 						= $day_work_hours_count - $AfternoonIN + $data_LunchTime;
									}
									
									if ($CompanyID == "28") {
										$TardyHour 					= $arr_TI - $data_flexi;
										$TardyHour 					= $TardyHour + 15;	
									}
									$TardyHour_PerWeek[$Week] 	+= $TardyHour;
									$TardyCount_PerWeek[$Week]++;

								} else {
									if ($data_isflexi != 1) {
										if ($arr_TI >= $data_timein) {
											$TardyHour 					= "";
										} else {
											$TardyHour 					= ($arr_TI - $data_flexi);
											$TardyHour_PerWeek[$Week] 	= $TardyHour_PerWeek[$Week] + $TardyHour;
											$TardyCount_PerWeek[$Week]++;
										}
									} else {
										if ($arr_TI <= $data_flexi) {
											$TardyHour = "";
										} else {
											$TardyHour 					= ($arr_TI - $data_flexi);
											$TardyHour_PerWeek[$Week] 	= $TardyHour_PerWeek[$Week] + $TardyHour;
											$TardyCount_PerWeek[$Week]++;
										}
									}	
								}
							}	
						}
						if ($arr_OffSus != "") {
							if ($arr_TI != "") {
								if ($arr_OffSus == "420") {
									$arr_TI = $data_timein;
									$arr_TO = $data_timeout;
									$TardyHour = 0;
									if ($CompanyID == 1000) {
										if ($Friday_Holiday_PerWeek[$Week] == 1) {
											$arr_TI = $data_timein;
											$arr_TO = "960";
										}
									}
								} else {
									if ($TardyHour != "") {
										$arr_TO = $core_time;
									} else {
										if ($arr_TO != "") {
											if ($arr_TO <= $arr_OffSus) {
												if (($arr_TO - $arr_TI) <= $day_work_hours_count) {
													if ($arr_TO <= $arr_OffSus) {
														$MidUnderTime = intval($arr_OffSus) - intval($arr_TO);	
														$arr_TO = $data_timeout - $MidUnderTime;
													}
												}	
											} else {
												if ($arr_TI < $data_timein) $arr_TI = $data_timein;
												$arr_TO = $arr_TI + $day_work_hours_count + $data_LunchTime;
											}
										}
									}
									if ($CompanyID == 1000) {
										if ($Friday_Holiday_PerWeek[$Week] == 1) {
											$arr_TI = $data_timein;
											$arr_TO = "960";
										}							
									}	
								}
							} else {
								if ($arr_OffSus == "420") {
									$arr_TI = $data_timein;
									$arr_TO = $data_timeout;
									$TardyHour = 0;
								} else {
									if ($CompanyID == "1000") {
										if ($arr_TI == "") {
											if ($arr_OffSus > 780) {
												$TardyHour = ($arr_OffSus - $data_timein) - $data_LunchTime;
											} else {
												$TardyHour = $day_work_hours_count / 2;	
											}
											$TardyHour_PerWeek[$Week] 	= $TardyHour_PerWeek[$Week] + $TardyHour;
											$TardyCount_PerWeek[$Week]++;
											$CutOff_PerWeek[$Week] += $TardyHour;
										}
									}
								}
							}
						}
						
						
						
						/*===================================================================================================*/
						/*===================================================================================================*/
						//GETTING THE WORKING AND EXCESS HOURS OF THE EMPLOYEE
						/*===================================================================================================*/
						if ($arr_TO != "" && $arr_TI != "" && $data_RestDay != 1) {
							if ($arr_TI <= $data_timein) {
								$arr_TI = $data_timein;
							}
							if ($TardyHour != "" && $TardyHour != "0") {
								if ($arr_TO > $core_time) {
									$arr_TO = $core_time;
								}
							}

							if ($arr_TO == $data_LunchOut) {
								$consume_time 	= ($arr_TO - $arr_TI);
							} else {
								$consume_time 	= (($arr_TO - $arr_TI) - $data_LunchTime);	
							}
							$excess_time 	= $consume_time - $day_work_hours_count; 	
						} else {
							$excess_time = 0;
							$consume_time = 0;
						}
						/*===================================================================================================*/
						/*===================================================================================================*/
						//GETTING THE UNDERTIME AND EXCESS HOURS OF THE EMPLOYEE
						/*===================================================================================================*/

						if ($arr_Holiday == "") {
							if ($consume_time >= $day_work_hours_count) {
								if ($day_work_hours_count != "") {
									$RegHour_PerWeek[$Week] = $RegHour_PerWeek[$Week] + $day_work_hours_count;
								}

								$consume_time = $day_work_hours_count;
							} else {
								if ($arr_TO != "" && $arr_TI != "" && $data_RestDay != 1) {
									$excess_time = 0;
									if ($TardyHour != "") {
										$UndertimeHour = $core_time - $arr_TO;
									} else {
										$UndertimeHour = $day_work_hours_count - $consume_time;	
									}
									if ($CompanyID == 2 || 
										$CompanyID == 28) {
										if ($arr_TO >= $data_timeout) {
											$UndertimeHour = 0;
										} else {
											$UndertimeHour = $data_timeout - $arr_TO;	
										}	
									} else if ($CompanyID == 1000) {
										if ($arr_TO >= "960") {
											$UndertimeHour = 0;
										} else {
											$UndertimeHour = "960" - $arr_TO;	
										}
									}
									if ($UndertimeHour != 0 && $arr_Holiday == "") {
										$UndertimeHour_PerWeek[$Week] 	= $UndertimeHour_PerWeek[$Week] + $UndertimeHour;
										$UndertimeCount_PerWeek[$Week]++;	
									}
								} else {
									$excess_time = 0;
								}
								if ($consume_time != 0) {
									$RegHour_PerWeek[$Week] = $RegHour_PerWeek[$Week] + $consume_time;
								}
							}	
						}
						
						if ($arr_TI != "") {
							if ($UndertimeHour == "") {
								$full_time = $arr_TI + $day_work_hours_count + (intval($data_LunchTime));
								if ($arr_TO >= $full_time) {
									if ($arr_TO > $core_time) {
										$exc = $arr_TO - $full_time;
										$new_exc = $core_time - $full_time;
										$invisible_time = $arr_TO - $core_time;
										$excess_time = $new_exc;
									}	
								}
							}
						}
						if ($arr_TO != "" && $data_RestDay != 1 && $arr_Holiday == "") {
							$CutOff_PerWeek[$Week] 		= $CutOff_PerWeek[$Week] + $day_work_hours_count;
						}

						
						/*===================================================================================================*/
						/*===================================================================================================*/
						/*===================================================================================================*/
						/*===================================================================================================*/
						/*===================================================================================================*/
						/*===================================================================================================*/
						//MAPPING
						/*===================================================================================================*/
						if ($TardyHour == "") {
							if ($arr_Holiday == "" && $data_RestDay != 1) {
								if ($arr_HasOT == "1") {
									if (intval($arr_OT) > 0) {
										$excess_time = $arr_OT;
									} else {
										$excess_time = $excess_time + $invisible_time;	
									}
									if ($excess_time >= $OTMinTime) {
										if ($excess_time >= 180) {
											for ($v=60; $v <= $excess_time ; $v+=60) { 
												if ($v == 180) {
													$dummy = $excess_time - 180;
													if ($dummy > 60) {
														$excess_time -= 60;	
													} else {
														$excess_time -= $dummy;
													}
												}
											}
										}
										if ($CompanyID == "1000") {
											if ($Friday_Holiday_PerWeek[$Week] == 1) {
												$excess_time = $excess_time - $data_LunchTime;
											}
										}
										$OTHours_PerWeek[$Week] 			= $OTHours_PerWeek[$Week] + $excess_time;
										$Overtime_Remarks 					.= "OT_".$row["AttendanceDate"];
										$Overtime_Remarks 					.= "_".intval($arr_TI)."_".intval($Lunch_Out);
										$Overtime_Remarks 					.= "_".intval($Lunch_In)."_".intval($arr_TO);
										$Overtime_Remarks 					.= "_".$OTHours_PerWeek[$Week]."|";
									} else {
										
										$ExcessHour_PerWeek[$Week] 			= $ExcessHour_PerWeek[$Week] + $excess_time;
									}
								} else if ($arr_HasOT == "0") {
									if (intval($arr_OT) > 0) {
										$excess_time = $arr_OT;
									} else {
										$excess_time = $excess_time + $invisible_time;	
									}
									if ($excess_time >= $COCMinTime) {
										if ($excess_time >= 180) {
											for ($w=60; $w <= $excess_time ; $w+=60) { 
												if ($w == 180) {
													$dummy = $excess_time - 180;
													if ($dummy > 60) {
														$excess_time -= 60;	
													} else {
														$excess_time -= $dummy;
													}
												}
											}	
										}
										if ($CompanyID == "1000") {
											if ($Friday_Holiday_PerWeek[$Week] == 1) {
												$excess_time = $excess_time - $data_LunchTime;
											}
										}
										$COCHours_PerWeek[$Week] 			= $COCHours_PerWeek[$Week] + $excess_time;
										$COC_Str 							.= "COC_".$day_number."_".$COCHours_PerWeek[$Week]."|";
										$Overtime_Remarks 					.= "COC_".$row["AttendanceDate"];
										$Overtime_Remarks 					.= "_".intval($arr_TI)."_".intval($Lunch_Out);
										$Overtime_Remarks 					.= "_".intval($Lunch_In)."_".intval($arr_TO);
										$Overtime_Remarks 					.= "_".$COCHours_PerWeek[$Week]."|";
									} else {
										$ExcessHour_PerWeek[$Week] 			= $ExcessHour_PerWeek[$Week] + $excess_time;
									}
								} else {
									$ExcessHour_PerWeek[$Week] 				= $ExcessHour_PerWeek[$Week] + $excess_time;
								}	
							}
						} else {
							$ExcessHour_PerWeek[$Week] 					= intval($ExcessHour_PerWeek[$Week]) + intval($excess_time);
						}
			            if ($row["Leave"] != "") {
							if ($data_RestDay != 1) {
								if ($arr_Holiday == "") {
									$half_leave 	= 0;
									$half_leave_eq 	= 0;
									if ($arr_OffSus != "") {
										if ($arr_OffSus <= 1020) {
											$half_leave = $arr_OffSus - 480;
											if ($arr_OffSus >= 780) {
												$half_leave -= 60;
											}
										}
										if ($half_leave > 0) {
											$half_leave_eq = getEquivalent($half_leave,"workinghrsconversion");
										}
									}
									$leave_row = FindFirst("leaves","WHERE RefId = '".$row["Leave"]."'","*");
					              	if ($row["FL"] == 1) {
					              		$FL_count++;
					              		$Days_FL .= $day."|";
					              	}
					              	switch ($leave_row["Code"]) {
					              		case 'SPL':
					              			$SPL_Count++;
					              			$Days_SPL .= $day."|";
					              			break;
					              		case 'VL':
					              			if ($half_leave_eq == 0) {
					              				$VL_Count++;	
					              			} else {
					              				$VL_Count += $half_leave_eq;
					              			}
					              			
					              			$Days_VL .= $day."|";
					              			break;
					              		case 'SL':
											if ($Half_SL > 0) {
												$Half_SL_EQ = getEquivalent($Half_SL,"workinghrsconversion");
												$SL_Count += $Half_SL_EQ;
											} else {
												if ($half_leave_eq == 0) {
													$SL_Count++;		
												} else {
													$SL_Count += $half_leave_eq;
												}
												
											}
					              			$Days_SL .= $day."|";
					              			break;
					              	}
					            }
							}
			            }
						if ($data_RestDay == 1) {
							if ($arr_HasOT == "0" || $arr_HasOT == 1) {
								if ($arr_OT != "") {
									if ($arr_HasOT == "1") {
										$OTHours_PerWeek[$Week] 	= $OTHours_PerWeek[$Week] + ($arr_OT * 1.5);
										$Overtime_Remarks 			.= "OT_".$row["AttendanceDate"];
										$Overtime_Remarks 			.= "_".intval($arr_TI)."_".intval($Lunch_Out);
										$Overtime_Remarks 			.= "_".intval($Lunch_In)."_".intval($arr_TO);
										$Overtime_Remarks 			.= "_".$OTHours_PerWeek[$Week]."|";
									} else {
										$COCHours_PerWeek[$Week]   	= $COCHours_PerWeek[$Week] + ($arr_OT * 1.5);
										$COC_Str 					.= "COC_".$day_number."_".$COCHours_PerWeek[$Week]."|";
										$Overtime_Remarks 			.= "COC_".$row["AttendanceDate"];
										$Overtime_Remarks 			.= "_".intval($arr_TI)."_".intval($Lunch_Out);
										$Overtime_Remarks 			.= "_".intval($Lunch_In)."_".intval($arr_TO);
										$Overtime_Remarks 			.= "_".$COCHours_PerWeek[$Week]."|";
									}	
								} else {
									if ($arr_TI != "" && $row["Leave"] == "" && $arr_OBName == "" && $arr_COC == "") {
										if ($CompanyID == "1000") {
											if ($arr_TI <= $data_timein) {
												$arr_TI = $data_timein;
											}
											if ($arr_TO >= $data_LunchOut && $arr_TO <= $data_LunchIn) {
												$arr_TO = $data_LunchOut;
											}
											$weekend_consume_time = (intval($arr_TO) - intval($arr_TI));
											if ($weekend_consume_time > $day_work_hours_count) {
												for ($y=60; $y <= $weekend_consume_time ; $y+=60) {
													if ($y == 180) {
														$dummy = $weekend_consume_time - 180;
														if ($dummy > 60) {
															$weekend_consume_time -= 60;	
														} else {
															$weekend_consume_time -= $dummy;
														}
													}
												}	
											}
											
											
										} else {
											$weekend_consume_time = (intval($arr_TO) - intval($arr_TI));
											if ($weekend_consume_time >= $day_work_hours_count) {
												$weekend_consume_time = $weekend_consume_time - 60;
											} else {
												$weekend_consume_time = $weekend_consume_time + 60;
											}
										}
										$weekend_excess_time = $weekend_consume_time - $day_work_hours_count;
										if ($weekend_consume_time > $day_work_hours_count) {
											for ($z=60; $z <= $weekend_excess_time ; $z+=60) { 
												if ($z == 180) {
													$new_dummy = $weekend_consume_time - 180;
													if ($new_dummy > 60) {
														$weekend_consume_time -= 60;	
													} else {
														$weekend_consume_time -= $new_dummy;
													}
												}
											}
										}
										if ($arr_HasOT == "1") {
											$OTHours_PerWeek[$Week] 	= $OTHours_PerWeek[$Week] + ($weekend_consume_time * 1.5);
											$Overtime_Remarks 			.= "OT_".$row["AttendanceDate"];
											$Overtime_Remarks 			.= "_".intval($arr_TI)."_".intval($Lunch_Out);
											$Overtime_Remarks 			.= "_".intval($Lunch_In)."_".intval($arr_TO);
											$Overtime_Remarks 			.= "_".$OTHours_PerWeek[$Week]."|";
										} else {
											$COCHours_PerWeek[$Week]   	= $COCHours_PerWeek[$Week] + ($weekend_consume_time * 1.5);
											$COC_Str 					.= "COC_".$day_number."_".$COCHours_PerWeek[$Week]."|";
											$Overtime_Remarks 			.= "COC_".$row["AttendanceDate"];
											$Overtime_Remarks 			.= "_".intval($arr_TI)."_".intval($Lunch_Out);
											$Overtime_Remarks 			.= "_".intval($Lunch_In)."_".intval($arr_TO);
											$Overtime_Remarks 			.= "_".$COCHours_PerWeek[$Week]."|";
										}
									}
								}
							}
						} else {
							if ($arr_Holiday != "") {
								if ($arr_HasOT == "0" or $arr_HasOT == 1) {
									if ($arr_OT == "") {
										if ($CompanyID == "1000") {
											if ($arr_TI <= $data_timein) {
												$arr_TI = $data_timein;
											}	
										}	
										$arr_OT = (intval($arr_TO) - intval($arr_TI));
										if ($arr_OT > $day_work_hours_count) {
											for ($x=60; $x < $arr_OT ; $x+=60) { 
												if ($x == 180) {
													$dummy = $arr_OT - 180;
													if ($dummy > 60) {
														$arr_OT -= 60;	
													} else {
														$arr_OT -= $dummy;
													}
												}
											}	
										}
										if ($arr_TO >= $data_LunchOut) {
											$arr_OT = $arr_OT - 60;
										}
									}
									if ($arr_HasOT == "1") {
										$OTHours_PerWeek[$Week] 	= $OTHours_PerWeek[$Week] + ($arr_OT * 1.5);
									} else {
										$COCHours_PerWeek[$Week]   	= $COCHours_PerWeek[$Week] + ($arr_OT * 1.5);
										$COC_Str 					.= "COC_".$day_number."_".$COCHours_PerWeek[$Week]."|";
									}	
								}
							}
						}
						if ($arr_TI == "" && $data_RestDay != 1 && $arr_OffSus == "") {
							$AbsentCount_PerWeek[$Week]++;
							$Days_Absent .= $day."|";
							if (isset($dtr_process[$day_number."_Absent"])) {
								$dtr_process[$day_number."_Absent"] = 1;
							}
						} else {
							if (isset($dtr_process[$day_number."_Absent"])) {
								$dtr_process[$day_number."_Absent"] = 0;
							}
						}
						if (isset($dtr_process[$day_number."_Late"])) {
							$dtr_process[$day_number."_Late"] = intval($TardyHour);
						}
						if (isset($dtr_process[$day_number."_UT"])) {
							$dtr_process[$day_number."_UT"] = intval($UndertimeHour);
						}
					}
				}
			}
			for ($i=1; $i <= 6; $i++) { 
				if ($ExcessHour_PerWeek[$i] < 0) {
					$ExcessHour_PerWeek[$i] = 0;
				}

				if ($CompanyID == "1000") {	
					$RegHour_PerWeek[$i] = $RegHour_PerWeek[$i] + $ExcessHour_PerWeek[$i];
					if ($CutOff_PerWeek[$i] >= ($RegHour_PerWeek[$i] + $TardyHour_PerWeek[$i])) {
						$UndertimeHour_PerWeek[$i] = $CutOff_PerWeek[$i] - ($RegHour_PerWeek[$i] + $TardyHour_PerWeek[$i]);
					}
				}
				if ($CompanyID == '1000') {
					$TotalCOC_OTHour = $OTHours_PerWeek[$i] + $COCHours_PerWeek[$i];
					if ($TotalCOC_OTHour > 0) {
						if ($TotalCOC_OTHour > $UndertimeHour_PerWeek[$i]) {
							$TotalCOC_OTHour -= $UndertimeHour_PerWeek[$i];
							$RegHour_PerWeek[$i] += $UndertimeHour_PerWeek[$i];
							$COCHours_PerWeek[$i] -= $UndertimeHour_PerWeek[$i];
							$UndertimeHour_PerWeek[$i] = 0;
						}
					}
				}
			}
			for ($a=1; $a <= 6 ; $a++) { 
				$TotalRegHour 			= intval($TotalRegHour) + $RegHour_PerWeek[$a];
				$TotalTardyHour 		= intval($TotalTardyHour) + $TardyHour_PerWeek[$a];
				$TotalUndertimeHour 	= intval($TotalUndertimeHour) + $UndertimeHour_PerWeek[$a]; 
				$TotalExcessHour    	= intval($TotalExcessHour) + $ExcessHour_PerWeek[$a];
				$TotalCOCHour       	= intval($TotalCOCHour) + $COCHours_PerWeek[$a];
				$TotalTardyCount    	= intval($TotalTardyCount) + $TardyCount_PerWeek[$a];
				$TotalUndertimeCount 	= intval($TotalUndertimeCount) + $UndertimeCount_PerWeek[$a]; 
				$TotalAbsentCount  		= intval($TotalAbsentCount) + $AbsentCount_PerWeek[$a];
				$TotalOTHour       		= intval($TotalOTHour) + $OTHours_PerWeek[$a];
			}
			if ($exact_end_days == $num_of_days) {
				$max_day = 30;
			} else {
				switch ($exact_end_days) {
					case '31':
						$max_day = $num_of_days - 1;
						break;
					case '28':
						$max_day = $num_of_days + 2;
						break;
					case '29':
						$max_day = $num_of_days + 1;
						break;
					default:
						$max_day = $num_of_days;
						break;
				}
			}
			
			$Total_Days 	= $max_day - $TotalAbsentCount;
			if ($Late_Employee == 1) {
		   		$Total_Days = $PresentDays;
		   		if ($CompanyID == "1000") $Total_Days = $Total_Days * "1.25";
		   	}

		  	$day_eq_vl 		= FindFirst("slvlearneddaily","WHERE NoOfDays = ".$Total_Days,"VLEarned");
			$day_eq_sl 		= FindFirst("slvlearneddaily","WHERE NoOfDays = ".$Total_Days,"SLEarned");

			if ($exact_end_days == $num_of_days) {
				if ($CompanyID == "14" || $CompanyID == "28") {
		   			$day_eq_vl = "1.250";
		   			$day_eq_sl = "1.250";
		   		}
			}


			$Setting_json        = file_get_contents(json."Settings_".$CompanyID.".json");
		   	$Setting_arr         = json_decode($Setting_json, true);
		   	$PerDayHours         = $Setting_arr["WorkingHrs"];


		   	if (!isset($PerDayHours)) $PerDayHours = 8;
			if (isset($PerDayHours)) {
	            for ($i=1; $i <= $Total_Days; $i++) { 
	               $Total_Days_EQ = $WorkDayEQ["WorkDayConversion"][$PerDayHours] + $Total_Days_EQ;
	            }
	      	}
	      	if ($PerDayHours != "") {
	            for ($i=1; $i <= $TotalAbsentCount; $i++) { 
	               $Total_Absent_EQ = $WorkDayEQ["WorkDayConversion"][$PerDayHours] + $Total_Absent_EQ;
	            }
	     	}

	      	$TardyEQ 			= getEquivalent($TotalTardyHour,"workinghrsconversion");
	   		$UndertimeEQ		= getEquivalent($TotalUndertimeHour,"workinghrsconversion");
	   		$TotalRegEQ			= getEquivalent($TotalRegHour,"workinghrsconversion");


	     	$dtr_process["EmployeesRefId"] 			= $emprefid;
			$dtr_process["WorkScheduleRefId"] 		= $workschedule;
			$dtr_process["Month"] 					= $month;
			$dtr_process["Year"] 					= $year;
			$dtr_process["Total_Regular_Hr"] 		= $TotalRegHour;
			$dtr_process["Total_Excess_Hr"] 		= $TotalExcessHour;
			$dtr_process["Total_COC_Hr"] 			= $TotalCOCHour;
			$dtr_process["Total_OT_Hr"] 			= $TotalOTHour;
			$dtr_process["Total_Tardy_Hr"] 			= $TotalTardyHour;
			$dtr_process["Total_Undertime_Hr"] 		= $TotalUndertimeHour;
			$dtr_process["Total_Absent_Count"] 		= $TotalAbsentCount;
			$dtr_process["Total_Present_Count"] 	= $PresentDays;
			$dtr_process["Total_Tardy_Count"] 		= $TotalTardyCount;
			$dtr_process["Total_Undertime_Count"] 	= $TotalUndertimeCount;
			$dtr_process["Days_Absent"] 			= $Days_Absent;
			$dtr_process["Days_Tardy"] 				= $Days_Tardy;
			$dtr_process["Days_Undertime"] 			= $Days_Undertime;
			$dtr_process["VL_Earned"] 				= $day_eq_vl;
			$dtr_process["SL_Earned"] 				= $day_eq_sl;
			$dtr_process["Total_Present_Count"] 	= $PresentDays;
			$dtr_process["SPL_Used"] 				= $SPL_Count;
			$dtr_process["VL_Used"] 				= $VL_Count;
			$dtr_process["SL_Used"] 				= $SL_Count;
			$dtr_process["Tardy_Deduction_EQ"] 		= $TardyEQ;
			$dtr_process["Undertime_Deduction_EQ"] 	= $UndertimeEQ;
			$dtr_process["Regular_Hr_EQ"] 			= $TotalRegEQ;
			$dtr_process["Total_Days_EQ"] 			= $Total_Days_EQ;
			$dtr_process["Total_Absent_EQ"] 		= $Total_Absent_EQ;	
			$dtr_process["VL_Days"] 				= $Days_VL;	
			$dtr_process["FL_Days"] 				= $Days_FL;	
			$dtr_process["SL_Days"] 				= $Days_SL;	
			$dtr_process["SPL_Days"] 				= $Days_SPL;	
			$dtr_process["Overtime_Remarks"] 		= $Overtime_Remarks;

			return $dtr_process;	
		}
	}

	function saveDTRSummary($emprefid,$month,$year){
		$workschedule 	= FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","WorkScheduleRefId");
		$error 			= 0;
		$refid 			= 0;
		if ($workschedule) {
			$array 			= fullDTR($emprefid,$month,$year,$workschedule);
			$check_where 	= "WHERE EmployeesRefId = '$emprefid' AND Month = '$month' AND Year = '$year'";
			$checking 		= FindFirst("dtr_process",$check_where,"RefId");
			$Fields 		= "";
			$Values 		= "";
			$FldnVal 		= "";
			if ($array) {
				foreach ($array as $key => $value) {
					$Fields .= "`$key`, ";
					$Values .= "'$value', ";
					$FldnVal .= "`$key` = '$value', ";
				}
				if ($checking) {
					$result = f_SaveRecord("EDITSAVE","dtr_process",$FldnVal,$checking);
					if ($result != "") {
						echo "Error -> $checking -> ".$FldnVal;
						$error++;
					} else {
						$error = 0;
						$refid = $checking;
					}
				} else {
					$save   = f_SaveRecord("NEWSAVE","dtr_process",$Fields,$Values);
					if (!is_numeric($save)) {
						echo "Error -> $Fields -> $Values";
						$error++;
					} else {
						$error = 0;
						$refid = $save;
					}
				}		
			}
			

			if ($error == 0) return $refid;
		} else {
			return "Error";
		}
	}
?>