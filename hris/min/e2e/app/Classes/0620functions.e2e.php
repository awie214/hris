<?php
   date_default_timezone_set("Asia/Manila");
   $WhatScreen = "";
   $path = "../../../public/";

   $user = "Err";
   if (isset($_GET["hUser"]))
      $user = $_GET["hUser"];
   else if (isset($_POST["hUser"]))
      $user = $_POST["hUser"];

   

      if (!function_exists('getvalue')) {
         function getvalue($idx) {
            $retval = "";
            if (isset($_GET[$idx]))
               $retval = $_GET[$idx];
            else if (isset($_POST[$idx]))
               $retval = $_POST[$idx];

            if ($retval!="") return $retval;
            else $retval;
         }
      }

      if (!function_exists('f_encode')) {
         function f_encode($str) {
            $str = $str . "_" . $str . "_az";
            return base64_encode($str);
         }
      }

      if (!function_exists('f_SaveRecord')) {
         function f_SaveRecord($mode,$tables,$flds,$values,$loc_user = "No User Assigned") {
            include "conn.e2e.php";
            $t             = time();
            $msg           = "";
            $date_today    = date("Y-m-d",$t);
            $curr_time     = date("H:i:s",$t);
            $saveRecord    = fopen(path("syslog/SaveRecord_".date("Ymd",$t).".log"), 'a+');
            if (isset($GLOBALS["user"])) {
               $loc_user = $GLOBALS["user"];
            } else {
               if ($tables == "newsignup") {
                  $loc_user = $tables;
               }
            }
            if (getvalue("hUser") != "") {
               $loc_user = getvalue("hUser");
            }
            if ($loc_user != "") {
               if ($mode == "NEWSAVE") {
                     $trackingA_fld = "";
                     $trackingA_val = "";

                     $sql = "SHOW FULL COLUMNS FROM ".strtolower($tables);
                     $Colresult = mysqli_query($conn,$sql) or die(mysqli_error($conn));
                     if ($Colresult) {
                        while($row = mysqli_fetch_array($Colresult))
                        {
                           if ($row['Field'] == "CompanyRefId" || $row['Field'] == "BranchRefId") {
                              $rowCompany = FindFirst("company","","*");
                              if ($rowCompany) {
                                 $CompanyId     = $rowCompany["RefId"];
                                 $BranchId      = $rowCompany["BranchRefId"];
                                 $CompCode      = $rowCompany["Code"];
                                 $trackingA_fld = "`CompanyRefId`, `BranchRefId`, ";
                                 $trackingA_val = "$CompanyId, $BranchId, ";
                              }
                              break;
                           }
                        }
                     } else {
                        $msg .= "System Error $Colresult";
                     }

                     $trackingA_fld .= "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
                     $trackingA_val .= "'$date_today', '$curr_time', '$loc_user', 'A'";
                     $flds   = $flds.$trackingA_fld;
                     $rawValues = explode(",",$values);
                     $values = $values.$trackingA_val;
                     $sql = "INSERT INTO `".strtolower($tables)."` ($flds) VALUES ($values)";
                     if ($conn->query($sql) === TRUE) {
                        return mysqli_insert_id($conn);
                     } else {
                        $msg .= " Saving Error: ".$conn->error;
                        if (isset($_SESSION['debugging'])) {
                           if ($_SESSION['debugging']) {
                              $msg .= " --- ".$sql;
                           }
                        }
                        fwrite($saveRecord,$msg."\n");
                        return $msg;

                     }
               }
               else if ($mode == "EDITSAVE") {
                  $js = "";
                  $dbtable = $tables;
                  $criteria  = " WHERE RefId = $values";
                  $criteria .= " LIMIT 1";

                  $recordSet = f_Find($dbtable,$criteria);
                  $rowcount = mysqli_num_rows($recordSet);
                  if ($rowcount) {
                     $tableRS   = array();
                     $tableRS   = mysqli_fetch_assoc($recordSet);
                     $fldsNVal = realEscape(json_encode($tableRS));

                     $MAC = getMAC();
                     $trnLogvals = "'$dbtable','','$fldsNVal','E',";
                     $trnLogflds = "`TableName`,`FldnVal`, `TrnType`,";
                     //$SaveSuccessfull = f_SaveRecord("NEWSAVE","trnlog",$trnLogflds,$trnLogvals);
                     $SaveSuccessfull = 1;
                     if (is_numeric($SaveSuccessfull)) {
                        $trackingE = "";
                        $trackingE  = "`LastUpdateDate` = '$date_today',";
                        $trackingE .= "`LastUpdateTime` = '$curr_time',";
                        $trackingE .= "`LastUpdateBy` = '$loc_user',";
                        $trackingE .= "`Data`= 'E'";
                        $fieldnvalue = $flds.$trackingE;
                        $refid       = $values;
                        $qry_update = "UPDATE `".strtolower($tables)."` SET $fieldnvalue WHERE RefId = $refid";

                        if ($conn->query($qry_update) === TRUE) {
                           return "";
                        } else {
                           $msg .= " Saving Error: ".$conn->error;
                           if (isset($_SESSION['debugging'])) {
                              if ($_SESSION['debugging']) {
                                 $msg .= " --- ".$qry_update;
                              }
                           }
                           fwrite($saveRecord,$msg."\n");
                           return $msg;
                        }
                     }
                  }
               }
            } else {
               $msg = "Error: No User Assigned";
               echo "<li>Error: No User Assigned";
               return $msg;
            }
            fclose($saveRecord);
            mysqli_close($conn);
         }
      }
      if (!function_exists('f_Find')) {
         function f_Find($tables,$criteria) { // FOR EACH
            include 'conn.e2e.php';
            $sql      = "SELECT * FROM ".strtolower($tables)." ".$criteria;
            $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
            mysqli_close($conn);
            if (mysqli_num_rows($result) > 0) {
               return $result;
            } else {
               return false;
            }
         }
      }
      if (!function_exists('getRecordSet')) {
         function getRecordSet($table,$id) {
            $row = "";
            if ($id != null) {
               include 'conn.e2e.php';
               $sql = "SELECT * FROM ".strtolower($table)." WHERE RefId = $id LIMIT 1";
               $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
               $row = mysqli_fetch_assoc($result);
               //$numrow = mysqli_num_rows($result);
               mysqli_close($conn);
               return $row;

            }
         }
      }
      if (!function_exists('chkDataType')) {
         function chkDataType($p_dbtable,$p_dbfield) {
            include 'conn.e2e.php';
            $sql = "SHOW COLUMNS FROM `".strtolower($p_dbtable)."`";
            $Colresult = mysqli_query($conn,$sql) or die(mysqli_error($conn));
            while($row = mysqli_fetch_array($Colresult))
            {
               if ($row['Field'] == $p_dbfield) {
                  return $row['Type'];
                  mysqli_close($conn);
                  break;
               }
            }
            mysqli_close($conn);
         }
      }

      if (!function_exists('SelectEach')) {
         function SelectEach($table,$whereClause) {
            include 'conn.e2e.php';
            $sql = "SELECT * FROM `".strtolower($table)."` ".$whereClause;
            $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
            mysqli_close($conn);
            if (mysqli_num_rows($result) > 0) return $result;
            else return false;
         }
      }

      if (!function_exists('FindFirst')) {
         function FindFirst($table,$whereClause,$fld) {
            include 'conn.e2e.php';

            if ($fld == "*") {
               $sql = "SELECT * FROM `".strtolower($table)."` ".$whereClause." order by refid limit 1";
            } else if (stripos($fld,",") > 0) {
               $sql = "SELECT $fld FROM `".strtolower($table)."` ".$whereClause." order by refid limit 1";
            } else {
               $sql = "SELECT $fld FROM `".strtolower($table)."` ".$whereClause." order by refid limit 1";
            }
            $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
            $row = mysqli_fetch_assoc($result);
            $numrow = mysqli_num_rows($result);
            mysqli_close($conn);
            if ($numrow <= 0) {
               return false;
            } else {
               if ($fld == "*" || stripos($fld,",") > 0) {
                  return $row;
               } else {
                  return $row[$fld];
               }
            }
         }
      }


      if (!function_exists('FFirstRefId')) {
         function FFirstRefId($table,$refid,$fld) {
            include 'conn.e2e.php';
            if ($refid) {
               if ($fld == "*") $sql = "SELECT * FROM `".strtolower($table)."` WHERE RefId = $refid";
                           else $sql = "SELECT $fld FROM `".strtolower($table)."` WHERE RefId = $refid";
               $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
               if ($result) {
                  $row = mysqli_fetch_assoc($result);
                  $numrow = mysqli_num_rows($result);
                  mysqli_close($conn);
                  if ($numrow <= 0) {
                     return false;
                  } else {
                     if ($fld == "*") return $row;
                                 else return $row[$fld];

                  }
               }
               else return false;
            } else return false;
         }
      }

      if (!function_exists('FindLast')) {
         function FindLast($table,$whereClause,$fld) {
            include 'conn.e2e.php';
            if ($fld == "*") {
               $sql = "SELECT * FROM `".strtolower($table)."` ".$whereClause." order by RefId DESC LIMIT 1";
            } else {
               $sql = "SELECT $fld FROM `".strtolower($table)."` ".$whereClause." order by RefId DESC LIMIT 1";
            } 
            $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
            if ($result) {
               $row = mysqli_fetch_assoc($result);
               $numrow = mysqli_num_rows($result);
               mysqli_close($conn);
               if ($numrow <= 0) {
                  return false;
               } else {
                  if ($fld == "*") return $row;
                              else return $row[$fld];

               }
            } else {
               return false;
            }
            /*include 'conn.e2e.php';
            if ($fld == "*") $sql = "SELECT * FROM `".strtolower($table)."` ".$whereClause." order by refid DESC LIMIT 1";
                        else $sql = "SELECT $fld FROM `".strtolower($table)."` ".$whereClause." order by refid DESC LIMIT 1";
            $stmt = $conn->query($sql);
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $db = null;
            if ($fld == "*") return $row;
                        else return $row[$fld];*/
         }
      }

      if (!function_exists('FindOrderBy')) {
         function FindOrderBy($table,$whereClause,$fld,$order) {
            include 'conn.e2e.php';
            if ($fld == "*") {
               $sql = "SELECT * FROM `".strtolower($table)."` ".$whereClause." order by $order DESC limit 1";
            } else if (stripos($fld,",") > 0) {
               $sql = "SELECT $fld FROM `".strtolower($table)."` ".$whereClause." order by $order DESC limit 1";
            } else {
               $sql = "SELECT $fld FROM `".strtolower($table)."` ".$whereClause." order by $order DESC limit 1";
            }
            $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
            $row = mysqli_fetch_assoc($result);
            $numrow = mysqli_num_rows($result);
            mysqli_close($conn);
            if ($numrow <= 0) {
               return false;
            } else {
               if ($fld == "*" || stripos($fld,",") > 0) {
                  return $row;
               } else {
                  return $row[$fld];
               }
            }
         }
      }

      if (!function_exists('FLastRefId')) {
         function FLastRefId($table,$refid,$fld) {
            include 'conn.e2e.php';
            if ($fld == "*") $sql = "SELECT * FROM `".strtolower($table)."` WHERE RefId = $refid order by refid DESC LIMIT 1";
                        else $sql = "SELECT $fld FROM `".strtolower($table)."` WHERE RefId = $refid order by refid DESC LIMIT 1";
            $stmt = $db->query($sql);
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $db = null;
            if ($fld == "*") return $row;
                        else return $row[$fld];
         }
      }

      if (!function_exists('getNextRow')) {
         function getNextRow($idx,$rs,$numCount) {
            if ($numCount > ($idx + 1)) {
               mysqli_data_seek($rs,($idx + 1));
               $NextRow = mysqli_fetch_assoc($rs);
               return $NextRow;
            }
         }
      }

      if (!function_exists('spacer')) {
         function spacer($h) {
            echo  '<div style="height:'.$h.'px;">&nbsp;</div>';
         }
      }

      if (!function_exists('bar')) {
         function bar() {
            echo '<div style="background:var(--border);height:1px;margin-top:5px;margin-bottom:5px;"></div>';
         }
      }

      if (!function_exists('hdrMenu')) {
         function hdrMenu($sys) {
               echo
               '<ul class="dropdown-menu">';
                     if ($GLOBALS['UserCode']!="COMPEMP") {
                        echo '
                        <li><a href="#" id="mUserProfile" class="Menu" pre="scrn" route="Users">Profile</a></li>
                        <li class="divider"></li>
                        <li class="dropdown-header">SETTINGS</li>
                        <li><a href="#" id="mCreateUser" class="Menu" pre="scrn" route="Users">Create New User</a></li>
                        <li><a href="#" id="mSysUserProfile" class="Menu" pre="scrn" route="SysAccount">System User Profile</a></li>
                        <li><a href="#" id="mEmployeesAccess" class="Menu" pre="scrn" route="AccessEmp">Employees Access</a></li>
                        <li><a href="#" id="mAuditTrail" class="Menu" pre="scrn" route="AuditTrails">Audit Trail</a></li>
                        <!--<li><a href="#" id="mUpload201" class="Menu" pre="scrn" route="UploadPDS">Upload 201</a></li>-->
                        <!-- <li><a href="#" id="mSysConfig" class="Menu" pre="scrn" route="">System Config</a></li> -->
                        <li><a href="#" id="mCompProfile" class="Menu" pre="scrn" route="CompanyProfile">Company Profile</a></li>
                        <li class="dropdown-header">SYSTEM</li>
                        <li>
                           <a href="javascript:void(0)" id="mResetPW" class="Menu" pre="scrn" route="ResetPW">
                              Employees Reset PW
                           </a>
                        </li>
                        <li>
                           <a href="javascript:void(0)" id="mUserManagement" class="Menu" pre="scrn" route="UserManagement">
                              User Management
                           </a>
                        </li>';
                     }
                     /*switch ($sys) {
                        case "AMS":
                           echo '
                              <li><a href="#" id="wsysPIS" class="Menu">PIS</a></li>
                              <li><a href="#" id="wsysPAY" class="Menu">PMS</a></li>
                           ';
                        break;
                        case "PIS":
                           echo '
                              <li><a href="#" id="wsysAMS" class="Menu">AMS</a></li>
                              <li><a href="#" id="wsysPAY" class="Menu">PMS</a></li>
                           ';
                        break;
                        case "PMS":
                           echo '
                              <li><a href="#" id="wsysAMS" class="Menu">AMS</a></li>
                              <li><a href="#" id="wsysPIS" class="Menu">PIS</a></li>
                           ';
                        break;
                     }*/
                  echo
                  '<li class="divider"></li>
                  <li><a href="#" id="mExitModule" class="Menu" pre="scrn" route="Welcome">Exit Module</a></li>
                  <li><a href="#" id="mLogOut" class="Menu" pre="scrn" route="LogOut">Log Out</a></li>
               </ul>';


         }

      }

      if (!function_exists('doAMS_sideBar')) {
         function doAMS_sideBar() {
            $emprefid = getvalue("hEmpRefId");
            $where_access = "WHERE EmployeesRefId = '".$emprefid."'";
            $access = FindFirst("usermanagement",$where_access,"SystemAccess");
            if ($access) {
               $access_arr = explode("|", $access);
               echo '<div class="sub-sidebar-wrapper txt-left margin-top">';
               echo '<div>';
               $str = "";
               foreach ($access_arr as $key => $value) {
                  if ($value == 3 || $value == 4) {
                     $str = '
                        <div class="sideMenu" route="Transaction" pre="ams" id="amsTrns">
                           <img src="' . img("transaction.png") . '" class="pngIcons">
                           <span style="margin-left:10px;">TRANSACTION</span>
                        </div>
                     ';
                  }
               }
               echo $str;
               echo '
                  <div id="submenuTrn">
               ';
               foreach ($access_arr as $key => $value) {
                  if ($value == 3) {
                     echo '
                        <div class="subMenu" route="TrnDTR" pre="ams" id="amsDTR" memof="amsTrns">
                           <span style="margin-left:30px;" title="Daily Time Record">DTR</span>
                        </div>
                     ';
                  }
                  if ($value == 4) {
                     echo '
                        <div class="subMenu" route="TrnEmpCreditBal" pre="ams" id="amsEmpCreditBal" memof="amsTrns">
                              <span style="margin-left:30px;" title="Employees Credit Balance">Emp. Credit Balance</span>
                        </div>
                     ';
                  }
               }
               echo '</div>';
               echo '</div>';
               echo '<div>';
               $str = "";
               foreach ($access_arr as $key => $value) {
                  if ($value == 5 ||
                      $value == 6 ||
                      $value == 7 ||
                      $value == 8 ||
                      $value == 9 ||
                      $value == 10 ||
                      $value == 11 ||
                      $value == 12 ||
                      $value == 22
                  ) {
                     $str = '
                        <div class="sideMenu" route="Request" pre="ams" id="amsReqs">
                           <img src="' . img("request.png") . '" class="pngIcons">
                           <span style="margin-left:10px;">APPLICATION</span>
                        </div>
                     ';
                  }
               }
               echo $str;
               echo '
                     <div id="submenuReq">
               ';
               foreach ($access_arr as $key => $value) {
                  if ($value == 5) {
                     echo '
                        <div class="subMenu" route="ReqChangeShift" pre="ams" id="amsChangeShift" memof="amsReqs">
                           <span style="margin-left:30px;">Change Shift</span>
                        </div>
                     ';
                  }
                  if ($value == 6) {
                     echo '
                        <div class="subMenu" route="ReqOvertime" pre="ams" id="amsOvertime" memof="amsReqs">
                           <span style="margin-left:30px;">Overtime</span>
                        </div>
                     ';
                  }
                  if ($value == 7) {
                     echo '
                        <div class="subMenu" route="ReqForceLeave" pre="ams" id="amsForceLeave" memof="amsReqs">
                           <span style="margin-left:30px;">Cancellation of Leave</span>
                        </div>
                     ';
                  }
                  if ($value == 8) {
                     echo '
                        <div class="subMenu" route="AttendanceRequest" pre="ams" id="amsAttendanceReq" memof="amsReqs">
                           <span style="margin-left:30px;">Attendance Registration</span>
                        </div>
                     ';
                  }
                  if ($value == 9) {
                     echo '
                        <div class="subMenu" route="AvailLeave" pre="ams" id="amsLeave" memof="amsReqs">
                           <span style="margin-left:30px;">Leave</span>
                        </div>
                     ';
                  }
                  if ($value == 10) {
                     echo '
                        <div class="subMenu" route="AvailCTO" pre="ams" id="amsCTO" memof="amsReqs">
                           <span style="margin-left:30px;">CTO</span>
                        </div>
                     ';
                  }
                  if ($value == 11) {
                     echo '
                        <div class="subMenu" route="AvailAuthority" pre="ams" id="amsAuthority" memof="amsReqs">
                           <span style="margin-left:30px;">Office Authority</span>
                        </div>
                     ';
                  }
                  if ($value == 22) {
                     echo '
                        <div class="subMenu" route="AvailTripTicket" pre="ams" id="amsTripTicket" memof="amsReqs">
                           <span style="margin-left:30px;">Trip Ticket</span>
                        </div>
                     ';
                  }
                  if ($value == 12) {
                     echo '
                        <div class="subMenu" route="AvailLeaveMonitization" pre="ams" id="amsLeaveMonitize" memof="amsReqs">
                           <span style="margin-left:30px;">Leave Monetization</span>
                        </div>
                     ';
                  }
               }
               echo '</div>';
               echo '</div><div>';
               $str = "";
               foreach ($access_arr as $key => $value) {
                  if ($value == 13 ||
                      $value == 14 ||
                      $value == 15 ||
                      $value == 16 ||
                      $value == 17 ||
                      $value == 18 ||
                      $value == 19 ||
                      $value == 20
                  ) {
                     $str = '
                        <div class="sideMenu" route="Approval" pre="ams" id="amsAppv">
                           <img src="' . img("signatories.png") . '" class="pngIcons">
                           <span style="margin-left:10px;">APPROVAL</span>
                        </div>
                     ';
                  }
               }
               echo $str;
               echo '
                     <div id="submenuAppv">
               ';
               foreach ($access_arr as $key => $value) {
                  if ($value == 13) {
                     echo '
                        <div class="subMenu" route="AppvChangeShift" pre="ams" id="amsForceLeave" memof="amsAppv">
                           <span style="margin-left:30px;">Change of Shift</span>
                        </div>
                     ';
                  }
                  if ($value == 14) {
                     echo '
                        <div class="subMenu" route="AppvCOCOvertime" pre="ams" id="amsForceLeave" memof="amsAppv">
                           <span style="margin-left:30px;">COC/Overtime</span>
                        </div>
                     ';
                  }
                  if ($value == 15) {
                     echo '
                        <div class="subMenu" route="AppvForceLeave" pre="ams" id="amsForceLeave" memof="amsAppv">
                           <span style="margin-left:30px;">Cancellation of Leave</span>
                        </div>
                     ';
                  }

                  if ($value == 16) {
                     echo '
                        <div class="subMenu" route="AppvAttendanceRegistration" pre="ams" id="amsAttendanceRegistration" memof="amsAppv">
                           <span style="margin-left:30px;">Attendance Registration</span>
                        </div>
                     ';
                  }                  

                  if ($value == 17) {
                     echo '
                        <div class="subMenu" route="AppvLeaveAvail" pre="ams" id="amsForceLeave" memof="amsAppv">
                           <span style="margin-left:30px;">Leave Availment</span>
                        </div>
                     ';
                  }

                  if ($value == 18) {
                     echo '
                        <div class="subMenu" route="AppvCTOAvail" pre="ams" id="amsForceLeave" memof="amsAppv">
                           <span style="margin-left:30px;">CTOs Availment</span>
                        </div>
                     ';
                  }

                  if ($value == 19) {
                     echo '
                        <div class="subMenu" route="AppvOfficeAuthority" pre="ams" id="amsForceLeave" memof="amsAppv">
                           <span style="margin-left:30px;">Office Authority</span>
                        </div>
                     ';
                  }

                  if ($value == 20) {
                     echo '
                        <div class="subMenu" route="AppvMonetization" pre="ams" id="amsForceLeave" memof="amsAppv">
                           <span style="margin-left:30px;">Leave Monetization</span>
                        </div>
                     ';
                  }
               }
               echo '</div>';
               echo '</div>';
               foreach ($access_arr as $key => $value) {
                  if ($value == 21) {
                     echo '
                        <div>
                           <div class="subMenu" id="amsRpts" route="Report" pre="ams">
                              <img src="' . img("reports.png") . '" class="pngIcons">
                              <span style="margin-left:10px;">REPORTS</span>
                           </div>
                        </div>
                     ';
                  }
               }
               echo '</div>';
            } else {
               echo '
                  <div class="sub-sidebar-wrapper txt-left margin-top">';
                     echo
                     '
                        <div>
                           <div class="sideMenu" route="Transaction" pre="ams" id="amsTrns">
                              <img src="' . img("transaction.png") . '" class="pngIcons">
                              <span style="margin-left:10px;">TRANSACTION</span>
                           </div>
                           <div id="submenuTrn">
                              <div class="subMenu" route="TrnDTR" pre="ams" id="amsDTR" memof="amsTrns">
                                 <span style="margin-left:30px;" title="Daily Time Record">DTR</span>
                              </div>
                              <!--
                              <div class="subMenu" route="TrnOfficeSuspension" pre="ams" id="amsOfficeSus" memof="amsTrns">
                                 <span style="margin-left:30px;" title="Office Suspension">Office Suspension</span>
                              </div>
                              -->
                              <div class="subMenu" route="TrnEmpCreditBal" pre="ams" id="amsEmpCreditBal" memof="amsTrns">
                                 <span style="margin-left:30px;" title="Employees Credit Balance">Emp. Credit Balance</span>
                              </div>
                           </div>
                        </div>';
                        include_once "inc_user_sidebar.e2e.php";
                        echo'
                        <div>
                           <div class="sideMenu" route="Approval" pre="ams" id="amsAppv">
                              <img src="' . img("signatories.png") . '" class="pngIcons">
                              <span style="margin-left:10px;">APPROVAL</span>
                           </div>
                           <div id="submenuAppv">
                              ';
                           if (getvalue("hCompanyID") != 1000) {
                              echo '
                                 <div class="subMenu" route="AppvChangeShift" pre="ams" id="amsForceLeave" memof="amsAppv">
                                    <span style="margin-left:30px;">Change of Shift</span>
                                 </div>
                              ';
                           }

                              echo '
                              <div class="subMenu" route="AppvCOCOvertime" pre="ams" id="amsForceLeave" memof="amsAppv">
                                 <span style="margin-left:30px;">COC/Overtime</span>
                              </div>
                              <div class="subMenu" route="AppvForceLeave" pre="ams" id="amsForceLeave" memof="amsAppv">
                                 <span style="margin-left:30px;">Cancellation of Leave</span>
                              </div>
                              <div class="subMenu" route="AppvAttendanceRegistration" pre="ams" id="amsAttendanceRegistration" memof="amsAppv">
                                 <span style="margin-left:30px;">Attendance Registration</span>
                              </div>
                              ';
                              bar();
                              echo
                              '<div class="subMenu" route="AppvLeaveAvail" pre="ams" id="amsForceLeave" memof="amsAppv">
                                 <span style="margin-left:30px;">Leave Availment</span>
                              </div>
                              <div class="subMenu" route="AppvCTOAvail" pre="ams" id="amsForceLeave" memof="amsAppv">
                                 <span style="margin-left:30px;">CTOs Availment</span>
                              </div>
                              <div class="subMenu" route="AppvOfficeAuthority" pre="ams" id="amsForceLeave" memof="amsAppv">
                                 <span style="margin-left:30px;">Office Authority</span>
                              </div>

                              <!-- <div class="subMenu" route="AppvDayOff" pre="ams" id="amsForceLeave" memof="amsAppv">
                                 <span style="margin-left:30px;">Compensatory Service Day-Off Availment</span>
                              </div> -->

                              <div class="subMenu" route="AppvMonetization" pre="ams" id="amsForceLeave" memof="amsAppv">
                                 <span style="margin-left:30px;">Leave Monetization</span>
                              </div>
                           </div>
                        </div>

                        <div>
                           <div class="subMenu" id="amsRpts" route="Report" pre="ams">
                              <img src="' . img("reports.png") . '" class="pngIcons">
                              <span style="margin-left:10px;">REPORTS</span>
                           </div>
                        </div>
                     ';
               echo
               '</div>';
            }
         }

         function doPMS_sideBar() {
            echo '
            <div class="sub-sidebar-wrapper txt-left margin-top">';
                  echo
                  '
                     <div class="subMenu" route="Transaction" pre="pms" id="pmsTrns">
                        <img src="' . img("transaction.png") . '" class="pngIcons">
                        <span style="margin-left:10px;">TRANSACTION</span>
                     </div>
                     <div class="subMenu" route="SpecialPayroll" pre="pms" id="pmsSpecialPayroll">
                        <img src="' . img("availability.png") . '" class="pngIcons">
                        <span style="margin-left:10px;">SPECIAL PAYROLL</span>
                     </div>
                     <div class="subMenu" route="OtherPayroll" pre="pms" id="pmsOtherPayroll">
                        <img src="' . img("signatories.png") . '" class="pngIcons">
                        <span style="margin-left:10px;">OTHER PAYROLL</span>
                     </div>
                     <div class="subMenu" route="Reports" pre="pms" id="pmsReports">
                        <img src="' . img("reports.png") . '" class="pngIcons">
                        <span style="margin-left:10px;">REPORTS</span>
                     </div>
            </div>';
         }

      }


      if (!function_exists('footer')) {
         function footer() {
            echo '<div class="footer">
            Powered By : e2e Solutions Management Phils. Inc. &copy; ('.date("Y",time()).')
            </div>';
         }
      }

      if (!function_exists('fTimePicker')) {
         function fTimePicker() {
            echo "<span>\n";
               echo "<select class='form-input saveFields--' name='drpHrs';>\n";
               for ($j=0;$j<24;$j++) {
                  if ($j <= 9) $j = "0".$j;
                  echo "<option value='".$j."'>".$j."</option>\n";
               }
               echo "</select>\n";
               echo "<select class='form-input' name='drpMin'>\n";
               for ($j=0;$j<60;$j++) {
                  if ($j <= 9) $j = "0".$j;
                  echo "<option value='".$j."'>".$j."</option>\n";
               }
               echo "</select>\n";
               echo "<select class='form-input' name='drpSec'>\n";
               for ($j=0;$j<60;$j++) {
                  if ($j <= 9) $j = "0".$j;
                  echo "<option value='".$j."'>".$j."</option>\n";
               }
               echo "</select>\n";
            echo "</span>\n";
         }

         function fTimePicker_WS($name,$defval) {
            echo "<span>\n";
               echo "<select class='form-input saveFields-- tmPicker--' name='".$name."';>\n";
               $inMinutes = 0;
               for ($j=0;$j<48;$j++) {
                  $hr = $inMinutes / 60;
                  if ($hr <= 9.5) $hr = "0".$hr;
                  $selected = "";
                  if ($j == $defval * 2) {
                     $selected = "selected";
                  }
                  echo "<option value='$inMinutes' $selected>";
                     echo str_replace(".5","",$hr).":";
                     if (($inMinutes % 60) == 0) {
                        echo "00";
                     } else {
                        echo "30";
                     }
                  echo "</option>\n";
                  $inMinutes = $inMinutes + 30;
               }
               echo "</select>\n";
            echo "</span>\n";
         }
         function drpTime($name,$selected,$min_interval = "15",$hour_start = "5"){
            if ($selected < 9) $selected = "0".$selected;
            $selected = $selected.":00";
            echo "<span>\n";
               echo "<select class='form-input saveFields--' name='".$name."' id='".$name."'>\n";
               echo '<option value="">--Time--</option>';
                  for ($hours=$hour_start; $hours <= 24 ; $hours++) { 
                     if ($hours < 24) {
                        for ($mins=0; $mins < 60 ; $mins+=$min_interval) { 
                           $time = str_pad($hours, 2, '0',STR_PAD_LEFT).':'.str_pad($mins, 2, '0',STR_PAD_LEFT);
                           if ($selected == $time) {
                              echo '<option value="'.(($hours * 60) + $mins).'" selected>'.$time.'</option>';   
                           } else {
                              echo '<option value="'.(($hours * 60) + $mins).'">'.$time.'</option>';
                           }
                        }   
                     } else {
                        echo '<option value="'.($hours * 60).'">24:00</option>';
                     }
                     

                  }

               echo "</select>\n";
            echo "</span>\n";   
         }
      }

      if (!function_exists('populateDropDown')) {
         function populateDropDown($form,$obj,$table,$lbl,$lbl2,$qrysort) {
            include 'conn.e2e.php';
            $myCounter = 0;
            $js = "";
            $js .= "\n".'var d = '.$form.';';
            //$js .= "\n".'if(d.'.$obj.'!=null){';
            $js .= "\n".'d.'.$obj.'.length = 0;';
            $js .= "\n".'d.'.$obj.'.options[0] = new Option();';
            $js .= "\n".'d.'.$obj.'.options[0].value = "";';
            $js .= "\n".'d.'.$obj.'.options[0].text = "'.$lbl.'";';
            $recordSet = f_Find($table,$qrysort);
            while($row = $recordSet->fetch_assoc()) {
               $myCounter = $myCounter + 1;
               $js .= "\n".'d.'.$obj.'.options['.$myCounter.'] = new Option();';
               $js .= "\n".'d.'.$obj.'.options['.$myCounter.'].value = "'.$row["RefId"].'";';
               if ($table == "services") {
                  $js .= "\n".'d.'.$obj.'.options['.$myCounter.'].text = "'.$row["ServiceName"].'";';
               }
               else if ($table == "staff") {
                  $js .= "\n".'d.'.$obj.'.options['.$myCounter.'].text = "'.$row["LastName"].', '.$row["FirstName"].'";';
               }
            }
            mysql_free_result($recordSet);
            $myCounter = $myCounter + 1;
            $js .= "\n".'d.'.$obj.'.options['.$myCounter.'] = new Option();';
            $js .= "\n".'d.'.$obj.'.options['.$myCounter.'].value = "1001";';
            $js .= "\n".'d.'.$obj.'.options['.$myCounter.'].text = "'.$lbl2.'";';
            //$js .= "}";
            return $js;
         }
      }

      if (!function_exists('msg')) {
         function msg($typ,$msg) {
            echo '<div class="alerts" id="msgbox">';
            if ($typ=="warn") {
               echo
                  '<div class="alert-warn-top">
                     <span class="glyphicon glyphicon-exclamation-sign"></span> WARNING <button type="button" class="close">&times;</button>
                  </div>';
               echo '<div class="alert-warn-msg">';
            } else if ($typ=="info") {
               echo '<div class="alert-info-top"><span class="glyphicon glyphicon-info-sign"></span> INFORMATION <button type="button" class="close">&times;</button></div>';
               echo '<div class="alert-info-msg">';
            } else if ($typ=="danger") {
               echo '<div class="alert-danger-top"><span class="glyphicon glyphicon-exclamation-sign"></span> WARNING <button type="button" class="close">&times;</button></div>';
               echo '<div class="alert-danger-msg">';
            } else {
               echo '<div class="alert-warn-top"><span class="glyphicon glyphicon-exclamation-sign"></span> MESSAGE
                     <button type="button" class="close">&times;</button>
                     </div>
                     <div class="alert-warn-msg">';
            }
            echo $msg.'</div>';
            echo '</div>';

         }
      }

      if (!function_exists('alert')) {
         function alert($alertTitle,$alertMsg) {
            echo
            '<div class="alert">
               <div class="alert-head">
                  '.$alertTitle.'
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
               </div>
               <div class="alert-body">'.$alertMsg.'</div>
            </div>';
         }
      }

      if (!function_exists('createSelectGender')) {
         function createSelectGender($objname,$val,$disabled) {
            echo '<select name="'.$objname.'" '.$disabled.' class="form-input saveFields-- rptCriteria--" id="'.$objname.'">';
            echo '<option value="" '; if ($val == "") echo "selected"; else true; echo '>Select Sex</option>';
            echo '<option value="F" '; if ($val == "F") echo "selected"; else true; echo '>FEMALE</option>';
            echo '<option value="M" '; if ($val == "M") echo "selected"; else true; echo '>MALE</option>';
            echo '</select>'."\n";
         }
      }
      if (!function_exists('createSelectCivilStatus')) {
         function createSelectCivilStatus($objname,$val,$disabled) {
            echo '<select name="'.$objname.'" '.$disabled.' class="form-input saveFields-- rptCriteria--" id="'.$objname.'">';
            echo '<option value="" '; if ($val == "") echo "selected"; else true; echo '>SELECT CIVIL STATUS</option>';
            echo '<option value="Si" '; if ($val == "Si") echo "selected"; else true; echo '>SINGLE</option>';
            echo '<option value="Ma" '; if ($val == "Ma") echo "selected"; else true; echo '>MARRIED</option>';
            echo '<option value="An" '; if ($val == "An") echo "selected"; else true; echo '>ANNULLED</option>';
            echo '<option value="Wi" '; if ($val == "Wi") echo "selected"; else true; echo '>WIDOWED</option>';
            echo '<option value="Se" '; if ($val == "Se") echo "selected"; else true; echo '>SEPARATED</option>';
            echo '<option value="Ot" '; if ($val == "Ot") echo "selected"; else true; echo '>OTHERS, PLEASE SPECIFY</option>';
            echo '</select>'."\n";
         }
      }

      if (!function_exists('createSelectApptType')) {
         function createSelectApptType($objname,$val,$disabled) {
            $ApptTypeContent = file_get_contents(json.'appttype.json');
            $ApptType = json_decode($ApptTypeContent, true);
            echo '<select name="'.$objname.'" '.$disabled.' class="form-input saveFields-- rptCriteria--" id="'.$objname.'">';
            echo '<option value=0>-- Select Appt. Type --</option>';
               foreach ($ApptType["ApptType"] as $value) {
                  $value_Arr = explode("=>",$value);
                  echo '<option value='.$value_Arr[0].'>'.strtoupper($value_Arr[1]).'</option>'."\n";
               }
            /*echo '<option value=1 '; if ($val == 1) echo "selected"; else true; echo '>Appointment</option>';
            echo '<option value=2 '; if ($val == 2) echo "selected"; else true; echo '>Non-Appointment</option>';
            echo '<option value=3 '; if ($val == 3) echo "selected"; else true; echo '>Separated</option>';*/
            echo '</select>'."\n";
         }
      }

      if (!function_exists('createForm')) {
         function createForm($type,$objname,$objid,$val,$others,$addClass) {
            echo '<input type="'.$type.'"
                         name="'.$objname.'"
                         id="'.$objid.'"
                         class="form-input '.$addClass.'"
                         value="'.$val.'" '.$others.'>'."\n";
         }
      }

      if (!function_exists('createFormSys')) {
         function createFormSys($type,$objname,$objid,$val,$others) {
            echo '<input type="'.$type.'"
                         name="'.$objname.'"
                         id="'.$objid.'"
                         class="form-input-sys"
                         value="'.$val.'" '.$others.'>'."\n";
         }
      }

      if (!function_exists('createSelectGradeLevel')) {
         function createSelectGradeLevel($objname,$objid,$val,$disabled) {
            echo '<select name="'.$objname.'" '.$disabled.' class="form-input" id="'.$objid.'">';
            echo '<option value="">--GRADE LEVEL--</option>';
            for ($j=1;$j<=12;$j++) {
               echo '<option value="'.$j.'"';
               if ($val==$j) echo 'selected'; else true;
               echo '>GRADE '.$j.'</option>';
            }
            echo '</select>';
         }
      }

      if (!function_exists('createSelect')) {
         function createSelect($table,$objname,$val,$w = 90,$name,$initValue,$disabled,$showIconDL = true) {
            include "conn.e2e.php";
            if (strtolower($table) == "salarygrade" || strtolower($table) == "jobgrade") {
               $sql = "SELECT * FROM ".strtolower($table)." ORDER BY RefId";
            } else {
               $sql = "SELECT * FROM ".strtolower($table)." ORDER BY Name";
            }
            $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
            if ($result) {
               echo '<select id="'.$objname.'" name="'.$objname.'" '.$disabled.' class="form-input saveFields-- rptCriteria-- createSelect--" style="width:'.$w.'%">'."\n";
               echo '<option value="0">'.$initValue.'</option>'."\n";
               //echo '<option value="0">NO SELECTED VALUE</option>'."\n";
               while($row = mysqli_fetch_assoc($result)) {
                  if ($row[$name] != "") {
                     echo '<option value="'.$row["RefId"].'"';
                     if ($val==$row["RefId"]) echo 'selected'; else true;
                     echo '>'.strtoupper($row[$name]).'</option>'."\n";
                  }

               }
               echo '</select>'."\n";
               if ($showIconDL) {
                  echo
                  '<span class="newDataLibrary">
                        <a href="javascript:void(0);"
                        onclick="popAddDataLibrary(\''.$objname.'\',\''.$table.'\');"
                        id="FM_'.$table.'" class="FMModalNewRecord--" for="'.$objname.'">
                        <i class="fa fa-plus-square" aria-hidden="true"></i>
                        </a>
                  </span>';
                  /*<a href="javascript:void(0)"
                  onclick="refreshFMDrop(\''.$objname.'\',\''.$table.'\');">
                  <i class="fa fa-refresh" aria-hidden="true"></i>
                  </a>*/
               }

            }
            mysqli_close($conn);
         }

         function createSelect2($table,$objname,$val,$w,$name,$initValue,$disabled,$where,$showIconDL = true) {
            include "conn.e2e.php";
            $w = 90;
            if (strtolower($table) == "salarygrade") {
               $sql = "SELECT * FROM ".strtolower($table)." $where ORDER BY Name + 0";
            } else {
               $sql = "SELECT * FROM ".strtolower($table)." $where ORDER BY Name";
            }
            $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
            if ($result) {
               echo '<select id="'.$objname.'" name="'.$objname.'" '.$disabled.' class="form-input saveFields-- rptCriteria-- createSelect--" style="width:'.$w.'%">'."\n";
               echo '<option value="">'.$initValue.'</option>'."\n";
               //echo '<option value="0">NO SELECTED VALUE</option>'."\n";
               while($row = mysqli_fetch_assoc($result)) {
                  if ($row[$name] != "") {
                     echo '<option value="'.$row["RefId"].'"';
                     if ($val==$row["RefId"]) echo 'selected'; else true;
                     echo '>'.strtoupper($row[$name]).'</option>'."\n";
                  }
               }
               echo '</select>'."\n";
               if ($showIconDL) {
                  echo
                  '<span class="newDataLibrary">
                     <a href="javascript:void(0);"
                        onclick="popAddDataLibrary(\''.$objname.'\',\''.$table.'\');"
                        id="FM_'.$table.'" class="FMModalNewRecord--" for="'.$objname.'">
                        <i class="fa fa-plus-square" aria-hidden="true"></i>
                     </a>
                  </span>';
                  /*<a href="javascript:void(0)"
                     onclick="refreshFMDrop(\''.$objname.'\',\''.$table.'\');">
                     <i class="fa fa-refresh" aria-hidden="true"></i>
                  </a>*/
               }
            }
            mysqli_close($conn);
         }

         function createSelectSysUser($obj,$where) {
            include "conn.e2e.php";
            $sql = "SELECT * FROM `sysuser` $where ORDER BY RefId";
            $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
            if ($result) {
               echo '<select name="'.$obj.'" id="'.$obj.'" class="form-input saveFields-- rptCriteria--">'."\n";
               echo '<option value=""></option>'."\n";
               while($row = mysqli_fetch_assoc($result)) {
                  echo '<option value="'.$row["UserName"].'"';
                  echo '>['.$row["RefId"].'] '.$row["UserName"].'</option>'."\n";
               }
               echo '</select>'."\n";
            }
            mysqli_close($conn);
         }
         function setRefIdValue($RefIdVal) {
            if ($RefIdVal == "") {
               return 0;
            } else return $RefIdVal; 

         } 

      }

      if (!function_exists('doSPMS_SideBar')) {
         function doSPMS_SideBar() {
            /*
            <div class="subMenu" id="spmsDASHBOARD" memof="spms">
                        <img src="'.img("dashboard.png").'" class="pngIcons">
                        DASHBOARD
                     </div>*/

            echo '
            <div class="sub-sidebar-wrapper txt-left" style="background:#222;">';
                  /*echo
                  '
                     <div class="subMenu" id="spmsIPCR" title="Individual Performance Commitment and Review" memof="spms">
                        <img src="'.img("vacantpos.png").'" class="pngIcons">
                        IPCR
                     </div>
                     <div class="subMenu" id="spmsDPCR" title="Division Performance Commitment and Review" memof="spms">
                        <img src="'.img("vacantpos.png").'" class="pngIcons">
                        DPCR
                     </div>
                     <!--<div class="subMenu" id="spmsOPCR" title="Office Performance Commitment and Review" memof="spms">
                        <img src="'.img("empinfo.png").'" class="pngIcons">
                        OPCR
                     </div>-->
                     <div class="subMenu" id="spmsIPAR" title="Individual Performance Accomplishment and Review" memof="spms">
                        <img src="'.img("empinfo.png").'" class="pngIcons">
                        IPAR
                     </div>
                     <div class="subMenu" id="spmsDPAR" title="Division Performance Accomplishment and Review" memof="spms">
                        <img src="'.img("empinfo.png").'" class="pngIcons">
                        DPAR
                     </div>
                     <div class="subMenu" id="spmsSummRating" title="Summary Rating" memof="spms">
                        <img src="'.img("performance-icon.png").'" class="pngIcons">
                        SUMMARY RATING
                     </div>
                     ';*/
                     echo '
                        <div class="subMenu" id="spmsSummRating" title="Summary Rating" memof="spms">
                           <img src="'.img("performance-icon.png").'" class="pngIcons">
                           SUMMARY RATING
                        </div>
                     ';
                  $rs = SelectEach("modules","WHERE SystemRefId = 5 ORDER BY Ordinal");
                  if ($rs) {
                     while ($row = mysqli_fetch_assoc($rs)) {
                        $title = $row["Name"];
                        $file  = $row["Filename"];
                        $route = $row["Route"];
                        echo '
                           <div class="subMenu" 
                                title="'.$title.'" 
                                memof="ldms" 
                                pre="'.$route.'" 
                                route="'.$file.'">
                                 <img src="'.img("analytics.png").'" class="pngIcons">
                                 '.strtoupper($title).'
                           </div>
                        ';
                     }
                  }
            echo
            '</div>';

         }

         function doLDMS_SideBar() {
            /*echo '
            <div class="sub-sidebar-wrapper txt-left" style="background:#222;">';
               echo
               '<div class="subMenu" id="ldmsIDP" title="Individual Developtment Plan" memof="ldms" pre="ldms" route="IndividualDevtPlan">
                  <img src="'.img("vacantpos.png").'" class="pngIcons">
                  INDIVIDUAL DEVELOPTMENT <br>PLAN
               </div>
               <div class="subMenu" id="ldmsLAP" title="Learning Application Plan" memof="ldms" pre="ldms" route="LearningAppPlan">
                  <img src="'.img("empinfo.png").'" class="pngIcons">
                  LEARNING APPLICATION <br>PLAN
               </div>
               <div class="subMenu" id="ldmsRAP" title="Re-entry Action Plan" memof="ldms" pre="ldms" route="Re-entryAction">
                  <img src="'.img("performance-icon.png").'" class="pngIcons">
                  RE-ENTRY ACTION <br>PLAN
               </div>
               <div class="subMenu" id="ldmsRU" title="Record Update" memof="ldms" pre="ldms" route="RecordUpdate">
                  <img src="'.img("records.png").'" class="pngIcons">
                  RECORD UPDATE
               </div>';
            echo
            '</div>';*/
            echo '<div class="sub-sidebar-wrapper txt-left" style="background:#222;">';
            /*echo '
               <div class="subMenu" memof="ldms" pre="ldms" route="_training_calendar">
                  <img src="'.img("availability.png").'" class="pngIcons">
                  Training Calendar
               </div>
               <div class="subMenu" memof="ldms" pre="ldms" route="_competency">
                  <img src="'.img("semplace.png").'" class="pngIcons">
                  Competency Table
               </div>
               <div class="subMenu" memof="ldms" pre="ldms" route="_position_profile">
                  <img src="'.img("semtype.png").'" class="pngIcons">
                  Position Profile
               </div>
               <div class="subMenu" memof="ldms" pre="ldms" route="_service_provider">
                  <img src="'.img("poslevel.png").'" class="pngIcons">
                  Service Provider Profile
               </div>
               <div class="sideMenu" id="drpCompetency">
                  <img src="' . img("semclass.png") . '" class="pngIcons">
                  Competency Assessment
               </div>
               <div id="subMenuCompetency">
                  <div class="subMenu" memof="ldms" pre="ldms" route="_individual_competency_data">
                     <span style="margin-left:30px;">Individual Competency Data</span>
                  </div>
                  <div class="subMenu" memof="ldms" pre="ldms" route="IndividualDevtPlan">
                     <span style="margin-left:30px;">Individual Devt Plan</span>
                  </div>
               </div>
               <div class="subMenu" memof="ldms" pre="ldms" route="_intervention">
                  <img src="'.img("semtraining.png").'" class="pngIcons">
                  Trainings/Programs
               </div>
               <div class="sideMenu" id="drpIntervention">
                  <img src="' . img("posname.png") . '" class="pngIcons">
                  Learning And Devt Intervention
               </div>
               <div id="subMenuIntervention">
                  <div class="subMenu" memof="ldms" pre="ldms" route="_nominees">
                     <span style="margin-left:30px;">Nominees</span>
                  </div>
                  <div class="subMenu" memof="ldms" memof="ldms" pre="ldms" route="_assigned_course">
                     <span style="margin-left:30px;">Assigned Courses</span>
                  </div>
               </div>
               <div class="subMenu" memof="ldms" pre="ldms" route="_office_order">
                  <img src="'.img("citizenship.png").'" class="pngIcons">
                  Office Order
               </div>
               <div class="sideMenu" id="drpServiceObligation">
                  <img src="' . img("career.png") . '" class="pngIcons">
                  Service Obligation
               </div>
               <div id="subMenuServiceObligation">
                  <div class="subMenu" memof="ldms" pre="ldms" route="_return_service_obligation">
                     <span style="margin-left:30px;">Return Service Obligation</span>
                  </div>
                  <div class="subMenu" memof="ldms" memof="ldms" pre="ldms" route="_REAP">
                     <span style="margin-left:30px;">Re-Entry Action Plan</span>
                  </div>
                  <div class="subMenu" memof="ldms" memof="ldms" pre="ldms" route="_LAP">
                     <span style="margin-left:30px;">Learning Application Plan</span>
                  </div>
               </div>
               <div class="subMenu" id="" title="" memof="ldms" pre="ldms" route="_report">
                  <img src="'.img("reports.png").'" class="pngIcons">
                  Reports
               </div>
            ';
            echo '
               

               <!--
               <div class="subMenu" id="" title="" memof="ldms" pre="ldms" route="_individual_competency_data">
                  <img src="'.img("analytics.png").'" class="pngIcons">
                  Individual Competency Data
               </div>
               <div class="subMenu" id="" title="" memof="ldms" pre="ldms" route="IndividualDevtPlan">
                  <img src="'.img("analytics.png").'" class="pngIcons">
                  Individual Devt Plan
               </div>
               <div class="subMenu" id="" title="" memof="ldms" pre="ldms" route="_nominees">
                  <img src="'.img("analytics.png").'" class="pngIcons">
                  Nominees
               </div>
               <div class="subMenu" id="" title="" memof="ldms" pre="ldms" route="_assigned_course">
                  <img src="'.img("analytics.png").'" class="pngIcons">
                  Assigned Courses
               </div>
               <div class="subMenu" id="" title="" memof="ldms" pre="ldms" route="_return_service_obligation">
                  <img src="'.img("analytics.png").'" class="pngIcons">
                  Return Service Obligation
               </div>
               <div class="subMenu" id="" title="" memof="ldms" pre="ldms" route="_REAP">
                  <img src="'.img("analytics.png").'" class="pngIcons">
                  Re-Entry Action Plan
               </div>
               <div class="subMenu" id="" title="" memof="ldms" pre="ldms" route="_LAP">
                  <img src="'.img("analytics.png").'" class="pngIcons">
                  LAP
               </div>
               -->
            ';*/
            $rs = SelectEach("modules","WHERE SystemRefId = 4 ORDER BY Ordinal");
            if ($rs) {
               while ($row = mysqli_fetch_assoc($rs)) {
                  $title = $row["Name"];
                  $file  = $row["Filename"];
                  $route = $row["Route"];
                  echo '
                     <div class="subMenu" 
                          title="'.$title.'" 
                          memof="ldms" 
                          pre="'.$route.'" 
                          route="'.$file.'">
                           <img src="'.img("analytics.png").'" class="pngIcons">
                           '.strtoupper($title).'
                     </div>
                  ';
               }
            }
            echo '</div>';

         }



         function doRMS_SideBar() {
            echo '
            <div class="sub-sidebar-wrapper txt-left" style="background:#222;">';
                  echo
                  '<div class="subMenu" id="rmsEmpApp" memof="rms">
                     <img src="'.img("empinfo.png").'" class="pngIcons">
                     EMPLOYMENT APPLICATION
                  </div>
                  <div class="subMenu" id="rmsVacantPos" memof="rms">
                     <img src="'.img("vacantpos.png").'" class="pngIcons">
                     VACANT POSITION
                  </div>
                  <div class="subMenu" id="rmsSchedInterview" memof="rms">
                     <img src="'.img("dashboard.png").'" class="pngIcons">
                     SCHEDULE FOR INTERVIEW
                  </div>
                  <div class="subMenu" id="rmsEvaluate" memof="rms">
                     <img src="'.img("appstatus.png").'" class="pngIcons">
                     EVALUATE
                  </div>
                  <div class="subMenu" id="rmsEmpReq" memof="rms">
                     <img src="'.img("empattach.png").'" class="pngIcons">
                     EMPLOYMENT REQUIREMENTS
                  </div>
                  <div class="subMenu" id="rmsReports" memof="rms">
                     <img src="'.img("reports.png").'" class="pngIcons">
                     REPORTS
                  </div>';
            echo
            '</div>';

         }
         function doSideBarMain() {
            echo
            '<div class="sideNav" id="div_RSideBar">
               <a href="#" onclick="closeNav();">
                  <i class="fa fa-backward" aria-hidden="true" id="closeNav"></i>
               </a>
               <div class="sub-sidebar-wrapper txt-left margin-top"><br><br>';
                  $sysuser = FindFirst("sysuser","WHERE EmployeesRefId = ".getvalue("hEmpRefId"),"*");
                  if ($sysuser["SysGroupRefId"] == 1) {
                     echo
                     '<div class="MAINSYS">
                        <div class="sideMenu" onclick="openNav_adm()">
                           <img src="'.img("Settings-icon.png").'" class="pngIcons">
                           <span style="margin-left:10px;">SYS ADMIN</span>
                        </div>
                        <div id="mySidenav" class="sidenav">
                          <div class="txt-right"><a href="javascript:void(0)" onclick="closeNav_adm()" style="color:white">&times;</a></div>
                          <div class="txt-center" style="padding:7px;"><b>ADMINISTRATION</b></div>
                          <div class="subMenu" id="admUserAccessSys" pre="adm" route="UserAccessSystem"><a href="javascript:void(0)" >User Access (System)</a></div>
                          <div class="subMenu" id="admForms"         pre="adm" route="Forms"><a href="javascript:void(0)">System Forms</a></div>
                          <div class="subMenu" id="admReports"       pre="adm" route="Reports"><a href="javascript:void(0)">System Reports</a></div>
                        </div>
                     </div>';
                  }
                  if ($sysuser["SysGroupRefId"] == 2 || $sysuser["SysGroupRefId"] == 1) {
                     echo
                     '<div class="MAINSYS">
                        <!--<div class="sideMenu" onclick="openNav_AgencyConfig()">
                           <img src="'.img("icon-human-resources.png").'" class="pngIcons">
                           <span style="margin-left:10px;">HR ADMIN</span>
                        </div>-->
                        <div id="myAgencyConfig" class="sidenav">
                           <div class="txt-right"><a href="javascript:void(0)" onclick="closeNav_AgencyConfig()" style="color:white">&times;</a></div>
                           <div class="txt-center" style="padding:7px;"><b>ADMINISTRATION</b></div>
                           <div class="subMenu" id="mCompProfile"     pre="scrn" route="CompanyProfile">
                              <a href="javascript:void(0)" >Company Profile</a></div>
                           <div class="subMenu" id="mSysConfig"       pre="scrn" route="SysConfig">
                              <a href="javascript:void(0)" >System Configuration</a></div>
                           <div class="subMenu" id="mSysUserProfile"  pre="scrn" route="SysAccount">
                              <a href="javascript:void(0)" >System User Profile</a></div>
                           <div class="subMenu" id="mEmployeesAccess" pre="scrn" route="AccessEmp">
                              <a href="javascript:void(0)" >Employees Access</a></div>
                           <div class="subMenu" id="mAuditTrail"      pre="scrn" route="AuditTrails">
                              <a href="javascript:void(0)" >Audit Trail</a></div>
                           <!--
                           <div class="subMenu" id="mResetPW"         pre="scrn" route="ResetPW">
                              <a href="javascript:void(0)" >Employees Reset PW</a>
                           </div>   
                           -->
                        </div>
                     </div>';
                  }


                  $sys = FindFirst("companysettings","WHERE CompanyRefId = ".$GLOBALS["CompanyId"],"Systems");
                  if ($sys) {
                     $sys_Array = explode("|",$sys);

                     foreach ($sys_Array as $systems) {
                        $row = FindFirst("system","WHERE Code = '$systems'","*");
                        $systemRefId = $row["RefId"];
                        if ($row) {
                           $show = false;
                           if ($sysuser["SysGroupRefId"] == 1) {
                              $show = true;
                           }
                           else {
                              $sql = "WHERE CompanyRefId = ".$GLOBALS["CompanyId"];
                              $sql .= " AND BranchRefId = ".$GLOBALS["BranchId"];
                              $sql .= " AND EmployeesRefId = ".getvalue("hEmpRefId");
                              $sql .= " AND SystemRefId = ".$systemRefId;
                              $show = FindFirst("sysusersystems",$sql,"*");
                              if ($show["Show"] == 1) {
                                 $show = true;
                              } else {
                                 $show = false;
                              }
                           }
                           if ($show) {
                              echo
                              '<div class="MAINSYS" syscode="'.strtoupper($row["Code"]).'">
                                 <div class="sideMenu" id="wmain'.strtoupper($row["Code"]).'">
                                    <img src="'.img($row["Icons"]).'" class="pngIcons">
                                    <span style="margin-left:10px;">'.strtoupper($row["Code"]).'</span>
                                 </div>
                                 <div class="txt-center subLabel" id="wsys'.strtoupper($row["Code"]).'">'.strtoupper($row["Name"]).'</div>
                              </div>';
                           }
                        } else {
                           echo
                           '<div>'.$systems.' UNASSIGNED</div>';
                        }
                     }

                  } else {
                     balloonMsg("UNASSIGNED COMPANY SETTINGS!!!","warn");
                  }
               echo
               '</div>
            </div>';
         }

         function doUser_SideBar() {
            echo '
            <div class="sub-sidebar-wrapper txt-left margin-top">';
               echo
               '<div class="subMenu" id="m201FILE" memof="pis" route="201File" pre="scrn">
                  <img src="'.img("201file.png").'" class="pngIcons">
                  201 FILE
               </div>';
               if (getvalue("hCompanyID") != 2) {
                  echo
                  '<div class="subMenu" id="m201FILEATTACH" memof="pis">
                     <img src="'.img("fileattach.png").'" class="pngIcons">
                     201 FILE ATTACHMENTS
                  </div>';
                  if (getvalue("hCompanyID") == 1000) {
                     include_once "inc_user_sidebar.e2e.php";
                     echo
                     '<div class="subMenu" id="mLEAVECARD" memof="pis">
                        <img src="'.img("leavecard.png").'" class="pngIcons">
                        LEAVE CARD
                     </div>';
                  }
                  echo
                  '<div class="subMenu" id="mSERVICERECORD" memof="pis">
                     <img src="'.img("proj_lib.png").'" class="pngIcons">
                     SERVICE RECORDS
                  </div>';
                  if (getvalue("hCompanyID") != 14) {
                     echo
                     '<div class="subMenu" route="TrnDTR" pre="ams" id="amsDTR" memof="amsTrns">
                        <img src="'.img("salgrade.png").'" class="pngIcons">
                        DTR
                     </div>
                     <div class="subMenu" pre="ams" id="mPAYSLIP" memof="amsTrns">
                        <img src="'.img("salgrade.png").'" class="pngIcons">
                        PAYSLIP
                     </div>';
                  }
               }
            echo
            '</div>';
         }
         
         function doRSideBar($picFileName) {
            $moduleContent = file_get_contents(json.$_SESSION["jsonFile"]["side"]);
            $obj           = json_decode($moduleContent, true); 
            $picSource = "";
            if ($picFileName == "" || $picFileName == null) $picSource = img("nopic.png");
            echo '
            <div class="sub-sidebar-wrapper txt-left">
               <!--
               <div>
                  <div class="withSubMenu" id="mADMINISTRATION">
                     <img src="' . img("records.png") . '" class="pngIcons">
                     ADMINISTRATION
                  </div>
                  <div style="border-radius:0;width:250px" id="submenuADMINISTRATION">
                     <div class="subMenu childSubMenu" pre="scrn" route="SignUpList" id="mSignUp">
                        <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;
                        Sign Up List
                     </div>

                      <div class="subMenu childSubMenu" pre="scrn" route="UploadData" id="mUploadData">
                        <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;
                        Upload Data
                     </div>
                  </div>
               </div>
               -->';
               $side = $obj["system"]["pis"]["side"];
               $class = $obj["system"]["pis"]["class"];
               $pre = $obj["system"]["pis"]["pre"];
               $mem = $obj["system"]["pis"]["member"];
               foreach ($side as $key => $value) {
                  echo
                  '<div class="'.$class.'" 
                        id="'.$value[2].'" 
                        pre="'.$pre.'"
                        route="'.$value[4].'"
                        memof="'.$mem.'">
                     <img src="' . img($value[3]) . '" class="pngIcons">';
                     echo $value[0];
                  echo '</div>';
               }
            echo   
            '</div>';
         }
      }

      if (!function_exists('sideBar')) {
         function sideBar() {
            include_once "incSideBar.e2e.php";
         }
      }

      if (!function_exists('doGridTable')) {
         function doGridTable($table,
                              $tableHdr,
                              $tableFld,
                              $sql,
                              $action,
                              $gridId) {
            echo
            '<table class="table table-order-column table-striped table-bordered table-hover"
                   id="'.$gridId.'">
               <thead>
                  <tr style="color:#fff;font-weight:600;background:var(--DarkBG);">';
                     if ($action[0] || $action[1] || $action[2] || $action[3]) {
                        echo '<th style="text-align:center;width:80px;">ACTION</th>';
                     }
                     /*echo
                     '<th style="text-align:center;width:80px;">ID</th>';*/
                     for ($j=0;$j<count($tableHdr);$j++) {
                        echo '<th style="text-align:center;">'.$tableHdr[$j].'</th>';
                     }
                  echo
                  '</tr>
               </thead>
               <tbody>';
                  ListData($table,$tableFld,$sql,$action);
               echo
               '</tbody>
            </table>';
         }
      }

      if (!function_exists('ListData')) {
         function chkFieldAvail($table,$field) {
            include "conn.e2e.php";
            $sql = "SHOW FULL COLUMNS FROM $table";
            $Colresult = mysqli_query($conn,$sql) or die(mysqli_error($conn));
            $k = false;
            while($row = mysqli_fetch_array($Colresult)){
               if ($row["Field"] == $field) {
                  $k++;
                  return $k;
                  exit;
               }
            }
            return $k;
         }
         function chkValue($value,$column) {
            if (stripos($column,"RefId") > 0) {
               $fk_table = explode("RefId",$column)[0];
               switch ($fk_table) {
                  case 'ResiAddCity':
                  case 'PermanentAddCity':
                     $tbl = "city";
                     break;
                  case 'ResiAddProvince':
                  case 'PermanentAddProvince':
                     $tbl = "province";
                     break;
                  case 'ResiCountry':
                  case 'PermanentCountry':
                     $tbl = "country";
                     break;
                  default:
                     $tbl = $fk_table;
                     break;
               }
               return getRecord($tbl,$value,"Name");
            } else {
               return $value;
            }
         }
         function chkTableAvail($table) {
            include "conn.e2e.php";
            $sql = "SHOW TABLES FROM $dbname";
            $result = mysqli_query($conn,$sql);
            if (!$result) {
               echo "DB Error, could not list tables\n";
               echo 'MySQL Error: ' . mysqli_error($conn);
               exit;
            }
            while ($row = mysqli_fetch_row($result)) {
               if ($row[0] == strtolower($table)) {
                  return $table;
                  break;
               }
            }

         }
         function getTableMatch($tbl) {
            $return = "";
            switch ($tbl) {
               case "InitialInterviewBy":
               case "FinalInterviewBy":
               case "Custodian":
               case "ApprovedBy":
               case "TransferTo":
               case "TransferFrom":
                  $return = "employees";
                  break;
               default:
                  $return = "Err";
                  break;
            }
            return $return;

         }
         function ListData($table,$tableFld,$sql,$action) {
            require "conn.e2e.php";
            $table = strtolower($table);
            $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
            $rowcount = mysqli_num_rows($result);
            if ($rowcount) {
               while ($row = mysqli_fetch_assoc($result)) {
                  $refid   = $row['RefId'];
                  echo
                  '<tr title="Record ID:'.$refid.'">';

                     if ($action[0] || $action[1] || $action[2] || $action[3]) {
                        echo
                        '<td nowrap class="cellAction" style="padding:var(--grid_padding_td);">';
                           if (isset($action[0]) && $action[0]) {
                              echo
                              '<a style="text-decoration:none;cursor:pointer;margin-right:10px" title="View This Record" id="edit_'.$refid.'">
                                 <img src="'.img("view.png").'" onclick="viewInfo('.$refid.',3,\'\');">
                              </a>';
                           }
                           if (isset($action[1]) && $action[1]) {
                              echo
                              '<a style="text-decoration:none;cursor:pointer;margin-right:10px" title="Edit This Record">
                                 <img src="'.img("edit.png").'" onclick="viewInfo('.$refid.',2,\'\');">
                              </a>';
                           }
                           if (isset($action[2]) && $action[2]) {
                              echo
                              '<a style="text-decoration:none;cursor:pointer" onclick="deleteRecord('.$refid.');"
                                 title="Delete This Record" id="del_'.$refid.'">
                                 <img src="'.img("delete.png").'">
                              </a>';
                           }
                           if (isset($action[3]) && $action[3]) {
                              echo
                              '
                                 <!--<input type="radio" name="obj" id="idx_'.$refid.'" onclick="selectMe('.$refid.');">-->
                                 &nbsp;&nbsp;<i class="fa fa-file-text" name="obj_'.$refid.'" style="cursor: pointer;" id="idx_'.$refid.'" onclick="selectMe('.$refid.');" title="Select"></i>
                              ';
                           }
                        echo
                        '</td>';
                     }

                        /*echo '<td nowrap class="cellRefId" style="padding:var(--grid_padding_td);">'.$refid.'</td>';*/
                        for ($j=0;$j<count($tableFld);$j++) {
                           if (stripos($tableFld[$j],"RefId") > 0) { // determined if FK
                              $fk_table = explode("RefId",$tableFld[$j])[0];
                              $tbl = chkTableAvail($fk_table);
                              if (empty($tbl)) {
                                 $tbl = getTableMatch($fk_table);
                              }
                              if (strtolower($tbl) == "err") {
                                 $colValue = "err no tbl";
                              } else if (strtolower($tbl) == "employees") {
                                 $emp = FFirstRefId(strtolower($tbl),$row[$tableFld[$j]],"*");
                                 if ($emp) {
                                    $colValue = $emp["LastName"].", ".$emp["FirstName"];
                                 } else {
                                    $colValue = "";
                                 }

                              } else $colValue = getRecord($tbl,$row[$tableFld[$j]],"Name");
                           } else if (stripos($tableFld[$j],"Time") > 0) { // determined if time field
                              /*$textValue_hr = floor(($row[$tableFld[$j]]) / 60);
                              $textValue_min = $row[$tableFld[$j]] % 60;
                              if ($textValue_min <= 9) {
                                 $textValue_min = "0$textValue_min";
                              }
                              if ($textValue_hr >= 1 && $textValue_hr <= 9) {
                                 $textValue_hr = "0$textValue_hr";
                              }
                              if ($textValue_hr == 0) {
                                 $textValue_hr = "12";
                              }
                              if ((12 * 60) > $row[$tableFld[$j]]) {
                                 $textValue = $textValue_hr.":".$textValue_min." AM";
                              }
                              else {
                                 $textValue = ($textValue_hr - 12).":".$textValue_min." PM";
                              }*/
                              
                              $colValue = convertToHoursMins($row[$tableFld[$j]]);
                           } else if ($tableFld[$j] == "WithPay" ||
                                      $tableFld[$j] == "isLegal" ||
                                      $tableFld[$j] == "isApplyEveryYr" ||
                                      $tableFld[$j] == "Accumulating"
                                   ) {
                              if ($row[$tableFld[$j]] == 1) {
                                 $colValue = "YES";
                              } else {
                                 $colValue = "NO";
                              }
                           } else if ($tableFld[$j] == "Status") {
                              switch ($row[$tableFld[$j]]) {
                                 case 'Approved':
                                    $colValue = "Approved";
                                    break;
                                 case 'Cancelled':
                                    $colValue = "Cancelled";
                                    break;
                                 case 'Rejected':
                                    $colValue = "Disapproved";
                                    break;
                                 case 'Request For Cancellation':
                                    $colValue = "Request For Cancellation";
                                    break;
                                 default:
                                    $colValue = "For Approval";
                                    break;
                              }
                           } else if ($tableFld[$j] == "ExpiryDate" && $table == "employeesmovement") {
                              if ($row[$tableFld[$j]] == "") {
                                 $colValue = "PRESENT";
                              } else {
                                 $colValue = $row[$tableFld[$j]];
                              }
                           } else if ($tableFld[$j] == "IsHalf") {
                              switch ($row[$tableFld[$j]]) {
                                 case '0':
                                    $colValue = "Regular Monetization";
                                    break;
                                 case '1':
                                    $colValue = "Special Monetization";
                                    break;
                                 default:
                                    $colValue = "";
                                    break;
                              }
                           } else {
                              $colValue = $row[$tableFld[$j]];
                           }
                           if ($table == "course") {
                              if ($tableFld[$j] == "Level") {
                                 switch ($row[$tableFld[$j]]) {
                                    case '5':
                                       $colValue = "Graduate Studies";
                                       break;
                                    case '4':
                                       $colValue = "College";
                                       break;
                                    case '3':
                                       $colValue = "Vocational";
                                       break;
                                    default:
                                       $colValue = "";
                                       break;
                                 }
                              }
                           }
                           if ($table == "employeesperformance") {
                              if ($tableFld[$j] == "Adjectival") {
                                 switch ($row[$tableFld[$j]]) {
                                    case '1':
                                       $colValue = "Poor";
                                       break;
                                    case '2':
                                       $colValue = "Unsatisfactory";
                                       break;
                                    case '3':
                                       $colValue = "Satisfactory";
                                       break;
                                    case '4':
                                       $colValue = "Very Satisfactory";
                                       break;
                                    case '5':
                                       $colValue = "Outstanding";
                                       break;
                                    default:
                                       $colValue = "";
                                       break;
                                 }
                              }   
                           }
                           
                           
                           echo '<td nowrap class="cellGrid" style="padding:var(--grid_padding_td);">'.str_replace("ñ","ntilde",$colValue).'</td>';
                        }
                  echo
                  '</tr>';
               }
               //mysql_free_result($result);
            }
         }
      }

      if (!function_exists('rptHeader')) {
         function rptHeader($rptTitle,$rpt_code = "") {
            $t = time();
            $date_today    = monthName(date("m"),0)." ".date("d, Y",$t);
            $curr_time     = date("H:i:s",$t);
            $rs            = FindFirst("company","","*");
            $demo            = false;
            $refid         = $rs["RefId"];
            if ($refid == "2") {
               echo
               '<div class="row">

                  <div class="col-xs-6 txt-left" style="">
                     <img src="'.img($rs["RefId"]).'/PCCLOGO_report.png" style="width:250px;">
                  </div>
                  <div class="col-xs-6 txt-right">
                     <i class="fa fa-map-marker"></i>&nbsp; 25F Vertis North Corporate Center I 
                     <br>
                     North Avenue, Quezon City 1105
                     <br>
                     <i class="fa fa-envelope"></i>&nbsp;queries@phcc.gov.ph
                     <br>
                     <i class="fa fa-phone"></i>&nbsp;(+632) 7719-PCC (7719-722) 
                  </div>
               </div>';
               bar();
               echo
               '<div class="txt-center">
                  <div class="fontB10 txt-center">'.strtoupper($rptTitle).'</div>
               </div>'."\n";
            } else if ($refid == "35") {
               echo
               '<div class="row">
                  <div class="col-xs-12">
                     <table width="100%">
                        <tr>
                           <td rowspan="3" style="width: 20%;" class="text-center">
                              <img src="'.img($rs["RefId"]).'/report_logo.png" style="width:50%;">
                           </td>
                           <td rowspan="3" style="width: 50%;" class="text-center">
                              <h4><b>'.strtoupper($rptTitle).'</b></h4>
                           </td>
                           <!--<td colspan="2">
                              Document Code: '.$rpt_code.'
                           </td>-->
                        </tr>
                        <!--<tr>
                           <td colspan="2">
                              Date:
                           </td>
                        </tr>
                        <tr>
                           <td style="width: 15%;">
                              Revision No.:00
                           </td>
                           <td style="width: 15%;">
                              Page <u>1</u> of <u>1</u>
                           </td> 
                        </tr>-->
                     </table>
                  </div>
               </div>';
               bar();
            } else if ($refid == "14") {
               echo
               '<div class="row">
                  <div class="col-xs-12 txt-left">
                     <img src="'.img($rs["RefId"]."/".$rs["BigLogo"]).'" style="width:200px;">
                  </div>
               </div><br>'."\n";
               echo
               '<div class="txt-center">
                  <div class="fontB10 txt-center">'.strtoupper($rptTitle).'</div>
               </div>'."\n";
            } else if ($refid == "28") {
               echo '
                  <div class="row">
                     <div class="col-xs-12 text-center">
                        <span style="font-family: Old English Text MT; text-align: center; font-size: 20pt;">
                           Office of the Vice President
                           <br>
                           of the Philippines
                        </span>      
                     </div>
                  </div>
               ';
            } else if ($refid == "21") {
               echo '
                  <div class="row">
                     <div class="col-xs-12">
                        <table width="100%">
                           <tr>
                              <td style="border:none; width:*;">
                                 <img src="upl/21/rpt_logo.jpg" height="40">
                              </td>
                              <td style="border:none; width:92%;">
                                 Department of Science and Technology
                                 <br>
                                 Metals Industry Research and Development Center
                              </td>
                           </tr>
                           <tr>
                              <td style="border:none;" colspan="2">
                                 <div class="fontB10 text-center">'.strtoupper($rptTitle).'</div>
                              </td>
                           </tr>
                        </table>
                        <br><br>
                     </div>
                  </div>
               ';
            }
            else {
               echo
               '<div class="row">
                  <div class="col-xs-3 txt-left">
                  <img src="'.img($rs["RefId"]."/".$rs["SmallLogo"]).'" style="width:200px;">
                  </div>
                  <div class="col-xs-6 txt-center">
                     <div class="fontB11">REPUBLIC OF THE PHILIPPINES</div>
                     <div class="fontB11" style="vertical-align:bottom;">'.$rs["Name"].'</div>
                     <div class="font10">';
                        if ($rs["Address"]) {
                           echo $rs["Address"];
                        } else {
                           echo "No Address Assigned";
                        }

                     echo 
                     '</div>
                  </div>
                  <div class="col-xs-3"></div>
               </div><br>'."\n";
               echo
               '<div class="txt-center">
                  <div class="fontB10 txt-center">'.strtoupper($rptTitle).'</div>
               </div>'."\n";
            }
            

            /*echo
            '<div class="row">
               <div class="col-xs-6 fontB10 txt-left">
                  RUN DATE : '.$date_today.' - '.$curr_time.'
               </div>
               <div class="col-xs-6 fontB10 txt-right">
                  PRINTED BY: '.$_SESSION['sess_user'].'
               </div>
            </div>'."\n";
            */


         }
      }
      if (!function_exists('rptFooter')) {
         function rptFooter() {
            $t = time();
            $date_today    = monthName(date("m"),0)." ".date("d, Y",$t);
            $curr_time     = date("H:i:s",$t);
            echo
            '<div class="rptFooter runDate row">
               <div class="col-xs-4 txt-left">
                  RUN DATE : '.$date_today.' - '.$curr_time.'
               </div>
               <div class="col-xs-4 txt-center">
                  Powered By : E2E Solutions Management Phils. Inc. &copy; (EBM'.date("Y",time()).')
               </div>
               <div class="col-xs-4 txt-right">
                  PRINTED BY: '.getvalue("hUser").'
               </div>
            </div>';
         }

         function rpt_footer($cid) {
            if ($cid == "14") {
               echo
               '<div class="rptFooter row">
                  <div class="col-xs-4">
                     <div class="row">
                        <div class="col-xs-12">
                           Republika ng Pilipinas
                           <br>
                           Pangasiwaan ng Tubig at Alkantarilya sa Kalakhang Maynila
                           <br>
                           Metropolitan Waterworks and Sewerage System
                           <br>
                           Regulatory Office
                           <br>
                           Katipunan Road, Balara, Quezon City 1105, Philippines
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-4">
                     <div class="row">
                        <div class="col-xs-8">
                           Office of the Chief Regulatory
                           <br>
                           Administration and Legal Affairs
                           <br>
                           Customer Service Regulation
                           <br>
                           Financial Regulation
                           <br>
                           Technical Regulation
                        </div>
                        <div class="col-xs-4">
                           435-8900
                           <br>
                           435-8902
                           <br>
                           435-8903
                           <br>
                           435-8901
                           <br>
                           435-8904
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-4">
                     <div class="row">
                        <div class="col-xs-12">
                           <u>ro.mwss.gov.php</u>
                        </div>
                     </div>
                  </div>
               </div>';
            }
         }

         function fTabs($idx,$title,$iFile) {
            $CompanyId = $GLOBALS["CompanyId"];
            $BranchId = $GLOBALS["BranchId"];
            $EmployeesRefId = $GLOBALS["EmployeesRefId"];
            echo
               '<div id="tab_'.$idx.'">
                  <div class="panel panel-default">
                     <div class="panel-heading">'.$title.'</div>
                     <div class="panel-body" id="'.$iFile.'">';
                        include_once $iFile.".e2e.php";
                     echo
                     '</div>
                  </div>
               </div>'."\n";
         }

         function doTitleBar($str) {
            $strArr = explode("|",$str);
            echo
            '<div class="row panel-top" style="padding-left:0px;border-radius:0;">
               <div class="col-xs-6">
                  <a href="javascript:void(0)" class="mbar" id="titleBarIcons" onclick="openNav();">
                     <i class="fa fa-chevron-left" aria-hidden="true"></i>
                  </a>&nbsp;&nbsp;';
                  echo strtoupper($strArr[0]);
               echo
               '</div>
               <div class="col-xs-5 txt-right">
                  Today is <span id="TimeDate"></span>
               </div>
               <div class="col-xs-1 txt-right">
                  <!--<a href="#" id="mHome" style="color:white;">
                     <i class="fa fa-home" aria-hidden="true" title="Home/Exit Module"></i>
                  </a>-->
               </div>
            </div>';
         }

         function doHidden($name,$value,$other) {
            echo '<input type="hidden" '.$other.' name="'.$name.'" id="'.$name.'" value="'.$value.'">'."\n";
         }

         function saveRecord($dbtable,$fldnval,$isMultiple) {
            $FieldType = "";
            $Fields = "";
            $Values = "";
            $SaveSuccessfull = "";
            $js = "";
            $Field_and_Value_Array = "";
            $str = "";

            if ($isMultiple) {
               $last_insert_id = 0;
               $items_Arr = explode("»",$fldnval);
               for ($k=0;$k<count($items_Arr) - 1;$k++)
               {
                  $Field_and_Value_Array = explode("!",$items_Arr[$k]);
                  $Fields = "";
                  $Values = "";
                  for ($i=0;$i<count($Field_and_Value_Array) - 1;$i++) {
                     $Items = $Field_and_Value_Array[$i];
                     $Items_Arr = explode("|",$Items);
                     $FieldType_Arr = explode("_",$Items_Arr[1]);
                     $FieldType = $FieldType_Arr[0];
                     $Fields .= "`$FieldType_Arr[1]`, ";

                     if ($FieldType == "sint" ||
                         $FieldType == "bint" ||
                         $FieldType == "deci") {
                        $Values .= "$Items_Arr[2], ";
                     }
                     else {
                        $Values .= "'$Items_Arr[2]', ";
                     }
                  }
                  $str = f_SaveRecord("NEWSAVE",$dbtable,$Fields,$Values);
                  if (is_numeric($str)) {
                     $SaveSuccessfull = "";
                  } else {
                     $SaveSuccessfull .= $str;
                  }
               }
            } else {
               $Field_and_Value_Array = explode("!",$fldnval);
               for ($i=0;$i<count($Field_and_Value_Array) - 1;$i++) {
                  $Items = $Field_and_Value_Array[$i];
                  $Items_Arr = explode("|",$Items);
                  $FieldType_Arr = explode("_",$Items_Arr[1]);
                  $FieldType = $FieldType_Arr[0];
                  $Fields .= "`$FieldType_Arr[1]`, ";

                  if ($FieldType == "sint" ||
                      $FieldType == "bint" ||
                      $FieldType == "deci") {
                     $Values .= "$Items_Arr[2], ";
                  }
                  else {
                     $Values .= "'$Items_Arr[2]', ";
                  }
               }
               $SaveSuccessfull = f_SaveRecord("NEWSAVE",$dbtable,$Fields,$Values);
            }
            return $SaveSuccessfull;
         }
         function modalEmpLkUp() {
                  echo
                  '<div class="modal fade" id="modalEmpLookUp" role="dialog">
                     <div class="modal-dialog modal-lg">
                        <div class="mypanel">
                           <div class="panel-top">
                              EMPLOYEES LOOK UP
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                           </div>
                           <div class="panel-mid" id="EmpListLkUp" style="max-height:500px;overflow:auto;">
                              &nbsp;
                           </div>
                        </div>
                     </div>
                  </div>
                  <script language = "JavaScript">
                     function selectMe(emprefid) {
                        $.get("EmpQuery.e2e.php",
                        {
                           emprefid:emprefid
                        },
                        function(data,status) {
                           if(status == "error")return false;
                           if(status == "success"){
                              var data = JSON.parse(data);
                              var obj_EmpRefId = $("#objEmpRefIdFill").val();
                              var obj_EmpFullName = $("#objEmpFullNameFill").val();
                              try {
                                 $("#modalEmpLookUp").modal("hide");
                                 if ($("[name=\'EmpSearchTaskModal\']").val() == 1) {
                                    switch ($("#hProg").val()) {
                                       case "scrnUsers":
                                          fillUp(data);
                                       break;
                                       default:
                                          $(\'[name="sint_EmployeesRefId"]\').val(data.RefId);
                                          $(\'[name="txtRefId"]\').val(data.RefId);
                                          $(\'[name="txtLName"]\').val(data.LastName);
                                          $(\'[name="txtFName"]\').val(data.FirstName);
                                          $(\'[name="txtMidName"]\').val(data.MiddleName);
                                          $(\'[name="txtAgencyId"]\').val(data.AgencyId);
                                          submitForm();
                                       break;
                                    }
                                 } else if ($("[name=\'EmpSearchTaskModal\']").val() == 2) {
                                    $(\'[name="\'+obj_EmpFullName+\'"]\').val(data.LastName + ", " + data.FirstName);
                                    $(\'[name="\'+obj_EmpRefId+\'"]\').val(data.RefId);
                                 }
                                 $("[name=\'EmpSearchTaskModal\']").val(0);
                              }
                              catch (e)
                              {
                                 if (e instanceof SyntaxError) {
                                     alert(e.message);
                                 }
                              }
                           }
                        });
                     }
                  </script>
                  ';
         }
         function fillEmpLkUp($txtRefId,$txtLName,$txtFName,$txtMidName) {
            echo
            '<script language="javascript">
               $(\'[name="txtRefId"]\').val("'.$txtRefId.'");
               $(\'[name="txtLName"]\').val("'.$txtLName.'");
               $(\'[name="txtFName"]\').val("'.$txtFName.'");
               $(\'[name="txtMidName"]\').val("'.$txtMidName.'");
            </script>';
         }

         function modalFileManager () {
            echo
               '<div class="modal fade" id="FM_ModalDataEntry" role="dialog">
                  <div class="modal-dialog" style="padding:0px;width:97%;height:92%;">
                     <div class="mypanel">
                        <div class="panel-top">
                           <span id="FM_Title">$nbsp;</span>
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="panel-mid-litebg"
                             id="FM_DataEntry" style="max-height:500px;overflow:auto;">
                        </div>
                     </div>
                  </div>
               </div>';
         }
         function FMLookUp () {
            echo
               '<div class="modal fade" id="FMLookUp" role="dialog">
                  <div class="modal-dialog" style="padding:0px;width:97%;height:92%;">
                     <div class="mypanel">
                        <div class="panel-top">
                           <span id="FMLookUp_Title">$nbsp;</span>
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="panel-mid-litebg"
                             id="FMLookUp_List" style="max-height:500px;overflow:auto;">
                        </div>
                     </div>
                  </div>
               </div>';
         }

         function parseFieldnValue($dbField,$post) {
            $Fldnval = $Fields = $Values = "";
            foreach ($dbField as $key => $value) {
               $fieldName = $value[0];
               $objName   = $value[1];
               $defValue  = $value[2];
               $dataType  = $value[3];
               if ($objName != "") {
                  if (isset($post[$objName])) {
                     $val = realEscape($post[$objName]);
                     switch ($dataType) {
                        case "int":
                        case "deci":
                           $val = setValue_RefId($val);
                        break;
                        case "date":
                           if ($val == "") {
                              $val = NULL;
                           } 
                        break;
                     }
                     if ($val != "") {
                        $val = realEscape($val);
                        $Fldnval .= "`".$fieldName."` = '".$val."', "; 
                        $Fields  .= "`".$fieldName."`,";
                        $Values  .= "'".$val."',";      
                     }
                     
                  } else {
                     //echo "ERR : No obj available --->".$objName."\n";
                  }
               }    
            }
            return ["FieldsValues"=>$Fldnval,"Fields"=>$Fields,"Values"=>$Values];
         }


         function getRecord($table,$RefId,$fields){
               include 'conn.e2e.php';
               if ($RefId) {
                  $sql = "SELECT `$fields` FROM `".strtolower($table)."` where RefId = $RefId";
                  $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
                  $row = mysqli_fetch_assoc($result);
                  mysqli_close($conn);
                  $numrow = mysqli_num_rows($result);
                  if ($numrow <= 0) {
                     return "";
                  } else {
                     if ($fields == "*") return $row;
                                    else {
                                       $value = str_replace("(","&#40;",$row[$fields]);
                                       $value = str_replace(")","&#41;",$value);
                                       $value = str_replace('"',"&#34;",$value);
                                       return $value;
                                    }

                  }
               } else return "";

         }
         function createRatingScale($objname,$objid,$val,$disabled) {
            echo '<select name="'.$objname.'" '.$disabled.' class="form-input saveFields--" id="'.$objid.'">';
            echo '<option value="">--</option>';
               echo '<option value="1">1 - POOR</option>';
               echo '<option value="2">2 - UNSATISFACTORY</option>';
               echo '<option value="3">3 - SATISFACTORY</option>';
               echo '<option value="4">4 - VERY SATISFACTORY</option>';
               echo '<option value="5">5 - OUTSTANDING</option>';
            echo '</select>';
         }

         function btnSACABA($disp) {
            echo
            '<span id="spSAVECANCEL">';
                     if ($disp[0]) {
                        echo
                        '<button type="button" class="btn-cls4-sea trnbtn"
                                 name="btnSAVE" id="btnSAVE">
                           <i class="fa fa-floppy-o" aria-hidden="true"></i>
                           &nbsp;Save
                        </button>';
                     }
                     if ($disp[1]) {
                        echo
                        '<button type="button" class="btn-cls4-red trnbtn"
                                 name="btnCANCEL" id="btnCANCEL">
                           <i class="fa fa-undo" aria-hidden="true"></i>
                           &nbsp;Cancel
                        </button>';
                     }
                  echo
                  '</span>
                  <span id="spBACK">';
                     if ($disp[2]) {
                        echo
                        '<button type="button"
                                   class="btn-cls4-red trnbtn"
                                   id="btnBACK" name="btnBACK">
                           <i class="fa fa-backward" aria-hidden="true"></i>
                           &nbsp;Back
                        </button>';
                     }
                  echo
                  '</span>';
         }
         function btnINRECLO($disp) {
            if ($disp[0]) {
               echo
               '<button type="button"
                    class="btn-cls4-sea trnbtn"
                    id="btnINSERT" name="btnINSERT">
                  <i class="fa fa-file" aria-hidden="true"></i>
                  &nbsp;Insert New
               </button>';
            }
            if ($disp[1]) {
               /*echo
               '<button type="submit"
                    class="btn-cls4-choco trnbtn"
                    id="btnREFRESH" name="btnREFRESH">
                  <i class="fa fa-refresh" aria-hidden="true"></i>
                  &nbsp;Refresh List
               </button>';*/
            }
            if ($disp[2]) {
               echo
               '<button type="button"
                    class="btn-cls4-red trnbtn"
                    id="btnCLOSE" name="btnCLOSE">
                  <i class="fa fa-times" aria-hidden="true"></i>
                  &nbsp;Close
               </button>';
            }
         }
         function EmployeesSearch($attr) {
            if ($GLOBALS['UserCode']!="COMPEMP") {
               $LName = $FName = $MName = $FullName = "";
               $empRefId = $whereClause = "";
               $Entries = 0;
               //$whereClause = "WHERE CompanyRefId = ".$_SESSION['CompanyId'];
               //$whereClause .= " AND BranchRefId = ".$_SESSION['BranchId'];
               if (!empty($attr["empRefId"])) {
                  $whereClause .= " WHERE RefId = '".$attr["empRefId"]."'";
                  $Entries++;
               } else {
                  $whereClause .= " WHERE RefId = 0";
               }


               if (!empty($attr["empLName"])) {
                  $whereClause .= " AND LastName LIKE '".$attr["empLName"]."%'";
                  $Entries++;
               }
               if (!empty($attr["empFName"])) {
                  $whereClause .= " AND FirstName LIKE '".$attr["empFName"]."%'";
                  $Entries++;
               }
               if (!empty($attr["empMName"])) {
                  $whereClause .= " AND MiddleName LIKE '".$attr["empMName"]."%'";
                  $Entries++;
               }
               $whereClause .= " AND (Inactive != 1 OR Inactive IS NULL)";
               //echo $whereClause;
               $emp = FindFirst("employees",$whereClause,"RefId, LastName, FirstName, MiddleName");

               if ($emp) {
                  $empRefId = $emp["RefId"];
                  $LName = $emp["LastName"];
                  $FName = $emp["FirstName"];
                  $MName = $emp["MiddleName"];
               } else {
                  $LName = $attr["empLName"];
                  $FName = $attr["empFName"];
                  $MName = $attr["empMName"];
                  $empRefId = $attr["empRefId"];
               }
               spacer(5);
               echo
               '<div class="mypanel">
                  <div class="panel-top bgIntroBlack">Search Employee:</div>
                  <div class="panel-mid" style="padding:5px;">
                     <div class="row">
                        <div class="col-xs-1" style="display:none;">
                           <label>Ref. Id</label><br>
                           <input type="text" class="form-input rptCriteria-- number--" name="txtRefId" placeholder="Emp.RefId" value="'.$empRefId.'"/>
                        </div>
                        <div class="col-xs-2">
                           <label>Last Name</label><br>
                           <input type="text" class="form-input rptCriteria--" name="txtLName" placeholder="Last Name" value="'.$LName.'"/>
                        </div>
                        <div class="col-xs-2">
                           <label>First Name</label><br>
                           <input type="text" class="form-input rptCriteria--" name="txtFName" placeholder="First Name" value="'.$FName.'"/>
                        </div>
                        <div class="col-xs-2">
                           <label>Middle Name</label><br>
                           <input type="text" class="form-input rptCriteria--" name="txtMidName" placeholder="Middle Name" value="'.$MName.'"/>
                        </div>
                        <div class="col-xs-5">
                           <div style="margin-top:20px;">
                              <a href="javascript:clearFields(\'txtLName,txtFName,txtMidName,txtRefId\');">
                                 <i class="fa fa-minus-square-o" aria-hidden="true" title="Clear Entry"></i>
                              </a>
                              <button type="button" name="btnEmpLkUp" class="btn-cls4-sea" id="btnEmpLkUp" title="Employees Look Up" style="margin-left:5px;">
                                 ==
                              </button>
                              <button type="submit" value="submit" class="btn-cls4-sea" name="submit" style="margin-left:5px;">
                                 <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;SUBMIT
                              </button>
                           </div>
                        </div>
                     </div>
                     <!--
                     <div class="row margin-top">
                        <div class="col-xs-3">';
                           /*$ftable = "Agency";
                           createSelect("Agency",
                                        "srch".$ftable."RefId",
                                        "",100,"Name","Select ".$ftable,"");*/
                        echo
                        '</div>
                        <div class="col-xs-3">';
                           /*$ftable = "Position";
                           createSelect($ftable,
                                        "srch".$ftable."RefId",
                                        "",100,"Name","Select ".$ftable,"");*/
                        echo
                        '</div>
                        <div class="col-xs-3">';
                           /*$ftable = "Office";
                           createSelect($ftable,
                                        "srch".$ftable."RefId",
                                        "",100,"Name","Select ".$ftable,"");*/
                        echo
                        '</div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-3">';
                           /*$ftable = "Department";
                           createSelect($ftable,
                                        "srch".$ftable."RefId",
                                        "",100,"Name","Select ".$ftable,"");*/
                        echo
                        '</div>
                        <div class="col-xs-3">';
                           /*$ftable = "Division";
                           createSelect($ftable,
                                        "srch".$ftable."RefId",
                                        "",100,"Name","Select ".$ftable,"");*/
                        echo
                        '</div>
                        <div class="col-xs-3">';
                           /*$ftable = "EmpStatus";
                           createSelect($ftable,
                                        "srch".$ftable."RefId",
                                        "",100,"Name","Select Employees Status","");*/
                        echo
                        '</div>
                     </div>
                     -->
                  </div>
                  <div class="panel-bottom"></div>
               </div>';
               /*echo
               '<script language="javascript">
                  remIconDL();
               </script>';*/
               modalEmpLkUp();
               return $empRefId;
            }
            return 0;
         }

         function btnEmplkUp($idx) {
            echo
            '<a href="javascript:void();" name="btnEmpLkUp'.$idx.'" id="btnEmpLkUp" title="Employees Look Up" style="margin-left:10px;">
               <i class="fa fa-bars" aria-hidden="true"></i>
            </a>';
         }

         function dispCitizen($val) {
            if ($val == 1) echo "Filipino";
                      else echo "Dual";
         }
         function dispYN($val) {
            if ($val == 1) echo "YES";
                      else echo "NO";
         }
         function dispCivilStatus($val) {
            switch ($val) {
               case "Si":
                  echo "[Si] Single";
               break;
               case "Ma":
                  echo "[Ma] Married";
               break;
               case "An":
                  echo "[An] Annuled";
               break;
               case "Wi":
                  echo "[Wi] Widowed";
               break;
               case "Se":
                  echo "[Se] Separated";
               break;
               case "Ot":
                  echo "[Ot] Others";
               break;
            }
         }

         function monthName($m,$j) {
            if ($j == 0) {
               $month = explode(",","Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec");
            } else {
               $month = explode(",","January,February,March,April,May,June,July,August,September,October,November,December");
            }
            return $month[$m - 1];
         }

         function convDate($date,$t) {
            $Date_Arr = explode("-",$date);
            return $Date_name = monthName($Date_Arr[1],$t)." ".$Date_Arr[2].", ".$Date_Arr[0];
         }

         function dateInterval($dateInterval,$date) {
            $d = strtotime($dateInterval, strtotime($date));
            return date("Y-m-d",$d);
         }

         function img($file) {
            if (isset($_SESSION['path'])) return $_SESSION['path']."images/".$file;
                                     else return $GLOBALS['path']."images/".$file;
         }
         function jsPath($file) {
            if (isset($_SESSION['path'])) return $_SESSION['path']."js/".$file.".js";
                                     else return $GLOBALS['path']."js/".$file.".js";
         }
         function jsCtrl($file) {

            if (isset($_SESSION['path'])) return $_SESSION['path']."js/ctrl/".$file.".js?t=".md5(time());
                                     else return $GLOBALS['path']."js/ctrl/".$file.".js?t=".md5(time());
         }
         function path($file) {
            if (isset($_SESSION['path'])) return $_SESSION['path'].$file;
                                     else return $GLOBALS['path'].$file;
         }

         function createInput($listArr) {
            for ($i=0; $i<count($listArr); $i++) {
               $list = $listArr[$i];
               $row = $list["row"];
               echo "\n";
               if ($row) {
                  echo
                  '<div class="row">
                     <div class="col-xs-'.$list["col"].'">';
               }
                     echo
                     '<div class="form-group">
                        <label class="control-label" for="'.$list["id"].'">'.$list["label"].':</label><br/>
                        <input type="text"
                               name="'.$list["name"].'"
                               id="'.$list["id"].'"
                               class="form-input saveFields-- '.$list["class"].'"';
                               if ($list["style"] != "") {
                                 echo ' style="'.$list["style"].'"';
                               }
                        echo
                        '>
                     </div>';
               if ($row) {
                     echo
                     '</div>
                  </div>';
               }
            }
         }
         function createButton($btnvalue,$name,$class,$icon,$others){
            echo
            '<button type="button"
                     class="'.$class.'"
                     id="'.$name.'" name="'.$name.'" '.$others.'>
                     <i class="fa '.$icon.'" aria-hidden="true"></i>
                     &nbsp;'.$btnvalue.'
            </button>';
         }
         function createFormFK($attr) {
            $table = $attr["table"];
            $objname = "sint_".$attr["table"]."RefId";
            $row = $attr["row"];
            if ($row) {
               echo
               '<div class="row">
                  <div class="col-xs-'.$attr["col"].'">';
            }
                     echo'
                     <div class="form-group">
                        <label class="control-label">
                           '.$attr["label"].':
                        </label><br/>';
                           createSelect($table,$objname,
                               "",100,"Name",
                               "--","");
                     echo
                     '</div>';

            if ($row) {
               echo
                  '</div>
               </div>';
            }
         }

         function timePicker($objName,$label) {
            echo
            '<span>';
               if ($label!="") {
                  '<label for="'.$objName.'">'.$label.'</label><br>';
               }
               echo
               '<select id="'.$objName.'" class="form-input saveFields-- time-- rptCriteria-- " name="'.$objName.'">';
                  echo '<option value="">--Time--</option>';
                  for ($j=1;$j<48;$j++) {
                     $interval = 30;
                     $minValue = $j * $interval;
                     $textValue_hr = floor(($j * $interval) / 60);
                     $textValue_min = ($j * $interval) % 60;
                     if ($textValue_min <= 9) {
                        $textValue_min = "0$textValue_min";
                     }
                     if ($textValue_hr >= 1 && $textValue_hr <= 9) {
                        $textValue_hr = "0$textValue_hr";
                     }
                     if ($textValue_hr == 0) {
                        $textValue_hr = "12";
                     }
                     $textValue = $textValue_hr.":".$textValue_min;
                     echo '<option value="'.$minValue.'" ';
                     if ($minValue == 420) {
                        echo "selected";
                     } else {
                        echo " ";
                     }
                     echo 
                     '>'.$textValue.'</option>';
                  }
               echo
               '</select>
            </span>';
         }

         function modalReject() {
            echo
            '<div class="modal fade" id="modalReject" role="dialog">
               <div class="modal-dialog">
                  <div class="mypanel">
                     <div class="panel-top bgSea">
                        <span id="modalTitle" style="font-size:11pt;">Reason?</span>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <div class="panel-mid">
                        <textarea class="form-input" style="height:100px;" name="char_Reason"></textarea><br>
                        <input type="hidden" id="hRefIdReject" name="hRefIdReject" value="0">
                        <input type="hidden" id="hRefIdReject" name="hRefIdReject" value="0">
                        <button type="button" name="btnPostReason" id="PostReason" class="btn-cls2-sea">Post Reason</button>
                     </div>
                  </div>
               </div>
            </div>';

         }

         function entryAlert($id,$msg) {
            echo
            '<div class="alert alert-warning alert--" for="'.$id.'">
               <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;
               <span>'.$msg.'</span>
            </div>';
         }

         function today() {
            echo date("Y-m-d",time());
         }

         function label($label,$for){
            echo '<label for="'.$for.'">'.$label.'</label>';
         }

         function updateListTable($attr) {
            include "conn.e2e.php";
                  echo
                  '<div class="row" style="width:100%">
                  <table border=1 style="width:100%;">
                     <tr align="center" class="bgTree">';
                        echo
                        '<th class="txt-center bgDark" style="width:80px;border-color:white;">#REFID</th>
                         <th class="txt-center padd5" style="width:100px;border-color:white;">ACTION</th>';
                        $flds = $attr["ColHdr"];
                        foreach($flds as $value) {
                           echo '<th class="txt-center" style="border-color:white;">'.$value.'</th>';
                        }
                     echo
                     '</tr>';
                     $rs = SelectEach($attr["dbtable"],$attr["clause"]);
                     if ($rs) {
                        while ($row = mysqli_fetch_array($rs)) {
                           $refid = $row["RefId"];
                           echo
                           '<tr>
                              <td class="txt-center padd5 bgDark">'.$refid.'</td>
                              <td class="txt-center padd5" nowrap>
                                 <a href="javascript:void(0);" style="text-decoration:none;margin-right:5px" title="Update This Record"
                                    class="trnIconEdit--">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"
                                    onclick="viewInfo('.$refid.',2,\'\');"></i>
                                 </a>
                              </td>';
                              $flds = $attr["ColFld"];
                              foreach($flds as $value) {
                                 echo '<td class="txt-center padd5">'.$row[$value].'</td>';
                              }
                           echo
                           '</tr>';
                        }
                     } else {
                        echo
                        '<tr>
                           <td colspan="'.(count($flds) + 2).'" class="txt-center padd5">NO RECORD AVAILABLE</td>
                        </tr>';
                     }
                  echo
                  '</table></div>';
         }

         function fnAvail() {
            include "conn.e2e.php";
            $sql    = "SELECT * FROM ".strtolower(getvalue("table"))." WHERE ".getvalue("crit");
            $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
            if ($result) {
               if (mysqli_num_rows($result) > 0) {
                  echo "true";
               } else {
                  echo "false";
               }
            } else {
               echo "false";
            }
            mysqli_close($conn);
         }

         function fnViewRecord() {
            $criteria  = " WHERE RefId = ".getvalue("refid")." LIMIT 1";
            $recordSet = f_Find(getvalue("table"),$criteria);
            $rowcount  = mysqli_num_rows($recordSet);
            $tableRS   = array();
            $tableRS   = mysqli_fetch_assoc($recordSet);
            echo json_encode($tableRS);
         }

         function chkBox($label,$isChk) {
            if ($isChk) {
               echo '<input type="checkbox" checked disabled>&nbsp;'.$label;
            } else {
               echo '<input type="checkbox" disabled>&nbsp;'.$label;
            }
         }

         function fnUpdateGridTable() {
            $table = strtolower(getvalue("table"));
            $GridHdr_Arr = explode("|",getvalue("gridHDR"));
            $GridFld_Arr = explode("|",getvalue("gridDTL"));
            $gridTable = getvalue("gridTable");
            $sql = "select * from `$table` order by RefId DESC";
            if (getvalue("whereClause") != "") {
               $sql = "select * from `$table` where ".getvalue("whereClause")." order by RefId DESC";
            }
            doGridTable($table,
                  $GridHdr_Arr,
                  $GridFld_Arr,
                  $sql,
                  [true,true,true,false],
                  $gridTable);
         }

         function createSelectTaxType() {
            echo '<label for="inputs">TYPE:</label><br>
                        <select class="form-input saveFields--" name="char_TaxType">
                           <option value="A">[A] Non-Taxable</option>
                           <option value="B">[B] Taxable</option>
                           <option value="C">[C] Taxable in excess of threshold</option>
                        </select>';

         }

         function createSelectMinimisType() {
            echo '<label  for="inputs">De Minimis Type:</label><br>
                        <select class="form-input saveFields--" name="char_MinimisType">
                           <option value="">n/a</option>
                           <option value="A">[A] Monetized unused 10 VL/SL - government</option>
                           <option value="B">[B] Medical cash allowance</option>
                           <option value="C">[C] Rice subsidy</option>
                           <option value="D">[D] Uniform and clothing allowance</option>
                           <option value="E">[E] Actual medical assistance</option>
                           <option value="F">[F] Laundry allowance</option>
                           <option value="G">[G] Employee\'s acheivement awards</option>
                           <option value="H">[H] Cash Gifts</option>
                           <option value="I">[I] Meal Allowance</option>
                        </select>';

         }

         function createSelectComputationType() {
            echo '<label  for="inputs">Computation Type:</label><br>
                        <select class="form-input saveFields--" name="char_ComputationType">
                           <option value="A">[A] Base on Attendance</option>
                           <option value="B">[B] Base on Attendance less Absences</option>
                           <option value="C">[C] Base on Current Basic</option>
                           <option value="D">[D] Base on No. of Days Inputted</option>
                           <option value="E">[E] Base on No. of Days Present</option>
                           <option value="F">[F] Base on No. of Working Days</option>
                           <option value="G">[G] Base on Pro-rated Basic</option>
                           <option value="H">[H] Base on Pro-rated Basic less Lost Hours</option>
                           <option value="I">[I] Base on Qualified Depends</option>
                           <option value="J">[J] Fixed amount</option>
                        </select>';

         }
         function createSelectPayrollGroup() {
            echo '<label  for="inputs">Payroll Group:</label><br>
                        <select class="form-input saveFields--" name="char_PayrollGroup">
                           <option value="A">[A] Bonuses and Cash Gifts</option>
                           <option value="B">[B] General Payroll</option>
                           <option value="C">[C] Other Payroll</option>
                           <option value="D">[D] Special Payroll</option>
                        </select>';

         }
         function createSelectITRClassification() {
            echo '<label for="inputs">ITR Classification</label><br>
                           <select class="form-input saveFields--" name="char_ITRClassification">
                              <option value="A">[A] Cost of Living Allowance</option>
                              <option value="B">[B] 13th Month Pay and Other Benefits</option>
                              <option value="C">[C] Basic Salary</option>
                              <option value="D">[D] Commissions</option>
                              <option value="E">[E] Cost of Living Allowance</option>
                              <option value="F">[F] De Minimis</option>
                              <option value="G">[G] Fees Including Director\'s Fee</option>
                              <option value="H">[H] Fixed Housing Allowance</option>
                              <option value="I">[I] Hazard Pay</option>
                              <option value="J">[J] Profit Sharing</option>
                              <option value="K">[K] Presentation</option>
                              <option value="L">[L] Salaries adn Other Forms of Compensation</option>
                              <option value="M">[M] Transportation</option>
                           </select>';

         }
         function createSelectAlphalistClassification() {
            echo '<label for="inputs">Alphalist Classification</label><br>
                           <select class="form-input saveFields--" name="char_AlphalistClassification">
                              <option value="A">[A] Basic Salary</option>
                              <option value="B">[B] 13th Month Pay and Other Benefits</option>
                              <option value="C">[C] Contributions adn Union Dues</option>
                              <option value="D">[D] De Minimis</option>
                              <option value="E">[E] Hazard Pay</option>
                              <option value="F">[F] Non-Payroll</option>
                              <option value="G">[G] Salaries adn Other Forms of Compensation</option>
                           </select>';

         }
         function remquote($str) {
            return str_replace("'","&#39;",$str);
         }

         function fileManager($str,$table,$flds,$values) {
            include "conn.e2e.php";
            $t = time();
            $date_today    = date("Y-m-d",$t);
            $curr_time     = date("H:i:s",$t);
            $trackingA_fld = "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
            $trackingA_val = "'$date_today', '$curr_time', 'PHP', 'M'";

            if ($str != ""){
               $flds = $flds.$trackingA_fld;
               $values = $values.$trackingA_val;
               $table = strtolower($table);
               $isExist = FindFirst($table,"where Name = '$str'","RefId");
               if (!$isExist) {
                  $sql = "INSERT INTO `$table` ($flds) VALUES ($values)";
                  if ($conn->query($sql) === TRUE) {
                     $RefId = mysqli_insert_id($conn);
                  }
               } else {
                  $RefId = $isExist;
               }
            }
            return $RefId;
         }

         function employeeSelector(){
            echo
               '<div class="mypanel">
                  <div class="panel-top" style="padding:10px;">
                     <div class="row">
                        <div class="col-xs-12">
                           Filter By:
                           <select name="filter_table" id="filter_table" class="form-input" onchange="populateFilter($(this).val())">
                              <option value="">Select Filter</option>
                              <option value="Division">Division</option>
                              <option value="Office">Office</option>
                              <option value="EmpStatus">Employment Status</option>
                              <option value="Department">Department</option>
                           </select>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           Filter Value:
                           <select name="filter_value" id="filter_value" class="form-input">
                           </select>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           Employee Name:
                           <input type="text" class="form-input" name="SearchEmployees" placeholder="Enter Employee Name">
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-6">
                           Status:
                           <select name="Inactive" id="Inactive" class="form-input">
                              <option value="0" selected>ACTIVE</option>
                              <option value="1">INACTIVE</option>
                           </select>
                        </div>
                        <div class="col-xs-6">
                           &nbsp;<br>
                           <button type="button"
                                   class="btn-cls4-lemon"
                                   id="btnSearchEmployees"
                                   name="btnSearchEmployees" style="width: 100%;">
                              <i class="fa fa-search" aria-hidden="true"></i>Search
                           </button>      
                        </div>
                     </div>
               ';
                     //createButton("","btnSearchEmployees","btn-cls4-lemon","fa-search","");
                     //createButton("","btnSearchEmployees","btn-cls4-lemon","fa-search","");
                  echo
                  '</div>
                  <div class="panel-mid" style="height:500px;overflow:auto;padding:0px;" id="divEmployeesList">';
                  $rsEmployees = SelectEach("employees","WHERE Inactive != 1 OR Inactive IS NULL order by LastName, FirstName LIMIT 100");
                  if ($rsEmployees) {
                     if (mysqli_num_rows($rsEmployees) > 0) {
                        $count = 0;
                        while ($rows = mysqli_fetch_array($rsEmployees)) {
                           if ($rows["LastName"] != "" &&
                              $rows["FirstName"] != "") {
                              $count++;
                              echo '<div class="Employees--" refid="'.$rows["RefId"].'" style="padding:6px;">';

                              //echo '<a href="javascript:void(0);">'.$count.' - ['.$rows["RefId"].'] '.strtoupper($rows["LastName"]).", ".strtoupper($rows["FirstName"]).'</a>';

                              echo $count.' - '.strtoupper($rows["LastName"]).", ".strtoupper($rows["FirstName"]);

                              echo '</div>';
                           }
                        }
                     }
                  }
               echo
                  '</div>
                  <div class="panel-bottom" id="RefIdSelected">
                     <input type="hidden" id="hRefIdSelected" name="hRefIdSelected" value="">
                  </div>
               </div>';
         }

         function getMAC() {
            /*
            * Getting MAC Address using PHP
            * PHP Fresher
            */

            ob_start(); // Turn on output buffering
            system('ipconfig /all'); //Execute external program to display output
            $mycom = ob_get_contents(); // Capture the output into a variable

            ob_clean(); // Clean (erase) the output buffer

            $findme = "Physical";
            $pmac = strpos($mycom, $findme); // Find the position of Physical text
            $pcfind = "Host Name";
            $pcname = strpos($mycom, $pcfind);
            $pc = substr($mycom,($pcname+36),10);
            $mac = substr($mycom,($pmac+36),17); // Get Physical Address
            $mac = str_replace("-","",$mac);

            return $mac." - ".$pc;
         }
         
         function getFieldDataType($p_dbtable,$p_dbfield) {
            include "conn.e2e.php";  
            if ($p_dbfield != "") {
               $p_dbtable = strtolower($p_dbtable);
               $sql_ = "SHOW FULL COLUMNS FROM `{$p_dbtable}` WHERE `Field` = '{$p_dbfield}'";
               $Colresult = mysqli_query($conn,$sql_) or die(mysqli_error($conn));
               if ($Colresult) {
                  $type = mysqli_fetch_assoc($Colresult)["Type"];
                  if ($type != "") {
                     if (stripos($type,"ate")) {
                        return "date";
                     } else if (stripos($type,"nt")) {
                        return "integer";
                     } else if (stripos($type,"eci")) {
                        return "decimal";
                     } else {
                        return $type;
                     }
                     
                  } else {
                     return "";
                  }
               } else {
                  return "";
               }
            } else {
               return "";
            }
         }        
   
         function set_Field_Value($trnmode,$tab,$post,$conn,$p_dbtable) {
            $Fields = "";
            $Values = "";
            if ($trnmode == "NEWSAVE") {
               foreach ($tab as $key => $value) {
                  $objValue = $post[$value[1]];
                  if ($objValue != "") {
                     $Fields .= $value[0].", ";
                     $dataType = getFieldDataType($p_dbtable,$value[0]);
                     if ($dataType == "integer" || $dataType == "decimal") {
                        if ($objValue == '') {
                           $objValue = 0;
                        } 
                     } else if ($dataType == "date") {
                        if ($objValue == '') {
                           $objValue = '1901-01-01'; // meaning blanko
                        } 
                     } 
                     $objValue = mysqli_real_escape_string($conn,$objValue);
                     $Values .= "'{$objValue}', ";
                  }
               }
               return $Fields."║".$Values;
            } else {
               if ($trnmode == "EDITSAVE") {
                  foreach ($tab as $key => $value) {
                     $objValue = $post[$value[1]];
                     $dataType = getFieldDataType($p_dbtable,$value[0]);
                     if ($dataType == "integer" || $dataType == "decimal") {
                        if ($objValue == '') {
                           $objValue = 0;
                        } 
                     } else if ($dataType == "date") {
                        if ($objValue == '') {
                           $objValue = '1901-01-01'; // meaning blanko
                        } 
                     } 
                     $Values = mysqli_real_escape_string($conn,$objValue);
                     $Fields .= "`{$value[0]}` = '{$Values}', ";
                  }
                  return $Fields;
               } 
            }
         }

         function getLabel_AttendanceEntry($k) {
            switch ($k) {
               case 1:
                  return "Time In";
               break;
               case 2:
                  return "Lunch Out";
               break;
               case 3:
                  return "Lunch In";
               break;
               case 4:
                  return "Time Out";
               break;
               case 5:
                  return "OT In";
               break;
               case 6:
                  return "OT Out";
               break;
            }
         }

         function module($json) {
            $moduleContent = file_get_contents(json.$json.'.json');
            return json_decode($moduleContent, true);
         }         
         function yearPicker($i) {
            $yr = 1950;
            for ($j=$i; $j>=$yr; $j--) {
               echo '<option value="'.($j).'">'.($j).'</option>';
            }
         }
         function CValue($str) {
            $str = trim($str);
            switch ($str) {
               case "1901-01-01":
                  $value = "";
                  break;
               case "2999-12-31":
                  $value = "Present";
                  break;
               default:
                  $value = $str;
                  break;
            }
            return $value;
         }

         function setValue_RefId($str) {
            if ($str == "") {
               return 0;
            } else { 
               return $str; 
            }
         }

         function realEscape($strxx) {
            include "conn.e2e.php";   
            return mysqli_real_escape_string($conn,$strxx); 
         }

         function objSetValue($objName,$Str__) {
            $Str__ = str_replace(array("\n\r", "\n", "\r"), " ", $Str__);
            if ($Str__ == "1901-01-01") {
               $Str__ = "";
            }
            $Str__ = CValue($Str__);
            echo '$("[name=\''.$objName.'\']").val("'.$Str__.'");'."\n";
         }

         function setDefault($name) {
            include "conn.e2e.php";
            $value = getvalue($name);
            $name_arr = explode("_", $name);
            $FieldType = $name_arr[0];
            if ($FieldType == "sint" ||
                $FieldType == "bint" ||
                $FieldType == "deci") {
               if ($value == "") {
                  $newVal = 0;
               } else {
                  $newVal = $value;
               }
            } else if ($FieldType == "date") {
               if ($value == "") {
                  $newVal = "1901-01-01";
               } else {
                  if ($value == "Present") {
                     $newVal = "2999-12-31";
                  } else {
                     $date_arr = explode("-", $value);
                     if ($date_arr[0] == 0 || $date_arr[1] == 0 || $date_arr[2] == 0){
                        $newVal = "1901-01-01";
                     } else {
                        $newVal = $value;
                     }
                  }
               }
            } else {
               $newVal = $value;
            }
            $newVal = trim($newVal);
            return mysqli_real_escape_string($conn,$newVal);
            /*
            echo "Value: ".$newVal."<br>";
            echo "Type: ".$FieldType."<br>";
            echo "Name: ".$name_arr[1]."<br>--------------------<br>";
            if ($newVal == NULL) {
               return NULL;
            } else {
               $newVal = trim($newVal);
               return mysqli_real_escape_string($conn,$newVal);   
            }
            */
            //return htmlentities($newVal);
         }
         function balloonMsg($msg,$type) {
            echo
            '<script language="JavaScript">
               $.notify("'.$msg.'", "'.$type.'");
            </script>';
         }

         function get_today_minute($utc) {
            $baseline = strtotime(date("Y-m-d",$utc));
            return intval(($utc - $baseline) / 60);
         }

         function get_minute($time_in,$time_out) {
            return ((($time_in - $time_out) / 60) - 60);
         }
         function HrsFormat($timeInMin) {
            if ($timeInMin > 0) {
               $hr = explode(".",$timeInMin / 60)[0];
               $ampm = "AM";
               if ($hr > 12) {
                  $hr = $hr - 12;
                  $ampm = "PM";
               }
               if ($hr == 12) $ampm = "PM";
               if ($hr <= 9) {
                  $hr = "0".$hr;
               }
               $mod_min = ($timeInMin % 60);
               if ($mod_min <= 9) {
                  $mod_min = "0".$mod_min;
               }
               return $hr.":".$mod_min." ".$ampm;   
            } else {
               return "&nbsp;";
            }
         }
         function dateDifference($date_1 , $date_2 , $differenceFormat = '%a' ) {
            $datetime1 = date_create($date_1);
            $datetime2 = date_create($date_2);
            $interval = date_diff($datetime1, $datetime2);
            return $interval->format($differenceFormat);
         }
         function getEquivalent($value,$table) {
            if ($value != "" || $value != 0) {
               if ($value <= 60) {
                  $EQDay = FindFirst($table,"WHERE NoOf = ".$value,"EquivalentDay");
                  return $EQDay; 
               } else {
                  $initial = 0;
                  $whole = explode(".",$value / 60)[0];
                  $rem = $value % 60;
                  for ($i=1; $i <= $whole; $i++) { 
                     $EQDay = FindFirst($table,"WHERE NoOf = 60","EquivalentDay");
                     $initial = $initial + $EQDay;
                  }
                  $EQDay = FindFirst($table,"WHERE NoOf = ".$rem,"EquivalentDay");
                  return ($EQDay + $initial);
               }
            } else {
               return "0";
            }
         }

         function rptDefaultValue($value,$FK = "") {
            $value = trim($value);
            if ($value != "") {
               if ($FK != "") {
                  $table = $FK;
                  $value = getRecord($table,$value,"Name");
                  return $value;
               } else {
                  return $value;   
               }
            } else {
               return "&nbsp;";
            }   
         }

         function rpt_HoursFormat($timeInMin) {
            if ($timeInMin > 0) {
               $hr = explode(".",$timeInMin / 60)[0];
               if ($hr <= 9) {
                  $hr = "0".$hr;
               }
               $mod_min = ($timeInMin % 60);
               if ($mod_min <= 9) {
                  $mod_min = "0".$mod_min;
               }
               return $hr.":".$mod_min;   
            } else {
               return "&nbsp;";
            }   
         }  
         function savePMS($table,$flds,$vals,$createdby = 0) {
            include 'conn.e2e.php';
            $newFld = $flds."`created_by`, `created_at`";
            $newVal = $vals."'$createdby', '".date("Y-m-d H:i",time())."'";
            $sql = "INSERT INTO $table ($newFld) VALUES ($newVal)";
            $result = mysqli_query($conn,$sql);
            if ($result) {
               return mysqli_insert_id($conn);
            } else {
               return false;
            }
            mysqli_close($conn);
         }
         function updatePMS($table,$fldnval,$where,$createdby = 0) {
            include 'conn.e2e.php';
            $newFldnval = $fldnval."`updated_by` = '$createdby', updated_at = '".date("Y-m-d H:i",time())."'";
            $sql = "UPDATE $table SET $newFldnval $where";
            $result = mysqli_query($conn,$sql);
            if ($result) {
               return "";
            } else {
               return false;
            }
         }
         function pms_FindLast($table,$whereClause,$fld) {
            include 'conn.e2e.php';
            if ($fld == "*") {
               $sql = "SELECT * FROM `".strtolower($table)."` ".$whereClause." order by id DESC LIMIT 1";
            } else {
               $sql = "SELECT $fld FROM `".strtolower($table)."` ".$whereClause." order by id DESC LIMIT 1";
            } 
            $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
            if ($result) {
               $row = mysqli_fetch_assoc($result);
               $numrow = mysqli_num_rows($result);
               mysqli_close($conn);
               if ($numrow <= 0) {
                  return false;
               } else {
                  if ($fld == "*") return $row;
                              else return $row[$fld];

               }
            } else {
               return false;
            }
         }
         function pms_FindFirst($table,$whereClause,$fld = "*") {
            include 'conn.e2e.php';

            if ($fld == "*") {
               $sql = "SELECT * FROM `".strtolower($table)."` ".$whereClause." limit 1";
            } else if (stripos($fld,",") > 0) {
               $sql = "SELECT $fld FROM `".strtolower($table)."` ".$whereClause." limit 1";
            } else {
               $sql = "SELECT $fld FROM `".strtolower($table)."` ".$whereClause." limit 1";
            }
            $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
            $row = mysqli_fetch_assoc($result);
            $numrow = mysqli_num_rows($result);
            mysqli_close($conn);
            if ($numrow <= 0) {
               return false;
            } else {
               if ($fld == "*" || stripos($fld,",") > 0) {
                  return $row;
               } else {
                  return $row[$fld];
               }
            }
         }
         function pms_SelectEach($table,$whereClause) {
            include 'conn.e2e.php';
            $sql = "SELECT * FROM `".strtolower($table)."` ".$whereClause;
            $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
            mysqli_close($conn);
            if (mysqli_num_rows($result) > 0) return $result;
            else return false;
         }
         function convertToHoursMins($time, $format = '%02d:%02d') {
            if ($time < 1) {
               return;
            }
            $hours = floor($time / 60);
            $minutes = ($time % 60);
            return sprintf($format, $hours, $minutes);
         }
         function get_empid() {
            include 'conn.e2e.php';
            $sql = "SELECT AgencyId FROM employees ORDER BY AgencyId DESC LIMIT 1";
            $rs = mysqli_query($conn,$sql);
            if ($rs) {
               while ($row = mysqli_fetch_assoc($rs)) {
                  $empid = $row["AgencyId"];
                  $count = substr($empid, 4) + 1;
                  $count = str_pad($count, 4, '0',STR_PAD_LEFT);
                  $new_empid = date("Y",time()).$count;
                  return $new_empid;
               }
            }  
         }
         function get_ControlNumber($code = "") {
            include 'conn.e2e.php';
            $sql = "SELECT ControlNumber FROM employeesauthority WHERE ControlNumber LIKE '%$code%' ORDER BY ControlNumber DESC LIMIT 1";
            $new_sequence = 1;
            $rs = mysqli_query($conn,$sql);
            if ($rs) {
               while ($row = mysqli_fetch_assoc($rs)) {
                  $ctrl_number = $row["ControlNumber"];
                  if ($ctrl_number != "") {
                     $arr = explode("-", $ctrl_number);
                     $sequence = $arr[2];
                     $sequence = intval($sequence);   
                  } else {
                     $sequence = 0;
                  }
                  $new_sequence = $sequence + 1;
               }
            }
            $new_sequence = str_pad($new_sequence, 3, '0',STR_PAD_LEFT);
            return $new_sequence;
         }
         function count_leave($emprefid,$from,$to) {
            $result        = 0;
            $worksched     = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","WorkScheduleRefId");
            $arr        = array();
            $leave_count   = dateDifference($from,$to);
            for ($i=0; $i <= $leave_count ; $i++) { 
               $leave_date = date("Y-m-d",strtotime($from." + $i day"));
               $arr[$leave_date] = ["Holiday"=>"", "RestDay"=>"" , "Suspension"=> ""];
            }
            $holiday_rs = SelectEach("holiday","");
            if ($holiday_rs) {
               while ($row = mysqli_fetch_assoc($holiday_rs)) {
                  $curr_year  = date("Y",time());
                  $every_year = $row["isApplyEveryYr"];
                  if ($every_year == "1") {
                     $curr_start_date_arr = explode("-", $row["StartDate"]);
                     $curr_end_date_arr   = explode("-", $row["EndDate"]);
                     $Start               = $curr_year."-".$curr_start_date_arr[1]."-".$curr_start_date_arr[2];
                     $End                 = $curr_year."-".$curr_end_date_arr[1]."-".$curr_end_date_arr[2];
                  } else {
                     $Start   = $row["StartDate"];
                     $End     = $row["EndDate"];
                  }
                  if ($Start == $End) {
                     if (isset($arr[$Start])) {
                        $arr[$Start]["Holiday"] = 1;  
                     }
                  } else {
                     $days_count = dateDifference($Start,$End);
                     for ($i=0; $i <= $days_count; $i++) { 
                        $holiday_date = date("Y-m-d",strtotime($Start." + $i day"));
                        if (isset($arr[$holiday_date])) {
                           $arr[$holiday_date]["Holiday"] = 1; 
                        }
                     }
                  }
               }
            }

            //$suspension_rs = SelectEach("officesuspension","");
            if ($worksched) {
               $workschedule = FindFirst("workschedule","WHERE RefId = '$worksched'","*");
               if ($workschedule) {
                  foreach ($arr as $key => $value) {
                     $day = date("D",strtotime($key));
                     switch ($day) {
                        case 'Mon':
                           $day = "Monday";
                           break;
                        case 'Tue':
                           $day = "Tuesday";
                           break;
                        case 'Wed':
                           $day = "Wednesday";
                           break;
                        case 'Thu':
                           $day = "Thursday";
                           break;
                        case 'Fri':
                           $day = "Friday";
                           break;
                        case 'Sat':
                           $day = "Saturday";
                           break;
                        case 'Sun':
                           $day = "Sunday";
                           break;
                     }
                     if ($workschedule[$day."isRestDay"] == 1) {
                        $arr[$key]["RestDay"] = 1;
                     }
                  }  
               }  
            }
            foreach ($arr as $key => $value) {
               $Holiday = $value["Holiday"];
               $RestDay = $value["RestDay"];
               if ($Holiday != "1" && $RestDay != "1") {
                  $result++;
               }
            }
            return $result;
         }
         function computeAge($bday,$param = "today"){
            return date_diff(date_create($bday), date_create($param))->y;
         }

         function updateDate($table,$fld,$value,$refid){
            include 'conn.e2e.php';
            if ($value == "") {
               $sql = "UPDATE $table SET $fld = NULL WHERE RefId = '$refid'";
            } else {
               $sql = "UPDATE $table SET $fld = '$value' WHERE RefId = '$refid'";
            }
            $rs = mysqli_query($conn,$sql);
            if ($rs) {
               return "";
            } else {
               return false;
            }
            mysqli_close($conn);

         }

         function computeCredit($emprefid,$start,$end,$year,$coc_year = "") {
            $vl       = 0;
            $sl       = 0;
            $ot       = 0;
            $spl      = 0;
            $fl       = 0;
            $new_vl   = 0;
            $new_sl   = 0;
            $lwop     = 0;

            $start    = intval($start);
            $end      = intval($end);
            if ($start <= 9) $start = "0".$start;
            if ($end <= 9) $end = "0".$end;

            $coc_where  = "WHERE EmployeesRefId = '$emprefid' AND EffectivityYear = '$coc_year' AND NameCredits = 'OT'";
            $COC        = FindFirst("employeescreditbalance",$coc_where,"BeginningBalance");
            if ($COC) {
               $ot = $COC;
            }
            $balance    = SelectEach("employeescreditbalance","WHERE EmployeesRefId = '$emprefid' AND EffectivityYear = '$year'");
            if ($balance) {
               while ($row_balance = mysqli_fetch_assoc($balance)) {
                  $credit_name = $row_balance["NameCredits"];
                  switch ($credit_name) {
                     case 'VL':
                        $vl = $row_balance["BeginningBalance"];
                        $fl = $row_balance["ForceLeave"];
                        break;
                     case 'SL':
                        $sl = $row_balance["BeginningBalance"];
                        break;
                     case 'SPL':
                        $spl = $row_balance["BeginningBalance"];
                        break;
                  }
               }
            }
            if ($start <= $end) {
               $where = "WHERE EmployeesRefId = '$emprefid' AND Month >= '$start' AND Year = '$year' AND Month <= '$end'";
               $rs = SelectEach("dtr_process",$where);
               if ($rs) {
                  while ($row = mysqli_fetch_assoc($rs)) {
                     $where_monetization = "WHERE EmployeesRefId = '$emprefid'";
                     $where_monetization .= " AND MONTH(FiledDate) = '".$row["Month"]."'";
                     $where_monetization .= " AND YEAR(FiledDate) = '".$year."'";
                     $where_monetization .= " AND Status = 'Approved'";
                     $monetize   = FindFirst("employeesleavemonetization",$where_monetization,"*");
                     if ($monetize) {
                        $vl_monetize = $monetize["VLValue"];
                        $sl_monetize = $monetize["SLValue"];
                     } else {
                        $vl_monetize = $sl_monetize = 0;
                     }
                     $coc        = $row["Total_COC_Hr"];
                     $absent     = $row["Total_Absent_Count"];
                     $vl_earned  = $row["VL_Earned"];
                     $sl_earned  = $row["SL_Earned"];
                     $spl_used   = $row["SPL_Used"];
                     $coc_used   = $row["COC_Used"];
                     $tardy      = $row["Tardy_Deduction_EQ"];
                     $undertime  = $row["Undertime_Deduction_EQ"];
                     $vl_used    = $row["VL_Used"];
                     $sl_used    = $row["SL_Used"];
                     $fl_days    = $row["FL_Days"];
                     $fl_used    = 0;
                     if ($fl_days != "") {
                        $fl_arr = explode("|", $fl_days);
                        foreach ($fl_arr as $key => $value) {
                           if ($value != "") {
                              $fl_used++;
                           }
                        }
                     }
                     if ($coc >= 2400) $coc = 2400;
                     $ot         = $ot + $coc - $coc_used;
                     $spl        = $spl - $spl_used;
                     $deduction  = $tardy + $undertime + $absent + $vl_used + $vl_monetize;
                     if ($new_vl == 0) {
                        $new_vl = $vl - $deduction;
                     } else {
                        $new_vl = $new_vl - $deduction;
                     }
                     if ($new_vl < 0) {
                        $lwop = abs($new_vl);
                        $new_vl = 0;
                     }
                    

                     if ($new_sl == 0) {
                        $new_sl = $sl - ($sl_used + $sl_monetize);
                     } else {
                        $new_sl = $new_sl - ($sl_used + $sl_monetize);
                     }

                     $new_vl += floatval($vl_earned);
                     $new_sl += floatval($sl_earned);
                     $fl -= $fl_used;
                  }
               } else {
                  $new_vl = $vl;
                  $new_sl = $sl;
               }
            } else {
               $new_vl = $vl;
               $new_sl = $sl;
            }
            
            $arr["VL"]  = $new_vl;
            $arr["SL"]  = $new_sl;
            $arr["OT"]  = $ot;
            $arr["SPL"] = $spl;
            $arr["FL"]  = $fl;
            return $arr;
         }

         function convertBR($str) {
            $str = str_replace(array("\r\n","\r","\n","\\r\\n"),"<br>",$str);
            return $str;
         }
         function convertNumberToWord($num = false) {
            $num = str_replace(array(',', ' '), '' , trim($num));
            if(! $num) {
               return false;
            }
            $num = (int) $num;
            $words = array();
            $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
                 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
            );
            $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
            $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
                 'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
                 'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
            );
            $num_length = strlen($num);
            $levels = (int) (($num_length + 2) / 3);
            $max_length = $levels * 3;
            $num = substr('00' . $num, -$max_length);
            $num_levels = str_split($num, 3);
            for ($i = 0; $i < count($num_levels); $i++) {
               $levels--;
               $hundreds = (int) ($num_levels[$i] / 100);
               $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
               $tens = (int) ($num_levels[$i] % 100);
               $singles = '';
               if ( $tens < 20 ) {
                  $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
               } else {
                  $tens = (int)($tens / 10);
                  $tens = ' ' . $list2[$tens] . ' ';
                  $singles = (int) ($num_levels[$i] % 10);
                  $singles = ' ' . $list1[$singles] . ' ';
               }
               $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
            }
            $commas = count($words);
            if ($commas > 1) {
               $commas = $commas - 1;
            }
            return implode(' ', $words);
         }


         function application_filter() {
            bar();
            echo '<div class="row">';
            echo '
               <div class="col-xs-2">
                  <select class="form-input" id="month_" name="month_">
                     <option value="0">Select Month</option>
                     <option value="01">January</option>
                     <option value="02">February</option>
                     <option value="03">March</option>
                     <option value="04">April</option>
                     <option value="05">May</option>
                     <option value="06">June</option>
                     <option value="07">July</option>
                     <option value="08">August</option>
                     <option value="09">September</option>
                     <option value="10">October</option>
                     <option value="11">November</option>
                     <option value="12">December</option>
                  </select>
               </div>
            ';
            echo '
               <div class="col-xs-2">
                  <select class="form-input" id="year_" name="year_">
                     <option value="0">Select Year</option>
            ';

                        $start   = date("Y",time()) - 2;
                        $end     = date("Y",time());
                        for ($a=$start; $a <= $end ; $a++) { 
                           echo '<option value="'.$a.'">'.$a.'</option>';
                        }
            echo '
                  </select>
               </div>
            ';    
            echo '
                  <div class="col-xs-8">
                     <button type="button" name="filter_button" id="filter_button" class="btn-cls4-sea">FILTER</button>
                     &nbsp;&nbsp;<i>Filter is based on <b>filed date</b> of the application.</i>
                  </div>
               </div>
            ';
            bar();
            echo '<br>';
         }
         function sync_pms($emprefid,$mode,$AgencyId = "") {
            $Company          = FindFirst("employees","WHERE RefId = '$emprefid'","CompanyRefId");
            if (
                  // $Company == "28" || 
                  // $Company == "1000" || 
                  // $Company == "2" || 
                  // $Company == "35" || 
                  // $Company == "21"
                  $Company > 0
               ) {
               $Sync             = file_get_contents("upl/json/pis_pms_sync.json");
               $Sync_arr         = json_decode($Sync, true);
               if ($AgencyId == "") {
                  $AgencyId         = FindFirst("employees","WHERE RefId = '$emprefid'","AgencyId");   
               }
               
               $PMS_emprefid     = pms_FindFirst("pms_employees","WHERE employee_number = '$AgencyId'","id");
               $fldnval          = "";
               $fld              = "employee_id,";
               $val              = "'$PMS_emprefid',";
               $refid            = "";
               $table            = "";
               $error            = 0;
               if (is_numeric($PMS_emprefid)) {
                  switch ($mode) {
                     case '1':
                        $table   = "pms_employees";
                        $where   = "WHERE id = '$PMS_emprefid'";
                        $row     = FindFirst("employees","WHERE RefId = '$emprefid'","*");
                        if ($row) {
                           foreach ($Sync_arr[$table] as $nkey => $nvalue) {
                              if ($row[$nkey] != "") {
                                 $fldnval .= "`$nvalue` = '".$row[$nkey]."',"; 
                              }
                           }
                        } 
                     break;
                     case '2':
                        $table   = "pms_employee_information";
                        $where   = "WHERE employee_id = '$PMS_emprefid'";
                        $row     = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
                        if ($row) {
                           foreach ($Sync_arr[$table] as $nkey => $nvalue) {
                              if (intval($row[$nkey]) > 0) {
                                 $fldnval .= "`$nvalue` = '".$row[$nkey]."',";   
                                 $fld     .= "`$nvalue`, ";
                                 $val     .= "'".$row[$nkey]."', ";  
                              }
                           }
                        }
                     break;
                     case '3':
                        $table   = "pms_salaryinfo";
                        $where   = "WHERE employee_id = '$PMS_emprefid'";
                        $row     = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
                        if ($row) {
                           foreach ($Sync_arr[$table] as $nkey => $nvalue) {
                              if (intval($row[$nkey]) > 0) {
                                 $fldnval .= "`$nvalue` = '".$row[$nkey]."',";   
                                 $fld     .= "`$nvalue`, ";
                                 $val     .= "'".$row[$nkey]."', ";  
                              }
                           }
                           $empstatus = $row["EmpStatusRefId"];
                           $check_plantilla = getRecord("empstatus",$empstatus,"category");
                           $check_plantilla = intval($check_plantilla);
                           if ($check_plantilla == 0) {
                              $fldnval .= "`salary_effectivity_date` = '".$row["StartDate"]."',";   
                              $fld     .= "`salary_effectivity_date`, ";
                              $val     .= "'".$row["StartDate"]."', ";  
                           }
                        }
                     break;
                  }
               } else {
                  // echo "No Employee Id Match";
                  // return false;
                  return "No Employee Id Match";
               }
               if ($fldnval != "") {
                  if ($mode == 1) {
                     $update = updatePMS($table,$fldnval,$where);
                     if ($update != "") {
                        $error++;  
                     }
                  } else {
                     if ($mode == 2) {
                        $check = pms_FindFirst($table,"WHERE employee_id = '$PMS_emprefid'","id");
                     } else if ($mode == 3) {
                        $check = pms_FindLast($table,"WHERE employee_id = '$PMS_emprefid'","id");
                        $where = "WHERE id = '$check'";
                     }
                     if (intval($check) > 0) {
                        $update = updatePMS($table,$fldnval,$where);
                        if ($update != "") {
                           $error++;  
                        }  
                     } else {
                        $save = savePMS($table,$fld,$val);
                        if (!is_numeric($save)) {
                           $error++;
                        }
                     }
                  }
                  if ($error == 0) {
                     return "";
                  } else {
                     return "Error";
                  }
               }
            } else {
               return "";
            }
         }
         
   }

?>