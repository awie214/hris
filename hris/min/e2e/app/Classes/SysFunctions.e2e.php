<?php
   $user = getvalue("hUser");
   class SysFunctions {
      public function __construct() {
      }
      public function resources($file) {
         return "../resources/views/pages/".$file;
      }
      public function publicPath($filePath) {
         return $_SESSION["path"].$filePath;
      }
      public function img($file) {
         return $_SESSION["path"]."images/".$file;
      }
      public function js($file) {
         return $_SESSION["path"]."js/".$file;
      }
      public function jsCtrl($file) {
         return $_SESSION["path"]."js/ctrl/".$file;
      }
      public function css($file) {
         return $_SESSION["path"]."css/".$file;
      }
      public function upload($file) {
         return $_SESSION["path"]."uploads/".$file;
      }
      public function syslog($file) {
         return $_SESSION["path"]."syslog/".$file;
      }
      public function pds($file) {
         return $_SESSION["path"]."pds/".$file;
      }

      public function host($filepath) {
         return "http://".$_SERVER['HTTP_HOST']."/".$filepath;
      }
      public function destroy_css($str) {
         foreach(glob($_SESSION["path"].'css/'.$str.'*.css') as $file) {
            unlink($file);
         }
         return true;
      }
      public function css_create($css) {
         /*$isMIN = true;
         $file = "az.css";
         $mynewfile = $_SESSION["cssFile"];
         $dirTarget = $_SESSION["path"]."css";
         $dir = $_SESSION["path"]."css";
         $myfile = fopen($dir."/".$file, 'r') or die("Unable to open file!");
         $newFile = fopen($dirTarget."/".$mynewfile, 'w');
         $str = "";
         while(!feof($myfile)) {
            if ($isMIN) $str = trim(fgets($myfile));
                   else $str = fgets($myfile);
            $str = str_replace("||maincolor1||","var(--".$css[0].")",$str);
            $str = str_replace("||maincolor2||","var(--".$css[1].")",$str);
            $str = str_replace("||mainbg||","var(--".$css[2].")",$str);
            if (stripos($str,"//") > -1) {
               fwrite($newFile,substr($str,0,stripos($str,"//")));
               fwrite($newFile,"\n".substr($str,stripos($str,"//"))."\n");
            }
            else fwrite($newFile,$str);
         }
         fclose($newFile);
         fclose($myfile);*/
         $mynewfile = $_SESSION["path"]."css/az.css";
         return $mynewfile;
      }

      /*public function unlinkCSS() {
         $dir = $_SESSION["path"]."css";
         if (is_dir($dir)){
            if ($dh = opendir($dir)){
               while (($file = readdir($dh)) !== false){
                  echo "filename:" . $file . "<br>";
                  $path = $_FILES['image']['name'];
                  $ext = pathinfo($dir."/"., PATHINFO_EXTENSION);
               }
               closedir($dh);
            }
         }
      }*/

      public function min($file,$mynewfile,$dir_extname,$isMIN) {
         $dirTarget = $_SESSION["path"].$dir_extname[0];
         $dir = $_SESSION["path"].$dir_extname[0];
         $myfile = fopen($file, 'r') or die("Unable to open file!");
         $prefix = ".e2e.not";
         if ($isMIN) $prefix = ".e2e.min";
         $mynewfile = $mynewfile.date("Ymd",time());
         $mynewfile = $dir_extname[1].$dir_extname[2].f_encode($mynewfile).$prefix.".".$dir_extname[1];
         $newFile = fopen($dirTarget."/".$mynewfile, 'w');
         $str = "";
         while(!feof($myfile)) {
            if ($isMIN) $str = trim(fgets($myfile));
                   else $str = fgets($myfile);
            if (stripos($str,"//") > -1) {
               fwrite($newFile,substr($str,0,stripos($str,"//")));
               fwrite($newFile,"\n".substr($str,stripos($str,"//"))."\n");
            }
            else fwrite($newFile,$str);
         }
         fclose($newFile);
         fclose($myfile);
         return $mynewfile;
      }


      public function conn($dbname){
         if (phpversion() >= 5.5) {
            $servername = "localhost";
            $username = "root";
            $password = "az052006";
            $dbname = $dbname;
            if ($_SERVER["SERVER_NAME"] == "my-e2e.rf.gd") {
               $servername = "sql208.rf.gd";
               $username = "rfgd_20378909";
               $password = "az052006";
               $dbname = "rfgd_20378909_e2e";
            }
            // Create connection
            mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
            //$conn = mysqli_connect($servername, $username, $password, $dbname);
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection OOP
            if ($conn->connect_error) {
               die("Connection failed: " . $conn->connect_error);
            }
            // --------------------------------------------------------------------------------------------------
            // create connection
            $dbcon = $conn;
            //mysql_select_db($dbname, $dbcon);
            $db = new PDO('mysql:host='.$servername.';dbname='.$dbname.';charset=utf8mb4',$username,$password);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            return $conn;
         } else {
            echo "Error PHP Version, ".phpversion()." Please Upgrade PHP Version";
         }
      }

      public function SysHdr($sys,$sysModule) {
         $navHeight  = "60";
         $UserCode   = $GLOBALS['UserCode'];
         $SmallLogo  = $GLOBALS['cid']."/".$GLOBALS["SmallLogo"];
         $user       = $GLOBALS["user"];
         $EmpLogin   = $GLOBALS["EmployeesRefId_Login"];
         $EmployeesFullName = $GLOBALS['EmployeesFullName'];
         $EmployeesPic = $GLOBALS["EmployeesPic"];
         echo
         '<nav class="navbar navbar-fixed-top one-edge-shadow" style="border-bottom:5px solid var(--theme-bg);">
            <div class="container-fluid" style="background:#fdfdfd;">
               <div class="row">
                  <div class="col-sm-2" style="height:'.$navHeight.'px;">
         ';
         if ($GLOBALS['cid'] == "14") {
            echo '<a href="ro.mwss.gov.php"><img src="'.img($SmallLogo).'" style="height:'.$navHeight.'px;"></a>';
         } else {
            echo '<img src="'.img($SmallLogo).'" style="height:'.$navHeight.'px;">';
         }
                     
         echo '
                  </div>
                  <div class="col-sm-6 text-center CompanyName" style="vertical-align:middle;line-height:'.$navHeight.'px;font-size:16pt;">
                     <span style="color:var(--MainColor1);">';
                        $sysTitle = "null";
                        if ($UserCode != "COMPEMP") {
                           switch ($sysModule) {
                              case "spms":
                                 $sysTitle = 'Strategic Performance Management System';
                                 break;
                              case "ams":
                                 $sysTitle = 'Attendance Management System';
                                 break;
                              case "rms":
                                 $sysTitle = 'Recruitment Management System';
                                 break;
                              case "pis":
                                 $sysTitle = 'Personal Information System';
                                 break;
                              case "pms":
                                 $sysTitle = 'Payroll Management System';
                                 break;
                              case "ldms":
                                 $sysTitle = 'Learning Development Management System';
                              break;
                           }
                        } else {
                           if ($GLOBALS['cid'] == "21") {
                              $sysTitle = 'Human Resource And Payroll Management System';   
                           } else {
                              $sysTitle = 'Human Resource And Information System';   
                           }
                           
                        }
                        echo $sysTitle;
                     echo
                     '</span>
                  </div>
                  <div class="col-sm-4" style="height:'.$navHeight.'px;">
                     <div class="row">
                        <div class="col-sm-2 text-right`">';
                           $fileExist = file_exists(img($GLOBALS['cid']."/EmployeesPhoto/".$EmployeesPic));
                           if ($fileExist) {
                              $userPic = img($GLOBALS['cid']."/EmployeesPhoto/".$EmployeesPic);
                           } else {
                              $userPic = img("nopic.png");
                           }
                        echo
                           '<img src="'.$userPic.'" class="img-rounded" style="height:50px;margin-top:5px;">
                        </div>
                        <div class="col-sm-10">
                           <div>&nbsp;</div>
                           <div>'.$user. ' - ' .$EmployeesFullName.'</div>';
                           switch ($sysModule) {
                              case "spms":
                                 echo
                                 '<div class="dropdown">
                                    <a href="#" class="dropdown-toggle link" data-toggle="dropdown">
                                       '.$UserCode.'&nbsp;&nbsp;<span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                       <li><a href="#" id="mLogOut" class="Menu" pre="scrn" route="LogOut">Log Out</a></li>
                                    </ul>
                                 </div>';
                                 break;
                              case "ams":
                                 $sysTitle = 'Attendance Management System';
                                 echo
                                 '<div class="dropdown">
                                    <a href="#" class="dropdown-toggle link" data-toggle="dropdown">
                                       '.$UserCode.'&nbsp;&nbsp;<span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                       <li><a href="#" id="mLogOut" class="Menu" pre="scrn" route="LogOut">Log Out</a></li>
                                    </ul>
                                 </div>';
                                 break;
                              case "rms":
                                 echo
                                 '<div class="dropdown">
                                    <a href="#" class="dropdown-toggle link" data-toggle="dropdown">
                                       '.$UserCode.'&nbsp;&nbsp;<span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                       <li><a href="#" id="mLogOut" class="Menu" pre="scrn" route="LogOut">Log Out</a></li>
                                    </ul>
                                 </div>';
                                 break;
                              case "pis":
                                 echo
                                 '<div class="dropdown">
                                    <div class="col-sm-10">
                                       <div class="dropdown">
                                          <a href="#" class="dropdown-toggle link" data-toggle="dropdown">
                                             '.$UserCode.'&nbsp;&nbsp;<span class="caret"></span>
                                          </a>';
                                          hdrMenu("PIS");
                                       echo
                                       '</div>
                                    </div>
                                 </div>';
                                 break;
                              case "pms":
                                 echo
                                 '<div class="dropdown">
                                    <a href="#" class="dropdown-toggle link" data-toggle="dropdown">
                                       '.$UserCode.'&nbsp;&nbsp;<span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                       <li><a href="#" id="mLogOut" class="Menu" pre="scrn" route="LogOut">Log Out</a></li>
                                    </ul>
                                 </div>';
                                 break;
                              default:
                                 echo
                                 '<div class="dropdown">
                                    <a href="#" class="dropdown-toggle link" data-toggle="dropdown">
                                       '.$UserCode.'&nbsp;&nbsp;<span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                       <li><a href="#" id="mLogOut" class="Menu" pre="scrn" route="LogOut">Log Out</a></li>
                                    </ul>
                                 </div>';
                                 break;
                           }
                        echo
                        '</div>
                     </div>
                  </div>
               </div>
            </div>
         </nav>';
         spacer(intval($navHeight + 5));
         if ($UserCode != "COMPEMP") {
            if ($sysModule == "ams") {
               
               echo '<div class="row" style="background:#1c1d22;color:#fff;margin:0">';
               if (getvalue("hCompanyID") == 28) {
                  echo '
                     <div route="Dashboard" href="javascript:void();" pre="ams" class="col-xs-1 topMenu txt-center" id="amsDash">
                        DASHBOARD
                     </div>
                  ';   
               }
               if ($UserCode == "AA") {
                  $emprefid = getvalue("hEmpRefId");
                  $access = FindFirst("usermanagement","WHERE EmployeesRefId = '".$emprefid."'","SystemAccess");
                  if ($access) {
                     $access_arr = explode("|", $access);
                     foreach ($access_arr as $key => $value) {
                        if ($value != "") {
                           switch ($value) {
                              case '0':
                                 echo '
                                    <div route="Employee" href="javascript:void();" pre="ams" class="col-xs-1 topMenu txt-center" id="amsEmpl">
                                       EMPLOYEE
                                    </div> 
                                 ';
                                 break;
                              case '1':
                                 echo '<div route="Policy" href="javascript:void();" pre="ams" class="col-xs-1 topMenu txt-center" id="amsPolc">POLICY</div>';
                                 break;
                              case '2':
                                 echo '<div route="FileSetup" href="javascript:void();" pre="ams" class="col-xs-1 topMenu txt-center" id="amsFile">FILE SETUP</div>';
                                 break;
                           }
                        }
                     }
                     echo '</div>';
                  } else {
                     echo 
                     '
                        <div route="Employee" href="javascript:void();" pre="ams" class="col-xs-1 topMenu txt-center" id="amsEmpl">
                        EMPLOYEE</div>
                        <div route="Policy" href="javascript:void();" pre="ams" class="col-xs-1 topMenu txt-center" id="amsPolc">POLICY</div>
                        <div route="FileSetup" href="javascript:void();" pre="ams" class="col-xs-1 topMenu txt-center" id="amsFile">FILE SETUP</div>
                     </div>';
                  }
               } else {
                  echo 
                  '
                     <div route="Employee" href="javascript:void();" pre="ams" class="col-xs-1 topMenu txt-center" id="amsEmpl">
                     EMPLOYEE</div>
                     <div route="Policy" href="javascript:void();" pre="ams" class="col-xs-1 topMenu txt-center" id="amsPolc">POLICY</div>
                     <div route="FileSetup" href="javascript:void();" pre="ams" class="col-xs-1 topMenu txt-center" id="amsFile">FILE SETUP</div>
                  </div>';
               }
               
            } else if ($sysModule == "pms") {
               echo
               '<div class="row" style="background:#1c1d22;color:#fff;margin:0">
                  <div route="Employee" href="javascript:void();" pre="pms" class="col-xs-1 topMenu txt-center" id="pmsEmployee">EMPLOYEE</div>
                  <div route="FileManager" href="javascript:void();" pre="pms" class="col-xs-1 topMenu txt-center" id="pmsFileManager">FILE MANAGER</div>
               </div>';
            }
         }
         echo
         '<div class="sideNav" id="div_RSideBar">';
            if ($UserCode != "COMPEMP") {
?>
               <a href="#" onclick="closeNav();" style="vertical-align:middle;line-height:23px;">
                  <i class="fa fa-backward" aria-hidden="true" id="closeNav"></i>
               </a>
            <?php
               if ($GLOBALS['cid'] == "1000" || $GLOBALS['cid'] == "35") {
            ?>
               <a href="#" id="mHome" style="float:right;color:white;">
                  <i class="fa fa-home" aria-hidden="true" title="Home/Exit Module"></i>
               </a>
            <?php
               }
            ?>
               
               <!--<a href="#" onclick="alert('Under Construction')" style="float:right;color:white;">
                  <i class="fa fa-bars" aria-hidden="true" id="openSysMenu"></i>
               </a>-->
<?php
               switch ($sysModule) {
                  case "spms";
                     doSPMS_SideBar();
                     break;
                  case "pis";
                     doRSideBar("");
                     break;
                  case "rms";
                     doRMS_SideBar();
                     break;
                  case "ams";
                     doAMS_SideBar();
                     break;
                  case "pms";
                     doPMS_sideBar();
                     break;
                  case "ldms";
                     doLDMS_SideBar();
                     break;
               }
            } else {
               doUser_SideBar();
            }
         echo
         '</div>';
         return true;
      }
   }
?>