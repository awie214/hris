<!DOCTYPE html>
<html>
<body onload="alert('Done');">

<?php
require "constant.e2e.php";
require "conn.e2e.php";
require_once pathClass."0620functions.e2e.php";

            $conn->query("TRUNCATE TABLE `employees`;");
            $conn->query("TRUNCATE TABLE `employeesfamily`;");
            $conn->query("TRUNCATE TABLE `employeespdsq`;");
            $conn->query("TRUNCATE TABLE `employeesreference`;");
            $conn->query("TRUNCATE TABLE `bloodtype`;");
            $conn->query("TRUNCATE TABLE `occupations`;");

            $t = time();
            $date_today    = date("Y-m-d",$t);
            $curr_time     = date("H:i:s",$t);
            $trackingA_fld = "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
            $trackingA_val = "'$date_today', '$curr_time', 'PHP', 'M'";

            $sql = "SELECT * FROM `employee_personal_information_sheets`";
            $rs = mysqli_query($pids_conn,$sql) or die(mysqli_error($pids_conn));
            echo "Number Records : ".mysqli_num_rows($rs)."<br>";
            if ($rs) {
               if (mysqli_num_rows($rs) > 0) {
                  while ($pids = mysqli_fetch_array($rs)) {
                     $refid = $pids["id"];
                     $empRefId = $refid;
                     $wLastName = $pids["surname"];
                     $wFirstName = $pids["first_name"];
                     $wMiddleName = $pids["middle_name"];
                     $wExtName = $pids["name_extension"];
                     $wBirthDate = $pids["date_of_birth"];
                     $wBirthPlace = $pids["place_of_birth"];

                     if ($pids["sex"] == 1) $wSex = "M";
                     else if ($pids["sex"] == 2) $wSex = "F";

                     $wCitizenshipRefId = 0;
                     $wCountryCitizenRefId = 0;
                     $wBloodType = $pids["blood_type"];
                     $wBloodType = str_replace("Type","",$wBloodType);
                     $wBloodType = str_replace("TYPE","",$wBloodType);
                     $wBloodType = str_replace('"','',$wBloodType);
                     $wBloodType = str_replace("'","",$wBloodType);
                     $wBloodType = trim($wBloodType," ");
                     $wBloodType = strtoupper($wBloodType);
                     $wBloodTypeRefId = fileManager($wBloodType,
                                          "bloodtype",
                                          "`Code`,`Name`,`Remarks`,",
                                          "'','".$wBloodType."','Migrated',");


                     if ($pids["civil_status"] == 1) {
                        $wCivilStatus = "Si";
                     } else if ($pids["civil_status"] == 2) {
                        $wCivilStatus = "Ma";
                     } else if ($pids["civil_status"] == 3) {
                        $wCivilStatus = "An";
                     } else if ($pids["civil_status"] == 4) {
                        $wCivilStatus = "Wi";
                     } else if ($pids["civil_status"] == 5) {
                        $wCivilStatus = "Se";
                     } else if ($pids["civil_status"] == 6) {
                        $wCivilStatus = "Ot";
                     }

                     $wHeight = intval($pids["height"]);
                     $wWeight = intval($pids["weight"]);
                     $wAgencyId = $pids["agency_employee_no"];
                     $wGovtIssuedID = "";
                     $wGovtIssuedIDNo = "";
                     $wGovtIssuedIDPlace = "";
                     $wResiHouseNo = "";
                     $wResiStreet = str_replace("'"," ",$pids["residential_address"]);
                     $wPermanentStreet = str_replace("'"," ",$pids["permanent_address"]);
                     $wResiBrgy = "";
                     $wResiCountryRefId = 0;
                     $wResiAddProvinceRefId = 0;
                     $wResiAddCityRefId = 0;
                     $wPermanentHouseNo = "";
                     $wPermanentBrgy = "";
                     $wPermanentCountryRefId = 0;
                     $wPermanentAddProvinceRefId = 0;
                     $wPermanentAddCityRefId = 0;
                     $wTelNo = "";
                     $wMobileNo = $pids["cellphone_no"];
                     $wEmailAdd = $pids["email_address"];
                     $wGSIS = $pids["gsis_id_no"];
                     $wPAGIBIG = $pids["pag-ibig_id_no"];
                     $wPHIC = $pids["philhealth_no"];
                     $wTIN = $pids["tin"];
                     $wSSS = $pids["sss_no"];
                     $null = "";
                     $wRemarks = $pids["remarks"].";Migrated";
                     $wResidentTelNo = $pids["residential_address_telephone_no"];
                     $wisFilipino = 1;
                     $wisByBirthOrNatural = 0;
                     $Inactive = $pids["inactive"];
                     $pw = $pids['password'];
                     $image = $pids['picture'];


                     $values = "'$refid','1000','1',";
                     $values .= "'$wLastName','$wFirstName','$wMiddleName','$wExtName','$null','$pw','$image',";
                     $values .= "'$null','$wBirthDate','$wBirthPlace','$wEmailAdd','$wSex',";
                     $values .= "'$wCivilStatus','$wCitizenshipRefId','$wisFilipino','$wisByBirthOrNatural','$wCountryCitizenRefId',";
                     $values .= "'$wHeight','$wWeight','$wBloodTypeRefId','$wPAGIBIG','$wGSIS',";
                     $values .= "'$wPHIC','$wTIN','$wSSS','$wAgencyId','$wResiHouseNo',";
                     $values .= "'$wResiStreet','$wResiBrgy','$wResiAddCityRefId','$wResiAddProvinceRefId','$wResiCountryRefId',";
                     $values .= "'$wResidentTelNo','$wPermanentHouseNo','$wPermanentStreet','$wPermanentBrgy','$wPermanentAddCityRefId',";
                     $values .= "'$wPermanentAddProvinceRefId','$wPermanentCountryRefId','$wTelNo','$wMobileNo','$wRemarks',";
                     $values .= "'$wGovtIssuedID', '$wGovtIssuedIDNo', '$wGovtIssuedIDPlace','$Inactive',".$trackingA_val;
                     $flds = "`RefId`,`CompanyRefId`,`BranchRefId`,";
                     $flds .= "`LastName`,`FirstName`,`MiddleName`,`ExtName`,`NickName`,`pw`,`PicFileName`,";
                     $flds .= "`ContactNo`,`BirthDate`,`BirthPlace`,`EmailAdd`,`Sex`,";
                     $flds .= "`CivilStatus`,CitizenshipRefId,isFilipino,isByBirthOrNatural,CountryCitizenRefId,";
                     $flds .= "`Height`,`Weight`,`BloodTypeRefId`,`PAGIBIG`,`GSIS`,";
                     $flds .= "`PHIC`,`TIN`,`SSS`,`AgencyId`,`ResiHouseNo`,";
                     $flds .= "`ResiStreet`,`ResiBrgy`,`ResiAddCityRefId`,`ResiAddProvinceRefId`,`ResiCountryRefId`,";
                     $flds .= "`ResidentTelNo`,`PermanentHouseNo`,`PermanentStreet`,`PermanentBrgy`,`PermanentAddCityRefId`,";
                     $flds .= "`PermanentAddProvinceRefId`,`PermanentCountryRefId`,`TelNo`,`MobileNo`,`Remarks`,";
                     $flds .= "`GovtIssuedID`, `GovtIssuedIDNo`, `GovtIssuedIDPlace`,`Inactive`,".$trackingA_fld;


                     /*$isExist = FindFirst("employees","where LastName = '$wLastName' AND FirstName = '$wFirstName'","*");*/
                     $isExist = false;
                     if (!$isExist) {
                        $sql = "INSERT INTO `employees` ($flds) VALUES ($values)";
                        if ($conn->query($sql) === TRUE) {
                           $wSpouseLastName = "";
                           $wSpouseFirstName = "";
                           $wSpouseMiddleName = "";
                           $wSpouseExtName = "";
                           $wSpouseBirthDate = "";
                           $wOccupationRefId = 0;
                           $wEmployersName = "";
                           $wBusinessAddress = "";
                           $wSpouseMobileNo = "";
                           $sql = "SELECT * FROM `employee_spouses` where employee_personal_information_sheet_id = $empRefId limit 1";
                           $result = mysqli_query($pids_conn,$sql) or die(mysqli_error($pids_conn));
                           if ($result) {
                              $spouses = mysqli_fetch_assoc($result);
                              $wSpouseLastName = remquote($spouses["surname"]);
                              $wSpouseFirstName = remquote($spouses["first_name"]);
                              $wSpouseMiddleName = remquote($spouses["middle_name"]);
                              $wEmployersName = remquote($spouses["employee_business_name"]);
                              $wBusinessAddress = remquote($spouses["business_address"]);
                              $wSpouseMobileNo = $spouses["telephone_no"];
                              $wOccupation = remquote($spouses["occupation"]);
                              $wOccupation = strtoupper($wOccupation);
                              $wOccupationRefId = fileManager($wOccupation,
                                          "occupations",
                                          "`Code`,`Name`,`Remarks`,",
                                          "'','".$wOccupation."','Migrated',");
                           }

                           $flds_family = "";
                           $values_family = "";
                           $wFatherLastName = $pids["father_surname"];
                           $wFatherFirstName = $pids["father_first_name"];
                           $wFatherMiddleName = $pids["father_middle_name"];
                           $wFatherExtName = "";
                           // MISSING Mothers Maiden Name Field??
                           $wMotherLastName = $pids["mother_surname"];
                           $wMotherFirstName = $pids["mother_first_name"];
                           $wMotherMiddleName = $pids["mother_middle_name"];
                           $wMotherExtName = "";
                           $wRemarks = "Migrated;";
                           $flds_family = "";
                           $flds_family .= "`CompanyRefId`,`BranchRefId`,";
                           $flds_family .= "`EmployeesRefId`, `SpouseLastName`, `SpouseFirstName`, `SpouseMiddleName`, `SpouseExtName`,";
                           $flds_family .= "`SpouseBirthDate`, `OccupationsRefId`, `EmployersName`, `BusinessAddress`, `SpouseMobileNo`,";
                           $flds_family .= "`FatherLastName`, `FatherFirstName`, `FatherMiddleName`, `FatherExtName`, `MotherLastName`,";
                           $flds_family .= "`MotherFirstName`, `MotherMiddleName`, `MotherExtName`, `Remarks`,".$trackingA_fld;
                           $values_family = "";
                           $values_family .= "'1000','1',";
                           $values_family .= "'$empRefId', '$wSpouseLastName', '$wSpouseFirstName', '$wSpouseMiddleName', '$wSpouseExtName',";
                           $values_family .= "'$wSpouseBirthDate', '$wOccupationRefId', '$wEmployersName', '$wBusinessAddress', '$wSpouseMobileNo',";
                           $values_family .= "'$wFatherLastName', '$wFatherFirstName', '$wFatherMiddleName', '$wFatherExtName', '$wMotherLastName',";
                           $values_family .= "'$wMotherFirstName', '$wMotherMiddleName', '$wMotherExtName', '$wRemarks',".$trackingA_val;

                           $sql_family = "INSERT INTO `employeesfamily` ($flds_family) VALUES ($values_family)";
                           $conn->query($sql_family);
// ===========================================================================================================================================
                           // PDSQ
                           $wQ1a    = remquote($pids["36A"]); // 34 old 36
                           $wQ1aexp = remquote($pids["36A"]);
                           $wQ1b    = remquote($pids["36B"]); // 34
                           $wQ1bexp = remquote($pids["36B"]);
                           $wQ2a    = remquote($pids["37A"]); // 35
                           $wQ2aexp = remquote($pids["37A"]);
                           $wQ2b    = remquote($pids["37B"]); // 35
                           $wQ2bexp = remquote($pids["37B"]);
                           $wQ3a    = remquote($pids["38"]); // 36
                           $wQ3aexp = remquote($pids["38"]);
                           $wQ4a    = remquote($pids["39"]); // 37
                           $wQ4aexp = str_replace("'"," ",$pids["37B"]);
                           $wQ5a    = remquote($pids["40"]); // 38
                           $wQ5aexp = str_replace("'"," ",$pids["40"]);
                           $wQ5b    = 0; // 38
                           $wQ5bexp = ""; //$pids["38"];
                           $wQ6a    = 0; //$pids[""]; // 39
                           $wQ6aexp = "";
                           $wQ7a    = remquote($pids["41A"]); // 40
                           $wQ7aexp = str_replace("'"," ",$pids["41A"]);
                           $wQ7b    = remquote($pids["41B"]); // 40
                           $wQ7bexp = str_replace("'"," ",$pids["41B"]);
                           $wQ7c    = remquote($pids["41C"]); // 40
                           $wQ7cexp = remquote($pids["41C"]);
                           $wQ2DateFiled = $pids["date_filed"];
                           $wQ2CaseStatus = str_replace("'"," ",$pids["status"]);

                           $flds_pdsq    = "";
                           $values_pdsq  = "";
                           $flds_pdsq .= "`CompanyRefId`,`BranchRefId`,";
                           $flds_pdsq .= "`EmployeesRefId`, `Q1a`, `Q1aexp`, `Q1b`, `Q1bexp`,";
                           $flds_pdsq .= "`Q2a`, `Q2aexp`, `Q2b`, `Q2bexp`, `Q3a`,";
                           $flds_pdsq .= "`Q3aexp`, `Q4a`, `Q4aexp`, `Q5a`, `Q5aexp`,";
                           $flds_pdsq .= "`Q5b`, `Q5bexp`, `Q6a`, `Q6aexp`, `Q7a`,";
                           $flds_pdsq .= "`Q7aexp`, `Q7b`, `Q7bexp`, `Q7c`, `Q7cexp`,";
                           $flds_pdsq .= "`Q2DateFiled`, `Q2CaseStatus`,".$trackingA_fld;


                           $values_pdsq .= "1000,1,'$empRefId', '$wQ1a', '$wQ1aexp', '$wQ1b', '$wQ1bexp',";
                           $values_pdsq .= "'$wQ2a', '$wQ2aexp', '$wQ2b', '$wQ2bexp', '$wQ3a',";
                           $values_pdsq .= "'$wQ3aexp', '$wQ4a', '$wQ4aexp', '$wQ5a', '$wQ5aexp',";
                           $values_pdsq .= "'$wQ5b', '$wQ5bexp', '$wQ6a', '$wQ6aexp', '$wQ7a',";
                           $values_pdsq .= "'$wQ7aexp', '$wQ7b', '$wQ7bexp', '$wQ7c', '$wQ7cexp',";
                           $values_pdsq .= "'$wQ2DateFiled', '$wQ2CaseStatus',".$trackingA_val;
                           $sql_pdsq = "INSERT INTO `employeespdsq` ($flds_pdsq) VALUES ($values_pdsq)";
                           $conn->query($sql_pdsq);

// ===========================================================================================================================================
                           // REFERENCES
                           $wName = $pids["reference_person_1_name"];
                           //$wAddress = $pids["reference_person_1_address"];
                           $wContactNo = $pids["reference_person_1_telephone_no"];

                           $wAddress = str_replace("'"," ",$pids["reference_person_1_address"]);

                           $flds_ref = "`CompanyRefId`,`BranchRefId`,`EmployeesRefId`, `Name`, `Address`, `ContactNo`, `Remarks`,".$trackingA_fld;
                           $values_ref = "1000,1,'$empRefId', '$wName', '$wAddress', '$wContactNo', '$wRemarks',".$trackingA_val;
                           $sql = "INSERT INTO `employeesreference` ($flds_ref) VALUES ($values_ref)";
                           $conn->query($sql);

                           $wName = $pids["reference_person_2_name"];
                           $wAddress = $pids["reference_person_2_address"];
                           $wContactNo = $pids["reference_person_2_telephone_no"];

                           $flds_ref = "`CompanyRefId`,`BranchRefId`,`EmployeesRefId`, `Name`, `Address`, `ContactNo`, `Remarks`,".$trackingA_fld;
                           $values_ref = "1000,1,'$empRefId', '$wName', '$wAddress', '$wContactNo', '$wRemarks',".$trackingA_val;
                           $sql = "INSERT INTO `employeesreference` ($flds_ref) VALUES ($values_ref)";
                           $conn->query($sql);

                           $wName = $pids["reference_person_3_name"];
                           $wAddress = $pids["reference_person_3_address"];
                           $wContactNo = $pids["reference_person_3_telephone_no"];

                           $flds_ref = "`CompanyRefId`,`BranchRefId`,`EmployeesRefId`, `Name`, `Address`, `ContactNo`, `Remarks`,".$trackingA_fld;
                           $values_ref = "1000,1,'$empRefId', '$wName', '$wAddress', '$wContactNo', '$wRemarks',".$trackingA_val;
                           $sql = "INSERT INTO `employeesreference` ($flds_ref) VALUES ($values_ref)";
                           $conn->query($sql);
// ===========================================================================================================================================


                        } else {
                           echo "Err in Employees:-->".$str."<br>";
                        }
                     } else {
                        echo "This Name is Already Exist ---->> ".$wLastName.", ".$wFirstName."<br>";
                     }
                  }

               }
            }
            echo "DONE";
            mysqli_close($conn);


?>

</body>
</html>