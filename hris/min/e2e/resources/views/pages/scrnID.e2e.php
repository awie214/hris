<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script type="text/javascript" src="<?php echo jsCtrl("ctrl_ID"); ?>"></script>
      <style type="text/css">
      </style>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar($modTitle); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div class="row" id="divList">
                        <div class="col-xs-3">
                           <?php employeeSelector(); ?>
                        </div>
                        <div class="col-xs-9">
                           
                        </div>
                     </div>
                     <div class="row" id="divView">
                        <div class="col-xs-12">
                           
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               $table = "";
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>



