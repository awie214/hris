<?php
	$corrected_by_refid  = getvalue("corrected_by_refid");
   $prepared_by_refid   = getvalue("prepared_by_refid");
   if (intval($corrected_by_refid) > 0) {
      $corrected_by_row = FindFirst("employees","WHERE RefId = '$corrected_by_refid'","`FirstName`,`LastName`,`MiddleName`");
      $FirstName        = trim($corrected_by_row["FirstName"]);
      $LastName         = trim($corrected_by_row["LastName"]);
      $MiddleName       = trim($corrected_by_row["MiddleName"]);
      $MiddleName       = substr($MiddleName, 0,1);
      $FullName         = $FirstName." ".$MiddleName." ".$LastName;
      $corrected_by     = $FullName;
      $corrected_by_empinfo_position   = FindFirst("empinformation","WHERE EmployeesRefId = '$corrected_by_refid'","PositionRefId");
      $corrected_by_position           = getRecord("position",$corrected_by_empinfo_position,"Name");
   } else {
      $corrected_by           = "No Set Person";
      $corrected_by_position  = "&nbsp;";
   }
   if (intval($prepared_by_refid) > 0) {
      $prepared_by_row  = FindFirst("employees","WHERE RefId = '$prepared_by_refid'","`FirstName`,`LastName`,`MiddleName`");
      $FirstName        = trim($prepared_by_row["FirstName"]);
      $LastName         = trim($prepared_by_row["LastName"]);
      $MiddleName       = trim($prepared_by_row["MiddleName"]);
      $MiddleName       = substr($MiddleName, 0,1);
      $FullName         = $FirstName." ".$MiddleName." ".$LastName;
      $prepared_by      = $FullName;
      $prepared_by_empinfo_position   = FindFirst("empinformation","WHERE EmployeesRefId = '$prepared_by_refid'","PositionRefId");
      $prepared_by_position           = getRecord("position",$prepared_by_empinfo_position,"Name");
   } else {
      $prepared_by            = "No Set Person";
      $prepared_by_position   = "&nbsp;";
   }
?>