<?php
   require_once "constant.e2e.php";
   $ScrnMode = $code = $name = $remarks = $disabled = $msg = $posrefid = $salarygrade = $poslevel = $salarystep =
   $posclass = $salaryamount = $jobgrade = "";
   $mainTable = $recordSet = "";
   if (isset($_GET["hScrnMode"]))
      $ScrnMode = $_GET["hScrnMode"];
   else if (isset($_POST["hScrnMode"]))
      $ScrnMode = $_POST["hScrnMode"];

   if (!function_exists('getvalue')) {
      function getvalue($idx) {
         $retval = "";
         if (isset($_GET[$idx]))
            $retval = $_GET[$idx];
         else if (isset($_POST[$idx]))
            $retval = $_POST[$idx];
         return $retval;
      }
   }

   if ($ScrnMode == 1) {
      $refid = "";
   } else if ($ScrnMode == 3 || $ScrnMode == 2) {
      session_start();
      require_once $_SESSION['Classes'].'0620functions.e2e.php';
      require_once "conn.e2e.php";
      $msg = getvalue("msg");
      $mainTable = getvalue("hTable");
      if ($ScrnMode == 3) $disabled = "disabled";
      $refid = getvalue("hRefId");
      if ($refid) {
         $criteria  = " WHERE RefId = $refid";
         $criteria .= " LIMIT 1";
         $recordSet = f_Find($mainTable,$criteria);
         $rowcount = mysqli_num_rows($recordSet);
         $row = array();
         $row = mysqli_fetch_assoc($recordSet);
         if ($rowcount) {
            $code = $row["Code"];
            $name = $row["Name"];
            $remarks = $row["Remarks"];
            $posrefid = $row["PositionRefId"];
            $salarygrade = $row["SalaryGradeRefId"];
            $poslevel = $row["PositionLevelRefId"];
            $salarystep = $row["StepIncrementRefId"];
            $posclass = $row["PositionClassificationRefId"];
            $salaryamount = $row["SalaryAmount"];
         }
      }
   }
   require_once "incUtilitiesJS.e2e.php";
?>

   <script src="<?php echo jsCtrl("ctrl_DataManager"); ?>"></script>
   <div class="container" id="EntryScrn">
      <div class="row">
         <div class="col-xs-4">
            <?php if ($ScrnMode != 1) { ?>
               <div class="row">
                  <ul class="nav nav-pills">
                     <li class="active" style="font-size:12pt;font-weight:600;">
                        <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                        <?php echo $refid; ?>
                        </span></a>
                     </li>
                  </ul>
               </div>
            <?php } ?>
            <!-- <div class="row margin-top10">
               <div class="form-group">
                  <label class="control-label" for="inputs">CODE:</label><br>
                  <input class="form-input uCase-- saveFields-- mandatory" type="text" placeholder="Code" <?php if ($ScrnMode == 2) { echo "disabled"; } else { echo $disabled; }  ?>
                     id="inputs" name="char_Code" style='width:75%' value="<?php echo $code; ?>" autofocus>
               </div>
            </div> -->
            <div class="row">
               <div class="col-xs-12">
                  <div class="form-group">
                     <label class="control-label" for="inputs">NAME:</label><br>
                     <input class="form-input uCase-- saveFields-- mandatory" type="text" placeholder="name" <?php echo $disabled; ?>
                        id="inputs" name="char_Name" value="<?php echo $name; ?>">
                  </div>
               </div>
               
            </div>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <div class="form-group">
                     <label class="control-label" for="inputs">POSITION:</label><br>
                     <?php
                        createSelect("Position",
                                     "sint_PositionRefId",
                                     $posrefid,90,"Name","Select Position",$disabled);
                     ?>
                  </div>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <div class="form-group">
                     <label class="control-label" for="inputs">POSITION LEVEL:</label><br>
                     <?php
                        createSelect("PositionLevel",
                                     "sint_PositionLevelRefId",
                                     $poslevel,90,"Name","Select Position Level",$disabled);
                     ?>
                  </div>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <div class="form-group">
                     <?php
                        if (getvalue("hCompanyID") != "2") {
                     ?>
                     <label class="control-label" for="inputs">SALARY GRADE:</label><br>
                     <?php
                        createSelect("SalaryGrade",
                                     "sint_SalaryGradeRefId",
                                     $salarygrade,90,"Name","Select Salary Grade",$disabled);
                     ?>
                     <?php
                        } else {
                     ?>
                     <label class="control-label" for="inputs">JOB GRADE:</label><br>
                     <?php
                        createSelect("JobGrade",
                                     "sint_JobGradeRefId",
                                     $jobgrade,90,"Name","Select Job Grade",$disabled);
                     ?>
                     <?php
                        }
                     ?>
                  </div>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <div class="form-group">
                     <label class="control-label" for="inputs">SALARY STEP:</label><br>
                     <?php
                        createSelect("StepIncrement",
                                     "sint_StepIncrementRefId",
                                     $salarystep,90,"Name","Select SALARY STEP",$disabled);
                     ?>
                  </div>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <div class="form-group">
                     <label class="control-label" for="inputs">POSITION CLASSIFICATION:</label><br>
                     <?php
                        createSelect("PositionClassification",
                                     "sint_PositionClassificationRefId",
                                     $posclass,90,"Name","Select Position Classification",$disabled);
                     ?>



                  </div>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <div class="form-group">
                     <label class="control-label" for="inputs">OFFICE:</label><br>
                     <?php
                        createSelect("office",
                                     "sint_OfficeRefId",
                                     "",90,"Name","Select Office",$disabled);
                     ?>



                  </div>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <div class="form-group">
                     <label class="control-label" for="inputs">SALARY AMOUNT:</label><br>
                     <input class="form-input saveFields-- number--" type="text" placeholder="Salary Amount" <?php echo $disabled; ?>
                        name="deci_SalaryAmount" value="<?php echo $salaryamount; ?>">
                  </div>
               </div>
            </div>
            <!-- <div class="row">
               <div class="form-group">
                  <label class="control-label" for="inputs">REMARKS:</label>
                  <textarea class="form-input" rows="5" name="char_Remarks" <?php echo $disabled; ?>
                  placeholder="remarks"><?php echo $remarks; ?></textarea>
               </div>
            </div> -->
         </div>
         <div class="col-xs-5">
            <div class="row">
               <div class="col-xs-12">
                  <label>
                     EDUCATIONAL QUALIFICATION:
                  </label>
                  <textarea class="form-input saveFields--" rows="5" name="char_EducRequirements" <?php echo $disabled; ?> placeholder="Educational Qualification"></textarea>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <label>
                     WORK EXPERIENCE QUALIFICATION:
                  </label>
                  <textarea class="form-input saveFields--" rows="5" name="char_WorkExpRequirements" <?php echo $disabled; ?> placeholder="Work Experience Qualification"></textarea>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <label>
                     TRAINING QUALIFICATION:
                  </label>
                  <textarea class="form-input saveFields--" rows="5" name="char_TrainingRequirements" <?php echo $disabled; ?> placeholder="Training Qualification"></textarea>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <label>
                     ELIGIBILITY QUALIFICATION:
                  </label>
                  <textarea class="form-input saveFields--" rows="5" name="char_EligibilityRequirements" <?php echo $disabled; ?> placeholder="Eligibility Qualification"></textarea>
               </div>
            </div>
         </div>
      </div>
   </div>
   <script type="text/javascript">
      $(document).ready(function () {
         $("[name='sint_SalaryGradeRefId']").change(function () {
            var sg = $(this).val();
            var jg = $("[name='sint_JobGradeRefId']").val(); 
            var si = $("[name='sint_StepIncrementRefId']").val(); 
            getSalary(sg,jg,si);
         });
         $("[name='sint_StepIncrementRefId']").change(function () {
            var si = $(this).val();
            var jg = $("[name='sint_JobGradeRefId']").val(); 
            var sg = $("[name='sint_SalaryGradeRefId']").val(); 
            getSalary(sg,jg,si);
         });
         $("[name='sint_JobGradeRefId']").change(function () {
            var jg = $(this).val();
            var sg = $("[name='sint_SalaryGradeRefId']").val(); 
            var si = $("[name='sint_StepIncrementRefId']").val(); 
            getSalary(sg,jg,si);
         });
      });
   </script>