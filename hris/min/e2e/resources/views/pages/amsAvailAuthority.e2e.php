<?php 
   $module = module("AvailmentAuthority"); 
   $m = date("m",time());
   $y = date("Y",time());
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript">
         $(document).ready(function () {
            $("[name='sint_AbsencesRefId']").addClass("mandatory");
            $("#btnPRINT").click(function () {
               popPDS("OB_2","");
            });
            $("#LocSAVE").click(function () {
               if (saveProceed() == 0) {
                  $(this).attr("type","submit"); 
               }
            });
            remIconDL();
            //#destination
            $("#destination").show();
            $("#time_canvas").hide();
            /*$("[name='sint_AbsencesRefId']").change(function () {
               var value = $("[name='sint_AbsencesRefId'] option:selected").text().toLowerCase();
               if (
                     value == "official business" ||
                     value == "travel order" ||
                     value == "office order" ||
                     value == "foreign travel order"
                  ) {
                  $("#destination").show();
               } else {
                  $("#destination").hide();
               }
            }); */
            $("[name='sint_AbsencesRefId']").change(function () {
               var value = $(this).val();
               if (value > 0) {
                  getCtrlNo(value);   
               }
               
            });
            $("[name='sint_Whole']").change(function () {
               var value = $(this).val();
               if (value > 0) {
                  $("#time_canvas").hide();
               } else {
                  $("#time_canvas").show();
               }
            });
            <?php
               if (isset($_GET["errmsg"])) {
                  echo '$.notify("'.$_GET["errmsg"].'");';
               }
            ?>
            <?php
               echo '$("#month_").val("'.$m.'");';        
               echo '$("#year_").val("'.$y.'");';     
            ?>
            $("#btnPrintMultiple").click(function () {
               var control_number       = $("#control_number").val();
               var type                 = control_number.split("-")[0];
               if (control_number != "") {
                  $("#rptContent").attr("src","blank.htm");
                  var rptFile = "rpt_Multiple_" + type + "_35";
                  var url = "ReportCaller.e2e.php?file=" + rptFile;
                  url += "&control_number=" + control_number;
                  url += "&" + $("[name='hgParam']").val();
                  $("#prnModal").modal();
                  $("#rptContent").attr("src",url);         
               } else {
                  $.notify("Control Number is Required");
               }
               
            });
         });
         function getCtrlNo(value) {
            $.get("trn.e2e.php",
            {
               fn:"getCtrlNo",
               refid:value
            },
            function(data,status){
               if (status == 'success') {
                  eval(data);
               }
            });
         }
         function afterDelete() {
            alert("Successfully Deleted");
            gotoscrn($("#hProg").val(),"");
         }
         function selectMe(refid) {
            $("#rptContent").attr("src","blank.htm");
            var rptFile = "rpt_OB_2";
            var url = "ReportCaller.e2e.php?file=" + rptFile;
            url += "&refid=" + refid;
            url += "&diff_report=1";
            url += "&" + $("[name='hgParam']").val();
            $("#prnModal").modal();
            $("#rptContent").attr("src",url);
         }
      </script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="postMultiple.e2e.php">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar($paramTitle); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                        <div id="divList">
                           <div class="mypanel">
                              <div class="panel-top">List of Office Authority Application</div>
                              <div class="panel-mid">
                                 <?php
                                    application_filter();
                                 ?>
                                 <span id="spGridTable">
                                    <?php
                                          $sizeCol = "col-xs-6";
                                          if ($UserCode == "COMPEMP") {
                                             $sizeCol = "col-xs-12";
                                             $gridTableHdr_arr = ["Office Authority","File Date", "Application Date From", "Application Date To", "Status"];
                                             $gridTableFld_arr = ["AbsencesRefId","FiledDate", "ApplicationDateFrom", "ApplicationDateTo", "Status"];
                                             $sql = "SELECT * FROM $table WHERE CompanyRefId = $CompanyId AND BranchRefId = $BranchId AND EmployeesRefId = $EmployeesRefId ORDER BY FiledDate DESC";
                                             $Action = [false,true,false,true];
                                          } else {
                                             $sql = "SELECT * FROM $table WHERE Month(FiledDate) = '$m' AND Year(FiledDate) = '$y' ORDER BY FiledDate";   
                                          }
                                          
                                          doGridTable($table,
                                                      $gridTableHdr_arr,
                                                      $gridTableFld_arr,
                                                      $sql,
                                                      $Action,
                                                      "gridTable");
                                    ?>
                                 </span>
                                 <?php
                                       btnINRECLO([true,true,false]);
                                       /*echo '
                                          <button type="button"
                                               class="btn-cls4-lemon trnbtn"
                                               id="btnPRINT" name="btnPRINT">
                                             <i class="fa fa-print" aria-hidden="true"></i>&nbsp;
                                             PRINT
                                          </button>
                                       ';*/
                                 ?>
                                 <?php
                                    if ($UserCode != "COMPEMP") {
                                       if ($CompanyId == 35) {
                                 ?>
                                 <button type="button" class="btn-cls4-tree" id="btnPrintMultiple">
                                    <i class="fa fa-print"></i>&nbsp;&nbsp;Print Multiple Office Order
                                 </button>
                                 <label>Control Number:&nbsp;&nbsp;</label>
                                 <select type="text" name="control_number" id="control_number" class="form-input" style="width: 20%;">
                                    <option value="">Select Control Number</option>
                                    <?php
                                       $rs = SelectEach("employeesauthority"," GROUP BY ControlNumber DESC");
                                       if ($rs) {
                                          while ($ctrl_row = mysqli_fetch_assoc($rs)) {
                                             if ($ctrl_row["ControlNumber"] != "") {
                                                echo '<option value="'.$ctrl_row["ControlNumber"].'">'.$ctrl_row["ControlNumber"].'</option>';   
                                             }
                                          }
                                       }
                                    ?>
                                 </select>
                                 <?php
                                       }
                                    }
                                 ?>
                              </div>
                              <div class="panel-bottom"></div>
                           </div>
                        </div>
                        <div id="divView">
                           <div class="row">
                              <div class="col-xs-6">
                                 <div class="mypanel">
                                    <div class="panel-top">
                                       <span id="ScreenMode">AVAILING</span> <?php echo strtoupper($ScreenName); ?>
                                    </div>
                                    <div class="panel-mid">
                                       <div id="EntryScrn">
                                          <div class="row margin-top">
                                             <div class="<?php echo $sizeCol; ?>">
                                                   <div class="row" id="badgeRefId">
                                                      <div class="col-xs-6">
                                                         <ul class="nav nav-pills">
                                                            <li class="active" style="font-size:12pt;font-weight:600;">
                                                               <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                                                               </span></a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                   <?php
                                                      $attr = ["br"=>true,
                                                               "type"=>"text",
                                                               "row"=>true,
                                                               "name"=>"date_FiledDate",
                                                               "col"=>"6",
                                                               "id"=>"FiledDate",
                                                               "label"=>"Date of Filing",
                                                               "class"=>"saveFields-- mandatory date--",
                                                               "style"=>"",
                                                               "other"=>""];
                                                      $form->eform($attr);
                                                      spacer(10);
                                                      /*$attr = array(
                                                         "row"=>true,
                                                         "col"=>"12",
                                                         "label"=>"Office Authority",
                                                         "table"=>"Absences",
                                                      );
                                                      createFormFK($attr);*/  
                                                      echo '
                                                         <div class="row margin-top">
                                                            <div class="col-xs-12">
                                                            <label>Office Authority</label>
                                                      ';
                                                      createSelect("Absences",
                                                                   "sint_AbsencesRefId",
                                                                   "",100,"Name","Select Office Authority","required",false);
                                                      echo '
                                                            </div>
                                                         </div>
                                                      ';
                                                      if ($CompanyId == 35) {
                                                         echo '
                                                            <div class="row margin-top">
                                                               <div class="col-xs-6">
                                                                  <div class="row">
                                                                     <div class="col-xs-12">
                                                                        <label>Control Number:</label>
                                                                        <input type="text" class="form-input saveFields--" name="char_ControlNumber" id="char_ControlNumber">
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         ';
                                                      }
                                                      ?>
                                                      <div class="row margin-top">
                                                         <div class="col-xs-12">
                                                            <div class="row">
                                                               <div class="col-xs-12">
                                                                  <label>Inclusive Date</label>
                                                               </div>
                                                            </div>
                                                            <div class="row">
                                                               <div class="col-xs-6">
                                                                  <label>From</label>
                                                                  <br>
                                                                  <input type="text" class="form-input mandatory saveFields-- date--" name="date_ApplicationDateFrom" id="date_ApplicationDateFrom" required="true">
                                                               </div>
                                                               <div class="col-xs-6">
                                                                  <label>To</label>
                                                                  <br>
                                                                  <input type="text" class="form-input mandatory saveFields-- date--" name="date_ApplicationDateTo" id="date_ApplicationDateTo" required="true">
                                                               </div>
                                                            </div>

                                                         </div>
                                                      </div>
                                                      <div class="row margin-top">
                                                         <div class="col-xs-6">
                                                            <label>Whole Day?</label>
                                                            <select class="form-input saveFields--" name="sint_Whole" id="sint_Whole">
                                                               <option value="">Select --</option>
                                                               <option value="0">NO</option>
                                                               <option value="1">YES</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                      <?php
                                                      echo
                                                      '
                                                      <div id="time_canvas">
                                                         <label>Time</label><br>
                                                         <div class="row margin-top">
                                                            <div class="col-xs-12">
                                                               <div class="col-xs-6">
                                                               <label>From Time</label>
                                                               ';
                                                                  //timePicker("FromTime","From Time");
                                                               drpTime("FromTime",8,30,0);
                                                               echo
                                                               '</div>
                                                               <div class="col-xs-6">
                                                               <label>To Time</label>
                                                               ';
                                                                  //timePicker("ToTime","To Time");
                                                               drpTime("ToTime",8,30,0);
                                                               echo
                                                               '</div>';

                                                         echo
                                                         '
                                                            </div>
                                                         </div>
                                                      </div>
                                                      ';

                                                   ?>
                                                   
                                                   <div class="row margin-top" id="destination">
                                                      <div class="col-xs-12">
                                                         <div class="form-group">
                                                            <label class="control-label" for="inputs">Destination:</label>
                                                            <textarea class="form-input saveFields--" rows="2" name="char_Destination" placeholder="Destination"></textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="row margin-top">
                                                      <div class="col-xs-12">
                                                         <div class="form-group">
                                                            <label class="control-label" for="inputs">Purpose:</label>
                                                            <textarea class="form-input saveFields--" rows="5" name="char_Remarks" placeholder="Purpose"></textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <?php
                                                      if ($CompanyId == 21) {
                                                   ?>
                                                   <div class="row">
                                                      <div class="col-xs-6">
                                                         <div class="form-group">
                                                            <label class="control-label" for="inputs">Fund Type:</label>
                                                            <select class="form-input saveFields--" name="sint_FundType" id="sint_FundType">
                                                               <option value="0">Select Fund Type</option>
                                                               <option value="1">General Fund</option>
                                                               <option value="2">Project Funds</option>
                                                               <option value="3">Others</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="row">
                                                      <div class="col-xs-12">
                                                         <div class="form-group">
                                                            <label class="control-label" for="inputs">Fund Description:</label>
                                                            <textarea class="form-input saveFields--" rows="5" name="char_FundDesc" placeholder="Fund Description"></textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <?php
                                                      }
                                                      if ($CompanyId == 35) {
                                                   ?>
                                                   <div class="row margin-top">
                                                      <div class="col-xs-12">
                                                         <div class="form-group">
                                                            <label class="control-label" for="inputs">Title of Activity:</label>
                                                            <textarea class="form-input saveFields--" rows="3" name="char_EventName" placeholder="Title of Activity"></textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="row margin-top">
                                                      <div class="col-xs-12">
                                                         <div class="form-group">
                                                            <label class="control-label" for="inputs">Nature of Activity:</label>
                                                            <textarea class="form-input saveFields--" rows="5" name="char_ActivityNature" placeholder="Nature of Activity"></textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="row margin-top">
                                                      <div class="col-xs-12">
                                                         <div class="form-group">
                                                            <label class="control-label" for="inputs">Sponsored/Conducted/Requested by:</label>
                                                            <textarea class="form-input saveFields--" rows="5" name="char_Sponsor" placeholder="Sponsored/Conducted/Requested by:"></textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <?php
                                                      }
                                                   ?>
                                                   <div class="row">
                                                      <div class="col-xs-12">
                                                         <label class="control-label" for="inputs">Recommended By:</label>
                                                         <?php
                                                            createSelect("signatories",
                                                                         "sint_Signatory1",
                                                                         "",100,"Name","Select Signatory","",false);
                                                         ?>
                                                      </div>
                                                   </div>
                                                   <div class="row">
                                                      <div class="col-xs-12">
                                                         <label class="control-label" for="inputs">Approved By:</label>
                                                         <?php
                                                            createSelect("signatories",
                                                                         "sint_Signatory2",
                                                                         "",100,"Name","Select Signatory","",false);
                                                         ?>
                                                      </div>
                                                   </div>
                                             </div>
                                             <div class="col-xs-6 padd5 adminUse--">
                                                <label>Employees Selected</label>
                                                <select multiple id="EmpSelected" name="EmpSelected" class="form-input" style="height:250px;">
                                                </select>
                                                <p>Double Click the items to remove in the List</p>
                                                <input type="hidden" class="saveFields--" name="hEmpSelected">
                                             </div>
                                          </div>
                                       </div>
                                       <?php
                                          spacer(10);
                                          echo
                                          '<button type="button" class="btn-cls4-sea"
                                                   name="btnLocSAVE" id="LocSAVE">
                                             <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                             &nbsp;Save
                                          </button>';
                                          btnSACABA([false,true,true]);
                                       ?>
                                    </div>
                                    <div class="panel-bottom"></div>
                                 </div>
                              </div>

                              <div class="col-xs-6 adminUse--">
                                 <div class="mypanel">
                                    <div class="panel-top">Employees List</div>
                                    <div class="panel-mid">
                                       <?php include_once "incEmpListSearchCriteria.e2e.php" ?>
                                       <!-- <a href="javascript:void(0);" title="Search Employees">
                                          <i class="fa fa-search" aria-hidden="true"></i>
                                       </a>-->
                                       <div id="empList" style="max-height:300px;overflow:auto;">
                                          <h4>No Employees Selected</h4>
                                       </div>
                                    </div>
                                    <div class="panel-bottom">
                                       <a href="javascript:void(0);" id="clearEmpSelected">Clear Employees Selected</a>&nbsp;
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varJSON.e2e.php";
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>

   </body>
</html>