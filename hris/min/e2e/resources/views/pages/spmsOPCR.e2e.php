

<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_AfterTrn"); ?>"></script>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_OPCR"); ?>"></script>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"spms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("OFFICE PERFORMANCE COMMITMENT AND REVIEW [OPCR]"); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="divList">
                     <div class="mypanel">
                        <div class="row panel-top"></div>
                        <div class="row panel-mid">
                           <div class="col-xs-12">
                              <div class="row">
                                 <div class="col-xs-2">
                                    <span class="label">Employee Name:</span>
                                 </div>
                                 <div class="col-xs-3">
                                    <input class="form-input" type="text" readonly>
                                 </div>
                                 <div class="col-xs-2">
                                    <span class="label">Department Division\Office</span>
                                 </div>
                                 <div class="col-xs-2">
                                    <input class="form-input" type="text" readonly>
                                 </div>
                                 <div class="col-xs-1">
                                    <span class="label">Date:</span>
                                 </div>
                                 <div class="col-xs-1">
                                    <input class="form-input date--" type="text" readonly>
                                 </div>
                              </div>
                              <?php spacer(30);?>
                              <div class="row">
                                 <div id="spGridTable">
                                    <?php
                                       $table = "employeesopcr";
                                       $gridTableHdr = "STRATEGIC OBJECTIVES/FUNCTIONS|SUCCESS INDICATOR|ACTUAL ACCOMPLISHMENTS|Q1|E2|T3|A4|REMARKS";
                                       $gridTableFld = "StrategicObjective|SuccessIndicator|ActualAccomplishment|QualityRate|EfficiencyRate|TimelinessRate|AverageRate|Remarks";
                                       $sql = "SELECT * FROM `$table` WHERE EmployeesRefId = ".getvalue("hEmpRefId")." ORDER BY RefId";
                                       $gridTableHdr_arr = explode("|",$gridTableHdr);
                                       $gridTableFld_arr = explode("|",$gridTableFld);
                                       $_SESSION["module_table"] = $table;
                                       $_SESSION["module_gridTableHdr_arr"] = $gridTableHdr_arr;
                                       $_SESSION["module_gridTableFld_arr"] = $gridTableFld_arr;
                                       $_SESSION["module_sql"] = $sql;
                                       $_SESSION["module_gridTable_ID"] = "gridTable";

                                       doGridTable($table,
                                                   $gridTableHdr_arr,
                                                   $gridTableFld_arr,
                                                   $sql,
                                                   [true,true,true,false],
                                                   $_SESSION["module_gridTable_ID"]);
                                    ?>
                                 </div>
                              </div>
                              <?php spacer(20);?>
                              <div class="row">
                                 <div class="col-xs-4" style="padding-left:20px;">
                                    <a href="javascript:void(0);" id="popAveRating">AVERAGE RATING</a>
                                 </div>
                                 <div class="col-xs-8">
                                    <div class="row">
                                       <div class="col-xs-2">
                                          <span class="label">Rating Scale:</span>
                                       </div>
                                       <div class="col-xs-2">
                                          <span class="label">5 - Outstanding</span>
                                       </div>
                                       <div class="col-xs-2">
                                          <span class="label">4 - Very Satisfactory</span>
                                       </div>
                                       <div class="col-xs-2">
                                          <span class="label">3 - Satisfactory</span>
                                       </div>
                                       <div class="col-xs-2">
                                          <span class="label">2 - Unsatisfactory</span>
                                       </div>
                                       <div class="col-xs-2">
                                          <span class="label">1 - Poor</span>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-xs-2">
                                          <span class="label">Legend:</span>
                                       </div>
                                       <div class="col-xs-2">
                                          <span class="label">1 - Quality</span>
                                       </div>
                                       <div class="col-xs-2">
                                          <span class="label">2 - Efficiency</span>
                                       </div>
                                       <div class="col-xs-2">
                                          <span class="label">3 - Timeliness</span>
                                       </div>
                                       <div class="col-xs-2">
                                          <span class="label">4 - Average</span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <?php btnINRECLO([true,true,false]);
                                    echo '<button type="button"
                                               class="btn-cls4-lemon trnbtn"
                                               id="btnPRINT" name="btnPRINT">
                                             <i class="fa fa-print" aria-hidden="true"></i>&nbsp;
                                             PRINT
                                          </button>';
                              ?>
                           </div>
                        </div>
                       <div class="row panel-bottom"></div>
                     </div>
                  </div>
                  <div id="divView">
                     <div class="mypanel panel panel-default">
                        <div class="panel-top">
                           <span id="ScreenMode">INSERTING NEW OPCR</span>
                        </div>
                        <div class="panel-mid-litebg">
                           <div class="container" id="EntryScrn">
                              <div class="row">
                                 <div class="col-xs-6" id="EntryScrn">
                                    <div class="row" id="badgeRefId">
                                       <ul class="nav nav-pills">
                                          <li class="active" style="font-size:12pt;font-weight:600;">
                                             <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                                             </span></a>
                                          </li>
                                       </ul>
                                    </div>
                                    <div class="row margin-top10">
                                       <div class="form-group">
                                          <label class="control-label" for="inputs">STRATEGIC OBJECTIVES/FUNCTIONS</label><br>
                                          <textarea class="form-input saveFields--" rows="5" name="char_StrategicObjective"></textarea>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group">
                                          <label class="control-label" for="inputs">SUCCESS INDICATOR</label><br>
                                          <textarea class="form-input saveFields--" rows="5" name="char_SuccessIndicator"></textarea>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group">
                                          <label class="control-label" for="inputs">ACTUAL ACCOMPLISHMENTS</label>
                                          <textarea class="form-input saveFields--" rows="5" name="char_ActualAccomplishment"></textarea>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-xs-3">
                                          <div class="form-group">
                                             <label class="control-label" for="inputs">QUALITY</label>
                                             <?php
                                                createRatingScale("sint_QualityRate","QualityRate",0,"");
                                             ?>
                                          </div>
                                       </div>
                                       <div class="col-xs-3">
                                          <div class="form-group">
                                             <label class="control-label" for="inputs">EFFICIENCY</label>
                                             <?php
                                                createRatingScale("sint_EfficiencyRate","EfficiencyRate",0,"");
                                             ?>
                                          </div>
                                       </div>
                                       <div class="col-xs-3">
                                          <div class="form-group">
                                             <label class="control-label" for="inputs">TIMELINESS</label>
                                             <?php
                                                createRatingScale("sint_TimelinessRate","TimelinessRate",0,"");
                                             ?>
                                          </div>
                                       </div>
                                       <div class="col-xs-3">
                                          <div class="form-group">
                                             <label class="control-label" for="inputs">AVERAGE</label>
                                             <?php
                                                createRatingScale("sint_AverageRate","AverageRate",0,"");
                                             ?>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group">
                                          <label class="control-label" for="inputs">REMARKS:</label>
                                          <textarea class="form-input saveFields--" rows="5" name="char_Remarks"></textarea>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="panel-bottom">
                           <?php
                              btnSACABA([true,true,true]);
                           ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal fade border0" id="ModalAveRating" role="dialog">
               <div class="modal-dialog border0" style="padding:0px;width:97%;height:92%;">
                  <div class="mypanel border0" style="height:100%;">
                     <div class="panel-top bgSilver">
                        AVERAGE RATING
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <div class="panel-mid" id="OPCR" style="height:300px;">
                        <table border="1" width="100%" align="center">
                           <tr style="height:30px;text-align:center;">
                              <td>CATEGORY</td>
                              <td>Q1</td>
                              <td>E2</td>
                              <td>T3</td>
                              <td>A4</td>
                           </tr>
                           <tr style="height:50px;">
                              <td>Strategic Objective</td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                           </tr>
                           <tr style="height:50px;">
                              <td>Core Function</td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                           </tr>
                           <tr style="height:50px;">
                              <td>Support</td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                           </tr>
                           <tr style="height:30px;">
                              <td>Total Overall Rating</td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                           </tr>
                           <tr style="height:30px;">
                              <td>Final Average Rating</td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                           </tr>
                           <tr style="height:30px;">
                              <td>Adjective Rating</td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                           </tr>
                         </table>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade border0" id="prnModal" role="dialog">
               <div class="modal-dialog border0" style="padding:0px;width:97%;height:92%;">
                  <div class="mypanel border0" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Print Now" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include_once ("varHidden.e2e.php");
            ?>
         </div>
      </form>
   </body>
</html>




