<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <style type="text/css">
         textarea {resize: none;}
      </style>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_AfterTrn") ?>"></script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post">
         <?php $sys->SysHdr($sys,"ldms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("Position Profile"); ?>
            <div class="container-fluid margin-top">
               <div class="mypanel">
                  <div class="row">
                     <div class="col-xs-12" id="div_CONTENT">
                        <div class="margin-top">
                           <div id="divList">
                              <span id="spGridTable">
                                 <?php
                                       $table = "position_competency";
                                       $gridTableHdr_arr = ["Position","Name of Competency","Type of Competency","Level"];
                                       $gridTableFld_arr = ["PositionRefId","CompetencyRefId","Type","Level"];
                                       $sql = "SELECT * FROM position_competency";
                                       $Action = [true,true,true,false];
                                       doGridTable($table,
                                                   $gridTableHdr_arr,
                                                   $gridTableFld_arr,
                                                   $sql,
                                                   $Action,
                                                   $_SESSION["module_gridTable_ID"]);
                                 ?>
                              </span>
                              <?php
                                 btnINRECLO([true,false,false]);
                              ?>
                           </div>
                           <div id="divView">
                              <div class="mypanel panel panel-default">
                                 <div class="panel-top">
                                    <span id="ScreenMode">INSERTING NEW
                                 </div>
                                 <div class="panel-mid-litebg" id="EntryScrn">
                                    <div id="EntryScrn">
                                       <div class="row">
                                          <div class="col-xs-6">
                                             <div class="row">
                                                <div class="col-xs-12">
                                                   <label class="control-label" for="inputs">POSITION:</label>
                                                   <?php
                                                      createSelect("Position",
                                                                   "sint_PositionRefId",
                                                                   "",100,"Name","Select Position","");
                                                   ?>
                                                </div>
                                             </div>
                                             <div class="row margin-top">
                                                <div class="col-xs-12">
                                                   <label class="control-label" for="inputs">COMPETENCY NAME:</label>
                                                   <?php
                                                      createSelect("Competency",
                                                                   "sint_CompetencyRefId",
                                                                   "",100,"Name","Select Competency","");
                                                   ?>
                                                </div>
                                             </div>
                                             <div class="row margin-top">
                                                <div class="col-xs-12">
                                                   <label class="control-label" for="inputs">COMPETENCY TYPE:</label>
                                                   <input class="form-input saveFields-- mandatory" 
                                                          type="text"
                                                          name="char_Type"
                                                          id="char_Type" readonly>
                                                </div>
                                             </div>
                                             <div class="row margin-top">
                                                <div class="col-xs-12">
                                                   <label class="control-label" for="inputs">OPERATIONAL DEFINITION:</label>
                                                   <textarea class="form-input saveFields--" name="char_Definition" id="char_Definition" rows="3" readonly></textarea>
                                                </div>
                                             </div>
                                             <div class="row margin-top">
                                                <div class="col-xs-6">
                                                   <label class="control-label" for="inputs">PROFICIENCY LEVEL:</label>
                                                   <select class="form-input saveFields--" name="char_Level" id="char_Level">
                                                      <option value="">Select Level</option>
                                                      <option value="1">1</option>
                                                      <option value="2">2</option>
                                                      <option value="3">3</option>
                                                      <option value="4">4</option>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="row margin-top">
                                                <div class="col-xs-12">
                                                   <label class="control-label" for="inputs">INDICATOR:</label>
                                                   <textarea class="form-input saveFields--" name="char_Indicator" id="char_Indicator" rows="7" readonly></textarea>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel-bottom">
                                    <?php btnSACABA([true,true,true]); ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               $table = "position_competency";
               doHidden("paramTitle",getvalue("paramTitle"),"");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
   <script language="JavaScript">
      $(document).ready(function () {
         remIconDL();
         $("#sint_CompetencyRefId").change(function () {
            getCompetency($(this).val());
         });
         $("#char_Level").change(function () {
            getCompetencyIndicator($("#sint_CompetencyRefId").val(),$(this).val());
         });
      });
      function getCompetency(refid) {
         $.post("ldms_transaction.e2e.php",
         {
            fn:"getCompetency",
            refid:refid
         },
         function(data,status){
            if (status == 'success') {
               eval(data);
            }
         });
      }
      function getCompetencyIndicator(refid,lvl) {
         $.post("ldms_transaction.e2e.php",
         {
            fn:"getCompetencyIndicator",
            refid : refid,
            level : lvl
         },
         function(data,status){
            if (status == 'success') {
               $("#char_Indicator").val(data.trim());
            }
         });
      }
      function afterNewSave() {
         alert("Successfully Saved");
         gotoscrn("ldms_position_profile","");
      }
      function afterEditSave() {
         alert("Successfully Updated");
         gotoscrn("ldms_position_profile","");   
      }
   </script>
</html>



