<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript">
      </script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php
               doTitleBar("Uploading Data");
               spacer(5);
            ?>
            <div class="row">
               <div class="col-xs-6">
                  <label>Select Module:</label><br>
                  <select class="form-input">
                     <option value="1">PERSONAL INFORMATION</option>
                     <option value="2">FAMILY INFORMATION (SPOUSE AND PARENTS)</option>
                     <option value="3">CHILDREN INFORMATION</option>
                     <option value="4">ELIGIBILITY INFORMATION</option>
                     <option value="5">WORK EXPERIENCE INFORMATION</option>
                     <option value="6">VOLUNTARY WORK INFORMATION</option>
                     <option value="7">REFERENCE INFORMATION</option>
                     <option value="8">TRAININGS AND SEMINAR INFORMATION</option>
                     <option value="9">PDS QUESTIONS INFORMATION</option>
                     <option value="10">OTHER INFORMATION</option>
                     <option value="11">CURRENT EMPLOYEES INFORMATION</option>
                  </select>
               </div>
            </div>

            <?php
               footer();
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_Dashboard"); ?>"></script>
   </body>
</html>



