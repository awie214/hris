<?php 
   $emprefid = $_GET["hEmpRefId"];
   $table = "training_evaluation";
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript">
         $(document).ready(function () {
            remIconDL();
            $("#LocSAVE").click(function () {
               if (saveProceed() == 0) {
                  $(this).attr("type","submit"); 
               }
            });
            <?php
               if (isset($_GET["errmsg"])) {
                  echo '$.notify("'.$_GET["errmsg"].'");';
               }
            ?>
         });
         
         function afterDelete() {
            alert("Successfully Deleted");
            gotoscrn($("#hProg").val(),"");
         }
      </script>
   </head>
   <body>
      <form name="xForm" method="post" action="">
         <?php $sys->SysHdr($sys,"ldms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("Evaluation Form"); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div id="divList">
                        <div class="mypanel">
                           <div class="panel-top">List of Evaluations</div>
                           <div class="panel-mid">
                              <span id="spGridTable">
                                 
                                 <table class="table table-order-column table-striped table-bordered table-hover" id="gridTable">
                                    <thead>
                                       <tr style="color:#fff;font-weight:600;" class="text-center">
                                          <th style="width: 20%;">ACTION</th>
                                          <th>Employee Name</th>
                                          <th>Trainings/Programs</th>
                                          <th>Covered Date</th>
                                          <th>Provider</th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <?php
                                          $rs = SelectEach("assigned_courses","WHERE EmployeesRefId = '$emprefid'");
                                          if ($rs) {
                                             while ($row = mysqli_fetch_assoc($rs)) {
                                                $refid = $row["RefId"];
                                                $emprefid = $row["EmployeesRefId"];
                                                $emprow = FindFirst("employees","WHERE RefId = '$emprefid'","`FirstName`,`LastName`,`MiddleName`");
                                                $LastName      = $emprow["LastName"];
                                                $FirstName     = $emprow["FirstName"];
                                                $MiddleName    = $emprow["MiddleName"];
                                                $FullName      = $LastName.", ".$FirstName." ".$MiddleName;
                                                $program = FindFirst("ldmslndprogram","WHERE RefId = '".$row["LDMSLNDProgramRefId"]."'","*");
                                                if ($program) {
                                                   $where = "WHERE LDMSLNDProgramRefId = '".$program["RefId"]."' AND EmployeesRefId = '$emprefid'";
                                                   $check = FindFirst($table,$where,"RefId");
                                                   echo '<tr>';
                                                   echo '<td nowrap class="cellAction" style="padding:var(--grid_padding_td);">';
                                                   if (intval($check) > 0) {
                                                      echo
                                                      '
                                                      <button type="button" class="btn-cls4-sea"onclick="viewInfo('.$check.',2,\'\');">
                                                         EVALUATE
                                                      </button>
                                                      <button type="button" class="btn-cls4-lemon" onclick="selectMe('.$check.')">
                                                         <i class="fa fa-eye"></i>&nbsp;VIEW
                                                      </button>
                                                      ';
                                                   } else {
                                                      echo
                                                      '
                                                      <button type="button" class="btn-cls4-sea" onclick="evaluateMe('.$program["RefId"].')">
                                                         EVALUATE
                                                      </button>
                                                      <button type="button" class="btn-cls4-lemon" disabled>
                                                         <i class="fa fa-eye"></i>&nbsp;VIEW
                                                      </button>
                                                      ';
                                                   }
                                                   
                                                   echo '</td>';
                                                   echo '<td nowrap>'.$FullName.'</td>';
                                                   echo '<td>'.$program["Name"].'</td>';
                                                   if ($program["StartDate"] != "") {
                                                      echo '<td>'.date("F d, Y",strtotime($program["StartDate"]))." to ".date("F d, Y",strtotime($program["EndDate"])).'</td>';   
                                                   } else {
                                                      echo '<td></td>';
                                                   }
                                                   
                                                   echo '<td>'.$program["TrainingInstitution"].'</td>';
                                                   echo '</tr>';  
                                                }
                                             }
                                          }
                                       ?>
                                    </tbody>
                                 </table>
                              </span>
                           </div>
                           <div class="panel-bottom">
                              <?php
                                 btnINRECLO([true,true,false]);
                              ?>
                           </div>
                        </div>
                     </div>
                     <div id="divView">
                        <div class="row" id="EntryScrn">
                           <div class="col-xs-12">
                              <div class="mypanel">
                                 <div class="panel-top">
                                    <span id="ScreenMode">ADD NEW Evaluation
                                 </div>
                                 <div class="panel-mid">
                                    <div class="row">
                                       <div class="col-xs-12">
                                          <input type="hidden" name="sint_EmployeesRefId" id="sint_EmployeesRefId" class="saveFields--" value="<?php echo $emprefid; ?>">
                                          <div class="row margin-top">
                                             <div class="col-xs-6">
                                                <label>TITLE OF INTERVENTIONS:</label>
                                                <select class="form-input saveFields--" name="sint_LDMSLNDProgramRefId" id="sint_LDMSLNDProgramRefId">
                                                   <option value="">Select Training</option>
                                                   <?php
                                                      $rs2 = SelectEach("ldmslndprogram","");
                                                      if ($rs) {
                                                         while ($row2 = mysqli_fetch_assoc($rs2)) {
                                                            $refid = $row2["RefId"];
                                                            $title = $row2["Name"];
                                                            echo '<option value="'.$refid.'">'.$title.'</option>';
                                                         }
                                                      }
                                                   ?>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-12">
                                                <b>I. Design</b>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-4">
                                                <label>Clarity of Training Objectives:</label>
                                                <select class="form-input saveFields--" name="sint_Clarity" id="sint_Clarity">
                                                   <option value="0">Select Rating</option>
                                                   <option value="1">Poor</option>
                                                   <option value="2">Fair</option>
                                                   <option value="3">Satisfactory</option>
                                                   <option value="4">Very Satisfactory</option>
                                                   <option value="5">Excellent</option>
                                                </select>
                                             </div>
                                             <div class="col-xs-4">
                                                <label>Relevance of Content:</label>
                                                <select class="form-input saveFields--" name="sint_Relevance" id="sint_Relevance">
                                                   <option value="0">Select Rating</option>
                                                   <option value="1">Poor</option>
                                                   <option value="2">Fair</option>
                                                   <option value="3">Satisfactory</option>
                                                   <option value="4">Very Satisfactory</option>
                                                   <option value="5">Excellent</option>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-4">
                                                <label>Sequence Content:</label>
                                                <select class="form-input saveFields--" name="sint_Sequence" id="sint_Sequence">
                                                   <option value="0">Select Rating</option>
                                                   <option value="1">Poor</option>
                                                   <option value="2">Fair</option>
                                                   <option value="3">Satisfactory</option>
                                                   <option value="4">Very Satisfactory</option>
                                                   <option value="5">Excellent</option>
                                                </select>
                                             </div>
                                             <div class="col-xs-4">
                                                <label>Usefulness of Learning Materials:</label>
                                                <select class="form-input saveFields--" name="sint_Usefulness" id="sint_Usefulness">
                                                   <option value="0">Select Rating</option>
                                                   <option value="1">Poor</option>
                                                   <option value="2">Fair</option>
                                                   <option value="3">Satisfactory</option>
                                                   <option value="4">Very Satisfactory</option>
                                                   <option value="5">Excellent</option>
                                                </select>
                                             </div>
                                          </div>
                                          <?php bar(); ?>
                                          <div class="row margin-top">
                                             <div class="col-xs-12">
                                                <b>II. Facilitation</b>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-4">
                                                <label>Mastery of the Subject Matter:</label>
                                                <select class="form-input saveFields--" name="sint_Mastery" id="sint_Mastery">
                                                   <option value="0">Select Rating</option>
                                                   <option value="1">Poor</option>
                                                   <option value="2">Fair</option>
                                                   <option value="3">Satisfactory</option>
                                                   <option value="4">Very Satisfactory</option>
                                                   <option value="5">Excellent</option>
                                                </select>
                                             </div>
                                             <div class="col-xs-4">
                                                <label>Time Management:</label>
                                                <select class="form-input saveFields--" name="sint_TimeManagement" id="sint_TimeManagement">
                                                   <option value="0">Select Rating</option>
                                                   <option value="1">Poor</option>
                                                   <option value="2">Fair</option>
                                                   <option value="3">Satisfactory</option>
                                                   <option value="4">Very Satisfactory</option>
                                                   <option value="5">Excellent</option>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-4">
                                                <label>Appropriateness of Learning Methodologies:</label>
                                                <select class="form-input saveFields--" name="sint_Appropriateness" id="sint_Appropriateness">
                                                   <option value="0">Select Rating</option>
                                                   <option value="1">Poor</option>
                                                   <option value="2">Fair</option>
                                                   <option value="3">Satisfactory</option>
                                                   <option value="4">Very Satisfactory</option>
                                                   <option value="5">Excellent</option>
                                                </select>
                                             </div>
                                             <div class="col-xs-4">
                                                <label>Professional Conduct:</label>
                                                <select class="form-input saveFields--" name="sint_Professional" id="sint_Professional">
                                                   <option value="0">Select Rating</option>
                                                   <option value="1">Poor</option>
                                                   <option value="2">Fair</option>
                                                   <option value="3">Satisfactory</option>
                                                   <option value="4">Very Satisfactory</option>
                                                   <option value="5">Excellent</option>
                                                </select>
                                             </div>
                                          </div>
                                          <?php bar(); ?>
                                          <div class="row margin-top">
                                             <div class="col-xs-12">
                                                <b>III. Administration</b>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-4">
                                                <label>Learning Environment:</label>
                                                <select class="form-input saveFields--" name="sint_Learning" id="sint_Learning">
                                                   <option value="0">Select Rating</option>
                                                   <option value="1">Poor</option>
                                                   <option value="2">Fair</option>
                                                   <option value="3">Satisfactory</option>
                                                   <option value="4">Very Satisfactory</option>
                                                   <option value="5">Excellent</option>
                                                </select>
                                             </div>
                                             <div class="col-xs-4">
                                                <label>Pre-Course Coordination:</label>
                                                <select class="form-input saveFields--" name="sint_Coordination" id="sint_Coordination">
                                                   <option value="0">Select Rating</option>
                                                   <option value="1">Poor</option>
                                                   <option value="2">Fair</option>
                                                   <option value="3">Satisfactory</option>
                                                   <option value="4">Very Satisfactory</option>
                                                   <option value="5">Excellent</option>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-4">
                                                <label>Assistance to Participants:</label>
                                                <select class="form-input saveFields--" name="sint_Assistance" id="sint_Assistance">
                                                   <option value="0">Select Rating</option>
                                                   <option value="1">Poor</option>
                                                   <option value="2">Fair</option>
                                                   <option value="3">Satisfactory</option>
                                                   <option value="4">Very Satisfactory</option>
                                                   <option value="5">Excellent</option>
                                                </select>
                                             </div>
                                          </div>
                                          <?php bar(); ?>
                                          <div class="row margin-top">
                                             <div class="col-xs-4">
                                                <label>Overall Rating for this Training:</label>
                                                <select class="form-input saveFields--" name="sint_Overall" id="sint_Overall">
                                                   <option value="0">Select Rating</option>
                                                   <option value="1">Poor</option>
                                                   <option value="2">Fair</option>
                                                   <option value="3">Satisfactory</option>
                                                   <option value="4">Very Satisfactory</option>
                                                   <option value="5">Excellent</option>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-4">
                                                <label>Comment/Remarks:</label>
                                                <textarea class="form-input saveFields--" name="char_Comments" id="char_Comments" rows="3" style="resize: none;"></textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel-bottom">
                                    <?php
                                       btnSACABA([true,true,true]);
                                    ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
   <script type="text/javascript">
      $(document).ready(function () {
         $("#sint_LDMSLNDProgramRefId").change(function () {
            var emprefid = $("#hEmpRefId").val();
            $("#sint_EmployeesRefId").val(emprefid);
         });
      });
      function evaluateMe(refid) {
         $("#divList").hide();
         $("#divView").show();
         $("#sint_LDMSLNDProgramRefId").val(refid);
      }
      function afterNewSave() {
         alert("Successfully Saved");
         gotoscrn($("#hProg").val(),"");
      }
      function afterEditSave() {
         alert("Successfully Saved");
         gotoscrn($("#hProg").val(),"");
      }
      function selectMe(refid){
         $("#rptContent").attr("src","blank.htm");
         var rptFile = "rpt_TrainingEvaluation";
         var url = "ReportCaller.e2e.php?file=" + rptFile;
         url += "&refid=" + refid;
         url += "&" + $("[name='hgParam']").val();
         $("#prnModal").modal();
         $("#rptContent").attr("src",url);
      }
   </script>
</html>