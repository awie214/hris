<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <style type="text/css">
         .rpt_row:hover {
            padding-left: 10px;
            transition: 0.5s;
            font-weight: 500;
         }
         .rpt_row {
            padding: 10px;
            margin-left: 20px;
         }
      </style>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post">
         <?php $sys->SysHdr($sys,"ldms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("LDMS Reports"); ?>
            <div class="container-fluid margin-top">
               <div class="mypanel">
                  <div class="row">
                     <div class="col-xs-12">
                        <div class="panel-top">Reports</div>
                        <div class="panel-mid-litebg" style="padding: 10px;">
                           <div class="row">
                              <div class="col-xs-12">
                                 <label>Select Report</label>
                                 <select class="form-input" id="report_type" name="report_type">
                                    <option value="">Select Report</option>
                                    <option value="rpt_competency_assessment">
                                       Summary of Competency Assessment
                                    </option>
                                    <option value="rpt_return_service">
                                       Status of Return Service Agreement
                                    </option>
                                    <option value="rpt_lnd_intervention">
                                       Summary of L&D Interventions
                                    </option>
                                    <option value="rpt_training_effectiveness">
                                       Summary of Training Effectiveness Rating
                                    </option>
                                    <option value="rpt_competency">
                                       Competency Table
                                    </option>
                                    <option value="rpt_individual_competency_data">
                                       Individual Competency Data
                                    </option>
                                    <option value="rpt_individual_development_plan">
                                       Individual Development Plan
                                    </option>
                                    <option value="rpt_training_list">
                                       List of Training Programs
                                    </option>
                                    <option value="rpt_participants_feedback">
                                       Summary of Participants' Feedback
                                    </option>
                                    <option value="rpt_lnd_monitoring_for_employees">
                                       Monitoring of the L & D Intervention Provided to Employees
                                    </option>
                                    <option value="rpt_budget_utilization">
                                       Budget Utilization Report on Trainings Conducted
                                    </option>
                                    <option value="rpt_Service_Provider">
                                       Learning Service Provider
                                    </option>
                                 </select>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-4">
                                 <label>Select Quarter</label>
                                 <select class="form-input" id="quarter" name="quarter">
                                    <option value="">Yearly</option>
                                    <option value="1">1st Quarter</option>
                                    <option value="2">2nd Quarter</option>
                                    <option value="3">3rd Quarter</option>
                                    <option value="4">4th Quarter</option>
                                 </select>
                              </div>
                              <div class="col-xs-4">
                                 <label>Year</label>
                                 <select class="form-input" id="year" name="year">
                                    <?php
                                       $backyear  = date("Y",time());
                                       for ($i=0; $i <= 3 ; $i++) { 
                                          echo '<option value="'.($backyear-$i).'">'.($backyear-$i).'</option>';
                                       }
                                    ?>
                                 </select>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <br>
                                 <button type="button" class="btn-cls4-tree" id="generate">GENERATE REPORT</button>
                              </div>
                           </div>
                        </div>
                        <div class="panel-bottom"></div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal fade border0" id="modalLDMS" role="dialog">
               <div class="modal-dialog border0" style="padding:0px;width:90%;height:92%;">
                  <div class="mypanel border0" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                              <i class="fa fa-print" aria-hidden="true"></i>
                           </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               doHidden("paramTitle",getvalue("paramTitle"),"");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
   <script language="JavaScript">
      $(document).ready(function () {
         $("#generate").click(function () {
            var file       = $("#report_type").val();
            var url        = "ReportCaller.e2e.php?file=" + file;   
            url            += "&" + $("[name='hgParam']").val();
            url            += "&quarter=" + $("#quarter").val();
            url            += "&year=" + $("#year").val();
            if (file == "") {
               $.notify("No Report Selected");
               return false;
            }
            $("#rptContent").attr("src",url);
            $("#modalLDMS").modal();
         });
      });
   </script>
</html>



