<?php
   $emprefid   = getvalue("hEmpRefId");
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>

   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post">
         <?php $sys->SysHdr($sys,"ldms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("Learning And Development Intervention"); ?>
            <div class="container-fluid margin-top">
               <div class="mypanel">
                  <div>
                     <div class="row">
                        <div class="col-xs-12" id="div_CONTENT">
                           <div class="margin-top">
                              <div id="divList">
                                 <div class="panel-top">
                                    List of Trainings & Programs
                                 </div>
                                 <div class="panel-mid">
                                    <table class="table table-order-column table-striped table-bordered table-hover" id="gridTable">
                                       <thead>
                                          <tr style="color:#fff;font-weight:600;" class="text-center">
                                             <th style="width: 20%;">ACTION</th>
                                             <th>Course Title</th>
                                             <th>Training Institution</th>
                                             <th>Covered Date</th>
                                             <th>Venue</th>
                                             <th>Deadline</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <?php
                                             $rs = SelectEach("ldmslndprogram","WHERE Posted > 0");
                                             if ($rs) {
                                                while ($row = mysqli_fetch_assoc($rs)) {
                                                   $refid = $row["RefId"];
                                                   $where = "WHERE EmployeesRefId = '$emprefid' AND LDMSLNDProgramRefId = '$refid'";
                                                   $check = FindFirst("assigned_courses",$where,"RefId");
                                                   echo '<tr>';
                                                   if (intval($check) > 0) {
                                                      echo '
                                                         <td class="text-center">
                                                            <button type="button" class="btn-cls4-sea" disabled>
                                                               APPLIED
                                                            </button>
                                                            <button type="button" class="btn-cls4-lemon" onclick="selectMe('.$check.')">
                                                               <i class="fa fa-eye"></i>&nbsp;VIEW
                                                            </button>
                                                         </td>
                                                      ';
                                                   } else {
                                                      echo '
                                                         <td class="text-center">
                                                            <button type="button" class="btn-cls4-sea" onclick="apply('.$refid.')">
                                                               <i class="fa fa-check"></i>&nbsp;APPLY
                                                            </button>
                                                            <button type="button" class="btn-cls4-lemon" onclick="viewInfo('.$refid.',3,\'\')">
                                                               <i class="fa fa-eye"></i>&nbsp;VIEW
                                                            </button>
                                                         </td>
                                                      ';   
                                                   }
                                                   $StartDate  = $row["StartDate"];
                                                   $EndDate    = $row["EndDate"];
                                                   if ($StartDate != "") {
                                                      if ($StartDate == $EndDate) {
                                                         $Date = date("F d, Y",strtotime($StartDate));
                                                      } else {
                                                         $Date = date("F d, Y",strtotime($StartDate))." to ".date("F d, Y",strtotime($EndDate));
                                                      }
                                                   } else {
                                                      $Date = "Not Set";
                                                   }
                                                   
                                                   echo '
                                                         <td>'.$row["Name"].'</td>
                                                         <td class="text-center">'.$row["TrainingInstitution"].'</td>
                                                         <td class="text-center">'.$Date.'</td>
                                                         <td>'.$row["Venue"].'</td>
                                                         <td class="text-center">'.date("F d, Y",strtotime($row["Deadline"])).'</td>
                                                      </tr>
                                                   ';
                                                }
                                             }
                                          ?>
                                       </tbody>
                                    </table>
                                 </div>
                                 <div class="panel-bottom"></div>
                              </div>
                              
                              <div id="divView">
                                 <div class="mypanel panel panel-default">
                                    <div class="panel-top">
                                       <span id="ScreenMode">INSERTING NEW INTERVENTION
                                    </div>
                                    <div class="panel-mid-litebg" id="EntryScrn">
                                       <div>
                                          <div class="row">
                                             <div class="col-xs-8">
                                                <label class="control-label" for="inputs">Course Title:</label><br>
                                                <input class="form-input saveFields-- mandatory" 
                                                       type="text"
                                                       name="char_Title"
                                                       id="char_Title">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-8">
                                                <label class="control-label" for="inputs">Training Institution:</label><br>
                                                <input class="form-input saveFields-- mandatory" 
                                                       type="text"
                                                       name="char_TrainingInstitution"
                                                       id="char_TrainingInstitution">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-4">
                                                <label class="control-label" for="inputs">Start Date:</label><br>
                                                <input class="form-input saveFields-- mandatory date--" 
                                                       type="text"
                                                       name="date_StartDate"
                                                       id="date_StartDate">
                                             </div>
                                             <div class="col-xs-4">
                                                <label class="control-label" for="inputs">End Date:</label><br>
                                                <input class="form-input saveFields-- mandatory date--" 
                                                       type="text"
                                                       name="date_EndDate"
                                                       id="date_EndDate">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-8">
                                                <label class="control-label" for="inputs">Venue:</label><br>
                                                <input class="form-input saveFields--" 
                                                       type="text"
                                                       name="char_Venue"
                                                       id="char_Venue">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-4">
                                                <label class="control-label" for="inputs">Slots:</label><br>
                                                <input class="form-input saveFields--" 
                                                       type="text"
                                                       name="char_Slot"
                                                       id="char_Slot">
                                             </div>
                                             <div class="col-xs-4">
                                                <label class="control-label" for="inputs">Cost:</label><br>
                                                <input class="form-input saveFields--" 
                                                       type="text"
                                                       name="char_Cost"
                                                       id="char_Cost">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-8">
                                                <label>COMPETENCY</label>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-8">
                                                <label class="control-label" for="inputs">Core:</label><br>
                                                <input class="form-input saveFields--" 
                                                       type="text"
                                                       name="char_Core"
                                                       id="char_Core">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-8">
                                                <label class="control-label" for="inputs">Foundation:</label><br>
                                                <input class="form-input saveFields--" 
                                                       type="text"
                                                       name="char_Foundation"
                                                       id="char_Foundation">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-8">
                                                <label class="control-label" for="inputs">Organizational:</label><br>
                                                <input class="form-input saveFields--" 
                                                       type="text"
                                                       name="char_Organizational"
                                                       id="char_Organizational">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-8">
                                                <label class="control-label" for="inputs">Leadership:</label><br>
                                                <input class="form-input saveFields--" 
                                                       type="text"
                                                       name="char_Leadership"
                                                       id="char_Leadership">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-8">
                                                <label class="control-label" for="inputs">Technical:</label><br>
                                                <input class="form-input saveFields--" 
                                                       type="text"
                                                       name="char_Technical"
                                                       id="char_Technical">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel-bottom">
                                       <?php btnSACABA([true,true,true]); ?>
                                    </div>
                                 </div>
                              </div>

                              <div id="apply_canvas">
                                 <div class="row">
                                    <div class="col-xs-6">
                                       <div class="mypanel panel panel-default">
                                          <div class="panel-top">
                                             NOMINATION FORM
                                          </div>
                                          <div class="panel-mid-litebg">
                                             <div class="row margin-top">
                                                <div class="col-xs-12">
                                                   <label class="control-label" for="inputs">Date Filed:</label><br>
                                                   <input class="form-input saveFields-- mandatory date--" 
                                                       type="text"
                                                       name="FiledDate"
                                                       id="FiledDate" readonly value="<?php echo date("Y-m-d",time()); ?>">
                                                </div>
                                             </div>
                                             <div class="row margin-top">
                                                <div class="col-xs-12">
                                                   <label class="control-label" for="inputs">Course Title:</label><br>
                                                   <select class="form-input saveFields--" name="LDMSLNDProgramRefId" id="LDMSLNDProgramRefId">
                                                      <option value="">Select Training</option>
                                                      <?php
                                                         $rs2 = SelectEach("ldmslndprogram","");
                                                         if ($rs) {
                                                            while ($row2 = mysqli_fetch_assoc($rs2)) {
                                                               $refid = $row2["RefId"];
                                                               $title = $row2["Name"];
                                                               echo '<option value="'.$refid.'">'.$title.'</option>';
                                                            }
                                                         }
                                                      ?>
                                                      
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="row margin-top">
                                                <div class="col-xs-12">
                                                   <label class="control-label" for="inputs">Endorsed By:</label><br>
                                                   <input class="form-input saveFields-- mandatory" 
                                                       type="text"
                                                       name="char_EndorsedBy"
                                                       id="char_EndorsedBy">
                                                </div>
                                             </div>
                                          </div>
                                          <div class="panel-bottom">
                                             <button class="btn-cls4-sea" type="button" id="save_">
                                                <i class="fa fa-save"></i> SAVE
                                             </button>
                                             <button class="btn-cls4-red" type="button" id="reload_">
                                                <i class="fa fa-back"></i> BACK
                                             </button>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               $table = "ldmslndprogram";
               doHidden("paramTitle",getvalue("paramTitle"),"");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
   <script language="JavaScript">
      $(document).ready(function () {
         remIconDL();
         $("#apply_canvas").hide();
         $("#reload_").click(function () {
            gotoscrn("ldms_trainings_and_programs","");
         });
         $("#save_").click(function () {
            var FiledDate = $("#FiledDate").val();
            var refid = $("#LDMSLNDProgramRefId").val();
            var EndorsedBy = $("#char_EndorsedBy").val();
            saveNomination(FiledDate,refid,EndorsedBy)            
         });
      });
      function selectMe(refid){
         $("#rptContent").attr("src","blank.htm");
         var rptFile = "rpt_nomination_form";
         var url = "ReportCaller.e2e.php?file=" + rptFile;
         url += "&refid=" + refid;
         url += "&" + $("[name='hgParam']").val();
         $("#prnModal").modal();
         $("#rptContent").attr("src",url);
      }
      function saveNomination(FiledDate,refid,EndorsedBy) {
         $.get("ldms_transaction.e2e.php",
         {
            fn: "saveNomination",
            FiledDate : FiledDate,
            LDMSLNDProgramRefId: refid,
            EndorsedBy: EndorsedBy,
            EmployeesRefId: $("#hEmpRefId").val()
         },
         function(data,status) {
            if (status == "success") {
               try {
                  eval(data);
               } catch (e) {
                  if (e instanceof SyntaxError) {
                     alert(e.message);
                  }
               }
            }
         });
      }
      function afterNewSave() {
         alert("Successfully Saved");
         gotoscrn("ldms_trainings_and_programs","");
      }
      function afterEditSave() {
         alert("Successfully Updated");
         gotoscrn("ldms_trainings_and_programs","");
      }
      function apply(refid) {
         $("#program_id").val(refid);
         $("#divList").hide();
         $("#apply_canvas").show();
         $("#LDMSLNDProgramRefId").val(refid);
      }
   </script>
</html>



