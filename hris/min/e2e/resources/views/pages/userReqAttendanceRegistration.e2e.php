<?php
   require_once "conn.e2e.php";
   require_once "constant.e2e.php";
   require_once pathClass.'0620RptFunctions.e2e.php';
   $table = "employees";
   $emprefid = getvalue("hEmpRefId");
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      
      <style type="text/css">

      </style>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar ("REQUEST FOR ATTENDANCE REGISTRATION"); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div class="container-fluid rptBody">
                        <div>
                           <div class="row" id="divList">
                              <div class="col-xs-12">
                                 <div class="panel-top">
                                    List of Attendance Registration Request
                                 </div>
                                 <div class="panel-mid">
                                    <span id="spGridTable">
                                       <?php
                                          $table = "attendance_request";
                                          $gridTableHdr_arr = ["Reason","File Date","Date Applied For", "Status"];
                                          $gridTableFld_arr = ["Type","FiledDate","AppliedDateFor", "Status"];
                                          $sql = "SELECT * FROM $table WHERE EmployeesRefId = '$emprefid' ORDER BY FiledDate";
                                          $Action = [false,true,false,true];
                                          doGridTable($table,
                                                      $gridTableHdr_arr,
                                                      $gridTableFld_arr,
                                                      $sql,
                                                      $Action,
                                                      "gridTable");
                                       ?>
                                    </span>
                                 </div>
                                 <div class="panel-bottom">
                                    <button type="button" class="btn-cls4-sea" id="btnINSERT">
                                       <i class="fa fa-file"></i>&nbsp;&nbsp;&nbsp;NEW REQUEST
                                    </button>
                                    <button type="button" class="btn-cls4-tree" id="btnPrintMultiple">
                                       <i class="fa fa-print"></i>&nbsp;&nbsp;Print Multiple Request
                                    </button>
                                    <label>Date From:&nbsp;&nbsp;</label>
                                    <input type="text" name="from_date" id="from_date" class="form-input date--">
                                    <label>Date To:&nbsp;&nbsp;</label>
                                    <input type="text" name="to_date" id="to_date" class="form-input date-- dateto" for="from_date">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div id="divView">
                           <div id="">
                              <div class="row margin-top" id="EntryScrn">
                                 <div class="col-xs-6">
                                    <div class="panel-top">
                                       NEW REQUEST FOR ATTENDANCE REGISTRATION
                                    </div>
                                    <input type="hidden" 
                                           class="saveFields--"
                                           name="sint_EmployeesRefId" 
                                           id="sint_EmployeesRefId" 
                                           value="<?php echo $emprefid; ?>">
                                    <div class="panel-mid" style="padding: 10px;">
                                       <div class="row">
                                          <div class="col-xs-6">
                                             <label>DATE OF FILING:</label>
                                             <br>
                                             <input type="text"
                                                    name="date_FiledDate" 
                                                    id="date_FiledDate" 
                                                    class="form-input saveFields-- date--" 
                                                    value="<?php echo date("Y-m-d",time()); ?>" 
                                                    readonly>
                                          </div>
                                          <div class="col-xs-6">
                                             <label>APPLICATION DATE FOR</label>
                                             <br>
                                             <input type="text" 
                                                    class="form-input saveFields-- date-- mandatory" 
                                                    name="date_AppliedDateFor" 
                                                    id="date_AppliedDateFor">
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-12">
                                             <label>REASON</label>
                                             <br>
                                             <select class="form-input saveFields-- mandatory" name="sint_Type" id="sint_Type">
                                                <option value="">Select Reason</option>
                                                <option value="FORGOT TO FINGER SCAN">
                                                   FORGOT TO FINGER SCAN
                                                </option>
                                                <option value="BIOMETRIC FINGERSCAN MALFUNCTION">
                                                   BIOMETRIC FINGERSCAN MALFUNCTION
                                                </option>
                                                <option value="IN A MEETING">
                                                   IN A MEETING
                                                </option>
                                             </select>
                                          </div>
                                       </div>
                                       <?php
                                          if ($CompanyId == 35) {
                                       ?>
                                       <div class="row margin-top">
                                          <div class="col-xs-6">
                                             <div class="form-group">
                                                <label class="control-label" for="inputs">
                                                   Recommended by:
                                                </label>
                                                <?php
                                                   createSelect("signatories",
                                                                "sint_Signatory1",
                                                                "",100,"Name","Select Signatory","",false);
                                                ?>
                                             </div>
                                          </div>
                                       </div>
                                       <?php
                                          }
                                       ?>
                                    </div>
                                    <div class="panel-bottom">
                                       <div class="row">
                                          <div class="col-xs-12">
                                             <button type="button" class="btn-cls4-sea" id="btnSAVE">
                                                <i class="fa fa-save"></i>&nbsp;REQUEST
                                             </button>
                                             <button type="button" class="btn-cls4-red" id="btnCANCEL">CANCEL</button>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>   
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               $table = "attendance_request";
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
   <script type="text/javascript">
      $(document).ready(function () {
         $("#availment_entry").hide();
         $("#btn_add_availment").click(function () {
            $("#divList").slideUp();
            $("#divView").show(500);   
         });
         $("#btnPrintMultiple").click(function () {
            var from       = $("#from_date").val();
            var to         = $("#to_date").val();
            if (from != "" && to != "") {
               $("#rptContent").attr("src","blank.htm");
               var rptFile = "rpt_Attendance_Request_Multiple_14";
               var url = "ReportCaller.e2e.php?file=" + rptFile;
               url += "&date_from=" + from;
               url += "&date_to=" + to;
               url += "&" + $("[name='hgParam']").val();
               $("#prnModal").modal();
               $("#rptContent").attr("src",url);         
            } else {
               $.notify("Incorrect Date Range");
            }
         });
      });
      function selectMe(refid) {
         printAttachment(refid)
      }
      function printAttachment(refid){
         $("#rptContent").attr("src","blank.htm");
         var cid = $("#hCompanyID").val();
         var rptFile = "";
         switch (cid) {
            case "14":
               rptFile = "rpt_Attendance_Request_14";
               break;
            case "35":
               rptFile = "rpt_Attendance_Request_35";
               break;
            case "21":
               rptFile = "rpt_Attendance_Request_21";
               break;
            default:
               rptFile = "rpt_Attendance_Request";
               break;
         }
         var url = "ReportCaller.e2e.php?file=" + rptFile;
         url += "&refid=" + refid;
         url += "&" + $("[name='hgParam']").val();
         $("#prnModal").modal();
         $("#rptContent").attr("src",url);
      }
      function afterEditSave(refid) {
         alert("Record Updated");
         gotoscrn($("#hProg").val(),"");
      }
      function afterNewSave(newRefId) {
         alert("Successfully Requested. Print the Form and Proceed to the Security Officer.");
         gotoscrn($("#hProg").val(),"");
      }
   </script>
</html>



