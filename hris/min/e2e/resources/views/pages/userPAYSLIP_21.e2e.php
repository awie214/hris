<?php
   function getAmount($table,$where) {
      $result = pms_FindFirst($table,$where,"amount");
      if ($result) {
         return $result;
      } else {
         return 0;
      }
   }
   if (isset($_POST["month_"])) {
      $m = $_POST["month_"];
   } else {
      $m = intval(date("m",time()));
   }
   if (isset($_POST["year_"])) {
      $y = $_POST["year_"];
   } else {
      $y = date("Y",time());
   }
   $month_name = monthName($m,1);
   $emprefid   = getvalue("hEmpRefId");
   $AgencyId   = FindFirst("employees","WHERE RefId = '$emprefid'","AgencyId");
   $emp_id     = pms_FindFirst("pms_employees","WHERE employee_number = '$AgencyId'","id");
   $row_emp    = FindFirst("employees","WHERE RefId = '$emprefid'","*");
   if ($row_emp) {
      $FullName = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["ExtName"]." ".$row_emp["MiddleName"];
   } else {
      $FullName = "";
   }
   $emp_info   = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
   if ($emp_info) {
      $Position = $emp_info["PositionRefId"];
      $Position = getRecord("Position",$Position,"Name");
      $Office   = $emp_info["OfficeRefId"];
      $Office   = getRecord("Office",$Office,"Name");
      $Division = $emp_info["DivisionRefId"];
      $Division = getRecord("Division",$Division,"Name");
   } else {
      $Position = $Office = "";
   }

   $total_basicpay_amount  = 0; 
   $pagibig_share          = 0;
   $phic_share             = 0;
   $tax_amount             = 0;
   $total_loan             = 0;
   $undertime_sala         = 0;
   $absent_sala            = 0;
   $pera                   = 0;
   $rata1                  = 0;
   $rata2                  = 0;
   $sa                     = 0;
   $la                     = 0;
   $hazard                 = 0;
   $ra                     = 0;
   $lp                     = 0;
   $pdiem                  = 0;
   $sala                   = 0;
   $gsis_ee_share          = 0;
   $rata                   = 0;
   $deduction              = 0;
   $benefits               = 0;
   $gross_income           = 0;
   $net_income             = 0;
   $total_otherdeduct      = 0;
   $used_amount            = 0;
   $ta_covered             = "";
   $pera_id                = "";
   $rata1_id               = "";
   $rata2_id               = "";
   $hazard_id              = "";
   $sa_id                  = "";
   $la_id                  = "";
   $ra_id                  = "";
   $lp_id                  = "";
   $pdiem_id               = "";

   $trn_where     = "WHERE employee_id = '$emp_id' AND year = '$y' AND month = '$m'";
   $trn_row       = pms_FindFirst("pms_transactions",$trn_where,"*");


   $benefits_rs   = pms_SelectEach("pms_benefits","");
   if ($benefits_rs) {
      while ($benefit_row = mysqli_fetch_assoc($benefits_rs)) {
         $code = $benefit_row["code"];
         switch ($code) {
            case 'PERA':
               $pera_id    = $benefit_row["id"];
               break;
            case 'RATA1':
               $rata1_id   = $benefit_row["id"];
               break;
            case 'RATA2':
               $rata2_id   = $benefit_row["id"];
               break;
            case 'HP':
               $hazard_id  = $benefit_row["id"];
               break;
            case 'SA':
               $sa_id      = $benefit_row["id"]; 
               break;
            case 'LA':
               $la_id      = $benefit_row["id"];
               break;
            case 'RA':
               $ra_id      = $benefit_row["id"];
               break;
            case 'LP':
               $lp_id      = $benefit_row["id"];
               break;
            case 'PDIEM':
               $pdiem_id   = $benefit_row["id"];
               break;
         }
      }
   }

   

   $pera_where    = $trn_where." AND benefit_id = '$pera_id'";
   $rata1_where   = $trn_where." AND benefit_id = '$rata1_id'";
   $rata2_where   = $trn_where." AND benefit_id = '$rata2_id'";
   $sa_where      = $trn_where." AND benefit_id = '$sa_id'";
   $la_where      = $trn_where." AND benefit_id = '$la_id'";
   $hazard_where  = $trn_where." AND benefit_id = '$hazard_id'";
   $ra_where      = $trn_where." AND benefit_id = '$ra_id'";
   $lp_where      = $trn_where." AND benefit_id = '$lp_id'";

   
   if ($trn_row) {
      $total_basicpay_amount  = $trn_row["total_basicpay_amount"];   
      $pagibig_share          = $trn_row["pagibig_share"];
      $phic_share             = $trn_row["phic_share"];
      $tax_amount             = $trn_row["tax_amount"];
      $total_loan             = $trn_row["total_loan"];
      $gsis_ee_share          = $trn_row["gsis_ee_share"];
      $total_otherdeduct      = $trn_row["total_otherdeduct"];

      

      // $pera          = getAmount("pms_benefitsinfo_transactions",$pera_where);
      // $rata1         = getAmount("pms_benefitsinfo_transactions",$rata1_where);
      // $rata2         = getAmount("pms_benefitsinfo_transactions",$rata2_where);
      // $sa            = getAmount("pms_benefitsinfo_transactions",$sa_where);
      // $la            = getAmount("pms_benefitsinfo_transactions",$la_where);
      // $hazard        = getAmount("pms_benefitsinfo_transactions",$hazard_where);
      // $ra            = getAmount("pms_benefitsinfo_transactions",$ra_where);
      // $lp            = getAmount("pms_benefitsinfo_transactions",$lp_where);

      $rs            = pms_SelectEach("pms_benefitsinfo_transactions",$trn_where);
      if ($rs) {
         while ($n_row = mysqli_fetch_assoc($rs)) {
            $id      = $n_row["benefit_id"];
            $amount  = $n_row["amount"];
            $undertime_sala += floatval($n_row["sala_undertime_amount"]);
            $absent_sala    += floatval($n_row["sala_absent_amount"]);
            switch ($id) {
               case $pera_id:
                  $pera = $amount;
                  break;
               case $rata1_id:
                  $rata1 = $amount;
                  break;
               case $rata2_id:
                  $rata2 = $amount;
                  $used_amount = $n_row["used_amount"];
                  $ta_covered = $n_row["ta_covered_date"];
                  break;
               case $sa_id:
                  $sa = $amount;
                  break;
               case $la_id:
                  $la = $amount;
                  break;
               case $hazard_id:
                  $hazard = $amount;
                  break;
               case $lp_id:
                  $lp = $amount;
                  break;
               case $pdiem_id:
                  $pdiem = $amount;
                  break;
            }
         }
      }

      $sala          = floatval($sa) + floatval($la);
      $rata          = floatval($rata1) + floatval($rata2);
      $deduction     = $pagibig_share + $phic_share + $tax_amount + $total_loan + $gsis_ee_share;
      $deduction     = $deduction + $undertime_sala + $absent_sala + $total_otherdeduct + $used_amount;
      $benefits      = $pera + $rata + $sala + $hazard + $lp + $pdiem;
      $gross_income  = $total_basicpay_amount + $benefits;
      $net_income    = $gross_income - $deduction;
   }
?>
<!DOCTYPE>
<html>
<head>
   <title></title>
   <?php include_once $files["inc"]["pageHEAD"]; ?>
   <script type="text/javascript">
      $(document).ready(function () {
         $("#btnPrint").click(function () {
            var head = $("head").html();
            printDiv('div_CONTENT',head);
         });
         <?php
            if (isset($_POST["month_"])) {
               $m = $_POST["month_"];
            } else {
               $m = intval(date("m",time()));
            }

            if (isset($_POST["year_"])) {
               $y = $_POST["year_"];
            } else {
               $y = date("Y",time());
            }
            echo '$("#month_").val("'.$m.'");';        
            echo '$("#year_").val("'.$y.'");';        
            if (!$trn_row) {
               echo '$("#div_CONTENT").html("<div id=\'not_avail\'>PAYSLIP NOT AVAILABLE</div>");';
            }
         ?>

      });
   </script>
   <style type="text/css">
      #not_avail {
         font-weight: 900;
         font-size: 15pt;
         padding-left: 9%;
      }
      @media print {
         body {
            font-size: 9pt;
         }
         .footer_div {
            position: fixed;
            right: 0;
            bottom: 0;
            left: 0;
            padding: 3px;
            font-size: 8pt;
         }
      }
   </style>
</head>
<body onload = "indicateActiveModules();">
   <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
      <?php $sys->SysHdr($sys,"pis"); ?>
      <div class="container-fluid" id="mainScreen">
         <?php doTitleBar ("PAYROLL"); ?>
         <div class="container-fluid margin-top">
            <br><br>
            <div class="row">
               <div class="col-xs-2">
                  <select class="form-input" id="month_" name="month_">
                     <option value="0">Select Month</option>
                     <option value="1">January</option>
                     <option value="2">February</option>
                     <option value="3">March</option>
                     <option value="4">April</option>
                     <option value="5">May</option>
                     <option value="6">June</option>
                     <option value="7">July</option>
                     <option value="8">August</option>
                     <option value="9">September</option>
                     <option value="10">October</option>
                     <option value="11">November</option>
                     <option value="12">December</option>
                  </select>
               </div>
               <div class="col-xs-2">
                  <select class="form-input" id="year_" name="year_">
                     <option value="0">Select Year</option>
                     <?php
                        $start   = date("Y",time()) - 1;
                        $end     = date("Y",time());
                        for ($a=$start; $a <= $end ; $a++) { 
                           echo '<option value="'.$a.'">'.$a.'</option>';
                        }
                     ?>
                  </select>
               </div>
               <div class="col-xs-3">
                  <button type="submit" id="" class="btn-cls4-sea">SUBMIT</button>      
                  <button type="button" id="btnPrint" class="btn-cls4-lemon">PRINT</button>      
               </div>
            </div>
            <br><br>
            <div class="row">
               <div class="col-xs-12" id="div_CONTENT">
                  <div class="container-fluid rptBody">
                     <div class="row">
                        <div class="col-xs-12">
                           <?php rptHeader(""); ?>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-2">
                           <b>Employee Name:</b>
                        </div>
                        <div class="col-xs-4">
                           <?php echo strtoupper($FullName); ?>
                        </div>
                        <div class="col-xs-2">
                           <b>Employee No:</b>
                        </div>
                        <div class="col-xs-4">
                           <?php echo $AgencyId; ?>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-2">
                           <b>Position:</b>
                        </div>
                        <div class="col-xs-4">
                           <?php echo strtoupper($Position); ?>
                        </div>
                        <div class="col-xs-2">
                           <b>Division:</b>
                        </div>
                        <div class="col-xs-4">
                           <?php echo strtoupper($Division); ?>
                        </div>
                     </div>
                     <br><br>
                     <div class="row margin-top">
                        <div class="col-xs-12" style="padding: 10px;">
                           <div class="row" style="border:  2px solid black;">
                              <div class="col-xs-12 text-center">
                                 <b>EMPLOYEE PAYSLIP</b>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-3" style="padding: 10px;">
                           <div class="row" style="border:  2px solid black;">
                              <div class="col-xs-12 text-center">
                                 <b>BASIC SALARY</b>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       BASIC:
                                    </div>
                                    <div class="col-xs-1">:</div>
                                    <div class="col-xs-5 text-right">
                                       <?php echo number_format($total_basicpay_amount,2); ?>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       PERA:
                                    </div>
                                    <div class="col-xs-1">:</div>
                                    <div class="col-xs-5 text-right">
                                       <?php echo number_format($pera,2); ?>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       RATA:
                                    </div>
                                    <div class="col-xs-1">:</div>
                                    <div class="col-xs-5 text-right">
                                       <?php echo number_format($rata,2); ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-3" style="padding: 10px;">
                           <div class="row" style="border:  2px solid black;">
                              <div class="col-xs-12 text-center">
                                 <b>BENEFITS</b>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       SALA:
                                    </div>
                                    <div class="col-xs-1">:</div>
                                    <div class="col-xs-5 text-right">
                                       <?php echo number_format($sala,2); ?>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       Hazard Pay:
                                    </div>
                                    <div class="col-xs-1">:</div>
                                    <div class="col-xs-5 text-right">
                                       <?php echo number_format($hazard,2); ?>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       Longevity Pay:
                                    </div>
                                    <div class="col-xs-1">:</div>
                                    <div class="col-xs-5 text-right">
                                       <?php echo number_format($lp,2); ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-6" style="padding: 10px;">
                           <div class="row" style="border: 2px solid black;">
                              <div class="col-xs-12 text-center">
                                 <b>DEDUCTIONS</b>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <div class="row margin-top">
                                    <div class="col-xs-3">
                                       GSIS:
                                    </div>
                                    <div class="col-xs-1">:</div>
                                    <div class="col-xs-2 text-right">
                                       <?php echo number_format($gsis_ee_share,2); ?>
                                    </div>
                                    <div class="col-xs-3">
                                       Withholding Tax:
                                    </div>
                                    <div class="col-xs-1">:</div>
                                    <div class="col-xs-2 text-right">
                                       <?php echo number_format($tax_amount,2); ?>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-3">
                                       Pagibig:
                                    </div>
                                    <div class="col-xs-1">:</div>
                                    <div class="col-xs-2 text-right">
                                       <?php echo number_format($pagibig_share,2); ?>
                                    </div>
                                    <div class="col-xs-3">
                                       Philhealth:
                                    </div>
                                    <div class="col-xs-1">:</div>
                                    <div class="col-xs-2 text-right">
                                       <?php echo number_format($phic_share,2); ?>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-3">
                                       Absent SALA:
                                    </div>
                                    <div class="col-xs-1">:</div>
                                    <div class="col-xs-2 text-right">
                                       <?php echo number_format($absent_sala,2); ?>
                                    </div>
                                    <?php if ($pdiem > 0) { ?>
                                    <div class="col-xs-3">
                                       PDIEM:
                                    </div>
                                    <div class="col-xs-1">:</div>
                                    <div class="col-xs-2 text-right">
                                       <?php echo number_format($pdiem,2); ?>
                                    </div>
                                    <?php } ?>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-3">
                                       Undertime SALA:
                                    </div>
                                    <div class="col-xs-1">:</div>
                                    <div class="col-xs-2 text-right">
                                       <?php echo number_format($undertime_sala,2); ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-6"></div>
                        <div class="col-xs-6">
                           <div class="row">
                              <div class="col-xs-6"></div>
                              <div class="col-xs-6">
                                 <?php
                                    if ($used_amount > 0) {
                                       echo '<div class="row">';
                                       echo '<div class="col-xs-6">TA ';
                                       echo '('.$ta_covered.')';
                                       echo '</div>';
                                       echo '<div class="col-xs-1">:</div>';
                                       echo '<div class="col-xs-5 text-right">';
                                       echo number_format($used_amount,2);
                                       echo '</div>';
                                       echo '</div>';
                                    }
                                 ?>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-6"></div>
                        <div class="col-xs-6">
                           <div class="row">
                              <div class="col-xs-6"></div>
                              <div class="col-xs-6">
                                 <?php
                                    $pms_loan = pms_SelectEach("pms_loaninfo_transactions",$trn_where);
                                    if ($pms_loan) {
                                       while ($loan_row = mysqli_fetch_assoc($pms_loan)) {
                                          $loan_id = $loan_row["loan_id"];
                                          $loan_name = pms_FindFirst("pms_loans","WHERE id = '$loan_id'","name");
                                          echo '<div class="row">';
                                          echo '<div class="col-xs-6">';
                                          echo strtoupper($loan_name);
                                          echo '</div>';
                                          echo '<div class="col-xs-1">:</div>';
                                          echo '<div class="col-xs-5 text-right">';
                                          echo number_format($loan_row["amount"],2);
                                          echo '</div>';
                                          echo '</div>';
                                       }
                                    }
                                 ?>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-6"></div>
                        <div class="col-xs-6">
                           <div class="row">
                              <div class="col-xs-6"></div>
                              <div class="col-xs-6">
                                 <?php
                                    $pms_deduction = pms_SelectEach("pms_deductioninfo_transactions",$trn_where);
                                    if ($pms_deduction) {
                                       while ($deduction_row = mysqli_fetch_assoc($pms_deduction)) {
                                          $deduction_id = $deduction_row["deduction_id"];
                                          $deduction_name = pms_FindFirst("pms_deductions","WHERE id = '$deduction_id'","name");
                                          echo '<div class="row">';
                                          echo '<div class="col-xs-6">';
                                          echo strtoupper($deduction_name);
                                          echo '</div>';
                                          echo '<div class="col-xs-1">:</div>';
                                          echo '<div class="col-xs-5 text-right">';
                                          echo number_format($deduction_row["amount"],2);
                                          echo '</div>';
                                          echo '</div>';
                                       }
                                    }
                                 ?>
                              </div>
                           </div>
                        </div>
                     </div>
                     <br>
                     <br>
                     <div class="row margin-top">
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3">
                           <div class="row margin-top">
                              <div class="col-xs-6">
                                 <b>GROSS INCOME</b>
                              </div>
                              <div class="col-xs-1">:</div>
                              <div class="col-xs-5 text-right">
                                 <?php echo number_format($gross_income,2); ?>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-6">
                           <div class="row margin-top">
                              <div class="col-xs-9 text-right">
                                 <b>TOTAL DEDUCTIONS</b>
                              </div>
                              <div class="col-xs-1">:</div>
                              <div class="col-xs-2 text-right">
                                 <?php echo number_format($deduction,2); ?>
                              </div>
                           </div>
                        </div>
                     </div>
                     <br>
                     <div class="row margin-top">
                        <div class="col-xs-3">
                           <div class="row margin-top">
                              <div class="col-xs-6">
                                 <b>NET INCOME</b>
                              </div>
                              <div class="col-xs-1">:</div>
                           </div>
                        </div>
                        <div class="col-xs-3">
                           <div class="row margin-top">
                              <div class="col-xs-6">
                                 <b><?php echo $month_name." ".$y; ?></b>
                              </div>
                              <div class="col-xs-6 text-right">
                                 <?php echo number_format($net_income,2); ?>
                              </div>
                           </div>
                        </div>
                     </div>
                     <br>
                     <div class="row margin-top">
                        <div class="col-xs-3">
                           <div class="row margin-top">
                              <div class="col-xs-6">
                                 <b>NET HALF</b>
                              </div>
                              <div class="col-xs-1">:</div>
                           </div>
                        </div>
                        <div class="col-xs-3">
                           <div class="row margin-top">
                              <div class="col-xs-6">
                                 
                              </div>
                              <div class="col-xs-6 text-right">
                                 <?php echo number_format(($net_income / 2),2); ?>
                              </div>
                           </div>
                        </div>
                     </div>
                     <br>
                     <br>
                     <qoute>
                        <i>
                           This is a computer generated document and does not require any signature if without alterations
                        </i>
                     </qoute>
                  </div>
               </div>
            </div>
         </div>
         <?php
            footer();
            include "varHidden.e2e.php";
         ?>
      </div>
   </form>
</body>
</html>
