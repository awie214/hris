<?php
   error_reporting(E_ALL);
   ini_set('display_errors', 1);
   session_start();
   include_once "constant.e2e.php";
   include_once pathClass.'0620functions.e2e.php';
   include_once pathClass.'SysFunctions.e2e.php';
   include_once pathClass.'0620TrnData.e2e.php';
   $trn = new Transaction();
   $user = getvalue("hUser");


   function fndeletePDS() {
      $refid   = getvalue("refid");
      $table   = getvalue("table");
      $refid   = intval($refid);
      if ($refid > 0) {
         $result  = true;
         if ($result) {
            echo 1;
         } else {
            echo 0;
         }   
      } else {
         echo 1;
      }
   }

   function fngetEmpinfo() {
      $emprefid = getvalue("emprefid");
      $empinfo  = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
      if ($empinfo) {
         $PositionRefId = $empinfo["PositionRefId"];
         $DivisionRefId = $empinfo["DivisionRefId"];
         $DepartmentRefId = $empinfo["DepartmentRefId"];
         echo '$("#sint_PositionRefId").val("'.$PositionRefId.'");';
         echo '$("#sint_DivisionRefId").val("'.$DivisionRefId.'");';
         echo '$("#sint_DepartmentRefId").val("'.$DepartmentRefId.'");';
      }
   }

   function fngetCtrlNo() {
      $refid      = getvalue("refid");
      $code       = getRecord("absences",$refid,"Code");
      $sequence   = get_ControlNumber($code);
      
      $ctrl_no    = $code."-".date("Y",time())."".date("m",time())."-".$sequence;
      echo '$("#char_ControlNumber").val("'.$ctrl_no.'");';
   }

   function fnloadPersonalInfo() {
      $emprefid   = getvalue("emprefid");
      $row        = FindFirst("employees","WHERE RefId = '$emprefid'","*");
      if ($row) {
         $img  = $row["PicFilename"];
         if ($img == "") {
            $img_str = '<img src=\''.img('nopic.png').'\' style=\'width:150px;\'/>';
         } else {
            $img_str = '<img src=\''.img($row["CompanyRefId"].'/EmployeesPhoto/'.$img).'\' style=\'width:150px;border: 1px solid gray;\'/>';   
         }
         
         echo '$("#char_FirstName").val("'.$row["FirstName"].'");'."\n";
         echo '$("#char_LastName").val("'.$row["LastName"].'");'."\n";
         echo '$("#char_MiddleName").val("'.$row["MiddleName"].'");'."\n";
         echo '$("#char_ExtName").val("'.$row["ExtName"].'");'."\n";
         echo '$("#date_BirthDate").val("'.$row["BirthDate"].'");'."\n";
         echo '$("#char_AgencyId").val("'.$row["AgencyId"].'");'."\n";
         echo '$("#char_BiometricsID").val("'.$row["BiometricsID"].'");'."\n";
      } else {
         $img_str = '<img src=\''.img('nopic.png').'\' style=\'width:150px;\'/>';
      }
      echo '$("#img_div").html("'.$img_str.'");';
   }
   function fnuploadPic() {
      $img_loc    = getvalue("img_src");
      $img_name   = getvalue("img_name");
      $emprefid   = getvalue("emprefid");
      $img_path   = explode("EmpDocument", $img_loc)[0];
      $new_loc    = $img_path."EmployeesPhoto/".$img_name;
      if (file_exists($new_loc)) {
         unlink($new_loc);
      }
      if (copy($img_loc, $new_loc)) {
         $fldnval = "PicFilename = '$img_name', ";
         $update = f_SaveRecord("EDITSAVE","employees",$fldnval,$emprefid);
         if ($update == "") {
            echo 'alert("Successfully Set As Profile Pic");';
         } else {
            echo 'alert("Error in updating employee picture.");';
         }
      } else {
         echo 'alert("Error in updating employee picture.");';
      }
   }

   function fnremoveFile() {
      $file_src   = getvalue("file_src");
      $file_name  = explode("EmpDocument/", $file_src)[1];
      $file_path  = explode("EmpDocument/", $file_src)[0];
      $new_loc    = $file_path."Delete Files/".time()."_".$file_name;
      if (rename($file_src, $new_loc)) {
         echo 'alert("Successfully Remove");';
      }
   }
   function fngetPlantilla() {
      $refid = getvalue("refid");
      $row = FindFirst("positionitem","WHERE RefId = '$refid'","*");
      if ($row) {
         echo json_encode($row);
      } else {
         echo "No Record Found";
      }
   }

   function fncompletion() {
      $emprefid = getvalue("emprefid");
      $fldnval = "Completed = 1, ";
      $complete = f_SaveRecord("EDITSAVE","employees",$fldnval,$emprefid);
      if ($complete == "") {
         echo 1;
      } else {
         echo $complete;
      }
   }
   
   function fngetPosition() {
      $emprefid = getvalue("emprefid");
      $Position = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","PositionRefId");
      if ($Position) {
         echo '$("[name=\'sint_PositionRefId\']").val("'.$Position.'");';
      } else {
         echo '$("[name=\'sint_PositionRefId\']").val("");';
      }
   }

   function fngetSignatoryLabel() {
      $idx = getvalue("idx");
      $object     = file_get_contents(json."signatories.json");
      $obj_arr    = json_decode($object, true);
      if (isset($obj_arr[$idx])) {
         echo '$("#char_SignatoryLabel").append( $(\'<option>\',{value:0,text:\'Signatory Label\'}));';
         foreach ($obj_arr[$idx] as $akey => $avalue) {
            $arr = explode("=>", $avalue);
            echo '$("#char_SignatoryLabel").append( $(\'<option>\',{value:'.$arr[0].',text:\''.strtoupper($arr[1]).'\'}));'."\n";
         }   
      }
      
   }
   function fngetDivision() {
      $emprefid = getvalue("emprefid");
      $Division = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","DivisionRefId");
      if ($Division) {
         echo '$("[name=\'sint_DivisionRefId\']").val("'.$Division.'");';
      } else {
         echo '$("[name=\'sint_DivisionRefId\']").val("");';
      }
   }

   function fncheckAppv() {
      $table = getvalue("table");
      $refid = getvalue("refid");
      if (
         $table == "overtime_request" ||
         $table == "fl_cancellation_request" ||
         $table == "employeesauthority" ||
         $table == "employeesleave" ||
         $table == "attendance_request" ||
         $table == "employeescto" ||
         $table == "employeesleavemonetization" 
      ) {
         $check = FindFirst($table,"WHERE RefId = '$refid'","Status");
         if ($check == "Approved") {
            echo '1';
         } else {
            echo '0';
         }
      } else {
         echo '0';
      }
   }
   function fnEmployeeAutoComplete() {
      $arr = array();
      $table = getvalue("table");
      $rs = SelectEach($table,"");
      if ($rs) {
         while ($row = mysqli_fetch_assoc($rs)) {
            $FirstName = trim($row["FirstName"]);
            $LastName = trim($row["LastName"]);
            $MiddleName = trim($row["MiddleName"]);
            $MiddleName = substr($MiddleName, 0,1);
            $FullName = $FirstName." ".$MiddleName.". ".$LastName." ".$row["ExtName"];
            $FullName = htmlentities($FullName);
            $emprefid = $row["RefId"];
            $name = $emprefid."-".$FullName;
            if ($LastName != "") {
               array_push($arr, $name);   
            }
            
         }
         echo json_encode($arr);
      }
   }
   function fnupdateUsername() {
      $value = getvalue("value");
      $emprefid = getvalue("emprefid");
      $fldnval = "UserName = '$value', ";
      $result = f_SaveRecord("EDITSAVE","employees",$fldnval,$emprefid);
      if ($result == "") {
         echo "1";
      } else {
         echo "0";
      }
   }
   function fngetEmpID(){
      $empid = get_empid();
      echo '$("#char_NewEmployeesId").val("'.$empid.'");';
   }
   function fnsaveServiceRecord() {
      $object     = file_get_contents(json."object.json");
      $obj_arr    = json_decode($object, true);
      $table      = "employeesmovement";
      $end_date   = getvalue("date_ExpiryDate");
      $present    = getvalue("sint_Present");
      $new_empid  = getvalue("char_NewEmployeesId");
      $emprefid   = getvalue("sint_EmployeesRefId");
      $agencyid   = FindFirst("employees","WHERE RefId = '$emprefid'","AgencyId");
      $old_salary = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","SalaryAmount");
      $refid      = getvalue("hRefId");
      $apptstatus = getvalue("sint_ApptStatusRefId");
      $Fields     = "";
      $Values     = "";
      $FldnVal    = "";
      $pms_fld    = "";
      $pms_val    = "";
      $present    = intval($present);
      $start_date = getvalue("date_EffectivityDate");
      $empinfo_fldnval = "";

      $apptstatus = intval($apptstatus);
      

      foreach ($obj_arr["employeesmovement"] as $key => $nvalue) {
         if (isset($_POST[$nvalue[0]])) {
            $obj_value = realEscape(getvalue($nvalue[0]));
            if ($nvalue[2] == "int") {
               $obj_value  = intval($obj_value);
               $Fields .= "`".$nvalue[1]."`,";
               $Values .= "'".$obj_value."',";
               $FldnVal .= "`".$nvalue[1]."` = '".$obj_value."',";
            } else if ($nvalue[2] == "deci") {
               if (stripos($obj_value,",") > 0) {
                  $obj_value = str_replace(",", "", $obj_value);
               }
               $obj_value  = intval($obj_value);
               if ($obj_value > 0) {
                  $Fields .= "`".$nvalue[1]."`,";
                  $Values .= "'".$obj_value."',";
                  $FldnVal .= "`".$nvalue[1]."` = '".$obj_value."',";   
               }
               
            } else {
               if ($obj_value != "") {
                  $Fields .= "`".$nvalue[1]."`,";
                  $Values .= "'".$obj_value."',";
                  $FldnVal .= "`".$nvalue[1]."` = '".$obj_value."',";  
               }
            }
         }
      }
      foreach ($obj_arr["employeesmovement_empinfo"] as $akey => $avalue) {
         if (isset($_POST[$avalue[0]])) {
            $obj_value = realEscape(getvalue($avalue[0]));
            if ($avalue[2] == "int") {
               $obj_value  = intval($obj_value);
               $empinfo_fldnval .= "`".$avalue[1]."` = '".$obj_value."',";
            } else if ($avalue[2] == "deci") {
               $obj_value = str_replace(",", "", $obj_value);
               $obj_value = intval($obj_value);
               $empinfo_fldnval .= "`".$avalue[1]."` = '".$obj_value."',";           
            } else {
               if ($obj_value != "") {
                  $empinfo_fldnval .= "`".$avalue[1]."` = '".$obj_value."',";  
               }
            }
         }
      }
      if ($refid > 0) {
         if ($end_date != "PRESENT") {
            $FldnVal .= "ExpiryDate = '$end_date',";
         }
         $update_service_record = f_SaveRecord("EDITSAVE",$table,$FldnVal,$refid);
         if ($update_service_record == "") {
            if (isset($_POST["sint_Present"])) {
               $update_Expiry = updateDate("employeesmovement","ExpiryDate","",$refid);
               if ($update_Expiry != "") {
                  echo "Error in updating End Date ".$update_Expiry;
                  return false;
               }
            }
            echo "Update Success";
         } else {
            echo "Error in Updating in Service Record";
         }
      } else {
         if ($end_date != "PRESENT" && $present == 0) {
            $Fields .= "`ExpiryDate`, ";
            $Values .= "'$end_date',";
            $save_service_record = f_SaveRecord("NEWSAVE",$table,$Fields,$Values);
            if (is_numeric($save_service_record)) {
               echo "Save Success";
            } else {
               echo "Error in Saving in Service Record";
            }
         } else {
            if (isset($_POST["deci_SalaryAmount"])) {
               $new_salary = getvalue("deci_SalaryAmount");
               if (stripos($new_salary,",") > 0) {
                  $new_salary = str_replace(",", "", $new_salary);
                  $new_salary = intval($new_salary);
               }             
               $salary_diff = $new_salary - $old_salary;
            }
            $empinfo_refid = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","RefId");
            if ($empinfo_refid) {
               $update_empinfo = f_SaveRecord("EDITSAVE","empinformation",$empinfo_fldnval,$empinfo_refid);
               if ($update_empinfo == "") {
                  $save_service_record = f_SaveRecord("NEWSAVE",$table,$Fields,$Values);
                  if (is_numeric($save_service_record)) {
                     $update_Expiry = updateDate("employeesmovement","ExpiryDate","",$save_service_record);
                     if ($update_Expiry != "") {
                        echo "Error in updating ExpiryDate.";
                     }
                     if (intval($salary_diff) > 0) {
                        $sr_row = FindFirst("employeesmovement","WHERE RefId = '$save_service_record'","*");
                        if ($sr_row) {
                           foreach ($obj_arr["pms_salaryinfo"] as $bkey => $bvalue) {
                              if (isset($sr_row[$bvalue[0]])) {
                                 $pms_fld .= $bvalue[1].", ";
                                 $pms_val .= "'".$sr_row[$bvalue[0]]."',";                                 
                              }
                           }
                           $check_pms = pms_FindFirst("pms_employees","WHERE employee_number = '$agencyid'","id");
                           if ($check_pms) {
                              $pms_fld .= "salary_old_rate, salary_adjustment, salary_new_rate, employee_id,salary_effectivity_date,";
                              $pms_val .= "'$old_salary', '$salary_diff', '$new_salary', '$check_pms','$start_date',";   
                              /*if ($apptstatus > 0) {
                                 $check_apptstatus = FindFirst("ApptStatus","WHERE RefId = '$apptstatus'","Code");
                                 if ($check_apptstatus == "SI" || $check_apptstatus == "SA" || $check_apptstatus == "LP") {
                                    $pms_fld .= "status, ";
                                    $pms_val .= "1, ";
                                 }
                              }*/
                              $save_pms_salary = savePMS("pms_salaryinfo",$pms_fld,$pms_val);
                              if (is_numeric($save_pms_salary)) {
                                 echo "Save Success";      
                              } else {
                                 echo "Error in Saving in PMS Salary Info";
                              }
                           } else {
                              echo "No Employee Number Match in Payroll";
                           }
                        }
                     } else {
                        echo "Save Success";
                     }
                     
                  } else {
                     echo "Error in Saving in Service Record";
                  }
               }
            }
         }
      }
   }
   function fnretrieveUsername() {
      $emprefid = getvalue("emprefid");
      $UserName = FindFirst("employees","WHERE RefId = '$emprefid'","UserName");
      echo '$("#UName").val("'.$UserName.'");';
   }
   function fngetSalaryAmount() {
      $salary_two = 0;
      $sg_val     = intval(getvalue("sg"));
      $jg_val     = intval(getvalue("jg"));
      $si_val     = intval(getvalue("si"));
      $cid        = getvalue("cid");
      $si         = getRecord("stepincrement",getvalue("si"),"Name");
      $sg         = getRecord("salarygrade",getvalue("sg"),"Name");
      $js         = getRecord("jobgrade",getvalue("jg"),"Name");
      $file       = file_get_contents(json."second_tranche.json");
      $arr_object = json_decode($file, true);


      if ($cid == 21) $salary_two = $arr_object[$sg][$si];
      if ($si > 0) {
         $Fld = "Step".$si;
         if ($sg_val > 0) {
            $table = "salarygrade";
            $refid = $sg_val;
         } else if ($jg_val > 0) {
            $table = "jobgrade";
            $refid = $jg_val;
         } else {
            $table = "";
            $refid = "";
         }
         if ($table != "") {
            $salary = FindFirst($table,"WHERE RefId = '$refid'",$Fld);
            if ($salary) {
               if ($cid == "21") {
                  echo '$("[name=\'deci_SalaryAmount\']").val("'.number_format($salary_two,2).'");'."\n";
                  echo '$("[name=\'deci_SalaryAmountTwo\']").val("'.number_format(($salary - $salary_two),2).'");'."\n";   
               } else {
                  echo '$("[name=\'deci_SalaryAmount\']").val("'.number_format($salary,2).'");'."\n";   
               }
            }
         }
      }
   }
   function fnupdateEmpInfo() {
      $refid      = getvalue("hRefId");
      $emprefid   = getvalue("hEmpRefid");
      $table      = "empinformation";
      $fldnval    = "";
      if ($refid > 0) {
         if (isset($_POST["date_HiredDate"])) {
            $HiredDate = realEscape(getvalue("date_HiredDate"));
            //$fldnval .= "HiredDate = '$HiredDate',";   
            updateDate($table,"HiredDate",$HiredDate,$refid);
         }
         if (isset($_POST["char_TransferTo"])) {
            $TransferTo = realEscape(getvalue("char_TransferTo"));
            $fldnval .= "TransferTo = '$TransferTo',";   
         }
         if (isset($_POST["date_AssumptionDate"])) {
            $AssumptionDate = realEscape(getvalue("date_AssumptionDate"));
            //$fldnval .= "AssumptionDate = '$AssumptionDate',";
            updateDate($table,"AssumptionDate",$AssumptionDate,$refid);
         }
         if (isset($_POST["date_ResignedDate"])) {
            $ResignedDate = realEscape(getvalue("date_ResignedDate"));
            //$fldnval .= "ResignedDate = '$ResignedDate',";   
            updateDate($table,"ResignedDate",$ResignedDate,$refid);
         }
         if (isset($_POST["date_StartDate"])) {
            $StartDate = realEscape(getvalue("date_StartDate"));
            //$fldnval .= "StartDate = '$StartDate',";   
            updateDate($table,"StartDate",$StartDate,$refid);
         }
         if (isset($_POST["date_EndDate"])) {
            $EndDate = realEscape(getvalue("date_EndDate"));
            //$fldnval .= "EndDate = '$EndDate',";   
            updateDate($table,"EndDate",$EndDate,$refid);
         }
         if (isset($_POST["sint_AgencyRefId"])) {
            $AgencyRefId = realEscape(getvalue("sint_AgencyRefId"));
            $AgencyRefId = intval($AgencyRefId);
            $fldnval .= "AgencyRefId = '$AgencyRefId',";   
         }
         if (isset($_POST["sint_PositionItemRefId"])) {
            $PositionItemRefId = realEscape(getvalue("sint_PositionItemRefId"));
            $PositionItemRefId = intval($PositionItemRefId);
            $fldnval .= "PositionItemRefId = '$PositionItemRefId',";   
         }
         if (isset($_POST["sint_PositionRefId"])) {
            $PositionRefId = realEscape(getvalue("sint_PositionRefId"));
            $PositionRefId = intval($PositionRefId);
            $fldnval .= "PositionRefId = '$PositionRefId',";   
         }
         if (isset($_POST["sint_ProjectRefId"])) {
            $ProjectRefId = realEscape(getvalue("sint_ProjectRefId"));
            $ProjectRefId = intval($ProjectRefId);
            $fldnval .= "ProjectRefId = '$ProjectRefId',";   
         }
         if (isset($_POST["sint_OfficeRefId"])) {
            $OfficeRefId = realEscape(getvalue("sint_OfficeRefId"));
            $OfficeRefId = intval($OfficeRefId);
            $fldnval .= "OfficeRefId = '$OfficeRefId',";   
         }
         if (isset($_POST["sint_ResponsibilityCenterRefId"])) {
            $ResponsibilityCenterRefId = realEscape(getvalue("sint_ResponsibilityCenterRefId"));
            $ResponsibilityCenterRefId = intval($ResponsibilityCenterRefId);
            $fldnval .= "ResponsibilityCenterRefId = '$ResponsibilityCenterRefId',";   
         }
         if (isset($_POST["sint_DivisionRefId"])) {
            $DivisionRefId = realEscape(getvalue("sint_DivisionRefId"));
            $DivisionRefId = intval($DivisionRefId);
            $fldnval .= "DivisionRefId = '$DivisionRefId',";   
         }
         if (isset($_POST["sint_DepartmentRefId"])) {
            $DepartmentRefId = realEscape(getvalue("sint_DepartmentRefId"));
            $DepartmentRefId = intval($DepartmentRefId);
            $fldnval .= "DepartmentRefId = '$DepartmentRefId',";   
         }
         if (isset($_POST["sint_EmpStatusRefId"])) {
            $EmpStatus = realEscape(getvalue("sint_EmpStatusRefId"));
            $EmpStatus = intval($EmpStatus);
            $fldnval .= "EmpStatusRefId = '$EmpStatus',";   
         }
         if (isset($_POST["sint_ApptStatusRefId"])) {
            $ApptStatus = realEscape(getvalue("sint_ApptStatusRefId"));
            $ApptStatus = intval($ApptStatus);
            $fldnval .= "ApptStatusRefId = '$ApptStatus',";   
         }
         if (isset($_POST["sint_SalaryGradeRefId"])) {
            $SalaryGradeRefId = realEscape(getvalue("sint_SalaryGradeRefId"));
            $SalaryGradeRefId = intval($SalaryGradeRefId);
            $fldnval .= "SalaryGradeRefId = '$SalaryGradeRefId',";   
         }
         if (isset($_POST["sint_JobGradeRefId"])) {
            $JobGradeRefId = realEscape(getvalue("sint_JobGradeRefId"));
            $JobGradeRefId = intval($JobGradeRefId);
            $fldnval .= "JobGradeRefId = '$JobGradeRefId',";   
         }
         if (isset($_POST["sint_StepIncrementRefId"])) {
            $StepIncrementRefId = realEscape(getvalue("sint_StepIncrementRefId"));
            $StepIncrementRefId = intval($StepIncrementRefId);
            $fldnval .= "StepIncrementRefId = '$StepIncrementRefId',";   
         }
         if (isset($_POST["sint_UnitsRefId"])) {
            $UnitsRefId = realEscape(getvalue("sint_UnitsRefId"));
            $UnitsRefId = intval($UnitsRefId);
            $fldnval .= "UnitsRefId = '$UnitsRefId',";   
         }
         if (isset($_POST["sint_LocationsRefId"])) {
            $LocationsRefId = realEscape(getvalue("sint_LocationsRefId"));
            $LocationsRefId = intval($LocationsRefId);
            $fldnval .= "LocationsRefId = '$LocationsRefId',";   
         }
         if (isset($_POST["sint_SectionsRefId"])) {
            $SectionsRefId = realEscape(getvalue("sint_SectionsRefId"));
            $SectionsRefId = intval($SectionsRefId);
            $fldnval .= "SectionsRefId = '$SectionsRefId',";   
         }
         if (isset($_POST["InterimPositionRefId"])) {
            $InterimPositionRefId = realEscape(getvalue("InterimPositionRefId"));
            $InterimPositionRefId = intval($InterimPositionRefId);
            $fldnval .= "InterimPositionRefId = '$InterimPositionRefId',";   
         }
         if (isset($_POST["InterimOfficeRefId"])) {
            $InterimOfficeRefId = realEscape(getvalue("InterimOfficeRefId"));
            $InterimOfficeRefId = intval($InterimOfficeRefId);
            $fldnval .= "InterimOfficeRefId = '$InterimOfficeRefId',";   
         }
         if (isset($_POST["InterimDivisionRefId"])) {
            $InterimDivisionRefId = realEscape(getvalue("InterimDivisionRefId"));
            $InterimDivisionRefId = intval($InterimDivisionRefId);
            $fldnval .= "InterimDivisionRefId = '$InterimDivisionRefId',";   
         }
         if (isset($_POST["sint_IsHead"])) {
            $IsHead = realEscape(getvalue("sint_IsHead"));
            $IsHead = intval($IsHead);
            $fldnval .= "IsHead = '$IsHead',";   
         }
         if (isset($_POST["deci_SalaryAmount"])) {
            $SalaryAmount = realEscape(getvalue("deci_SalaryAmount"));
            if (stripos($SalaryAmount,",") > 0) {
               $SalaryAmount = str_replace(",", "", $SalaryAmount);
            }
            $fldnval .= "SalaryAmount = '".floatval($SalaryAmount)."',";   
         }
         if (isset($_POST["deci_SalaryAmountTwo"])) {
            $SalaryAmountTwo = realEscape(getvalue("deci_SalaryAmountTwo"));
            if (stripos($SalaryAmountTwo,",") > 0) {
               $SalaryAmountTwo = str_replace(",", "", $SalaryAmountTwo);
            }
            $fldnval .= "SalaryAmountTwo = '".floatval($SalaryAmountTwo)."',";
         }
         /*echo $fldnval;
         return false;*/
         $update_empinfo = f_SaveRecord("EDITSAVE",$table,$fldnval,$refid);
         if ($update_empinfo == "") {
            $update_pms_empinfo = sync_pms($emprefid,"2"); 
            if ($update_pms_empinfo == "") {
               $update_pms_salaryinfo = sync_pms($emprefid,"3");  
               if ($update_pms_salaryinfo == "") {
                  echo 'afterEditSave('.$emprefid.');';
               } else {
                  echo '$.notify("Error In Updating Salary Info");';
               }
            } else {
               echo '$.notify("Error In Updating PMS Employee Info -> '.$update_pms_empinfo.'");';
            }
         } else {
            echo '$.notify("Error In Updating Employee Info");';
         }   
      } else {
         $Fld = "EmployeesRefId,";
         $Val = "'$emprefid',";
         if (isset($_POST["date_HiredDate"])) {
            $HiredDate = realEscape(getvalue("date_HiredDate"));
            if ($HiredDate != "") {
               $Fld .= 'HiredDate, ';
               $Val .= "'$HiredDate',";
            }
         }
         if (isset($_POST["char_TransferTo"])) {
            $TransferTo = realEscape(getvalue("char_TransferTo"));
            if ($TransferTo != "") {
               $Fld .= 'TransferTo, ';
               $Val .= "'$TransferTo',";
            }
         }
         if (isset($_POST["date_AssumptionDate"])) {
            $AssumptionDate = realEscape(getvalue("date_AssumptionDate"));
            if ($AssumptionDate != "") {
               $Fld .= 'AssumptionDate, ';
               $Val .= "'$AssumptionDate',";
            }
         }
         if (isset($_POST["date_ResignedDate"])) {
            $ResignedDate = realEscape(getvalue("date_ResignedDate"));
            if ($ResignedDate != "") {
               $Fld .= 'ResignedDate, ';
               $Val .= "'$ResignedDate',";
            }
         }
         if (isset($_POST["date_StartDate"])) {
            $StartDate = realEscape(getvalue("date_StartDate"));
            if ($StartDate != "") {
               $Fld .= 'StartDate, ';
               $Val .= "'$StartDate',";
            }
         }
         if (isset($_POST["date_EndDate"])) {
            $EndDate = realEscape(getvalue("date_EndDate"));
            if ($EndDate != "") {
               $Fld .= 'EndDate, ';
               $Val .= "'$EndDate',";
            }
         }
         if (isset($_POST["sint_AgencyRefId"])) {
            $AgencyRefId = realEscape(getvalue("sint_AgencyRefId"));
            $AgencyRefId = intval($AgencyRefId);
            $Fld .= 'AgencyRefId, ';
            $Val .= "'$AgencyRefId',";
         }
         if (isset($_POST["sint_PositionItemRefId"])) {
            $PositionItemRefId = realEscape(getvalue("sint_PositionItemRefId"));
            $PositionItemRefId = intval($PositionItemRefId);
            $Fld .= 'PositionItemRefId, ';
            $Val .= "'$PositionItemRefId',";
         }
         if (isset($_POST["sint_PositionRefId"])) {
            $PositionRefId = realEscape(getvalue("sint_PositionRefId"));
            $PositionRefId = intval($PositionRefId);
            $Fld .= 'PositionRefId, ';
            $Val .= "'$PositionRefId',";
         }
         if (isset($_POST["sint_OfficeRefId"])) {
            $OfficeRefId = realEscape(getvalue("sint_OfficeRefId"));
            $OfficeRefId = intval($OfficeRefId);
            $Fld .= 'OfficeRefId, ';
            $Val .= "'$OfficeRefId',";
         }
         if (isset($_POST["sint_ResponsibilityCenterRefId"])) {
            $ResponsibilityCenterRefId = realEscape(getvalue("sint_ResponsibilityCenterRefId"));
            $ResponsibilityCenterRefId = intval($ResponsibilityCenterRefId);
            $Fld .= 'ResponsibilityCenterRefId, ';
            $Val .= "'$ResponsibilityCenterRefId',";
         }
         if (isset($_POST["sint_DivisionRefId"])) {
            $DivisionRefId = realEscape(getvalue("sint_DivisionRefId"));
            $DivisionRefId = intval($DivisionRefId);
            $Fld .= 'DivisionRefId, ';
            $Val .= "'$DivisionRefId',";
         }
         if (isset($_POST["sint_DepartmentRefId"])) {
            $DepartmentRefId = realEscape(getvalue("sint_DepartmentRefId"));
            $DepartmentRefId = intval($DepartmentRefId);
            $Fld .= 'DepartmentRefId, ';
            $Val .= "'$DepartmentRefId',";
         }
         if (isset($_POST["sint_EmpStatusRefId"])) {
            $EmpStatus = realEscape(getvalue("sint_EmpStatusRefId"));
            $EmpStatus = intval($EmpStatus);
            $Fld .= 'EmpStatusRefId, ';
            $Val .= "'$EmpStatus',";
         }
         if (isset($_POST["sint_ApptStatusRefId"])) {
            $ApptStatus = realEscape(getvalue("sint_ApptStatusRefId"));
            $ApptStatus = intval($ApptStatus);
            $Fld .= 'ApptStatusRefId, ';
            $Val .= "'$ApptStatus',";
         }
         if (isset($_POST["sint_SalaryGradeRefId"])) {
            $SalaryGradeRefId = realEscape(getvalue("sint_SalaryGradeRefId"));
            $SalaryGradeRefId = intval($SalaryGradeRefId);
            $Fld .= 'SalaryGradeRefId, ';
            $Val .= "'$SalaryGradeRefId',";
         }
         if (isset($_POST["sint_JobGradeRefId"])) {
            $JobGradeRefId = realEscape(getvalue("sint_JobGradeRefId"));
            $JobGradeRefId = intval($JobGradeRefId);
            $Fld .= 'JobGradeRefId, ';
            $Val .= "'$JobGradeRefId',";
         }
         if (isset($_POST["sint_StepIncrementRefId"])) {
            $StepIncrementRefId = realEscape(getvalue("sint_StepIncrementRefId"));
            $StepIncrementRefId = intval($StepIncrementRefId);
            $Fld .= 'StepIncrementRefId, ';
            $Val .= "'$StepIncrementRefId',";
         }
         if (isset($_POST["sint_UnitsRefId"])) {
            $UnitsRefId = realEscape(getvalue("sint_UnitsRefId"));
            $UnitsRefId = intval($UnitsRefId);
            $Fld .= 'UnitsRefId, ';
            $Val .= "'$UnitsRefId',";
         }
         if (isset($_POST["sint_LocationsRefId"])) {
            $LocationsRefId = realEscape(getvalue("sint_LocationsRefId"));
            $LocationsRefId = intval($LocationsRefId);
            $Fld .= 'LocationsRefId, ';
            $Val .= "'$LocationsRefId',";
         }
         if (isset($_POST["sint_SectionsRefId"])) {
            $SectionsRefId = realEscape(getvalue("sint_SectionsRefId"));
            $SectionsRefId = intval($SectionsRefId);
            $Fld .= 'SectionsRefId, ';
            $Val .= "'$SectionsRefId',";
         }
         if (isset($_POST["InterimPositionRefId"])) {
            $InterimPositionRefId = realEscape(getvalue("InterimPositionRefId"));
            $InterimPositionRefId = intval($InterimPositionRefId);
            $Fld .= 'InterimPositionRefId, ';
            $Val .= "'$InterimPositionRefId',";
         }
         if (isset($_POST["InterimOfficeRefId"])) {
            $InterimOfficeRefId = realEscape(getvalue("InterimOfficeRefId"));
            $InterimOfficeRefId = intval($InterimOfficeRefId);
            $Fld .= 'InterimOfficeRefId, ';
            $Val .= "'$InterimOfficeRefId',";
         }
         if (isset($_POST["InterimDivisionRefId"])) {
            $InterimDivisionRefId = realEscape(getvalue("InterimDivisionRefId"));
            $InterimDivisionRefId = intval($InterimDivisionRefId);
            $Fld .= 'InterimDivisionRefId, ';
            $Val .= "'$InterimDivisionRefId',";
         }
         if (isset($_POST["deci_SalaryAmount"])) {
            $SalaryAmount = realEscape(getvalue("deci_SalaryAmount"));
            if (stripos($SalaryAmount,",")) {
               $SalaryAmount = str_replace(",", "", $SalaryAmount);
            }
            $Fld .= 'SalaryAmount, ';
            $Val .= "'$SalaryAmount',";
         }
         if (isset($_POST["deci_SalaryAmountTwo"])) {
            $SalaryAmountTwo = realEscape(getvalue("deci_SalaryAmountTwo"));
            if (stripos($SalaryAmountTwo,",")) {
               $SalaryAmountTwo = str_replace(",", "", $SalaryAmountTwo);
            }
            $Fld .= 'SalaryAmountTwo, ';
            $Val .= "'$SalaryAmountTwo',";
         }
         $save_empinfo = f_SaveRecord("NEWSAVE",$table,$Fld,$Val);
         if (is_numeric($save_empinfo)) {
            $update_pms_empinfo = sync_pms($emprefid,"2"); 
            if ($update_pms_empinfo == "") {
               $update_pms_salaryinfo = sync_pms($emprefid,"3");  
               if ($update_pms_salaryinfo == "") {
                  echo 'afterEditSave('.$emprefid.');';
               } else {
                  echo '$.notify("Error In Updating Salary Info");';
               }
            } else {
               echo '$.notify("Error In Updating PMS Employee Info -> '.$update_pms_empinfo.'");';
            }
         } else {
            echo '$.notify("Error In Saving Employee Info");';
         }
      }
      
   }

   function fnSaveNewEmp() {
      $user = getvalue("hUser");
      $employee_Fld        = "`isFilipino`, ";
      $employee_Val        = "'1', ";
      $empinfo_Fld         = "";
      $empinfo_Val         = "";
      $pms_employee_fld    = "";
      $pms_employee_val    = "";
      $pms_empinfo_fld     = "";
      $pms_empinfo_val     = "";
      $pms_salaryinfo_fld  = "";
      $pms_salaryinfo_val  = "";
      if (isset($_POST["LastName"])) {
         $LastName = realEscape(getvalue("LastName"));
         $employee_Fld .= "`LastName`, ";
         $employee_Val .= "'$LastName', ";
         $pms_employee_fld .= "`lastname`,";
         $pms_employee_val .= "'$LastName',";
      }
      if (isset($_POST["FirstName"])) {
         $FirstName = realEscape(getvalue("FirstName"));
         $employee_Fld .= "`FirstName`, ";
         $employee_Val .= "'$FirstName', ";
         $pms_employee_fld .= "`firstname`,";
         $pms_employee_val .= "'$FirstName',";
      }
      if (isset($_POST["MiddleName"])) {
         $MiddleName = realEscape(getvalue("MiddleName"));
         $employee_Fld .= "`MiddleName`, ";
         $employee_Val .= "'$MiddleName', ";
         $pms_employee_fld .= "`middlename`,";
         $pms_employee_val .= "'$MiddleName',";
      }
      if (isset($_POST["ExtName"])) {
         $ExtName = realEscape(getvalue("ExtName"));
         $employee_Fld .= "`ExtName`, ";
         $employee_Val .= "'$ExtName', ";
         $pms_employee_fld .= "`extension_name`,";
         $pms_employee_val .= "'$ExtName',";
      }
      if (isset($_POST["BirthDate"])) {
         $BirthDate = realEscape(getvalue("BirthDate"));
         if ($BirthDate != "") {
            $employee_Fld .= "`BirthDate`, ";
            $employee_Val .= "'$BirthDate', ";
         }
      }
      if (isset($_POST["AgencyId"])) {
         $AgencyId = realEscape(getvalue("AgencyId"));
         if ($AgencyId != "") {
            $employee_Fld .= "`AgencyId`, ";
            $employee_Val .= "'$AgencyId', ";
            $pms_employee_fld .= "`employee_number`,";
            $pms_employee_val .= "'$AgencyId',";
         }
      }
      if (isset($_POST["UserName"])) {
         $UserName = realEscape(getvalue("UserName"));
         if ($UserName != "") {
            $employee_Fld .= "`UserName`, ";
            $employee_Val .= "'$UserName', ";
         }
      }
      if (isset($_POST["hPass"])) {
         $hPass = realEscape(getvalue("hPass"));
         if ($hPass != "") {
            $employee_Fld .= "`pw`, ";
            $employee_Val .= "'$hPass', ";
         }
      }
      if (isset($_POST["PositionItemRefId"])) {
         $PositionItemRefId = intval(realEscape(getvalue("PositionItemRefId")));
         if ($PositionItemRefId != "0") {
            $empinfo_Fld .= "`PositionItemRefId`, ";
            $empinfo_Val .= "'$PositionItemRefId', ";
            $pms_empinfo_fld .= "`position_item_id`, ";
            $pms_empinfo_val .= "'$PositionItemRefId',";
         }
      }
      if (isset($_POST["PositionRefId"])) {
         $PositionRefId = intval(realEscape(getvalue("PositionRefId")));
         if ($PositionRefId != "0") {
            $empinfo_Fld .= "`PositionRefId`, ";
            $empinfo_Val .= "'$PositionRefId', ";
            $pms_empinfo_fld .= "`position_id`, ";
            $pms_empinfo_val .= "'$PositionRefId',";
         }
      }
      if (isset($_POST["OfficeRefId"])) {
         $OfficeRefId = intval(realEscape(getvalue("OfficeRefId")));
         if ($OfficeRefId != "0") {
            $empinfo_Fld .= "`OfficeRefId`, ";
            $empinfo_Val .= "'$OfficeRefId', ";
         }
      }
      if (isset($_POST["DivisionRefId"])) {
         $DivisionRefId = intval(realEscape(getvalue("DivisionRefId")));
         if ($DivisionRefId != "0") {
            $empinfo_Fld .= "`DivisionRefId`, ";
            $empinfo_Val .= "'$DivisionRefId', ";
            $pms_empinfo_fld .= "`division_id`, ";
            $pms_empinfo_val .= "'$DivisionRefId',";
         }
      }
      if (isset($_POST["InterimPositionRefId"])) {
         $InterimPositionRefId = intval(realEscape(getvalue("InterimPositionRefId")));
         if ($InterimPositionRefId != "0") {
            $empinfo_Fld .= "`InterimPositionRefId`, ";
            $empinfo_Val .= "'$InterimPositionRefId', ";
         }
      }
      if (isset($_POST["InterimOfficeRefId"])) {
         $InterimOfficeRefId = intval(realEscape(getvalue("InterimOfficeRefId")));
         if ($InterimOfficeRefId != "0") {
            $empinfo_Fld .= "`InterimOfficeRefId`, ";
            $empinfo_Val .= "'$InterimOfficeRefId', ";
         }
      }
      if (isset($_POST["InterimDivisionRefId"])) {
         $InterimDivisionRefId = intval(realEscape(getvalue("InterimDivisionRefId")));
         if ($InterimDivisionRefId != "0") {
            $empinfo_Fld .= "`InterimDivisionRefId`, ";
            $empinfo_Val .= "'$InterimDivisionRefId', ";
         }
      }
      if (isset($_POST["EmpStatusRefId"])) {
         $EmpStatusRefId = intval(realEscape(getvalue("EmpStatusRefId")));
         if ($EmpStatusRefId != "0") {
            $empinfo_Fld .= "`EmpStatusRefId`, ";
            $empinfo_Val .= "'$EmpStatusRefId', ";
            $pms_empinfo_fld .= "`employee_status_id`, ";
            $pms_empinfo_val .= "'$EmpStatusRefId',";
         }
      }
      //var_dump($_POST);
      //echo $employee_Fld."<br>".$employee_Val;
      //return false;
      $where = "WHERE LastName = '$LastName' AND FirstName = '$FirstName' AND BirthDate = '$BirthDate'";
      $check_employee = FindFirst("employees",$where,"RefId");
      if (is_numeric($check_employee)) {
         echo '
            $.notify("Employee Exist!!!");
         ';
      } else {
         $save_employee = f_SaveRecord("NEWSAVE","employees",$employee_Fld,$employee_Val,$user);
         if (is_numeric($save_employee)) {
            $EmployeesRefId = $save_employee;
            $empinfo_Fld .= "`EmployeesRefId`, ";
            $empinfo_Val .= "'$EmployeesRefId', ";
            $save_empinfo = f_SaveRecord("NEWSAVE","empinformation",$empinfo_Fld,$empinfo_Val,$user);
            if (is_numeric($save_empinfo)) {
               $employee_info_Fld = "EmployeesRefId, ";
               $employee_info_Val = "'$EmployeesRefId',"; 
               $save_emp_fam = f_SaveRecord("NEWSAVE","employeesfamily",$employee_info_Fld,$employee_info_Val,$user);
               $save_emp_pdsq = f_SaveRecord("NEWSAVE","employeespdsq",$employee_info_Fld,$employee_info_Val,$user);
               if (is_numeric($save_emp_fam) && is_numeric($save_emp_pdsq)) {
                  $emp_row = FindFirst("employees","WHERE RefId = '$save_employee'","`CompanyRefId`,`BranchRefId`");
                  $company = $emp_row["CompanyRefId"];
                  $branch  = $emp_row["BranchRefId"];
                  $pms_employee_fld .= "`company_id`, `branch_id`, `active`, ";
                  $pms_employee_val .= "'$company', '$branch', '1',";
                  $save_pms_employees = savePMS("pms_employees",$pms_employee_fld,$pms_employee_val);
                  if (is_numeric($save_pms_employees)) {
                     $pms_empinfo_fld .= "`employee_id`,";
                     $pms_empinfo_val .= "'$save_pms_employees',";
                     $save_pms_employees = savePMS("pms_employee_information",$pms_empinfo_fld,$pms_empinfo_val);
                     if (is_numeric($save_pms_employees)) {
                        echo '
                           alert("New Employees Successfully Inserted\nYou may continue encoding of the other information after this transaction.");
                           gotoscrn("scrn201File","");
                        ';
                     } else {
                        echo '$.notify("Error In Saving PMS Employees\'s Information!!!");';   
                     }
                  } else {
                     echo '$.notify("Error In Saving in PMS Employees\'s Personal Info!!!");';   
                  }
               } else {
                  echo '$.notify("Error In Saving Employees\'s Other Info!!!");';   
               }
            } else {
               echo '$.notify("Error In Saving In Employee Information Table!!!");';
            }
         } else {
            echo '$.notify("Error In Saving In Employee Table!!!");';
         }   
      }
      

   }
   function fnSaveNewEmployeesInit() {
      include "conn.e2e.php";
      $data = json_decode(getvalue("data"), true);
      $dbField = json_decode(getvalue("field"), true); 
      $user = getvalue("user");
      $Fields = "";
      $Values = "";
      $empinfo_Fields = "";
      $empinfo_Values = "";
      foreach ($dbField as $key => $value) {
         $FieldValue = realEscape($data[$value]);
         if (
               $value == "PositionRefId" || 
               $value == "PositionItemRefId" || 
               $value == "InterimOfficeRefId" || 
               $value == "InterimDivisionRefId" || 
               $value == "EmpStatusRefId"
            ) {
            $empinfo_Fields .= $value.", ";
            $empinfo_Values .= "'{$FieldValue}', ";   
         } else {
            $Fields .= $value.", ";
            $Values .= "'{$FieldValue}', ";   
         }
         
      }
      //echo $Fields."\n";
      //echo $Values."\n";
      $SaveSuccessfull = f_SaveRecord("NEWSAVE","employees",$Fields,$Values,$user);
      if (!is_numeric($SaveSuccessfull)) {
         echo '
            $.notify("Saving Error!!!");
            return false;
         ';
      } else {
         $empinfo_Fields .= "`EmployeesRefId`, ";
         $empinfo_Values .= "$SaveSuccessfull, ";
         $empinfo_SaveSuccessfull = f_SaveRecord("NEWSAVE","empinformation",$empinfo_Fields,$empinfo_Values,$user);
         if (!is_numeric($empinfo_SaveSuccessfull)) {
           echo '
               $.notify("Saving Error!!!");
               return false;
            '; 
         } else {
            $new_fld = "EmployeesRefId, ";
            $new_val = "'".$SaveSuccessfull."',";
            $save_emp_fambg = f_SaveRecord("NEWSAVE","employeesfamily",$new_fld,$new_val,$user);
            if (is_numeric($save_emp_fambg)) {
               $save_emp_pdsq = f_SaveRecord("NEWSAVE","employeespdsq",$new_fld,$new_val,$user);
               if (is_numeric($save_emp_pdsq)) {
                  echo '
                     alert("New Employees Successfully Inserted\nYou may continue encoding of the other information after this transaction.");
                     gotoscrn("scrn201File","");
                  ';
               } else {
                  echo '
                     $.notify("Saving Error in Employees PDSQ!!!");
                     return false;
                  '; 
               }
            } else {
               echo '
                  $.notify("Saving Error in Family Background!!!");
                  return false;
               '; 
            }
               
         }
      } 
   }
   function refreshGridAccountablity($params) {
      $paramsAssArr = convArray($params);
      $divId = $paramsAssArr["divId"];
      $empRefId = $paramsAssArr["empRefId"];
      echo "$('#".$divId."').html('<h3>HELLO $params</h3>')";
   }
   function convArray($params) {
      $paramsArr = explode("&",$params);
      $AssociativeArray = [];
      foreach($paramsArr as $parameters) {
         $itemArr = explode("=",$parameters);
         $AssociativeArray[$itemArr[0]] = $itemArr[1];
      }
      return $AssociativeArray;
   }
   function fnComputeLeaveAmount($params){

      /*echo 'alert("'.$params['lastname'].'");';
      echo 'alert("'.$params['firstname'].'");';
      echo 'alert("'.$params['middlename'].'");';*/
      $amount = getvalue("objname_value") * getvalue("salary") * 0.0481927;
      $amount = round($amount,3);
      echo '$("[name=\''.getvalue("objname_amount").'\']").val("'.$amount.'");';
   }
   function fnDBTransac($params) {
      $today = date("Y-m-d",time());
      $Fields = $Values = "";
      $tblno        = getvalue("tbl");
      $transactMode = getvalue("hmode");
      $Remarks      = getvalue("char_Remarks");
      $VLEarned     = getvalue("deci_VLEarned");
      $SLEarned     = getvalue("deci_SLEarned");
      switch ($tblno) {
         case 1:
            $table      = "slvlearnedmonthly";
            $NoOfMonths = getvalue("sint_NoOfMonths");
            if ($transactMode == "ADD") {
               $Fields     = "`NoOfMonths`, `VLEarned`, `SLEarned`,`Remarks`,";
               $Values     = "'$NoOfMonths', '$VLEarned', '$SLEarned','$Remarks',";
            }
            if ($transactMode == "EDIT") {
               $Fields     .= "`NoOfMonths` = '$NoOfMonths',";
               $Fields     .= "`VLEarned` = '$VLEarned',";
               $Fields     .= "`SLEarned` = '$SLEarned',";
               $Fields     .= "`Remarks` = '$Remarks',";
               $Values     = getvalue("hRefId");
            }
            $attr = array (
               "ColHdr"=>["NO. OF MONTHS","VACATION LEAVE EARNED","SICK LEAVE EARNED"],
               "ColFld"=>["NoOfMonths","VLEarned","SLEarned"],
               "clause"=>"where EffectivityDate <= '$today' ORDER BY EffectivityDate DESC, NoOfMonths LIMIT 12",
               "dbtable"=>$table
            );
         break;
         case 2:
            $table = "slvlearneddaily";
            $NoOfDays   = getvalue("sint_NoOfDays");
            if ($transactMode == "ADD") {
               $Fields     = "`NoOfDays`, `VLEarned`, `SLEarned`,`Remarks`,";
               $Values     = "'$NoOfDays', '$VLEarned', '$SLEarned','$Remarks',";
            }
            if ($transactMode == "EDIT") {
               $Fields     .= "`NoOfDays` = '$NoOfDays',";
               $Fields     .= "`VLEarned` = '$VLEarned',";
               $Fields     .= "`SLEarned` = '$SLEarned',";
               $Fields     .= "`Remarks` = '$Remarks',";
               $Values     = getvalue("hRefId");
            }
            $attr = array (
               "ColHdr"=>["NO. OF DAYS","VACATION LEAVE EARNED","SICK LEAVE EARNED"],
               "ColFld"=>["NoOfDays","VLEarned","SLEarned"],
               "clause"=>"where EffectivityDate <= '$today' ORDER BY EffectivityDate DESC, NoOfDays LIMIT 31",
               "dbtable"=>$table
            );
         break;
         case 3:
            $table  = "leavecreditsearnedwopay";
            $NoOfDaysPresent = getvalue("deci_NoOfDaysPresent");
            $NoOfDaysLeaveWOP = getvalue("deci_NoOfDaysLeaveWOP");
            $LeaveCreditsEarned = getvalue("deci_LeaveCreditsEarned");
            if ($transactMode == "ADD") {
               $Fields = "`NoOfDaysPresent`, `NoOfDaysLeaveWOP`, `LeaveCreditsEarned`,`Remarks`,";
               $Values = "'$NoOfDaysPresent', '$NoOfDaysLeaveWOP', '$LeaveCreditsEarned','$Remarks',";
            }
            if ($transactMode == "EDIT") {
               $Fields = "`NoOfDaysPresent`='$NoOfDaysPresent',`NoOfDaysLeaveWOP`='$NoOfDaysLeaveWOP',`LeaveCreditsEarned`='$LeaveCreditsEarned',`Remarks`='$Remarks',";
               $Values = getvalue("hRefId");
            }
            $attr = array (
               "ColHdr"=>["NO. OF DAYS PRESENT","NO. OF DAYS ON LEAVE W/O PAY","LEAVE CREDITS EARNED"],
               "ColFld"=>["NoOfDaysPresent","NoOfDaysLeaveWOP","LeaveCreditsEarned"],
               "clause"=>"ORDER BY NoOfDaysPresent",
               "dbtable"=>$table
            );
         break;
         case 4:
            $table = "workinghrsconversion";
            $NoOf = getvalue("sint_NoOf");
            $EquivalentDay = getvalue("deci_EquivalentDay");
            $Type = getvalue("char_Type");
            if ($transactMode == "ADD") {
               $Fields = "`NoOf`, `EquivalentDay`, `Remarks`, `Type`,";
               $Values = "'$NoOf', '$EquivalentDay', '$Remarks', '$Type',";
            }
            if ($transactMode == "EDIT") {
               $Fields = "`NoOf` = '$NoOf', `EquivalentDay` = '$EquivalentDay', `Remarks` = '$Remarks', `Type` = '$Type',";
               $Values = getvalue("hRefId");
            }
            $attr = array (
               "ColHdr"=>["TYPE","NO. OF","EQUIVALENT"],
               "ColFld"=>["Type","NoOf","EquivalentDay"],
               "clause"=>"ORDER BY Type, NoOf",
               "dbtable"=>$table
            );
         break;
      }

      if ($transactMode == "ADD") {
         $SaveSuccessfull = f_SaveRecord("NEWSAVE",$table,$Fields,$Values);
         if (is_numeric($SaveSuccessfull)) {
            updateListTable($attr);
         } else {
            echo $SaveSuccessfull;
         }
      }
      if ($transactMode == "EDIT") {
         $SaveSuccessfull = f_SaveRecord("EDITSAVE",$table,$Fields,$Values);
         if ($SaveSuccessfull != "") {
            echo $SaveSuccessfull;
         } else {
            updateListTable($attr);
         }
      }
   }
   function fnsetObjEnabled() {
         $emprefid = getvalue("emprefid");
         $emppdsq = FindFirst("employeespdsq","WHERE EmployeesRefId = ".$emprefid,"*");
         if ($emppdsq) {
            if ($emppdsq["Q1a"] == 1) $TF1 = "false";
                                 else $TF1 = "true";
            if ($emppdsq["Q1b"] == 1) $TF2 = "false";
                                 else $TF2 = "true";
            if ($emppdsq["Q2a"] == 1) $TF3 = "false";
                                 else $TF3 = "true";
            if ($emppdsq["Q2b"] == 1) $TF4 = "false";
                                 else $TF4 = "true";
            if ($emppdsq["Q3a"] == 1) $TF5 = "false";
                                 else $TF5 = "true";
            if ($emppdsq["Q4a"] == 1) $TF6 = "false";
                                 else $TF6 = "true";
            if ($emppdsq["Q5a"] == 1) $TF7 = "false";
                                 else $TF7 = "true";
            if ($emppdsq["Q5b"] == 1) $TF8 = "false";
                                 else $TF8 = "true";
            if ($emppdsq["Q6a"] == 1) $TF9 = "false";
                                 else $TF9 = "true";
            if ($emppdsq["Q7a"] == 1) $TF10 = "false";
                                 else $TF10 = "true";
            if ($emppdsq["Q7b"] == 1) $TF11 = "false";
                                 else $TF11 = "true";
            if ($emppdsq["Q7c"] == 1) $TF12 = "false";
                                 else $TF12 = "true";
            echo '$("[name=\'char_Q1aexp\']").prop("disabled",'.$TF1.');';
            echo '$("[name=\'char_Q1bexp\']").prop("disabled",'.$TF2.');';
            echo '$("[name=\'char_Q2aexp\']").prop("disabled",'.$TF3.');';
            echo '$("[name=\'char_Q2bexp\'], [name=\'date_Q2DateFiled\'], [name=\'char_Q2CaseStatus\']").prop("disabled",'.$TF4.');';
            echo '$("[name=\'char_Q3aexp\']").prop("disabled",'.$TF5.');';
            echo '$("[name=\'char_Q4aexp\']").prop("disabled",'.$TF6.');';
            echo '$("[name=\'char_Q5aexp\']").prop("disabled",'.$TF7.');';
            echo '$("[name=\'char_Q5bexp\']").prop("disabled",'.$TF8.');';
            echo '$("[name=\'char_Q6aexp\']").prop("disabled",'.$TF9.');';
            echo '$("[name=\'char_Q7aexp\']").prop("disabled",'.$TF10.');';
            echo '$("[name=\'char_Q7bexp\']").prop("disabled",'.$TF11.');';
            echo '$("[name=\'char_Q7cexp\']").prop("disabled",'.$TF12.');';
         }
         /*$rowEmpEduc = FindFirst("employeeseduc","WHERE EmployeesRefId = ".$emprefid,"*");
         if ($rowEmpEduc) {
            if ($rowEmpEduc["YearGraduated"] > 0) {
               echo '$("[name=\'char_Q7cexp\']").prop("disabled",'.$TF12.');';
            }
         }*/
   }
   function fnDBTransac2($params) {
      // echo 'alert("'.getvalue("date_EffectivityDate").'");';
      $tblno = getvalue("tbl");
      $effDate = getvalue("date_EffectivityDate");
      $transactMode = getvalue("hmode");
      $err = "";
      switch ($tblno) {
         case 1:
            $table      = "slvlearnedmonthly";
            $NoOfMonths = getvalue("sint_NoOfMonths");
            if ($transactMode == "ADD") {
               $Fields     = "`EffectivityDate`,`NoOfMonths`, `VLEarned`, `SLEarned`,`Remarks`,";
               for ($h=1;$h<=12;$h++) {
                  $vl = getvalue("deci_VLEarned_".$h);
                  $sl = getvalue("deci_SLEarned_".$h);
                  if ($vl == "") $vl = "0.00";
                  if ($sl == "") $sl = "0.00";
                  $Values     = "'$effDate','".getvalue("sint_NoOfMonths_".$h)."', '".$vl."', '".$sl."','',";
                  $SaveSuccessfull = f_SaveRecord("NEWSAVE",$table,$Fields,$Values);
                  if (!is_numeric($SaveSuccessfull)) {
                     $err .= $SaveSuccessfull;
                  }
               }
            }
            if ($err != "") {
               echo $err;
            }
            $attr = array (
               "ColHdr"=>["EFF. DATE","NO. OF MONTHS","VACATION LEAVE EARNED","SICK LEAVE EARNED"],
               "ColFld"=>["EffectivityDate","NoOfMonths","VLEarned","SLEarned"],
               "clause"=>"where EffectivityDate = '$effDate' ORDER BY EffectivityDate, NoOfMonths",
               "dbtable"=>$table
            );
         break;
         case 2:
            $table      = "slvlearneddaily";
            $NoOfMonths = getvalue("sint_NoOfDays");
            if ($transactMode == "ADD") {
               $Fields = "`EffectivityDate`,`NoOfDays`, `VLEarned`, `SLEarned`,";
               for ($h=1;$h<=31;$h++) {
                  $vl = getvalue("deci_VLEarned_".$h);
                  $sl = getvalue("deci_SLEarned_".$h);
                  if ($vl == "") $vl = "0.00";
                  if ($sl == "") $sl = "0.00";
                  $Values     = "'$effDate','".getvalue("sint_NoOfDays_".$h)."', '".$vl."', '".$sl."',";
                  $SaveSuccessfull = f_SaveRecord("NEWSAVE",$table,$Fields,$Values);
                  if (!is_numeric($SaveSuccessfull)) {
                     $err .= $SaveSuccessfull;
                  }
               }
            }
            if ($err != "") {
               echo $err;
            }
            $attr = array (
               "ColHdr"=>["EFF. DATE","NO. OF DAYS","VACATION LEAVE EARNED","SICK LEAVE EARNED"],
               "ColFld"=>["EffectivityDate","NoOfDays","VLEarned","SLEarned"],
               "clause"=>"where EffectivityDate = '$effDate' ORDER BY EffectivityDate, NoOfDays",
               "dbtable"=>$table
            );
         break;
         case 3:
            $table  = "leavecreditsearnedwopay";
            if ($transactMode == "ADD") {
               $Fields = "`EffectivityDate`, `NoOfDaysPresent`, `NoOfDaysLeaveWOP`, `LeaveCreditsEarned`,";
               for ($h=1;$h<=61;$h++) {
                  $NoOfDaysPresent = getvalue("sint_NoOfDaysPresent_".$h);
                  $NoOfDaysLeaveWOP = getvalue("deci_NoOfDaysLeaveWOP_".$h);
                  $LeaveCreditsEarned = getvalue("deci_LeaveCreditsEarned_".$h);
                  $Values = "'$effDate', '$NoOfDaysPresent', '$NoOfDaysLeaveWOP', '$LeaveCreditsEarned',";
                  $SaveSuccessfull = f_SaveRecord("NEWSAVE",$table,$Fields,$Values);
                  if (!is_numeric($SaveSuccessfull)) {
                     $err .= $SaveSuccessfull;
                  }
               }
            }
            if ($err != "") {
               echo $err;
            }
            $attr = array (
               "ColHdr"=>["EFF. DATE","NO. OF DAYS PRESENT","NO. OF DAYS ON LEAVE W/O PAY","LEAVE CREDITS EARNED"],
               "ColFld"=>["EffectivityDate","NoOfDaysPresent","NoOfDaysLeaveWOP","LeaveCreditsEarned"],
               "clause"=>"where EffectivityDate = '$effDate' ORDER BY EffectivityDate DESC, NoOfDaysPresent DESC",
               "dbtable"=>$table
            );
         break;
         case 4:
            $table  = "workinghrsconversion";
            if ($transactMode == "ADD") {
               $Fields = "`EffectivityDate`, `NoOf`, `EquivalentDay`,";
               for ($h=1;$h<=60;$h++) {
                  $NoOf          = getvalue("sint_NoOf_".$h);
                  $EquivalentDay = getvalue("deci_EquivalentDay_".$h);
                  $Values = "'$effDate', '$NoOf', '$EquivalentDay',";
                  $SaveSuccessfull = f_SaveRecord("NEWSAVE",$table,$Fields,$Values);
                  if (!is_numeric($SaveSuccessfull)) {
                     $err .= $SaveSuccessfull;
                  }
               }
            }
            if ($err != "") {
               echo $err;
            }
            $attr = array (
               "ColHdr"=>["EFF. DATE","NO. OF","EQUIVALENT"],
               "ColFld"=>["EffectivityDate", "NoOf", "EquivalentDay"],
               "clause"=>"where EffectivityDate = '$effDate' ORDER BY EffectivityDate DESC, NoOf",
               "dbtable"=>$table
            );
         break;
      }
      updateListTable($attr);
   }
   function fnLoadTable($params) {
      switch (getvalue("tblNo")) {
         case 1:
            $rs = SelectEach("slvlearnedmonthly","where EffectivityDate = '".getvalue("effDate")."' order by NoOfMonths");
            if ($rs) {
               while ($row = mysqli_fetch_array($rs)) {
                  $idx = $row["NoOfMonths"];
                  echo '$("[name=\'sint_NoOfMonths_'.$idx.'\']").val("'.$idx.'");';
                  echo '$("[name=\'deci_VLEarned_'.$idx.'\']").val("'.$row["VLEarned"].'");';
                  echo '$("[name=\'deci_SLEarned_'.$idx.'\']").val("'.$row["SLEarned"].'");';
               }
            }
         break;
         case 2:
            $rs = SelectEach("slvlearneddaily","where EffectivityDate = '".getvalue("effDate")."' order by NoOfDays");
            if ($rs) {
               $idx = 0;
               while ($row = mysqli_fetch_array($rs)) {
                  $idx = $row["NoOfDays"];
                  echo '$("[name=\'sint_NoOfDays_'.$idx.'\']").val("'.$idx.'");';
                  echo '$("[name=\'deci_VLEarned_'.$idx.'\']").val("'.$row["VLEarned"].'");';
                  echo '$("[name=\'deci_SLEarned_'.$idx.'\']").val("'.$row["SLEarned"].'");';
               }
            }
         break;
         case 3:
            $rs = SelectEach("slvlearneddaily","where EffectivityDate = '".getvalue("effDate")."' order by NoOfDays");
            if ($rs) {
               $idx = 0;
               while ($row = mysqli_fetch_array($rs)) {
                  $idx = $row["NoOfDays"];
                  echo '$("[name=\'sint_NoOfDays_'.$idx.'\']").val("'.$idx.'");';
                  echo '$("[name=\'deci_VLEarned_'.$idx.'\']").val("'.$row["VLEarned"].'");';
                  echo '$("[name=\'deci_SLEarned_'.$idx.'\']").val("'.$row["SLEarned"].'");';
               }
            }
         break;
         case 4:
            $rs = SelectEach("slvlearneddaily","where EffectivityDate = '".getvalue("effDate")."' order by NoOfDays");
            if ($rs) {
               $idx = 0;
               while ($row = mysqli_fetch_array($rs)) {
                  $idx = $row["NoOfDays"];
                  echo '$("[name=\'sint_NoOfDays_'.$idx.'\']").val("'.$idx.'");';
                  echo '$("[name=\'deci_VLEarned_'.$idx.'\']").val("'.$row["VLEarned"].'");';
                  echo '$("[name=\'deci_SLEarned_'.$idx.'\']").val("'.$row["SLEarned"].'");';
               }
            }
         break;
      }
   }
   function fnSaveRecord($params) {
      include_once 'conn.e2e.php';
      $err    = "";
      $table  = getvalue("hTable");
      $_SESSION["sysData"] = "DataLibrary";
      $RefId = getvalue("hRefId");
      $transactMode = getvalue("hmode");
      $sql = "SELECT * FROM `$table` ORDER BY RefId Desc LIMIT 100";
      $trnParam = "NEWSAVE";
      $sql = "SHOW FULL COLUMNS FROM `$table`";
      $Colresult = mysqli_query($conn,$sql) or die(mysqli_error($conn));
      $Fields = "";
      $Values = "";
      $Fields2 = "";
      if ($Colresult) {
         while($row = mysqli_fetch_array($Colresult))
         {
            if ($row['Field'] == "RefId" ||
                $row['Field'] == "CompanyRefId" ||
                $row['Field'] == "BranchRefId" ||
                $row['Field'] == "EmployeesRefId" ||
                $row['Field'] == "LastUpdateBy" ||
                $row['Field'] == "LastUpdateDate" ||
                $row['Field'] == "LastUpdateTime" ||
                $row['Field'] == "Data") {
               $include = false;
               $flds = "";
            } else {
               $flds = $row['Field'];
               $include = true;
            }
            if ($include) {
               $Fields .= "`$flds`,";
               if ($row['Type'] == "date") {
                  $Val1 = "date_".$flds;
               } else if (stripos($row['Type'],"in(")) {
                  $Val1 = "sint_".$flds;

               } else if (stripos($row['Type'],"ecimal")) {
                  $Val1 = "deci_".$flds;
               } else if (stripos($row['Type'],"char")) {
                  $Val1 = "char_".$flds;
               }
               $Values .= "'".getvalue($Val1)."',";

               if ($transactMode == "EDIT") {
                  $Fields2 .= "`$flds` = '".getvalue($Val1)."',";
                  $trnParam = "EDITSAVE";
               }
            }
         }
         if ($transactMode == "EDIT") {
            $Fields = $Fields2;
            $Values = $RefId;
         }
      }

      switch ($table) {
         case "leavepolicy":
            /*$Fields  = "`Name`, `LeavePolicyGroupRefId`, `LeavesRefId`, `Accumulating`, `OffsetLate`,";
            $Fields .= "`OffsetUT`, `OffsetIfNoFile`, `ForceLeaveApplied`, `AffectedByAbs`, `Value`, `MaxForceLeave`,";
            $Values  = "'".getvalue("char_LPName")."','".getvalue("sint_LeavePolicyGroupRefId")."','".getvalue("sint_LeavesRefId")."','".getvalue("sint_Accumulating")."','".getvalue("sint_OffsetLate")."',";
            $Values  .= "'".getvalue("sint_OffsetUT")."','".getvalue("sint_OffsetIfNoFile")."','".getvalue("sint_ForceLeaveApplied")."','".getvalue("sint_AffectedByAbs")."','".getvalue("deci_Value")."','".getvalue("char_MaxForceLeave")."',";*/
            $gridTable = "gridTable_lp";
            $GridHdr_Arr = ["Name","Leave Policy Group","Accumulating","Offset Late","Offset Undertime","Offset If No File"];
            $GridFld_Arr = ["Name","LeavePolicyGroupRefId","Accumulating","OffsetLate","OffsetUT","OffsetIfNoFile"];
         break;
         case "leavepolicygroup":
            $gridTable = "spGridTable_lpgroup";

            /*$Fields = "Code,Name,Description,Remarks,";
            $Values = "'".getvalue("char_Code")."','".getvalue("char_Name")."','".getvalue("char_Description")."','".getvalue("char_Remarks")."',";*/

            $GridHdr_Arr = ["Code","Name","Description"];
            $GridFld_Arr = ["Code","Name","Description"];
            //$GridHdr_Arr = array("Code","Name","Description");
            //$GridFld_Arr = array("Code","Name","Description");

         break;
         case "overtimepolicy":
            /*$Fields = "`Name`,`Remarks`,`OvertimePolicyGroupRefId`,`MaxOTWorkingDays`,`MaxOTSaturday`,";
            $Fields .= "`MaxOTHolidays`,`MaxOTMonth`,`MaxUnusedCOCYear`,`ValidYears`,`COCRatesWorkingDays`,`COCRatesSaturday`,`COCRatesHolidays`,`PrevDay`,";
            $Values = "'".getvalue("char_OTName")."','".getvalue("char_Remarks")."',";
            $Values .= "'".getvalue("sint_OvertimePolicyGroupRefId")."','".getvalue("sint_MaxOTWorkingDays")."','".getvalue("sint_MaxOTSaturday")."',";
            $Values .= "'".getvalue("sint_MaxOTHolidays")."','".getvalue("sint_MaxOTMonth")."','".getvalue("sint_MaxUnusedCOCYear")."',";
            $Values .= "'".getvalue("sint_ValidYears")."','".getvalue("sint_COCRatesWorkingDays")."','".getvalue("sint_COCRatesSaturday")."',";
            $Values .= "'".getvalue("sint_COCRatesHolidays")."','".getvalue("sint_PrevDay")."',";*/
            $gridTable   = "gridTable_ot";
            $GridHdr_Arr = ["Name", "OT Policy Group", "Max OT in Month"];
            $GridFld_Arr = ["Name", "OvertimePolicyGroupRefId", "MaxOTMonth"];
         break;
         case "overtimepolicygroup":
            $gridTable = "gridTable_opgroup";
            /*$Fields = "Code,Name,Description,Remarks,";
            $Values = "'".getvalue("char_Code")."','".getvalue("char_Name")."','".getvalue("char_Description")."','".getvalue("char_Remarks")."',";*/
            $GridHdr_Arr = ["Code","Name","Description"];
            $GridFld_Arr = ["Code","Name","Description"];
         break;
      }
      $SaveSuccessfull = f_SaveRecord($trnParam,$table,$Fields,$Values);

      /*if (!is_numeric($SaveSuccessfull)) {
         echo $SaveSuccessfull;
      } else {*/
         $sql = "SELECT * FROM $table ORDER BY RefId Desc LIMIT 100";
         doGridTable($table,
            $GridHdr_Arr,
            $GridFld_Arr,
            $sql,
            [true,true,true,false],
            $gridTable);
      /*}*/

   }
   function fnPostStatus($params) {
      $Fields = "";
      $counter = 0;
      $thisYear = date("Y",time());
      if (getvalue("status") == "Rejected")  {
         $Fields  = "`Reason` = '".getvalue("reason")."', ";
         $Fields .= "`Status` = '".getvalue("status")."', ";
      } else {
         $Fields  = "`ApprovedByRefId` = '".getvalue("apprvBy")."', ";
         $Fields .= "`Status` = '".getvalue("status")."', ";
      }
      $SaveSuccessfull = f_SaveRecord("EDITSAVE",getvalue("tbl"),$Fields,getvalue("refid"));
      //$SaveSuccessfull = "";
      if ($SaveSuccessfull == "") {
         switch (getvalue("tbl")) {
            case 'employeesleavemonetization':
               $rowMonetization = FindFirst(getvalue("tbl"),"WHERE RefId = ".getvalue("refid"),"*");
               if ($rowMonetization) {
                  $slVal = $rowMonetization["SLValue"];
                  $vlVal = $rowMonetization["VLValue"];
                  $emprefid = $rowMonetization["EmployeesRefId"];
                  $company = $rowMonetization["CompanyRefId"];
                  $branch = $rowMonetization["BranchRefId"];
                  $where = "WHERE CompanyRefId = $company AND BranchRefId = $branch AND EmployeesRefId = $emprefid";
                  $where .= " AND EffectivityYear = '".$thisYear."'";
                  if ($slVal > 0) {
                     $CBrow = FindFirst("employeescreditbalance",$where." AND NameCredits = 'SL'","*");
                     $cbrefid = $CBrow["RefId"];
                     if ($CBrow["OutstandingBalance"] != "") {
                        $newsl = $CBrow["OutstandingBalance"] - $slVal;
                     } else {
                        $newsl = $CBrow["BeginningBalance"] - $slVal;
                     }
                     $fldnval = "OutstandingBalance = '".$newsl."', BegBalAsOfDate = '".date("Y-m-d",time())."',";
                     $update = f_SaveRecord("EDITSAVE","employeescreditbalance",$fldnval,$cbrefid);
                     if ($update != "") {
                        $counter++;
                     }
                  }
                  if ($vlVal > 0) {
                     $CBrow = FindFirst("employeescreditbalance",$where." AND NameCredits = 'VL'","*");
                     $cbrefid = $CBrow["RefId"];
                     if ($CBrow["OutstandingBalance"] != "") {
                        $newvl = $CBrow["OutstandingBalance"] - $vlVal;
                     } else {
                        $newvl = $CBrow["BeginningBalance"] - $vlVal;
                     }
                     $fldnval = "OutstandingBalance = '".$newsl."', BegBalAsOfDate = '".date("Y-m-d",time())."',";
                     $update = f_SaveRecord("EDITSAVE","employeescreditbalance",$fldnval,$cbrefid);
                     if ($update != "") {
                        $counter++;
                     }
                  }
               }
               break;
            case 'employeesauthority':
               $rowAuthority = FindFirst(getvalue("tbl"),"WHERE RefId = ".getvalue("refid"),"*");
               if ($rowAuthority) {
                  $emprefid = $rowAuthority["EmployeesRefId"];
                  $workschedule = FindFirst("empinformation","WHERE EmployeesRefId = $emprefid","WorkScheduleRefId");
                  $rowWorkSched = FindFirst("workschedule","WHERE RefId = $workschedule","*");
                  $OB_Fld       = "EmployeesRefId, AttendanceDate, AttendanceTime, CheckTime, KindOfEntry,";
                  if ($rowAuthority["FromTime"] != "" || $rowAuthority["ToTime"]) {
                     $OBOUT = $rowAuthority["FromTime"] * 60;   
                     $OBIN = $rowAuthority["ToTime"] * 60;
                     $OBOUT_Val  = "$emprefid, '$AttendanceDate', $OBOUT, $OBOUT, 7,";
                     $OBIN_Val  = "$emprefid, '$AttendanceDate', $OBIN, $OBIN, 8,";
                     $save_obout = f_SaveRecord("NEWSAVE","employeesattendance",$OB_Fld,$OBOUT_Val);
                     if (is_numeric($save_obout)) {
                        $save_obin = f_SaveRecord("NEWSAVE","employeesattendance",$OB_Fld,$OBIN_Val);
                        if (!is_numeric($save_obin)) {
                           echo $save_obin;
                           $counter++;
                        }
                     } else {
                        echo $save_obout;
                     }
                  } else {
                     $DateFrom = $rowAuthority["ApplicationDateFrom"];
                     $DateTo = $rowAuthority["ApplicationDateTo"];
                     $date_diff = dateDifference($DateFrom,$DateTo);
                     $counter = 0;
                     for ($i=0; $i <= $date_diff; $i++) { 
                        $AttendanceDate = date('Y-m-d', strtotime( "$DateFrom + $i day" ));
                        $day_name = date("D",strtotime( "$DateFrom + $i day" ));
                        switch ($day_name) {
                           case 'Mon':
                              $day_name = "Monday";
                              break;
                           case 'Tue':
                              $day_name = "Tuesday";
                              break;
                           case 'Wed':
                              $day_name = "Wednesday";
                              break;
                           case 'Thu':
                              $day_name = "Thursday";
                              break;
                           case 'Fri':
                              $day_name = "Friday";
                              break;
                           case 'Sat':
                              $day_name = "Saturday";
                              break;
                           case 'Sun':
                              $day_name = "Sunday";
                              break;
                        }
                        $OBOUT = $rowWorkSched[$day_name."In"] * 60;
                        $OBIN  = $rowWorkSched[$day_name."Out"] * 60;
                        $OBOUT = strtotime($AttendanceDate) + $OBOUT;
                        $OBIN  = strtotime($AttendanceDate) + $OBIN; 
                        $OBOUT_Val  = "$emprefid, '$AttendanceDate', $OBOUT, $OBOUT, 7,";
                        $OBIN_Val  = "$emprefid, '$AttendanceDate', $OBIN, $OBIN, 8,";
                        $save_obout = f_SaveRecord("NEWSAVE","employeesattendance",$OB_Fld,$OBOUT_Val);
                        if (is_numeric($save_obout)) {
                           $save_obin = f_SaveRecord("NEWSAVE","employeesattendance",$OB_Fld,$OBIN_Val);
                           if (!is_numeric($save_obin)) {
                              echo $save_obin;
                              $counter++;
                           }
                        } else {
                           echo $save_obout;
                           $counter++;
                        }
                     }
                  }
               } else {
                  echo "alert('No Record Found');";   
               }
            break;
            case "employeescto":
               $row = FindFirst(getvalue("tbl"),"WHERE RefId = ".getvalue("refid"),"*");
               if ($row) {
                  $time       = $row["Hours"] * 60;
                  $emprefid   = $row["EmployeesRefId"];
                  $company    = $row["CompanyRefId"];
                  $branch     = $row["BranchRefId"];
                  $where      = "WHERE CompanyRefId = $company AND BranchRefId = $branch AND EmployeesRefId = $emprefid";
                  $where      .= " AND EffectivityYear = '".$thisYear."'";
                  $CBrow = FindFirst("employeescreditbalance",$where." AND NameCredits = 'OT'","*");
                  $cbrefid = $CBrow["RefId"];
                  if ($CBrow["OutstandingBalance"] != "") {
                     $newcoc = $CBrow["OutstandingBalance"] - $time;
                  } else {
                     $newcoc = $CBrow["BeginningBalance"] - $time;
                  }
                  $fldnval = "OutstandingBalance = '".$newcoc."', BegBalAsOfDate = '".date("Y-m-d",time())."',";
                  $update = f_SaveRecord("EDITSAVE","employeescreditbalance",$fldnval,$cbrefid);
                  if ($update != "") {
                     $counter++;
                  }
               }
               break;
            case 'employeesleave':     
               $row = FindFirst(getvalue("tbl"),"WHERE RefId = ".getvalue("refid"),"*");
               if ($row) {
                  $emprefid   = $row["EmployeesRefId"];
                  $company    = $row["CompanyRefId"];
                  $branch     = $row["BranchRefId"];
                  $DateFrom   = $row["ApplicationDateFrom"];
                  $DateTo     = $row["ApplicationDateTo"];
                  $date_diff  = dateDifference($DateFrom,$DateTo);
                  $credit     = getRecord("leaves",$row["LeavesRefId"],"Code");
                  $where      = "WHERE CompanyRefId = $company";
                  $where      .= " AND BranchRefId = $branch";
                  $where      .= " AND EmployeesRefId = $emprefid";
                  $where      .= " AND EffectivityYear = '".$thisYear."'";
                  $where      .= " AND NameCredits = '".$credit."'";
                  if ($credit == "VL") {
                     $CBrow = FindFirst("employeescreditbalance",$where,"*");
                     if ($CBrow) {
                        $cbrefid = $CBrow["RefId"];
                        if ($CBrow["OutstandingBalance"] != "") {
                           $newleave = $CBrow["OutstandingBalance"] - $date_diff;
                        } else {
                           $newleave = $CBrow["BeginningBalance"] - $date_diff;
                        }
                        if ($newleave <= 0) {
                           $newleave = 0;
                        }
                        $fldnval = "OutstandingBalance = '".$newleave."', BegBalAsOfDate = '".date("Y-m-d",time())."',";
                        $update = f_SaveRecord("EDITSAVE","employeescreditbalance",$fldnval,$cbrefid);
                        if ($update != "") {
                           $counter++;
                        }
                     }   
                  } else if ($credit == "SL") {
                     $CBrow = FindFirst("employeescreditbalance",$where,"*");
                     if ($CBrow) {
                        $cbrefid = $CBrow["RefId"];
                        if ($CBrow["OutstandingBalance"] != "") {
                           $newleave = $CBrow["OutstandingBalance"] - $date_diff;
                        } else {
                           $newleave = $CBrow["BeginningBalance"] - $date_diff;
                        }
                        if ($newleave < 0) {
                           $where      = "WHERE CompanyRefId = $company";
                           $where      .= " AND BranchRefId = $branch";
                           $where      .= " AND EmployeesRefId = $emprefid";
                           $where      .= " AND EffectivityYear = '".$thisYear."'";
                           $where      .= " AND NameCredits = 'VL'";
                           $new_CBrow  = FindFirst("employeescreditbalance",$where,"*");
                           if ($new_CBrow) {
                              $borrow_refid = $new_CBrow["RefId"];
                              if ($CBrow["OutstandingBalance"] != "") {
                                 $borrow_leave = $CBrow["OutstandingBalance"];
                              } else {
                                 $borrow_leave = $CBrow["BeginningBalance"];
                              }
                              if ($borrow_leave > $newleave) {
                                 $borrow_left = $borrow_leave + $newleave;   
                              } else {
                                 $borrow_left = 0;
                              }
                              if ($borrow_left > 0) {
                                 $remarks = "You have borrowed '".$borrow_left."' from your VL";
                                 $update_leave = "WithPay = 1, ";
                                 $borrow_result = f_SaveRecord("EDITSAVE","employeesleave",$update_leave,getvalue("refid"));
                                 if ($borrow_result != "") {
                                    $counter++;
                                 }
                              } else {
                                 $remarks = "You have borrowed all your VL Earnings (".$borrow_leave."). ";
                                 $update_leave = "WithPay = 0, ";
                                 $borrow_result = f_SaveRecord("EDITSAVE","employeesleave",$update_leave,getvalue("refid"));
                                 if ($borrow_result != "") {
                                    $counter++;
                                 }
                              }
                              $borrowed = $borrow_left - $borrow_leave;
                              $new_fldnval   = "OutstandingBalance = '".$borrow_left."',";
                              $new_fldnval   .= "BegBalAsOfDate = '".date("Y-m-d",time())."',";
                              $new_fldnval   .= "Remarks = '$remarks', ";
                              $new_update    = f_SaveRecord("EDITSAVE","employeescreditbalance",$new_fldnval,$borrow_refid);
                              if ($new_update != "") {
                                 $counter++;
                              } else {
                                 $newleave = 0;
                              }
                           }
                        }
                        $fldnval = "OutstandingBalance = '".$newleave."', BegBalAsOfDate = '".date("Y-m-d",time())."',";
                        $update = f_SaveRecord("EDITSAVE","employeescreditbalance",$fldnval,$cbrefid);
                        if ($update != "") {
                           $counter++;
                        }
                     }
                  }
                  
               }
               break;

            case 'fl_cancellation_request':
               $row = FindFirst(getvalue("tbl"),"WHERE RefId = ".getvalue("refid"),"*");
               if ($row) {
                  $leaverefid    = $row["EmployeesLeaveRefId"];
                  $row_leave     = FindFirst("employeesleave","WHERE RefId = $leaverefid","*");
                  if ($row_leave) {
                     $creditName    = getRecord("leaves",$row_leave["LeavesRefId"],"Code");
                     $DateFrom      = $row_leave["ApplicationDateFrom"];
                     $DateTo        = $row_leave["ApplicationDateTo"];
                     $emprefid      = $row_leave["EmployeesRefId"];
                     $date_diff     = dateDifference($DateFrom,$DateTo);
                     if ($date_diff == 0) $date_diff = 1;
                     if ($creditName == "VL" || $creditName == "SL") {
                        $wherecreditbal = "WHERE EmployeesRefId = ".$emprefid." AND NameCredits = '$creditName'";
                        $EmpCreditBal = FindFirst("employeescreditbalance",$wherecreditbal,"*");
                        if ($EmpCreditBal) {
                           $fldnval = "BegBalAsOfDate = '".date("Y-m-d",time())."',";
                           if ($EmpCreditBal["OutstandingBalance"] != "") {
                              $fldnval .= "OutstandingBalance = '".($EmpCreditBal["OutstandingBalance"] + $date_diff)."',";
                           } else {
                              $fldnval .= "OutstandingBalance = '".($EmpCreditBal["BeginningBalance"] + $date_diff)."',";
                           }
                           $update = f_SaveRecord("EDITSAVE","employeescreditbalance",$fldnval,$EmpCreditBal["RefId"]);
                           if ($update != "") {
                              $counter++;
                           } else {
                              $leave_fldnval = "Status = 'Cancelled', ";
                              $update_leave = f_SaveRecord("EDITSAVE","employeesleave",$leave_fldnval,$row_leave["RefId"]);
                              if ($update_leave != "") {
                                 $counter++;
                              }
                           }   
                        }
                     }   
                  }
                  
               }
               break;
            default:
               $counter = 0;
               break;
         }
         if ($counter == 0) {
            echo "alert('Record Succesfully Updated');";         
         }
      }
   }
   function fnsearchEmployees($searchString) {
      include 'conn.e2e.php';
      $searchString = getvalue("srch");
      $inactive = getvalue("Inactive");
      $filter_table = getvalue("table");
      $filter_refid = getvalue("refid");
      if ($filter_table != "") {
         $where_empinfo = "WHERE RefId = '$filter_refid'";
      } else {
         $where_empinfo = "";
      }
      $where = "";
      if (is_numeric($searchString)) {
         $where = "WHERE employees.RefId = $searchString";
         if ($inactive == 1) {
            $where .= " AND employees.Inactive = 1";
         } else {
            $where .= " AND (employees.Inactive != 1 OR employees.Inactive IS NULL)";
         }
      } else {
         if (!empty($searchString)) {
            $where = "WHERE (employees.LastName LIKE '%$searchString%' OR employees.FirstName LIKE '%$searchString%')";
            if ($inactive == 1) {
               $where .= " AND employees.Inactive = 1";
            } else {
               $where .= " AND (employees.Inactive != 1 OR employees.Inactive IS NULL)";
            }
         } else {
            if ($inactive == 1) {
               $where .= " WHERE employees.Inactive = 1";
            } else {
               $where .= " WHERE (employees.Inactive != 1 OR employees.Inactive IS NULL)";
            }      
         }
      }
      //echo $where;
      if ($where_empinfo != "") {
         $ColumnRefId = $filter_table."RefId";
         $where .= " AND empinformation.".$ColumnRefId." = '$filter_refid' AND empinformation.EmployeesRefId = employees.RefId";
         $sql = "SELECT `EmployeesRefId`,`LastName`,`FirstName` FROM employees INNER JOIN empinformation ".$where;
      } else {
         $sql = "SELECT * FROM employees ".$where;
      }
      $sql = $sql." ORDER BY employees.LastName, employees.FirstName";
      //$rsEmployees = SelectEach("employees",$where." ORDER BY employees.LastName, employees.FirstName");
      $rsEmployees = mysqli_query($conn,$sql);
      if ($rsEmployees) {
         if (mysqli_num_rows($rsEmployees) > 0) {
            if (getvalue("hCtrl") != "") {
               echo '<script language="JavaScript" src="'.jsCtrl(getvalue("hCtrl")).'"></script>';
            }
            echo '
            <script language="JavaScript">
               $(document).ready(function () {
                  $(".Employees--").each(function (){
                     $(this).click(function () {
                        selectMe($(this).attr("refid"));
                     });
                  });
               });
            </script>';
            $count = 0;
            while ($rows = mysqli_fetch_array($rsEmployees)) {
               if ($rows["LastName"] != "" &&
                  $rows["FirstName"] != "") {
                  $count++;
                  if ($where_empinfo != "") {
                     $emprefid = $rows["EmployeesRefId"];
                  } else {
                     $emprefid = $rows["RefId"];   
                  }
                  
                  echo '<div class="Employees--" refid="'.$emprefid.'" style="padding:6px;">';
                  /*echo '<a href="javascript:void(0);">['.$rows["RefId"].'] '.strtoupper($rows["LastName"]).", ".strtoupper($rows["FirstName"]).'</a>';*/
                  echo $count.' - '.strtoupper($rows["LastName"]).", ".strtoupper($rows["FirstName"]);
                  echo '</div>';
                  echo "\n";
               }
            }
         } else {
            //echo "<div>Searching ...</div>";
            echo "<h4 style='color:red;'>NO RECORD FOUND !!!</h4>";
         }
      } else {
         //echo "<div>Searching ... $searchString</div>";
         echo "<h4 style='color:red;'>NO RECORD FOUND !!!</h4>";
      }
   }
   function fnCompEmpChangeToken() {
      include_once 'conn.e2e.php';
      $criteria  = " WHERE RefId = ".getvalue("EmpRefId");
      $criteria .= " AND pw = '".getvalue("cuToken")."'";
      $criteria .= " LIMIT 1";
      $recordSet = f_Find("employees",$criteria);
      if ($recordSet) {
         $t             = time();
         $date_today    = date("Y-m-d",$t);
         $curr_time     = date("H:i:s",$t);
         $trackingE  = "";
         $trackingE  = "`LastUpdateDate` = '$date_today',";
         $trackingE .= "`LastUpdateTime` = '$curr_time',";
         $trackingE .= "`LastUpdateBy` = '".getvalue("hUser")."',";
         $trackingE .= "`Data`= 'E'";

         $sql  = "";
         $sql .= "UPDATE `employees` SET pw = '".getvalue("newToken")."',";
         $sql .= "Remarks = 'Change PW by the User',".$trackingE." WHERE RefId = ".getvalue("EmpRefId");
         if ($conn->query($sql) === TRUE) {
            echo 'alert("Password successfully changed. You need to login again.");';
            echo '$(location).attr("href","idx.e2e.php?cpwsuccess=yes");';
         }
      } else {
         echo '$("#currenToken").val("");';
         echo '$("#currenToken").focus();';
         echo '$("[for=\'cuPW\']").show(100)';
      }
   }
   function fnLoadEmp201() {
      $rowEmployees = FindFirst("employees","WHERE RefId = ".getvalue("emprefid"),"*");
      if ($rowEmployees) {
         echo '$("#chkPersonalInfo").attr("refid","'.$rowEmployees["RefId"].'");';
         objSetValue("char_LastName",$rowEmployees["LastName"]);
         objSetValue("char_FirstName",$rowEmployees["FirstName"]);
         objSetValue("char_MiddleName",$rowEmployees["MiddleName"]);
         objSetValue("char_ExtName",$rowEmployees["ExtName"]);
         objSetValue("date_BirthDate",$rowEmployees["BirthDate"]);
         objSetValue("char_BirthPlace",$rowEmployees["BirthPlace"]);
         objSetValue("char_Sex",$rowEmployees["Sex"]);
         if ($rowEmployees["Sex"] == "F"){
            echo '$("#sexFemale").attr("checked",true);';
         } else if ($rowEmployees["Sex"] == "M") {
            echo '$("#sexMale").attr("checked",true);';
         } else {
            echo '$("#sexFemale").attr("checked",false);';
            echo '$("#sexMale").attr("checked",false);';
         }
         objSetValue("sint_isFilipino",$rowEmployees["isFilipino"]);
         if ($rowEmployees["isFilipino"] == 1){
            echo '$("#isFilipino").attr("checked",true);';
            echo '$("#CountryCitizen").hide();';
         } else if ($rowEmployees["isFilipino"] == 0) {
            echo '$("#isDual").attr("checked",true);';
            echo '$("#CountryCitizen").show();';
         } else {
            echo '$("#isFilipino").attr("checked",true);';
            echo '$("#CountryCitizen").hide();';
         }

         objSetValue("sint_isByBirthOrNatural",$rowEmployees["isByBirthOrNatural"]);
         objSetValue("char_CivilStatus",$rowEmployees["CivilStatus"]);
         objSetValue("char_Others",$rowEmployees["Others"]);

         if ($rowEmployees["CivilStatus"] == "Si") {
            echo '$("#EntrySpouseInfo").hide();';
         } else {
            echo '$("#EntrySpouseInfo").show();';
         }

         if ($rowEmployees["CivilStatus"] == "Ot") {
            echo '$("#other_cs").show();';
         } else {
            echo '$("#other_cs").hide();';
         }

         $PhilRefId = FindFirst("country","WHERE Name = 'Philippines'","RefId");
         if ($rowEmployees["ResiCountryRefId"] > 0) {
            $defValue_Resi = $rowEmployees["ResiCountryRefId"];
         } else {
            $defValue_Resi = $PhilRefId;
         }

         if ($rowEmployees["PermanentCountryRefId"] == 0) {
            $defValue_Perma = $PhilRefId;
         } else {
            $defValue_Perma = $rowEmployees["PermanentCountryRefId"];
         }
         objSetValue("deci_Height",$rowEmployees["Height"]);
         objSetValue("deci_Weight",$rowEmployees["Weight"]);
         objSetValue("sint_BloodTypeRefId",$rowEmployees["BloodTypeRefId"]);
         objSetValue("char_AgencyId",$rowEmployees["AgencyId"]);
         objSetValue("char_GovtIssuedID",$rowEmployees["GovtIssuedID"]);
         objSetValue("char_GovtIssuedIDNo",$rowEmployees["GovtIssuedIDNo"]);
         objSetValue("char_GovtIssuedIDPlace",$rowEmployees["GovtIssuedIDPlace"]);
         objSetValue("char_GovtIssuedIDDate",$rowEmployees["GovtIssuedIDDate"]);
         objSetValue("char_ResiHouseNo",$rowEmployees["ResiHouseNo"]);
         objSetValue("char_ResiStreet",$rowEmployees["ResiStreet"]);
         objSetValue("char_ResiBrgy",$rowEmployees["ResiBrgy"]);
         objSetValue("sint_ResiCountryRefId",$defValue_Resi);
         objSetValue("char_ResiSubd",$rowEmployees["ResiSubd"]);
         objSetValue("char_ResiAddZipCode",FFirstRefId("city",$rowEmployees["ResiAddCityRefId"],"ZipCode"));
         if ($rowEmployees["ResiAddProvinceRefId"]>0) {
            objSetValue("sint_ResiAddProvinceRefId",$rowEmployees["ResiAddProvinceRefId"]);
         }
         objSetValue("char_PermanentHouseNo",$rowEmployees["PermanentHouseNo"]);
         objSetValue("char_PermanentStreet",$rowEmployees["PermanentStreet"]);
         objSetValue("char_PermanentBrgy",$rowEmployees["PermanentBrgy"]);
         objSetValue("sint_PermanentCountryRefId",$defValue_Perma);
         if ($rowEmployees["PermanentAddProvinceRefId"]>0) {
            objSetValue("sint_PermanentAddProvinceRefId",$rowEmployees["PermanentAddProvinceRefId"]);
         }
         objSetValue("char_PermanentSubd",$rowEmployees["PermanentSubd"]);
         objSetValue("char_PermanentAddZipCode",FFirstRefId("city",$rowEmployees["PermanentAddCityRefId"],"ZipCode"));
         objSetValue("char_TelNo",$rowEmployees["TelNo"]);
         objSetValue("char_MobileNo",$rowEmployees["MobileNo"]);
         objSetValue("char_EmailAdd",$rowEmployees["EmailAdd"]);
         objSetValue("char_GSIS",$rowEmployees["GSIS"]);
         objSetValue("char_PAGIBIG",$rowEmployees["PAGIBIG"]);
         objSetValue("char_PHIC",$rowEmployees["PHIC"]);
         objSetValue("char_TIN",$rowEmployees["TIN"]);
         objSetValue("char_SSS",$rowEmployees["SSS"]);
         
         //objSetValue("sint_ResiAddCityRefId",$rowEmployees["ResiAddCityRefId"]);
         //objSetValue("sint_PermanentAddCityRefId",$rowEmployees["PermanentAddCityRefId"]);

         //FAMILY BACKGROUND
         // $Family = FindFirst("employeesfamily","WHERE EmployeesRefId = ".getvalue("emprefid"),"*");
         // if ($Family) {
         //    echo '$("#FamilyBG").attr("refid","'.$Family["RefId"].'");';
         //    setToolTip($Family,"famBG");
         //    objSetValue("FamilyRefId",$Family["RefId"]);
         //    objSetValue("char_SpouseLastName",$Family["SpouseLastName"]);
         //    objSetValue("char_SpouseFirstName",$Family["SpouseFirstName"]);
         //    objSetValue("char_SpouseExtName",$Family["SpouseExtName"]);
         //    objSetValue("char_SpouseMiddleName",$Family["SpouseMiddleName"]);
         //    //objSetValue("date_SpouseBirthDate",$Family["SpouseBirthDate"]);
         //    objSetValue("char_SpouseMobileNo",$Family["SpouseMobileNo"]);
         //    objSetValue("sint_OccupationsRefId",$Family["OccupationsRefId"]);
         //    objSetValue("char_EmployersName",$Family["EmployersName"]);
         //    objSetValue("char_BusinessAddress",$Family["BusinessAddress"]);

         //    objSetValue("char_FatherLastName",$Family["FatherLastName"]);
         //    objSetValue("char_FatherFirstName",$Family["FatherFirstName"]);
         //    objSetValue("char_FatherMiddleName",$Family["FatherMiddleName"]);
         //    objSetValue("char_FatherExtName",$Family["FatherExtName"]);
         //    objSetValue("char_MotherLastName",$Family["MotherLastName"]);
         //    objSetValue("char_MotherFirstName",$Family["MotherFirstName"]);
         //    objSetValue("char_MotherMiddleName",$Family["MotherMiddleName"]);
         //    objSetValue("char_MotherExtName",$Family["MotherExtName"]);
         // }

         
         


         //EDUCATIONAL BACKGROUND
         // $row = FindFirst("employeeseduc","WHERE LevelType = 1 AND EmployeesRefId =".getvalue("emprefid"),"*");
         // if ($row){
         //    setToolTip($row,"EntryElementary_1");
         //    echo '$("#Elementary_1").attr("refid","'.$row["RefId"].'");';
         //    objSetValue("elemRefId_1",$row["RefId"]);
         //    objSetValue("sint_SchoolsRefId_1_1",$row["SchoolsRefId"]);
         //    echo '$("#sint_SchoolsRefId_1_1").val("'.getRecord("schools",$row["SchoolsRefId"],"Name").'");'."\n";
         //    objSetValue("sint_YearGraduated_1_1",$row["YearGraduated"]);
         //    objSetValue("char_HighestGrade_1_1",$row["HighestGrade"]);
         //    objSetValue("sint_DateFrom_1_1",$row["DateFrom"]);
         //    objSetValue("sint_DateTo_1_1",$row["DateTo"]);
         //    objSetValue("char_Honors_1_1",$row["Honors"]);
         // }

         // $secondary = FindFirst("employeeseduc","WHERE LevelType = 2 AND EmployeesRefId =".getvalue("emprefid"),"*");
         // if ($secondary){
         //    if ($row["LastUpdateDate"] != "") {
         //       echo '$("#EntrySecondary_1").attr("title","Modified Date : '.$secondary["LastUpdateDate"]." - ".$secondary["LastUpdateTime"]." By ".$secondary["LastUpdateBy"].'");'."\n";
         //    }
         //    setToolTip($secondary,"EntrySecondary_1");
         //    echo '$("#Secondary_1").attr("refid","'.$secondary["RefId"].'");';
         //    objSetValue("secondaryRefId_1",$secondary["RefId"]);
         //    objSetValue("sint_SchoolsRefId_2_1",$secondary["SchoolsRefId"]);
         //    echo '$("#sint_SchoolsRefId_2_1").val("'.getRecord("schools",$secondary["SchoolsRefId"],"Name").'");'."\n";
         //    objSetValue("sint_YearGraduated_2_1",$secondary["YearGraduated"]);
         //    objSetValue("char_HighestGrade_2_1",$secondary["HighestGrade"]);
         //    objSetValue("sint_DateFrom_2_1",$secondary["DateFrom"]);
         //    objSetValue("sint_DateTo_2_1",$secondary["DateTo"]);
         //    objSetValue("char_Honors_2_1",$secondary["Honors"]);
         // }

         // $vocational = SelectEach("employeeseduc","WHERE LevelType = 3 AND EmployeesRefId =".getvalue("emprefid"));
         // if ($vocational) {
         //    $j = 0;
         //    while ($row = mysqli_fetch_assoc($vocational)){
         //       $j++;
         //       setToolTip($row,"EntryVocCourse_");
         //       echo '$("#VocCourse_3_'.$j.'").attr("refid","'.$row["RefId"].'");';
         //       objSetValue("VocationalRefId_3_".$j,$row["RefId"]);
         //       objSetValue("sint_SchoolsRefId_3_".$j,$row["SchoolsRefId"]);
         //       echo '$("#sint_SchoolsRefId_3_'.$j.'").val("'.getRecord("schools",$row["SchoolsRefId"],"Name").'");'."\n";
         //       objSetValue("sint_CourseRefId_3_".$j,$row["CourseRefId"]);
         //       objSetValue("sint_YearGraduated_3_".$j,$row["YearGraduated"]);
         //       objSetValue("char_HighestGrade_3_".$j,$row["HighestGrade"]);
         //       objSetValue("sint_DateFrom_3_".$j,$row["DateFrom"]);
         //       objSetValue("sint_DateTo_3_".$j,$row["DateTo"]);
         //       objSetValue("char_Honors_3_".$j,$row["Honors"]);
         //    }
         // }

         // $college = SelectEach("employeeseduc","WHERE LevelType = 4 AND EmployeesRefId =".getvalue("emprefid"));
         // if ($college){
         //    $j = 0;
         //    while ($row = mysqli_fetch_assoc($college)){
         //       $j++;
         //       setToolTip($row,"EntryCollege_".$j);
         //       echo '$("#College_4_'.$j.'").attr("refid","'.$row["RefId"].'");'."\n";
         //       objSetValue("collegeRefId_".$j,$row["RefId"]);
         //       objSetValue("sint_SchoolsRefId_4_".$j,$row["SchoolsRefId"]);
         //       echo '$("#sint_SchoolsRefId_4_'.$j.'").val("'.getRecord("schools",$row["SchoolsRefId"],"Name").'");'."\n";
         //       objSetValue("sint_CourseRefId_4_".$j,$row["CourseRefId"]);
         //       objSetValue("sint_YearGraduated_4_".$j,$row["YearGraduated"]);
         //       objSetValue("char_HighestGrade_4_".$j,$row["HighestGrade"]);
         //       objSetValue("sint_DateFrom_4_".$j,$row["DateFrom"]);
         //       objSetValue("sint_DateTo_4_".$j,$row["DateTo"]);
         //       objSetValue("char_Honors_4_".$j,$row["Honors"]);
         //    }
         // }

         // $gradStudies = SelectEach("employeeseduc","WHERE LevelType = 5 AND EmployeesRefId =".getvalue("emprefid"));
         // if ($gradStudies) {
         //    $j = 0;
         //    while ($row = mysqli_fetch_assoc($gradStudies)){
         //       $j++;
         //       setToolTip($row,"EntryGradStudies_".$j);
         //       echo '$("#GradStudies_5_'.$j.'").attr("refid","'.$row["RefId"].'");';
         //       objSetValue("GradStudiesRefId_5_".$j,$row["RefId"]);
         //       objSetValue("sint_SchoolsRefId_5_".$j,$row["SchoolsRefId"]);
         //       echo '$("#sint_SchoolsRefId_5_'.$j.'").val("'.getRecord("schools",$row["SchoolsRefId"],"Name").'");'."\n";
         //       objSetValue("sint_CourseRefId_5_".$j,$row["CourseRefId"]);
         //       objSetValue("sint_YearGraduated_5_".$j,$row["YearGraduated"]);
         //       objSetValue("char_HighestGrade_5_".$j,$row["HighestGrade"]);
         //       objSetValue("sint_DateFrom_5_".$j,$row["DateFrom"]);
         //       objSetValue("sint_DateTo_5_".$j,$row["DateTo"]);
         //       objSetValue("char_Honors_5_".$j,$row["Honors"]);
         //    }
         // }
         
         //ELIGIBILITY
         // $eligibiltyRow = SelectEach("employeeselegibility","WHERE EmployeesRefId =".getvalue("emprefid"));
         // if ($eligibiltyRow) {
         //    $j = 0;
         //    while ($row = mysqli_fetch_assoc($eligibiltyRow)){
         //       $j++;
         //       setToolTip($row,"EntryEligib_".$j);
         //       echo '$("#Eligib_'.$j.'").attr("refid","'.$row["RefId"].'");';
         //       objSetValue("eligibilityRefId_".$j,$row["RefId"]);
         //       objSetValue("sint_CareerServiceRefId_".$j,$row["CareerServiceRefId"]);
         //       objSetValue("sint_Rating_".$j,$row["Rating"]);
         //       objSetValue("date_ExamDate_".$j,$row["ExamDate"]);
         //       objSetValue("char_ExamPlace_".$j,$row["ExamPlace"]);
         //       objSetValue("char_LicenseNo_".$j,$row["LicenseNo"]);
         //       objSetValue("date_LicenseReleasedDate_".$j,$row["LicenseReleasedDate"]);
         //    }
         // }
         
         //WORK EXPERIENCE
         // $empWorkExperience = SelectEach("employeesworkexperience","WHERE EmployeesRefId =".getvalue("emprefid"));
         // if ($empWorkExperience) {
         //    $j = 0;
         //    while ($row = mysqli_fetch_assoc($empWorkExperience)){
         //       $j++;

         //       setToolTip($row,"EntryWorkExp_".$j);
         //       echo '$("[name=\'sint_PositionRefId_'.$j.'\'], [name=\'sint_AgencyRefId_'.$j.'\'], [name=\'sint_EmpStatusRefId_'.$j.'\']").empty();';
         //       populateDropDown("sint_PositionRefId_|sint_AgencyRefId_|sint_EmpStatusRefId_",$j,$row["isGovtService"]);
         //       echo '$("#exp_'.$j.'").attr("refid","'.$row["RefId"].'");';
         //       if ($row["WorkEndDate"] == "2999-12-31") {
         //          objSetValue("date_WorkEndDate_".$j,"Present");
         //       } else {
         //          objSetValue("date_WorkEndDate_".$j,$row["WorkEndDate"]);
         //       }
         //       setChkBox("sint_Present_".$j,$row["Present"],"name");
         //       objSetValue("workexperienceRefId_".$j,$row["RefId"]);
         //       objSetValue("date_WorkStartDate_".$j,$row["WorkStartDate"]);
         //       //objSetValue("date_WorkEndDate_".$j,$row["WorkEndDate"]);
         //       objSetValue("sint_PositionRefId_".$j,$row["PositionRefId"]);
         //       objSetValue("sint_AgencyRefId_".$j,$row["AgencyRefId"]);
         //       objSetValue("deci_SalaryAmount_".$j,number_format($row["SalaryAmount"],2));
         //       objSetValue("sint_SalaryGradeRefId_".$j,$row["SalaryGradeRefId"]);
         //       objSetValue("sint_JobGradeRefId_".$j,$row["JobGradeRefId"]);
         //       objSetValue("sint_EmpStatusRefId_".$j,$row["EmpStatusRefId"]);
         //       objSetValue("sint_StepIncrementRefId_".$j,$row["StepIncrementRefId"]);
         //       objSetValue("sint_isGovtService_".$j,$row["isGovtService"]);
         //       objSetValue("char_PayGroup_".$j,$row["PayGroup"]);
         //       objSetValue("sint_PayrateRefId_".$j,$row["PayrateRefId"]);
         //       objSetValue("char_LWOP_".$j,$row["LWOP"]);
         //       objSetValue("date_ExtDate_".$j,$row["ExtDate"]);
         //       objSetValue("date_SeparatedDate_".$j,$row["SeparatedDate"]);
         //       objSetValue("char_Reason_".$j,$row["Reason"]);
         //       objSetValue("char_Remarks_".$j,$row["Remarks"]);
         //       objSetValue("sint_isServiceRecord_".$j,$row["isServiceRecord"]);
         //    }
         // }
         

         //VOLUNTARY WORK
         // $empVoluntaryWork = SelectEach("employeesvoluntary","WHERE EmployeesRefId =".getvalue("emprefid"));
         // if ($empVoluntaryWork) {
         //    $j = 0;
         //    while ($row = mysqli_fetch_assoc($empVoluntaryWork)){
         //       $j++;
         //       setToolTip($row,"EntryVolWork_".$j);
         //       if ($row["EndDate"] == "2999-12-31") {
         //          objSetValue("date_EndDate_".$j,"Present");
         //       } else {
         //          objSetValue("date_EndDate_".$j,$row["EndDate"]);
         //       }
         //       setChkBox("sint_Present_vw_".$j,$row["Present"],"name");

         //       echo '$("#VolWork_'.$j.'").attr("refid","'.$row["RefId"].'");';
         //       objSetValue("voluntaryRefId_".$j,$row["RefId"]);
         //       objSetValue("bint_OrganizationRefId_".$j,$row["OrganizationRefId"]);
         //       objSetValue("date_StartDate_".$j,$row["StartDate"]);
         //       //objSetValue("date_EndDate_".$j,$row["EndDate"]);
         //       objSetValue("sint_NumofHrs_".$j,$row["NumofHrs"]);
         //       objSetValue("char_WorksNature_".$j,$row["WorksNature"]);
         //    }
         // }
         

         //TRAINING PROGRAMS
         // $empTraining = SelectEach("employeestraining","WHERE EmployeesRefId =".getvalue("emprefid"));
         // if ($empTraining) {
         //    $j = 0;
         //    while ($row = mysqli_fetch_assoc($empTraining)){
         //       $j++;
         //       setToolTip($row,"EntryTrainingProg_".$j);
         //       if ($row["EndDate"] == "2999-12-31") {
         //          objSetValue("date_EndDate_".$j."_TrainProg","Present");
         //       } else {
         //          objSetValue("date_EndDate_".$j."_TrainProg",$row["EndDate"]);
         //       }
         //       setChkBox("sint_Present_tp_".$j,$row["Present"],"name");

         //       echo '$("#TrainProg_'.$j.'").attr("refid","'.$row["RefId"].'");';
         //       objSetValue("trainingRefId_".$j,$row["RefId"]);
         //       objSetValue("bint_SeminarsRefId_".$j,$row["SeminarsRefId"]);
         //       objSetValue("date_StartDate_".$j."_TrainProg",$row["StartDate"]);
         //       //objSetValue("date_EndDate_".$j."_TrainProg",$row["EndDate"]);
         //       objSetValue("sint_NumofHrs_".$j."_TrainProg",$row["NumofHrs"]);
         //       objSetValue("bint_SponsorRefId_".$j,$row["SponsorRefId"]);
         //       //objSetValue("bint_SeminarPlaceRefId_".$j,$row["SeminarPlaceRefId"]);
         //       objSetValue("char_SeminarPlace_".$j,$row["SeminarPlace"]);
         //       objSetValue("bint_SeminarTypeRefId_".$j,$row["SeminarTypeRefId"]);
         //       objSetValue("sint_SeminarClassRefId_".$j,$row["SeminarClassRefId"]);
         //       objSetValue("char_Description_".$j,getRecord("seminars",$row["SeminarsRefId"],"Description"));
         //    }
         // }
         

         //OTHER INFO
         // $empotherinfo = SelectEach("employeesotherinfo","WHERE EmployeesRefId =".getvalue("emprefid"));
         // if ($empotherinfo) {
         //    $j = 0;
         //    while ($row = mysqli_fetch_assoc($empotherinfo)){
         //       $j++;
         //       setToolTip($row,"EntryOtherInfo_".$j);
         //       echo '$("#OtherInfo_'.$j.'").attr("refid","'.$row["RefId"].'");';
         //       objSetValue("otherinfoRefId_".$j,$row["RefId"]);
         //       objSetValue("char_Skills_".$j,$row["Skills"]);
         //       objSetValue("char_Recognition_".$j,$row["Recognition"]);
         //       objSetValue("char_Affiliates_".$j,$row["Affiliates"]);
         //    }
         // }
         

         //REFERENCE

         // $empreference = SelectEach("employeesreference","WHERE EmployeesRefId =".getvalue("emprefid"));
         // if ($empreference) {
         //    $j = 0;
         //    while ($row = mysqli_fetch_assoc($empreference)){
         //       $j++;
         //       setToolTip($row,"EntryRef_".$j);
         //       echo '$("#Ref_'.$j.'").attr("refid","'.$row["RefId"].'");';
         //       objSetValue("referenceRefId_".$j,$row["RefId"]);
         //       objSetValue("char_Name_".$j,$row["Name"]);
         //       objSetValue("char_Address_".$j,$row["Address"]);
         //       objSetValue("char_ContactNo_".$j,$row["ContactNo"]);
         //    }
         // }
         

         //PDSQ
         $emppdsq = FindFirst("employeespdsq","WHERE EmployeesRefId =".getvalue("emprefid"),"*");
         if ($emppdsq){
            //setToolTip($row,"EntryPDSQ");
            objSetValue("pdsqRefId",$emppdsq["RefId"]);
            objSetValue("chkPDSQ",$emppdsq["RefId"]);

            echo '$("[name*=\'Ans_\']").prop("disabled",true);'."\n";
            if ($emppdsq["Q1a"] == 1){
               echo '$("#YAns_Q1a").attr("checked",true);'."\n";
            }
            objSetValue("sint_Q1a",$emppdsq["Q1a"]);
            objSetValue("char_Q1aexp",$emppdsq["Q1aexp"]);

            if ($emppdsq["Q1b"] == 1){
               echo '$("#YAns_Q1b").attr("checked",true);'."\n";
            }
            objSetValue("sint_Q1b",$emppdsq["Q1b"]);
            objSetValue("char_Q1bexp",$emppdsq["Q1bexp"]);

            if ($emppdsq["Q2a"] == 1){
               echo '$("#YAns_Q2a").attr("checked",true);'."\n";
            }
            objSetValue("sint_Q2a",$emppdsq["Q2a"]);
            objSetValue("char_Q2aexp",$emppdsq["Q2aexp"]);

            if ($emppdsq["Q2b"] == 1){
               echo '$("#YAns_Q2b").attr("checked",true);'."\n";
            }
            objSetValue("sint_Q2b",$emppdsq["Q2b"]);
            objSetValue("char_Q2bexp",$emppdsq["Q2bexp"]);
            objSetValue("date_Q2DateFiled",$emppdsq["Q2DateFiled"]);
            objSetValue("char_Q2CaseStatus",$emppdsq["Q2CaseStatus"]);

            if ($emppdsq["Q3a"] == 1){
               echo '$("#YAns_Q3a").attr("checked",true);'."\n";
            }
            objSetValue("sint_Q3a",$emppdsq["Q3a"]);
            objSetValue("char_Q3aexp",$emppdsq["Q3aexp"]);

            if ($emppdsq["Q4a"] == 1){
               echo '$("#YAns_Q4a").attr("checked",true);'."\n";
            }
            objSetValue("sint_Q4a",$emppdsq["Q4a"]);
            objSetValue("char_Q4aexp",$emppdsq["Q4aexp"]);


            if ($emppdsq["Q5a"] == 1){
               echo '$("#YAns_Q5a").attr("checked",true);'."\n";
            }
            objSetValue("sint_Q5a",$emppdsq["Q5a"]);
            objSetValue("char_Q5aexp",$emppdsq["Q5aexp"]);


            if ($emppdsq["Q5b"] == 1){
               echo '$("#YAns_Q5b").attr("checked",true);'."\n";
            }
            objSetValue("sint_Q5b",$emppdsq["Q5b"]);
            objSetValue("char_Q5bexp",$emppdsq["Q5bexp"]);


            if ($emppdsq["Q6a"] == 1){
               echo '$("#YAns_Q6a").attr("checked",true);'."\n";
            }
            objSetValue("sint_Q6a",$emppdsq["Q6a"]);
            objSetValue("char_Q6aexp",$emppdsq["Q6aexp"]);


            if ($emppdsq["Q7a"] == 1){
               echo '$("#YAns_Q7a").attr("checked",true);'."\n";
            }
            objSetValue("sint_Q7a",$emppdsq["Q7a"]);
            objSetValue("char_Q7aexp",$emppdsq["Q7aexp"]);


            if ($emppdsq["Q7b"] == 1){
               echo '$("#YAns_Q7b").attr("checked",true);'."\n";
            }
            objSetValue("sint_Q7b",$emppdsq["Q7b"]);
            objSetValue("char_Q7bexp",$emppdsq["Q7bexp"]);


            if ($emppdsq["Q7c"] == 1){
               echo '$("#YAns_Q7c").attr("checked",true);'."\n";
            }
            objSetValue("char_Q7cexp",$emppdsq["Q7cexp"]);
            objSetValue("sint_Q7c",$emppdsq["Q7c"]);
         }
         /*-----*/
         //echo 'document.getElementById("overlay").style.display = "none";';
      }
   }
   function fnload_tab() {
      $emprefid   = getvalue("emprefid");
      $idx        = getvalue("idx");
      switch ($idx) {
         case '2':
            load_emp_child($emprefid);
            break;
         case '3':
            load_emp_familybg($emprefid);
            break;
         case '4':
            load_emp_educ($emprefid);
            break;
         case '5':
            load_emp_eligibility($emprefid);
            break;
         case '6':
            load_emp_work_exp($emprefid);
            break;
         case '7':
            load_emp_voluntary($emprefid);
            break;
         case '8':
            load_emp_lnd($emprefid);
            break;
         case '9':
            load_emp_other_info($emprefid);
            break;
         case '11':
            load_emp_reference($emprefid);
            break;
      }
   }
   function load_emp_reference($emprefid) {
      $empreference = SelectEach("employeesreference","WHERE EmployeesRefId =".$emprefid);
      if ($empreference) {
         $j = 0;
         while ($row = mysqli_fetch_assoc($empreference)){
            $j++;
            setToolTip($row,"EntryRef_".$j);
            echo '$("#Ref_'.$j.'").attr("refid","'.$row["RefId"].'");';
            objSetValue("referenceRefId_".$j,$row["RefId"]);
            objSetValue("char_Name_".$j,$row["Name"]);
            objSetValue("char_Address_".$j,$row["Address"]);
            objSetValue("char_ContactNo_".$j,$row["ContactNo"]);
         }
      }
   }

   function load_emp_other_info($emprefid) {
      $empotherinfo = SelectEach("employeesotherinfo","WHERE EmployeesRefId =".$emprefid);
      if ($empotherinfo) {
         $j = 0;
         while ($row = mysqli_fetch_assoc($empotherinfo)){
            $j++;
            setToolTip($row,"EntryOtherInfo_".$j);
            echo '$("#OtherInfo_'.$j.'").attr("refid","'.$row["RefId"].'");';
            objSetValue("otherinfoRefId_".$j,$row["RefId"]);
            objSetValue("char_Skills_".$j,$row["Skills"]);
            objSetValue("char_Recognition_".$j,$row["Recognition"]);
            objSetValue("char_Affiliates_".$j,$row["Affiliates"]);
         }
      }
   }


   function load_emp_lnd ($emprefid) {
      $cid = FindFirst("employees","WHERE RefId = '$emprefid'","CompanyRefId");
      if ($cid == 21) {
         $empTraining = SelectEach("employeestraining","WHERE EmployeesRefId =".$emprefid." ORDER BY StartDate DESC, RefId DESC LIMIT 30");
      } else {
         $empTraining = SelectEach("employeestraining","WHERE EmployeesRefId =".$emprefid);   
      }
      
      if ($empTraining) {
         $j = 0;
         while ($row = mysqli_fetch_assoc($empTraining)){
            $j++;
            setToolTip($row,"EntryTrainingProg_".$j);
            if ($row["EndDate"] == "2999-12-31") {
               objSetValue("date_EndDate_".$j."_TrainProg","Present");
            } else {
               objSetValue("date_EndDate_".$j."_TrainProg",$row["EndDate"]);
            }
            setChkBox("sint_Present_tp_".$j,$row["Present"],"name");

            echo '$("#TrainProg_'.$j.'").attr("refid","'.$row["RefId"].'");';
            objSetValue("trainingRefId_".$j,$row["RefId"]);
            objSetValue("bint_SeminarsRefId_".$j,$row["SeminarsRefId"]);
            objSetValue("date_StartDate_".$j."_TrainProg",$row["StartDate"]);
            //objSetValue("date_EndDate_".$j."_TrainProg",$row["EndDate"]);
            objSetValue("sint_NumofHrs_".$j."_TrainProg",$row["NumofHrs"]);
            objSetValue("bint_SponsorRefId_".$j,$row["SponsorRefId"]);
            //objSetValue("bint_SeminarPlaceRefId_".$j,$row["SeminarPlaceRefId"]);
            objSetValue("char_SeminarPlace_".$j,$row["SeminarPlace"]);
            objSetValue("bint_SeminarTypeRefId_".$j,$row["SeminarTypeRefId"]);
            objSetValue("sint_SeminarClassRefId_".$j,$row["SeminarClassRefId"]);
            objSetValue("char_Description_".$j,getRecord("seminars",$row["SeminarsRefId"],"Description"));
         }
      }
   }

   function load_emp_work_exp ($emprefid) {
      $empWorkExperience = SelectEach("employeesworkexperience","WHERE EmployeesRefId =".$emprefid);
      if ($empWorkExperience) {
         $j = 0;
         while ($row = mysqli_fetch_assoc($empWorkExperience)){
            $j++;

            setToolTip($row,"EntryWorkExp_".$j);
            //echo '$("[name=\'sint_PositionRefId_'.$j.'\'], [name=\'sint_AgencyRefId_'.$j.'\'], [name=\'sint_EmpStatusRefId_'.$j.'\']").empty();';
            //populateDropDown("sint_PositionRefId_|sint_AgencyRefId_|sint_EmpStatusRefId_",$j,$row["isGovtService"]);
            echo '$("#exp_'.$j.'").attr("refid","'.$row["RefId"].'");';
            if ($row["WorkEndDate"] == "2999-12-31") {
               objSetValue("date_WorkEndDate_".$j,"Present");
            } else {
               objSetValue("date_WorkEndDate_".$j,$row["WorkEndDate"]);
            }


            setChkBox("sint_Present_".$j,$row["Present"],"name");
            objSetValue("workexperienceRefId_".$j,$row["RefId"]);
            objSetValue("date_WorkStartDate_".$j,$row["WorkStartDate"]);
            objSetValue("sint_PositionRefId_".$j,$row["PositionRefId"]);
            objSetValue("sint_AgencyRefId_".$j,$row["AgencyRefId"]);
            objSetValue("deci_SalaryAmount_".$j,number_format($row["SalaryAmount"],2));
            objSetValue("sint_SalaryGradeRefId_".$j,$row["SalaryGradeRefId"]);
            objSetValue("sint_JobGradeRefId_".$j,$row["JobGradeRefId"]);
            objSetValue("sint_EmpStatusRefId_".$j,$row["EmpStatusRefId"]);
            objSetValue("sint_StepIncrementRefId_".$j,$row["StepIncrementRefId"]);
            objSetValue("sint_isGovtService_".$j,$row["isGovtService"]);
            objSetValue("char_PayGroup_".$j,$row["PayGroup"]);
            objSetValue("sint_PayrateRefId_".$j,$row["PayrateRefId"]);
            objSetValue("char_LWOP_".$j,$row["LWOP"]);
            objSetValue("date_ExtDate_".$j,$row["ExtDate"]);
            objSetValue("date_SeparatedDate_".$j,$row["SeparatedDate"]);
            objSetValue("char_Reason_".$j,$row["Reason"]);
            objSetValue("char_Remarks_".$j,$row["Remarks"]);
            objSetValue("sint_isServiceRecord_".$j,$row["isServiceRecord"]);
         }
      }
   }

   function load_emp_voluntary ($emprefid) {
      $empVoluntaryWork = SelectEach("employeesvoluntary","WHERE EmployeesRefId =".$emprefid);
      if ($empVoluntaryWork) {
         $j = 0;
         while ($row = mysqli_fetch_assoc($empVoluntaryWork)){
            $j++;
            setToolTip($row,"EntryVolWork_".$j);
            if ($row["EndDate"] == "2999-12-31") {
               objSetValue("date_EndDate_".$j,"Present");
            } else {
               objSetValue("date_EndDate_".$j,$row["EndDate"]);
            }
            setChkBox("sint_Present_vw_".$j,$row["Present"],"name");

            echo '$("#VolWork_'.$j.'").attr("refid","'.$row["RefId"].'");';
            objSetValue("voluntaryRefId_".$j,$row["RefId"]);
            objSetValue("bint_OrganizationRefId_".$j,$row["OrganizationRefId"]);
            objSetValue("date_StartDate_".$j,$row["StartDate"]);
            //objSetValue("date_EndDate_".$j,$row["EndDate"]);
            objSetValue("sint_NumofHrs_".$j,$row["NumofHrs"]);
            objSetValue("char_WorksNature_".$j,$row["WorksNature"]);
         }
      }
   }

   function load_emp_eligibility ($emprefid) {
      $eligibiltyRow = SelectEach("employeeselegibility","WHERE EmployeesRefId =".$emprefid);
      if ($eligibiltyRow) {
         $j = 0;
         while ($row = mysqli_fetch_assoc($eligibiltyRow)){
            $j++;
            setToolTip($row,"EntryEligib_".$j);
            echo '$("#Eligib_'.$j.'").attr("refid","'.$row["RefId"].'");';
            objSetValue("eligibilityRefId_".$j,$row["RefId"]);
            objSetValue("sint_CareerServiceRefId_".$j,$row["CareerServiceRefId"]);
            objSetValue("sint_Rating_".$j,$row["Rating"]);
            objSetValue("date_ExamDate_".$j,$row["ExamDate"]);
            objSetValue("char_ExamPlace_".$j,$row["ExamPlace"]);
            objSetValue("char_LicenseNo_".$j,$row["LicenseNo"]);
            objSetValue("date_LicenseReleasedDate_".$j,$row["LicenseReleasedDate"]);
         }
      }
   }

   function load_emp_educ ($emprefid) {
      $row = FindFirst("employeeseduc","WHERE LevelType = 1 AND EmployeesRefId =".$emprefid,"*");
      if ($row){
         setToolTip($row,"EntryElementary_1");
         echo '$("#Elementary_1").attr("refid","'.$row["RefId"].'");';
         objSetValue("elemRefId_1",$row["RefId"]);
         objSetValue("sint_SchoolsRefId_1_1",$row["SchoolsRefId"]);
         echo '$("#sint_SchoolsRefId_1_1").val("'.getRecord("schools",$row["SchoolsRefId"],"Name").'");'."\n";
         objSetValue("sint_YearGraduated_1_1",$row["YearGraduated"]);
         objSetValue("char_HighestGrade_1_1",$row["HighestGrade"]);
         objSetValue("sint_DateFrom_1_1",$row["DateFrom"]);
         objSetValue("sint_DateTo_1_1",$row["DateTo"]);
         objSetValue("char_Honors_1_1",$row["Honors"]);
      }

      $secondary = FindFirst("employeeseduc","WHERE LevelType = 2 AND EmployeesRefId =".$emprefid,"*");
      if ($secondary){
         if ($row["LastUpdateDate"] != "") {
            echo '$("#EntrySecondary_1").attr("title","Modified Date : '.$secondary["LastUpdateDate"]." - ".$secondary["LastUpdateTime"]." By ".$secondary["LastUpdateBy"].'");'."\n";
         }
         setToolTip($secondary,"EntrySecondary_1");
         echo '$("#Secondary_1").attr("refid","'.$secondary["RefId"].'");';
         objSetValue("secondaryRefId_1",$secondary["RefId"]);
         objSetValue("sint_SchoolsRefId_2_1",$secondary["SchoolsRefId"]);
         echo '$("#sint_SchoolsRefId_2_1").val("'.getRecord("schools",$secondary["SchoolsRefId"],"Name").'");'."\n";
         objSetValue("sint_YearGraduated_2_1",$secondary["YearGraduated"]);
         objSetValue("char_HighestGrade_2_1",$secondary["HighestGrade"]);
         objSetValue("sint_DateFrom_2_1",$secondary["DateFrom"]);
         objSetValue("sint_DateTo_2_1",$secondary["DateTo"]);
         objSetValue("char_Honors_2_1",$secondary["Honors"]);
      }

      $vocational = SelectEach("employeeseduc","WHERE LevelType = 3 AND EmployeesRefId =".$emprefid);
      if ($vocational) {
         $j = 0;
         while ($row = mysqli_fetch_assoc($vocational)){
            $j++;
            setToolTip($row,"EntryVocCourse_");
            echo '$("#VocCourse_3_'.$j.'").attr("refid","'.$row["RefId"].'");';
            objSetValue("VocationalRefId_3_".$j,$row["RefId"]);
            objSetValue("sint_SchoolsRefId_3_".$j,$row["SchoolsRefId"]);
            echo '$("#sint_SchoolsRefId_3_'.$j.'").val("'.getRecord("schools",$row["SchoolsRefId"],"Name").'");'."\n";
            objSetValue("sint_CourseRefId_3_".$j,$row["CourseRefId"]);
            objSetValue("sint_YearGraduated_3_".$j,$row["YearGraduated"]);
            objSetValue("char_HighestGrade_3_".$j,$row["HighestGrade"]);
            objSetValue("sint_DateFrom_3_".$j,$row["DateFrom"]);
            objSetValue("sint_DateTo_3_".$j,$row["DateTo"]);
            objSetValue("char_Honors_3_".$j,$row["Honors"]);
         }
      }

      $college = SelectEach("employeeseduc","WHERE LevelType = 4 AND EmployeesRefId =".$emprefid);
      if ($college){
         $j = 0;
         while ($row = mysqli_fetch_assoc($college)){
            $j++;
            setToolTip($row,"EntryCollege_".$j);
            echo '$("#College_4_'.$j.'").attr("refid","'.$row["RefId"].'");'."\n";
            objSetValue("collegeRefId_".$j,$row["RefId"]);
            objSetValue("sint_SchoolsRefId_4_".$j,$row["SchoolsRefId"]);
            echo '$("#sint_SchoolsRefId_4_'.$j.'").val("'.getRecord("schools",$row["SchoolsRefId"],"Name").'");'."\n";
            objSetValue("sint_CourseRefId_4_".$j,$row["CourseRefId"]);
            objSetValue("sint_YearGraduated_4_".$j,$row["YearGraduated"]);
            objSetValue("char_HighestGrade_4_".$j,$row["HighestGrade"]);
            objSetValue("sint_DateFrom_4_".$j,$row["DateFrom"]);
            objSetValue("sint_DateTo_4_".$j,$row["DateTo"]);
            objSetValue("char_Honors_4_".$j,$row["Honors"]);
         }
      }

      $gradStudies = SelectEach("employeeseduc","WHERE LevelType = 5 AND EmployeesRefId =".$emprefid);
      if ($gradStudies) {
         $j = 0;
         while ($row = mysqli_fetch_assoc($gradStudies)){
            $j++;
            setToolTip($row,"EntryGradStudies_".$j);
            echo '$("#GradStudies_5_'.$j.'").attr("refid","'.$row["RefId"].'");';
            objSetValue("GradStudiesRefId_5_".$j,$row["RefId"]);
            objSetValue("sint_SchoolsRefId_5_".$j,$row["SchoolsRefId"]);
            echo '$("#sint_SchoolsRefId_5_'.$j.'").val("'.getRecord("schools",$row["SchoolsRefId"],"Name").'");'."\n";
            objSetValue("sint_CourseRefId_5_".$j,$row["CourseRefId"]);
            objSetValue("sint_YearGraduated_5_".$j,$row["YearGraduated"]);
            objSetValue("char_HighestGrade_5_".$j,$row["HighestGrade"]);
            objSetValue("sint_DateFrom_5_".$j,$row["DateFrom"]);
            objSetValue("sint_DateTo_5_".$j,$row["DateTo"]);
            objSetValue("char_Honors_5_".$j,$row["Honors"]);
         }
      }
   }
   function load_emp_familybg ($emprefid) {
      $Family = FindFirst("employeesfamily","WHERE EmployeesRefId = ".$emprefid,"*");
      if ($Family) {
         echo '$("#FamilyBG").attr("refid","'.$Family["RefId"].'");';
         setToolTip($Family,"famBG");
         objSetValue("FamilyRefId",$Family["RefId"]);
         objSetValue("char_SpouseLastName",$Family["SpouseLastName"]);
         objSetValue("char_SpouseFirstName",$Family["SpouseFirstName"]);
         objSetValue("char_SpouseExtName",$Family["SpouseExtName"]);
         objSetValue("char_SpouseMiddleName",$Family["SpouseMiddleName"]);
         //objSetValue("date_SpouseBirthDate",$Family["SpouseBirthDate"]);
         objSetValue("char_SpouseMobileNo",$Family["SpouseMobileNo"]);
         objSetValue("sint_OccupationsRefId",$Family["OccupationsRefId"]);
         objSetValue("char_EmployersName",$Family["EmployersName"]);
         objSetValue("char_BusinessAddress",$Family["BusinessAddress"]);

         objSetValue("char_FatherLastName",$Family["FatherLastName"]);
         objSetValue("char_FatherFirstName",$Family["FatherFirstName"]);
         objSetValue("char_FatherMiddleName",$Family["FatherMiddleName"]);
         objSetValue("char_FatherExtName",$Family["FatherExtName"]);
         objSetValue("char_MotherLastName",$Family["MotherLastName"]);
         objSetValue("char_MotherFirstName",$Family["MotherFirstName"]);
         objSetValue("char_MotherMiddleName",$Family["MotherMiddleName"]);
         objSetValue("char_MotherExtName",$Family["MotherExtName"]);
      }
   }



   function load_emp_child ($emprefid) {
      $Children = SelectEach("employeeschild","WHERE EmployeesRefId = ".$emprefid." ORDER BY BirthDate");
      if ($Children) {
         $j = 0;
         while ($row = mysqli_fetch_assoc($Children)){
            $j++;
            setToolTip($row,"EntryChildrens_".$j);
            echo '$("#childrens_'.$j.'").attr("refid","'.$row["RefId"].'");';
            objSetValue("ChildrenRefId_".$j,$row["RefId"]);
            objSetValue("char_FullName_".$j,$row["FullName"]);
            objSetValue("char_Gender_".$j,$row["Gender"]);
            objSetValue("date_BirthDate_".$j,$row["BirthDate"]);
         }
      }
   }
   function setToolTip($row,$objId) {
      if ($row["LastUpdateDate"] != "") {
         echo '$("#'.$objId.'").attr("title","Modified Date : '.$row["LastUpdateDate"]." - ".$row["LastUpdateTime"]." By ".$row["LastUpdateBy"].'");'."\n";
      }
   } 
   function setChkBox($obj,$isChk,$by) {
      if ($isChk == 1) $bool = "true";
      else if ($isChk == 0) $bool = "false";
      if ($by == "id") {
         echo '$("#'.$obj.'").attr("checked",'.$bool.');'."\n";
      }
      else if ($by == "name") {
         echo '$("[name=\''.$obj.'\']").attr("checked",'.$bool.');'."\n";
      }
      //echo "setValueByName(objName,objValue);"
   }
   function fnUpdate201() {
      if (getvalue("hUserGroup") == "COMPEMP") {
         $emprefid = getvalue("hEmpRefId");
      } else {
         $emprefid = getvalue("hRefId");
      }
      if ($emprefid > 0) {
            if (getvalue("chkPersonalInfo") == "true") {
               $empField = "";
               $empField .= "LastName = '".setDefault("char_LastName")."', ";
               $empField .= "MiddleName = '".setDefault("char_MiddleName")."', ";
               $empField .= "FirstName = '".setDefault("char_FirstName")."', ";
               $empField .= "ExtName = '".setDefault("char_ExtName")."', ";
               $empField .= "BirthDate = '".setDefault("date_BirthDate")."', ";
               $empField .= "BirthPlace = '".setDefault("char_BirthPlace")."', ";
               $empField .= "Sex = '".setDefault("char_Sex")."', ";
               $empField .= "isFilipino = '".setDefault("sint_isFilipino")."', ";
               $empField .= "isByBirthOrNatural = '".setDefault("sint_isByBirthOrNatural")."', ";
               $empField .= "CountryCitizenRefId = '".setDefault("sint_CountryCitizenRefId")."', ";
               $empField .= "CivilStatus = '".setDefault("char_CivilStatus")."', ";
               $empField .= "Others = '".setDefault("char_Others")."', ";
               $empField .= "Height = '".setDefault("deci_Height")."', ";
               $empField .= "Weight = '".setDefault("deci_Weight")."', ";
               $empField .= "BloodTypeRefId = '".setDefault("sint_BloodTypeRefId")."', ";
               $empField .= "AgencyId = '".setDefault("char_AgencyId")."', ";
               $empField .= "GovtIssuedID = '".setDefault("char_GovtIssuedID")."', ";
               $empField .= "GovtIssuedIDNo = '".setDefault("char_GovtIssuedIDNo")."', ";
               $empField .= "GovtIssuedIDPlace = '".setDefault("char_GovtIssuedIDPlace")."', ";
               if (getvalue("char_GovtIssuedIDDate") == "") {
                  //$empField .= "GovtIssuedIDDate = '".setDefault("char_GovtIssuedIDDate")."', ";
                  $empField .= "GovtIssuedIDDate = '1901-01-01', ";
               } else {
                  $empField .= "GovtIssuedIDDate = '".setDefault("char_GovtIssuedIDDate")."', ";
               }
               $empField .= "ResiHouseNo = '".setDefault("char_ResiHouseNo")."', ";
               $empField .= "ResiStreet = '".setDefault("char_ResiStreet")."', ";
               $empField .= "ResiBrgy = '".setDefault("char_ResiBrgy")."', ";
               $empField .= "ResiCountryRefId = '".setDefault("sint_ResiCountryRefId")."', ";
               $empField .= "ResiAddProvinceRefId = '".setDefault("sint_ResiAddProvinceRefId")."', ";
               $empField .= "ResiAddCityRefId = '".setDefault("sint_ResiAddCityRefId")."', ";
               $empField .= "ResiSubd = '".setDefault("char_ResiSubd")."', ";
               $empField .= "PermanentHouseNo = '".setDefault("char_PermanentHouseNo")."', ";
               $empField .= "PermanentStreet = '".setDefault("char_PermanentStreet")."', ";
               $empField .= "PermanentBrgy = '".setDefault("char_PermanentBrgy")."', ";
               $empField .= "PermanentCountryRefId = '".setDefault("sint_PermanentCountryRefId")."', ";
               $empField .= "PermanentAddProvinceRefId = '".setDefault("sint_PermanentAddProvinceRefId")."', ";
               $empField .= "PermanentAddCityRefId = '".setDefault("sint_PermanentAddCityRefId")."', ";
               $empField .= "PermanentSubd = '".setDefault("char_PermanentSubd")."', ";
               $empField .= "TelNo = '".setDefault("char_TelNo")."', ";
               $empField .= "MobileNo = '".setDefault("char_MobileNo")."', ";
               $empField .= "EmailAdd = '".setDefault("char_EmailAdd")."', ";
               $empField .= "GSIS = '".setDefault("char_GSIS")."', ";
               $empField .= "PAGIBIG = '".setDefault("char_PAGIBIG")."', ";
               $empField .= "PHIC = '".setDefault("char_PHIC")."', ";
               $empField .= "TIN = '".setDefault("char_TIN")."', ";
               $empField .= "SSS = '".setDefault("char_SSS")."', ";
               if (getvalue("char_AgencyId") != "") {
                  $empField .= "Inactive = '0', ";
               }
               //echo $empField;
               f_SaveRecord("EDITSAVE","employees",$empField,$emprefid);
            }
      } else {
         //echo "alert('No Employees RefId Assigned !!!');";
         $flds = "";
         $vals = "";
         $flds .= "LastName,MiddleName,FirstName,ExtName,BirthDate,BirthPlace,Sex,isFilipino,isByBirthOrNatural,";
         $flds .= "CountryCitizenRefId,CivilStatus,Height,Weight,BloodTypeRefId,AgencyId,GovtIssuedID,";
         $flds .= "GovtIssuedIDNo,GovtIssuedIDPlace,ResiHouseNo,ResiStreet,ResiBrgy,ResiCountryRefId,";
         $flds .= "ResiAddProvinceRefId,ResiAddCityRefId,ResiSubd,PermanentHouseNo,PermanentStreet,";
         $flds .= "PermanentBrgy,PermanentCountryRefId,PermanentAddProvinceRefId,PermanentAddCityRefId,";
         $flds .= "PermanentSubd,TelNo,MobileNo,EmailAdd,GSIS,PAGIBIG,PHIC,TIN,SSS,Others,";
         $vals .= "'".setDefault("char_LastName")."','".setDefault("char_MiddleName")."','".setDefault("char_FirstName")."',";
         $vals .= "'".setDefault("char_ExtName")."','".setDefault("date_BirthDate")."',";
         $vals .= "'".setDefault("char_BirthPlace")."','".setDefault("char_Sex")."',";
         $vals .= "'".setDefault("sint_isFilipino")."','".setDefault("sint_isByBirthOrNatural")."',";
         $vals .= "'".setDefault("sint_CountryCitizenRefId")."','".setDefault("char_CivilStatus")."',";
         $vals .= "'".setDefault("deci_Height")."','".setDefault("deci_Weight")."',";
         $vals .= "'".setDefault("sint_BloodTypeRefId")."','".setDefault("char_AgencyId")."',";
         $vals .= "'".setDefault("char_GovtIssuedID")."','".setDefault("char_GovtIssuedIDNo")."',";
         $vals .= "'".setDefault("char_GovtIssuedIDPlace")."', '".setDefault("char_ResiHouseNo")."',";
         $vals .= "'".setDefault("char_ResiStreet")."','".setDefault("char_ResiBrgy")."',";
         $vals .= "'".setDefault("sint_ResiCountryRefId")."','".setDefault("sint_ResiAddProvinceRefId")."',";
         $vals .= "'".setDefault("sint_ResiAddCityRefId")."','".setDefault("char_ResiSubd")."',";
         $vals .= "'".setDefault("char_PermanentHouseNo")."','".setDefault("char_PermanentStreet")."',";
         $vals .= "'".setDefault("char_PermanentBrgy")."','".setDefault("sint_PermanentCountryRefId")."',";
         $vals .= "'".setDefault("sint_PermanentAddProvinceRefId")."','".setDefault("sint_PermanentAddCityRefId")."',";
         $vals .= "'".setDefault("char_PermanentSubd")."','".setDefault("char_TelNo")."',";
         $vals .= "'".setDefault("char_MobileNo")."','".setDefault("char_EmailAdd")."',";
         $vals .= "'".setDefault("char_GSIS")."','".setDefault("char_PAGIBIG")."',";
         $vals .= "'".setDefault("char_PHIC")."','".setDefault("char_TIN")."',";
         $vals .= "'".setDefault("char_SSS")."',";
         $vals .= "'".setDefault("char_Others")."',";
         if (getvalue("char_AgencyId") != "") {
            $flds .= "Inactive, ";
            $vals .= "0, ";   
         }
         if (getvalue("char_GovtIssuedIDDate") != "") {
            $flds .= "GovtIssuedIDDate, ";
            $vals .= "'".setDefault("char_GovtIssuedIDDate")."',";
         }
         $emprefid = f_SaveRecord("NEWSAVE","employees",$flds,$vals);
         if (!is_numeric($emprefid)) {
            echo 'alert("Error in Saving in Employees");';
         }
      }

      if ($emprefid) {
         saveChildrenInfo($emprefid);
         saveFamilyInfo($emprefid);
         saveEducInfo($emprefid);
         saveEligibility($emprefid);
         saveWorkExp($emprefid);
         saveVolWork($emprefid);
         saveTrainProg($emprefid);
         saveOtherInfo($emprefid);
         saveReference($emprefid);
         savePDSQ($emprefid);
         echo 'alert("Record Succesfully Saved.");';
         echo "var addParam = '';";
         echo "addParam += '&paramTitle=201 File&hTable=employees';";
         echo "gotoscrn('scrn201File',addParam);";
      }
   }
   function saveChildrenInfo($emprefid) {
      for($j=1;$j<=getvalue("hCount_Childrens");$j++) {
         if (getvalue("chkChild_".$j) == "on" || getvalue("chkChild_".$j) == "true") {
            $childRefId = getvalue("ChildrenRefId_".$j);
            if ($childRefId == ""){
               $childRefId = 0;
            }
            if (
                  (
                     setDefault("char_FullName_".$j) == "" ||
                     setDefault("date_BirthDate_".$j) == "" ||
                     setDefault("char_Gender_".$j) == ""
                  )
               )
            {
               echo 'alert("Children\'s Information... All Fields are required...");';
            } else {
               $child = FindFirst("employeeschild","WHERE RefId = $childRefId","*");
               if ($child) {
                  $childField = "";
                  $childField .= "FullName = '".setDefault("char_FullName_".$j)."', ";
                  $childField .= "Gender = '".setDefault("char_Gender_".$j)."', ";
                  $childField .= "BirthDate = '".setDefault("date_BirthDate_".$j)."', ";
                  f_SaveRecord("EDITSAVE","employeeschild",$childField,setDefault("ChildrenRefId_".$j));
               } else {
                  $values = "";
                  $childField = "FullName,Gender,BirthDate,EmployeesRefId, ";
                  $values .= "'".setDefault("char_FullName_".$j)."', ";
                  $values .= "'".setDefault("char_Gender_".$j)."', ";
                  $values .= "'".setDefault("date_BirthDate_".$j)."', ";
                  $values .= "$emprefid,";
                  $criteria  = "WHERE CompanyRefId = ".getvalue("hCompanyID");
                  $criteria .= " AND BranchRefId = ".getvalue("hBranchID");
                  $criteria .= " AND EmployeesRefId = ".$emprefid;
                  $criteria .= " AND FullName = '".setDefault("char_FullName_".$j)."' ";
                  $criteria .= " AND BirthDate = '".setDefault("date_BirthDate_".$j)."'";
                  $child = FindFirst("employeeschild",$criteria,"RefId");
                  if ($child) {
                     echo 'alert("WARNING!!! \n Children\'s Record is already exist !!!\nOther Transactions will continue.");'."\n";
                  } else {
                     $newReFId = f_SaveRecord("NEWSAVE","employeeschild",$childField,$values);
                  }
               }
            }

         }
      }
   }
   function saveFamilyInfo($emprefid) {
      if (getvalue("FamilyBG") == "true") {
         $Family = FindFirst("employeesfamily","WHERE EmployeesRefId = ".$emprefid." ","RefId");
         if ($Family) {
            $famField = "";
            $famField .= "SpouseLastName = '".setDefault("char_SpouseLastName")."', ";
            $famField .= "SpouseFirstName = '".setDefault("char_SpouseFirstName")."', ";
            $famField .= "SpouseMiddleName = '".setDefault("char_SpouseMiddleName")."', ";
            $famField .= "SpouseExtName = '".setDefault("char_SpouseExtName")."', ";
            //$famField .= "SpouseBirthDate = '".setDefault("date_SpouseBirthDate")."', ";
            $famField .= "SpouseMobileNo = '".setDefault("char_SpouseMobileNo")."', ";
            $famField .= "OccupationsRefId = '".setDefault("sint_OccupationsRefId")."', ";
            $famField .= "EmployersName = '".setDefault("char_EmployersName")."', ";
            $famField .= "BusinessAddress = '".setDefault("char_BusinessAddress")."', ";


            $famField .= "FatherFirstName = '".setDefault("char_FatherFirstName")."', ";
            $famField .= "FatherLastName = '".setDefault("char_FatherLastName")."', ";
            $famField .= "FatherMiddleName = '".setDefault("char_FatherMiddleName")."', ";
            $famField .= "FatherExtName = '".setDefault("char_FatherExtName")."', ";
            $famField .= "MotherFirstName = '".setDefault("char_MotherFirstName")."', ";
            $famField .= "MotherLastName = '".setDefault("char_MotherLastName")."', ";
            $famField .= "MotherMiddleName = '".setDefault("char_MotherMiddleName")."', ";
            $famField .= "MotherExtName = '".setDefault("char_MotherExtName")."', ";

            f_SaveRecord("EDITSAVE","employeesfamily",$famField,getvalue("FamilyRefId"));

         } else {
            $famField = "";
            $famField .= "SpouseLastName,SpouseFirstName,SpouseMiddleName,SpouseExtName,";
            $famField .= "SpouseMobileNo,OccupationsRefId,EmployersName,BusinessAddress,EmployeesRefId,";
            $famField .= "FatherFirstName,FatherLastName,FatherMiddleName,FatherExtName,";
            $famField .= "MotherFirstName,MotherLastName,MotherMiddleName,";
            $values = "";
            $values .= "'".setDefault("char_SpouseLastName")."', ";
            $values .= "'".setDefault("char_SpouseFirstName")."', ";
            $values .= "'".setDefault("char_SpouseMiddleName")."', ";
            $values .= "'".setDefault("char_SpouseExtName")."', ";
            //$values .= "'".setDefault("date_SpouseBirthDate")."', ";
            $values .= "'".setDefault("char_SpouseMobileNo")."', ";
            $values .= "'".setDefault("sint_OccupationsRefId")."', ";
            $values .= "'".setDefault("char_EmployersName")."', ";
            $values .= "'".setDefault("char_BusinessAddress")."', ";
            $values .= "'$emprefid',";
            $values .= "'".setDefault("char_FatherFirstName")."', ";
            $values .= "'".setDefault("char_FatherLastName")."', ";
            $values .= "'".setDefault("char_FatherMiddleName")."', ";
            $values .= "'".setDefault("char_FatherExtName")."', ";
            $values .= "'".setDefault("char_MotherFirstName")."', ";
            $values .= "'".setDefault("char_MotherLastName")."', ";
            $values .= "'".setDefault("char_MotherMiddleName")."', ";

            f_SaveRecord("NEWSAVE","employeesfamily",$famField,$values);

         }
      }
   }
   function saveEducInfo($emprefid) {
      if (getvalue("chkElementary_1") == "true") {

         $elemRow = FindFirst("employeeseduc","WHERE EmployeesRefId = ".$emprefid." AND LevelType = 1","*");
         /*if (setDefault("sint_Present_1_1") == 1) {
            $date_to = "2999-12-31";
         } else {
            $date_to = setDefault("date_DateTo_1_1");
         }*/
         if ($elemRow){
            $elemField = "";
            $elemField .= "SchoolsRefId = '".setDefault("sint_SchoolsRefId_1_1")."', ";
            $elemField .= "YearGraduated = '".setDefault("sint_YearGraduated_1_1")."', ";
            $elemField .= "HighestGrade = '".setDefault("char_HighestGrade_1_1")."', ";
            $elemField .= "DateFrom = '".setDefault("sint_DateFrom_1_1")."', ";
            $elemField .= "DateTo = '".setDefault("sint_DateTo_1_1")."', ";
            $elemField .= "Honors = '".setDefault("char_Honors_1_1")."', ";
            //$elemField .= "Present = '".setDefault("sint_Present_1_1")."', ";
            echo 'console.log("'.$elemField.'");';
            f_SaveRecord("EDITSAVE","employeeseduc",$elemField,getvalue("elemRefId_1"));
         } else {
            if (!(  getvalue("sint_SchoolsRefId_1_1") == 0 &&
                    getvalue("sint_CourseRefId_1_1") == 0 &&
                    getvalue("sint_YearGraduated_1_1") == "" &&
                    getvalue("char_HighestGrade_1_1") == "" &&
                    getvalue("sint_DateFrom_1_1") == "" &&
                    getvalue("sint_DateTo_1_1") == "" &&
                    getvalue("char_Honors_1_1") == "")){
               $elemField = "";
               $values = "";
               $elemField .= "SchoolsRefId,YearGraduated,HighestGrade,DateFrom,DateTo,Honors,EmployeesRefId,LevelType, ";
               $values .= "'".setDefault("sint_SchoolsRefId_1_1")."', ";
               $values .= "'".setDefault("sint_YearGraduated_1_1")."', ";
               $values .= "'".setDefault("char_HighestGrade_1_1")."', ";
               $values .= "'".setDefault("sint_DateFrom_1_1")."', ";
               $values .= "'".setDefault("sint_DateTo_1_1")."', ";
               //$values .= "'".setDefault("sint_Present_1_1")."', ";
               $values .= "'".setDefault("char_Honors_1_1")."', ";
               $values .= "$emprefid,1,";
               //echo 'console.log("'.$elemField.'");';
               //echo 'console.log("'.$values.'");';
               f_SaveRecord("NEWSAVE","employeeseduc",$elemField,$values);
            }
         }
      }

      if (getvalue("chkSecondary_1") == "true") {
         $secondaryRow = FindFirst("employeeseduc","WHERE EmployeesRefId = ".$emprefid." AND LevelType = 2","*");
         /*if (setDefault("sint_Present_2_1") == 1) {
            $date_to = "2999-12-31";
         } else {
            $date_to = setDefault("date_DateTo_2_1");
         }*/
         if ($secondaryRow){
            $secondaryField = "";
            $secondaryField .= "SchoolsRefId = '".setDefault("sint_SchoolsRefId_2_1")."', ";
            $secondaryField .= "YearGraduated = '".setDefault("sint_YearGraduated_2_1")."', ";
            $secondaryField .= "HighestGrade = '".setDefault("char_HighestGrade_2_1")."', ";
            $secondaryField .= "DateFrom = '".setDefault("sint_DateFrom_2_1")."', ";
            $secondaryField .= "DateTo = '".setDefault("sint_DateTo_2_1")."', ";
            $secondaryField .= "Honors = '".setDefault("char_Honors_2_1")."', ";
            //$secondaryField .= "Present = '".setDefault("sint_Present_2_1")."', ";
            echo 'console.log("'.$secondaryField.'");';
            f_SaveRecord("EDITSAVE","employeeseduc",$secondaryField,getvalue("secondaryRefId_1"));

         } else {
            if (!(  getvalue("sint_SchoolsRefId_2_1") == 0 &&
                    getvalue("sint_CourseRefId_2_1") == 0 &&
                    getvalue("sint_YearGraduated_2_1") == "" &&
                    getvalue("char_HighestGrade_2_1") == "" &&
                    getvalue("sint_DateFrom_2_1") == "" &&
                    getvalue("sint_DateTo_2_1") == "" &&
                    getvalue("char_Honors_2_1") == "")){
               $secondaryField = "";
               $values = "";
               $secondaryField .= "SchoolsRefId,YearGraduated,HighestGrade,DateFrom,DateTo,Honors,EmployeesRefId,LevelType, ";
               $values .= "'".setDefault("sint_SchoolsRefId_2_1")."', ";
               $values .= "'".setDefault("sint_YearGraduated_2_1")."', ";
               $values .= "'".setDefault("char_HighestGrade_2_1")."', ";
               $values .= "'".setDefault("sint_DateFrom_2_1")."', ";
               $values .= "'".setDefault("sint_DateTo_2_1")."', ";
               //$values .= "'".setDefault("sint_Present_2_1")."', ";
               $values .= "'".setDefault("char_Honors_2_1")."', ";
               $values .= "$emprefid,2,";
               f_SaveRecord("NEWSAVE","employeeseduc",$secondaryField,$values);

            }
         }
      }

      for($j=1;$j<=getvalue("hCount_College");$j++) {
         if (getvalue("chkCollege_4_".$j) == "true" || getvalue("chkCollege_4_".$j) == "on") {
            $collegerefid = getvalue("collegeRefId_".$j);
            if ($collegerefid == "") {
               $collegerefid = 0;
            }
            $educ = FindFirst("employeeseduc","WHERE RefId = ".$collegerefid,"*");
            /*if (setDefault("sint_Present_4_".$j) == 1) {
               $date_to = "2999-12-31";
            } else {
               $date_to = setDefault("date_DateTo_4_".$j);
            }*/
            if ($educ){
               $educField = "";
               $educField .= "SchoolsRefId = '".setDefault("sint_SchoolsRefId_4_".$j)."', ";
               $educField .= "CourseRefId = '".setDefault("sint_CourseRefId_4_".$j)."', ";
               $educField .= "YearGraduated = '".setDefault("sint_YearGraduated_4_".$j)."', ";
               $educField .= "HighestGrade = '".setDefault("char_HighestGrade_4_".$j)."', ";
               $educField .= "DateFrom = '".setDefault("sint_DateFrom_4_".$j)."', ";
               $educField .= "DateTo = '".setDefault("sint_DateTo_4_".$j)."', ";
               $educField .= "Honors = '".setDefault("char_Honors_4_".$j)."', ";
               //$educField .= "Present = '".setDefault("sint_Present_4_".$j)."', ";
               f_SaveRecord("EDITSAVE","employeeseduc",$educField,$collegerefid);

            } else {
               /*if (!(  getvalue("sint_SchoolsRefId_4_".$j) == 0 &&
                       getvalue("sint_CourseRefId_4_".$j) == 0 &&
                       getvalue("sint_YearGraduated_4_".$j) == "" &&
                       getvalue("char_HighestGrade_4_".$j) == "" &&
                       getvalue("date_DateFrom_4_".$j) == "" &&
                       getvalue("date_DateTo_4_".$j) == "" &&
                       getvalue("char_Honors_4_".$j) == "")){
                          */
                  $educField = "";
                  $values = "";
                  $educField .= "SchoolsRefId,CourseRefId,YearGraduated,HighestGrade,DateFrom,DateTo,Honors,EmployeesRefId,LevelType, ";
                  $values .= "'".setDefault("sint_SchoolsRefId_4_".$j)."', ";
                  $values .= "'".setDefault("sint_CourseRefId_4_".$j)."', ";
                  $values .= "'".setDefault("sint_YearGraduated_4_".$j)."', ";
                  $values .= "'".setDefault("char_HighestGrade_4_".$j)."', ";
                  $values .= "'".setDefault("sint_DateFrom_4_".$j)."', ";
                  $values .= "'".setDefault("sint_DateTo_4_".$j)."', ";
                  //$values .= "'".setDefault("sint_Present_4_".$j)."', ";
                  $values .= "'".setDefault("char_Honors_4_".$j)."', ";
                  $values .= "$emprefid,4,";
                  f_SaveRecord("NEWSAVE","employeeseduc",$educField,$values);

               /*}*/
            }
         }
      }


      for($j=1;$j<=getvalue("hCount_VocCourse");$j++) {
         if (getvalue("chkVocCourse_3_".$j) == "true" || getvalue("chkVocCourse_3_".$j) == "on") {
            $vocationalRefId = getvalue("VocationalRefId_3_".$j);
            if ($vocationalRefId == ""){
               $vocationalRefId = 0;
            }
            $voc = FindFirst("employeeseduc","WHERE RefId = $vocationalRefId","*");
            /*if (setDefault("sint_Present_3_".$j) == 1) {
               $date_to = "2999-12-31";
            } else {
               $date_to = setDefault("date_DateTo_3_".$j);
            }*/
            if ($voc) {
               $vocField = "";
               $vocField .= "SchoolsRefId = '".setDefault("sint_SchoolsRefId_3_".$j)."', ";
               $vocField .= "CourseRefId = '".setDefault("sint_CourseRefId_3_".$j)."', ";
               $vocField .= "YearGraduated = '".setDefault("sint_YearGraduated_3_".$j)."', ";
               $vocField .= "HighestGrade = '".setDefault("char_HighestGrade_3_".$j)."', ";
               $vocField .= "DateFrom = '".setDefault("sint_DateFrom_3_".$j)."', ";
               $vocField .= "DateTo = '".setDefault("sint_DateTo_3_".$j)."', ";
               $vocField .= "Honors = '".setDefault("char_Honors_3_".$j)."', ";
               //$vocField .= "Present = '".setDefault("sint_Present_3_".$j)."', ";
               f_SaveRecord("EDITSAVE","employeeseduc",$vocField,$vocationalRefId);

            } else {
               /*if (!(getvalue("sint_SchoolsRefId_3_".$j) == 0 &&
                    getvalue("sint_CourseRefId_3_".$j) == 0 &&
                    getvalue("sint_YearGraduated_3_".$j) == "" &&
                    getvalue("char_HighestGrade_3_".$j) == "" &&
                    getvalue("date_DateFrom_3_".$j) == "" &&
                    getvalue("date_DateTo_3_".$j) == "" &&
                    getvalue("char_Honors_3_".$j) == "")){*/
                  $vocField = "";
                  $values = "";
                  $vocField .= "SchoolsRefId,CourseRefId,YearGraduated,HighestGrade,DateFrom,DateTo,Honors,EmployeesRefId,LevelType, ";
                  $values .= "'".setDefault("sint_SchoolsRefId_3_".$j)."', ";
                  $values .= "'".setDefault("sint_CourseRefId_3_".$j)."', ";
                  $values .= "'".setDefault("sint_YearGraduated_3_".$j)."', ";
                  $values .= "'".setDefault("char_HighestGrade_3_".$j)."', ";
                  $values .= "'".setDefault("sint_DateFrom_3_".$j)."', ";
                  $values .= "'".setDefault("sint_DateTo_3_".$j)."', ";
                  //$values .= "'".setDefault("sint_Present_3_".$j)."', ";
                  $values .= "'".setDefault("char_Honors_3_".$j)."', ";
                  $values .= "$emprefid,3,";

                  $criteria  = "WHERE CompanyRefId = ".getvalue("hCompanyID");
                  $criteria .= " AND BranchRefId = ".getvalue("hBranchID");
                  $criteria .= " AND EmployeesRefId = ".$emprefid;
                  $criteria .= " AND SchoolsRefId = '".setDefault("sint_SchoolsRefId_3_".$j)."' ";
                  $criteria .= " AND CourseRefId = '".setDefault("sint_CourseRefId_3_".$j)."' ";
                  $criteria .= " AND DateFrom = '".setDefault("sint_DateFrom_3_".$j)."' ";
                  $criteria .= " AND LevelType = 3";
                  $voc = FindFirst("employeeseduc",$criteria,"RefId");
                  if ($voc) {
                     echo 'alert("Vocational is already exist !!!\nOther Transactions will continue.");'."\n";
                  } else {
                     f_SaveRecord("NEWSAVE","employeeseduc",$vocField,$values);

                  }

               //}
            }
         }
      }

      for($j=1;$j<=getvalue("hCount_GradStudies");$j++) {
         if (getvalue("chkGradStudies_5_".$j) == "true" || getvalue("chkGradStudies_5_".$j) == "on") {
            $gradStudiesRefId = getvalue("GradStudiesRefId_5_".$j);
            if ($gradStudiesRefId == ""){
               $gradStudiesRefId = 0;
            }
            $gradStudies = FindFirst("employeeseduc","WHERE RefId = $gradStudiesRefId","*");
            /*if (setDefault("sint_Present_5_".$j) == 1) {
               $date_to = "2999-12-31";
            } else {
               $date_to = setDefault("date_DateTo_5_".$j);
            }*/
            if ($gradStudies) {
               $gradField = "";
               $gradField .= "SchoolsRefId = '".setDefault("sint_SchoolsRefId_5_".$j)."', ";
               $gradField .= "CourseRefId = '".setDefault("sint_CourseRefId_5_".$j)."', ";
               $gradField .= "YearGraduated = '".setDefault("sint_YearGraduated_5_".$j)."', ";
               $gradField .= "HighestGrade = '".setDefault("char_HighestGrade_5_".$j)."', ";
               $gradField .= "DateFrom = '".setDefault("sint_DateFrom_5_".$j)."', ";
               $gradField .= "DateTo = '".setDefault("sint_DateTo_5_".$j)."', ";
               $gradField .= "Honors = '".setDefault("char_Honors_5_".$j)."', ";
               //$gradField .= "Present = '".setDefault("sint_Present_5_".$j)."', ";
               f_SaveRecord("EDITSAVE","employeeseduc",$gradField,$gradStudiesRefId);
            } else {
               /*if ((getvalue("sint_SchoolsRefId_5_".$j) != 0 ||
                    getvalue("sint_CourseRefId_5_".$j) != 0 ||
                    getvalue("sint_YearGraduated_5_".$j) != "" ||
                    getvalue("char_HighestGrade_5_".$j) != "" ||
                    getvalue("date_DateFrom_5_".$j) != "" ||
                    getvalue("date_DateTo_5_".$j) != "" ||
                    getvalue("char_Honors_5_".$j) != "")){*/
                  $gradField = "";
                  $values = "";
                  $gradField .= "SchoolsRefId,CourseRefId,YearGraduated,HighestGrade,DateFrom,DateTo,Honors,EmployeesRefId,LevelType, ";

                  $values .= "'".setDefault("sint_SchoolsRefId_5_".$j)."', ";
                  $values .= "'".setDefault("sint_CourseRefId_5_".$j)."', ";
                  $values .= "'".setDefault("sint_YearGraduated_5_".$j)."', ";
                  $values .= "'".setDefault("char_HighestGrade_5_".$j)."', ";
                  $values .= "'".setDefault("sint_DateFrom_5_".$j)."', ";
                  $values .= "'".setDefault("sint_DateTo_5_".$j)."', ";
                  $values .= "'".setDefault("char_Honors_5_".$j)."', ";
                  //$values .= "'".setDefault("sint_Present_5_".$j)."', ";
                  $values .= "$emprefid,5,";


                  $criteria  = "WHERE CompanyRefId = ".getvalue("hCompanyID");
                  $criteria .= " AND BranchRefId = ".getvalue("hBranchID");
                  $criteria .= " AND EmployeesRefId = ".$emprefid;
                  $criteria .= " AND SchoolsRefId = '".setDefault("sint_SchoolsRefId_5_".$j)."' ";
                  $criteria .= " AND CourseRefId = '".setDefault("sint_CourseRefId_5_".$j)."' ";
                  $criteria .= " AND DateFrom = '".setDefault("sint_DateFrom_5_".$j)."'";
                  $criteria .= " AND LevelType = 5";

                  $grad = FindFirst("employeeseduc",$criteria,"RefId");
                  if ($grad) {
                     echo 'alert("Graduate Study is already exist !!!\nOther Transactions will continue.");'."\n";
                  } else {
                     f_SaveRecord("NEWSAVE","employeeseduc",$gradField,$values);

                  }
               //}
            }
         }
      }
   }
   function saveEligibility($emprefid) {

      for($j=1;$j<=getvalue("hCount_Eligibility");$j++) {
         if (getvalue("chkEligib_".$j) == "true" || getvalue("chkEligib_".$j) == "on") {
            $eligibilityRefId = getvalue("eligibilityRefId_".$j);
            if ($eligibilityRefId == ""){
               $eligibilityRefId = 0;
            }
            $eligibility = FindFirst("employeeselegibility","WHERE RefId = $eligibilityRefId","*");
            if ($eligibility) {
               $eligibilityField = "";
               $eligibilityField .= "CareerServiceRefId = '".setDefault("sint_CareerServiceRefId_".$j)."', ";
               $eligibilityField .= "Rating = '".setDefault("sint_Rating_".$j)."', ";
               $eligibilityField .= "ExamDate = '".setDefault("date_ExamDate_".$j)."', ";
               $eligibilityField .= "ExamPlace = '".setDefault("char_ExamPlace_".$j)."', ";
               $eligibilityField .= "LicenseNo = '".setDefault("char_LicenseNo_".$j)."', ";
               $eligibilityField .= "LicenseReleasedDate = '".setDefault("date_LicenseReleasedDate_".$j)."', ";
               f_SaveRecord("EDITSAVE","employeeselegibility",$eligibilityField,$eligibilityRefId);

            } else {
               if (!(getvalue("sint_CareerServiceRefId_".$j) == 0 &&
                    getvalue("sint_Rating_".$j) == 0 &&
                    getvalue("date_ExamDate_".$j) == "" &&
                    getvalue("char_ExamPlace_".$j) == "" &&
                    getvalue("char_LicenseNo_".$j) == "" &&
                    getvalue("date_LicenseReleasedDate_".$j) == "")){

                  $eligibilityField = "";
                  $values = "";
                  $eligibilityField .= "CareerServiceRefId,Rating,ExamDate,ExamPlace,LicenseNo,LicenseReleasedDate,EmployeesRefId,";
                  $values .= "'".setDefault("sint_CareerServiceRefId_".$j)."', ";
                  $values .= "'".setDefault("sint_Rating_".$j)."', ";
                  $values .= "'".setDefault("date_ExamDate_".$j)."', ";
                  $values .= "'".setDefault("char_ExamPlace_".$j)."', ";
                  $values .= "'".setDefault("char_LicenseNo_".$j)."', ";
                  $values .= "'".setDefault("date_LicenseReleasedDate_".$j)."', ";
                  $values .= "$emprefid,";
                  $criteria  = "WHERE CompanyRefId = ".getvalue("hCompanyID");
                  $criteria .= " AND BranchRefId = ".getvalue("hBranchID");
                  $criteria .= " AND EmployeesRefId = ".$emprefid;
                  $criteria .= " AND CareerServiceRefId = '".setDefault("sint_CareerServiceRefId_".$j)."' ";
                  $criteria .= " AND ExamDate = '".setDefault("date_ExamDate_".$j)."' ";
                  $eligib = FindFirst("employeeselegibility",$criteria,"RefId");
                  //echo $eligibilityField."<br>".$values;
                  if ($eligib) {
                     echo 'alert("Eligibility is already exist !!!\nOther Transactions will continue.");'."\n";
                  } else {
                     //echo $eligibilityField;
                     $eligibility_result = f_SaveRecord("NEWSAVE","employeeselegibility",$eligibilityField,$values);
                     if (!is_numeric($eligibility_result)) {
                        echo "alert(Error in Saving Eligibility)";
                     }
                  }
               }
            }
         }
      }

   }
   function saveWorkExp($emprefid) {
      for($j=1;$j<=getvalue("hCount_WorkExp");$j++) {
         if (getvalue("chkWorkExp_".$j) == "true" || getvalue("chkWorkExp_".$j) == "on") {
            $workexperienceRefId = getvalue("workexperienceRefId_".$j);
            if ($workexperienceRefId == ""){
               $workexperienceRefId = 0;
            }
            if (setDefault("sint_Present_".$j) == 1) {
               $date_to = "2999-12-31";
            } else {
               $date_to = setDefault("date_WorkEndDate_".$j);
            }

            $workexp = FindFirst("employeesworkexperience","WHERE RefId = $workexperienceRefId","*");
            if ($workexp) {
               $workexpField = "";

               $workexpField .= " WorkStartDate = '".setDefault("date_WorkStartDate_".$j)."', ";
               $workexpField .= " WorkEndDate = '".$date_to."', ";
               $workexpField .= " PositionRefId = '".setDefault("sint_PositionRefId_".$j)."', ";
               $workexpField .= " AgencyRefId = '".setDefault("sint_AgencyRefId_".$j)."', ";
               $workexpField .= " SalaryAmount = '".str_replace(",","",setDefault("deci_SalaryAmount_".$j))."', ";
               $workexpField .= " SalaryGradeRefId = '".setDefault("sint_SalaryGradeRefId_".$j)."', ";
               $workexpField .= " JobGradeRefId = '".setDefault("sint_JobGradeRefId_".$j)."', ";
               $workexpField .= " EmpStatusRefId = '".setDefault("sint_EmpStatusRefId_".$j)."', ";
               $workexpField .= " StepIncrementRefId = '".setDefault("sint_StepIncrementRefId_".$j)."', ";
               $workexpField .= " isGovtService = '".setDefault("sint_isGovtService_".$j)."', ";
               $workexpField .= " Present = '".setDefault("sint_Present_".$j)."', ";
               //$workexpField .= " PayGroup = '".setDefault("char_PayGroup_".$j)."', ";
               //$workexpField .= " PayrateRefId = '".setDefault("sint_PayrateRefId_".$j)."', ";
               //$workexpField .= " LWOP = '".setDefault("char_LWOP_".$j)."', ";
               //$workexpField .= " ExtDate = '".setDefault("date_ExtDate_".$j)."', ";
               //$workexpField .= " SeparatedDate = '".setDefault("date_SeparatedDate_".$j)."', ";
               //$workexpField .= " Reason = '".setDefault("char_Reason_".$j)."', ";
               //$workexpField .= " Remarks = '".setDefault("char_Remarks_".$j)."', ";
               $workexpField .= " isServiceRecord = '".setDefault("sint_isServiceRecord_".$j)."', ";
               f_SaveRecord("EDITSAVE","employeesworkexperience",$workexpField,$workexperienceRefId);

            } else {
               if (!(getvalue("date_WorkStartDate_".$j) == "" &&
                     getvalue("date_WorkEndDate_".$j) == "" &&
                     getvalue("sint_PositionRefId_".$j) == 0 &&
                     getvalue("sint_AgencyRefId_".$j) == 0 &&
                     getvalue("deci_SalaryAmount_".$j) == "" &&
                     //getvalue("sint_SalaryGradeRefId_".$j) == 0 &&
                     //getvalue("sint_ApptStatusRefId_".$j) == 0 &&
                     getvalue("sint_isGovtService_".$j) == 0 &&
                     //getvalue("char_PayGroup_".$j) == "" &&
                     //getvalue("sint_PayrateRefId_".$j) == 0 &&
                     //getvalue("char_LWOP_".$j) == "" &&
                     //getvalue("date_ExtDate_".$j) == "" &&
                     //getvalue("date_SeparatedDate_".$j) == "" &&
                     //getvalue("char_Reason_".$j) == "" &&
                     //getvalue("char_Remarks_".$j) == "" &&
                     getvalue("sint_isServiceRecord") == "")){

                  $workexpField = "";
                  $values = "";
                  /*$workexpField .= "WorkStartDate,WorkEndDate,PositionRefId,AgencyRefId,SalaryAmount,SalaryGradeRefId,ApptStatusRefId,";
                  $workexpField .= "isGovtService,PayGroup,PayrateRefId,LWOP,ExtDate,SeparatedDate,Reason,Remarks,EmployeesRefId,isServiceRecord,";*/
                  $workexpField .= "WorkStartDate,WorkEndDate,PositionRefId,AgencyRefId,SalaryAmount,SalaryGradeRefId,JobGradeRefId,EmpStatusRefId,";
                  $workexpField .= "isGovtService,EmployeesRefId,isServiceRecord,Present,StepIncrementRefId,";
                  $values .= "'".setDefault("date_WorkStartDate_".$j)."', ";
                  $values .= "'".$date_to."', ";
                  $values .= "'".setDefault("sint_PositionRefId_".$j)."', ";
                  $values .= "'".setDefault("sint_AgencyRefId_".$j)."', ";
                  $values .= "'".str_replace(",","",setDefault("deci_SalaryAmount_".$j))."', ";
                  $values .= "'".setDefault("sint_SalaryGradeRefId_".$j)."', ";
                  $values .= "'".setDefault("sint_JobGradeRefId_".$j)."', ";
                  $values .= "'".setDefault("sint_EmpStatusRefId_".$j)."', ";
                  $values .= "'".setDefault("sint_isGovtService_".$j)."', ";
                  //$values .= "'".setDefault("char_PayGroup_".$j)."', ";
                  //$values .= "'".setDefault("sint_PayrateRefId_".$j)."', ";
                  //$values .= "'".setDefault("char_LWOP_".$j)."', ";
                  //$values .= "'".setDefault("date_ExtDate_".$j)."', ";
                  //$values .= "'".setDefault("date_SeparatedDate_".$j)."', ";
                  //$values .= "'".setDefault("char_Reason_".$j)."', ";
                  //$values .= "'".setDefault("char_Remarks_".$j)."', ";
                  $values .= "$emprefid,";
                  $values .= "'".setDefault("sint_isServiceRecord_".$j)."', ";
                  $values .= "'".setDefault("sint_Present_".$j)."', ";
                  $values .= "'".setDefault("sint_StepIncrementRefId_".$j)."', ";

                  $criteria  = "WHERE CompanyRefId = ".getvalue("hCompanyID");
                  $criteria .= " AND BranchRefId = ".getvalue("hBranchID");
                  $criteria .= " AND EmployeesRefId = ".$emprefid;
                  $criteria .= " AND WorkEndDate = '".$date_to."'";
                  $criteria .= " AND PositionRefId = '".setDefault("sint_PositionRefId_".$j)."'";
                  $criteria .= " AND AgencyRefId = '".setDefault("sint_AgencyRefId_".$j)."' ";

                  $eligib = FindFirst("employeesworkexperience",$criteria,"RefId");
                  if ($eligib) {
                     echo 'alert("Work Experience is already exist !!!\nOther Transactions will continue.");'."\n";
                  } else {
                     f_SaveRecord("NEWSAVE","employeesworkexperience",$workexpField,$values);
                  }
               }
            }
         }
      }
   }
   function saveVolWork($emprefid) {
      /*EDIT SAVE FOR VOLUNTARY WORK*/
      for($j=1;$j<=getvalue("hCount_VoluntaryWork");$j++) {
         if (getvalue("chkVolWork_".$j) == "true" || getvalue("chkVolWork_".$j) == "on") {
            $voluntaryRefId = getvalue("voluntaryRefId_".$j);
            if ($voluntaryRefId == ""){
               $voluntaryRefId = 0;
            }
            if (setDefault("sint_Present_vw_".$j) == 1) {
               $date_to = "2999-12-31";
            } else {
               $date_to = setDefault("date_EndDate_".$j);
            }
            $voluntaryworkRow = FindFirst("employeesvoluntary","WHERE RefId = $voluntaryRefId","*");
            if ($voluntaryworkRow) {
               $voluntaryField = "";
               $voluntaryField .= "OrganizationRefId = '".setDefault("bint_OrganizationRefId_".$j)."', ";
               $voluntaryField .= "StartDate = '".setDefault("date_StartDate_".$j)."', ";
               $voluntaryField .= "EndDate = '".$date_to."', ";
               $voluntaryField .= "NumofHrs = '".setDefault("sint_NumofHrs_".$j)."', ";
               $voluntaryField .= "WorksNature = '".setDefault("char_WorksNature_".$j)."', ";
               $voluntaryField .= "Present = '".setDefault("sint_Present_vw_".$j)."', ";
               f_SaveRecord("EDITSAVE","employeesvoluntary",$voluntaryField,$voluntaryRefId);
            } else {
               if (!(getvalue("bint_OrganizationRefId_".$j) == 0 &&
                    getvalue("date_StartDate_".$j) == "" &&
                    getvalue("date_EndDate_".$j) == "" &&
                    getvalue("sint_NumofHrs_".$j) == 0 &&
                    getvalue("char_WorksNature_".$j) == "")){

                  $voluntaryField = "";
                  $values = "";
                  $voluntaryField .= "OrganizationRefId,StartDate,EndDate,NumofHrs,WorksNature,EmployeesRefId,Present,";
                  $values .= "'".setDefault("bint_OrganizationRefId_".$j)."', ";
                  $values .= "'".setDefault("date_StartDate_".$j)."', ";
                  $values .= "'".$date_to."', ";
                  $values .= "'".setDefault("sint_NumofHrs_".$j)."', ";
                  $values .= "'".setDefault("char_WorksNature_".$j)."', ";
                  $values .= "$emprefid,";
                  $values .= "'".setDefault("sint_Present_vw_".$j)."', ";

                  $criteria  = "WHERE CompanyRefId = ".getvalue("hCompanyID");
                  $criteria .= " AND BranchRefId = ".getvalue("hBranchID");
                  $criteria .= " AND EmployeesRefId = ".$emprefid;
                  $criteria .= " AND OrganizationRefId = ".setDefault("sint_OrganiztionRefId_".$j);
                  $criteria .= " AND StartDate = '".setDefault("date_StartDate_".$j)."' ";
                  $volWork = FindFirst("employeesvoluntary",$criteria,"RefId");
                  if ($volWork) {
                     echo 'alert("Voluntary Work is already exist !!!\nOther Transactions will continue.");'."\n";
                  } else {
                     f_SaveRecord("NEWSAVE","employeesvoluntary",$voluntaryField,$values);

                  }
               }
            }
         }
      }
   }
   function saveTrainProg($emprefid) {
      for($j=1;$j<=getvalue("hCount_TrainProg");$j++) {
         if (getvalue("chkTrainProg_".$j) == "true" || getvalue("chkTrainProg_".$j) == "on") {
            $trainingRefId = getvalue("trainingRefId_".$j);
            if ($trainingRefId == ""){
               $trainingRefId = 0;
            }
            if (setDefault("sint_Present_tp_".$j) == 1) {
               $date_to = "2999-12-31";
            } else {
               $date_to = setDefault("date_EndDate_".$j."_TrainProg");
            }
            $trainingRow = FindFirst("employeestraining","WHERE RefId = $trainingRefId","*");
            if ($trainingRow) {
               $trainingField = "";
               $trainingField .= "SeminarsRefId = '".setDefault("bint_SeminarsRefId_".$j)."', ";
               $trainingField .= " StartDate = '".setDefault("date_StartDate_".$j."_TrainProg")."', ";
               $trainingField .= " EndDate = '".$date_to."', ";
               $trainingField .= " NumofHrs = '".setDefault("sint_NumofHrs_".$j."_TrainProg")."', ";
               $trainingField .= " SponsorRefId = '".setDefault("bint_SponsorRefId_".$j)."', ";
               //$trainingField .= " SeminarPlaceRefId = '".setDefault("bint_SeminarPlaceRefId_".$j)."', ";
               $trainingField .= " SeminarPlace = '".setDefault("char_SeminarPlace_".$j)."', ";
               $trainingField .= " SeminarClassRefId = '".setDefault("sint_SeminarClassRefId_".$j)."', ";
               $trainingField .= " SeminarTypeRefId = '".setDefault("bint_SeminarTypeRefId_".$j)."', ";
               $trainingField .= " Present = '".setDefault("sint_Present_tp_".$j)."', ";
               //$trainingField .= " Description = '".setDefault("char_Description_".$j)."', ";
               //echo $trainingField;
               f_SaveRecord("EDITSAVE","employeestraining",$trainingField,$trainingRefId);
            } else {
               if (!(
                     getvalue("bint_SeminarsRefId_".$j) == 0 &&
                     getvalue("date_StartDate_".$j."_TrainProg") == "" &&
                     getvalue("date_EndDate_".$j."_TrainProg") == "" &&
                     getvalue("sint_NumofHrs_".$j."_TrainProg") == 0 &&
                     getvalue("bint_SponsorRefId_".$j) == 0 &&
                     getvalue("bint_SeminarTypeRefId_".$j) == 0
                     //getvalue("char_Description_".$j) == ""
                  )){

                  $trainingField = "";
                  $values = "";
                  $trainingField .= "SeminarsRefId,StartDate,EndDate,NumofHrs,SponsorRefId,SeminarPlace,SeminarClassRefId,SeminarTypeRefId,EmployeesRefId,Present,";
                  $values .= "'".setDefault("bint_SeminarsRefId_".$j)."', ";
                  $values .= "'".setDefault("date_StartDate_".$j."_TrainProg")."', ";
                  $values .= "'".$date_to."', ";
                  $values .= "'".setDefault("sint_NumofHrs_".$j."_TrainProg")."', ";
                  $values .= "'".setDefault("bint_SponsorRefId_".$j)."', ";
                  $values .= "'".setDefault("char_SeminarPlace_".$j)."', ";
                  $values .= "'".setDefault("sint_SeminarClassRefId_".$j)."', ";
                  $values .= "'".setDefault("bint_SeminarTypeRefId_".$j)."', ";
                  //$values .= "'".setDefault("char_Description_".$j)."', ";
                  $values .= "$emprefid,";
                  $values .= "'".setDefault("sint_Present_tp_".$j)."', ";
                  //echo $values;
                  //return false;

                  $criteria  = "WHERE CompanyRefId = ".getvalue("hCompanyID");
                  $criteria .= " AND BranchRefId = ".getvalue("hBranchID");
                  $criteria .= " AND EmployeesRefId = ".$emprefid;
                  $criteria .= " AND SeminarsRefId = '".setDefault("bint_SeminarsRefId_".$j)."' ";
                  $criteria .= " AND StartDate = '".setDefault("date_StartDate_".$j."_TrainProg")."' ";
                  $criteria .= " AND SponsorRefId = '".setDefault("bint_SponsorRefId_".$j)."' ";
                  $criteria .= " AND SeminarTypeRefId = '".setDefault("bint_SeminarTypeRefId_".$j)."' ";
                  $volWork = FindFirst("employeestraining",$criteria,"RefId");
                  if ($volWork) {
                     echo 'alert("Training Program is already exist !!!\nOther Transactions will continue.");'."\n";
                  } else {
                     f_SaveRecord("NEWSAVE","employeestraining",$trainingField,$values);

                  }


               }
            }
         }
      }
   }
   function saveOtherInfo($emprefid) {
      for($j=1;$j<=getvalue("hCount_otherinfo");$j++) {
         if (getvalue("chkOtherInfo_".$j) == "true" || getvalue("chkOtherInfo_".$j) == "on") {
            $otherinfoRefId = getvalue("otherinfoRefId_".$j);
            if ($otherinfoRefId == ""){
               $otherinfoRefId = 0;
            }
            $otherinfoRow = FindFirst("employeesotherinfo","WHERE RefId = $otherinfoRefId","*");
            if ($otherinfoRow) {
               $otherinfoField = "";
               $otherinfoField .= "Skills = '".setDefault("char_Skills_".$j)."', ";
               $otherinfoField .= " Recognition = '".setDefault("char_Recognition_".$j)."', ";
               $otherinfoField .= " Affiliates = '".setDefault("char_Affiliates_".$j)."', ";
               f_SaveRecord("EDITSAVE","employeesotherinfo",$otherinfoField,$otherinfoRefId);

            } else {
               $otherinfoField = "";
               $values = "";
               if (getvalue("char_Skills_".$j) != "") {
                  $otherinfoField .= "Skills,";
                  $values .= "'".setDefault("char_Skills_".$j)."', ";
               }
               if (getvalue("char_Recognition_".$j) != "") {
                  $otherinfoField .= "Recognition,";
                  $values .= "'".setDefault("char_Recognition_".$j)."', ";
               }
               if (getvalue("char_Affiliates_".$j) != "") {
                  $otherinfoField .= "Affiliates,";
                  $values .= "'".setDefault("char_Affiliates_".$j)."', ";
               }
               $otherinfoField .= "EmployeesRefId,";
               $values .= "$emprefid,";

               $criteria  = "WHERE CompanyRefId = ".getvalue("hCompanyID");
               $criteria .= " AND BranchRefId = ".getvalue("hBranchID");
               $criteria .= " AND EmployeesRefId = ".$emprefid;
               $criteria .= " AND Skills = '".setDefault("char_Skills_".$j)."' ";
               $criteria .= " AND Recognition = '".setDefault("char_Recognition_".$j)."' ";
               $criteria .= " AND Affiliates = '".setDefault("char_Affiliates_".$j)."' ";
               $volWork = FindFirst("employeesotherinfo",$criteria,"RefId");
               if ($volWork) {
                  echo 'alert("Information is already exist !!!\nOther Transactions will continue.");'."\n";
               } else {
                  f_SaveRecord("NEWSAVE","employeesotherinfo",$otherinfoField,$values);

               }
            }
         }
      }
   }
   function savePDSQ($emprefid) {
      if (getvalue("chkPDSQ") == 1) {
         $pdsq = FindFirst("employeespdsq","WHERE EmployeesRefId = ".$emprefid." ","RefId");
         if ($pdsq){
            $pdsqField = "";
            $pdsqField .= " Q1a = '".setDefault("sint_Q1a")."', ";
            $pdsqField .= " Q1b = '".setDefault("sint_Q1b")."', ";
            $pdsqField .= " Q2a = '".setDefault("sint_Q2a")."', ";
            $pdsqField .= " Q2b = '".setDefault("sint_Q2b")."', ";
            $pdsqField .= " Q3a = '".setDefault("sint_Q3a")."', ";
            $pdsqField .= " Q4a = '".setDefault("sint_Q4a")."', ";
            $pdsqField .= " Q5a = '".setDefault("sint_Q5a")."', ";
            $pdsqField .= " Q5b = '".setDefault("sint_Q5b")."', ";
            $pdsqField .= " Q6a = '".setDefault("sint_Q6a")."', ";
            $pdsqField .= " Q7a = '".setDefault("sint_Q7a")."', ";
            $pdsqField .= " Q7b = '".setDefault("sint_Q7b")."', ";
            $pdsqField .= " Q7c = '".setDefault("sint_Q7c")."', ";
            $pdsqField .= " Q2CaseStatus = '".setDefault("char_Q2CaseStatus")."', ";
            $pdsqField .= " Q2DateFiled = '".setDefault("date_Q2DateFiled")."', ";
            $pdsqField .= " Q1aexp = '".setDefault("char_Q1aexp")."', ";
            $pdsqField .= " Q1bexp = '".setDefault("char_Q1bexp")."', ";
            $pdsqField .= " Q2aexp = '".setDefault("char_Q2aexp")."', ";
            $pdsqField .= " Q2bexp = '".setDefault("char_Q2bexp")."', ";
            $pdsqField .= " Q3aexp = '".setDefault("char_Q3aexp")."', ";
            $pdsqField .= " Q4aexp = '".setDefault("char_Q4aexp")."', ";
            $pdsqField .= " Q5aexp = '".setDefault("char_Q5aexp")."', ";
            $pdsqField .= " Q5bexp = '".setDefault("char_Q5bexp")."', ";
            $pdsqField .= " Q6aexp = '".setDefault("char_Q6aexp")."', ";
            $pdsqField .= " Q7aexp = '".setDefault("char_Q7aexp")."', ";
            $pdsqField .= " Q7bexp = '".setDefault("char_Q7bexp")."', ";
            $pdsqField .= " Q7cexp = '".setDefault("char_Q7cexp")."', ";
            $pdsq_result = f_SaveRecord("EDITSAVE","employeespdsq",$pdsqField,getvalue("pdsqRefId"));
            if ($pdsq_result != "") {
               echo "alert('Error in Updating PDSQ =====".$pdsqField."');";
            }
         } else {
            $pdsqField = "";
            $values = "";
            $pdsqField .= "Q1a,Q1b,Q2a,Q2b,Q3a,Q4a,Q5a,Q5b,Q6a,Q7a,Q7b,Q7c,Q2DateFiled,Q2CaseStatus,";
            $pdsqField .= "Q1aexp,Q1bexp,Q2aexp,Q2bexp,Q3aexp,Q4aexp,Q5aexp,Q5bexp,Q6aexp,Q7aexp,Q7bexp,Q7cexp,EmployeesRefId,";
            $values .= "'".setDefault("sint_Q1a")."', ";
            $values .= "'".setDefault("sint_Q1b")."', ";
            $values .= "'".setDefault("sint_Q2a")."', ";
            $values .= "'".setDefault("sint_Q2b")."', ";
            $values .= "'".setDefault("sint_Q3a")."', ";
            $values .= "'".setDefault("sint_Q4a")."', ";
            $values .= "'".setDefault("sint_Q5a")."', ";
            $values .= "'".setDefault("sint_Q5b")."', ";
            $values .= "'".setDefault("sint_Q6a")."', ";
            $values .= "'".setDefault("sint_Q7a")."', ";
            $values .= "'".setDefault("sint_Q7b")."', ";
            $values .= "'".setDefault("sint_Q7c")."', ";
            $values .= "'".setDefault("date_Q2DateFiled")."', ";
            $values .= "'".setDefault("char_Q2CaseStatus")."', ";
            $values .= "'".setDefault("char_Q1aexp")."', ";
            $values .= "'".setDefault("char_Q1bexp")."', ";
            $values .= "'".setDefault("char_Q2aexp")."', ";
            $values .= "'".setDefault("char_Q2bexp")."', ";
            $values .= "'".setDefault("char_Q3aexp")."', ";
            $values .= "'".setDefault("char_Q4aexp")."', ";
            $values .= "'".setDefault("char_Q5aexp")."', ";
            $values .= "'".setDefault("char_Q5bexp")."', ";
            $values .= "'".setDefault("char_Q6aexp")."', ";
            $values .= "'".setDefault("char_Q7aexp")."', ";
            $values .= "'".setDefault("char_Q7bexp")."', ";
            $values .= "'".setDefault("char_Q7cexp")."', ";
            $values .= "$emprefid, ";
            $pdsq_result = f_SaveRecord("NEWSAVE","employeespdsq",$pdsqField,$values);
            if (!is_numeric($pdsq_result)) {
               echo "alert('Error in Saving in PDSQ ======".$pdsqField."========".$values."');";
            }

         }
      }

   }
   function saveReference($emprefid) {
      for($j=1;$j<=getvalue("hCount_Reference");$j++) {
         if (getvalue("chkRef_".$j) == "true" || getvalue("chkRef_".$j) == "on") {
            $referenceRefId = getvalue("referenceRefId_".$j);
            if ($referenceRefId == ""){
               $referenceRefId = 0;
            }
            //echo $referenceRefId;
            $referenceRow = FindFirst("employeesreference","WHERE RefId = $referenceRefId","*");
            if ($referenceRow) {
               $referenceField = "";
               $referenceField .= "Name = '".setDefault("char_Name_".$j)."', ";
               $referenceField .= " Address = '".setDefault("char_Address_".$j)."', ";
               $referenceField .= " ContactNo = '".setDefault("char_ContactNo_".$j)."', ";
               f_SaveRecord("EDITSAVE","employeesreference",$referenceField,$referenceRefId);
            } else {
               $referenceField = "";
               $values = "";
               $referenceField .= "Name,Address,ContactNo,EmployeesRefId,";
               $values .= "'".setDefault("char_Name_".$j)."', ";
               $values .= "'".setDefault("char_Address_".$j)."', ";
               $values .= "'".setDefault("char_ContactNo_".$j)."', ";
               $values .= "$emprefid,";

               $criteria  = "WHERE CompanyRefId = ".getvalue("hCompanyID");
               $criteria .= " AND BranchRefId = ".getvalue("hBranchID");
               $criteria .= " AND EmployeesRefId = ".$emprefid;
               $criteria .= " AND Name = '".setDefault("char_Name_".$j)."'";
               $criteria .= " AND Address = '".setDefault("char_Address_".$j)."'";
               $criteria .= " AND ContactNo = '".setDefault("char_ContactNo_".$j)."'";
               $volWork = FindFirst("employeesreference",$criteria,"RefId");
               if ($volWork) {
                  echo 'alert("Reference is already exist !!!");'."\n";
               } else {
                  f_SaveRecord("NEWSAVE","employeesreference",$referenceField,$values);

               }

            }
         }
      }
   }
   function fnloadCity() {
      include 'conn.e2e.php';
      echo '$("[name=\''.getvalue("targetObjName").'\'] option").remove();'."\n";
      $rs = SelectEach("city","WHERE ProvinceRefId = ".getvalue("provinceRefId")." ORDER BY Name");
      echo '$("[name=\''.getvalue("targetObjName").'\']").append("<option value=\'0\'>Select City</option>");'."\n";
      if ($rs) {
         while($row = mysqli_fetch_array($rs)) {
            echo '$("[name=\''.getvalue("targetObjName").'\']").append("<option value=\''.$row["RefId"].'\'>'.strtoupper($row["Name"]).'</option>");'."\n";
         }
      } 
   }
   function fnChkUName() {
      $u = getvalue("u");
      $pw = getvalue("t");
      $account = getvalue("account");
      $recordSet = f_Find("sysuser","WHERE UserName = '$u' LIMIT 1");
      if ($recordSet) {
         $recordSet = f_Find("sysuser","WHERE Password = '$pw' AND UserName = '$u' LIMIT 1");
         if ($recordSet) {
            if ($account != "") {
               if ($account != "admin") {
                  echo '$.notify("Access Denied.");';
               } else {
                  echo "loginNow();";   
               }   
            } else {
               echo "loginNow();";   
            }
            
         } else {
            echo '$("[name=\'token\']").notify(
              "Ooops!!! Wrong Password.","error",
              { position:"right" }
            );';
            echo '$("[name=\'token\']").val("");';
            echo '$("[name=\'hToken\']").val("");';
            echo '$("[name=\'token\']").focus();';
         }
      } else {
         $recordSet = f_Find("employees","WHERE UserName = '$u' LIMIT 1");
         if ($recordSet) {
            $recordSet = f_Find("employees","WHERE pw = '$pw' AND UserName = '$u' LIMIT 1");
            if ($recordSet) {
               if ($account != "") {
                  if ($account != "employee") {
                     echo '$.notify("Access Denied.");';
                  } else {
                     echo "loginNow();";   
                  }
               } else {
                  echo "loginNow();";   
               }
            } else {
               echo '$("[name=\'token\']").val("");';
               echo '$("[name=\'hToken\']").val("");';
               echo '$("[name=\'token\']").focus();';
               echo '$("[name=\'token\']").notify(
                 "Ooops!!! Wrong Password.","error",
                 { position:"right" }
               );';
            }
         } else {
            echo '$("[name=\'txtUser\']").val("");';
            echo '$("[name=\'txtUser\']").focus();';
            echo '$("[name=\'token\']").notify(
              "Username '.$u.' does not exist !!!","error",
              { position:"right" }
            );';
         }
      }
   }
   function fnchkRecord() {
      $criteria = "";

      $table = getvalue("dbtable");
      $criteria  = "WHERE CompanyRefId = ".getvalue("hCompanyID");
      $criteria .= " AND BranchRefId = ".getvalue("hBranchID");
      $criteria .= getvalue("criteria");

      //echo $criteria;
      $row = FindFirst($table,$criteria,"RefId");
      if ($row) {
         echo 1;
      } else {
         echo 0;
      }
   }
   function fnSave201Updates(){
      include_once 'conn.e2e.php';
      $t = time();
      $msg = "";
      $date_today = date("Y-m-d",$t);
      $curr_time  = date("H:i:s",$t);
      $fields     = getvalue("dbfield");
      $newValue   = getvalue("newval");
      $tables     = getvalue("dbtable");
      $refid      = getvalue("refid");
      $user       = getvalue("user");
      $datatype   = chkDataType($tables,$fields);
      $idx        = getvalue("idx");
      $flds       = "$fields = '$newValue', ";

      $trackingE  = "Remarks = 'thru 201 Updates', ";
      $trackingE  = "LastUpdateDate = '$date_today', ";
      $trackingE .= "LastUpdateTime = '$curr_time', ";
      $trackingE .= "LastUpdateBy = '$user', ";
      $trackingE .= "Data='E' ";
      $fieldnvalue = $flds.$trackingE;


      $qry_update = "UPDATE `$tables` SET $fieldnvalue WHERE RefId = $refid";
      if ($conn->query($qry_update) === TRUE) {
         if (stripos($fields,"RefId") > 0) {
            $fk_table = explode("RefId",$fields)[0];
            switch ($fk_table) {
               case 'ResiAddCity':
               case 'PermanentAddCity':
                  $tbl = "city";
                  break;
               case 'ResiAddProvince':
               case 'PermanentAddProvince':
                  $tbl = "province";
                  break;
               case 'ResiCountry':
               case 'PermanentCountry':
                  $tbl = "country";
                  break;
               default:
                  $tbl = $fk_table;
                  break;
            }
            $newValue = getRecord($tbl,$newValue,"Name");
         }
         echo '$("#currValue_'.$idx.'").html("'.$newValue.'");';
         echo '$("#NewValue_'.$idx.'").val("");';
         echo '$("#NewValue_'.$idx.'").prop("disabled",true);';
         echo '$("[name=\'chk_'.$idx.'\']").prop("checked",false);';
         echo "alert('Successfully Updated');";
         //echo 'loadEmpDtl("'.$tables.'");';
      } else {
         $msg .= " Saving Error: ".$conn->error;
         if (isset($_SESSION['debugging'])) {
            if ($_SESSION['debugging']) {
               $msg .= " --- ".$qry_update;
            }

         }
         echo $msg;
      }
   }
   function fnrtrnVal(){
      $RefId = getvalue("refid");
      $idx = getvalue("idx");
      $fldName = getvalue("fldName");
      $isMultiple = getvalue("isMultiple");
      $table = getvalue("table");
      $fldName_arr = explode(",", $fldName);

      if ($table == "employeeseduc") {
         $row = FindFirst($table,"WHERE RefId = '$RefId'","*");
         if ($row){
            if ($isMultiple == 1) {
               foreach ($fldName_arr as $value) {
                  $value = trim($value);
                  $value = trim($value," ");
                  $value = trim($value,"\t");
                  $value = trim($value,"\n");
                  $value = trim($value,"\r");

                  $val_arr = explode("_", $value);
                  $type = $val_arr[0];
                  $name = $val_arr[1];
                  $lvl = $val_arr[2];
                  $objValue = $row[$name];

                  if ($name != "SchoolsRefId") {
                     if ($objValue == "2999-12-31") {
                        objSetValue($value,"Present");
                     } else {
                        objSetValue($value,$objValue);
                     }
                     if ($type."_".$name == "sint_Present") {
                        if ($objValue == 1) {
                           echo '$("#sint_Present_'.$lvl."_".$idx.'").prop("checked",true);'."\n";
                        } else {
                           echo '$("#sint_Present_'.$lvl."_".$idx.'").prop("checked",false);'."\n";
                        }
                     }
                  } else {
                     objSetValue($value,$objValue);
                     echo '$("#'.$value.'").val("'.$objValue.'");';
                     echo '$("#'.$value.'").val("'.strtoupper(getRecord("schools",$objValue,"Name")).'");';
                  }

               }
            }
         }
      } 
      else {
         $row = FindFirst($table,"WHERE RefId = '$RefId'","*");
         if ($row) {
            if ($isMultiple == 1) {
               foreach ($fldName_arr as $value) {
                  $value = trim($value);
                  $value = trim($value," ");
                  $value = trim($value,"\t");
                  $value = trim($value,"\n");
                  $value = trim($value,"\r");
                  $val_arr = explode("_", $value);
                  $type = $val_arr[0];
                  $name = $val_arr[1];
                  $objValue = $row[$name];
                  if ($value == "bint_SeminarsRefId_".$idx &&
                      $table == "employeestraining") {
                     $desc = getRecord("Seminars",$row["SeminarsRefId"],"Description");
                     objSetValue("char_Description_".$idx,$desc);
                     objSetValue("bint_SeminarsRefId_".$idx,$objValue);
                  } else if ($value == "sint_PositionRefId_".$idx &&
                             $table == "employeesworkexperience") {
                     echo '$("[name=\'sint_PositionRefId_'.$idx.'\']").empty();';
                     populateDropDown("sint_PositionRefId_",$idx,$row["isGovtService"]);
                     objSetValue("sint_PositionRefId_".$idx,$objValue);
                  } else if ($value == "sint_AgencyRefId_".$idx &&
                             $table == "employeesworkexperience") {
                     echo '$("[name=\'sint_AgencyRefId_'.$idx.'\']").empty();';
                     populateDropDown("sint_AgencyRefId_",$idx,$row["isGovtService"]);
                     objSetValue("sint_AgencyRefId_".$idx,$objValue);
                  } else if ($value == "sint_EmpStatusRefId_".$idx &&
                             $table == "employeesworkexperience") {
                     echo '$("[name=\'sint_EmpStatusRefId_'.$idx.'\']").empty();';
                     populateDropDown("sint_EmpStatusRefId_",$idx,$row["isGovtService"]);
                     objSetValue("sint_EmpStatusRefId_".$idx,$objValue);
                  } else if ($value == "sint_Present_".$idx) {
                     if ($row[$name] == 1) {
                        echo '$("[name=\'sint_Present_'.$idx.'\']").prop("checked",true);'."\n";
                     } else {
                        echo '$("[name=\'sint_Present_'.$idx.'\']").prop("checked",false);'."\n";
                     }
                  } else {
                     objSetValue($value,strtoupper($objValue));
                     if ($objValue == "2999-12-31") {
                        objSetValue($value,"Present");
                     }
                  }
               }
            }
         }
      }
   }
   function fnrtrnEmpVal() {
      $RefId = getvalue("refid");
      $fldName = getvalue("fldName");
      $fldName_arr = explode(",", $fldName);
      $table = getvalue("table");
      if ($table == "employees") {
         $row = FindFirst("employees","WHERE RefId = '$RefId'","*");
         if ($row){
            foreach ($fldName_arr as $value) {
               if ($value != "") {
                  $val_arr = explode("_", $value);
                  $type = $val_arr[0];
                  $name = $val_arr[1];
                  if ($value == "sint_isFilipino") {
                     if ($row["isFilipino"] == 1) {
                        echo '$("#isFilipino").prop("checked",true);';
                        echo '$("#CountryCitizen").hide();';
                     } else {
                        echo '$("#isDual").prop("checked",true);';
                        echo '$("#CountryCitizen").show();';
                     }
                  } 
                  if ($value == "char_CivilStatus") {
                     objSetValue($type."_".$name,$row[$name]);
                  } else {
                     objSetValue($type."_".$name,strtoupper($row[$name]));
                  }
               }
            }
         }
      } else if ($table == "employeesfamily") {
         $row = FindFirst("employeesfamily","WHERE RefId = '$RefId'","*");
         if ($row){
            foreach ($fldName_arr as $value) {
               if ($value != "") {
                  $val_arr = explode("_", $value);
                  $type = $val_arr[0];
                  $name = $val_arr[1];
                  objSetValue($type."_".$name,strtoupper($row[$name]));
               }
            }
         }
      } else if ($table == "employeespdsq") {
         $row = FindFirst("employeespdsq","WHERE RefId = '$RefId'","*");
         if ($row){
            foreach ($fldName_arr as $value) {
               if ($value != "") {
                  $val_arr = explode("_", $value);
                  $type = $val_arr[0];
                  $name = $val_arr[1];
                  if ($row[$name] == 1) {
                     echo '$("#YAns_'.$name.'").prop("checked",true);';
                  } else {
                     echo '$("#NAns_'.$name.'").prop("checked",true);';
                  }
                  objSetValue($type."_".$name,$row[$name]);
               }
            }
         }
      }

   }
   function fncancelRequest(){
      $refid = getvalue("refid");
      $fldnval = "Status = 'C',";
      $result = f_SaveRecord("EDITSAVE","updates201",$fldnval,$refid);
      if ($result != "") {
         echo 'alert("Error in Cancelling your request.")';
      } else {
         echo 'alert("Request Has Been Cancelled.");';
         echo "gotoscrn('scrnWelcome','');";
      }
   }
   function fnsaveDevPlan() {
      include_once 'conn.e2e.php';
      $flds = "";
      $vals = "";
      if (isset($_POST["sint_P1"])) {
         $p1 = getvalue("sint_P1");
      } else {
         $p1 = 0;
      }
      if (isset($_POST["sint_P2"])) {
         $p2 = getvalue("sint_P2");
      } else {
         $p2 = 0;
      }
      if (isset($_POST["sint_P3"])) {
         $p3 = getvalue("sint_P3");
      } else {
         $p3 = 0;
      }
      if (isset($_POST["sint_P4"])) {
         $p4 = getvalue("sint_P4");
      } else {
         $p4 = 0;
      }
      if (isset($_POST["sint_P5"])) {
         $p5 = getvalue("sint_P5");
      } else {
         $p5 = 0;
      }
      $emprefid = getvalue("hEmpRefId");
      $flds .= "`EmployeesRefId`, `DesiredYears`, `SupervisorName`,";
      $flds .= "`Purpose1`, `Purpose2`, `Purpose3`, `Purpose4`, `Purpose5`, `Purpose5Others`,";
      $vals .= "'$emprefid', '".getvalue("sint_desiredyr")."', '".getvalue("char_supervisor")."',";
      $vals .= "'".$p1."', '".$p2."', '".$p3."',";
      $vals .= "'".$p4."', '".$p5."', '".getvalue("p5_exp")."',";
      $result = f_SaveRecord("NEWSAVE","employeesdevtplan",$flds,$vals);
      //echo $result;
      if (is_numeric($result)) {
         $sql = "SELECT * FROM employeesdevtplan WHERE RefId = $result";
         $rs = mysqli_query($conn,$sql);
         if ($rs) {
            $row = mysqli_fetch_assoc($rs);
            for($i=1;$i<=5;$i++) {
               if ($row["Purpose".$i] == 1) {
                  $name = "sint_P".$i;
                  echo '$("[name=\''.$name.'\']").prop("checked",true);';
               } else {
                  echo '$("[name=\''.$name.'\']").prop("checked",false);';
               }
               echo '$("[name=\''.$name.'\']").val("'.$row["Purpose".$i].'");';
            }
            echo '$("[name=\'sint_desiredyr\']").val("'.$row["DesiredYears"].'");';
            echo '$("[name=\'char_supervisor\']").val("'.$row["SupervisorName"].'");';
            echo '$("[name=\'p5_exp\']").val("'.$row["Purpose5Others"].'");';
         }
         echo 'alert("Successfully Save");';
      } else {
         echo 'alert("Error in saving");';
      }
   }
   function fngetName() {
      $refid = getvalue("refid");
      $tbl = getvalue("tbl");
      $objname = getvalue("objname");
      if ($refid == "" || $refid == 0) {
         echo '$("#val_'.$objname.'").html("N/A");';
      } else {
         echo '$("#val_'.$objname.'").html("'.strtoupper(getRecord($tbl,$refid,trim("Name"))).'");';
      }
   }
   function fngetZipCode() {
      $cityrefid = getvalue("cityrefid");
      $obj = getvalue("objcontent");
      $row = FindFirst("city","WHERE RefId = $cityrefid","ZipCode");
      echo '$("#'.$obj.'").val("'.$row.'");';
   }
   function fnAddEmpMovement() {
      require_once "conn.e2e.php";
      $emprefid = getvalue("emprefid");
      $criteria = " WHERE CompanyRefId = ".getvalue("hCompanyID");
      $criteria .= " AND BranchRefId = ".getvalue("hBranchID");
      $criteria .= " AND EmployeesRefId = $emprefid";
      $criteria .= " LIMIT 1";
      $rs = f_Find("empinformation",$criteria);
      if ($rs) {
         $row = array();
         $row = mysqli_fetch_assoc($rs);
         echo json_encode($row);
      }
   }
   function fnsetScrnId() {
      $systemRefId = getvalue("systemRefId");
      $systemCode = FindFirst("system","WHERE RefId = ".$systemRefId,"Code");
      $ScrnID = $systemCode.strtotime("Now");
      echo 'setValueByName("char_ScrnId","'.$ScrnID.'");';
   }
   function fnloadSysUserAccess() {
      $sysuserrefid = getvalue("sysuserrefid");
      $EmployeesRefId = FindFirst("sysuser","WHERE RefId = ".$sysuserrefid,"EmployeesRefId");
      $sys = SelectEach("system","");
      if ($sys) {
         echo
         '<table class="table">
            <tr>
               <th>&nbsp;</th>
               <th>Sys Code</th>
               <th>Sys Name</th>
            </tr>';
         while ($row = mysqli_fetch_assoc($sys)) {
            $SystemRefId = $row["RefId"];
            $chk = "";
            $chkValue = "0";
            $sysusersystems_row = FindFirst("sysusersystems",
                                           "WHERE EmployeesRefId = ".$EmployeesRefId." AND SystemRefId = ".$row["RefId"],
                                           "*");
            if ($sysusersystems_row) {
               $Show = $sysusersystems_row["Show"];
               if ($Show == 1) {
                  $chk = "CHECKED";
                  $chkValue = "1";
                  echo '<tr style="background:var(--bgIntroBlack2);color:white;">';
               } else {
                  $chk = "";
                  $chkValue = "0";
                  echo '<tr>';

               }
            }
               echo
               '<td class="text-center">
                  <div class="checkbox">
                     <label><input type="checkbox" '.$chk.'
                                   value="'.$chkValue.'"
                                   class="systemChk--"
                                   name="chk'.$row["Code"].'"
                                   id="chk'.$row["Code"].'">
                     </label>
                  </div>
               </td>
               <td style="font-size:11pt" class="txt-center">'.$row["Code"].'</td>
               <td style="font-size:11pt">'.$row["Name"].'</td>
            </tr>';
         }
         echo
         '</table>';
      }
   }
   function fnUpdateSysAccess() {
      UpdateSysAccess($_POST["hSelectedSysUSer"],"PIS");
      UpdateSysAccess($_POST["hSelectedSysUSer"],"AMS");
      UpdateSysAccess($_POST["hSelectedSysUSer"],"PMS");
      UpdateSysAccess($_POST["hSelectedSysUSer"],"LDMS");
      UpdateSysAccess($_POST["hSelectedSysUSer"],"RMS");
      UpdateSysAccess($_POST["hSelectedSysUSer"],"SPMS");
      UpdateSysAccess($_POST["hSelectedSysUSer"],"TC");
   }
   function UpdateSysAccess($selectedUser,$sysCode) {
      $EmployeesRefId = FindFirst("sysuser","WHERE RefId = ".$selectedUser,"EmployeesRefId");
      $tables = "sysusersystems";
      if (isset($_POST["chk".$sysCode])) {
         $Show = $_POST["chk".$sysCode];
      } else {
         $Show = 0;
      }
      $SystemRefId = FindFirst("system","WHERE Code = '$sysCode'","RefId");
      $sysusersystems_REFID = FindFirst($tables,"WHERE EmployeesRefId = '".$EmployeesRefId."' AND SystemRefId = '".$SystemRefId."'","RefId");
      if ($sysusersystems_REFID) {
         $flds = "`Show` = '$Show',";
         f_SaveRecord("EDITSAVE",$tables,$flds,$sysusersystems_REFID);
         echo "$.notify('$sysCode Successfully Updated !!!','success');";
      } else {
         $flds = "`EmployeesRefId`,`SystemRefId`,`SysUserRefId`,`Show`,";
         $values = "'$EmployeesRefId','$SystemRefId','$selectedUser','$Show',";
         f_SaveRecord("NEWSAVE",$tables,$flds,$values);
         echo "$.notify('$sysCode Successfully Added !!!','success');";
      }

   }
   function fngetSeminarDes() {
      $idx = getvalue("idx");
      $val = getvalue("val");
      $result = "";
      $result = getRecord("seminars",$val,"Description");
      echo '$("[name=\'char_Description_'.$idx.'\']").val("'.$result.'");';

   }
   function fngetPrivate() {
      $value = getvalue("val");
      $idx = getvalue("idx");
      $obj = getvalue("obj");
      $table = "";
      populateDropDown($obj,$idx,$value);
   }
   function fngetEmpName() {
      $emprefid = getvalue("emprefid");
      $obj = getvalue("obj");
      $row = FindFirst("employees","WHERE RefId = $emprefid","*");
      $name = $row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"];
      echo '$("#'.$obj.'").append(\'<option value="'.$emprefid.'">'.$name.'</option>\');';
   }
   function fngetLeaveCode() {
      $refid = getvalue("refid");
      $code = getRecord("leaves",$refid,"Code");
      $code = strtolower($code);
      if ($code == "vl"){
         echo '$("#char_MaxForceLeave").prop("readonly",false);';
         echo 'setValueByName("deci_Value","1.25")';
      } else {
         if ($code == "sl") {
            echo 'setValueByName("deci_Value","1.25")';
         } else {
            echo '$("#char_MaxForceLeave").prop("readonly",true);';
            echo 'setValueByName("deci_Value","")';
         }
      }
   }
   function fngetPolicyName() {
      $refid = getvalue("refid");
      $objname = getvalue("objname");
      $table = getvalue("tbl");
      if ($refid == "" || $refid == 0) {
         echo 'setValueByName("'.$objname.'","")';
      } else {
         echo 'setValueByName("'.$objname.'","'.strtoupper(getRecord($table,$refid,trim("Name"))).'")';
      }

   }
   function populateDropDown($obj,$idx,$value) {
      $obj_arr = explode("|", $obj);
      $where = "";
      if ($value == 0) {
         $where = "WHERE IsPrivate = 1 ORDER BY Name";
         $hint = "P";
      } else {
         $where = "WHERE IsPrivate = 0 OR IsPrivate IS NULL ORDER BY Name";
         $hint = "G";
      }
      foreach ($obj_arr as $obj) {
         if ($obj != "") {
            $obj_table = explode("_", $obj)[1];
            $table = strtolower(str_replace("RefId", "", $obj_table));
            $objName = $obj.$idx;
            $tbl = $table;
            if ($table == "empstatus") $tbl = "Employment Status";
            echo '$("[name=\''.$objName.'\']").append( $(\'<option>\',{value:0,text:\'SELECT '.strtoupper($tbl).'\'}));';
            $rs = SelectEach($table,$where); {
               if ($rs) {
                  while ($row = mysqli_fetch_assoc($rs)) {
                     $name = str_replace("'", " ", $row["Name"]);
                     echo '$("[name=\''.$objName.'\']").append( $(\'<option>\',{value:'.$row["RefId"].',text:\''.strtoupper($name)." [".$hint.']\'}));'."\n";
                  }
                  //echo "\n".'$("[name=\''.$objName.'\']").val("'.FindLast($table,"","RefId").'");'."\n";
               }
            }
         }
      }
   }

   function fnpopulateFilter() {
      $table = getvalue("table");
      $table = strtolower($table);
      echo '$("#filter_value").append( $(\'<option>\',{value:0,text:\'SELECT '.strtoupper($table).'\'}));';
      $rs = SelectEach($table,""); {
         if ($rs) {
            while ($row = mysqli_fetch_assoc($rs)) {
               $name = str_replace("'", " ", $row["Name"]);
               echo '$("#filter_value").append( $(\'<option>\',{value:'.$row["RefId"].',text:\''.strtoupper($name).'\'}));'."\n";
            }
         }
      }
   }
   function fngetApptStat() {
      include 'conn.e2e.php';
      $rs = SelectEach("apptstatus","WHERE ApptType = ".getvalue("refid")." ORDER BY Name");
      if ($rs) {
         while($row = mysqli_fetch_array($rs)) {
            $name = str_replace("'", " ", $row["Name"]);
            echo '$("[name=\'sint_ApptStatusRefId\']").append("<option value=\''.$row["RefId"].'\'>'.strtoupper($name).'</option>");'."\n";
         }
      }
   }
   function fnupdateEmpActiveness() {
      $refid = getvalue("emprefid");
      $fldnval = "Inactive = '".getvalue("val")."', ";
      $ResignedDate = getvalue("resigned_date");
      if (getvalue("val") == 1) {
         $pms_fldnval      = "`resigned_date` = '$ResignedDate', ";   
         $AgencyId         = FindFirst("employees","WHERE RefId = '$refid'","AgencyId");
         $pms_empid        = pms_FindFirst("pms_employees","WHERE employee_number = '$AgencyId'","id");
         $update_pms       = updatePMS("pms_employee_information",$pms_fldnval,"WHERE employee_id = '$pms_empid'");
      }
      $result = f_SaveRecord("EDITSAVE","employees",$fldnval,$refid,getvalue("user"));
      if ($result != "") {
         echo $result;
      }
   }
   function fngetCreditBalance() {
      $emprefid = getvalue("emprefid");
      $vlobj = getvalue("vlobj");
      $slobj = getvalue("slobj");
      $hBranchID = getvalue("BranchId");
      $hCompanyID = getvalue("CompanyId");
      $where = "WHERE CompanyRefId = $hCompanyID AND BranchRefId = $hBranchID AND EmployeesRefId = $emprefid";
      $where .= " AND EffectivityYear = '".date("Y",time())."'";
      $rs = SelectEach("employeescreditbalance",$where);
      if ($rs) {
         while ($row = mysqli_fetch_assoc($rs)) {
            $bal = $row["BeginningBalance"];
            switch ($row["NameCredits"]) {
               case 'VL':
                  echo 'setValueByName("'.$vlobj.'","'.$bal.'");';
                  if ($bal < 15) {
                     echo '$.notify("No Enough Leave Earnings");';
                  }
                  break;
               case 'SL':
                  echo 'setValueByName("'.$slobj.'","'.$bal.'");';
                  break;
            }
         }
      }
   }
   function fngetSalary() {
      $emprefid = getvalue("emprefid");   
      $obj = getvalue("obj");
      $company = getvalue("CompanyId");
      $branch = getvalue("BranchId");
      $where = "WHERE CompanyRefId = $company AND BranchRefId = $branch AND EmployeesRefId = $emprefid";
      $Salary = FindFirst("empinformation",$where,"SalaryAmount");
      echo 'setValueByName("'.$obj.'","'.$Salary.'");';
   }

   function fncancelLeave() {
      $refid = getvalue("leaverefid");
      $reason = getvalue("reason");
      $row = FindFirst("employeesleave","WHERE RefId = $refid","*");
      if ($row) {
         $creditName    = getRecord("leaves",$row["LeavesRefId"],"Code");
         $DateFrom      = $row["ApplicationDateFrom"];
         $DateTo        = $row["ApplicationDateTo"];
         $date_diff     = dateDifference($DateFrom,$DateTo);
         $empLeaveRefId = $row["RefId"];
         $emprefid      = $row["EmployeesRefId"];
         $fld           = "EmployeesLeaveRefId, Reason, FiledDate, EmployeesRefId, LeavesRefId, ";
         $vals          = "$empLeaveRefId, '$reason', '".date("Y-m-d",time())."', '".$emprefid."', '".$row["LeavesRefId"]."',";
         $save          = f_SaveRecord("NEWSAVE","fl_cancellation_request",$fld,$vals);
         $fldnval       = "Status = 'Request For Cancellation',";
         $update_empleave = f_SaveRecord("EDITSAVE","employeesleave",$fldnval,$empLeaveRefId);
         if ($update_empleave == "") {
            if (is_numeric($save)) {
               echo 'alert("Cancellation of Leave Requested");';
            } else {
               echo "Error ".$save;
            }   
         } else {
            echo "Error ".$update_empleave;
         }
      } else {
         echo 'alert("No Record Found");';
      }
   }

   function fngetWorkExp() {
      $refid = getvalue("refid");
      $row   = FindFirst("employeesworkexperience","WHERE RefId = $refid","*");
      if ($row) {
         $OfficeRefId = FindFirst("empinformation","WHERE EmployeesRefId = ".$row["EmployeesRefId"],"OfficeRefId");
         if ($OfficeRefId) {
            $OfficeRefId = $OfficeRefId;
         } else {
            $OfficeRefId = "";
         }
         echo '$("#sint_WorkExperienceRefId").val("'.$refid.'");'."\n";
         echo '$("#sint_EmployeesRefId").val("'.$row["EmployeesRefId"].'");'."\n";
         echo '$("#sint_EndDate").val("'.$row["WorkEndDate"].'");'."\n";
         echo '$("#sint_AgencyRefId").val("'.$row["AgencyRefId"].'");'."\n";
         echo '$("#sint_PositionRefId").val("'.$row["PositionRefId"].'");'."\n";
         echo '$("#sint_OfficeRefId").val("'.$OfficeRefId.'");'."\n";
         echo '$("#duration").val("'.date("F d, Y",strtotime($row["WorkStartDate"])).' to '.date("F d, Y",strtotime($row["WorkEndDate"])).'");'."\n";
         echo '$("#position").val("'.getRecord("position",$row["PositionRefId"],"Name").'");'."\n";
         echo '$("#agency").val("'.getRecord("agency",$row["AgencyRefId"],"Name").'");'."\n";
         echo '$("#sint_StartDate").val("'.$row["WorkStartDate"].'");'."\n";
         if ($OfficeRefId != "") echo '$("#office").val("'.getRecord("office",$OfficeRefId,"Name").'");'."\n";
      }
   }

   function fnSaveWorkExpAttachment() {
      $obj = getvalue("obj");
      $obj_arr = explode("!", $obj);
      $Flds = "";
      $Vals = "";
      
      foreach ($obj_arr as $key => $value) {
         if ($value != "") {
            $n_arr = explode("|", $value);
            $save_value = realEscape($n_arr[1]);
            if ($save_value != "") {
               $save_fld = explode("_", $n_arr[0])[1];
               $Flds .= "`$save_fld`, ";
               $Vals .= "'$save_value', ";
            }   
         }
      }
      /*echo $Flds."<br>".$Vals;
      return false;*/
      $result = f_SaveRecord("NEWSAVE","employees_work_experience_attachments",$Flds,$Vals,"USER");
      if (is_numeric($result)) {
         echo 'alert("Successfully Saved.");';
         echo 'gotoscrn("userWork_Exp_Attachment","");';
      } else {
         echo $Flds."<br>".$Vals;
         echo 'alert("'.$result.'");';
      }
   }

   function fngetRecordSet() {
      include 'conn.e2e.php';
      $table = getvalue("tbl");
      $refid = getvalue("refid");
      $row = getRecordSet($table,$refid);
      echo json_encode($row);
   }
   function fnTrnSaveRecord() {
      $GLOBALS["trn"]->trnSaveRecord($_POST);
   }
   function fnLoadRecord() {
      $GLOBALS["trn"]->trnLoadRecord(getvalue("refid"));
   }
   function fnLoadDefValue() {
      $GLOBALS["trn"]->trnLoadDefValue();
   }
   function fnPOST() {
      $post = $_POST;
      $moduleContent = file_get_contents(json.$post['json'].".json");
      $module        = json_decode($moduleContent, true);
      include_once $module[$post['elem1']]["trnFile"];
   }
   function fnverifyPW() {
      $u = getvalue("u");
      $pw = getvalue("s");
      $criteria  = " WHERE RefId = '$u'";
	   $criteria .= " AND Password = '$pw'";
      $userRefId = FindFirst("sysuser",$criteria,"RefId");
      if ($userRefId != $u) {
         echo '
            $("#tSecret").val("");
            $("#tSecret").focus();
            $("#tSecret").select();
            $("#tSecret").notify("Ooops!!! Incorrect Password, Not Verified");
         ';
      } else {
         echo '
            if (confirm("Press OK to confirm !!! Thank you")) {
               var frm = document.xForm;
               var valtoken = frm.dummy.value;
               frm.dummy.value = "dbgebm";
               frm.hToken.value = hash(valtoken + "123");
               frm.submit();
            }
         ';
      }
   }
   function fnNewPageSession() {
      $_SESSION["pageSession".$GLOBALS["user"]] = "";
      $_SESSION["pageSession".$GLOBALS["user"]] = date("Ymdhi");
      echo $_SESSION["pageSession".$GLOBALS["user"]];
   }

   /*DONT MODIFY HERE*/
   $funcname = "fn".getvalue("fn");
   $params   = getvalue("params");
   if (!empty($funcname)) {
      $funcname($params);
   } else {
      echo 'alert("Error... No Function defined");';
   }

?>