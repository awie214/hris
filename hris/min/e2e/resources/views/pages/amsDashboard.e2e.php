<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="postMultiple.e2e.php">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar($paramTitle); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-8">
                           <div class="panel panel-default">
                              <div class="panel-heading ">PENDING APPROVALS:</div>
                              <div class="panel-body">
                                 <ul class="list-group">
                                    <li class="list-group-item counts--" onclick="gotoscrn('amsAppvLeaveAvail','');">
                                       Leave Applications:
                                       <?php 
                                          $leave_count = SelectEach("employeesleave","WHERE Status IS NULL");
                                          if ($leave_count) {
                                             $leave_count = mysqli_num_rows($leave_count);
                                          } else {
                                             $leave_count = 0;
                                          }
                                       ?>
                                       <span class="badge"><?php echo $leave_count; ?></span>
                                    </li>
                                    <li class="list-group-item counts--" onclick="gotoscrn('amsAppvOfficeAuthority','');">
                                       Office Authority Applications:
                                       <?php 
                                          $authority_count = SelectEach("employeesauthority","WHERE Status IS NULL");
                                          if ($authority_count) {
                                             $authority_count = mysqli_num_rows($authority_count);
                                          } else {
                                             $authority_count = 0;
                                          }
                                       ?>
                                       <span class="badge"><?php echo $authority_count; ?></span>
                                    </li>
                                    <li class="list-group-item counts--" onclick="gotoscrn('amsAppvCTOAvail','');">
                                       CTO Applications:
                                       <?php 
                                          $CTO_count = SelectEach("employeescto","WHERE Status IS NULL");
                                          if ($CTO_count) {
                                             $CTO_count = mysqli_num_rows($CTO_count);
                                          } else {
                                             $CTO_count = 0;
                                          }
                                       ?>
                                       <span class="badge"><?php echo $CTO_count; ?></span>
                                    </li>
                                    <li class="list-group-item counts--" onclick="gotoscrn('amsAppvMonetization','');">
                                       Leave Monetization Applications:
                                       <?php 
                                          $monetization_count = SelectEach("employeesleavemonetization","WHERE Status IS NULL");
                                          if ($monetization_count) {
                                             $monetization_count = mysqli_num_rows($monetization_count);
                                          } else {
                                             $monetization_count = 0;
                                          }
                                       ?>
                                       <span class="badge"><?php echo $monetization_count; ?></span>
                                    </li>
                                    <li class="list-group-item counts--" onclick="gotoscrn('amsAppvAttendanceRegistration','');">
                                       Attendance Registration Request:
                                       <?php 
                                          $attendancereq_count = SelectEach("attendance_request","WHERE Status IS NULL");
                                          if ($attendancereq_count) {
                                             $attendancereq_count = mysqli_num_rows($attendancereq_count);
                                          } else {
                                             $attendancereq_count = 0;
                                          }
                                       ?>
                                       <span class="badge"><?php echo $attendancereq_count; ?></span>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <?php
               footer();
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>

   </body>
</html>