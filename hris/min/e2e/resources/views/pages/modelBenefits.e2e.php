<?php
   require_once "constant.e2e.php";
   require_once "inc_model.e2e.php";
   require_once "incUtilitiesJS.e2e.php";
?>
   <div class="container" id="EntryScrn">
      <div class="row" id="EntryScrn">
         <div class="col-xs-1"></div>
         <div class="col-xs-10">
            <?php if ($ScrnMode != 1) { ?>
               <div class="row">
                  <ul class="nav nav-pills">
                     <li class="active" style="font-size:12pt;font-weight:600;">
                        <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                        <?php echo $refid; ?>
                        </span></a>
                     </li>
                  </ul>
               </div>
            <?php } ?>
            <div class="row">
               <div class="col-xs-6">
                  <div class="row margin-top10">
                     <div class="form-group">
                        <label for="inputs">CODE:</label><br>
                        <input class="form-input saveFields-- uCase-- mandatory" type="text" name="char_Code"
                           placeholder="Code" <?php if ($ScrnMode == 2) { echo "disabled"; } else { echo $disabled; }  ?>
                           id="inputs" name="char_Code" style='width:100%' autofocus>
                     </div>
                  </div>
                  <div class="row">
                     <div class="form-group">
                        <label  for="inputs">NAME:</label><br>
                        <input class="form-input saveFields-- mandatory"
                               type="text" placeholder="name"
                               id="inputs" name="char_Name"
                               style='width:100%'>
                     </div>
                  </div>
                  <div class="row">
                     <div class="form-group">
                        <?php createSelectTaxType(); ?>
                     </div>
                  </div>
                  <div class="row">
                     <div class="form-group">
                        <?php createSelectMinimisType(); ?>

                     </div>
                  </div>
                  <div class="row">
                     <div class="form-group">
                        <?php createSelectComputationType(); ?>
                     </div>
                  </div>
                  <div class="row">
                     <div class="form-group">
                        <label for="inputs">Amount:</label><br>
                        <input class="form-input saveFields-- mandatory number--"
                               type="text" placeholder="Amount"
                               id="inputs" name="char_Amount">
                     </div>
                  </div>
                  <div class="row">
                     <div class="form-group">
                        <?php createSelectPayrollGroup(); ?>
                     </div>
                  </div>
                  <div class="row">
                     <div class="form-group">
                        <label for="inputs">REMARKS:</label>
                        <textarea class="form-input saveFields--" rows="5" name="char_Remarks" <?php echo $disabled; ?>
                        placeholder="remarks"><?php echo $remarks; ?></textarea>
                     </div>
                  </div>
               </div>
               <div class="col-xs-6">
                  <div class="mypanel">
                     <div class="panel-top">Reporting</div>
                     <div class="panel-mid">
                        <div class="row padd10">
                           <?php createSelectITRClassification(); ?>
                        </div>
                        <div class="row padd10">
                           <?php createSelectAlphalistClassification(); ?>
                        </div>
                     </div>
                  </div>
                  <?php spacer(10) ?>
                  <div class="mypanel">
                     <div class="panel-top">
                        Include in Computation
                     </div>
                     <div class="panel-mid">
                        <div class="row">
                           <div class="col-xs-4">
                              <input type="checkbox" name="chkSSS" class="chkIncComp--" string="SSS">&nbsp;<label>SSS</label>
                           </div>
                           <div class="col-xs-4">
                              <input type="checkbox" name="chkGSIS" class="chkIncComp--" string="GSIS">&nbsp;<label>GSIS</label>
                           </div>
                           <div class="col-xs-4">
                              <input type="checkbox" name="chkPhilHealth" class="chkIncComp--" string="PHILHEALTH">&nbsp;<label>Philhealth</label>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-4">
                              <input type="checkbox" name="chkPAGIBIG" class="chkIncComp--" string="PAGIBIG">&nbsp;<label>Pag-IBIG</label>
                           </div>
                           <div class="col-xs-4">
                              <input type="checkbox" name="chkBASICSALARY" class="chkIncComp--" string="BASICSALARY">&nbsp;<label>Basic Salary</label>
                           </div>
                           <div class="col-xs-4">
                              <input type="checkbox" name="chkOVERTIMEPAY" class="chkIncComp--" string="OVERTIMEPAY">&nbsp;<label>Overtime Pay</label>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-4">
                              <input type="checkbox" name="chk13MONTHPAY" class="chkIncComp--" string="13MONTHPAY">&nbsp;<label>13th Month Pay</label>
                           </div>
                           <div class="col-xs-8">
                              <input type="checkbox" name="chkANNUALIZED" class="chkIncComp--" string="ANNUALIZED">&nbsp;<label>Annualized(Assumed)</label>
                           </div>
                        </div>
                        <input type="hidden" name="char_IncludeComputation" class="saveFields--" value="">
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-xs-1"></div>

      </div>
   </div>