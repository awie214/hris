<?php
   session_start();
   include_once "constant.e2e.php";
   include_once pathClass.'0620functions.e2e.php';
 
   $moduleContent = file_get_contents(json.getvalue("json").'.json');
   $module        = json_decode($moduleContent, true); 
   $dbField       = $module[getvalue("elem1")]["dbField"];
   $dbTable       = $module[getvalue("elem1")]["dbTable"];
   $Fields        = "";
   $Values        = "";
   $Fldnval       = "";
   $err           = "";
   $table         = getvalue("hTable");
   if ($dbTable == "") {
      $table = $dbTable;
   }
   $EmpSelected         = getvalue("hEmpSelected");
   $Name                = getvalue("char_Name");
   $StartDate           = getvalue("date_StartDate");
   $StartTime           = getvalue("StartTime");
   $StartTime_Mode      = getvalue("StartTime_mode");
   $EndDate             = getvalue("date_EndDate");
   $EndTime             = getvalue("EndTime");
   $EndTime_Mode        = getvalue("EndTime_mode");
   $Remarks             = getvalue("char_Remarks");
   $FiledDate           = getvalue("date_FiledDate");
   $ApplicationDateFrom = getvalue("date_ApplicationDateFrom");
   $ApplicationDateTo   = getvalue("date_ApplicationDateTo");
   $balance             = getvalue("deci_Balance");
   $RefId               = getvalue("hRefId");
   /*echo "RefId: ".$RefId."\n";
   return false;*/

   if (getvalue("hUserGroup") == "COMPEMP") {
      $EmpSelected = getvalue("hEmpRefId");
   }
   $EmployeesArray = explode(":",$EmpSelected);
   if (trim($EmpSelected) != "") {
      foreach ($EmployeesArray as $EmpRefId) {
         $counter = 0;
         $Fldnval = "";
         $worksched = FindFirst("empinformation","WHERE EmployeesRefId = $EmpRefId","WorkscheduleRefId");
         if (is_numeric($worksched)) {
            switch ($table) {
               case "officesuspension":
                  $StartTimeInMin = $StartTime;
                  if ($StartTime_Mode == "PM") {
                     $StartTimeInMin = $StartTimeInMin + (12 * 60);
                  }
                  $EndTimeInMin = $EndTime;
                  if ($EndTime_Mode == "PM") {
                     $EndTimeInMin = $EndTimeInMin + (12 * 60);
                  }
                  $Fields = "`Name`,`StartDate`,`StartTime`,`EmployeesRefId`,`Remarks`,`EndDate`,";
                  $Values = "'$Name','$StartDate','$StartTimeInMin','$EmpRefId','$Remarks','$EndDate',";
                  $Fldnval = "Name = '$Name', StartDate = '$StartDate', StartTime = '$StartTimeInMin', ";
                  $Fldnval .= "Remarks = '$Remarks', ";
               break;
               /*case "employeescreditbalance":
                  $NameCredits = getvalue("char_NameCredits");
                  if ($NameCredits == "OT") {
                     $EffectivityYear     = getvalue("sint_EffectivityYearOT");
                     $BegBalAsOfDate      = getvalue("date_BegBalAsOfDateOT");
                     $OutstandingBalance  = getvalue("sint_OutstandingBalance");
                     $ExpiryDate          = getvalue("date_ExpiryDate");
                     $Fields = "`BegBalAsOfDate`,`OutstandingBalance`,`ExpiryDate`,`EffectivityYear`,`NameCredits`,`EmployeesRefId`,`Remarks`,";
                     $Values = "'$BegBalAsOfDate','$OutstandingBalance','$ExpiryDate','$EffectivityYear','$NameCredits','$EmpRefId','$Remarks',";
                  } else {
                     $EffectivityYear     = getvalue("sint_EffectivityYear");
                     $BegBalAsOfDate      = getvalue("date_BegBalAsOfDate");
                     $BeginningBalance    = getvalue("sint_BeginningBalance");
                     $ForceLeave          = getvalue("sint_ForceLeave");
                     $Fields = "`BegBalAsOfDate`,`BeginningBalance`,`ForceLeave`,`EffectivityYear`,`NameCredits`,`EmployeesRefId`,`Remarks`,";
                     $Values = "'$BegBalAsOfDate','$BeginningBalance','$ForceLeave','$EffectivityYear','$NameCredits','$EmpRefId','$Remarks',";
                  }
                  break;*/
               default:
                  $fv = parseFieldnValue($dbField,$_POST);
                  $Fields  = $fv["Fields"]."`EmployeesRefId`,";
                  $Values  = $fv["Values"]."'$EmpRefId',";
                  $Fldnval = $fv["FieldsValues"];
               break;
            }
            if ($table == 'employeesleave' || $table == "employeescto") {
               if ($table == "employeescto") {
                  $leave = dateDifference(getvalue("date_ApplicationDateFrom"),getvalue("date_ApplicationDateTo"));
                  $hrs = getvalue("deci_Hours");
                  $leave = $hrs * $leave;
                  $credit = 'OT';
               } else {
                  $leave = dateDifference(getvalue("date_ApplicationDateFrom"),getvalue("date_ApplicationDateTo"));
                  $leave_code = getRecord("leaves",getvalue("sint_LeavesRefId"),"Code");
                  if ($leave_code == "VL") {
                     $credit = "VL";
                  } else if ($leave_code == "SL") {
                     $credit = "SL";
                  }
               }         
               $where = "WHERE EmployeesRefId = $EmpRefId";
               $where .= " AND NameCredits = '".$credit."'";
               $where .= " AND EffectivityYear = '".date("Y",time())."'";
               $row   = FindFirst("employeescreditbalance",$where,"*");
               if ($row["OutstandingBalance"] != "") {
                  $bal = $row["OutstandingBalance"];
               } else {
                  $bal = $row["BegBalAsOfDate"];
               }
               /*echo "Balance: ".$bal."<br>";
               echo "Leave: ".$leave."<br>";
               return false;*/
               if ($credit == "OT") {
                  /*if ($bal != "") {
                     if ($bal < $leave) {
                        $counter++;
                        $err .= "Employee ".$EmpRefId." has no Enough ".$credit." Earnings.";
                        echo $err;
                     }   
                  } else {
                     $counter++;
                     $err .= "Employee ".$EmpRefId." has no ".$credit." Earnings.";
                     echo $err;
                  }*/
               } else {
                  if ($bal != "") {
                     if ($bal < $leave) {
                        $Fields .= 'WithPay,';
                        $Values .= '0,';
                     } else {
                        $Fields .= 'WithPay,';
                        $Values .= '1,';
                     }
                  } else { 
                     $Fields .= 'WithPay,';
                     $Values .= '0,';
                  }
               }
            }
            /*echo "Balance: ".$bal."<br>Leave: ".$leave."<br>";
            echo $Fields."<br>".$Values."<br>";
            return false;*/
            /*
            switch ($table) {
               case 'employeesleave':
                  $leave = dateDifference(getvalue("date_ApplicationDateFrom"),getvalue("date_ApplicationDateTo"));
                  $where = "WHERE EmployeesRefId = $EmpRefId AND NameCredits = 'VL' AND EffectivityYear = '".date("Y",time())."'";
                  $row   = FindFirst("employeescreditbalance",$where,"OutstandingBalance");
                  if ($row < $leave) {
                     $counter++;
                     $err .= "Employee ".$EmpRefId." has no Enough Earnings.";
                     echo $err;
                  }
                  break;
            }
            */
            
            // if ($table != "officesuspension") {
            //    $checkRequest = FindFirst($table,"WHERE EmployeesRefId = ".$EmpRefId." AND Status IS NULL OR Status = ''","RefId");
            //    if ($checkRequest) {
            //       $counter++;
            //    } else {
            //       if (isset($module[getvalue("elem1")]["field.EndDate"])) {
            //          $enddate_valid = $module[getvalue("elem1")]["field.EndDate"];
            //          $where = "WHERE EmployeesRefId = ".$EmpRefId." AND Status = 'Approved' AND ".$enddate_valid." >= '".date("Y-m-d",time())."'";
            //          $checkRequest = FindFirst($table,$where,"RefId");   
            //          if ($checkRequest) {
            //             $counter++;
            //             $err .= "Employee ".$EmpRefId." has already filed request.";
            //             echo $err;
            //          }
            //       }
            //    }   
            // }
            
            //echo $counter;
            //return false;
            if ($counter == 0) {
               $check = FindFirst($table,"WHERE RefId = ".$RefId,"*");
               if ($check) {
                  //echo $Fldnval;
                  $update = f_SaveRecord("EDITSAVE",$table,$Fldnval,$RefId);  
                  if ($update != "") {
                     $err .= $update;
                  }
               } else {
                  /*echo $Fields."<br>";
                  echo $Values;
                  return false;*/
                  $check_where = "WHERE EmployeesRefId = '$EmpRefId'";
                  if ($table != "assigned_courses") {
                     if ($table == "overtime_request") {
                        $check_where .= " AND StartDate = '$StartDate'";
                        $check_where .= " AND EndDate = '$EndDate'";
                     } else {
                        $check_where .= " AND ApplicationDateFrom = '$ApplicationDateFrom'";
                        $check_where .= " AND ApplicationDateTo = '$ApplicationDateTo'";
                     }   
                  } else {
                     $check_where .= " AND FiledDate = '$FiledDate'";
                  }
                  
                  $check_application = FindFirst($table,$check_where,"RefId");
                  if (intval($check_application) > 0) {
                     $err .= "Application already exist.";
                  } else {
                     $SaveSuccessfull = f_SaveRecord("NEWSAVE",$table,$Fields,$Values);
                     if (!is_numeric($SaveSuccessfull)) {
                        $err .= $SaveSuccessfull;
                     }   
                  }
               }
            }
         } else {
            $err .= "Employee $EmpRefId has no Workschedule.";
            echo $err;
         }
      }
   } else {
      $err .= "No Employees Selected";
      echo $err;
   }
   $gPARAM = "";
   $gPARAM .= getvalue("hgParam");
   $gPARAM .= "&hProg=".getvalue("hProg");
   $gPARAM .= "&hTable=".$table;
   $gPARAM .= "&hmemof=".getvalue("hmemof");
   $gPARAM .= "&hModId=".getvalue("hModId");
   $gPARAM .= "&hmode=VIEW";
   $gPARAM .= "&errmsg=".$err;
   header ("Location:GlobalCaller.e2e.php?".$gPARAM);
   return;
?>