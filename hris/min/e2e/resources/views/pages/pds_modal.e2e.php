   <div class="modal fade border0" id="pds_modal" role="dialog">
      <div class="modal-dialog border0" style="padding:0px;width:9in;height:92%;">
         <div class="mypanel border0" style="height:100%;">
            <div class="panel-top bgSilver">
               <a href="#" title="Print Now" id="btnPRINTNOW" style="text-decoration: none;">
                  <i class="fa fa-print" aria-hidden="true"></i>&nbsp;<label>PRINT PDS</label>
               </a>
               <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
         </div>
      </div>
   </div>