<!DOCTYPE html>
<html>
<body>

<?php
require "constant.e2e.php";
require "conn.e2e.php";
require_once pathClass."0620functions.e2e.php";
   $conn->query("TRUNCATE TABLE `employeesrecognitionandreward`");
   $conn->query("TRUNCATE TABLE `awards`");
   
   $t = time();
   $date_today    = date("Y-m-d",$t);
   $curr_time     = date("H:i:s",$t);
   $trackingA_fld = "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
   $trackingA_val = "'$date_today', '$curr_time', 'PHP', 'M'";

   
   
   
   
   $sql = "SELECT * FROM `employee_rr`";
   $rs = mysqli_query($conn,$sql) or die(mysqli_error($conn));
   echo "Number Records : ".mysqli_num_rows($rs)."<br>";
   if ($rs) {
      if (mysqli_num_rows($rs) > 0) {
         while ($row = mysqli_fetch_array($rs)) {
            $award = $row['award'];
            
            $wAward = strtoupper(remquote($award));
            $wAwardRefId = fileManager($wAward,
                                 "awards",
                                 "`Code`,`Name`,`Remarks`,",
                                 "'','".$wAward."','Migrated',");
            $AgencyId = $row['idno'];
            $EmpRefId = FindFirst("employees","WHERE AgencyId = $AgencyId","RefId");
            $year = $row['year_received'];
            $time = $row['timestamp'];
            
            $vals = "1000,1,'$EmpRefId','$AgencyId','$wAwardRefId','$year','$time',".$trackingA_val;
            $flds = "`CompanyRefId`,`BranchRefId`,`EmployeesRefId`,`EmployeesId`,`AwardsRefId`,`YearReceived`,`TimeReceived`,".$trackingA_fld;
            $conn->query("INSERT INTO `employeesrecognitionandreward` ($flds) VALUES ($vals)");
            
            
         }
      }
   }
?>
</body>
</html>