<?php

   require_once "constant.e2e.php";
   require_once "conn.e2e.php";
   require_once pathClass."0620functions.e2e.php";
   
   $dbmPOSITION_Refid = 0;
   $dbmOFFICE_Refid = 0;
   $dbmDIVISION_Refid = 0;
   $INTERIMPOSITION_Refid = 0;
   $INTERIMOFFICE_Refid = 0; 
   $INTERIMDIVISION_Refid = 0;
   $PositionItem_Refid = 0;                                        
   
?>
<!DOCTYPE html>
<html>
   <script type="text/javascript" src="<?php echo $path; ?>jquery/jquery.js"></script>
   <script type="text/javascript" src="<?php echo $path; ?>js/js7_login.js"></script>
   <script type="text/javascript" src="<?php echo $path; ?>js/jsSHAver2/src/sha.js"></script>
   <script>
      $(document).ready(function () {
      });
      
      function setPW(pw,empRefId) {
         pw = calcHash(pw);
         $.post("setpw.php",
         {
            pw:pw,
            empRefId:empRefId
         },
         function(data,status) {
            if (status == "success") {
               code = data;
               try {
                  eval(code);
               } catch (e) {
                   if (e instanceof SyntaxError) {
                       alert(e.message);
                   }
               }
            }
            else {
               alert("Ooops Error : " + status + "[005]");
            }
         });
      }
   </script>
   
<body>

<?php
$f = path."migration/EMPLIST_NUMBER.csv";
$myfile = fopen($f, "r") or die("Unable to open file!");

         $conn->query("TRUNCATE TABLE `employees`;");
         $conn->query("TRUNCATE TABLE `empinformation`;");
         
         $t = time();
         $date_today    = date("Y-m-d",$t);
         $curr_time     = date("H:i:s",$t);
         $trackingA_fld = "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
         $trackingA_val = "'$date_today', '$curr_time', 'PHP', 'M'"; 

         $loop = 0;
         while(!feof($myfile)) {
            $str = fgets($myfile);
            $str_Arr = explode(",",$str);
            $loop++;
            if ($loop > 1) {

            
               $null = "";
               $wRemarks = "Uploaded";
               $wisFilipino = "1";
               $wisByBirthOrNatural = "1";
               
               $AgencyId = $str_Arr[0];
               $LastName = $str_Arr[1];
               $FirstName = $str_Arr[2];
               $MiddleName = $str_Arr[3];
               $ExtName = $str_Arr[4];
               $PositionItem = $str_Arr[5];
               $dbmPOSITION = $str_Arr[6];
               $dbmOFFICE = $str_Arr[7];
               $dbmDIVISION = $str_Arr[8];
               
               $INTERPOSITION = $str_Arr[9];
               $INTEROFFICE = $str_Arr[10];
               $INTERDIVISION = $str_Arr[11];
               
               $USERNAME = $str_Arr[12];   
               $EmailAdd = $str_Arr[13];
               $AssumptionDate = $str_Arr[14];
               $Status = $str_Arr[15];
               
               
               $AssumptionDate_Extract = explode ("/",$AssumptionDate);
               $month = $AssumptionDate_Extract[0];
               $day = $AssumptionDate_Extract[1];
               $year = $AssumptionDate_Extract[2];
               if ($month <= 9) $month = "0".$month;
               if ($day <= 9) $day = "0".$day;
               $AssumptionDate = $year."-".$month."-".$day;
               
               /*if ($EmailAdd != "") {
                  $EmailAdd_Arr = explode("@",$EmailAdd);
                  if (count($EmailAdd_Arr) == 2) {
                     $pw = $EmailAdd_Arr[0]."123";
                  } else {
                     $pw = $LastName."123";   
                  }
               } else {
                  $pw = $LastName."123";
               }
               */
               $pw = $USERNAME."123";
               $encryptPW = f_encode($pw);
               
               $flds = "`LastName`,`FirstName`,`MiddleName`,`EmailAdd`,`isByBirthOrNatural`,`ExtName`,`Username`,";
               $flds .= "`isFilipino`,`AgencyId`,`CompanyRefId`,`BranchRefId`,".$trackingA_fld;
               $values = "'$LastName','$FirstName','$MiddleName','$EmailAdd','$wisByBirthOrNatural','$ExtName','$USERNAME',";
               $values .= "'$wisFilipino','$AgencyId','2','1',".$trackingA_val;
               
               
               
               $isExist = FindFirst("employees","where LastName = '$LastName' AND FirstName = '$FirstName'","*");

               if (!$isExist) {
                  $sql = "INSERT INTO `employees` ($flds) VALUES ($values)";
                  if ($conn->query($sql) === TRUE) {
                     $EmployeesRefId = mysqli_insert_id($conn);
                     
                     /*$POSITION_RefId = fileManager($POSITION,
                                        "position",
                                        "`Code`,`Name`,`Remarks`,",
                                        "'','$POSITION','auto',");
                     $OFFICE_RefId = fileManager($OFFICE,
                                        "office",
                                        "`Code`,`Name`,`Remarks`,",
                                        "'','$OFFICE','auto',");
                     $DIVISION_RefId = fileManager($DIVISION,
                                        "division",
                                        "`Code`,`Name`,`Remarks`,",
                                        "'','".$DIVISION."','auto',");*/
                     
                     if ($dbmPOSITION != "") {
                        $dbmPOSITION_Refid = fileManager($dbmPOSITION,
                                           "position",
                                           "`Code`,`Name`,`Remarks`,",
                                           "'','$dbmPOSITION','auto',");
                        echo $dbmPOSITION." - $dbmPOSITION_Refid<br>";                   
                     }           
                     
                     if ($dbmOFFICE != "") {
                        $dbmOFFICE_Refid = fileManager($dbmOFFICE,
                                           "office",
                                           "`Code`,`Name`,`Remarks`,",
                                           "'','$dbmOFFICE','auto',");                    
                     }
                     
                     if ($dbmDIVISION != "") {
                        $dbmDIVISION_Refid = fileManager($dbmDIVISION,
                                           "division",
                                           "`Code`,`Name`,`Remarks`,",
                                           "'','$dbmDIVISION','auto',");                                           
                     }
                     
                     if ($INTERPOSITION != "") {
                           $INTERIMPOSITION_Refid = fileManager($INTERPOSITION,
                                           "interimposition",
                                           "`Code`,`Name`,`Remarks`,",
                                           "'','$INTERPOSITION','auto',");
                     }
                     
                     if ($INTEROFFICE != "") {
                        $INTERIMOFFICE_Refid = fileManager($INTEROFFICE,
                                           "interimoffice",
                                           "`Code`,`Name`,`Remarks`,",
                                           "'','$INTEROFFICE','auto',");                    
                     }
                     
                     if ($INTERDIVISION != "") {
                        $INTERIMDIVISION_Refid = fileManager($INTERDIVISION,
                                           "interimdivision",
                                           "`Code`,`Name`,`Remarks`,",
                                           "'','$INTERDIVISION','auto',");                                                                                         
                     }
                     
                     
                     if ($PositionItem != "") {
                        $PositionItem_Refid = fileManager($PositionItem,
                                           "positionitem",
                                           "`Code`,`Name`,`Remarks`,",
                                           "'$PositionItem','$PositionItem','auto',");                    
                     }
                     
                     switch ($Status) {
                        case "P":
                           $Status_Name = "Permanent";
                        break; 
                        case "T":
                           $Status_Name = "Temporary";
                        break; 
                     }
                     
                     if ($Status_Name != "")  {
                           $Status_RefId = fileManager($Status_Name,
                                    "empstatus",
                                    "`Code`,`Name`,`Remarks`,",
                                    "'$Status','$Status_Name','auto',");
                     }
                     
                                    
                     $flds  = "`CompanyRefId`, `BranchRefId`, `EmployeesRefId`, `HiredDate`, `AssumptionDate`,"; 
                     $flds .= "`PositionRefId`, `OfficeRefId`, `DivisionRefId`,"; 
                     $flds .= "`InterimPositionRefId`, `InterimOfficeRefId`, `InterimDivisionRefId`,";
                     $flds .= "`EmpStatusRefId`,`PositionItemRefId`,`AgencyRefId`,".$trackingA_fld;
                     
                     $values = "'2','1','$EmployeesRefId','$AssumptionDate','$AssumptionDate',";
                     $values .= "'$dbmPOSITION_Refid','$dbmOFFICE_Refid','$dbmDIVISION_Refid',";
                     $values .= "'$INTERIMPOSITION_Refid','$INTERIMOFFICE_Refid','$INTERIMDIVISION_Refid',";
                     $values .= "'$Status_RefId','$PositionItem_Refid','1',".$trackingA_val; 
                     echo $values;
                     $sql = "INSERT INTO `empinformation` ($flds) VALUES ($values)";
                     echo $sql."<br>";
                     if ($conn->query($sql) === TRUE) {
                        echo "Success!!! -->$pw ".$LastName.", ".$FirstName."<br>";      
                        echo "<script>";
                        echo "setPW('$pw',$EmployeesRefId);";
                        echo "</script>";
                        
                     } else {
                        echo "No Emp. Information Saved ".$conn->error;
                     } 
                  } else {
                     echo "$loop Err:-->".$str."<br>".$EmployeesRefId."<br>";
                  }
                  echo "<br>";
               } else {
                  echo "This Name is Already Exist ---->> ".$wLastName.", ".$wFirstName."<br>";
               }
            } 
         }
         

fclose($myfile);
?>

</body>
</html>