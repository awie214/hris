<?php 
	include 'FnUpload.php';
	$count = 0;
	$emp_personal_array = [
		"LastName","FirstName","MiddleName","Extname","IsFilipino","BirthDate","BirthPlace","Sex",
		"CivilStatus","Weight","Height","BloodTypeRefId","GSIS","PAGIBIG","PHIC","SSS","TIN","AgencyId",
		"TelNo","MobileNo","EmailAdd","ResiHouseNo","ResiStreet","ResiSubd",
		"ResiBrgy","ResiAddCityRefId","ResiAddProvinceRefId","PermanentHouseNo",
		"PermanentStreet","PermanentSubd","PermanentBrgy","PermanentAddCityRefId",
		"PermanentAddProvinceRefId","GovtIssuedID","GovtIssuedIDNo","GovtIssuedIDPlace","GovtIssuedIDDate"
	];
	$file = fopen("csv/35/personal_info.csv", "r");
	while(!feof($file)) {
		$fldnval = "";
		$str = fgets($file);
		$row = explode(",", $str);
		if (count($row) > 1) {
			
			$row[0] = intval($row[0]);
			$where = "WHERE AgencyId = '".$row[0]."'";
			$emp_row = FindFirst("employees",$where,"*",$conn);
			if ($emp_row) {
				$emprefid = $emp_row["RefId"];
				$AgencyId = $emp_row["AgencyId"];
				$count++;
				for ($a=1; $a <= 36; $a++) { 
					if ($emp_personal_array[$a - 1] == "CivilStatus") {
						switch ($row[$a]) {
							case 'M':
								$row[$a] = "Ma";
								break;
							case 'S':
								$row[$a] = "Si";
								break;
							case 'A':
								$row[$a] = "An";
								break;
							default:
								$row[$a] = "";
								break;
						}	
					}
					$value = clean($row[$a]);
					if ($emp_personal_array[$a - 1] != "AgencyId") {
						if ($value != "") {

							$fldnval .= "`".$emp_personal_array[$a - 1]."` = '".$value."',";	
						}
					}
					
				}
				$update = update("employees",$fldnval,$emprefid);
				if ($update == "") {
					echo "Employee $AgencyId record updated.<br>";
				} else {
					echo "Error in Updating for employee $AgencyId.<br>";
				}
				//echo $fldnval."<br><br>";
				//echo "$count. ".$emp_row["FirstName"]." ".$emp_row["LastName"]." ".$emp_row["AgencyId"]."<br>";
			} else {
				//echo "$count. ".$row[2]." ".$row[1]." ".$row[0]." -> Couldnt Find<br>";
			}
		}
		
	}
?>