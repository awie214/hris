<?php
	include 'FnUpload.php';
	mysqli_query($conn,"DELETE FROM employeescreditbalance WHERE NameCredits = 'OT'");
	$expiry_date = "2019-12-31";

	$file = fopen("csv/coc_1000.csv", "r");
	while(!feof($file)) {
		$str = fgets($file);
		$new_str = str_replace(",", "|", $str);
		$str_arr = explode("|", $new_str);
		$empid = $str_arr[0];
		$coc = floatval($str_arr[1]);
		if (is_deci($coc) == 1) {
			$coc_arr = explode(".", $coc);
			$coc = ($coc_arr[0] * 60) + $coc_arr[1];
		} else {
			$coc = $coc * 60;
		}
		$row = FindFirst("employees","WHERE AgencyId = '$empid'","*",$conn);
		if ($row) {
			$RefId = $row["RefId"];
			$LastName = $row["LastName"];
			$FirstName = $row["FirstName"];

			$fld = "NameCredits,CompanyRefId,BranchRefId,EmployeesRefId,EffectivityYear,BeginningBalance,ExpiryDate,";
			$val = "'OT',1000,1,$RefId,2017,'$coc','$expiry_date',";
			$result = save("employeescreditbalance",$fld,$val);
			if (is_numeric($result)) {
				echo "Overtime Credit Uploaded for $LastName $FirstName.<br>";
			} else {
				echo "Error In Uploading Overtime Credit Of $LastName $FirstName.<br>";
			}

		} else { 
			echo "Employee Number $empid <b>NO Match Found</b>.<br>";
		}
	}
	function is_deci($val) {
		return is_numeric($val) && floor($val) != $val;
	}
?>