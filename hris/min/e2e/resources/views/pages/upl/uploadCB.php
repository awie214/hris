<?php
include 'conn.e2e.php';
include 'constant.e2e.php';
include pathClass.'0620functions.e2e.php';
include "pageHEAD.e2e.php";

$file = fopen(textFile."PIDS_BegBal.csv","r");
$file_new = fopen(textFile."noVLSL.txt","a+");


$date_today    = date("Y-m-d",time());
$curr_time     = date("H:i:s",time());
$trackingflds = "`LastUpdateBy`, `LastUpdateDate`, `LastUpdateTime`, `Data`";
$trackingvals = "'SYSTEM', '$date_today', '$curr_time', 'A'";
$j = 0;
//TRUNCATE FIRST THE CREDIT BALANCE TABLE
//mysqli_query($conn,"TRUNCATE employeescreditbalance");

while(! feof($file)) {
	$j++;
  	$filex = fgets($file);
  	$file_arr = explode(",", $filex);
  	$agencyID = $file_arr[0];
  	$vl = $file_arr[1];
  	$sl = $file_arr[2];
  	$sql = "SELECT * FROM `employees` WHERE AgencyId = '$agencyID'";
  	$rs = mysqli_query($conn,$sql);
  	echo "LINE NO: ".$j."<br>";
  	if (mysqli_num_rows($rs) > 0) {
  		$row = mysqli_fetch_assoc($rs);
  		$emprefid = $row["RefId"];
  		$companyrefid = $row["CompanyRefId"];
  		$branchrefid = $row["BranchRefId"];
  		$exist = true;
  	} else {
  		$exist = false;
  	}
  	if ($exist) {
  		if ($vl != "" && $sl != "") {
	  		$vl_flds = "`CompanyRefId`, `BranchRefId`, `EmployeesRefId`, `NameCredits`,";
		  	$vl_flds .= "`EffectivityYear`, `BegBalAsOfDate`, `BeginningBalance`, ";
		  	$vl_flds = $vl_flds.$trackingflds;
		  	$vl_vals = "'$companyrefid', '$branchrefid', '$emprefid', 'VL', '2018', '2017-12-31', '$vl', ";
		  	$vl_vals = $vl_vals.$trackingvals;
		  	$vl_sql = "INSERT INTO `employeescreditbalance` ($vl_flds) VALUES ($vl_vals)";
		  	//UNCOMMENT THE QUERY TO SAVE AND COMMENT THE LINE THAT NEXT TO IT
		  	//$vl_rs = mysqli_query($conn,$vl_sql);
		  	$vl_rs = true;
		  	if ($vl_rs) {
		  		echo "<label>Employee: ".$emprefid." SUCCESSFULLY ADDED his/her VL</label><br>";
		  		$sl_flds = "`CompanyRefId`, `BranchRefId`, `EmployeesRefId`, `NameCredits`,";
			  	$sl_flds .= "`EffectivityDate`, `BegBalAsOfDate`, `BeginningBalance`, ";
			  	$sl_flds = $sl_flds.$trackingflds;
			  	$sl_vals = "'$companyrefid', '$branchrefid', '$emprefid', 'SL', '2018-01-01', '2017-12-31', '$sl', ";
			  	$sl_vals = $sl_vals.$trackingvals;
			  	$sl_sql = "INSERT INTO `employeescreditbalance` ($sl_flds) VALUES ($sl_vals)";
			  	//UNCOMMENT THE QUERY TO SAVE AND COMMENT THE LINE THAT NEXT TO IT
			  	//$sl_rs = mysqli_query($conn,$vl_sql);
			  	$sl_rs = true;
			  	if (!$sl_rs) {
			  		echo "Error in Saving in employeescreditbalance SL";
			  	} else {
			  		echo "<label>Employee: ".$emprefid." SUCCESSFULLY ADDED his/her SL</label><br><br>";
			  	}
		  	} else {
		  		echo "Error in Saving in employeescreditbalance VL";
		  	}
	  	} else {
	  		echo "<label>Employee: ".$emprefid." HAS NO VL AND SL</label><br><br>";
	  	}
  	} else {
  		fwrite($file_new, "NO EMPLOYEE HAS AGENCY ID ".$agencyID."\n");
  	}
}
fclose($file);
?> 