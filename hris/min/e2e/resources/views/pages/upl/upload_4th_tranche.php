<?php
	include 'FnUpload.php';
	$file = fopen("csv/4th_tranche.csv", "r");
	while(!feof($file)) {
		$str = fgets($file);
		$row = explode(",", $str);
		$Fld = "Name, Step1, EffectivityDateS1, Step2, EffectivityDateS2, Step3, EffectivityDateS3, ";
		$Fld .= "Step4, EffectivityDateS4, Step5, EffectivityDateS5, Step6, EffectivityDateS6, Step7, EffectivityDateS7, ";
		$Fld .= "Step8, EffectivityDateS8,";

		$name 	= "4th Tranche - SG ".trim($row[0]);
		$Step1 	= trim(intval($row[1]));
		$Step2 	= trim(intval($row[2]));
		$Step3 	= trim(intval($row[3]));
		$Step4 	= trim(intval($row[4]));
		$Step5 	= trim(intval($row[5]));
		$Step6 	= trim(intval($row[6]));
		$Step7 	= trim(intval($row[7]));
		$Step8 	= trim(intval($row[8]));

		$Val = "'$name', '$Step1', '2019-01-01', '$Step2', '2019-01-01', '$Step3', '2019-01-01', '$Step4', '2019-01-01',";
		$Val .= " '$Step5', '2019-01-01', '$Step6', '2019-01-01', '$Step7', '2019-01-01', '$Step8', '2019-01-01',";

		$result = save("salarygrade",$Fld,$Val);
		if (is_numeric($result)) {
			echo "Successfully Saved $name.<br>";
		} else {
			echo "Error in Saving";
			return false;
		}
		echo "Name: $name <br>";
		echo "Step1 : $Step1 <br>";
		echo "Step2 : $Step2 <br>";
		echo "Step3 : $Step3 <br>";
		echo "Step4 : $Step4 <br>";
		echo "Step5 : $Step5 <br>";
		echo "Step6 : $Step6 <br>";
		echo "Step7 : $Step7 <br>";
		echo "Step8 : $Step8 <br>";
		echo "====================================<br>";
	}
?>