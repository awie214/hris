<?php
	include 'FnUpload.php';
	include '../conn.e2e.php';
	$table = "ldmslndprogram";
	mysqli_query($conn,"TRUNCATE $table");
	$file = fopen("14/trainings.csv", "r");
	//Training Course,Fee,Provider,Venue,Period,Year,Start Date,End Date,Whole Day,Type of Training,Course Description,Proposed Attendees,
	$count = 0;
	while(!feof($file)) {
		$count++;
		$row 					= explode(",", fgets($file));
		$Name 					= $row[0];
		$Cost 					= floatval($row[1]);
		$TrainingInstitution 	= $row[2];
		$Venue 					= $row[3];
		$Semester 				= $row[4]; 
		$Year 					= $row[5];
		$StartDate 				= $row[6];
		$EndDate 				= $row[7];
		$WholeDay 				= intval($row[8]);
		$TrainingType 			= $row[9];
		$CourseDescription 		= $row[10];
		$ProposedAttendees 		= $row[11];

		$flds = "`Name`,`Cost`,`TrainingInstitution`,`Venue`,`Semester`,`Year`,`StartDate`,`EndDate`,`WholeDay`,`TrainingType`,";
		$flds .= "`CourseDescription`,`ProposedAttendees`,";
		$vals = "'$Name','$Cost','$TrainingInstitution','$Venue','$Semester','$Year','$StartDate','$EndDate','$WholeDay','$TrainingType',";
		$vals .= "'$CourseDescription','$ProposedAttendees',";

		$result = save($table,$flds,$vals);
		if (is_numeric($result)) {
			echo $count.". ".$Name." Successfully Added.<br>";
		} else {
			echo "Error -> ".$count;
			return false;
		}
	}
?>
