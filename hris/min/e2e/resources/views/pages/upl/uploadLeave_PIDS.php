<!DOCTYPE html>
<html>
<body onload="alert('Done');">

<?php
require "constant.e2e.php";
require "conn.e2e.php";
require_once pathClass."0620functions.e2e.php";


            $conn->query("TRUNCATE TABLE `employeesleave`;");


            $t = time();
            $date_today    = date("Y-m-d",$t);
            $curr_time     = date("H:i:s",$t);
            $trackingA_fld = "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
            $trackingA_val = "'$date_today', '$curr_time', 'PHP', 'M'";

            $sql = "SELECT * FROM `employee_leaves`";
            $rs = mysqli_query($pids_conn,$sql) or die(mysqli_error($pids_conn));
            echo "Number Records : ".mysqli_num_rows($rs)."<br>";
            if ($rs) {
               if (mysqli_num_rows($rs) > 0) {
                  while ($pids = mysqli_fetch_array($rs)) {
                     $refid = $pids["employee_personal_information_sheet_id"];
                     $empRefId = $refid;

                     $wLeaveType = $pids['leave_type'];
                     $wStartMonth = $pids ['start_month'];
                     $wEndMonth = $pids['end_month'];
                     $wRemarks = remquote($pids['remarks'])."; Migrated";
                     $wStartDay = $pids['start_day'];
                     $wEndDay = $pids['end_day'];
                     $wApplicationDateFrom = $wStartMonth."-".$wStartDay;
                     $wApplicationDateTo = $wEndMonth."-".$wEndDay;
                     $wDuration = $pids['duration'];
                     $wPeriod = $pids['period'];


                     $flds = "";
                     $values = "";

                     $flds .= "`CompanyRefId`,`BranchRefId`,`EmployeesRefId`,`LeavesRefId`,`Remarks`,`ApplicationDateFrom`,`ApplicationDateTo`,".$trackingA_fld;
                     $values .= "1000,1,'$empRefId','$wLeaveType','$wRemarks','$wApplicationDateFrom','$wApplicationDateTo',".$trackingA_val;
                     $sql = "INSERT INTO `employeesleave` ($flds) VALUES ($values)";
                     if ($conn->query($sql) === TRUE) {
                        echo "Migrated Leave -->$empRefId<br>";
                     }
                     else {
                        echo "ERROR Migration Leave -->$empRefId<br>";
                     }

                  }
               }
            }
            mysqli_close($conn);
?>
</body>
</html>