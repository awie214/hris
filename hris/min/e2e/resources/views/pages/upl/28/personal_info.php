<?php
	include '../../conn.e2e.php';
	include '../FnUpload.php';
	$flds = "`LastName`,`FirstName`,`MiddleName`,`ExtName`,`NickName`,";
    $flds .= "`ContactNo`,`BirthDate`,`BirthPlace`,`EmailAdd`,`Sex`,";
    $flds .= "`CivilStatus`,CitizenshipRefId,isFilipino,isByBirthOrNatural,CountryCitizenRefId,";
    $flds .= "`Height`,`Weight`,`BloodTypeRefId`,`PAGIBIG`,`GSIS`,";
    $flds .= "`PHIC`,`TIN`,`SSS`,`AgencyId`,`ResiHouseNo`,";
    $flds .= "`ResiStreet`,`ResiBrgy`,`ResiAddCityRefId`,`ResiAddProvinceRefId`,`ResiCountryRefId`,";
    $flds .= "`ResidentTelNo`,`PermanentHouseNo`,`PermanentStreet`,`PermanentBrgy`,`PermanentAddCityRefId`,";
    $flds .= "`PermanentAddProvinceRefId`,`PermanentCountryRefId`,`TelNo`,`MobileNo`,`Remarks`,";
    $flds .= "`GovtIssuedID`, `GovtIssuedIDNo`, `GovtIssuedIDPlace`";

    $myfile = fopen("personal_info.csv", "r") or die("Unable to open file!");
    while(!feof($myfile)) {
        $str = fgets($myfile);
        $row = explode(",", $str);
        if ($row[1] != "") {
        	$LastName = $row[1];
	        $FirstName = $row[2];
	        $MiddleName = $row[3];
	        $ExtName = $row[4];
	        $isFilipino = 1;
	        $BirthDate = $row[6];
	        if ($BirthDate != "") {
	        	$BirthDate_arr = explode("/",$BirthDate);
	        	$day = $BirthDate_arr[0];
	        	$month = $BirthDate_arr[1];
	        	$year = $BirthDate_arr[2];
	        	$day = intval($day);
	        	$month = intval($month);
	        	if ($month < 13) {
	        		if ($day <= 9) $day = "0".$day;
	        		if ($month <= 9) $month = "0".$month;
	        		$BirthDate = $year."-".$month."-".$day;
	        	}
	        }
	        $BirthPlace = $row[7];
	        $Sex = $row[8];
	        $CivilStatus = $row[9];
	        $Weight = $row[10];
	        $Height = $row[11];
	        $BloodTypeRefId = saveFM("bloodtype","Name, ",$vals,$name,$where = "");
        }
        
    }
    fclose($myfile);
?>