<?php
	include 'FnUpload.php';
	include '../conn.e2e.php';
	mysqli_query($conn,"TRUNCATE competency");
	function convertNumberToWord($num = false) {
        $num = str_replace(array(',', ' '), '' , trim($num));
        if(! $num) {
           return false;
        }
        $num = (int) $num;
        $words = array();
        $list1 = array('', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'eleven',
             'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
        );
        $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
        $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
             'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
             'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
        );
        $num_length = strlen($num);
        $levels = (int) (($num_length + 2) / 3);
        $max_length = $levels * 3;
        $num = substr('00' . $num, -$max_length);
        $num_levels = str_split($num, 3);
        for ($i = 0; $i < count($num_levels); $i++) {
           $levels--;
           $hundreds = (int) ($num_levels[$i] / 100);
           $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
           $tens = (int) ($num_levels[$i] % 100);
           $singles = '';
           if ( $tens < 20 ) {
              $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
           } else {
              $tens = (int)($tens / 10);
              $tens = ' ' . $list2[$tens] . ' ';
              $singles = (int) ($num_levels[$i] % 10);
              $singles = ' ' . $list1[$singles] . ' ';
           }
           $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
        }
        $commas = count($words);
        if ($commas > 1) {
           $commas = $commas - 1;
        }
        return implode(' ', $words);
    }

	$obj = fopen("14/competency.csv", "r");
	while(!feof($obj)) {
		$obj_row 		= explode(",", fgets($obj));
		$type 			= $obj_row[0];
		$name 			= $obj_row[1];
		$definition     = $obj_row[2];
		$count 			= 0;
		$word 			= 1;
		/*for ($i=3; $i <= 42; $i++) { 
			$count++;
			if ($count > 10) {
				$word++;
				$count = 1;
			}
			$nword = convertNumberToWord($word);
			echo "Level".$nword.$count." = obj_row[".$i."];<br>";
		}
		echo "<BR><BR><BR>";*/
		$first 			= substr($type, 0, 1);
		$type 			= str_replace($first, "", $type);
		$type 			= strtolower($type);
		$type 			= $first.$type;
		$LevelOne1 		= clean($obj_row[3]);
		$LevelOne2 		= clean($obj_row[4]);
		$LevelOne3 		= clean($obj_row[5]);
		$LevelOne4 		= clean($obj_row[6]);
		$LevelOne5 		= clean($obj_row[7]);
		$LevelOne6 		= clean($obj_row[8]);
		$LevelOne7 		= clean($obj_row[9]);
		$LevelOne8 		= clean($obj_row[10]);
		$LevelOne9 		= clean($obj_row[11]);
		$LevelOne10 	= clean($obj_row[12]);
		$LevelTwo1 		= clean($obj_row[13]);
		$LevelTwo2 		= clean($obj_row[14]);
		$LevelTwo3 		= clean($obj_row[15]);
		$LevelTwo4 		= clean($obj_row[16]);
		$LevelTwo5 		= clean($obj_row[17]);
		$LevelTwo6 		= clean($obj_row[18]);
		$LevelTwo7 		= clean($obj_row[19]);
		$LevelTwo8 		= clean($obj_row[20]);
		$LevelTwo9 		= clean($obj_row[21]);
		$LevelTwo10 	= clean($obj_row[22]);
		$LevelThree1 	= clean($obj_row[23]);
		$LevelThree2 	= clean($obj_row[24]);
		$LevelThree3 	= clean($obj_row[25]);
		$LevelThree4 	= clean($obj_row[26]);
		$LevelThree5 	= clean($obj_row[27]);
		$LevelThree6 	= clean($obj_row[28]);
		$LevelThree7 	= clean($obj_row[29]);
		$LevelThree8 	= clean($obj_row[30]);
		$LevelThree9 	= clean($obj_row[31]);
		$LevelThree10 	= clean($obj_row[32]);
		$LevelFour1 	= clean($obj_row[33]);
		$LevelFour2 	= clean($obj_row[34]);
		$LevelFour3 	= clean($obj_row[35]);
		$LevelFour4 	= clean($obj_row[36]);
		$LevelFour5 	= clean($obj_row[37]);
		$LevelFour6 	= clean($obj_row[38]);
		$LevelFour7 	= clean($obj_row[39]);
		$LevelFour8 	= clean($obj_row[40]);
		$LevelFour9 	= clean($obj_row[41]);
		$LevelFour10 	= clean($obj_row[42]);


		$flds = "`LevelOne1`,`LevelOne2`,`LevelOne3`,`LevelOne4`,`LevelOne5`,`LevelOne6`,`LevelOne7`,`LevelOne8`,
		`LevelOne9`,`LevelOne10`,`LevelTwo1`,`LevelTwo2`,`LevelTwo3`,`LevelTwo4`,`LevelTwo5`,`LevelTwo6`,`LevelTwo7`,`LevelTwo8`,`LevelTwo9`,`LevelTwo10`,`LevelThree1`,`LevelThree2`,`LevelThree3`,`LevelThree4`,`LevelThree5`,`LevelThree6`,`LevelThree7`,`LevelThree8`,`LevelThree9`,`LevelThree10`,`LevelFour1`,`LevelFour2`,`LevelFour3`,`LevelFour4`,`LevelFour5`,`LevelFour6`,`LevelFour7`,`LevelFour8`,`LevelFour9`,`LevelFour10`,";
		
		$vals = "'$LevelOne1','$LevelOne2','$LevelOne3','$LevelOne4','$LevelOne5','$LevelOne6','$LevelOne7','$LevelOne8',
		'$LevelOne9','$LevelOne10','$LevelTwo1','$LevelTwo2','$LevelTwo3','$LevelTwo4','$LevelTwo5','$LevelTwo6','$LevelTwo7','$LevelTwo8','$LevelTwo9','$LevelTwo10','$LevelThree1','$LevelThree2','$LevelThree3','$LevelThree4','$LevelThree5','$LevelThree6','$LevelThree7','$LevelThree8','$LevelThree9','$LevelThree10','$LevelFour1','$LevelFour2','$LevelFour3','$LevelFour4','$LevelFour5','$LevelFour6','$LevelFour7','$LevelFour8','$LevelFour9','$LevelFour10',";

		$flds .= "`Name`,`Type`,`Definition`,";
		$vals .= "'$name','$type','$definition',";

		$result = save("competency",$flds,$vals);
		if (is_numeric($result)) {
			echo "Successfully Saved $name Competency.<br>";
		} else {
			echo $Flds." ======> ".$Vals;
			return false;
		}
	}
?>