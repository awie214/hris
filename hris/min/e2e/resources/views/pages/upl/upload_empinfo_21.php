<?php
	include 'FnUpload.php';
	include '../conn.e2e.php';
	mysqli_query($conn,"TRUNCATE empinformation");
	$EmpInfo = fopen("csv/empinfo_21_2.csv", "r");
	while(!feof($EmpInfo)) {
		/*
		empid 0
		lname 1
		fname 2
		mname 3
		position 4
		office 5 
		division 6
		item 7
		appointment 8
		sg 9
		step 10 
		sal1 11
		sal2 12
		empstatus 13
		*/
		$empinfo_row 		= explode(",", fgets($EmpInfo));
		$AgencyId 			= clean($empinfo_row[0]);
		$LastName 			= clean($empinfo_row[1]);
		$FirstName 			= clean($empinfo_row[2]);
		$Position 			= clean($empinfo_row[4]);
		$Office 			= clean($empinfo_row[5]);
		$Division 			= clean($empinfo_row[6]);
		//$Unit 				= clean($empinfo_row[6]);
		$PlantillaItem 		= clean($empinfo_row[7]);
		$AppointmentDate 	= clean($empinfo_row[8]);
		if ($AppointmentDate != "") {
			$AppointmentDate = date("Y-m-d",strtotime($AppointmentDate));
		}
		
		$SalaryGrade 		= clean($empinfo_row[9]);
		$StepIncrement 		= clean($empinfo_row[10]);
		$SalaryOne 			= clean($empinfo_row[11]);
		$SalaryTwo 			= clean($empinfo_row[12]);
		$EmpStatus 			= clean($empinfo_row[13]);
		$sql = "SELECT `RefId` FROM employees WHERE AgencyId = '$AgencyId'";
		$rs = mysqli_query($conn,$sql);
		if (mysqli_num_rows($rs) > 0) {
			$row = mysqli_fetch_assoc($rs);
			$emprefid = $row["RefId"];
			$Flds = "`CompanyRefId`, `BranchRefId`, `EmployeesRefId`,";
			$Vals = "21,1,$emprefid,";
			$plantilla_fldnval = "";
			if ($AppointmentDate != "") {
				if (strtoupper($EmpStatus) == "CONTRACTUAL" || strtoupper($EmpStatus) == "CONTRACT OF SERVICE") {
					$Flds .= "`StartDate`,`AssumptionDate`, ";
					$Vals .= "'$AppointmentDate', '$AppointmentDate',";	
				} else {
					$Flds .= "`HiredDate`,`AssumptionDate`, ";
					$Vals .= "'$AppointmentDate', '$AppointmentDate',";	
				}
			}
			if ($PlantillaItem != "" ) {
				$PlantillaItem = saveFM("positionitem","`Name`, ","'$PlantillaItem', ",$PlantillaItem);
				$Flds .= "`PositionItemRefId`, ";
				$Vals .= "'$PlantillaItem', ";
			}
			if ($Position != "" ) {
				$Position = saveFM("position","`Name`, ","'$Position', ",$Position);
				$Flds .= "`PositionRefId`, ";
				$Vals .= "'$Position', ";
				$plantilla_fldnval .= "`PositionRefId` = '$Position',";
			}

			if ($SalaryGrade != "" ) {
				$SalaryGrade = intval($SalaryGrade);
				$SalaryGrade = saveFM("salarygrade","`Name`, ","'$SalaryGrade', ",$SalaryGrade);
				$Flds .= "`SalaryGradeRefId`, ";
				$Vals .= "'$SalaryGrade', ";
				$plantilla_fldnval .= "`SalaryGradeRefId` = '$SalaryGrade',";
			}

			if ($StepIncrement != "" ) {
				$StepIncrement = intval($StepIncrement);
				$StepIncrement = saveFM("stepincrement","`Name`, ","'$StepIncrement', ",$StepIncrement);
				$Flds .= "`StepIncrementRefId`, ";
				$Vals .= "'$StepIncrement', ";
				$plantilla_fldnval .= "`StepIncrementRefId` = '$StepIncrement',";
			}

			if ($SalaryOne != "" ) {
				$SalaryOne = floatval($SalaryOne);
				$Flds .= "`SalaryAmount`, ";
				$Vals .= "'$SalaryOne', ";
				$plantilla_fldnval .= "`SalaryAmount` = '$SalaryOne',";
			}

			if ($SalaryTwo != "" ) {
				$SalaryTwo = floatval($SalaryTwo);
				$Flds .= "`SalaryAmountTwo`, ";
				$Vals .= "'$SalaryTwo', ";
				$plantilla_fldnval .= "`SalaryAmount` = '$SalaryTwo',";
			}

			if ($Office != "" ) {
				$Office = saveFM("office","`Name`, ","'$Office', ",$Office);
				$Flds .= "`OfficeRefId`, ";
				$Vals .= "'$Office', ";
				$plantilla_fldnval .= "`OfficeRefId` = '$Office',";
			}

			if ($Division != "" ) {
				$Division = saveFM("division","`Name`, ","'$Division', ",$Division);
				$Flds .= "`DivisionRefId`, ";
				$Vals .= "'$Division', ";
				$plantilla_fldnval .= "`DivisionRefId` = '$Division',";
			}

			if ($EmpStatus != "" ) {
				$EmpStatus = saveFM("empstatus","`Name`, ","'$EmpStatus', ",$EmpStatus);
				$Flds .= "`EmpStatusRefId`, ";
				$Vals .= "'$EmpStatus', ";
			}
			if (intval($PlantillaItem) > 0) {
				$update_plantilla = update("positionitem",$plantilla_fldnval,$PlantillaItem);
				if ($update_plantilla != "") {
					echo "Error in updating plantilla.<br>";
				}
			}
			
			$result = save("empinformation",$Flds,$Vals);
			if (is_numeric($result)) {
				echo "$FirstName $LastName Successfully Updated Employee Information.<br>";
			} else {
				echo $Flds." ======> ".$Vals;
			}
		} else {
			echo $AgencyId." -> No Emprefid<br>";
		}
	}
?>