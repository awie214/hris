<?php
	include 'FnUpload.php';
	mysqli_query($conn,"TRUNCATE employeeselegibility");
	$EmpEligibility = fopen("21/eligibility.csv", "r");
	while(!feof($EmpEligibility)) {
		$Fld = "CompanyRefId, BranchRefId, ";
		$Val = "21, 1, ";

		$eligibility_row = explode(",", fgets($EmpEligibility));
		$EmpAgencyID 		= clean($eligibility_row[0]);
		$CareerServiceRefId = clean($eligibility_row[1]);
		$Rating      		= clean($eligibility_row[2]);
		$ExamDate 	 		= clean($eligibility_row[3]);
		$ExamPlace   		= clean($eligibility_row[4]);
		$LicenseNo			= clean($eligibility_row[5]);
		$LicenseReleasedDate = clean($eligibility_row[6]);


		if ($CareerServiceRefId != "") {
			if ($ExamDate != "") {
				$Fld .= "ExamDate, ";
				$Val .= "'$ExamDate', ";
			}

			if ($LicenseReleasedDate != "") {
				$Fld .= "LicenseReleasedDate, ";
				$Val .= "'$LicenseReleasedDate', ";
			}
			$emprefid = FindFirst("employees","WHERE AgencyId = '$EmpAgencyID'","RefId",$conn);
			if (is_numeric($emprefid)) {
				if ($CareerServiceRefId != "") {
					$CareerServiceRefId = saveFM("careerservice","Name, ","'$CareerServiceRefId', ",$CareerServiceRefId);
					$Fld .= "CareerServiceRefId, ";
					$Val .= "'$CareerServiceRefId', ";
				}
				$Fld .= "EmployeesRefId, Rating, ExamPlace, LicenseNo, ";
				$Val .= "$emprefid, '$Rating', '$ExamPlace', '$LicenseNo',";
				$save_eligibility = save("employeeselegibility",$Fld,$Val);
				if (is_numeric($save_eligibility)) {
					echo "$emprefid Eligibility Saved <br>";
				}
			}	
		}
	
		
	}
?>