<?php
	include 'FnUpload.php';
	$obj = fopen("21/work_exp.csv", "r");
	while(!feof($obj)) {
		$row = explode(",", fgets($obj));
		$empid = $row[0];
		if ($empid != "") {
			$Fld 			= "CompanyRefId, BranchRefId,";
			$Val 			= "21, 1, ";
			$from 			= clean($row[1]);
			$to 			= clean($row[2]);
			$Position 		= clean($row[3]);
			$Agency 		= clean($row[4]);
			$Salary 		= clean($row[5]);
			$SalaryGrade 	= clean($row[6]);
			$EmpStatus 		= clean($row[7]);
			$Service 		= clean($row[8]);
			$Present 		= 0;

			$Service 		= strtolower($Service);
			$Salary 		= floatval($Salary);
			$SalaryGrade 	= intval($SalaryGrade);
			$Position 		= strtoupper($Position);
			$Agency 		= strtoupper($Agency);
			$EmpStatus 		= strtoupper($EmpStatus);

			//$SalaryGrade 	= saveFM("salarygrade","Name, ","'$SalaryGrade', ",$SalaryGrade);
			$SalaryGrade 	= FindFirst("salarygrade","WHERE Name = '$SalaryGrade'","RefId",$conn);
			$Position 		= saveFM("position","Name, ","'$Position', ",$Position);
			$Agency 		= saveFM("Agency","Name, ","'$Agency', ",$Agency);
			$EmpStatus 		= saveFM("EmpStatus","Name, ","'$EmpStatus', ",$EmpStatus);

			if ($to != "") {
				if (strtolower($to) == "present") {
					$Fld .= "Present, ";
					$Val .= "1, ";
					$to = "";
				} else {
					$Fld .= "WorkEndDate, ";
					$Val .= "'$to', ";
				}
			}

			if ($from != "") {
				$Fld .= "WorkStartDate, ";
				$Val .= "'$from', ";	
			}
			if ($Service == "y" || $Service == "yes") {
				$Fld .= "isGovtService, isServiceRecord, ";
				$Val .= "'1', '1',";
			} else {
				$Fld .= "isGovtService, isServiceRecord, ";
				$Val .= "'0', '0',";
			}

			if ($Salary > 0) {
				$Fld .= "SalaryAmount, ";
				$Val  .= "'$Salary', ";
			}

			if ($Agency > 0) {
				$Fld .= "AgencyRefId, ";
				$Val  .= "'$Agency', ";
			}
			if ($Position > 0) {
				$Fld .= "PositionRefId, ";
				$Val  .= "'$Position', ";
			}
			if ($EmpStatus > 0) {
				$Fld .= "EmpStatusRefId, ";
				$Val  .= "'$EmpStatus', ";
			}
			if ($SalaryGrade > 0) {
				$Fld .= "SalaryGradeRefId, ";
				$Val  .= "'$SalaryGrade', ";
			}

			$emprefid = FindFirst("employees","WHERE AgencyId = '$empid'","RefId",$conn);
			if (is_numeric($emprefid)) {
				$Fld .= "EmployeesRefId, ";
				$Val .= "'$emprefid', ";

				$save = save("employeesworkexperience",$Fld,$Val);
				if (is_numeric($save)) {
					echo "Successfully Saved Record For $empid.<br>";
				} else {
					echo "Error Saving Record For $empid.<br>";
				}
			}
			// echo "Employee ID: $empid <br> From: $from <br> To: $to <br> Present: $Present <br> Position: $Position <br> Agency: $Agency <br> Salary: $Salary <br>";
			// echo "SalaryGrade: $SalaryGrade <br> Emp Status: $EmpStatus <br> Service Type: $Service <br>---------------<br>-------------<br>";
			
		}
		

		
	}
?>