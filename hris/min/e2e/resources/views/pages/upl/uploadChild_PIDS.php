<!DOCTYPE html>
<html>
<body>

<?php
require "constant.e2e.php";
require "conn.e2e.php";
require_once pathClass."0620functions.e2e.php";

/*
$f = "../../../../e2e/public/pds/migration/PERSONALINFO.csv";
$myfile = fopen($f, "r") or die("Unable to open file!");

         $loop = 0;
         while(!feof($myfile)) {
            $str = fgets($myfile);
            $str_Arr = explode(";",$str);
            $loop++;
            if ($loop != 1) {

            $wLastName = $str_Arr[0];
            $wFirstName = $str_Arr[1];
            $wMiddleName = $str_Arr[2];
            $wExtName = $str_Arr[3];
            $wBirthDate = $str_Arr[4];
            $wBirthPlace = $str_Arr[5];
            $wSex = $str_Arr[6];
            $wCitizenshipRefId = $str_Arr[7];
            $wCountryCitizenRefId = $str_Arr[8];
            $wCivilStatus = $str_Arr[9];
            $wHeight = $str_Arr[10];
            $wWeight = $str_Arr[11];
            $wBloodTypeRefId = $str_Arr[12];
            $wAgencyId = $str_Arr[13];
            $wGovtIssuedID = $str_Arr[14];
            $wGovtIssuedIDNo = $str_Arr[15];
            $wGovtIssuedIDPlace = $str_Arr[16];
            $wResiHouseNo = $str_Arr[17];
            $wResiStreet = $str_Arr[18];
            $wResiBrgy = $str_Arr[19];
            $wResiCountryRefId = $str_Arr[20];
            $wResiAddProvinceRefId = $str_Arr[21];
            $wResiAddCityRefId = $str_Arr[22];
            $wPermanentHouseNo = $str_Arr[23];
            $wPermanentStreet = $str_Arr[24];
            $wPermanentBrgy = $str_Arr[25];
            $wPermanentCountryRefId = $str_Arr[26];
            $wPermanentAddProvinceRefId = $str_Arr[27];
            $wPermanentAddCityRefId = $str_Arr[28];
            $wTelNo = $str_Arr[29];
            $wMobileNo = $str_Arr[30];
            $wEmailAdd = $str_Arr[31];
            $wGSIS = $str_Arr[32];
            $wPAGIBIG = $str_Arr[33];
            $wPHIC = $str_Arr[34];
            $wTIN = $str_Arr[35];
            $wSSS = $str_Arr[36];
            $null = "";
            $wRemarks = "MIGRATED";
            $wResidentTelNo = $str_Arr[37];
            $wisFilipino = "1";
            $wisByBirthOrNatural = "1";

            $values = "'$wLastName','$wFirstName','$wMiddleName','$wExtName','$null',";
            $values .= "'$null','$wBirthDate','$wBirthPlace','$wEmailAdd','$wSex',";
            $values .= "'$wCivilStatus','$wCitizenshipRefId','$wisFilipino','$wisByBirthOrNatural','$wCountryCitizenRefId',";
            $values .= "'$wHeight','$wWeight','$wBloodTypeRefId','$wPAGIBIG','$wGSIS',";
            $values .= "'$wPHIC','$wTIN','$wSSS','$wAgencyId','$wResiHouseNo',";
            $values .= "'$wResiStreet','$wResiBrgy','$wResiAddCityRefId','$wResiAddProvinceRefId','$wResiCountryRefId',";
            $values .= "'$wResidentTelNo','$wPermanentHouseNo','$wPermanentStreet','$wPermanentBrgy','$wPermanentAddCityRefId',";
            $values .= "'$wPermanentAddProvinceRefId','$wPermanentCountryRefId','$wTelNo','$wMobileNo','$wRemarks',";
            $values .= "'$wGovtIssuedID', '$wGovtIssuedIDNo', '$wGovtIssuedIDPlace'";

            $flds = "`LastName`,`FirstName`,`MiddleName`,`ExtName`,`NickName`,";
            $flds .= "`ContactNo`,`BirthDate`,`BirthPlace`,`EmailAdd`,`Sex`,";
            $flds .= "`CivilStatus`,CitizenshipRefId,isFilipino,isByBirthOrNatural,CountryCitizenRefId,";
            $flds .= "`Height`,`Weight`,`BloodTypeRefId`,`PAGIBIG`,`GSIS`,";
            $flds .= "`PHIC`,`TIN`,`SSS`,`AgencyId`,`ResiHouseNo`,";
            $flds .= "`ResiStreet`,`ResiBrgy`,`ResiAddCityRefId`,`ResiAddProvinceRefId`,`ResiCountryRefId`,";
            $flds .= "`ResidentTelNo`,`PermanentHouseNo`,`PermanentStreet`,`PermanentBrgy`,`PermanentAddCityRefId`,";
            $flds .= "`PermanentAddProvinceRefId`,`PermanentCountryRefId`,`TelNo`,`MobileNo`,`Remarks`,";
            $flds .= "`GovtIssuedID`, `GovtIssuedIDNo`, `GovtIssuedIDPlace`";





               $isExist = FindFirst("employees","where LastName = '$wLastName' AND FirstName = '$wFirstName'","*");

               if (!$isExist) {
                  $sql = "INSERT INTO `employees` ($flds) VALUES ($values)";
                  if ($conn->query($sql) === TRUE) {
                     echo "Migrated -->".$wLastName.", ".$wFirstName."<br>";
                  } else {
                     echo "Err:-->".$str."<br>";
                  }
               } else {
                  echo "This Name is Already Exist ---->> ".$wLastName.", ".$wFirstName."<br>";
               }
            }
         }

fclose($myfile);
*/

            $conn->query("TRUNCATE `employeeschild`;");

            $t = time();
            $date_today    = date("Y-m-d",$t);
            $curr_time     = date("H:i:s",$t);
            $trackingA_fld = "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
            $trackingA_val = "'$date_today', '$curr_time', 'PHP', 'M'";

            $sql = "SELECT * FROM `employee_children`";
            $rs = mysqli_query($pids_conn,$sql) or die(mysqli_error($pids_conn));
            echo "Number Records : ".mysqli_num_rows($rs)."<br>";
            if ($rs) {
               if (mysqli_num_rows($rs) > 0) {
                  while ($pids = mysqli_fetch_array($rs)) {
                     $refid = $pids["employee_personal_information_sheet_id"];
                     $empRefId = $refid;

                     $wChildName = $pids['name_of_child'];
                     $wChildBirthDate = $pids ['date_of_birth'];

                     $flds = "";
                     $values = "";

                     $flds .= "`CompanyRefId`,`BranchRefId`,`EmployeesRefId`,`FullName`,`BirthDate`,".$trackingA_fld;
                     $values .= "1000,1,'$empRefId','$wChildName','$wChildBirthDate',".$trackingA_val;
                     $sql = "INSERT INTO `employeeschild` ($flds) VALUES ($values)";
                     if ($conn->query($sql) === TRUE) {
                        echo "Migrated Children -->$empRefId<br>";
                     }
                     else {
                        echo "ERROR Migration Children -->$empRefId<br>";
                     }

                  }
               }
            }
            mysqli_close($conn);


?>

</body>
</html>