<?php
	session_start();
	include 'conn.e2e.php';
	include_once "constant.e2e.php";
   	include_once pathClass.'0620functions.e2e.php';

   	$user = getvalue("user");

   	function HoursFormat($timeInMin) {
        if ($timeInMin > 0) {
           $hr = explode(".",$timeInMin / 60)[0];
           if ($hr <= 9) {
              $hr = "0".$hr;
           }
           $mod_min = ($timeInMin % 60);
           if ($mod_min <= 9) {
              $mod_min = "0".$mod_min;
           }
           return $hr.":".$mod_min;   
        } else {
           return "0";
        }
    }
	
	$WorkDayConversion = file_get_contents(json."WorkDayConversion.json");
  	$WorkDayEQ   = json_decode($WorkDayConversion, true);


   	$NewMonth 						= getvalue("hNewMonth");
	$NewYear 						= getvalue("hNewYear");
	$emprefid 						= getvalue("emprefid");
	$DateSelected					= getvalue("DateSelected");
	$curr_date						= date("Y-m-d",time());

   	$worksched						= "";
   	$DTRRemarks						= "";
   	$tdyHours						= "";
   	$tdyAbsent						= "";
   	$tdyLate1						= "";
   	$tdyUT1							= "";
   	$tdyLate2						= "";
   	$tdyUT2							= "";
   	$tdyOT							= "";
   	$TOTDays						= "";
   	$TOTHours						= "";
   	$TOTAbsent						= "";
   	$TOTLate						= "";
   	$TOTUT							= "";
   	$TOTOT							= "";
   	$DayEQ							= "";
   	$HoursEQ						= "";
   	$AbsentEQ						= "";
   	$LateEQ							= "";
   	$UTEQ							= "";
   	$OTEQ							= "";
   	$VLEarned						= "";
   	$SLEarned						= "";
   	$VLBal							= "";
   	$SLBal							= "";
   	$COC							= "";
   	
	$new_week 						= "";
	$day 		 					= "";
	$remarks     					= "";
	$workschedrefid 				= 0;
	$day_count_diff					= 0;
	$week_count 					= 0;
	$userID 						= 0;
	$OT_Count						= 0;
	$Total_Days						= 0;
	$Total_OT						= 0;
	$arr 							= array();
	$RG_Time_Per_Week				= Array(0,0,0,0,0,0);
	$Tardy_Time_Per_Week			= Array(0,0,0,0,0,0);
	$UT_Time_Per_Week				= Array(0,0,0,0,0,0);
	$Excess_Time_Per_Week 			= Array(0,0,0,0,0,0);
	$Absent_Time_Per_Week 			= Array(0,0,0,0,0,0);

	$Absent_Count_Per_Week 			= Array(0,0,0,0,0,0);
	$Tardy_Count_Per_Week			= Array(0,0,0,0,0,0);
	$UT_Count_Per_Week				= Array(0,0,0,0,0,0);
	$COC_Time_Per_Week				= Array(0,0,0,0,0,0);




    $num_of_days = cal_days_in_month(CAL_GREGORIAN,$NewMonth,$NewYear);

	if ($NewMonth <= 9) {
		$NewMonth = "0".$NewMonth;
	}

	//$month_start = $NewYear."-".$NewMonth."-01";
	$curr_month_start = $NewYear."-".$NewMonth."-01";
	$month_start 	  = $NewYear."-01-01";
	$month_end 		  = $NewYear."-".$NewMonth."-".$num_of_days;
	//echo 'alert("'.$month_start.'");';
	//echo 'alert("'.$month_end.'");';
	if (strtotime($curr_date) <= strtotime($month_end)) {
		$month_end = $curr_date;
	} else {
		$month_end = $month_end;
	}
	for ($x=1; $x <= $NewMonth ; $x++) { 
		if ($x <= 9) {
			$x = "0".$x;
		}
		$week_count = 0;
		$new_week   = "";
		$num_of_days = cal_days_in_month(CAL_GREGORIAN,$x,$NewYear);

		for($i=1;$i<=$num_of_days;$i++) {
			$y = $NewYear."-".$x."-".$i;
			$day = date("D",strtotime($y));	
			if ($i <= 9) {
				$i = "0".$i;
			}
			$y = $NewYear."-".$x."-".$i;
			$week = date("W",strtotime($y));
			if ($week != $new_week) {
				$week_count++;
			}
			$dtr = [
		   		"AttendanceDate" => "$y",
		   		"AttendanceTime" => "",
		   		"UTC" => "",
		   		"TimeIn" => "",
		   		"LunchOut" => "",
		   		"LunchIn" => "",
		   		"TimeOut" => "",
		   		"OBOut" => "",
		   		"OBIn" => "",
		   		"Day" => "$i",
		   		"Week" => "$week_count",
		   		"KEntry" => "",
		   		"Holiday" => "",
		   		"CTO"=>"",
		   		"Leave"=> "",
		   		"OffSus" => "",
		   		"OffSusTime"=>""
		   	];
		   	$arr["ARR"][$y] = $dtr;
			$new_week = $week;
		}
	}
	//$num_of_days = date("d",strtotime($month_end));
	

	
	$row = getRecordSet("employees",$emprefid);
	if ($row) {

		$EmpName = $row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"];	
		$biometricsID 				= $row["BiometricsID"];
		$CompanyID 					= $row["CompanyRefId"];
		$BranchID					= $row["BranchRefId"];
		$Default_qry				= "WHERE CompanyRefId = ".$CompanyID." AND BranchRefId = ".$BranchID;
		$KEntryContent = file_get_contents(json."Settings_".$CompanyID.".json");
	  	$KEntry_json   = json_decode($KEntryContent, true);
	  	$OTMinTime = $KEntry_json["OTMinTime"];
	  	$COCMinTime = $KEntry_json["COCMinTime"];

		$EmpInfo = f_Find("empinformation",$Default_qry." AND EmployeesRefId = ".$emprefid);
		if ($EmpInfo) {
			$fetch = mysqli_fetch_assoc($EmpInfo);
			$workschedrefid = $fetch["WorkScheduleRefId"];
			$worksched = getRecord("workschedule",$workschedrefid,"Name");
		}
		if ($workschedrefid > 0) {
			$Emp_WorkSched = FindFirst("workschedule"," WHERE RefId = ".$workschedrefid,"*");
			$where_empAtt = $Default_qry;
			$where_empAtt .= " AND EmployeesRefId = '".$emprefid."'";
			$where_empAtt .= " AND AttendanceDate BETWEEN '".$month_start."' AND '".$month_end."'";
			$rs_empAtt = SelectEach("employeesattendance",$where_empAtt);
			if ($rs_empAtt) {
				while ($row = mysqli_fetch_assoc($rs_empAtt)) {
					$AttendanceDate 	= $row["AttendanceDate"];
					$AttendanceTime 	= $row["AttendanceTime"];
					$UTC 				= $row["CheckTime"];
					$KEntry         	= $row["KindOfEntry"];
					$fldnval        	= "AttendanceTime = '".$UTC."',";
					$fldnval        	.= "UTC = '".$UTC."',";
					$fld 				= "";
					$val 				= "";
					switch ($KEntry) {
						case 1:
							$fld 		= "TimeIn";
							$val     	= get_today_minute($UTC);
							$fldnval	.= "TimeIn = '".get_today_minute($UTC)."',";
							break;
						case 2:
							$fld 		= "LunchOut";
							$val     	= get_today_minute($UTC);
							$fldnval	.= "LunchOut = '".get_today_minute($UTC)."',";
							break;
						case 3:
							$fld 		= "LunchIn";
							$val     	= get_today_minute($UTC);
							$fldnval	.= "LunchIn = '".get_today_minute($UTC)."',";
							break;
						case 4:
							$fld 		= "TimeOut";
							$val     	= get_today_minute($UTC);
							$fldnval	.= "TimeOut = '".get_today_minute($UTC)."',";
							break;
						case 7:
							$fld 		= "OBOut";
							$val     	= get_today_minute($UTC);
							break;
						case 8:
							$fld 		= "OBIn";
							$val     	= get_today_minute($UTC);
							break;
					}
					$arr["ARR"][$AttendanceDate][$fld] = $val;
					$arr["ARR"][$AttendanceDate]["UTC"] = $UTC;
				}
			}

			include 'mdbcn.e2e.php';
			if (isset($connection)) {
				$biometricsID = FindFirst("employees","WHERE RefId = $emprefid","BiometricsID");
			    $query = "SELECT USERID, Badgenumber, Name FROM USERINFO";
			    $result = $connection->query($query)->fetchAll(PDO::FETCH_ASSOC);
			    if ($result) {
			        foreach($result as $row) {
			           	switch ($CompanyID) {
			              	case "1000":
			                	$mdb_field = "Badgenumber";
			              		break;
			              	case "2":
			                	$mdb_field = "Name";
			              		break;
			              	default:
			              		$mdb_field = "Badgenumber";
			              		break;
			           	}

			           	if ($row[$mdb_field] == $biometricsID) {
			              	$userID = $row["USERID"];
			              	break;
			           	}
			        }
			    }
			    $query = 'SELECT USERID, CHECKTIME, CHECKTYPE, VERIFYCODE, SENSORID, sn FROM CHECKINOUT WHERE USERID = '.$userID.' AND VERIFYCODE = 1';
			    $result = $connection->query($query)->fetchAll(PDO::FETCH_ASSOC);
			    foreach($result as $row) {
			       	$utc 				= strtotime($row["CHECKTIME"]);
			       	$d 					= date("Y-m-d",$utc);
			    	$mdbTime 			= date("H:i",$utc);
			    	$mdbDate 			= date("Y-m-d",$utc);
					$fld 				= "";
					$val 				= "";
			    	switch ($row["CHECKTYPE"]) {
			          	case "I":
			          		$fld 		= $KEntry_json["FieldTimeIn"];
			          		$val 		= get_today_minute($utc);
			             	break;
			          	case "0":
			          		$fld 		= $KEntry_json["FieldLunchOut"];
			          		$val 		= get_today_minute($utc);
			             	break;
			          	case "1":
			          		$fld 		= $KEntry_json["FieldLunchIn"];
			          		$val 		= get_today_minute($utc);
			             	break;
			          	case "O":
			          		$fld 		= $KEntry_json["FieldTimeOut"];
			          		$val 		= get_today_minute($utc);
			            	break;
			    	}
			    	if (isset($arr["ARR"][$d]["AttendanceDate"])) {
			    		if ($arr["ARR"][$d]["AttendanceDate"] == $d) {
				    		if ($arr["ARR"][$d][$fld] == "") {
					    		$arr["ARR"][$d][$fld] = $val;
					    		$arr["ARR"][$d]["UTC"] = $utc;
					    	}	
				    	}	
			    	}
				}
			}

			$holiday = SelectEach("holiday","");
	        if ($holiday) {
	            while ($row = mysqli_fetch_assoc($holiday)) {
	               	$StartDate 			= $row["StartDate"];
	               	$EndDate   			= $row["EndDate"];
	               	$holiday_date_diff 	= dateDifference($StartDate,$EndDate);
	               	$Name 				= $row["Name"];
	               	$EveryYr 			= $row["isApplyEveryYr"];
	               	$Legal 				= $row["isLegal"];
	               	$temp_arr 			= explode("-", $StartDate);
	               	$temp_date 			= $NewYear."-".$temp_arr[1]."-".$temp_arr[2];

	               	for ($H=0; $H <= $holiday_date_diff-1 ; $H++) { 
	               		$holiday_NewDate = date('Y-m-d', strtotime($StartDate. ' + '.$H.' days'));
	               		if ($EveryYr == 1) {
		                  	if(isset($arr["ARR"][$holiday_NewDate])) {
		                     	$arr["ARR"][$holiday_NewDate]["Holiday"] = $Name;
		                  	}   
		               	} else {
		                  	if(isset($arr["ARR"][$holiday_NewDate])) {
		                     	$arr["ARR"][$holiday_NewDate]["Holiday"] = $Name;
		                  	}
		               	}
	               	}
	               	
	            }
	        }
	        $officesuspension = SelectEach("officesuspension","");
		    if ($officesuspension) {
		        while ($row = mysqli_fetch_assoc($officesuspension)) {
		            $StartDate        = $row["StartDate"];
		            $EndDate          = $row["EndDate"];
		            $OffSus_date_diff = dateDifference($StartDate,$EndDate);
		            $Name             = $row["Name"];
		            $OffSusTime       = $row["StartTime"];
		            for ($OS=0; $OS <= $OffSus_date_diff-1 ; $OS++) { 
		               	$OffSus_NewDate = date('Y-m-d', strtotime($StartDate. ' + '.$OS.' days'));
		               	if ($EveryYr == 1) {
		                  	if(isset($arr["ARR"][$OffSus_NewDate])) {
		                  		$arr["ARR"][$OffSus_NewDate]["OffSus"] = $Name;
		                    	$arr["ARR"][$OffSus_NewDate]["OffSus"] = $OffSusTime;
		                  	}   
		               	} else {
		                  	if(isset($arr["ARR"][$OffSus_NewDate])) {
		                  		$arr["ARR"][$OffSus_NewDate]["OffSus"] = $Name;
		                     	$arr["ARR"][$OffSus_NewDate]["OffSus"] = $OffSusTime;
		                  	}
		               	}
		            }   
		        }
		    }
	        $where_leave 		= $Default_qry;
			$where_leave		.= " AND EmployeesRefId = ".$emprefid;
			$where_leave		.= " AND ApplicationDateFrom BETWEEN '".$month_start."' AND '".$month_end."' AND Status = 'Approved'";
			$rs_leave			= SelectEach("employeesleave",$where_leave);
			if ($rs_leave) {
				while ($row = mysqli_fetch_assoc($rs_leave)) {
					$diff = dateDifference($row["ApplicationDateFrom"],$row["ApplicationDateTo"]);
					for ($i=0; $i <= $diff ; $i++) { 
						$temp_date = date('Y-m-d', strtotime($row["ApplicationDateFrom"] . ' +'.$i.' day'));
						if(isset($arr["ARR"][$temp_date])) {
	                     	$arr["ARR"][$temp_date]["Leave"] = $row["LeavesRefId"];
	                  	}
					}
				}
			}


			$where_cto 		= $Default_qry;
			$where_cto		.= " AND EmployeesRefId = ".$emprefid;
			$where_cto		.= " AND ApplicationDateFrom BETWEEN '".$month_start."' AND '".$month_end."' AND Status = 'Approved'";
			$rs_cto			= SelectEach("employeescto",$where_cto);
			if ($rs_cto) {
				while ($row = mysqli_fetch_assoc($rs_cto)) {
					$diff = dateDifference($row["ApplicationDateFrom"],$row["ApplicationDateTo"]);
					for ($i=0; $i <= $diff ; $i++) { 
						$temp_date = date('Y-m-d', strtotime($row["ApplicationDateFrom"] . ' +'.$i.' day'));
						if(isset($arr["ARR"][$temp_date])) {
	                     	$arr["ARR"][$temp_date]["CTO"] = $row["Hours"]*60;
	                  	}
					}
				}
			}
			/*
			$where_offsus 	= $Default_qry;
			$where_offsus	.= " AND EmployeesRefId = ".$emprefid;
			$where_offsus	.= " AND StartDate BETWEEN '".$month_start."' AND '".$month_end."'";
			$rs_offsus		= SelectEach("officesuspension",$where_offsus);
			if ($rs_offsus) {
				while ($row = mysqli_fetch_assoc($rs_offsus)) {
					$diff = dateDifference($row["StartDate"],$row["EndDate"]);
					if ($diff == 0) {
						if(isset($arr["ARR"][$row["StartDate"]])) {
	                     	$arr["ARR"][$row["StartDate"]]["OffSus"] = $row["StartTime"];
	                  	}
					} else {
						for ($i=0; $i <= $diff ; $i++) { 
							$temp_date = date('Y-m-d', strtotime($row["StartDate"] . ' +'.$i.' day'));
							if(isset($arr["ARR"][$temp_date])) {
		                     	$arr["ARR"][$temp_date]["OffSus"] = $row["StartTime"];
		                  	}
						}
					}
					
					
				}
			}	
			*/
		} else {
			echo '$.notify("Employee '.$emprefid.' has no workschedule yet");';
            echo '$("#pop_dtr").prop("disabled",true);';
            echo 'document.getElementById("overlay").style.display = "none";';

		}
		
	/*==============================================================================================================*/
	//START OF COMPUTATION AND SHOOTING
	/*==============================================================================================================*/
	if ($workschedrefid > 0) {
		foreach ($arr as $value) {
			foreach ($value as $key => $row) {
				if (isset($row["AttendanceDate"])) {
					$day_name = date("D",strtotime($row['AttendanceDate']));
					if ($day_name != "") {
						switch ($day_name) {
							case 'Mon':
								$day_name = "Monday";
								break;
							case 'Tue':
								$day_name = "Tuesday";
								break;
							case 'Wed':
								$day_name = "Wednesday";
								break;
							case 'Thu':
								$day_name = "Thursday";
								break;
							case 'Fri':
								$day_name = "Friday";
								break;
							case 'Sat':
								$day_name = "Saturday";
								break;
							case 'Sun':
								$day_name = "Sunday";
								break;
						}
					}	
					
					/*if ($row["TimeIn"] != "") {
						$Total_Days++;
					}
					if ($row["Holiday"] != "") {
						$Total_Days++;	
					}*/
					
					$day 						= $row["Day"]; 
					$underTime 	 				= "";
					$late 						= "";
					$day_in             	   	= $day_name."In";
					$day_out            	   	= $day_name."Out";
					$day_flexi          	   	= $day_name."FlexiTime";
					$day_LBOut					= $day_name."LBOut";
					$day_LBIn					= $day_name."LBIn";
					$day_RestDay				= $day_name."isRestDay";
					$day_isflexi          	   	= $day_name."isFlexi";

					$data_isflexi             	= $Emp_WorkSched[$day_isflexi];
					$data_flexi             	= $Emp_WorkSched[$day_flexi];
					$data_timein            	= $Emp_WorkSched[$day_in];
					$data_timeout 				= $Emp_WorkSched[$day_out];
					$data_LunchOut 				= $Emp_WorkSched[$day_LBOut];
					$data_LunchIn 				= $Emp_WorkSched[$day_LBIn];
					$data_RestDay      			= $Emp_WorkSched[$day_RestDay];

					$day_work_hours_count   	= ($data_timeout - $data_timein) - ($data_LunchIn - $data_LunchOut);
					$Week 						= $row["Week"];	
					$AutoLB						= $Emp_WorkSched["AutoLB"];
					$PerDayHours                = $day_work_hours_count / 60;


					$arr_TI						= $row["TimeIn"];
					$arr_OBO					= $row["OBOut"];
					$arr_TO						= $row["TimeOut"];
					$arr_OBI					= $row["OBIn"];
					$arr_Holiday				= $row["Holiday"];
					$arr_leave 					= $row["Leave"];
					$arr_COC					= $row["CTO"];
					$arr_OffSus 				= $row["OffSus"];

					if ($arr_TI == "" && $arr_leave != "") {
						$arr_TI = $data_timein;
						$arr_TO = $data_timeout;
					}
					if ($arr_TI == "" && $arr_OBO != "") {
						$arr_TI = $arr_OBO;
					}
					if ($arr_TO == "" && $arr_OBI != "") {
						$arr_TO = $arr_OBI;
					}

					if ($arr_Holiday != "" && $arr_TI == "") {
						$arr_TI = $data_timein;
						$arr_TO = $data_timeout;
					}
					if ($arr_COC != "") {
						if ($arr_COC >= $day_work_hours_count) {
							$arr_TI = $data_timein;
							$arr_TO = $data_timeout;
						} else {
							if ($arr_TI == "") {
								$arr_TI = $data_timein;
							}
							if ($arr_TO == "") {
								$arr_TO = $data_timein + $arr_COC;
							} else {
								$arr_TO = $arr_TO + $arr_COC;
							}
							/*echo "COC: ".$arr_COC."<br>"; 
							echo "Work Hours: ".$day_work_hours_count."<br>"; 
							echo "Time In : ".$arr_TI."<br>"; 
							echo "Time Out: ".$arr_TO."<br>"; */
						}
					}

					if ($arr_OffSus != "") {
						if ($arr_TI == "") {
							$arr_TI = $arr_OffSus;
						}
						if ($arr_TO == "") {
							$arr_TO = $data_timeout;
						}
					}
					/*if ($arr_COC >= $day_work_hours_count) {
						$arr_TI = $data_timein;
						$arr_TO = $data_timeout;
					} else {
						if ($arr_TI == "" && $arr_COC != "") {
							$arr_TI = $data_timein;
							$arr_TO = $data_timein + $arr_COC;
						}
						if ($arr_TO != "" && $arr_COC != "") {
							$arr_TO = $arr_TO + $arr_COC;
						}
						if ($arr_TI != "" && $arr_TO == "" && $arr_COC != "") {
							$arr_TO = $data_LunchIn + $arr_COC;
						}	
					}*/




					if ($row["AttendanceDate"] >= $curr_month_start && $row["AttendanceDate"] <= $month_end) {
						if (
								(
									$row["Holiday"] != "" || 
									$row["TimeIn"] 	!= "" || 
									$row["Leave"] 	!= "" || 
									$row["CTO"] 	!= "" || 
									$row["OBOut"] 	!= ""
								) && $data_RestDay != 1
						   ) {
								$Total_Days++;	
							
						}
					/*==============================================================================================================*/
					//MAXIMUM TIME FROM FOR EMPLOYEE TO ENTER WITHOUT LATE
					/*==============================================================================================================*/
					if ($Emp_WorkSched["ScheduleType"] == "Fl") {
						$data_flexi = $data_flexi;
					} else if ($Emp_WorkSched["ScheduleType"] == "Fi") {
						$data_flexi = $data_timein;
					} else {
						$data_flexi = $data_flexi;
					}
					/*==============================================================================================================*/
					//GETTING THE LATE OF THE EMPLOYEE
					/*==============================================================================================================*/
					if ($data_RestDay != 1) {
						if ($arr_TI != "" || $arr_OBO != "") {
							if ($data_isflexi != 1) {
								if ($arr_TI >= $data_timein) {
									$late = "";
								} else {
									$late = ($arr_TI - $data_flexi);
									switch ($Week) {
										case 1:
											$Tardy_Time_Per_Week[1] = $Tardy_Time_Per_Week[1] + $late;
											$Tardy_Count_Per_Week[1]++;
											break;
										case 2:
											$Tardy_Time_Per_Week[2] = $Tardy_Time_Per_Week[2] + $late;
											$Tardy_Count_Per_Week[2]++;
											break;
										case 3:
											$Tardy_Time_Per_Week[3] = $Tardy_Time_Per_Week[3] + $late;
											$Tardy_Count_Per_Week[3]++;
											break;
										case 4:
											$Tardy_Time_Per_Week[4] = $Tardy_Time_Per_Week[4] + $late;
											$Tardy_Count_Per_Week[4]++;
											break;
										case 5:
											$Tardy_Time_Per_Week[5] = $Tardy_Time_Per_Week[5] + $late;
											$Tardy_Count_Per_Week[5]++;
											break;
									}		
								}
							} else {
								if ($arr_TI <= $data_flexi) {
									$late = "";
								} else {
									$late = ($arr_TI - $data_flexi);
									switch ($Week) {
										case 1:
											$Tardy_Time_Per_Week[1] = $Tardy_Time_Per_Week[1] + $late;
											$Tardy_Count_Per_Week[1]++;
											break;
										case 2:
											$Tardy_Time_Per_Week[2] = $Tardy_Time_Per_Week[2] + $late;
											$Tardy_Count_Per_Week[2]++;
											break;
										case 3:
											$Tardy_Time_Per_Week[3] = $Tardy_Time_Per_Week[3] + $late;
											$Tardy_Count_Per_Week[3]++;
											break;
										case 4:
											$Tardy_Time_Per_Week[4] = $Tardy_Time_Per_Week[4] + $late;
											$Tardy_Count_Per_Week[4]++;
											break;
										case 5:
											$Tardy_Time_Per_Week[5] = $Tardy_Time_Per_Week[5] + $late;
											$Tardy_Count_Per_Week[5]++;
											break;
									}		
								}
							}
							
						}	
					}
					
					/*==============================================================================================================*/
					//GETTING THE WORKING AND EXCESS HOURS OF THE EMPLOYEE
					/*==============================================================================================================*/
					if ($arr_TO != "" && $arr_TI != "" && $data_RestDay != 1) {
						if ($arr_TI <= $data_timein) {
							$arr_TI = $data_timein;
						}
						$consume_time = (($arr_TO - $arr_TI) - ($data_LunchIn - $data_LunchOut));
						$excess_time = $consume_time - $day_work_hours_count; 	
						if ($excess_time > 0) {
							if ($Week == 1) $Excess_Time_Per_Week[1] = $Excess_Time_Per_Week[1] + $excess_time;
							if ($Week == 2) $Excess_Time_Per_Week[2] = $Excess_Time_Per_Week[2] + $excess_time;
							if ($Week == 3) $Excess_Time_Per_Week[3] = $Excess_Time_Per_Week[3] + $excess_time;
							if ($Week == 4) $Excess_Time_Per_Week[4] = $Excess_Time_Per_Week[4] + $excess_time;
							if ($Week == 5) $Excess_Time_Per_Week[5] = $Excess_Time_Per_Week[5] + $excess_time;
						}
					} else {
						$excess_time = 0;
						$consume_time = "";
						$remarks = "<span style='color:red;'>No Time Out</span>";
					}
					/*==============================================================================================================*/
					//GETTING THE UNDERTIME AND EXCESS HOURS OF THE EMPLOYEE
					/*==============================================================================================================*/
					if ($consume_time >= $day_work_hours_count) {
						if ($day_work_hours_count != "") {
							if ($Week == 1) $RG_Time_Per_Week[1] = $RG_Time_Per_Week[1] + $day_work_hours_count;
							if ($Week == 2) $RG_Time_Per_Week[2] = $RG_Time_Per_Week[2] + $day_work_hours_count;
							if ($Week == 3) $RG_Time_Per_Week[3] = $RG_Time_Per_Week[3] + $day_work_hours_count;
							if ($Week == 4) $RG_Time_Per_Week[4] = $RG_Time_Per_Week[4] + $day_work_hours_count;
							if ($Week == 5) $RG_Time_Per_Week[5] = $RG_Time_Per_Week[5] + $day_work_hours_count;
						}
						$consume_time = $day_work_hours_count;
					} else {
						if ($arr_TO != "" && $arr_TI != "" && $data_RestDay != 1) {
							$excess_time = 0;
							$underTime = $day_work_hours_count - $consume_time;
							switch ($Week) {
								case 1:
									$UT_Time_Per_Week[1] = $UT_Time_Per_Week[1] + $underTime;
									$UT_Count_Per_Week[1]++;
									break;
								case 2:
									$UT_Time_Per_Week[2] = $UT_Time_Per_Week[2] + $underTime;
									$UT_Count_Per_Week[2]++;
									break;
								case 3:
									$UT_Time_Per_Week[3] = $UT_Time_Per_Week[3] + $underTime;
									$UT_Count_Per_Week[3]++;
									break;
								case 4:
									$UT_Time_Per_Week[4] = $UT_Time_Per_Week[4] + $underTime;
									$UT_Count_Per_Week[4]++;
									break;
								case 5:
									$UT_Time_Per_Week[5] = $UT_Time_Per_Week[5] + $underTime;
									$UT_Count_Per_Week[5]++;
									break;
							}
						} else {
							$excess_time = 0;
						}
						if ($consume_time != "") {
							if ($Week == 1) $RG_Time_Per_Week[1] = $RG_Time_Per_Week[1] + $consume_time;	
							if ($Week == 2) $RG_Time_Per_Week[2] = $RG_Time_Per_Week[2] + $consume_time;
							if ($Week == 3) $RG_Time_Per_Week[3] = $RG_Time_Per_Week[3] + $consume_time;
							if ($Week == 4) $RG_Time_Per_Week[4] = $RG_Time_Per_Week[4] + $consume_time;
							if ($Week == 5) $RG_Time_Per_Week[5] = $RG_Time_Per_Week[5] + $consume_time;
						}
						$consume_time = $consume_time;
					}
					/*==============================================================================================================*/
					//AUTO LUNCH BREAK
					/*==============================================================================================================*/
					if ($AutoLB == 1) {
						if ($arr_TI != "") {
							$Lunch_Out = $data_LunchOut;
							$Lunch_In = $data_LunchIn;
						} else {
							$Lunch_Out = "";
							$Lunch_In = "";
						}
					} else {
						$Lunch_Out = $row["LunchOut"];
						$Lunch_In = $row["LunchIn"];
					}	

					/*==============================================================================================================*/
					//GETTING ABSENT
					/*==============================================================================================================*/
					if ($arr_TI == "" && $data_RestDay != 1) {
						if ($Week == 1) $Absent_Count_Per_Week[1]++;
						if ($Week == 2) $Absent_Count_Per_Week[2]++;
						if ($Week == 3) $Absent_Count_Per_Week[3]++;
						if ($Week == 4) $Absent_Count_Per_Week[4]++;
						if ($Week == 5) $Absent_Count_Per_Week[5]++;
					}



					/*==============================================================================================================*/
					//MAPPING
					/*==============================================================================================================*/
					$where = $Default_qry;
					$where .= " AND EmployeesRefId = ".$emprefid;
					$where .= " AND StartDate = '".$row['AttendanceDate']."'";
					$where .= " AND Status = 'Approved'";
					$OTRow = FindFirst("overtime_request",$where,"*");
					if ($OTRow) {
						if ($OTRow["WithPay"] == 1) {
							if ($excess_time != "") {
								if ($excess_time >= $OTMinTime) {
									$OT_Count = $OT_Count + $excess_time;
								} else {
									/*if ($Week == 1) $COC_Time_Per_Week[1] = $COC_Time_Per_Week[1] + $excess_time;
									if ($Week == 2) $COC_Time_Per_Week[2] = $COC_Time_Per_Week[2] + $excess_time;
									if ($Week == 3) $COC_Time_Per_Week[3] = $COC_Time_Per_Week[3] + $excess_time;
									if ($Week == 4) $COC_Time_Per_Week[4] = $COC_Time_Per_Week[4] + $excess_time;
									if ($Week == 5) $COC_Time_Per_Week[5] = $COC_Time_Per_Week[5] + $excess_time;*/
								}
							}
							
						} else {
							if ($excess_time >= $COCMinTime) {
								if ($Week == 1) $COC_Time_Per_Week[1] = $COC_Time_Per_Week[1] + $excess_time;
								if ($Week == 2) $COC_Time_Per_Week[2] = $COC_Time_Per_Week[2] + $excess_time;
								if ($Week == 3) $COC_Time_Per_Week[3] = $COC_Time_Per_Week[3] + $excess_time;
								if ($Week == 4) $COC_Time_Per_Week[4] = $COC_Time_Per_Week[4] + $excess_time;
								if ($Week == 5) $COC_Time_Per_Week[5] = $COC_Time_Per_Week[5] + $excess_time;		
							}
						}
					} else {
						$Total_OT = $Total_OT + $excess_time;
					}

					if ($row["AttendanceDate"] == $DateSelected) {
						//echo 'alert("'.$row["AttendanceDate"].'");';
						if ($arr_TI == "" && $data_RestDay != 1) {
							$tdyAbsent = 1;
							$tdyLate1 = 0;
							$tdyLate2 = 0;
							$tdyUT1 = 0;
							$tdyUT2 = 0;
							$tdyOT = 0;
							$tdyHours = 0;
						} else {
							$tdyAbsent = 0;
							$tdyHours = HoursFormat($consume_time + $excess_time);
							$tdyLate1 = HoursFormat($late);
							$tdyUT2 = HoursFormat($underTime);
							$tdyOT = HoursFormat($excess_time);
							if ($AutoLB == 1) {
								$tdyUT1 = 0;
								$tdyLate2 = 0;
							} else {
								$LIN = $row["LunchOut"]; 
								$LOUT = $row["LunchIn"]; 
								if ($LIN == "") $LIN = 0;
								if ($LOUT == "") $LOUT = 0;

								if ($data_LunchOut < $LOUT) {
									$tdyUT1 = $data_LunchOut - $LOUT;	
								} else {
									$tdyUT1 = 0;
								}

								if ($data_LunchIn < $LIN) {
									$tdyLate2 = $data_LunchIn - $LIN;	
								} else {
									$tdyLate2 = 0;
								}
							}
						}
					}
					}
				}
			}
		}

		if ($tdyAbsent != 1) {
			$TOTDays = $Total_Days;
			$TOTOT   = HoursFormat($Total_OT);
			$COC  	= $COC_Time_Per_Week[1] + 
	                  $COC_Time_Per_Week[2] + 
	                  $COC_Time_Per_Week[3] + 
	                  $COC_Time_Per_Week[4] +
	                  $COC_Time_Per_Week[5];

			$TOTAbsent		    =  $Absent_Count_Per_Week[1] + 
		                           $Absent_Count_Per_Week[2] + 
		                           $Absent_Count_Per_Week[3] + 
		                           $Absent_Count_Per_Week[4] +
		                           $Absent_Count_Per_Week[5];

		   	$TOTLate		    = $Tardy_Time_Per_Week[1] + 
								  $Tardy_Time_Per_Week[2] + 
								  $Tardy_Time_Per_Week[3] + 
								  $Tardy_Time_Per_Week[4] + 
								  $Tardy_Time_Per_Week[5];

		   	$TOTUT   			= $UT_Time_Per_Week[1] + 
								  $UT_Time_Per_Week[2] + 
								  $UT_Time_Per_Week[3] + 
								  $UT_Time_Per_Week[4] + 
								  $UT_Time_Per_Week[5];

		   	$TOTRG 				= ($RG_Time_Per_Week[1] + 
								   $RG_Time_Per_Week[2] +
								   $RG_Time_Per_Week[3] + 
								   $RG_Time_Per_Week[4] + 
								   $RG_Time_Per_Week[5]);

			$TOTExcess 			= ($Excess_Time_Per_Week[1] + 
				                   $Excess_Time_Per_Week[2] + 
				                   $Excess_Time_Per_Week[3] + 
				                   $Excess_Time_Per_Week[4] +
				                   $Excess_Time_Per_Week[5]);

		   	$TOTHours = HoursFormat(($TOTRG + $TOTExcess));
		   	$HoursEQ = getEquivalent(($TOTRG + $TOTExcess),"workinghrsconversion");
		   	$LateEQ = getEquivalent($TOTLate,"workinghrsconversion");
		   	$UTEQ = getEquivalent($TOTUT,"workinghrsconversion");
		   	$OTEQ = getEquivalent($Total_OT,"workinghrsconversion");
		   	
			$TOTLate = HoursFormat($TOTLate);
			$TOTUT = HoursFormat($TOTUT);
			$DayEQ = 0;
			if ($PerDayHours != "") {
				for ($i=1; $i <= $TOTDays; $i++) { 
					$DayEQ = $WorkDayEQ["WorkDayConversion"][$PerDayHours] + $DayEQ;
				}
			}

			$AbsentEQ = 0;
			if ($PerDayHours != "") {
				for ($i=1; $i <= $TOTAbsent; $i++) { 
					$AbsentEQ = $WorkDayEQ["WorkDayConversion"][$PerDayHours] + $AbsentEQ;
				}
			}


			$where_leave 		= $Default_qry;
			$where_leave		.= " AND EmployeesRefId = ".$emprefid;
			$where_leave		.= " AND ApplicationDateFrom BETWEEN '".$month_start."' AND '".$month_end."' AND Status = 'Approved'";
			$rs_leave			= SelectEach("employeesleave",$where_leave);
			if ($rs_leave) {
				while ($row = mysqli_fetch_assoc($rs_leave)) {
					$diff = dateDifference($row["ApplicationDateFrom"],$row["ApplicationDateTo"]);
					$day_count_diff = $day_count_diff + $diff;
				}
			}




			/*$sl 				= 0;
			$vl 				= 0;
			$whereSL 			= $Default_qry;
			$whereSL           .= " AND EmployeesRefId = ".$emprefid;
			$whereSL           .= " AND NameCredits = 'SL'";
			$whereSL 			.= " AND BegBalAsOfDate <= '".$curr_date."'";
			$whereSL 			.= " AND EffectivityYear = ".$NewYear;

			$whereVL 			= $Default_qry;
			$whereVL           .= " AND EmployeesRefId = ".$emprefid;
			$whereVL           .= " AND NameCredits = 'VL'";
			$whereVL 			.= " AND BegBalAsOfDate <= '".$curr_date."'";
			$whereVL 			.= " AND EffectivityYear = ".$NewYear;
			//$whereVL 			.= " AND OutstandingBalance = $COC";

			$whereCOC 			= $Default_qry;
			$whereCOC           .= " AND EmployeesRefId = ".$emprefid;
			$whereCOC           .= " AND NameCredits = 'OT'";
			$whereCOC 			.= " AND BegBalAsOfDate <= '".$curr_date."'";
			$whereCOC 			.= " AND EffectivityYear = ".$NewYear;
			$whereCOC 			.= " AND OutstandingBalance = $COC";


			$nTotal_Days 		= 30 - $TOTAbsent;
			$day_eq_vl 			= FindFirst("slvlearneddaily","WHERE NoOfDays =".$nTotal_Days,"VLEarned");
		    $day_eq_sl 			= FindFirst("slvlearneddaily","WHERE NoOfDays =".$nTotal_Days,"SLEarned");
		    $fld				= "EmployeesRefId, NameCredits, BegBalAsOfDate, EffectivityYear, BeginningBalance, ";
		    $val_sl				= "$emprefid, 'SL', '".$DateSelected."', '".$NewYear."', $day_eq_sl,";
		    $val_vl				= "$emprefid, 'VL', '".$DateSelected."', '".$NewYear."', $day_eq_sl,";
		    $val_coc			= "$emprefid, 'OT', '".$DateSelected."', '".$NewYear."', $COC,";

		    $rowSL 				= FindFirst("employeescreditbalance",$whereSL,"*");	
		    if ($rowSL) {
		    	if ($rowSL["OutstandingBalance"] != "") {
			    	$sl = $rowSL["OutstandingBalance"];
			    } else {
			    	$sl = $rowSL["BeginningBalance"];
			    }	
		    } else {
		    	$save_sl = f_SaveRecord("NEWSAVE","employeescreditbalance",$fld,$val_sl);
		    	if (!is_numeric($save_sl)) {
		    		echo 'alert("'.$save_sl.'");';
		    	}
		    }
		    
		    $rowVL 				= FindFirst("employeescreditbalance",$whereVL,"*");
		    if ($rowVL) {
		    	if ($rowVL["OutstandingBalance"] != "") {
			    	$vl = $rowVL["OutstandingBalance"];
			    } else {
			    	$vl = $rowVL["BeginningBalance"];
			    }	
		    } else {
		    	$save_vl = f_SaveRecord("NEWSAVE","employeescreditbalance",$fld,$val_vl);
		    	if (!is_numeric($save_vl)) {
		    		echo 'alert("'.$save_vl.'");';
		    	}
		    }


		    $rowCOC 				= FindFirst("employeescreditbalance",$whereCOC,"*");
		    if ($rowCOC) {
		    	if ($rowCOC["OutstandingBalance"] != "") {
			    	$COC = $rowCOC["OutstandingBalance"];
			    } else {
			    	$COC = $rowCOC["BeginningBalance"];
			    }	
		    } else {
		    	$save_coc = f_SaveRecord("NEWSAVE","employeescreditbalance",$fld,$val_coc);
		    	if (!is_numeric($save_coc)) {
		    		echo 'alert("'.$save_coc.'");';
		    	}
		    }

		    
		    $VLEarned 			= $day_eq_vl;
		   	$SLEarned 			= $day_eq_sl;
		   	$VLBal 				= $vl + $day_eq_vl;
			$SLBal 				= $sl + "1.25";


			if ($VLBal > 0) {
				if ($day_count_diff > 0) {
					$leave_eq_vl	= FindFirst("slvlearneddaily","WHERE NoOfDays =".$day_count_diff,"VLEarned");
					$VLBal			= $VLBal - $leave_eq_vl;
				}	
			}
			
			if ($SLBal > 0) {
				if ($day_count_diff > 0) {
					$leave_eq_sl	= FindFirst("slvlearneddaily","WHERE NoOfDays =".$day_count_diff,"VLEarned");
					$SLBal			= $SLBal - $leave_eq_sl;
				}
			}
			
			
			if ($SLBal <= 0) {
				$SLBal = 0;
			}
			if ($VLBal <= 0) {
				$VLBal = 0;
			}

		    
			$where_coc			= $Default_qry." AND EmployeesRefId = ".$emprefid." AND AccumDate = '".$DateSelected."'"; 
		    $where_coc			.= " AND NameCredits = 'OT' AND Balance = $COC";
		    $check_coc			= FindLast("accumcreditbalance",$where_coc,"Balance");
		    //echo 'alert("'.$where_coc.'");';
		    if (is_numeric($check_coc)) {
		    	$COC 			= $check_coc;
		    } else {
		    	$COC 			= $check_coc;
		    	if ($COC == "") $COC = 0;
		    	$Flds 			= "EmployeesRefId, NameCredits, AccumDate, Balance, Earnings, ";
		    	$Vals 			= "$emprefid, 'OT', '".$DateSelected."', '$COC', '$COC',";
		    	$result_coc		= f_SaveRecord("NEWSAVE","accumcreditbalance",$Flds,$Vals);
		    	if (!is_numeric($result_coc)) {
		    		echo $result_coc;
		    	}
		    }
		    


		    $where_acc_sl		= $Default_qry." AND EmployeesRefId = ".$emprefid." AND AccumDate = '".$DateSelected."'"; 
		    $where_acc_sl		.= " AND NameCredits = 'SL' AND Balance = $SLBal";
		    $check_acc_sl		= FindLast("accumcreditbalance",$where_acc_sl,"Balance");
		    if (is_numeric($check_acc_sl)) {
		    	$sl 			= $check_acc_sl;
		    } else {
		    	$Flds 			= "EmployeesRefId, NameCredits, AccumDate, Balance, Earnings,";
		    	$Vals 			= "$emprefid, 'SL', '".$DateSelected."', $SLBal, $SLBal,";
		    	$result_sl		= f_SaveRecord("NEWSAVE","accumcreditbalance",$Flds,$Vals);
		    	if (!is_numeric($result_sl)) {
		    		echo $result_sl;
		    	}
		    }


		    
		    $where_acc_vl		= $Default_qry." AND EmployeesRefId = ".$emprefid." AND AccumDate = '".$DateSelected."'"; 
		    $where_acc_vl		.= " AND NameCredits = 'VL' AND Balance = $VLBal";
		    $check_acc_vl		= FindLast("accumcreditbalance",$where_acc_vl,"Balance");
		    if (is_numeric($check_acc_vl)) {
		    	$vl 			= $check_acc_vl;
		    } else {
		    	$Flds 			= "EmployeesRefId, NameCredits, AccumDate, Balance, Earnings,";
		    	$Vals 			= "$emprefid, 'VL', '".$DateSelected."', $VLBal, $VLBal, ";
		    	$result_vl		= f_SaveRecord("NEWSAVE","accumcreditbalance",$Flds,$Vals);
		    	if (!is_numeric($result_vl)) {
		    		echo $result_vl;
		    	}
		    }

		    
	    	$where_CB_vl		= $Default_qry." AND EmployeesRefId = ".$emprefid." AND NameCredits = 'VL'";
		    //$where_CB_vl		.= " AND EffectivityYear = '".$NewYear."'";
		    $rsEmpCB_vl 		= FindOrderBy("employeescreditbalance",$where_CB_vl,"*","BegBalAsOfDate");
		    if ($rsEmpCB_vl) {
		    	$refid = $rsEmpCB_vl["RefId"];
		    	if (strtotime($DateSelected) > strtotime($rsEmpCB_vl["BegBalAsOfDate"])) {
			    	$fldnval		= "BegBalAsOfDate = '".$DateSelected."', OutstandingBalance = $VLBal, ";
			    	$result_vl      = f_SaveRecord("EDITSAVE","employeescreditbalance",$fldnval,$refid);
			    	if ($result_vl != "") {
			    		echo 'alert("'.$result_vl.'");';
			    	}
		    	}
		    }


		    $where_CB_sl		= $Default_qry." AND EmployeesRefId = ".$emprefid." AND NameCredits = 'SL'";
		    //$where_CB_sl		.= " AND EffectivityYear = '".$NewYear."'";
		    $rsEmpCB_sl 		= FindOrderBy("employeescreditbalance",$where_CB_sl,"*","BegBalAsOfDate");
		    if ($rsEmpCB_sl) {
		    	$refid = $rsEmpCB_sl["RefId"];
		    	if (strtotime($DateSelected) > strtotime($rsEmpCB_sl["BegBalAsOfDate"])) {
			    	$fldnval		= "BegBalAsOfDate = '".$DateSelected."', OutstandingBalance = $SLBal, ";
			    	$result_sl      = f_SaveRecord("EDITSAVE","employeescreditbalance",$fldnval,$refid);
			    	if ($result_sl != "") {
			    		echo 'alert("'.$result_sl.'");';
			    	}
		    	}
		    }	

		    $where_CB_coc		= $Default_qry." AND EmployeesRefId = ".$emprefid." AND NameCredits = 'OT'";
		    //$where_CB_coc		.= " AND EffectivityYear = '".$NewYear."'";
		    $rsEmpCB_coc 		= FindOrderBy("employeescreditbalance",$where_CB_coc,"*","BegBalAsOfDate");
		    if ($rsEmpCB_coc) {
		    	$refid = $rsEmpCB_coc["RefId"];
		    	if (strtotime($DateSelected) > strtotime($rsEmpCB_coc["BegBalAsOfDate"])) {
			    	$fldnval		= "BegBalAsOfDate = '".$DateSelected."', OutstandingBalance = $COC, ";
			    	$result_coc      = f_SaveRecord("EDITSAVE","employeescreditbalance",$fldnval,$refid);
			    	if ($result_coc != "") {
			    		echo 'alert("'.$result_coc.'");';
			    	}
		    	}
		    }
		    $COC = HoursFormat($COC);*/
		    
		}
		
		

		/*==============================================================================================================*/
		//START OF SHOOTING
		/*==============================================================================================================*/
		//echo 'alert("'.print_r($arr).'");';
		echo 'setValueByName("worksched","'.$worksched.'");'."\n";
		echo 'setValueByName("DTRRemarks","'.$DTRRemarks.'");'."\n";
		echo 'setValueByName("tdyHours","'.$tdyHours.'");'."\n";
		echo 'setValueByName("tdyAbsent","'.$tdyAbsent.'");'."\n";
		echo 'setValueByName("tdyLate1","'.$tdyLate1.'");'."\n";
		echo 'setValueByName("tdyUT1","'.$tdyUT1.'");'."\n";
		echo 'setValueByName("tdyLate2","'.$tdyLate2.'");'."\n";
		echo 'setValueByName("tdyUT2","'.$tdyUT2.'");'."\n";
		echo 'setValueByName("tdyOT","'.$tdyOT.'");'."\n";
		echo 'setValueByName("TOTDays","'.$TOTDays.'");'."\n";
		echo 'setValueByName("TOTHours","'.$TOTHours.'");'."\n";
		echo 'setValueByName("TOTAbsent","'.$TOTAbsent.'");'."\n";
		echo 'setValueByName("TOTLate","'.$TOTLate.'");'."\n";
		echo 'setValueByName("TOTUT","'.$TOTUT.'");'."\n";
		echo 'setValueByName("TOTOT","'.$TOTOT.'");'."\n";
		echo 'setValueByName("DayEQ","'.$DayEQ.'");'."\n";
		echo 'setValueByName("HoursEQ","'.$HoursEQ.'");'."\n";
		echo 'setValueByName("AbsentEQ","'.$AbsentEQ.'");'."\n";
		echo 'setValueByName("LateEQ","'.$LateEQ.'");'."\n";
		echo 'setValueByName("UTEQ","'.$UTEQ.'");'."\n";
		echo 'setValueByName("OTEQ","'.$OTEQ.'");'."\n";
		echo 'setValueByName("VLEarned","'.$VLEarned.'");'."\n";
		echo 'setValueByName("SLEarned","'.$SLEarned.'");'."\n";
		echo 'setValueByName("VLBal","'.$VLBal.'");'."\n";
		echo 'setValueByName("SLBal","'.$SLBal.'");'."\n";
		echo 'setValueByName("COC","'.$COC.'");'."\n";
		echo 'document.getElementById("overlay").style.display = "none";';

		/*==============================================================================================================*/
		//END OF COMPUTATION AND SHOOTING
		/*==============================================================================================================*/
		}
	}

	
?>
