<?php
   $sysScreen = getvalue("sysScreen");
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_AfterTrn"); ?>"></script>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php
            $sys->SysHdr($sys,"rms");
         ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar(getvalue("paramTitle")); ?>
            <div class="container-fluid margin-top10">
               <div class="row">
                  <div class="col-xs-12" id="divList">
                     <div class="mypanel">
                        <div class="panel-top">LIST</div>
                        <div class="panel-mid">
                           <div class="row">
                              <div class="col-xs-12">
                                 <span id="spGridTable">
                                    <?php
                                       $sql  = "SELECT * FROM `$table` ORDER BY RefId Desc";
                                       doGridTable($table,
                                                   $gridTableHdr_arr,
                                                   $gridTableFld_arr,
                                                   $sql,
                                                   [true,true,true,false],
                                                   $_SESSION["module_gridTable_ID"]);
                                    ?>
                                 </span>
                              </div>
                           </div>
                        </div>
                        <div class="panel-bottom">
                           <?php
                              btnINRECLO([true,true,false]);
                           ?>
                        </div>
                     </div>
                  </div>
                  <div id="divView">
                     <div class="mypanel">
                        <div class="panel-top">
                           INSERTING NEW VACANT POSITION
                        </div>
                        <div class="panel-mid">
                           <div id="EntryScrn">
                              <div class="row" id="badgeRefId">
                                 <div class="col-xs-5">
                                    <ul class="nav nav-pills">
                                       <li class="active" style="font-size:12pt;font-weight:600;">
                                          <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                                          </span></a>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="row margin-top10">
                                 <div class="col-xs-5">
                                    <div class="form-group">
                                       <label class="control-label" for="inputs">POSITION</label>
                                       <?php
                                          createSelect("PositionItem",
                                                       "sint_PositionItemRefId",
                                                       0,100,"Name","Select Position Item","");
                                       ?>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-xs-5">
                                    <div class="form-group">
                                       <label class="control-label" for="inputs">SALARY AMOUNT</label>
                                       <input type="text" class="form-input saveFields-- number--" name="deci_SalaryAmount">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-xs-5">
                                    <div class="form-group">
                                       <label class="control-label" for="inputs">PLANTILLA</label>
                                       <input type="text" class="form-input saveFields-- number--" name="char_Plantilla">
                                    </div>
                                 </div>
                              </div>

                              <div class="row">
                                 <div class="col-xs-6">
                                    <div class="mypanel">
                                       <div class="panel-top">
                                          QUALIFICATION STANDARDS
                                       </div>
                                       <div class="panel-mid">
                                          <div class="row margin-top10">
                                             <div class="col-xs-6">
                                                <div class="form-group">
                                                   <label class="control-label" for="inputs">EDUCATION ATTAINMENT</label>
                                                   <textarea class="form-input saveFields--" rows="5" name="char_EducAttain"></textarea>
                                                </div>
                                             </div>
                                             <div class="col-xs-6">
                                                <div class="form-group">
                                                   <label class="control-label" for="inputs">CIVIL SERVICE</label>
                                                   <textarea class="form-input saveFields--" rows="5" name="char_CivilService"></textarea>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="row margin-top10">
                                             <div class="col-xs-6">
                                                <div class="form-group">
                                                   <label class="control-label" for="inputs">RELEVANT EXPERIENCE</label>
                                                   <textarea class="form-input saveFields--" rows="5" name="char_RelevantExperience"></textarea>
                                                </div>
                                             </div>
                                             <div class="col-xs-6">
                                                <div class="form-group">
                                                   <label class="control-label" for="inputs">RELEVANT TRAINING/SEMINARS</label>
                                                   <textarea class="form-input saveFields--" rows="5" name="char_RelevantTraining"></textarea>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel-bottom"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>

                        </div>
                        <div class="panel-bottom">
                           <?php
                              btnSACABA([true,true,true]);
                           ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>



