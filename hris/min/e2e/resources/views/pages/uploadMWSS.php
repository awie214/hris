<!DOCTYPE>
<html>
<head>
	<?php
		session_start();
		include 'conn.e2e.php';
		include 'constant.e2e.php';
		include pathClass.'0620functions.e2e.php';
	?>

	<title></title>
	<script type="text/javascript" src="<?php echo $path; ?>jquery/jquery.js"></script>
   	<script type="text/javascript" src="<?php echo $path; ?>js/js7_login.js"></script>
   	<script type="text/javascript" src="<?php echo $path; ?>js/jsSHAver2/src/sha.js"></script>
	<script type="text/javascript">
		function setPW(pw,empRefId) {
	        pw = calcHash(pw);
	        $.post("setpw.php",
	        {
	            pw:pw,
	            empRefId:empRefId
	        },
	        function(data,status) {
	            if (status == "success") {
	                code = data;
	                try {
	                  	//eval(code);
	                } catch (e) {
	                   	if (e instanceof SyntaxError) {
	                       	alert(e.message);
	                   	}
	                }
	            }
	            else {
	               alert("Ooops Error : " + status + "[005]");
	            }
	        });
	    }
	</script>
</head>
<body>
	<?php
		/*echo $dbname;
		return false;*/
		mysqli_query($conn,"TRUNCATE employees");
		mysqli_query($conn,"TRUNCATE empinformation");
		$file = fopen(textFile."emp_data_mwss.csv","r");
		while(! feof($file)) {
			$filex = fgets($file);
			$arr = explode(",", $filex);
			if ($arr[0] != "") {
				$emprefid 		= $arr[0];
				$lastname 		= trim($arr[1]);
				$firstname 		= trim($arr[2]);
				$midname 		= trim($arr[3]);
				$extname 		= trim($arr[4]);
				$position 		= trim($arr[5]);
				$office 		= trim($arr[6]);
				$division 		= trim($arr[7]);
				$email 			= trim($arr[8]);
				$hireddate 		= $arr[9];
				$empstatus 		= trim($arr[10]);
				$sex 			= strtolower($arr[11]);
				$bday 			= trim($arr[12]);

				$pw             = strtolower($lastname)."123";

				if (!empty($bday)) {
					$bday_arr       = explode("-", $bday);
					if ($bday_arr[0] <= 9) {
						$month = "0".$bday_arr[0];
					} else {
						$month = $bday_arr[0];
					}
					$bday = trim($bday_arr[2])."-".trim($month)."-".trim($bday_arr[1]);	
				}
				
				
				if ($hireddate != "") {
					$hireddate_arr       = explode("-",$hireddate);
					if ($hireddate_arr[0] <= 9) {
						$month = "0".$hireddate_arr[0];
					} else {
						$month = $hireddate_arr[0];
					}
					$hireddate = trim($hireddate_arr[2])."-".trim($month)."-".trim($hireddate_arr[1]);	
				}
				
				


				

				

				if ($sex == "male") {
					$sex = "M";
				} else if ($sex == "female") {
					$sex = "F";
				} else {
					$sex = "";
				}

				switch ($empstatus) {
		            case "P":
		               $empstatus = "Permanent";
		            break; 
		            case "T":
		               $empstatus = "Temporary";
		            break; 
		         }
				$employees_fld  = "RefId, LastName, FirstName, MiddleName, ExtName, Inactive, UserName, EmailAdd,";
				$employees_val  = "$emprefid, '$lastname', '$firstname', '$midname', '$extname', 0, '$lastname', '$email',";
				if ($bday != "") {
					$employees_fld .= "BirthDate, ";
					$employees_val .= "'$bday',";
				}
				$emp_save		= f_SaveRecord("NEWSAVE","employees",$employees_fld,$employees_val,"Admin");
				if (is_numeric($emp_save)) {
					if ($position != "") {
			            $positionRefId  = fileManager($position,"position","`Code`,`Name`,`Remarks`,","'','$position','auto',");
			        } else {
			        	$positionRefId = 0;
			        }
			        if ($office != "") {
			            $officeRefId  = fileManager($office,"office","`Code`,`Name`,`Remarks`,","'','$office','auto',");
			        } else {
			        	$officeRefId = 0;
			        }
			        if ($division != "") {
			            $divisionRefId  = fileManager($division,"division","`Code`,`Name`,`Remarks`,","'','$division','auto',");
			        } else {
			        	$divisionRefId = 0;
			        }
			        if ($empstatus != "") {
			            $empstatusRefId  = fileManager($empstatus,"empstatus","`Code`,`Name`,`Remarks`,","'','$empstatus','auto',");
			        } else {
			        	$empstatusRefId = 0;
			        }
			        $empinfo_fld   = "EmployeesRefId, ";
			        $empinfo_fld  .= "PositionRefId, OfficeRefId, DivisionRefId, EmpStatusRefId,";
			        $empinfo_val   = "$emprefid, ";
			        $empinfo_val  .= "'$positionRefId', '$officeRefId', '$divisionRefId', '$empstatusRefId', ";
			        if ($hireddate != "") {
			        	$empinfo_fld  .= " HiredDate, AssumptionDate, ";
			        	$empinfo_val  .= "'$hireddate', '$hireddate', ";
			        }

			        $empinfo_save = f_SaveRecord("NEWSAVE","empinformation",$empinfo_fld,$empinfo_val,"Admin");
			        if (is_numeric($empinfo_save)) {
			        	echo "Success!!! -->$pw-->$emprefid ".$lastname.", ".$firstname."<br>";      
	                    echo "<script>";
	                    echo "setPW('".$pw."',".$emprefid.");";
	                    echo "</script>";
			        }
				} else {
					echo "Error in Saving ".$emprefid;
				}
			}
		}
		fclose($file);

	?>
</body>
</html>


