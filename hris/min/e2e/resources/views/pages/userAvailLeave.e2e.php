<?php
   require_once "conn.e2e.php";
   require_once "constant.e2e.php";
   require_once pathClass.'0620RptFunctions.e2e.php';
   $table = "employees";
   $emprefid = getvalue("hEmpRefId");
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>      
      <style type="text/css">

      </style>
      
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar ("LEAVE APPLICATION"); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div class="container-fluid rptBody">
                        <div>
                           <div class="row" id="divList">
                              <div class="col-xs-12">
                                 <div class="panel-top">
                                    List of Leave Applications
                                 </div>
                                 <div class="panel-mid">
                                    <span id="spGridTable">
                                       <?php
                                          $table = "employeesleave";
                                          $gridTableHdr_arr = ["Leave Type", "Filed Date", "Date From", "Date To", "Status"];
                                          $gridTableFld_arr = ["LeavesRefId", "FiledDate", "ApplicationDateFrom", "ApplicationDateTo", "Status"];
                                          $sql = "SELECT * FROM $table WHERE EmployeesRefId = $EmployeesRefId ORDER BY FiledDate DESC";
                                          $Action = [false,true,false,true];
                                          doGridTable($table,
                                                      $gridTableHdr_arr,
                                                      $gridTableFld_arr,
                                                      $sql,
                                                      $Action,
                                                      "gridTable");
                                       ?>
                                    </span>
                                 </div>
                                 <div class="panel-bottom">
                                    <button type="button" class="btn-cls4-sea" id="btnINSERT">
                                       <i class="fa fa-file"></i>&nbsp;&nbsp;Insert New
                                    </button>
                                    <!-- <button type="button" class="btn-cls4-tree" id="btnPrintMultiple">
                                       <i class="fa fa-print"></i>&nbsp;&nbsp;Print Multiple Leaves
                                    </button>
                                    <label>Date From:&nbsp;&nbsp;</label>
                                    <input type="text" name="from_date" id="from_date" class="form-input date--">
                                    <label>Date To:&nbsp;&nbsp;</label>
                                    <input type="text" name="to_date" id="to_date" class="form-input date-- dateto" for="from_date">
                                    <label>Leave Type:&nbsp;&nbsp;</label>
                                    <?php
                                       createSelect("leaves",
                                                    "leave_type",
                                                    "",20,"Name","Select Leave","",false);
                                    ?> -->
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div id="divView">
                           <div id="">
                              <div class="row margin-top" id="EntryScrn">
                                 <div class="col-xs-6">
                                    <div class="panel-top">
                                       ADD LEAVE APPLICATION
                                    </div>
                                    <input type="hidden" 
                                           class="saveFields--"
                                           name="sint_EmployeesRefId" 
                                           id="sint_EmployeesRefId" 
                                           value="<?php echo $emprefid; ?>">
                                    <div class="panel-mid" style="padding: 10px;">
                                       <div class="row">
                                          <div class="col-xs-12">
                                             <label>DATE FILED:</label>
                                             <br>
                                             <input type="text"
                                                    name="date_FiledDate" 
                                                    id="date_FiledDate" 
                                                    class="form-input saveFields-- date--" 
                                                    value="<?php echo date("Y-m-d",time()); ?>" 
                                                    readonly>

                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-6">
                                             <label>LEAVE TYPE:</label>
                                             <?php
                                                createSelect("leaves",
                                                             "sint_LeavesRefId",
                                                             "",100,"Name","Select Leave","",false);
                                             ?>
                                          </div>
                                          <div class="col-xs-6" id="h_spl">
                                             <label>SPL TYPE:</label>
                                             <select name="char_SPLType" id="char_SPLType" class="form-input saveFields--">
                                                <option value="">SELECT SPL TYPE</option>
                                                <option value="PM">Personal Milestone</option>
                                                <option value="PO">Parental Obligation</option>
                                                <option value="Fil">Fillial</option>
                                                <option value="DomE">Domestic Emergencies</option>
                                                <option value="PTrn">Personal Transactions</option>
                                                <option value="C">Calamity</option>
                                             </select>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-6">
                                             <label>APPLICATION DATE FROM:</label>
                                             <br>
                                             <input type="text" 
                                                    class="form-input saveFields-- date-- mandatory" 
                                                    name="date_ApplicationDateFrom" 
                                                    id="date_ApplicationDateFrom">
                                          </div>
                                          <div class="col-xs-6">
                                             <label>APPLICATION DATE TO:</label>
                                             <br>
                                             <input type="text" 
                                                    class="form-input saveFields-- date-- mandatory" 
                                                    name="date_ApplicationDateTo" 
                                                    id="date_ApplicationDateTo">
                                          </div>
                                       </div>
                                       <div class="row margin-top" id="vl_detail">
                                          <div class="col-xs-12">
                                             <label>If Abroad (Specify):</label>
                                             <input type="text" class="form-input saveFields--" name="char_VLPlace" id="char_VLPlace" placeholder="City, Country">
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-6" id="is_fl">
                                             <label>IS THIS FORCED LEAVE?</label>
                                             <select name="sint_isForceLeave" id="sint_isForceLeave" class="form-input saveFields--">
                                                <option value="">NO</option>
                                                <option value="1">YES</option>
                                             </select>
                                          </div>
                                          <div class="col-xs-6" style="display: none;">
                                             <label>IS SATURDAY AND SUNDAY INCLUDE?</label>
                                             <select name="sint_IncludeSatSun" id="sint_IncludeSatSun" class="saveFields-- form-input">
                                                <option value="">NO</option>
                                                <option value="1">YES</option>
                                             </select>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-12">
                                             <div class="form-group">
                                                <label class="control-label" for="inputs">Remarks:</label>
                                                <textarea class="form-input saveFields--" rows="5" name="char_Remarks" placeholder="Remarks"></textarea>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-6">
                                             <div class="form-group">
                                                <label class="control-label" for="inputs">
                                                   Personnel Officer:
                                                </label>
                                                <?php
                                                   createSelect("signatories",
                                                                "sint_Signatory1",
                                                                "",100,"Name","Select Signatory","",false);
                                                ?>
                                             </div>
                                          </div>
                                          <div class="col-xs-6">
                                             <div class="form-group">
                                                <label class="control-label" for="inputs">
                                                   Authorized Official:
                                                </label>
                                                <?php
                                                   createSelect("signatories",
                                                                "sint_Signatory2",
                                                                "",100,"Name","Select Signatory","",false);
                                                ?>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-6">
                                             <div class="form-group">
                                                <label class="control-label" for="inputs">
                                                   Last Approver:
                                                </label>
                                                <?php
                                                   createSelect("signatories",
                                                                "sint_Signatory3",
                                                                "",100,"Name","Select Signatory","",false);
                                                ?>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel-bottom">
                                       <div class="row">
                                          <div class="col-xs-12">
                                             <button type="button" class="btn-cls4-sea" id="btnSAVE">
                                                <i class="fa fa-save"></i>&nbsp;AVAIL
                                             </button>
                                             <button type="button" class="btn-cls4-red" id="btnCANCEL">CANCEL</button>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>   
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               $table = "employeesleave";
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
   <script type="text/javascript">
      $(document).ready(function () {
         $("[class*='autocomplete--']").each(function () {
            $(this).blur(function () {
               var id      = $(this).attr("id");
               var value   = $(this).val();
               arr         = value.split("-");
               $(this).val(arr[1]);
               $("#sint_" + id).val(arr[0]);
            });
         });
         /*EmployeeAutoComplete("employees","Signatory1");
         EmployeeAutoComplete("employees","Signatory2");
         EmployeeAutoComplete("employees","Signatory3");*/
         $("[name='sint_LeavessRefId']").addClass("mandatory");
         $("#availment_entry").hide();
         $("#btn_add_availment").click(function () {
            $("#divList").slideUp();
            $("#divView").show(500);   
         });
         $("#h_spl, #is_fl, #vl_detail").hide();
         $("#sint_LeavesRefId").change(function () {
            var leave = $("#sint_LeavesRefId option:selected").text();
            leave = leave.toLowerCase();
            if (leave.indexOf("privilege leave") >= 0) {
               $("#h_spl").show();
            } else {
               $("#h_spl").hide();
            }
            //$.notify(leave.indexOf("acation leave"));
            if (leave.indexOf("vacation leave") >= 0) {
               $("#is_fl ,#vl_detail").show();
            } else {
               $("#is_fl ,#vl_detail").hide();
            }
         });
         $("#btnPrintMultiple").click(function () {
            var from       = $("#from_date").val();
            var to         = $("#to_date").val();
            var leave_type = $("#leave_type").val();
            if (from != "" && to != "") {
               if (leave_type != 0) {
                  $("#rptContent").attr("src","blank.htm");
                  var rptFile = "rpt_Multiple_Leave_Availment";
                  var url = "ReportCaller.e2e.php?file=" + rptFile;
                  url += "&date_from=" + from;
                  url += "&date_to=" + to;
                  url += "&leave_type=" + leave_type;
                  url += "&" + $("[name='hgParam']").val();
                  $("#prnModal").modal();
                  $("#rptContent").attr("src",url);         
               } else {
                  $.notify("Select Leave");
               }
            } else {
               $.notify("Incorrect Date Range");
            }
            
         });
      });
      function selectMe(refid) {
         printAttachment(refid);  
      }
      function printAttachment(refid){
         var cid = $("#hCompanyID").val();
         var rptFile = "";
         if (cid == 35) {
            rptFile = "rpt_Leave_Availment_35";
         } else {
            rptFile = "rpt_Leave_Availment";
         }
         /*
          else if (cid == 21) {
            rptFile = "rpt_Leave_Availment_21";
         }
         */
         $("#rptContent").attr("src","blank.htm");
         var url = "ReportCaller.e2e.php?file=" + rptFile;
         url += "&refid=" + refid;
         url += "&" + $("[name='hgParam']").val();
         $("#prnModal").modal();
         $("#rptContent").attr("src",url);
      }
      function afterEditSave(refid) {
         alert("Record Updated");
         gotoscrn($("#hProg").val(),"");
      }
      function afterNewSave(newRefId) {
         alert("Successfully Availed. Please wait for the HR's approval.");
         gotoscrn($("#hProg").val(),"");
      }
   </script>
</html>



