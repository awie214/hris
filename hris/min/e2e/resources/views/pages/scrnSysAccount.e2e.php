<?php
?>
<!DOCTYPE html>
<html>
   <head>
      <?php
         include_once $files["inc"]["pageHEAD"];
      ?>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,'pis') ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("SYSTEM ACCOUNT PROFILE"); ?>
            <div class="container-fluid margin-top10">
                     <div class="row">
                        <div class="col-xs-12" id="div_CONTENT">
                           <div class="row">
                              <div class="col-xs-3">
                                 <h4>SYSTEM GROUP</h4>
                                 <div class="list-group">
                                    <?php
                                       $rs = f_Find("sysgroup","order by Name");
                                       if($rs) {
                                          while ($row = mysqli_fetch_assoc($rs)) {
                                             if ($row["Code"] != "") {
                                                echo '<a href="#" onclick="selectSysGroup(\''.$row["Code"].'\','.$row["RefId"].');" id="'.$row["Code"].'" class="list-group-item">
                                                      ['.$row["RefId"].'] - ['.$row["Code"].'] - '.$row["Name"].'
                                                     </a>';
                                             }
                                          }
                                       } else {
                                         alert("Information","No Available Record !!!");
                                       }
                                    ?>
                                 </div>
                              </div>
                              <div class="col-xs-9">
                                 <div class="row">
                                    <div class="col-xs-12">
                                       <button type="button"
                                               class="btn-cls4-sea trnbtn"
                                               id="btnADD" name="btnADD">
                                          <i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;
                                          ADD
                                       </button>
                                       <button type="button"
                                               class="btn-cls4-lemon trnbtn"
                                               id="btnPRINT" name="btnPRINT">
                                          <i class="fa fa-print" aria-hidden="true"></i>&nbsp;
                                          PRINT
                                       </button>
                                    </div>
                                 </div>
                                 <?php bar(); ?>
                                 <!--
                                 <div class="row margin-top">
                                    <div class="col-xs-2"></div>
                                    <div class="col-xs-4">
                                       <label class="control-label" for="inputs">PROFILE:</label>
                                       <input class="form-input" type="text" id="inputs" name="txtPROFILE">
                                    </div>
                                 </div>
                                 <br>
                                 -->
                                 <div class="row">
                                    <div class="col-xs-12">
                                       <h4>System Group Details</h4>
                                    </div>
                                 </div>
                                 <div id="SysGroupDetailsAccess"></div>
                              </div>
                           </div>
                        </div>
                     </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="subSCRN_DataEntry" role="dialog">
               <div class="modal-dialog modal-lg">
                  <div id="entry-modal-content">
                  </div>
               </div>
            </div>
            <input type="hidden" name="hSelectedGroup" id="hSelectedGroup" value="">
            <input type="hidden" name="hSysGroupRefId" id="hSysGroupRefId" value="">
            <?php
               footer();
               $table = "sysgroupaccess";
               include "varHidden.e2e.php";
            ?>
         </div>
         <script language="JavaScript" src="<?php echo jsCtrl("ctrl_SysAccount"); ?>"></script>
      </form>
   </body>
</html>



