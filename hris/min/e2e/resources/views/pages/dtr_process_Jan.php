<?php
	include 'conn.e2e.php';
	include 'constant.e2e.php';
	include pathClass.'0620functions.e2e.php';
	include pathClass.'DTRFunction.e2e.php';

	$curr_date		= date("Y-m-d",time());
	$curr_month     = date("m",time());
	$curr_year		= date("Y",time());
	$start_date		= $curr_year."-".$curr_month."-01";
	$where = "WHERE RefId = '221' AND (Inactive != 1 OR Inactive IS NULL)";
	//$where = "WHERE (Inactive != 1 OR Inactive IS NULL)";
	mysqli_query($conn,"TRUNCATE dtr_process");
	$rs = SelectEach("employees",$where);
	if ($rs) {
		$count = 0;
		while ($row = mysqli_fetch_assoc($rs)) {
			$emprefid 		= $row["RefId"];
			$LastName       = $row["LastName"];
			$FirstName      = $row["FirstName"];
			$Name 			= strtoupper($FirstName." ".$LastName);
			$workschedule 	= FindFirst("empinformation","WHERE EmployeesRefId = $emprefid","WorkScheduleRefId");		
			if ($workschedule != "") {
				$dtr_array 		= DTR_Summary ($emprefid,"01",date("Y",time()),$workschedule);
				$flds 			= "";
				$vals 			= "";	
				$table 			= "dtr_process";
				foreach ($dtr_array as $key => $value) {
					$flds .= "`$key`,";
					$vals .= "'$value', ";
					
				}
				//echo $flds." => ".$vals."<br>";
				$save_dtr_process = f_SaveRecord("NEWSAVE",$table,$flds,$vals);
				if (is_numeric($save_dtr_process)) {
					$count++;
					echo $count." Successfully processed $Name's DTR for the month of January <br>";
				} 
			}
		}
	}
?>