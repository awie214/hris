<?php
   date_default_timezone_set("Asia/Manila");
   $fontcolor     = "#003972";
   $bordercolor   = $fontcolor;
   $bgcolor       = "#FFFFFF"; // body
   $divcolor      = "#E6EEF6";
   $titlebarcolor = $fontcolor;
   $hlightcolor   = "#E0E0E0";
   $font          = "Arial";
   $_SESSION['fontcolor']     = $fontcolor;
   $_SESSION['bordercolor']   = $bordercolor;
   $_SESSION['bgcolor']       = $bgcolor;
   $_SESSION['divcolor']      = $divcolor;
   $_SESSION['titlebarcolor'] = $fontcolor;
   $_SESSION['hlightcolor']   = $hlightcolor;
   $_SESSION['font']          = $font;
   setcookie('fontcolor'    ,$fontcolor    ,time() + 3600, '/');
   setcookie('bgcolor'      ,$bgcolor      ,time() + 3600, '/');
   setcookie('bordercolor'  ,$bordercolor  ,time() + 3600, '/');
   setcookie('divcolor'     ,$divcolor     ,time() + 3600, '/');
   setcookie('titlebarcolor',$titlebarcolor,time() + 3600, '/');
   setcookie('font'         ,$font         ,time() + 3600, '/');
   setcookie('hlightcolor'  ,$hlightcolor  ,time() + 3600, '/');

   $SCRNTYPE = "";
   $TRNBTN = 0;
   $today = "";

   $MenuId = "";
   $today   = date("mdY");
   $datelog = date("mdY")." ".date("h:i:s A");
?>
