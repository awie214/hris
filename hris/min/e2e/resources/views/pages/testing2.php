<?php
   if (isset($_GET["date_select"])) {
      echo $_GET["date_select"];
   }
?>
<!doctype html>
<html lang = "en">
   <head>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
      <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" rel="stylesheet"/>
      <script type="text/javascript">
         $(document).ready(function () {
            $(".date").attr("data-date-format", "yyyy-mm-dd");
            $('.date').datepicker({
                multidate: true
            });
         });
      </script>
   </head>
   
   <body>
      <form action="testing2.php" method="get">
         <input type="text" class="form-control date" name="date_select" />
         <button type="submit" id="btn_click">CHECK DATA</button>
      </form>
   </body>
</html>