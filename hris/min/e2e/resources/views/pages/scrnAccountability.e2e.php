<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_Accountability"); ?>"></script>
      <script>
      </script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar($modTitle); ?>
            <div class="container-fluid margin-top10">
               <div>
                  <?php
                     $EmpRefId = getvalue("txtRefId");
                     $attr = ["empRefId"=>getvalue("txtRefId"),
                              "empLName"=>getvalue("txtLName"),
                              "empFName"=>getvalue("txtFName"),
                              "empMName"=>getvalue("txtMidName")];
                     $EmpRefId = EmployeesSearch($attr);
                  ?>
               </div>
               <?php spacer(10) ?>
               <div class="row">
                  <div class="col-xs-12" id="divList">
                           <?php if ($EmpRefId) { ?>
                              <div class="row">
                                 <div class="col-xs-12">
                                    <div class="mypanel">
                                       <div class="panel-top">Accountability List Per Employee</div>
                                       <div class="panel-mid">
                                          <span id="gridAccountability">
                                             <?php
                                                $sql  = "SELECT * FROM `accountability` WHERE EmployeesRefId = $EmpRefId ORDER BY RefId";
                                                doGridTable("accountability",
                                                           ["Custodian Name","Accountability Name","Transfer From","Remarks"],
                                                           ["CustodianRefId","AccountabilityName","TransferFromRefId","Remarks"],
                                                           $sql,
                                                           [true,true,true,false],
                                                           "gridTable");

                                             ?>
                                          </span>
                                          <?php
                                             btnINRECLO([true,true,false]);
                                          ?>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           <?php } ?>
                  </div>
                  <div id="divView">
                     <div class="mypanel">
                        <div class="panel-top">
                           <span id="modalTitle">INSERTING NEW<span> ACCOUNTABILITY
                        </div>
                        <div class="panel-mid">
                           <div id="EntryScrn">
                              <div class="row" id="badgeRefId">
                                 <div class="col-xs-5">
                                    <ul class="nav nav-pills">
                                       <li class="active" style="font-size:12pt;font-weight:600;">
                                          <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                                          </span></a>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              <br>
                              <div class="row">
                                 <div class="col-xs-6">
                                    <label class="control-label">Custodian Name:</label><br/>
                                 </div>
                              </div>   
                              <div class="row">
                                 <div class="col-xs-1">
                                    <div class="form-group">
                                       <input type="text" id="CustodianId" name="sint_CustodianRefId" for="disp_CustodianName"
                                              class="form-input saveFields-- EmpLkUp-- number-- mandatory--">
                                    </div>
                                 </div>
                                 <div class="col-xs-5">
                                    <div class="form-group">
                                       <input type="text" id="disp_CustodianName"
                                              name="disp_CustodianName" class="form-input fkName--" disabled>
                                    </div>
                                 </div>
                              </div>
                              <?php
                                 $list = [
                                    array("row"=>true,
                                          "name"=>"char_AccountabilityName",
                                          "col"=>"6",
                                          "id"=>"AcctName",
                                          "label"=>"Accountability Name",
                                          "class"=>"saveFields-- mandatory--",
                                          "style"=>"")
                                 ];
                                 createInput($list);
                              ?>
                              <div class="row HideinAdd--">
                                 <div class="col-xs-8">
                                    <div class="form-group">
                                       <label class="control-label">Transfered To :</label><br/>
                                       <input type="text" id="TransferedId" name="sint_TransferToRefId" for="TransferedEmpName"
                                       class="form-input saveFields-- number-- EmpLkUp--" style="width:50px;">
                                       <input type="text" id="TransferedEmpName" name="TransferedEmpName" class="form-input fkName--" style="width:50%" disabled>
                                    </div>
                                    <?php createButton("Transfer Now","btnTransferNow","btn-cls4-lemon","fa-forward",""); ?>
                                    <?php createButton("Cancel","btnCancelNow","btn-cls4-red","fa-undo",""); ?>
                                 </div>
                              </div>

                              <div class="row">
                                 <div class="col-xs-6">
                                    <div class="form-group">
                                       <label class="control-label" for="inputs">Remarks:</label>
                                       <textarea class="form-input saveFields--" rows="5" name="char_Remarks" placeholder="remarks"></textarea>
                                    </div>
                                 </div>
                              </div>
                              <input type="hidden" value="<?php echo $EmpRefId; ?>" name="sint_EmployeesRefId" class="form-input saveFields-- mandatory--">
                              <input type="hidden" name="char_TransferedBy" class="form-input saveFields--">
                           </div>
                        </div>
                        <div class=" row panel-bottom">
                           <div id="sacaba">
                              <?php
                                 btnSACABA([true,true,true]);
                                 createButton("Transfer","btnTransfer","btn-cls4-lemon","fa-exchange","");
                              ?>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php modalEmpLkUp(); ?>
            <?php
               footer();
               doHidden("objEmpRefIdFill","","");
               doHidden("objEmpFullNameFill","","");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>



