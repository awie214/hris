
<div class="row">
   <div class="col-xs-6">
      <div class="row margin-top">
         <div class="col-xs-4 text-right">
            <!--<label class="control-label" for="inputs">AGENCY ID NO./EMPLOYEE ID NO.:</label>-->
            <label class="label control-label" for="inputs">AGENCY ID NO:</label>
         </div>
         <div class="col-xs-8">
            <input class="form-input saveFields-- text-center" type="text" id="" name="sint_AgencyId" value="">
         </div>
      </div>
      <div class="row margin-top">
         <div class="col-xs-4 text-right">
            <label class="label">POSITION ITEM:</label>
         </div>
         <div class="col-xs-8">
            <?php
               createSelect("PositionItem","sint_PositionItemRefId","",100,"Name","Select Position Item","");
            ?>

         </div>
      </div>
      <div class="row margin-top">
         <div class="col-xs-4 text-right">
            <label class="label control-label">POSITION:</label>
         </div>
         <div class="col-xs-8">
            <?php
               createSelect("Position","sint_PositionRefId","",100,"Name","Select Position","");
            ?>
         </div>
      </div>
      <div class="row margin-top">
         <div class="col-xs-4 text-right">
            <label class="label control-label" for="inputs">OFFICE (CO/RO):</label>
         </div>
         <div class="col-xs-8">
            <?php
               createSelect("Office","sint_OfficeRefId","",100,"Name","Select Office","");
            ?>
         </div>
      </div>
      <div class="row margin-top">
         <div class="col-xs-4 text-right">
            <label class="label control-label" for="inputs">DEPARTMENT:</label>
         </div>
         <div class="col-xs-8">
            <?php
               createSelect("Department","sint_DepartmentRefId","",100,"Name","Select Department","");
            ?>
         </div>
      </div>
      <div class="row margin-top">
         <div class="col-xs-4 text-right">
            <label class="label control-label" for="inputs">DIVISION:</label>
         </div>
         <div class="col-xs-8">
            <?php
               createSelect("Division","sint_DivisionRefId","",100,"Name","Select Division","");
            ?>
         </div>
      </div>
      <div class="row margin-top">
         <div class="col-xs-4 text-right">
            <label class="label control-label" for="inputs">EMPLOYEE STATUS:</label>
         </div>
         <div class="col-xs-8">
            <?php
               createSelect("EmpStatus","sint_EmpStatusRefId","",100,"Name","Select Emp Status","");
            ?>
         </div>
      </div>
   </div>
   <div class="col-xs-6">
      <div class="row">
         <div class="col-xs-12">
            <div class="row">
               <div class="col-xs-4 txt-right">
                  <label class="control-label" for="inputs">SALARY GRADE:</label>
               </div>
               <div class="col-xs-8">
                  <?php
                     createSelect("SalaryGrade","sint_SalaryGradeRefId","",100,"Name","Select Salary Grade","");
                  ?>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-4 txt-right">
                  <label class="control-label" for="inputs">SALARY STEP:</label>
               </div>
               <div class="col-xs-8">
                  <?php
                     createSelect("StepIncrement","sint_StepIncrementRefId","",100,"Name","Select Step Increment","");
                  ?>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-4 txt-right">
                  <label class="control-label" for="inputs">SALARY AMOUNT:</label>
               </div>
               <div class="col-xs-8">
                  <input class="form-input  saveFields--" type="text" id="" name="deci_SalaryAmount" value="">
               </div>
            </div>
         </div>
      </div>
      <!--
      <div class="row margin-top">
         <div class="col-xs-12">
            <div class="row">
               <div class="col-xs-6 txt-right">
                  <label class="control-label" for="inputs">PAY PERIOD:</label>
               </div>
               <div class="col-xs-6">
                  <input class="form-input  saveFields--" type="text" id="" name="char_PayPeriod" value="">
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-6 txt-right">
                  <label class="control-label" for="inputs">PAY RATE:</label>
               </div>
               <div class="col-xs-6">
                  <input class="form-input  saveFields--" type="text" id="" name="sint_PayrateRefId" value="">
               </div>
            </div>
         </div>
      </div>
      -->
   </div>
</div>