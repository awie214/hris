<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   include 'incSignatory.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName LIMIT 5";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
   
?>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style type="text/css">
         body {
            font-family:Arial;
         }
         @media print {
            @page {
               size: landscape;
            }
         }
      </style>
   </head>
   <body>
      <div class="container-fluid">
         <div class="row">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12 text-center">
                     <span style="font-size: 16pt;">
                        <b>PHILIPPINE INSTITUTE FOR DEVELOPMENT STUDIES</b>
                     </span>
                     <br>
                     <span style="font-size: 12pt;">
                        18th Floor, Three Cyberpod Centris-North Tower, EDSA corner Quezon Avenue, Quezon City
                     </span>
                     <br>
                     <br>
                     <span style="font-size: 14pt;">
                        <b>AGENCY REMITTANCE ADVICE</b>
                     </span>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <span><b>Agency Name:</b> Philippine Institute for Development Studies</span>
                     <br>
                     <span><b>Agency BP Number:</b> BP1000011921</span>
                     <br>
                     <span>
                        <b>FORM D.</b>List of employees with LWOP and for termination
                     </span>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table style="width: 100%;">
                        <thead>
                           <tr class="colHEADER" valign="bottom">
                              <th rowspan="2">Member BP<br>Number</th>
                              <th rowspan="2">Last Name</th>
                              <th rowspan="2">First Name</th>
                              <th rowspan="2">Suffix</th>
                              <th rowspan="2">MI</th>
                              <th rowspan="2">Date of Birth</th>
                              <th rowspan="2">Reason</th>
                              <th rowspan="2">Termination Date</th>
                              <th colspan="2">Inclusive Date</th>
                              <th rowspan="2">Remarks</th>
                           </tr>   
                           <tr class="colHEADER" valign="bottom">
                              <th>From</th>
                              <th>To</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php
                              while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                                 $FullName   = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
                                 $ResiStreet = $row_emp["ResiStreet"];
                                 $ResiSubd   = $row_emp["ResiSubd"];
                                 $ResiBrgy   = $row_emp["ResiBrgy"];
                                 $MobileNo   = $row_emp["MobileNo"];
                                 $EmailAdd   = $row_emp["EmailAdd"];
                                 $BirthDate  = $row_emp["BirthDate"];
                                 $BirthPlace = $row_emp["BirthPlace"];
                                 $ResiAddCityRefId      = getRecord("city",$row_emp["ResiAddCityRefId"],"Name");
                                 $ResiAddProvinceRefId  = getRecord("province",$row_emp["ResiAddProvinceRefId"],"Name");
                                 $ResiAddress = "";
                                 if ($ResiStreet != "") $ResiAddress .= "$ResiStreet, ";
                                 if ($ResiSubd != "") $ResiAddress .= "$ResiSubd, ";
                                 if ($ResiBrgy != "") $ResiAddress .= "$ResiBrgy, ";
                                 if ($ResiAddCityRefId != "") $ResiAddress .= "$ResiAddCityRefId, ";
                                 if ($ResiAddProvinceRefId != "") $ResiAddress .= "$ResiAddProvinceRefId";
                                 $CivilStat = $row_emp['CivilStatus']; 
                                 switch ($CivilStat) {
                                    case "Si":
                                       $CivilStat = 'Single';
                                    break;
                                    case "Ma":
                                       $CivilStat = 'Married';
                                    break;
                                    case "An":
                                       $CivilStat = 'Annulled';
                                    break;
                                    case "Wi":
                                       $CivilStat = 'Widowed';
                                    break;
                                    case "Se":
                                       $CivilStat = 'Separated';
                                    break;
                                    case "Ot":
                                       $CivilStat = 'Others';
                                    break;
                                 }
                                 $Gender = $row_emp["Sex"];
                                 if ($Gender == "M") {
                                    $Gender = "Male";
                                 } else if ($Gender == "F") {
                                    $Gender = "Female";
                                 } else {
                                    $Gender = "";
                                 }
                                 if ($BirthDate != "") {
                                    $BirthDate = date("m/d/Y",strtotime($BirthDate));
                                 } else {
                                    $BirthDate = "";
                                 }
                                 $emp_info = FindFirst("empinformation","WHERE EmployeesRefId = ".$row_emp["RefId"],"*");
                                 if ($emp_info) {
                                    $Position      = rptDefaultValue($emp_info["PositionRefId"],"position");
                                    $ApptStatus    = rptDefaultValue($emp_info["ApptStatusRefId"],"apptstatus");
                                    $SalaryAmount  = number_format($emp_info["SalaryAmount"],2);
                                    $Assumption    = date("m/d/Y",strtotime($emp_info["AssumptionDate"]));
                                 } else {
                                    $Position      = "";
                                    $ApptStatus    = "";
                                    $Assumption    = "";
                                    $SalaryAmount  = "0.00";
                                 }
                                 echo '<tr>';
                                    echo '
                                       <td></td>
                                       <td>'.$row_emp["LastName"].'</td>
                                       <td>'.$row_emp["FirstName"].'</td>
                                       <td>'.$row_emp["ExtName"].'</td>
                                       <td>'.substr($row_emp["MiddleName"], 0,1).'</td>
                                       <td>'.$BirthDate.'</td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                    ';
                                 echo '</tr>';
                              }
                           ?>
                        </tbody>
                     </table>
                  </div>
               </div>
               <br>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-6">
                     Prepared by:
                     <br><br><br>
                     <b><u><?php echo $prepared_by; ?></u></b>
                     <br>
                     <?php echo $prepared_by_position; ?>
                  </div>
                  <div class="col-xs-6">
                     Certified Correct:
                     <br><br><br>
                     <b><u><?php echo $corrected_by; ?></u></b>
                     <br>
                     <?php echo $corrected_by_position; ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>