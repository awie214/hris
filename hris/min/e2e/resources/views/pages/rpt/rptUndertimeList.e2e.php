<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   //$whereClause .= " LIMIT 10";
   $table = "employees";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   $from = getvalue("txtAttendanceDateFrom");
   $to   = getvalue("txtAttendanceDateTo");
   $month = getvalue("txtAttendanceMonth");
   $year  = getvalue("txtAttendanceYear");

   $from    = $year."-".$month."-01";
   $to      = $year."-".$month."-".cal_days_in_month(CAL_GREGORIAN,$month,$year);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $errmsg = "";
            rptHeader(getRptName(getvalue("drpReportKind")));
            if ($rsEmployees && $errmsg == "")
            {
         ?>
         <p class="txt-center">
            <?php
               echo "For the Date ";
               echo date("m/d/Y",strtotime($from))." - ".date("m/d/Y",strtotime($to));
            ?>
         </p>
         <table border="1">
            <tr>
               <th>Name of Employee</th>
               <?php for ($j=1;$j<=31;$j++) {?>
                  <th class="txt-center" style="width:30px;"><?php echo $j ?></th>
               <?php } ?>
               <th>Total</th>
            </tr>
            <?php
               $count = 0;
               while ($row_emp = mysqli_fetch_assoc($rsEmployees) ) {
                  if ($p_filter_value == "0" || $p_filter_table == "") {
                     $emprefid = $row_emp["RefId"];
                  } else {
                     $emprefid   = $row_emp["EmployeesRefId"];
                  }
                  $UT_count = 0;
                  $biometricsID   = $row_emp["BiometricsID"];
                  $CompanyID      = $row_emp["CompanyRefId"];
                  $BranchID       = $row_emp["BranchRefId"];
                  $Default_qry    = "WHERE CompanyRefId = ".$CompanyID." AND BranchRefId = ".$BranchID;
                  $workschedrefid = FindFirst("empinformation","WHERE EmployeesRefId = ".$row_emp["RefId"],"WorkscheduleRefId");
                  if (is_numeric($workschedrefid)) {
                     for ($v=1;$v<=31;$v++) {
                        if ($v <= 9) $v = "0".$v; 
                        ${"ut_".$row_emp["RefId"]."_".$v} = "&nbsp;";
                     } 
                     $count++;
                     $curr_date   = date("Y-m-d",time());
                     $month_start = $from;
                     $month_end   = $to;
            ?>
               <tr>
                  <td class="pad-left">
                     <?php 
                        echo $row_emp['LastName'].', '.$row_emp['FirstName'].', '.$row_emp['MiddleName'];
                     ?>
                  </td>
                  <?php
                     $dtr = FindFirst("dtr_process","WHERE EmployeesRefId = '$emprefid' AND Month = '$month' AND Year = '$year'","*");
                     if ($dtr) {
                        for ($v=1;$v<=31;$v++) {
                           if ($v <= 9) $v = "0".$v; 
                           $UT  = $dtr[$v."_UT"];
                           if ($UT > 0) {
                              $UT_count++;
                              echo '<td class="txt-center">'.rpt_HoursFormat($UT).'</td>';   
                           } else {
                              echo '<td class="txt-center"></td>';   
                           }
                        }   
                     } 
                  ?>
                  <td class="txt-center">
                     <?php echo $UT_count; ?>
                  </td>
               </tr>
            <?php
                  }
               }
               echo "RECORD COUNT : ".$count;
            }else {
               echo '<div>NO RECORD QUERIED base on your criteria!!!</div>';
               echo '<div>'.$errmsg.'</div>';
            }
            ?>
         </table>
         <p>
            <div class="row">
               <div class="col-xs-2 txt-right">Prepared By:</div>
               <div class="col-xs-4"></div>
               <div class="col-xs-2 txt-right">Approved By:</div>
               <div class="col-xs-4"></div>
            </div>
            <div class="row">
               <div class="col-xs-2"></div>
               <div class="col-xs-4">________________________</div>
               <div class="col-xs-2"></div>
               <div class="col-xs-3">________________________</div>
               <div class="col-xs-1"></div>
            </div>
         </p>

      </div>
   </body>
</html>