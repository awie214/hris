<?php
   include_once 'pageHEAD.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   $refid = getvalue("refid");
   $row = FindFirst("employeesauthority","WHERE RefId = $refid","*");
   if ($row) {
   	$FiledDate 			= $row["FiledDate"];
   	$FiledDate 			= date("F d, Y",strtotime($FiledDate));
		$Signatory1       = $row["Signatory1"];
     	$Signatory2       = $row["Signatory2"];
     	$Signatory3       = $row["Signatory3"];
     	$Purpose 			= $row["Remarks"];

     	$s1_row           = FindFirst("signatories","WHERE RefId = '$Signatory1'","*");
     	$s2_row           = FindFirst("signatories","WHERE RefId = '$Signatory2'","*");

     	$s1_name          = $s1_row["Name"];
     	$s1_position      = getRecord("position",$s1_row["PositionRefId"],"Name");

     	$s2_name          = $s2_row["Name"];
     	$s2_position      = getRecord("position",$s2_row["PositionRefId"],"Name");
     
		$authority = getRecord("absences",$row["AbsencesRefId"],"Name");
		$code      = getRecord("absences",$row["AbsencesRefId"],"Code");
		$authority = strtoupper($authority);
		$Remarks = $authority." - ".$row["Remarks"];
		$Destination = $row["Destination"];
		$emprefid = $row["EmployeesRefId"];
		$employees = FindFirst("employees","WHERE RefId = '$emprefid'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
		if ($employees) {
			$FirstName = $employees["FirstName"];
			$LastName = $employees["LastName"];
			$MiddleName = $employees["MiddleName"];
			$ExtName = $employees["ExtName"];
			$FullName = $LastName.", ".$FirstName." $ExtName ".$MiddleName;
		} else {
			$FullName = "&nbsp;";
		}
		$empinformation = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
		if ($empinformation) {
			$OfficeRefId = getRecord("office",$empinformation["OfficeRefId"],"Name");
			$DivisionRefId = getRecord("division",$empinformation["DivisionRefId"],"Name");
			$PositionRefId = getRecord("position",$empinformation["PositionRefId"],"Name");
		} else {
			$OfficeRefId = $DivisionRefId = $PositionRefId = "&nbsp;";
		}
		if ($row["Whole"] == 1) {
			$start_time = $end_time = "Whole Day";
		} else {
			$start_time = HrsFormat($row["FromTime"]);
			$end_time = HrsFormat($row["ToTime"]);
		}
		$from = $row["ApplicationDateFrom"];
		$to = $row["ApplicationDateTo"];
		if ($from == $to) {
			$date = date("F d, Y",strtotime($from));
		} else {
			$date = date("F d, Y",strtotime($from))." to ".date("F d, Y",strtotime($to));;
		}	
   }
?>
<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		td {
			border: 2px solid black;
			vertical-align: top;
			padding: 5px;
			font-size: 9pt;
		}
		.data {
			font-size: 10pt;
			text-transform: uppercase;
		}
		@media print {
			body {
				font-size: 10pt;
			}
		}
	</style>
</head>
<body>
	<div class="container-fluid rptBody">
		<div style="page-break-after: always;">
	   	<div class="row">
	   		<div class="col-xs-12 text-center" style="font-size: 15pt;">
	   			INDIVIDUAL PASS SLIP
	   		</div>
	   	</div>
	   	<div class="row margin-top">
	   		<div class="col-xs-12" style="border: 1px solid black; padding: 20px;">
	   			<div class="row">
	   				<div class="col-xs-8 text-center">
	   					<u><b><?php echo $FullName; ?></b></u>
	   					<br>
	   					(Printed name and signature of employee)
	   				</div>
	   				<div class="col-xs-4 text-center">
	   					<u><b><?php echo $FiledDate; ?></b></u>
	   					<br>
	   					(Date)
	   				</div>
	   			</div>
	   			<br>
	   			<div class="row margin-top">
	   				<div class="col-xs-12">
	   					Permission is requested to:
	   				</div>
	   			</div>
	   			<div class="row margin-top">
	   				<div class="col-xs-6 text-right">
	   					Leave the office during office hours from
	   				</div>
	   			</div>
	   			<div class="row margin-top">
	   				<div class="col-xs-6 text-right">
	   					Intended time of departure
	   				</div>
	   				<div class="col-xs-1"></div>
	   				<div class="col-xs-5">
	   					<u><b><?php echo $start_time; ?></b></u>
	   				</div>
	   			</div>
	   			<div class="row margin-top">
	   				<div class="col-xs-6 text-right">
	   					Intended time of arrival
	   				</div>
	   				<div class="col-xs-1"></div>
	   				<div class="col-xs-5">
	   					<u><b><?php echo $end_time; ?></b></u>
	   				</div>
	   			</div>
	   			<br><br>
	   			<div class="row margin-top">
	   				<div class="col-xs-4" style="padding: 5px;">
		   				<div class="row">
		   					<div class="col-xs-12" style="border: 1px solid black; padding: 5px; min-height: 100px;">
		   						<div class="row">
		   							<div class="col-xs-12">
		   								Purpose:
		   							</div>
		   						</div>
		   						<div class="row margin-top">
		   							<div class="col-xs-2"></div>
		   							<div class="col-xs-10">
		   								<input type="checkbox" name="official" id="official" disabled <?php if($code == "PSB") echo "checked"; ?>>
		   								&nbsp;&nbsp;&nbsp;OFFICIAL
		   								<br>
		   								<input type="checkbox" name="personal" id="personal" disabled <?php if($code == "PS") echo "checked"; ?>>
		   								&nbsp;&nbsp;&nbsp;PERSONAL
		   							</div>
		   						</div>
		   					</div>
		   				</div>
		   			</div>
		   			<div class="col-xs-1"></div>
		   			<div class="col-xs-7" style="padding: 5px;">
		   				<div class="row">
		   					<div class="col-xs-12" style="border: 1px solid black; padding: 5px; min-height: 100px;">
		   						<div class="row">
		   							<div class="col-xs-12">
		   								Reason: &nbsp;&nbsp;&nbsp;<b><?php echo $Purpose; ?></b>
		   							</div>
		   						</div>
		   					</div>
		   				</div>
		   			</div>
	   			</div>
	   		</div>
	   	</div>
	   	<div class="row margin-top">
	   		<div class="col-xs-12" style="border: 1px solid black; padding: 20px;">
	   			<div class="row">
	   				<div class="col-xs-6">
	   					Approved:
	   				</div>
	   				<div class="col-xs-6 text-center">
	   					<br><br><br>
	   					<b><?php echo "<u>".$s1_name."</u><br>".$s1_position; ?></b>
	   				</div>
	   			</div>
	   		</div>
	   	</div>
	   	<br><br><br>
	   	<div class="row margin-top">
	   		<div class="col-xs-12" style="border: 1px solid black; padding: 20px;">
	   			<div class="row">
			   		<div class="col-xs-12 text-center" style="font-size: 15pt;">
			   			CERTIFICATE OF APPEARANCE
			   			<br>
			   		</div>
			   	</div>
	   			<div class="row">
	   				<div class="col-xs-12">
	   					TO WHOM IT MAY CONCERN:
	   				</div>
	   			</div>
	   			<div class="row margin-top">
	   				<div class="col-xs-12" style="text-indent: 5%;">
	   					This is to certify that Mr./Ms. <u><b><?php echo $FullName; ?></b></u>
							of the Office of the Vice President personally appeared in this Agency / Company on <u><b><?php echo $start_time ?></b></u> at <u><b><?php echo $end_time ?></b></u> a.m. / p.m.
							<br><br><br>
	   				</div>
	   			</div>
	   			<div class="row margin-top">
	   				<div class="col-xs-8"></div>
	   				<div class="col-xs-4 text-center">
	   					_________________________________
	   					<br>
	   					Signature over Printed Name
	   					<br>
	   					of Attending Employee/Position
	   					<br><br>
	   				</div>
	   			</div>
	   			<div class="row margin-top">
	   				<div class="col-xs-8"></div>
	   				<div class="col-xs-4 text-center">
	   					Date: ___________________________
	   					<br><br><br>
	   				</div>
	   			</div>
	   			<div class="row margin-top">
	   				<div class="col-xs-2">Name of Agency:</div>
	   				<div class="col-xs-9 text-center" style="border-bottom: 1px solid black;">
	   					&nbsp;
	   				</div>
	   			</div>
	   			<div class="row margin-top">
	   				<div class="col-xs-2">Address:</div>
	   				<div class="col-xs-9 text-center" style="border-bottom: 1px solid black;">
	   					&nbsp;
	   				</div>
	   			</div>
	   			<div class="row margin-top">
	   				<div class="col-xs-2">Telephone No.:</div>
	   				<div class="col-xs-9 text-center" style="border-bottom: 1px solid black;">
	   					&nbsp;
	   				</div>
	   			</div>
	   			<div class="row margin-top">
	   				<div class="col-xs-12 text-center">
	   					<br>
	   					In case an employee buys office supplies, said employees shall attach an authenticated copy of OR purchases
	   				</div>
	   			</div>
	   		</div>
	   	</div>
	   </div>
    </div>
</body>
</html>