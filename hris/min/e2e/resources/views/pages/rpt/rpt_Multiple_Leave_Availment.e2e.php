<?php
   include_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   $emprefid            = getvalue("hEmpRefId");
   $from                = getvalue("date_from");
   $to                  = getvalue("date_to");
   $leaves_id           = getvalue("leave_type");
   $Inclusive_Date      = "";
   $personnel_officer   = "";
   $authorized_official = "";
   $approver            = "";
   $spl_types           = "";
   $leave_code          = "";
   $leave_names         = "";
   $leave_days          = 0;
   $where               = "WHERE EmployeesRefId = '$emprefid'";
   $where               .= " AND ApplicationDateFrom >= '$from'";
   $where               .= " AND ApplicationDateTo <= '$to'";
   $where               .= " AND LeavesRefId = '$leaves_id'";
   $where_credit  = "WHERE EmployeesRefId = '$emprefid' AND NameCredits = 'VL' AND EffectivityYear = '".date("Y",strtotime($from))."'";
   $leave_rs      = SelectEach("employeesleave",$where);
   $credit_detail = FindLast("employeescreditbalance",$where_credit,"*");
   if ($leave_rs) {
      if (mysqli_num_rows($leave_rs) > 0) {
         while ($leave_row = mysqli_fetch_assoc($leave_rs)) {
            $day_count  = count_leave($emprefid,$leave_row["ApplicationDateFrom"],$leave_row["ApplicationDateTo"]); 
            $spl_type   = $leave_row["SPLType"];
            $leave_type = $leave_row["LeavesRefId"];
            $leave_name = FindFirst("leaves","WHERE RefId = $leave_type","`Code`,`Name`");
            $name       = $leave_name["Name"];
            $code       = $leave_name["Code"];

            $FiledDate  = date("F d, Y",strtotime($leave_row["FiledDate"]));
            $Date_To    = date("F d, Y",strtotime($leave_row["ApplicationDateTo"]));
            $Date_From  = date("F d, Y",strtotime($leave_row["ApplicationDateFrom"]));
            if ($Date_From == $Date_To) {
               $Inclusive_Date .= "$Date_From<br>";
            } else {
               $Inclusive_Date .= "$Date_From to $Date_To<br>";
            }
            $leave_days    += $day_count;
            $leave_code    .= $code."|";
            $leave_names   .= $name."|";
            if ($code == "SPL") {
               $spl_types .= $spl_type."|";
            }
         }
      } else {
         echo "No Leave For Selected Criteria";
         return false;   
      }
   } else {
      echo "No Leave For Selected Criteria";
      return false;
   }

   if ($credit_detail) {
      $year          = $credit_detail["EffectivityYear"]; 
      $balance_date  = $credit_detail["BegBalAsOfDate"];
      $from_bal      = date("m",strtotime($balance_date." + 1 Day"));
      if (intval(date("m",strtotime($to))) > 1) {
         $to_bal     = date("m",strtotime($to)) - 1;   
      } else {
         $to_bal     = 12;
      }
      $credit_arr    = computeCredit($emprefid,$from_bal,$to_bal,$year);
      if ($to_bal <= 9) {
         $selected_month = "0".$to_bal;   
      } else {
         $selected_month = $to_bal;
      }
      $num_of_days   = cal_days_in_month(CAL_GREGORIAN,$selected_month,$year);
      $BegBalDate    = "$year-$selected_month-$num_of_days";
      $BegBalDate    = date("F d, Y",strtotime($BegBalDate));
      $VL            = $credit_arr["VL"];
      $SL            = $credit_arr["SL"];
      $total_leave   = $VL + $SL;
   } else {
      $VL = $SL = $total_leave = 0;
      $BegBalDate = "";
   }
   
   $emp_row       = FindFirst("employees","WHERE RefId = $emprefid","*");
   if ($emp_row) {
      $LastName      = $emp_row["LastName"];
      $FirstName     = $emp_row["FirstName"];
      $MiddleName    = $emp_row["MiddleName"];   
      $ExtName       = $emp_row["ExtName"];
      $FullName      = $LastName.", ".$FirstName." ".$ExtName." ".$MiddleName;
      $cid           = $emp_row["CompanyRefId"];
   } else {
      $FullName = $ExtName = $LastName = $FirstName = $MiddleName = "&nbsp;";
      $cid           = 0;
   }
   
   $empinfo_row   = FindFirst("empinformation","WHERE EmployeesRefId = $emprefid","*");

   if ($empinfo_row) {
      $Office        = getRecord("office",$empinfo_row["OfficeRefId"],"Name");
      $Agency        = getRecord("agency",$empinfo_row["AgencyRefId"],"Name");
      $Position      = getRecord("position",$empinfo_row["PositionRefId"],"Name");
      $Division      = getRecord("Division",$empinfo_row["DivisionRefId"],"Name");
      $SalaryAmount  = "₱ ".number_format($empinfo_row["SalaryAmount"],2);
      $DivisionRefId = $empinfo_row["DivisionRefId"];
   } else {
      $Office        = "&nbsp;";
      $Agency        = "&nbsp;";
      $Position      = "&nbsp;";
      $SalaryAmount  = "&nbsp;";
      $Division      = "&nbsp;";
   }
   $where_signatory  = "WHERE ForLabel = 'leave'";
   $rs_signatory     = SelectEach("signatories",$where_signatory);
   if ($rs_signatory) {
      while ($row_signatory = mysqli_fetch_assoc($rs_signatory)) {
         $SignatoryLabel = $row_signatory["SignatoryLabel"];
         switch ($SignatoryLabel) {
            case '1':
               $personnel_officer = $row_signatory["Name"];
               break;
            case '2':
               if (intval($DivisionRefId) > 0) {
                  if ($row_signatory["DivisionRefId"] == $DivisionRefId) {
                     $authorized_official = $row_signatory["Name"];
                  }
               }
               break;
            case '3':
               $approver = $row_signatory["Name"];
               break;
         }
      }
   }
   if ($cid == "35") $SalaryAmount = "&nbsp;";
?>
<!DOCTYPE HTML>
<html>
   <head>
      <?php 
         $file = "Availment of Leave";
         include_once 'pageHEAD.e2e.php';
      ?>
      <link rel="stylesheet" href="<?php echo path("js/autocomplete/css/jquery-ui.css"); ?>" type="text/css" />
      <script type="text/javascript" src="<?php echo path("js/autocomplete/jquery-ui.js") ?>"></script>
      <style type="text/css">
         .form-input {text-align: center;}
         @media print {
            body {
               font-size: 9pt;
            }
            .form-input {
               border: none;
               padding: 0px;
               font-weight: 400;
               text-decoration: underline;
               text-align: center;
            }
         }
      </style>
      <script type="text/javascript">
         $(document).ready(function () {
            EmployeeAutoComplete("employees","personnel_officer");
            EmployeeAutoComplete("employees","authorized_official");
            EmployeeAutoComplete("employees","approver");
            $("#personnel_officer, #authorized_official").blur(function () {
               var value = $(this).val();
               arr = value.split("-");
               $(this).val(arr[1]);
            });
            $("[type*='checkbox']").prop("disabled",true);
            <?php
               if ($leave_code != "") {
                  $leave_code_arr = explode("|", $leave_code);
                  foreach ($leave_code_arr as $akey => $avalue) {
                     if ($avalue != "") {
                        switch ($avalue) {
                           case 'VL':
                              echo '$("#VL_chk").prop("checked",true);';
                              $name = "";
                              break;
                           case 'SL':
                              echo '$("#SL_chk").prop("checked",true);';
                              $name = "";
                              break;
                           case 'ML':
                              echo '$("#ML_chk").prop("checked",true);';
                              $name = "";
                              break;
                           default:
                              echo '$("#OT_chk").prop("checked",true);';
                              break;
                        }         
                     }
                  }   
               }
               if ($spl_types != "") {
                  $spl_types_arr = explode("|", $spl_types);
                  foreach ($spl_types_arr as $bkey => $bvalue) {
                     if ($bvalue != "") {
                        switch ($bvalue) {
                           case 'PM':
                              $name = "SPL - Personal Milestone";
                              break;
                           case 'PO':
                              $name = "SPL - Parental Obligation";
                              break;
                           case 'Fil':
                              $name = "SPL - Fillial";
                              break;
                           case 'DomE':
                              $name = "SPL - Domestic Emergencies";
                              break;
                           case 'PTrn':
                              $name = "SPL - Personal Transactions";
                              break;
                           case 'C':
                              $name = "SPL - Calamity";
                              break;
                        }         
                     }
                  }
                  echo '$("#other_leave").html("'.$name.'");';   
               }
            ?>

         });
      </script>
   </head>
   <body>
      <div class="container-fluid">
         <div class="row">
            <div class="col-xs-12">
               CSC Form 6
               <br>
               Revised 1998
            </div>
         </div>
         <div class="row text-center" style="border:1px solid black; border-bottom: 1px solid white;">
            <div class="col-xs-12">
               <h3>APPLICATION FOR LEAVE</h3>
            </div>
         </div>
         <div class="row" style="border:1px solid black;margin-bottom:40px;">
            <div class="col-xs-12">
               <div class="row" style="border-bottom:1px solid black;">
                  <div class="col-xs-3" style="border-right:1px solid black;">
                     <label>1. Office/Agency</label>
                     <br>
                     <?php echo $Division; ?>
                  </div>
                  <div class="col-xs-9">
                     <div class="row">
                        <div class="col-xs-2">
                           <label>2. Name</label>
                           <br>
                           &nbsp;
                        </div>
                        <div class="col-xs-10">
                           <div class="row">
                              <div class="col-xs-4">
                                 <label>(Last)</label>
                                 <br>
                                 <?php echo $LastName; ?>
                              </div>
                              <div class="col-xs-4">
                                 <label>(First)</label>
                                 <br>
                                 <?php echo $FirstName." ".$ExtName; ?>
                              </div>
                              <div class="col-xs-4">
                                 <label>(Middle)</label>
                                 <br>
                                 <?php echo $MiddleName; ?>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row" style="border-bottom:1px solid black;">
                  <div class="col-xs-3" style="border-right:1px solid black;">
                     <label>3. Date of Filing</label>
                     <br>
                     <?php echo $FiledDate; ?>
                  </div>
                  <div class="col-xs-9">
                     <div class="row">
                        <div class="col-xs-6" style="border-right:1px solid black;">
                           <label>4. Position</label>
                           <br>
                           <?php echo $Position; ?>
                        </div>
                        <div class="col-xs-6">
                           <label>5. Salary</label>
                           <br>
                           <?php echo $SalaryAmount; ?>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row text-center" style="border-bottom:1px solid black;">
                  <label>DETAILS OF APPLICATION</label>
               </div>
               <div class="row" style="border-bottom:1px solid black;">
                  <div class="col-xs-6">
                     <div class="row">
                        <div class="col-xs-12">
                           <label>6. a) Type of Leave:</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row">
                              <input type="checkbox" id="VL_chk">&nbsp;<label>Vacation</label>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-11">
                                 <div class="row">
                                    <input type="checkbox">&nbsp;<label>To seek employment</label>
                                 </div>
                                 <div class="row margin-top">
                                    <input type="checkbox">&nbsp;<label>Others (Specify)</label>
                                    <u><input type="text" name="" id="" class="form-input"></u>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row margin-top">
                              <input type="checkbox" id="SL_chk">&nbsp;<label>Sick</label>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox" id="ML_chk">&nbsp;<label>Maternity</label>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox" id="OT_chk">&nbsp;<label>Others (Specify)</label>
                              <label><u id="other_leave"></u></label>
                           </div>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           <label>6. c) # of Working Days Applied for:</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <label>
                              <?php
                                 echo '<u>'.$leave_days.' day(s)</u>';
                              ?>
                           </label>
                           <br>
                           <label>Inclusive Dates:</label>
                           <br>
                           <label class="text-left"><u><?php echo $Inclusive_Date; ?></u></label>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-6" style="border-left:1px solid black;">
                     <div class="row">
                        <div class="col-xs-12">
                           <label>6. b) Where Leave will be Spent:</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row margin-top">
                              <label>(1)&nbsp;In case of Vacation Leave:</label>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox">&nbsp;<label>Within the Philippines</label>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox">&nbsp;<label>Abroad (Specify)</label>
                              <!-- <label>__________________________________________</label> -->
                              <u><input type="text" name="" id="" class="form-input"></u>
                           </div>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row margin-top">
                              <label>(2)&nbsp;In case of Sick Leave:</label>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox">&nbsp;<label>In hospital (Specify)</label>
                              <br>
                              <!-- <label>__________________________________________</label> -->
                              <u><input type="text" name="" id="" class="form-input"></u>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox">&nbsp;<label>Out-Patient (Specify)</label>
                              <br>
                              <!-- <label>__________________________________________</label> -->
                              <u><input type="text" name="" id="" class="form-input"></u>
                           </div>
                        </div>
                     </div>
                     <?php spacer(6); ?>
                     <div class="row">
                        <div class="col-xs-12">
                           <label>6. d) Commutation:</label>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row">
                              <div class="col-xs-6">
                                 <input type="checkbox">&nbsp;<label>Requested</label>
                              </div>
                              <div class="col-xs-6">
                                 <input type="checkbox">&nbsp;<label>Not Requested</label>
                              </div>
                           </div>
                           <div class="row margin-top text-center">
                              <br><br>
                              <label><u><?php echo $FullName; ?></u></label>
                              <br>
                              <label>(Signature of Applicant)</label>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row text-center" style="border-bottom:1px solid black;">
                  <label>DETAILS OF ACTION ON APPLICATION</label>
               </div>
               <div class="row" style="border-bottom:1px solid black;">
                  <div class="col-xs-6" style="border-right:1px solid black;">
                     <div class="row">
                        <div class="col-xs-12">
                           <label>7. a) Certification of Leave Credits as of:</label>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <?php
                              if ($BegBalDate != "") {
                                 echo '<label><u>'.$BegBalDate.'</u></label>';
                              } else {
                                 echo '<label>__________________________________________</label>';      
                              }
                           ?>
                           <br>
                           <div class="row margin-top" style="border:1px solid black;">
                              <div class="col-xs-4 text-center" style="border-right:1px solid black;">
                                 <div class="row" style="border-bottom:1px solid black;">
                                    <label>Vacation</label>
                                 </div>
                                 <div class="row margin-top" style="border-bottom:1px solid black;">
                                    <label>
                                       <?php echo $VL; ?>
                                    </label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>Days</label>
                                 </div>
                              </div>
                              <div class="col-xs-4 text-center" style="border-right:1px solid black;">
                                 <div class="row" style="border-bottom:1px solid black;">
                                    <label>Sick</label>
                                 </div>
                                 <div class="row margin-top" style="border-bottom:1px solid black;">
                                    <label>
                                       <?php echo $SL; ?>
                                    </label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>Days</label>
                                 </div>
                              </div>
                              <div class="col-xs-4 text-center">
                                 <div class="row" style="border-bottom:1px solid black;">
                                    <label>Total</label>
                                 </div>
                                 <div class="row margin-top" style="border-bottom:1px solid black;">
                                    <label>
                                       <?php echo $total_leave; ?>
                                    </label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>Days</label>
                                 </div>
                              </div>
                           </div>
                           <?php spacer(40);?>
                           <div class="row text-center">
                              <!-- <label>__________________________________________</label> -->
                              <input type="text" 
                                     name="personnel_officer" 
                                     id="personnel_officer" 
                                     class="form-input" 
                                     value="<?php echo $personnel_officer; ?>" 
                              >
                              <br>
                              <label>(Personnel Officer)</label>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-6">
                     <div class="row">
                        <div class="col-xs-12">
                           <label>7. b) Recommendation</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row">
                              <input type="checkbox" id="is_approve"><label>&nbsp;Approval</label>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox"><label>&nbsp;Disapproval due to</label>
                              <br>
                              <label>__________________________________________</label>
                              <br>
                              <label>__________________________________________</label>
                           </div>
                        </div>
                     </div>
                     <?php spacer(40);?>
                     <div class="row text-center">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <!-- <label>__________________________________________</label> -->
                           <input type="text" 
                                  name="authorized_official" 
                                  id="authorized_official" 
                                  class="form-input" 
                                  value="<?php echo $authorized_official; ?>" 
                           >
                           <br>
                           <label>(Authorized Official)</label>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-6" style="border-right:1px solid black;">
                     <div class="row">
                        <div class="col-xs-12">
                           <label>7. c) APPROVED FOR:</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row">
                              <div class="col-xs-6">
                                 <div class="row">
                                    <label>____________________</label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>____________________</label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>____________________</label>
                                 </div>
                              </div>
                              <div class="col-xs-6">
                                 <div class="row">
                                    <label>day/s with pay</label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>day/s without pay</label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>Others (Specify)</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-6">
                     <div class="row">
                        <div class="col-xs-12">
                           <label>7. d) DISAPPROVED DUE TO:</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10 text-center">
                           <label>_____________________________________</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10 text-center">
                           <label>_____________________________________</label>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-xs-3"></div>
            <div class="col-xs-6 text-center">
               <input type="text" 
                      name="approver" 
                      id="approver" 
                      class="form-input" 
                      value="<?php echo $approver; ?>" 
               >
               <br>
               <label>(Name & Signature of Authorized Official)</label>
            </div>
         </div>
      </div>
   </body>
</html>