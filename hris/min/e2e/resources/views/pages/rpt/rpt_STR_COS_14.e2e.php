<?php
	require_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   require_once pathClass.'0620RptFunctions.e2e.php';
   require_once pathClass.'DTRFunction.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $whereClause .= " ORDER BY LastName";
   $table = "employees";
   $rsEmployees_week1 = SelectEach($table,$whereClause);
   $rsEmployees_week2 = SelectEach($table,$whereClause);
   $rsEmployees_week3 = SelectEach($table,$whereClause);
   $rsEmployees_week4 = SelectEach($table,$whereClause);

   $start_date 	= getvalue("txtStartDate");
   $end_date      = date("Y-m-d",strtotime($start_date." + 15 Day"));
   $period  = date("F d",strtotime($start_date))." to ".date("F d, Y",strtotime($end_date));

   $cos = FindFirst("empstatus","WHERE Code = 'COS'","RefId");
   function chkDay($day,$month,$year) {
   	$date = $year."-".$month."-".$day;
   	$curr_day = date("D",strtotime($date));
   	if ($curr_day == "Sat" || $curr_day == "Sun") {
   		return $curr_day;
   	} else {
   		return $day."-".date("M",strtotime($date));;
   	}
   }
   function rmvValue($value){
   	if ($value > 0) {
   		return $value;
   	} else {
   		return "&nbsp;";
   	}
   }
   $week1         = date("Y-m-d",strtotime($start_date." + 8 Day"));
   $new_week      = date("Y-m-d",strtotime($start_date." + 8 Day"));
   $week2         = date("Y-m-d",strtotime($week1." + 8 Day"));
   $start_week1   = date("Y-m-d",strtotime($start_date));
   $end_week1     = date("Y-m-d",strtotime($week1));
   $start_week2   = date("Y-m-d",strtotime($week1." + 1 Day"));
   $end_week2     = date("Y-m-d",strtotime($week2));
   // echo "Week1_Start: ".$start_week1."<br>";
   // echo "Week1_End: ".$end_week1."<br>";
   // echo "Week2_Start: ".$start_week2."<br>";
   // echo "Week2_End: ".$end_week2."<br>";
   $start_month   = date("m",strtotime($start_date));
   $end_month     = date("m",strtotime($end_date));
   $year_range    = date("Y",strtotime($end_date));
   if ($start_month == $end_month) {
      $where_start   = "AND Month = '$start_month' AND Year = '$year_range'";
      $where_end     = "AND Month = '$start_month' AND Year = '$year_range'";
   } else {
      $where_start   = "AND Month = '$start_month' AND Year = '$year_range'";
      $where_end     = "AND Month = '$end_month' AND Year = '$year_range'";
   }
   
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php include_once $files["inc"]["pageHEAD"]; ?>
   <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
   <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   <style type="text/css">
   	th, td {
   		font-size: 7pt;
   	}
   	body {
   		background:#ffffff;
		   color:#000;
		   font-size:10pt;
		   font-family:Arial;
		   margin: 0;
   	}
   	@media print {
   		td {
   			font-size: 6pt;
   		}
   	}
   </style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-12 text-center">
						METROPOLITAN WATERWORKS AND SEWERAGE SYSTEM REGULATORY OFFICE
						<br>
						Katipunan Road, Balara, Quezon City
						<br>
						<b>SUMMARY TIME REPORT</b>
						<br>
						For the period <b><?php echo $period; ?></b>
					</div>
				</div>
			   <?php
               include_once 'inc/STR_week1_COS.php';
               include_once 'inc/STR_week2_COS.php';
            ?>
			</div>
		</div>
	</div>
</body>
</html>