<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY BirthDate";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $count = 0;
            if ($rsEmployees) {
         ?>
         <table style="width: 100%;">
            <thead>
               <tr>
                  <td colspan="5">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                  </td>
               </tr>
               <tr class="colHEADER">
                  <th style="width: 10%;">Sequence</th>
                  <th style="width: 45%;">Employee Name</th>
                  <th style="width: 15%;">Employee ID</th>
                  <th style="width: 15%;">Birthdate</th>
                  <th style="width: 15%;">Age</th>
               </tr>
            </thead>
            <tbody>
               <?php
                  $Male_20_Below       = "";
                  $Male_21_30          = "";
                  $Male_31_40          = "";
                  $Male_41_50          = "";
                  $Male_51_60          = "";
                  $Male_60_Above       = "";
                  $Total_Male_20_Below = 0;
                  $Total_Male_21_30    = 0;
                  $Total_Male_31_40    = 0;
                  $Total_Male_41_50    = 0;
                  $Total_Male_51_60    = 0;
                  $Total_Male_60_Above = 0;

                  $Female_20_Below       = "";
                  $Female_21_30          = "";
                  $Female_31_40          = "";
                  $Female_41_50          = "";
                  $Female_51_60          = "";
                  $Female_60_Above       = "";
                  $Total_Female_20_Below = 0;
                  $Total_Female_21_30    = 0;
                  $Total_Female_31_40    = 0;
                  $Total_Female_41_50    = 0;
                  $Total_Female_51_60    = 0;
                  $Total_Female_60_Above = 0;

                  $No_Birthday           = "";
                  $Total_No_Birthday     = 0;



                  while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                     $count++;
                     $FullName   = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
                     $BirthDate  = $row_emp["BirthDate"];
                     if ($BirthDate != "") {
                        $Today      = date("Y-m-d",time());
                        $Age        = dateDifference($BirthDate,$Today) / 365;   
                     } else {
                        $Age = "";
                     }
                     if ($Age <= 20 && $row_emp["Sex"] == "M") {
                        $Total_Male_20_Below++;
                        $Male_20_Below .= '
                        <tr>
                           <td class="text-center">'.$Total_Male_20_Below.'</td>
                           <td>'.strtoupper($FullName).'</td>
                           <td class="text-center">'.$row_emp["AgencyId"].'</td>
                           <td class="text-center">'.date("m/d/Y",strtotime($row_emp["BirthDate"])).'</td>
                           <td class="text-center">'.intval($Age).' year(s)</td>
                        </tr>';
                     } else if ($Age <= 20 && $row_emp["Sex"] == "F") {
                        $Total_Female_20_Below++;
                        $Female_20_Below .= '
                        <tr>
                           <td class="text-center">'.$Total_Female_20_Below.'</td>
                           <td>'.strtoupper($FullName).'</td>
                           <td class="text-center">'.$row_emp["AgencyId"].'</td>
                           <td class="text-center">'.date("m/d/Y",strtotime($row_emp["BirthDate"])).'</td>
                           <td class="text-center">'.intval($Age).' year(s)</td>
                        </tr>';
                     } else if (($Age > 20 && $Age <= 30) && $row_emp["Sex"] == "M") {
                        $Total_Male_21_30++;
                        $Male_21_30 .= '
                        <tr>
                           <td class="text-center">'.$Total_Male_21_30.'</td>
                           <td>'.strtoupper($FullName).'</td>
                           <td class="text-center">'.$row_emp["AgencyId"].'</td>
                           <td class="text-center">'.date("m/d/Y",strtotime($row_emp["BirthDate"])).'</td>
                           <td class="text-center">'.intval($Age).' year(s)</td>
                        </tr>';
                     } else if (($Age > 20 && $Age <= 30) && $row_emp["Sex"] == "F") {
                        $Total_Female_21_30++;
                        $Female_21_30 .= '
                        <tr>
                           <td class="text-center">'.$Total_Female_21_30.'</td>
                           <td>'.strtoupper($FullName).'</td>
                           <td class="text-center">'.$row_emp["AgencyId"].'</td>
                           <td class="text-center">'.date("m/d/Y",strtotime($row_emp["BirthDate"])).'</td>
                           <td class="text-center">'.intval($Age).' year(s)</td>
                        </tr>';
                     } else if (($Age > 30 && $Age <= 40) && $row_emp["Sex"] == "M") {
                        $Total_Male_31_40++;
                        $Male_31_40 .= '
                        <tr>
                           <td class="text-center">'.$Total_Male_31_40.'</td>
                           <td>'.strtoupper($FullName).'</td>
                           <td class="text-center">'.$row_emp["AgencyId"].'</td>
                           <td class="text-center">'.date("m/d/Y",strtotime($row_emp["BirthDate"])).'</td>
                           <td class="text-center">'.intval($Age).' year(s)</td>
                        </tr>';
                     } else if (($Age > 30 && $Age <= 40) && $row_emp["Sex"] == "F") {
                        $Total_Female_31_40++;
                        $Female_31_40 .= '
                        <tr>
                           <td class="text-center">'.$Total_Female_31_40.'</td>
                           <td>'.strtoupper($FullName).'</td>
                           <td class="text-center">'.$row_emp["AgencyId"].'</td>
                           <td class="text-center">'.date("m/d/Y",strtotime($row_emp["BirthDate"])).'</td>
                           <td class="text-center">'.intval($Age).' year(s)</td>
                        </tr>';
                     } else if (($Age > 40 && $Age <= 50) && $row_emp["Sex"] == "M") {
                        $Total_Male_41_50++;
                        $Male_41_50 .= '
                        <tr>
                           <td class="text-center">'.$Total_Male_41_50.'</td>
                           <td>'.strtoupper($FullName).'</td>
                           <td class="text-center">'.$row_emp["AgencyId"].'</td>
                           <td class="text-center">'.date("m/d/Y",strtotime($row_emp["BirthDate"])).'</td>
                           <td class="text-center">'.intval($Age).' year(s)</td>
                        </tr>';
                     } else if (($Age > 40 && $Age <= 50) && $row_emp["Sex"] == "F") {
                        $Total_Female_41_50++;
                        $Female_41_50 .= '
                        <tr>
                           <td class="text-center">'.$Total_Female_41_50.'</td>
                           <td>'.strtoupper($FullName).'</td>
                           <td class="text-center">'.$row_emp["AgencyId"].'</td>
                           <td class="text-center">'.date("m/d/Y",strtotime($row_emp["BirthDate"])).'</td>
                           <td class="text-center">'.intval($Age).' year(s)</td>
                        </tr>';
                     } else if (($Age > 50 && $Age <= 60) && $row_emp["Sex"] == "M") {
                        $Total_Male_51_60++;
                        $Male_51_60 .= '
                        <tr>
                           <td class="text-center">'.$Total_Male_51_60.'</td>
                           <td>'.strtoupper($FullName).'</td>
                           <td class="text-center">'.$row_emp["AgencyId"].'</td>
                           <td class="text-center">'.date("m/d/Y",strtotime($row_emp["BirthDate"])).'</td>
                           <td class="text-center">'.intval($Age).' year(s)</td>
                        </tr>';
                     } else if (($Age > 50 && $Age <= 60) && $row_emp["Sex"] == "F") {
                        $Total_Female_51_60++;
                        $Female_51_60 .= '
                        <tr>
                           <td class="text-center">'.$Total_Female_51_60.'</td>
                           <td>'.strtoupper($FullName).'</td>
                           <td class="text-center">'.$row_emp["AgencyId"].'</td>
                           <td class="text-center">'.date("m/d/Y",strtotime($row_emp["BirthDate"])).'</td>
                           <td class="text-center">'.intval($Age).' year(s)</td>
                        </tr>';
                     } else if ($Age > 60 && $row_emp["Sex"] == "M") {
                        $Total_Male_60_Above++;
                        $Male_60_Above .= '
                        <tr>
                           <td class="text-center">'.$Total_Male_60_Above.'</td>
                           <td>'.strtoupper($FullName).'</td>
                           <td class="text-center">'.$row_emp["AgencyId"].'</td>
                           <td class="text-center">'.date("m/d/Y",strtotime($row_emp["BirthDate"])).'</td>
                           <td class="text-center">'.intval($Age).' year(s)</td>
                        </tr>';
                     } else if ($Age > 60 && $row_emp["Sex"] == "F") {
                        $Total_Female_60_Above++;
                        $Female_60_Above .= '
                        <tr>
                           <td class="text-center">'.$Total_Female_60_Above.'</td>
                           <td>'.strtoupper($FullName).'</td>
                           <td class="text-center">'.$row_emp["AgencyId"].'</td>
                           <td class="text-center">'.date("m/d/Y",strtotime($row_emp["BirthDate"])).'</td>
                           <td class="text-center">'.intval($Age).' year(s)</td>
                        </tr>';
                     } else if ($Age == "") {
                        $Total_No_Birthday++;
                        $No_Birthday .= '
                        <tr>
                           <td class="text-center">'.$Total_No_Birthday.'</td>
                           <td>'.strtoupper($FullName).'</td>
                           <td class="text-center">'.$row_emp["AgencyId"].'</td>
                           <td></td><td></td>
                        </tr>';
                     }
                  }

               ?>
               <tr>
                  <td colspan="5">Male Below 20</td>
               </tr>
               <?php 
                  if ($Total_Male_20_Below > 0) { 
                     echo $Male_20_Below;
                  } else {
                     echo '
                        <tr>
                           <td colspan="5" class="text-center">No Male Employee With Age 20 and Below</td>
                        </tr>
                     ';
                  }
               ?>
               <tr>
                  <td colspan="5" class="text-center"> ------------------ </td>
               </tr>



               <tr>
                  <td colspan="5">Male 21 to 30</td>
               </tr>
               <?php 
                  if ($Total_Male_21_30 > 0) { 
                     echo $Male_21_30;
                  } else {
                     echo '
                        <tr>
                           <td colspan="5" class="text-center">No Male Employee With Age Between 21 to 30</td>
                        </tr>
                     ';
                  }
               ?>
               <tr>
                  <td colspan="5" class="text-center"> ------------------ </td>
               </tr>


               <tr>
                  <td colspan="5">Male 31 to 40</td>
               </tr>
               <?php 
                  if ($Total_Male_31_40 > 0) { 
                     echo $Male_31_40;
                  } else {
                     echo '
                        <tr>
                           <td colspan="5" class="text-center">No Male Employee With Age Between 31 to 40</td>
                        </tr>
                     ';
                  }
               ?>
               <tr>
                  <td colspan="5" class="text-center"> ------------------ </td>
               </tr>

               <tr>
                  <td colspan="5">Male 41 to 50</td>
               </tr>
               <?php 
                  if ($Total_Male_41_50 > 0) { 
                     echo $Male_41_50;
                  } else {
                     echo '
                        <tr>
                           <td colspan="5" class="text-center">No Male Employee With Age Between 41 to 50</td>
                        </tr>
                     ';
                  }
               ?>
               <tr>
                  <td colspan="5" class="text-center"> ------------------ </td>
               </tr>


               <tr>
                  <td colspan="5">Male 51 to 60</td>
               </tr>
               <?php 
                  if ($Total_Male_51_60 > 0) { 
                     echo $Male_51_60;
                  } else {
                     echo '
                        <tr>
                           <td colspan="5" class="text-center">No Male Employee With Age Between 51 to 60</td>
                        </tr>
                     ';
                  }
               ?>
               <tr>
                  <td colspan="5" class="text-center"> ------------------ </td>
               </tr>

               <tr>
                  <td colspan="5">Male 60 Above</td>
               </tr>
               <?php 
                  if ($Total_Male_60_Above > 0) { 
                     echo $Male_60_Above;
                  } else {
                     echo '
                        <tr>
                           <td colspan="5" class="text-center">No Male Employee With Age 60 and Above</td>
                        </tr>
                     ';
                  }
               ?>
               <tr>
                  <td colspan="5" class="text-center"> ------------------ </td>
               </tr>



               <tr>
                  <td colspan="5">Female Below 20</td>
               </tr>
               <?php 
                  if ($Total_Female_20_Below > 0) { 
                     echo $Female_20_Below;
                  } else {
                     echo '
                        <tr>
                           <td colspan="5" class="text-center">No Female Employee With Age 20 and Below</td>
                        </tr>
                     ';
                  }
               ?>
               <tr>
                  <td colspan="5" class="text-center"> ------------------ </td>
               </tr>



               <tr>
                  <td colspan="5">Female 21 to 30</td>
               </tr>
               <?php 
                  if ($Total_Female_21_30 > 0) { 
                     echo $Female_21_30;
                  } else {
                     echo '
                        <tr>
                           <td colspan="5" class="text-center">No Female Employee With Age Between 21 to 30</td>
                        </tr>
                     ';
                  }
               ?>
               <tr>
                  <td colspan="5" class="text-center"> ------------------ </td>
               </tr>


               <tr>
                  <td colspan="5">Female 31 to 40</td>
               </tr>
               <?php 
                  if ($Total_Female_31_40 > 0) { 
                     echo $Female_31_40;
                  } else {
                     echo '
                        <tr>
                           <td colspan="5" class="text-center">No Female Employee With Age Between 31 to 40</td>
                        </tr>
                     ';
                  }
               ?>
               <tr>
                  <td colspan="5" class="text-center"> ------------------ </td>
               </tr>

               <tr>
                  <td colspan="5">Female 41 to 50</td>
               </tr>
               <?php 
                  if ($Total_Female_41_50 > 0) { 
                     echo $Female_41_50;
                  } else {
                     echo '
                        <tr>
                           <td colspan="5" class="text-center">No Female Employee With Age Between 41 to 50</td>
                        </tr>
                     ';
                  }
               ?>
               <tr>
                  <td colspan="5" class="text-center"> ------------------ </td>
               </tr>


               <tr>
                  <td colspan="5">Female 51 to 60</td>
               </tr>
               <?php 
                  if ($Total_Female_51_60 > 0) { 
                     echo $Female_51_60;
                  } else {
                     echo '
                        <tr>
                           <td colspan="5" class="text-center">No Female Employee With Age Between 51 to 60</td>
                        </tr>
                     ';
                  }
               ?>
               <tr>
                  <td colspan="5" class="text-center"> ------------------ </td>
               </tr>

               <tr>
                  <td colspan="5">Female 60 Above</td>
               </tr>
               <?php 
                  if ($Total_Female_60_Above > 0) { 
                     echo $Female_60_Above;
                  } else {
                     echo '
                        <tr>
                           <td colspan="5" class="text-center">No Female Employee With Age 60 and Above</td>
                        </tr>
                     ';
                  }
               ?>
               <tr>
                  <td colspan="5" class="text-center"> ------------------ </td>
               </tr>


               <?php
                  if ($Total_No_Birthday > 0) {
               ?>
               <tr>
                  <td colspan="5">Employees With No Birthdays</td>
               </tr>
               <?php 
                  echo $No_Birthday;
               ?>
               <tr>
                  <td colspan="5" class="text-center"> ------------------ </td>
               </tr>
               <?php } ?>
            </tbody>
         </table>
         <?php
            } else {
         ?>
         <?php
            rptHeader(getvalue("RptName"));
         ?>
         <table border="1" style="width: 100%;">
            <thead>
               <tr>
                  <th>Sequence</th>
                  <th>Employee Name</th>
                  <th>Employee ID</th>
                  <th>Birthdate</th>
                  <th>Age</th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td colspan="5"> No Record Found.</td>
               </tr>
            </tbody>
         </table>
         <?php
            }
         ?>
      </div>
   </body>
</html>