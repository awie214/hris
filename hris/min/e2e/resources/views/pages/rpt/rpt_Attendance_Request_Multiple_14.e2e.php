<?php
   include_once 'pageHEAD.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $emprefid 	= getvalue("hEmpRefId");
   $from  		= getvalue("date_from");
   $to 			= getvalue("date_to");
   $employees = FindFirst("employees","WHERE RefId = '$emprefid'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
	if ($employees) {
		$FirstName 	= $employees["FirstName"];
		$LastName 	= $employees["LastName"];
		$MiddleName = $employees["MiddleName"];
		$ExtName 	= $employees["ExtName"];
		$FullName = $LastName.", ".$FirstName." $ExtName ".$MiddleName;
	} else {
		$FullName = "&nbsp;";
	}
	$empinformation = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
	if ($empinformation) {
		$OfficeRefId = getRecord("office",$empinformation["OfficeRefId"],"Name");
		$DivisionRefId = getRecord("division",$empinformation["DivisionRefId"],"Name");
		$PositionRefId = getRecord("position",$empinformation["PositionRefId"],"Name");
	} else {
		$OfficeRefId = $DivisionRefId = $PositionRefId = "&nbsp;";
	}
	$where = "WHERE EmployeesRefId = '$emprefid' AND AppliedDateFor >= '$from' AND AppliedDateFor <= '$to'";

?>
<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		td {
			border: 2px solid black;
			vertical-align: top;
			padding: 5px;
			font-size: 9pt;
		}
		.data {
			font-size: 10pt;
			text-transform: uppercase;
			font-weight: 600;
		}
	</style>
</head>
<body>
	<div class="container-fluid rptBody">
		<div style="page-break-after: always;">
	        <?php
	            rptHeader("Request for Time Capture Registration/Correction");
	        ?>
	        <div class="row">
	         	<div class="col-xs-12">
	         		<table width="100%">
	         			<tr style="display: none;">
	         				<td style="width: 11.11%"></td>
	         				<td style="width: 11.11%"></td>
	         				<td style="width: 11.11%"></td>
	         				<td style="width: 11.11%"></td>
	         				<td style="width: 11.11%"></td>
	         				<td style="width: 11.11%"></td>
	         				<td style="width: 11.11%"></td>
	         				<td style="width: 11.11%"></td>
	         				<td style="width: 11.11%"></td>
	         			</tr>
	         			<tr>
	         				<td colspan="3">
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Printed Name and Signature of <br>
	         							Official/Employee:
	         							<br>
	         							<span class="data">
		         							<?php
		         								echo $FullName;
		         							?>
	         							</span>
	         						</div>
	         					</div>
	         				</td>
	         				<td colspan="4">
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Position Title:
	         							<br>
	         							<span class="data">
		         							<?php
		         								echo $PositionRefId;
		         							?>
	         							</span>
	         						</div>
	         					</div>
	         				</td>
	         				<td colspan="2">
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Office/Division:
	         							<br>
	         							<span class="data">
		         							<?php
		         								echo $OfficeRefId."<br>".$DivisionRefId;
		         							?>
	         							</span>
	         						</div>
	         					</div>
	         				</td>
	         			</tr>
	         			<tr align="center">
	         				<td>Date</td>
	         				<td>TIME IN (AM)</td>
	         				<td>TIME OUT (AM)</td>
	         				<td>TIME IN (PM)</td>
	         				<td>TIME OUT (PM)</td>
	         				<td>TIME IN (OT)</td>
	         				<td>TIME OUT (OT)</td>
	         				<td>Security Officer on Duty</td>
	         				<td>Approved by Concerned DM/DA</td>
	         			</tr>
	         			<?php
	         				$rs = SelectEach("attendance_request",$where);
	         				if ($rs) {
	         					while ($row = mysqli_fetch_assoc($rs)) {
	         						$AppliedDateFor = $row["AppliedDateFor"];
	         						echo '
	         							<tr>
					         				<td>
					         					<span class="data">
					     							'.$AppliedDateFor.'			
					 							</span>
					         				</td>
					         				<td>&nbsp;</td>
					         				<td>&nbsp;</td>
					         				<td>&nbsp;</td>
					         				<td>&nbsp;</td>
					         				<td>&nbsp;</td>
					         				<td>&nbsp;</td>
					         				<td>&nbsp;</td>
					         				<td>&nbsp;</td>
					         			</tr>
	         						';
	         					}
	         				} else {
	         					echo '
	         						<tr><td colspan=9>No Record Found</td></tr>
	         					';
	         				}
	         			?>
	         			<tr>
	         				<td colspan="9">
	         					Recorded by HR:
	         					<?php spacer(30); ?>
	         				</td>
	         			</tr>
	         		</table>
	         	</div>
	        </div>
	    </div>
    </div>
</body>
</html>