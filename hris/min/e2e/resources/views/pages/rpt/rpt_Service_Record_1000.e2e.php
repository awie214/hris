<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }

?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         @media print {
            body {
               margin-top: 0;
               font-size: 9pt;
            }
            thead {
               font-size: 8pt;  
               font-weight: 400;
            }
            tbody {
               font-size: 8pt !important;
            }
            .header_table {
               font-size: 8pt;    
            }
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $count = 0;
            if ($rsEmployees) {
               while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                  $page_count    = 0;
                  $empid         = $row_emp["RefId"];
                  $MiddleInitial = substr($row_emp["MiddleName"], 0,1);
                  $FullName      = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$MiddleInitial;
                  if ($row_emp["Sex"] == "F") {
                     if ($row_emp["CivilStatus"] == "Ma") {
                        $MaidenName = $row_emp["MiddleName"];
                     } else {
                        $MaidenName = "";
                     }
                  } else {
                     $MaidenName = "";
                  }
                  $emprefid      = $row_emp["RefId"];
                  $where         = "WHERE EmployeesRefId = $emprefid ORDER BY EffectivityDate";
                  $emp_movement  = SelectEach("employeesmovement",$where);
                  $record_count  = mysqli_num_rows($emp_movement);
                  if ($record_count <= 11) {
                     $page_count = 1;
                  } else {
                     for ($i=1; $i <= $record_count; $i++) { 
                        $trigger = $i % 11;
                        if ($trigger == 0) $page_count++;
                     }
                     $check = $page_count * 11;
                     if ($record_count > $check) $page_count++;
                  }
                  include 'inc/inc_service_record_1000.php';
         ?>
         
         <?php
               }
            } else {
               echo "No Result For Criteria $searchCriteria";
            }
         ?>
      </div>
   </body>
</html>