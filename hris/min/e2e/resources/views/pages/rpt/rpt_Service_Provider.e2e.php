<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   function countRating($column,$refid) {
      require "conn.e2e.php";
      $sql = "SELECT `$column` FROM training_evaluation WHERE LDMSLNDProgramRefId = '$refid'";
      $rs  = mysqli_query($conn,$sql);
      if ($rs) {
         $num_row = mysqli_num_rows($rs);
         if ($num_row > 0) {
            $total = 0;
            while ($row = mysqli_fetch_assoc($rs)) {
               $total += $row[$column];
            }
            $count = $total / $num_row;
         } else {
            $count = 0;   
         }
      } else {
         $count = 0;
      }
      return $count;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style type="text/css">
         .gray {background: gray;}
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12 text-center">
                     <b>LEARNING SERVICE PROVIDER (LSP) MANAGEMENT SYSTEM</b>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table width="100%">
                        <thead>
                           <tr class="colHEADER">
                              <th>
                                 NAME OF LEARNING SERVICE PROVIDER
                              </th>
                              <th>
                                 COMPANY/OFFICE
                              </th>
                              <th>
                                 SPECIALIZATION/TRAINING PROGRAMS
                              </th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php
                              $rs = SelectEach("provider","");
                              if ($rs) {
                                 while ($row = mysqli_fetch_assoc($rs)) {
                                    echo '<tr valign="top">';
                                    echo '<td>'.$row["Facilitator"].'</td>';
                                    echo '<td>'.$row["Name"].'</td>';
                                    echo '<td>'.$row["Expertise"].'</td>';
                                    echo '</tr>';
                                 }
                              }
                           ?>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>
