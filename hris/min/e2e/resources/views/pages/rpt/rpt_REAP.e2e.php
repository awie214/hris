<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $refid               = getvalue("refid");
   $table               = "action_plan";
   $row                 = FindFirst($table,"WHERE RefId = '$refid'","*");
   $emprefid            = $row["EmployeesRefId"];
   $emp_row = FindFirst("employees","WHERE RefId = '$emprefid'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
   if ($emp_row) {
      $LastName = $emp_row["LastName"];
      $FirstName = $emp_row["FirstName"];
      $MiddleName = $emp_row["MiddleName"];
      $ExtName = $emp_row["ExtName"];
      $FullName = $LastName.", ".$FirstName." $ExtName ".$MiddleName;
   } else {
      $FullName = "";
   }
   $empinfo_row = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
   if ($empinfo_row) {
      $Office     = getRecord("office",$empinfo_row["OfficeRefId"],"Name");
      $Position   = getRecord("position",$empinfo_row["PositionRefId"],"Name");
      $Division   = getRecord("Division",$empinfo_row["DivisionRefId"],"Name");
   } else {
      $Office     = "";
      $Position   = "";
      $Division   = "";
   }
   $Signatory1 = getRecord("signatories",$row["Signatory1"],"Name");
   $Signatory2 = getRecord("signatories",$row["Signatory2"],"Name");
   $Signatory1_pos = getRecord("signatories",$row["Signatory1"],"PositionRefId");
   $Signatory2_pos = getRecord("signatories",$row["Signatory2"],"PositionRefId");
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style type="text/css">
         .gray {background: gray;}
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12" style="border: 1px solid black; padding: 10px;">
                     <br>
                     <div class="row">
                        <div class="col-xs-6">
                           Learner: <b><?php echo $FullName; ?></b>
                        </div>
                        <div class="col-xs-6">
                           Office: <b><?php echo $Office; ?></b>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-6">
                           Title of Intervention: <b><?php echo getRecord("ldmslndprogram",$row["LDMSLNDProgramRefId"],"Name"); ?></b>
                        </div>
                        <div class="col-xs-6">
                           Date Conducted: <b><?php echo getRecord("ldmslndprogram",$row["LDMSLNDProgramRefId"],"StartDate"); ?></b>
                        </div>
                     </div>
                  </div>
               </div>
               <br><br>
               <div class="row">
                  <div class="col-xs-12">
                     <table style="width: 100%;" border="1">
                        <tr>
                           <td style="width: 20%;"></td>
                           <td style="width: 20%;"></td>
                           <td style="width: 20%;"></td>
                           <td style="width: 20%;"></td>
                           <td style="width: 20%;"></td>
                        </tr>
                        <tr>
                           <td>REAP Title</td>
                           <td colspan="4"><b><?php echo $row["Title"]; ?></b></td>
                        </tr>
                        <tr>
                           <td>Objectives</td>
                           <td colspan="4"><b><?php echo $row["Objectives"]; ?></b></td>
                        </tr>
                        <tr>
                           <td>Duration</td>
                           <td colspan="4"><b><?php echo $row["Duration"]; ?></b></td>
                        </tr>
                        <tr>
                           <td>Expected Outputs</td>
                           <td colspan="4"><b><?php echo $row["Output"]; ?></b></td>
                        </tr>
                        <tr>
                           <td>Success Indicators</td>
                           <td colspan="4"><b><?php echo $row["Indicator"]; ?></b></td>
                        </tr>
                        <tr class="colHEADER">
                           <th colspan="2">Specific Actions</th>
                           <th>Resources</th>
                           <th>Target Date</th>
                           <th>Status/Remarks</th>
                        </tr>
                        <?php
                           for ($i=1; $i <= 10; $i++) { 
                              echo '
                                 <tr>
                                    <td colspan="2">'.$row["Action".$i].'</td>
                                    <td>'.$row["Resources".$i].'</td>
                                    <td class="text-center">'.$row["TargetDate".$i].'</td>
                                    <td>'.$row["Remarks".$i].'&nbsp;</td>
                                 </tr>
                              ';
                           }
                        ?>
                        <tr>
                           <td colspan="5">
                              Budgetary Requirements:
                              <br>
                              &nbsp;
                           </td>
                        </tr>
                        <tr>
                           <td colspan="5">
                              <div class="row">
                                 <br>
                                 <div class="col-xs-1">
                                    Signature:
                                 </div>
                                 <div class="col-xs-3 text-center">
                                    <u><b><?php echo $Signatory1; ?></b></u>
                                    <br>
                                    Head of Office
                                 </div>
                                 <div class="col-xs-3 text-center">
                                    <u><b><?php echo $Signatory2; ?></b></u>
                                    <br>
                                    Supervisor
                                 </div>
                                 <div class="col-xs-3 text-center">
                                    <u><b><?php echo $FullName; ?></b></u>
                                    <br>
                                    Learner
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-1">
                                    Date:
                                 </div>
                                 <div class="col-xs-3 text-center">
                                    _______________________
                                 </div>
                                 <div class="col-xs-3 text-center">
                                    _______________________
                                 </div>
                                 <div class="col-xs-3 text-center">
                                    _______________________
                                 </div>
                              </div>
                           </td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>
