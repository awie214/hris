<?php
	include_once 'constant.e2e.php';
	require_once pathClass.'0620functions.e2e.php';
	$emprefid 	= getvalue("refid");
	$emp_row 	= FindFirst("employees","WHERE RefId = '$emprefid'","*");
	$LastName 	= $emp_row["LastName"];
	$FirstName 	= $emp_row["FirstName"];
	$MiddleName = $emp_row["MiddleName"];
	$ExtName 	= $emp_row["ExtName"];
	$FullName 	= $FirstName." ".$MiddleName." ".$LastName." ".$ExtName; 
	$FullName 	= strtoupper($FullName);

	$empinfo_row = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
	if ($empinfo_row) {
		$Division 		= strtoupper(getRecord("division",$empinfo_row["DivisionRefId"],"Name"));
		$Position 		= strtoupper(getRecord("Position",$empinfo_row["PositionRefId"],"Name"));
		$SalaryGrade 	= strtoupper(getRecord("SalaryGrade",$empinfo_row["SalaryGradeRefId"],"Name"));
		$SalaryAmount 	= number_format($empinfo_row["SalaryAmount"],2);
		$Office 		= strtoupper(getRecord("Office",$empinfo_row["OfficeRefId"],"Name"));
	} else {
		$Division = $Position = $SalaryGrade = $Office = "";
		$SalaryAmount = "0.00";
	}
?>
<!DOCTYPE html>
<html>
<head>
	<?php
		include_once 'pageHEAD.e2e.php';
	?>
	<title></title>
	<style type="text/css">
		thead {
			text-transform: uppercase;
		}
		th {
            text-align: center;
            vertical-align: top;
            background: #d9d9d9;
        }
		td {
			padding: 2px;
			vertical-align: top;
		}
	</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-12 text-center" style="border-bottom: 2px solid black; padding: 10px;">
						<b>INDIVIDUAL DEVELOPMENT PLAN (IDP)</b>
					</div>
				</div>
				<br>
				<div class="row margin-top">
					<div class="col-xs-12">
						<table border="1" width="100%">
							<tr>
								<td style="width: 25%;">
									1. Name (Last, First, MI)
								</td>
								<td style="width: 25%;">
									<?php echo $FullName; ?>
								</td>
								<td style="width: 25%;">
									6. Two-Year Period
								</td>
								<td style="width: 25%;">
									
								</td>
							</tr>
							<tr>
								<td>
									2. Current Position
								</td>
								<td>
									<?php echo $Position; ?>
								</td>
								<td>
									7. Division
								</td>
								<td>
									
								</td>
							</tr>
							<tr>
								<td>
									3. Salary Grade
								</td>
								<td>
									<?php echo $SalaryGrade; ?>
								</td>
								<td>
									8. Office
								</td>
								<td>
									<?php echo $Office; ?>
								</td>
							</tr>
							<tr>
								<td>
									4. Years in Position
								</td>
								<td>
									
								</td>
								<td colspan="2">
									9. No further development is desired or required for this year/s
									<br>
									<i>(Please check the box here.)</i>
									<br>
									<input type="checkbox" name="year_one">&nbsp;<b>Year 1</b>
									<input type="checkbox" name="year_two">&nbsp;<b>Year 2</b>
									<input type="checkbox" name="both_year">&nbsp;<b>Both Years</b>
								</td>
							</tr>
							<tr>
								<td>
									5. Years in CSC
								</td>
								<td>
									
								</td>
								<td colspan="2">
									10. Supervisor's Name (Last, First, MI)
								</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="row margin-top">
					<div class="col-xs-12">
						<b>PURPOSE:</b>
						<br>
						[ ] To meet the comptencies of current position
						<br>
						[ ] To increase the level of competencies of current position
						<br>
						[ ] To meet the competencies of the next higher position
						<br>
						[ ] To acquire new competencies accross different functions/position
						<br>
						[ ] Others, please specify. ________________________________________
					</div>
				</div>
				<div class="row margin-top">
					<div class="col-xs-12">
						<table width="100%" border="1">
                          	<thead>
                             	<tr>
	                                <th>
	                                   Target Competency<br>(1)
	                                </th>
	                                <th>
	                                   Priority<br>IDP<br>(2)
	                                </th>
	                                <th>Code</th>
	                                <th>
	                                   Specific Behavioral<br>
	                                   Indicators NOT<br>
	                                   Consistently<br>
	                                   Demonstrated
	                                   (3)
	                                </th>
	                                <th>
	                                   Development<br>
	                                   Activity<br>
	                                   (4)
	                                </th>
	                                <th>
	                                   Support<br>
	                                   Needed(5)
	                                </th>
	                                <th>
	                                   Trainer/<br>
	                                   Provider<br>
	                                   (6)
	                                </th>
	                                <th>
	                                   Schedule or<br>
	                                   Completion<br>
	                                   Date<br>
	                                   (7)
	                                </th>
                             	</tr>
                          	</thead>
                          	<tbody>
                             	<?php
                                	for ($i=1; $i <= 5; $i++) { 
                                   		echo '<tr>';
                                   		echo '<td>&nbsp;</td>';
                                   		echo '<td>&nbsp;</td>';
                                   		echo '<td>&nbsp;</td>';
                                   		echo '<td>&nbsp;</td>';
                                   		echo '<td>&nbsp;</td>';
                                   		echo '<td>&nbsp;</td>';
                                   		echo '<td>&nbsp;</td>';
                                   		echo '<td>&nbsp;</td>';
                                   		echo '</tr>';
                                	}
                             	?>
                          	</tbody>
                       </table>
					</div>
				</div>
				<Br>
				<div class="row margin-top">
					<div class="col-xs-12">
						<table width="100%" border="1">
							<tr>
								<td>
									11. Employee Signature
								</td>
								<td>
									Date
								</td>
								<td>
									12. Supervisor's Signature
								</td>
								<td>
									Date
								</td>
								<td>
									13. Head/Assistant Head of Office's Signature
								</td>
								<td>
									Date
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>
									14A. Updated (Initials)
								</td>
								<td>
									Date
								</td>
								<td>
									14B. Updated (Initials)
								</td>
								<td>
									Date
								</td>
								<td>
									14C. Updated (Initials)
								</td>
								<td>
									Date
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>
									15. Check applicable copy designation as shown:
								</td>
								<td colspan="5">
									<input type="checkbox" name="">&nbsp;<b>Employee's Copy</b>
									<input type="checkbox" name="">&nbsp;<b>Supervisor's Copy</b>
									<input type="checkbox" name="">&nbsp;<b>OHRMD/HRD's Copy</b>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>