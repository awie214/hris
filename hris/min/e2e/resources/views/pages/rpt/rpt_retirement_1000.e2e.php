<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
   
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         @media print {
            body {
               font-size: 9pt;
            }
            thead {
               font-size: 9pt;  
            }
            tbody {
               font-size: 8pt !important;
            }
            .border-bottom {
               border-bottom: 1px solid black;
            }
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="col-xs-12">
            <div class="row margin-top">
               <div class="col-xs-12 text-center">
                  <b style="font-size: 12pt;">PHILIPPINE INSTITUTE FOR DEVELOPMENT STUDIES</b>
                  <br>
                  18F Three Cyberpod Centris - North Tower 
                  <br>
                  EDSA corner Quezon Avenue, Quezon City
               </div>
            </div>
            <br>
            <div class="row margin-top">
               <div class="col-xs-12 text-center">
                  <h4>LIST OF EMPLOYEES AT RETIREMENT AGE</h4>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  <table width="100%">
                     <thead>
                        <tr class="colHEADER">
                           <th>#</th>
                           <th>Employee ID</th>
                           <th>Employee Name</th>
                           <th>Age</th>
                           <th>Turning At</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           $count = 0;
                           if ($rsEmployees) {
                              while ($row = mysqli_fetch_assoc($rsEmployees)) {
                                 $AgencyId   = $row["AgencyId"];
                                 $MiddleName = $row["MiddleName"];
                                 $FirstName  = $row["FirstName"];
                                 $LastName   = $row["LastName"];
                                 $ExtName    = $row["ExtName"];
                                 $BirthDate  = $row["BirthDate"];
                                 $FullName   = $LastName.", ".$FirstName." ".$ExtName." ".$MiddleName;
                                 if ($BirthDate != "0000-00-00" || $BirthDate != "") {
                                    $Age           = computeAge($BirthDate);
                                    $YearEnd       = date("Y",time())."-12-31";
                                    $Turning_Age   = computeAge($BirthDate,$YearEnd);
                                    $Turning_Date  = date("F d",strtotime($BirthDate));
                                    if ($Age != $Turning_Age) {
                                       $Turning       = 'Turning '.$Turning_Age.' At '.$Turning_Date;   
                                    } else {
                                       $Turning       = "";
                                    }
                                    
                                    if ($Turning_Age >= 60) {
                                       $count++;
                                       echo '
                                       <tr>
                                          <td class="text-center">'.$count.'</td>
                                          <td class="text-center">'.$AgencyId.'</td>
                                          <td>'.$FullName.'</td>
                                          <td class="text-center">
                                             '.$Age.' Years Old
                                          </td>
                                          <td class="text-center">'.$Turning.'</td>
                                       </tr>
                                       ';
                                    }
                                 }
                              }
                           } else {
                              echo '
                                 <tr>
                                    <td class="text-center" colspan="5">No Employee Searched...</td>
                                 </tr>
                              ';      
                           }
                        ?>
                        
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>