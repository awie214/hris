<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $count = 0;
            if ($rsEmployees) {
         ?>
         
         <table style="width: 100%;">
            <thead>
               <tr>
                  <td colspan="4">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                  </td>
               </tr>
               <tr class="colHEADER">
                  <th>Name</th>
                  <th>Division</th>
                  <th>Month</th>
                  <th># of Incurred Tardiness</th>
               </tr>
            </thead>
            <tbody>
               <?php
                  while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                     $count++;
                     $FullName   = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
                     echo '
                        <tr>
                           <td>'.$FullName.'</td>
                           <td>&nbsp;</td>
                           <td></td>
                           <td></td>
                        </tr>
                     ';
                  }
               ?>
               
            </tbody>
         </table>
         <br>
         <br>
         <div class="row margin-top">
            <div class="col-xs-6">
               Prepared by:
               <br>
               <b>Laila R. Porlucas</b>
               <br>
               Admin. Officer IV
            </div>
            <div class="col-xs-6">
               Noted by:
               <br>
               <b>Jelly N. Ortiz, DPA</b>
               <br>
               Supvg. Admin. Officer
            </div>
         </div>
         <?php
            }
         ?>
      </div>
   </body>
</html>