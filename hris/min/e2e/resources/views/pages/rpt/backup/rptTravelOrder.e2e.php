<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader(getRptName(getvalue("drpReportKind")));
         ?>
         <p class="txt-center">For the Month of : <u><?php echo monthName(date("m",time()),1).", ".date("Y",time()) ?></u></p>
         <p>Division : _____________________________</p>

         <table border="1">
            <tr>
               <th rowspan="2">No</th>
               <th rowspan="2">Employees Name</th>
               <th rowspan="2">Date Filed</th>
               <th colspan="2">Application Date</th>
               <th rowspan="2">Travel Order No.#</th>
               <th rowspan="2">Encoded By:</th>
               <th rowspan="2">Updated By:</th>
            </tr>
            <tr>
               <th>From</th>
               <th>To</th>

            </tr>
            <?php for($j=1;$j<=12;$j++) {?>
            <tr>
               <td class="txt-center"><?php echo $j; ?></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
            </tr>
            <?php } ?>
         </table>
         <p>
            <div class="row">
               <div class="col-xs-2 txt-right">Prepared By:</div>
               <div class="col-xs-4"></div>
               <div class="col-xs-2 txt-right">Approved By:</div>
               <div class="col-xs-4"></div>
            </div>
            <div class="row">
               <div class="col-xs-2"></div>
               <div class="col-xs-4">________________________</div>
               <div class="col-xs-2"></div>
               <div class="col-xs-3">________________________</div>
               <div class="col-xs-1"></div>
            </div>
         </p>

      </div>
      <?php rptFooter(); ?>
   </body>
</html>