<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader(getRptName(getvalue("drpReportKind")));
         ?>
         <p class="txt-center">For the Month of <u><?php echo monthName(date("m",time()),1).", ".date("Y",time()) ?></u> </p>

         <table border="1">
            <tr>
               <th class="padd5" rowspan=2 style="width:*%">Agency Position</th>
               <th class="padd5" rowspan=2 style="width:7.5%">CES Status</th>
               <th class="padd5" rowspan=2 style="width:7.5%">Nature of Movement</th>
               <th class="padd5" rowspan=2 style="width:7.5%">Office</th>
               <th class="padd5" colspan="3" style="width:22.5%">FORMER</th>
               <th class="padd5" colspan="4" style="width:30%">PRESENT</th>
               <th class="padd5" rowspan=2 style="width:7.5%">Date of Effectivity</th>
               <th class="padd5" rowspan=2 style="width:7.5%">Remarks</th>
            </tr>
            <tr>
               <th class="padd5">Position</th>
               <th class="padd5">Office</th>
               <th class="padd5">Place of Assign</th>
               <th class="padd5">Position</th>
               <th class="padd5">Status of Appointment</th>
               <th class="padd5">Office</th>
               <th class="padd5">Place of Assign</th>
            </tr>
            <?php for ($j=1;$j<=15;$j++) {?>
               <tr>
                  <td>&nbsp;</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
               <tr>
            <?php } ?>
         </table>
         <br><br>
         <p>
            <div class="row">
               <div class="col-xs-2 txt-right">Prepared By:</div>
               <div class="col-xs-4"></div>
               <div class="col-xs-2 txt-right">Certified correct / Approved By:</div>
               <div class="col-xs-4"></div>
            </div>
            <div class="row">
               <div class="col-xs-2"></div>
               <div class="col-xs-4">________________________</div>
               <div class="col-xs-2"></div>
               <div class="col-xs-3">________________________</div>
               <div class="col-xs-1"></div>
            </div>
         </p>

      </div>
      <?php rptFooter(); ?>
   </body>
</html>