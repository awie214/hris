<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader(getRptName(getvalue("drpReportKind")));
         ?>
         <p class="txt-center">For the Month of <u><?php echo monthName(date("m",time()),1) ?></u> 2017</p>

         <table border="1">
            <tr style="text-align:center;">
               <td><label>No.</label></td>
               <td><label>Phic. No.</label></td>
               <td><label>Name of Employee</label></td>
               <td><label>Employee Share</label></td>
               <td><label>Employer Share</label></td>
               <td><label>Total</label></td>
            </tr>
            <?php for ($j=1;$j<=15;$j++) {?>
               <tr style="text-align:center;">
                  <td><?php echo $j ?></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
               </tr>
            <?php }?>
            <tr style="text-align:center;">
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td><label>TOTAL</label></td>
               <td>P 1 187.50</td>
               <td>P 1 187.50</td>
               <td>P 2 375.50</td>
            </tr>
         </table>
         <p>
            <div class="row">
               <div class="col-xs-2 txt-right">Prepared By:</div>
               <div class="col-xs-4"></div>
               <div class="col-xs-2 txt-right">Approved By:</div>
               <div class="col-xs-4"></div>
            </div>
            <div class="row">
               <div class="col-xs-2"></div>
               <div class="col-xs-4">________________________</div>
               <div class="col-xs-2"></div>
               <div class="col-xs-3">________________________</div>
               <div class="col-xs-1"></div>
            </div>
         </p>

      </div>
      <?php rptFooter(); ?>
   </body>
</html>