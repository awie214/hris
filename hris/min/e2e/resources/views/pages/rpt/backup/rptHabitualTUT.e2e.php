<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader(getRptName(getvalue("drpReportKind")));
         ?>
         <p class="txt-center">Covered Month <u><?php echo monthName(date("m",time()),1)." - ".monthName((date("m",time()) + 3),1) ?></u> 2017</p>
         <p>DIVISION:________________________________</p>
         <table border="1">
            <tr>
               <th>Emp. ID</th>
               <th>Name of Employee</th>
               <th>Months</th>
               <th>No. Of Late</th>
               <th>No. Of UT</th>
               <th>TOTAL</th>
            </tr>
            <?php for ($j=1;$j<=20;$j++) {?>
               <tr>
                  <td class="txt-center"><?php echo rand(1,100) ?></td>
                  <td>Dela Cruz, Juan</td>
                  <td class="txt-center">
                  <?php
                     $fromMonth = date("m",time());
                     $toMonth = date("m",time()) + 3;
                     echo monthName(rand($fromMonth,$toMonth),1);
                  ?>
                  </td>
                  <?php
                     $Lates = rand(1,6);
                     $UT    = rand(3,8);
                  ?>
                  <td class="txt-center"><?php echo $Lates ?></td>
                  <td class="txt-center"><?php echo $UT ?></td>
                  <td class="txt-center"><?php echo ($UT + $Lates) ?></td>
               </tr>
            <?php } ?>
         </table>
         <p>
            <div class="row">
               <div class="col-xs-2 txt-right">Prepared By:</div>
               <div class="col-xs-4"></div>
               <div class="col-xs-2 txt-right">Approved By:</div>
               <div class="col-xs-4"></div>
            </div>
            <div class="row">
               <div class="col-xs-2"></div>
               <div class="col-xs-4">________________________</div>
               <div class="col-xs-2"></div>
               <div class="col-xs-3">________________________</div>
               <div class="col-xs-1"></div>
            </div>
         </p>

      </div>
      <?php rptFooter(); ?>
   </body>
</html>