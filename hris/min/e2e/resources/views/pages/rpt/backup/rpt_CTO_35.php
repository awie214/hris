<?php
   include_once 'pageHEAD.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   $refid = getvalue("refid");
   $row = FindFirst("employeescto","WHERE RefId = '$refid'","*");
   if ($row) {
      $emprefid = $row["EmployeesRefId"];
      $Signatory1       = $row["Signatory1"];
      $Signatory2       = $row["Signatory2"];
      $Signatory3       = $row["Signatory3"];

      $s1_row           = FindFirst("signatories","WHERE RefId = '$Signatory1'","*");
      $s2_row           = FindFirst("signatories","WHERE RefId = '$Signatory2'","*");

      $s1_name          = $s1_row["Name"];
      $s1_position      = getRecord("position",$s1_row["PositionRefId"],"Name");

      $s2_name          = $s2_row["Name"];
      $s2_position      = getRecord("position",$s2_row["PositionRefId"],"Name");

      $employees = FindFirst("employees","WHERE RefId = '$emprefid'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
      if ($employees) {
         $FirstName  = $employees["FirstName"];
         $LastName   = $employees["LastName"];
         $MiddleName = $employees["MiddleName"];
         $ExtName    = $employees["ExtName"];
         $FullName = $LastName.", ".$FirstName." $ExtName ".$MiddleName;
      } else {
         $FullName = "&nbsp;";
      }
      $empinformation = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
      if ($empinformation) {
         $OfficeRefId = getRecord("office",$empinformation["OfficeRefId"],"Name");
         $DivisionRefId = getRecord("division",$empinformation["DivisionRefId"],"Name");
         $PositionRefId = getRecord("position",$empinformation["PositionRefId"],"Name");
      } else {
         $OfficeRefId = $DivisionRefId = $PositionRefId = "&nbsp;";
      }
      $FiledDate = date("F d, Y",strtotime($row["FiledDate"]));
      $Hours = $row["Hours"];
      $from = $row["ApplicationDateFrom"];
      $to = $row["ApplicationDateTo"];
      if ($from == $to) {
         $date = date("F d, Y",strtotime($from));
      } else {
         $diff = dateDifference($from,$to);
         $diff = $diff + 1;
         $Hours = $diff * $Hours;
         $date = date("F d, Y",strtotime($from))." to ".date("F d, Y",strtotime($to));;
      }
      $status = $row["Status"];
   }
   $user = getvalue("hUserRefId");
   $user_row = FindFirst("employees","WHERE RefId = '$user'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
   if ($user_row) {
      $user_LastName   = $user_row["LastName"];
      $user_FirstName  = $user_row["FirstName"];
   } else {
      $user_FirstName = $user_LastName = "";
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style>
         .border-left-bot {
            border-left: 1px solid black;
            border-bottom: 1px solid black;
         }
         .border-left-bot-right {
            border-left: 1px solid black;
            border-bottom: 1px solid black;
            border-right: 1px solid black;
         }
         .border-top {
            border-top: 1px solid black;
         }
         .border-left {
            border-left: 1px solid black;
         }
         .border-right {
            border-right: 1px solid black;
         }
         .gray {
            background: gray;
         }
      </style>
      <script type="text/javascript">
         $(document).ready(function () {
            <?php 
               if ($status == "Approved") {
                  echo '$("#approved").prop("checked",true);';
               } else if ($status == "Cancelled") {
                  echo '$("#disapproved").prop("checked",true);';
               } else {
                  echo '$("#disapproved").prop("checked",false);';
                  echo '$("#approved").prop("checked",false);';
               }
            ?>
         });
      </script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader("Request for Compensatory<br>Time-Off (CTO) Form","HRDMS-T-005");
         ?>
         <br><br>
         <div class="row margin-top">
            <div class="col-xs-6">
               Name: <b><u><?php echo $FullName; ?></u></b>
            </div>
            <div class="col-xs-6">
               Date of Application: <b><u><?php echo $FiledDate; ?></u></b>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-12">
               Position: <b><u><?php echo $PositionRefId; ?></u></b>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-12">
               Division: <b><u><?php echo $DivisionRefId; ?></u></b>
            </div>
         </div>
         <br>
         <br>
         <div class="row margin-top">
            <div class="col-xs-12">
               <b>COMPENSATORY TIME OFF (CTO) REQUEST</b>
               <br>
               No. of hours to be availed: <b><u><?php echo $Hours; ?></u></b>
               <br>
               Inclusive date/s of availment: <b><u><?php echo $date; ?></u></b>
               <br>
               <br>
               <br>
               ______________________________________________________
               <br>
               Signature of Applicant
            </div>
         </div>
         <br>
         <?php bar(); ?>
         <br>
         <div class="row margin-top">
            <div class="col-xs-12">
               <b>CERTIFICATION OF COMPENSATORY OVERTIME CREDIT (COC)</b>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-12">
               COC Credit: ________________ hours as of _______________________
            </div>
         </div>
         <br><br>
         <div class="row margin-top">
            <div class="col-xs-12">
               Certified by:
               <br><br>
               ___________________________
               <br>
               <b>
                  <?php
                     echo $s1_name;
                  ?>
               </b>
               <br>
               <?php 
                  echo $s1_position;
               ?>
            </div>
         </div>
         <br>
         <?php bar(); ?>
         <br>
         <div class="row margin-top">
            <div class="col-xs-12">
               Approved by:
               <br><br>
               ___________________________
               <br>
               <b>
                  <?php
                     echo $s2_name;
                  ?>
               </b>
               <br>
               <?php 
                  echo $s2_position;
               ?>
            </div>
         </div>
         <br>
         <br>
         <div class="row margin-top">
            <div class="col-xs-1"></div>
            <div class="col-xs-10" style="border: 1px solid black; padding: 5px;">
               The only CONTROLLED copy of this document is the online version maintained in the HRIS. The user must ensure that this or any other copy of a controlled document is current and complete prior to use. The MASTER copy of this document is with the AFD-HRMD Section. This document is UNCONTROLLED when downloaded and printed.
               <br>
               <br>
               Printed on <?php echo date("F d,Y",time()); ?>
               <br>
               Printed by <?php echo $user_LastName.", ".$user_FirstName; ?>
            </div>
         </div>
      </div>
   </body>
</html>