<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   //$whereClause .= " LIMIT 10";
   $table = "employees";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   $from = getvalue("txtAttendanceDateFrom");
   $to   = getvalue("txtAttendanceDateTo");
   $month = getvalue("txtAttendanceMonth");
   $year  = getvalue("txtAttendanceYear");
   $from    = $year."-".$month."-01";
   $to      = $year."-".$month."-".cal_days_in_month(CAL_GREGORIAN,$month,$year);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $errmsg = "";
            rptHeader(getRptName(getvalue("drpReportKind")));
            if ($rsEmployees && $errmsg == "")
            {
         ?>
         <p class="txt-center">
            <?php
               echo "For the Date ";
               echo date("m/d/Y",strtotime($from))." - ".date("m/d/Y",strtotime($to));
            ?>
         </p>
         <table border="1">
            <tr>
               <th rowspan=2>Name of Employee</th>
               <?php for ($j=1;$j<=31;$j++) {?>
                  <th rowspan=2 class="txt-center" style="width:25px;"><?php echo $j ?></th>
               <?php } ?>
               <th colspan=3>Total</th>
               <th colspan=8>Total Used Of</th>
            </tr>
            <tr>
               <th>L</th>
               <th>U</th>
               <th>A</th>
               <th>VL</th>
               <th>FL</th>
               <th>SL</th>
               <th>SPL</th>
               <th>ML</th>
               <th>PL</th>
               <th>MC8</th>
               <th>CTO</th>
            </tr>
            <?php
               $count = 0;
               while ($row_emp = mysqli_fetch_assoc($rsEmployees) ) {
                  $emprefid       = $row_emp["RefId"];
                  $biometricsID   = $row_emp["BiometricsID"];
                  $CompanyID      = $row_emp["CompanyRefId"];
                  $BranchID       = $row_emp["BranchRefId"];
                  $Default_qry    = "WHERE CompanyRefId = ".$CompanyID." AND BranchRefId = ".$BranchID;
                  $workschedrefid = FindFirst("empinformation","WHERE EmployeesRefId = ".$emprefid,"WorkscheduleRefId");
                  if (is_numeric($workschedrefid)) {
                     ${"L_".$emprefid}   = 0;
                     ${"U_".$emprefid}   = 0;
                     ${"A_".$emprefid}   = 0;
                     ${"VL_".$emprefid}   = 0;
                     ${"FL_".$emprefid}   = 0;
                     ${"SL_".$emprefid}   = 0;
                     ${"SPL_".$emprefid}  = 0;
                     ${"ML_".$emprefid}   = 0;
                     ${"PL_".$emprefid}   = 0;
                     ${"MC8_".$emprefid}  = 0;
                     ${"CTO_".$emprefid}  = 0;
                     for ($v=1;$v<=31;$v++) {
                        if ($v <= 9) $v = "0".$v; 
                        ${"d_".$emprefid."_".$v} = "&nbsp;";
                     } 
                     $count++;
                     $curr_date   = date("Y-m-d",time());
                     $month_start = $from;
                     $month_end   = $to;
            ?>
               <tr>
                  <td class="pad-left">
                     <?php 
                        echo $row_emp['LastName'].', '.$row_emp['FirstName'].', '.$row_emp['MiddleName'];
                     ?>
                  </td>
                  <?php
                     if ($p_filter_value == "0" || $p_filter_table == "") {
                        $emprefid = $row_emp["RefId"];
                     } else {
                        $emprefid   = $row_emp["EmployeesRefId"];
                     }
                     $dtr = FindFirst("dtr_process","WHERE EmployeesRefId = '$emprefid' AND Month = '$month' AND Year = '$year'","*");
                     if ($dtr) {
                        ${"L_".$emprefid} = $dtr["Total_Tardy_Count"];
                        ${"U_".$emprefid} = $dtr["Total_Undertime_Count"];
                        ${"A_".$emprefid} = $dtr["Total_Absent_Count"];
                        ${"VL_".$emprefid} = $dtr["VL_Used"];
                        ${"SL_".$emprefid} = $dtr["SL_Used"];
                        for ($v=1;$v<=31;$v++) {
                           if ($v <= 9) $v = "0".$v; 
                           $late    = $dtr[$v."_Late"];
                           $ut      = $dtr[$v."_UT"];
                           $absent  = $dtr[$v."_Absent"];
                           if ($absent > 0) {
                              echo '<td class="txt-center">A</td>';   
                           } else {
                              echo '<td class="txt-center"></td>';   
                           }
                           
                        }   
                     }
                      
                  ?>
                  <td class="txt-center">
                     <?php echo ${"L_".$emprefid}; ?>
                  </td>
                  <td class="txt-center">
                     <?php echo ${"U_".$emprefid}; ?>
                  </td>
                  <td class="txt-center">
                     <?php echo ${"A_".$emprefid}; ?>
                  </td>
                  <?php
                     if (
                        ${"L_".$emprefid}     == 0 &&
                        ${"U_".$emprefid}     == 0 &&
                        ${"A_".$emprefid}     == 0
                     ) {
                        echo '<td class="text-center" colspan=8>COMPLETE ATTENDANCE</td>';
                     } else {
                  ?>
                  <td class="txt-center">
                     <?php echo ${"VL_".$emprefid}; ?>
                  </td>
                  <td class="txt-center">
                     <?php echo ${"FL_".$emprefid}; ?>
                  </td>
                  <td class="txt-center">
                     <?php echo ${"SL_".$emprefid}; ?>
                  </td>
                  <td class="txt-center">
                     <?php echo ${"SPL_".$emprefid}; ?>
                  </td>
                  <td class="txt-center">
                     <?php echo ${"ML_".$emprefid}; ?>
                  </td>
                  <td class="txt-center">
                     <?php echo ${"PL_".$emprefid}; ?>
                  </td>
                  <td class="txt-center">
                     <?php echo ${"MC8_".$emprefid}; ?>
                  </td>
                  <td class="txt-center">
                     <?php echo ${"CTO_".$emprefid}; ?>
                  </td>
                  <?php } ?>
               </tr>
            <?php
                  }
               }
            }else {
               echo '<div>NO RECORD QUERIED base on your criteria!!!</div>';
               echo '<div>'.$errmsg.'</div>';
            }
            ?>
         </table>
         <p>
            <div class="row">
               <div class="col-xs-2 txt-right">Prepared By:</div>
               <div class="col-xs-4"></div>
               <div class="col-xs-2 txt-right">Approved By:</div>
               <div class="col-xs-4"></div>
            </div>
            <div class="row">
               <div class="col-xs-2"></div>
               <div class="col-xs-4">________________________</div>
               <div class="col-xs-2"></div>
               <div class="col-xs-3">________________________</div>
               <div class="col-xs-1"></div>
            </div>
         </p>

      </div>
   </body>
</html>