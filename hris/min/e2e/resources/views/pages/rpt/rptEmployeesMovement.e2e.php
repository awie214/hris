<?php
   include 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   require_once pathClass.'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = true;
   include_once 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <!--<table>
            <thead>
               <tr>
                  <th>
                  </th>
               </tr>   
            </thead>
            <tbody>
               <tr>
                  <td></td>
               </tr>   
            </tbody>
            <tfoot>
               <tr>
                  <td></td>
               </tr>   
            </tfoot>
         </table>-->   
         <table>
            <thead>
               <tr>
                  <th colspan="13" align="center" style="text-align:center;">
                     <?php
                        rptHeader(getRptName(getvalue("drpReportKind")));
                     ?>
                     <p class="txt-center">For the Month of <u><?php echo monthName(date("m",time()),1).", ".date("Y",time()) ?></u></p>
                  </th>
                  
               </tr>   
               <tr class="colHEADER">
                  <th class="padd5" rowspan=2 style="width:*%">Agency Position</th>
                  <th class="padd5" rowspan=2 style="width:7.5%">CES Status</th>
                  <th class="padd5" rowspan=2 style="width:7.5%">Nature of Movement</th>
                  <th class="padd5" rowspan=2 style="width:7.5%">Office</th>
                  <th class="padd5" colspan="3" style="width:22.5%">FORMER</th>
                  <th class="padd5" colspan="4" style="width:30%">PRESENT</th>
                  <th class="padd5" rowspan=2 style="width:7.5%">Date of Effectivity</th>
                  <th class="padd5" rowspan=2 style="width:7.5%">Remarks</th>
               </tr>
               <tr class="colHEADER">
                  <th class="padd5">Position</th>
                  <th class="padd5">Office</th>
                  <th class="padd5">Place of Assign</th>
                  <th class="padd5">Position</th>
                  <th class="padd5">Status of Appointment</th>
                  <th class="padd5">Office</th>
                  <th class="padd5">Place of Assign</th>
               </tr>
            </thead>
            <tbody>
                  <?php for ($j=1;$j<=60;$j++) 
                  { ?>
                     <tr>
                        <td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                     </tr>
                  <?php 
                  } ?>
                  <tr>
                     <td colspan="13">
                        <br><br>
                        <p>
                           <div class="row">
                              <div class="col-xs-2 txt-right">Prepared By:</div>
                              <div class="col-xs-4"></div>
                              <div class="col-xs-2 txt-right">Certified correct / Approved By:</div>
                              <div class="col-xs-4"></div>
                           </div>
                           <div class="row">
                              <div class="col-xs-2"></div>
                              <div class="col-xs-4">________________________</div>
                              <div class="col-xs-2"></div>
                              <div class="col-xs-3">________________________</div>
                              <div class="col-xs-1"></div>
                           </div>
                        </p>     
                     </td>
                  </tr>         
            </tbody> 
         </table>   
      </div>
   </body>
</html>