<?php
   include_once 'pageHEAD.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   $control_number                = getvalue("control_number");
   $where      = "WHERE ControlNumber = '$control_number'";
   $rs         = SelectEach("employeesauthority",$where);
   $rs2         = SelectEach("employeesauthority",$where);
   while ($dum_row = mysqli_fetch_assoc($rs)) {
      $Signatory1       = $dum_row["Signatory1"];
      $Signatory2       = $dum_row["Signatory2"];
      $date_start       = $dum_row["ApplicationDateFrom"];
      $date_end         = $dum_row["ApplicationDateTo"];
      $time_start       = $dum_row["FromTime"];
      $time_end         = $dum_row["ToTime"];
      $filed_date       = $dum_row["FiledDate"];
      $remarks          = $dum_row["Remarks"];
      $destination      = $dum_row["Destination"];
      $ActivityNature   = $dum_row["ActivityNature"];
      $Sponsor          = $dum_row["Sponsor"];

      $s1_row           = FindFirst("signatories","WHERE RefId = '$Signatory1'","*");
      $s2_row           = FindFirst("signatories","WHERE RefId = '$Signatory2'","*");

      $s1_name          = $s1_row["Name"];
      $s1_position      = getRecord("position",$s1_row["PositionRefId"],"Name");

      $s2_name          = $s2_row["Name"];
      $s2_position      = getRecord("position",$s2_row["PositionRefId"],"Name");
      $LDMSLNDProgramRefId = $dum_row["LDMSLNDProgramRefId"];
   }
   if ($date_start == $date_end) {
      $date_range = date("d F Y",strtotime($date_start));   
   } else {
      $date_range = date("d F Y",strtotime($date_start))." to ".date("d F Y",strtotime($date_end));   
   }
   if ($time_start != "") {
      $time_range = HrsFormat($time_start)." - ".HrsFormat($time_end);
   } else {
      $time_range = "Whole Day";
   }
   $user      = getvalue("hEmpRefId");
   $user_row  = FindFirst("employees","WHERE RefId = '$user'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
   if ($user_row) {
      $user_LastName   = $user_row["LastName"];
      $user_FirstName  = $user_row["FirstName"];
   } else {
      $user_FirstName = $user_LastName = "";
   }
   $user_FullName = $user_FirstName." ".$user_LastName;

   $program = FindFirst("ldmslndprogram","WHERE RefId = '$LDMSLNDProgramRefId'","*");
   
?>
<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		td, th {
			border: 2px solid black;
			vertical-align: top;
			padding: 5px;
			font-size: 9pt;
		}
      th {text-align: center;}
		.data {
			font-size: 10pt;
			text-transform: uppercase;
		}
	</style>
</head>
<body>
	<div class="container-fluid rptBody">
		<div style="page-break-after: always;">
      <?php rptHeader("Office Order",""); ?>
      <div class="row margin-top">
        	<div class="col-xs-12">
        		<b>OFFICE ORDER NO: <?php echo $control_number; ?></b>
            <br>
            Series of <?php echo date("Y",strtotime($filed_date)); ?>
        	</div>
      </div>  
      <br><br>
      <div class="row margin-top">
         <div class="col-xs-12 text-center">
            <b><?php echo $program["Name"]; ?></b>
         </div>
      </div>
      <br>
      <div class="row margin-top">
         <div class="col-xs-12">
            In the interest of the service, the following Regulatory Office (RO) talents are hereby authorized to attend to the said course to be conducted by the <u><b><?php echo $program["Facilitator"]; ?></b></u> on <u><b><?php echo date("F d, Y",strtotime($program["StartDate"]))." to ".date("F d, Y",strtotime($program["EndDate"])); ?></b></u> at the <u><b><?php echo $program["Venue"]; ?></b></u>:
            <br><br>
            <div class="row">
               <div class="col-xs-1"></div>
               <div class="col-xs-11">
                  <ol>
                     <?php
                        while ($row = mysqli_fetch_assoc($rs2)) {
                           $emprefid = $row["EmployeesRefId"];
                           $remarks  = $row["Remarks"];
                           $emp_row       = FindFirst("employees","WHERE RefId = $emprefid","*");
                           if ($emp_row) {
                              $LastName      = $emp_row["LastName"];
                              $FirstName     = $emp_row["FirstName"];
                              $MiddleName    = $emp_row["MiddleName"];   
                              $ExtName       = $emp_row["ExtName"];
                              $FullName      = $LastName.", ".$FirstName." ".$ExtName." ".$MiddleName;
                              $cid           = $emp_row["CompanyRefId"];
                           } else {
                              $FullName = $ExtName = $LastName = $FirstName = $MiddleName = "&nbsp;";
                              $cid           = 0;
                           }
                           echo '
                              <li>'.$FullName.'</li>
                           ';
                        }
                     ?>
                  </ol>      
               </div>
            </div>
            <br><br>
            The RO will shoulder the participants’ total registration fee of <b><?php echo convertNumberToWord($program["Cost"]); ?></b>    (Php <u><b><?php echo number_format($program["Cost"]); ?></b></u>), chargeable against the Training Budget for CY 2019, and subject to the usual accounting and auditing rules and regulations.
            <br>
            <br>
            The participants are required to submit their individual post-activity reports, certificates of attendance and other relevant documents to the Administration Department, after (10) days from the end of the said seminar.
            <br><br>
            Failure of the participants to comply with the stated requirements shall serve as a basis or ground for their automatic disqualification in attending succeeding seminars, trainings and the like. 
            <br><br>
            This Office Order shall take effect immediately.
            <br><br>
            <div class="row margin-top">
               <div class="col-xs-8"></div>
               <div class="col-xs-4 text-center">
                  PATRICK LESTER N. TY
                  <br>
                  Chief Regulator
               </div>
            </div>
         </div>
      </div>
   </div>
</body>
</html>