<?php
   include_once 'pageHEAD.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   $control_number                = getvalue("control_number");
   $where      = "WHERE ControlNumber = '$control_number'";
   $rs         = SelectEach("employeesauthority",$where);
   $rs2         = SelectEach("employeesauthority",$where);
   $driver     = "";
   while ($dum_row = mysqli_fetch_assoc($rs)) {
      $Signatory1       = $dum_row["Signatory1"];
      $Signatory2       = $dum_row["Signatory2"];
      $date_start       = $dum_row["ApplicationDateFrom"];
      $date_end         = $dum_row["ApplicationDateTo"];
      $time_start       = $dum_row["FromTime"];
      $time_end         = $dum_row["ToTime"];
      $filed_date       = $dum_row["FiledDate"];
      $remarks          = $dum_row["Remarks"];
      $destination      = $dum_row["Destination"];
      $FuelTicketNo     = $dum_row["FuelTicketNo"];
      $PlateNo          = $dum_row["PlateNo"];
      $VehicleType      = $dum_row["VehicleType"];
      $OtherPassenger   = $dum_row["OtherPassenger"];
      $RegulationArea   = $dum_row["RegulationArea"];
      $DivisionRefId    = $dum_row["DivisionRefId"];
      $Driver           = $dum_row["Driver"];

      $s1_row           = FindFirst("signatories","WHERE RefId = '$Signatory1'","*");
      $s2_row           = FindFirst("signatories","WHERE RefId = '$Signatory2'","*");

      $s1_name          = $s1_row["Name"];
      $s1_position      = getRecord("position",$s1_row["PositionRefId"],"Name");

      $s2_name          = $s2_row["Name"];
      $s2_position      = getRecord("position",$s2_row["PositionRefId"],"Name");
   }
   if ($date_start == $date_end) {
      $date_range = date("d F Y",strtotime($date_start));   
   } else {
      $date_range = date("d F Y",strtotime($date_start))." to ".date("d F Y",strtotime($date_end));   
   }
   if ($time_start != "") {
      $time_range = HrsFormat($time_start)." - ".HrsFormat($time_end);
   } else {
      $time_range = "Whole Day";
   }
   switch ($RegulationArea) {
      case 'FR':
         $RegulationArea = "Financial Regulation Area";
         break;
      case 'TR':
         $RegulationArea = "Technical Regulation Area";
         break;
      case 'CSR':
         $RegulationArea = "Customer Service Regulation Area";
         break;
      default:
         $RegulationArea = "";
         break;
   }
?>
<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		td, th {
			border: 2px solid black;
			vertical-align: top;
			padding: 5px;
			font-size: 9pt;
		}
      th {text-align: center;}
		.data {
			font-size: 10pt;
			text-transform: uppercase;
		}
      .border {
         border: 1px solid black;
         padding: 2px;
         text-align: center;
      }
	</style>
</head>
<body>
	<div class="container-fluid rptBody">
		<div class="row">
         <div class="col-xs-12">
            <?php
               //rptHeader("");
            ?>
            <div class="row">
               <div class="col-xs-12 text-right">
                  Date: <u><?php echo $date_range; ?></u>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <table width="100%" border="1">
                     <tr>
                        <td class="text-center" style="padding: 10px;">
                           <div class="row">
                              <div class="col-xs-8 text-center" style="font-size: 15pt;">
                                 <b>DRIVER'S TRIP TICKET</b>
                              </div>
                              <div class="col-xs-4 text-center" style="border: 2px solid black; font-size: 12pt;">
                                 <?php echo $control_number; ?>
                              </div>
                           </div>
                        </td>
                     </tr>
                     <tr>
                        <td style="padding: 20px;">
                           <div class="row">
                              <div class="col-xs-12">
                                 <div class="row">
                                    <div class="col-xs-2">
                                       <b>Name Of Driver</b>
                                    </div>
                                    <div class="col-xs-5 border">
                                       <b><?php echo $Driver; ?></b>
                                    </div>
                                    <div class="col-xs-2 text-center">
                                       <b>Fuel Ticket No</b>
                                    </div>
                                    <div class="col-xs-3 border">
                                       <b><?php echo $FuelTicketNo; ?></b>
                                    </div>
                                 </div>
                                 <br>
                                 <div class="row margin-top">
                                    <div class="col-xs-2">
                                       <b>Vehicle Type</b>
                                    </div>
                                    <div class="col-xs-3 border">
                                       <b><?php echo $VehicleType; ?></b>
                                    </div>
                                    <div class="col-xs-2 text-center">
                                       <b>Plate No.</b>
                                    </div>
                                    <div class="col-xs-2 border">
                                       <b><?php echo $PlateNo; ?></b>
                                    </div>
                                    <div class="col-xs-1 text-center">
                                       <b>ETD</b>
                                    </div>
                                    <div class="col-xs-2 border">
                                       <b><?php echo HrsFormat($time_start); ?></b>
                                    </div>
                                 </div>
                                 <br>
                                 <div class="row margin-top">
                                    <div class="col-xs-2">
                                       <b>Regulation Area</b>
                                    </div>
                                    <div class="col-xs-3 border">
                                       <b><?php echo $RegulationArea; ?></b>
                                    </div>
                                    <div class="col-xs-2 text-center">
                                       <b>Department</b>
                                    </div>
                                    <div class="col-xs-2 border">
                                       <b><?php echo getRecord("division",$DivisionRefId,"Name"); ?></b>
                                    </div>
                                    <div class="col-xs-1 text-center">
                                       <b>ETA</b>
                                    </div>
                                    <div class="col-xs-2 border">
                                       <b><?php echo HrsFormat($time_end); ?></b>
                                    </div>
                                 </div>
                                 <br>
                                 <div class="row margin-top">
                                    <div class="col-xs-3">
                                       <b>Authorized Passengers</b>
                                    </div>
                                    <div class="col-xs-9" style="border: 1px solid black; padding: 2px;">
                                       <b>
                                          <?php
                                             if ($rs2) {
                                                while ($n_row = mysqli_fetch_assoc($rs2)) {
                                                   $emprefid = $n_row["EmployeesRefId"];
                                                   $emp_position     = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","PositionRefId");
                                                   $passenger            = getRecord("position",$emp_position,"Code");
                                                   if ($passenger != "Driver") {
                                                      $passenger_row    = FindFirst("employees","WHERE RefId = '$emprefid'","`FirstName`,`MiddleName`,`LastName`,`ExtName`"); 
                                                      if ($passenger_row) {
                                                         $passenger     = $passenger_row["FirstName"]." ".substr($passenger_row["MiddleName"], 0,1).". ".$passenger_row["LastName"]." ".$passenger_row["ExtName"];
                                                         echo $passenger."<br>";
                                                      }
                                                   }
                                                }
                                             }
                                             echo convertBR($OtherPassenger);
                                          ?>
                                       </b>
                                    </div>
                                 </div>
                                 <br>
                                 <div class="row margin-top">
                                    <div class="col-xs-3">
                                       <b>Purpose of Travel</b>
                                    </div>
                                    <div class="col-xs-9" style="border: 1px solid black; padding: 2px;">
                                       <b><?php echo $remarks; ?></b>
                                    </div>
                                 </div>
                                 <br>
                                 <div class="row margin-top">
                                    <div class="col-xs-3">
                                       <b>Itinerary of Travel/<br>Places to be visited</b>
                                    </div>
                                    <div class="col-xs-9" style="border: 1px solid black; padding: 2px;">
                                       <b><?php echo $destination; ?></b>
                                    </div>
                                 </div>
                                 <br>
                                 <div class="row margin-top">
                                    <div class="col-xs-3 text-center">
                                       <b>Requested by:</b>
                                    </div>
                                    <div class="col-xs-3 border">
                                       <br><br><br>
                                       <?php echo "<b>".strtoupper($s1_name)."</b><br>".$s1_position; ?>
                                    </div>
                                    <div class="col-xs-3 text-center">
                                       <b>Approved by:</b>
                                    </div>
                                    <div class="col-xs-3 border">
                                       <br><br><br>
                                       <?php echo "<b>".strtoupper($s2_name)."</b><br>".$s2_position; ?>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-3 text-center"></div>
                                    <div class="col-xs-3 text-center">
                                       <b>Signature Over Printed Name</b>
                                    </div>
                                    <div class="col-xs-3 text-center"></div>
                                    <div class="col-xs-3 text-center">
                                       <b>Authorized DA/DM</b>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </td>
                     </tr>
                     <tr>
                        <td></td>
                     </tr>
                     <tr>
                        <td></td>
                     </tr>
                     <tr>
                        <td class="text-center">
                           <b>Record of Travel</b>
                           <br>
                           (To be accomplished by the driver)
                        </td>
                     </tr>
                     <tr>
                        <td></td>
                     </tr>
                     <tr>
                        <td></td>
                     </tr>
                     <tr>
                        <td style="padding: 20px;">
                           <div class="row">
                              <div class="col-xs-12">
                                 <div class="row">
                                    <div class="col-xs-3">
                                       <b>Odometer Reading</b>
                                    </div>
                                    <div class="col-xs-2 text-center">
                                       <b>Departure:</b>
                                    </div>
                                    <div class="col-xs-2 border">
                                       &nbsp;
                                    </div>
                                    <div class="col-xs-3 text-center">
                                       <b>=Distanced Traveled (Km)</b>
                                    </div>
                                    <div class="col-xs-2 border">
                                       &nbsp;
                                    </div>
                                 </div>
                                 <br>
                                 <div class="row margin-top">
                                    <div class="col-xs-3">
                                    </div>
                                    <div class="col-xs-2 text-center">
                                       <b>Arrival:</b>
                                    </div>
                                    <div class="col-xs-2 border">
                                       &nbsp;
                                    </div>
                                 </div>
                                 <br>
                                 <div class="row margin-top">
                                    <div class="col-xs-12">
                                       <b>Places Actually Visited:</b>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-1"></div>
                                    <div class="col-xs-11" style="border-bottom: 1px solid black;">
                                       &nbsp;
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-1"></div>
                                    <div class="col-xs-11" style="border-bottom: 1px solid black;">
                                       &nbsp;
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-1"></div>
                                    <div class="col-xs-11" style="border-bottom: 1px solid black;">
                                       &nbsp;
                                    </div>
                                 </div>
                                 <br>
                                 <div class="row margin-top">
                                    <div class="col-xs-12">
                                       <b>Remarks:</b>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </td>
                     </tr>
                     <tr>
                        <td style="padding: 20px;">
                           <div class="row">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-4" style="text-align: justify; text-indent: 25px;">
                                 I hereby certify that I have used the above described vehicle/equipment on official business as stated above and to be correctness of the record of travel.
                              </div>
                              <div class="col-xs-2"></div>
                              <div class="col-xs-4" style="text-align: justify; text-indent: 25px;">
                                 I hereby certify to the correctness of the above statement of travel made.
                              </div>
                           </div>
                           <br><br><br>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-4 text-center">
                                 ____________________________________
                                 <br>
                                 Authorized Passenger
                              </div>
                              <div class="col-xs-2"></div>
                              <div class="col-xs-4 text-center">
                                 ____________________________________
                                 <br>
                                 Driver
                              </div>
                           </div>
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
      </div>
    </div>
</body>
</html>