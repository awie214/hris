<?php
   //session_start();
   include_once 'colors.e2e.php';
   include_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   require_once pathClass.'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $fld = getvalue("fld");
   $isdoctype = getvalue("isdoctype");
   /*$rs = SelectEach("employees","LIMIT ".getvalue("count"));
   if ($rs) $rowcount = mysqli_num_rows($rs);
   switch (getvalue("rptIDX")) {
      case 1:
         $rptTitle = "TOTAL EMPLOYEES";
      break;
      case 2:
         $rptTitle = "NEWLY REGULAR";
      break;
      case 3:
         $rptTitle = "END OF CONTRACT";
      break;
      case 4:
         $rptTitle = "RESIGNED / TERMINATED";
      break;
      case 5:
         $rptTitle = "NOTIFICATION FOR BIRTHDAY";
      break;
      case 6:
         $rptTitle = "NOTIFICATION FOR ANNIVERSARY";
      break;
      case 7:
         $rptTitle = "NOTIFICATION FOR PROMOTED";
      break;
      case 8:
         $rptTitle = "NOTIFICATION FOR RETIREES";
      break;
      case 9:
         $rptTitle = "STEP INCREMENT AND LOYALTY (3 YEARS)";
      break;
      case 10:
         $rptTitle = "STEP INCREMENT AND LOYALTY (4 - 10 YEARS)";
      break;
      case 11:
         $rptTitle = "STEP INCREMENT AND LOYALTY (11 - 15 YEARS)";
      break;
      case 12:
         $rptTitle = "STEP INCREMENT AND LOYALTY (16 - UP YEARS)";
      break;
      case 13:
         $rptTitle = "NO BIRTH CERTIFICATE";
      break;
      case 14:
         $rptTitle = "NO TRANSCRIPT OF RECORDS (TOR)";
      break;
      case 15:
         $rptTitle = "DUAL CITIZEN";
      break;
      case 16:
         $rptTitle = "EMP. w/ GRAD. STUDIES";
      break;
   }*/
   $th = "";
   $td = "";
   switch ($fld) {
      case 'PHIC':
         $th = "<th>PhilHealth</th>";
         break;
      case 'SSS':
         $th = "<th>SSS</th>";
         break;
      case 'PAGIBIG':
         $th = "<th>PAGIBIG</th>";
         break;
      case 'GSIS':
         $th = "<th>GSIS</th>";
         break;
      case 'TIN':
         $th = "<th>TIN</th>";
         break;
      case 'PicFilename':
         $th = "<th>Picture</th>";
         break;
      case 'BirthDate':
         $th = "<th>Birthday</th>";
         break;
      case 'AgencyId':
         $th = "<th>Agency ID</th>";
         break;
   }
   /*==================================================================================*/
   $ttsql = "CREATE TEMPORARY TABLE `tt_empdoc` (
            EmployeesRefId INT(10) DEFAULT NULL,
            Has INT(1) DEFAULT NULL)";
   mysqli_query($conn,$ttsql) or die(mysqli_error());
   /*==================================================================================*/
   $dir = path."images/EmpDocument";
   if (is_dir($dir)){   
      if ($dh = opendir($dir)){
         while ($file = readdir($dh))
         {
            if (stripos($file,".png") > 0 ||
                stripos($file,".jpg") > 0 ||
                stripos($file,".gif") > 0 ||
                stripos($file,".jpeg") > 0 ||
                stripos($file,".pdf") > 0 
               ) {
               $arr = explode("_", $file);
               $type = $arr[0];
               $emprefid = explode(".", $arr[1])[0];
               if ($fld == $type) {
                  $select_sql = "SELECT * FROM tt_empdoc WHERE EmployeesRefId = ".$emprefid;                  
                  $select_qry = mysqli_query($conn,$select_sql);
                  if (mysqli_num_rows($select_qry) == 0) {
                     $row = mysqli_fetch_assoc($select_qry);
                     $flds = "EmployeesRefId, Has";
                     $vals = "'".$emprefid."', '1'";
                     $save_sql = "INSERT INTO tt_empdoc ($flds) VALUES ($vals)";
                     $save_qry = mysqli_query($conn,$save_sql);
                     if (!$save_qry) {
                        echo "Error in Saving ".$file;
                     }
                  }
               }
            }
         }
         closedir($dh);
      }
   }   

?>

<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style>
         td {vertical-align:top;}
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader("Employees ".getvalue("rptName"));
         ?>
            <table border="1">
               <thead>
                  <tr>
                     <th>#</th>
                     <th>EMPLOYEE ID</th>
                     <th>LAST NAME</th>
                     <th>FIRST NAME</th>
                     <th>MIDDLE NAME</th>
                     <?php echo $th; ?>
                  </tr>
               </thead>
               <?php
                  $count = 0;
                  $query = "";
                  if ($isdoctype == 0) {
                     $whereClause = "WHERE CompanyRefId = ".getvalue("hCompanyID")." AND BranchRefId = ".getvalue("hBranchID");
                     switch ($fld) {
                        case 'BirthDate':
                           $table = "employees";
                           $query = $whereClause." AND MONTH(BirthDate) = '".date("m",time())."'";
                           $query .= " AND (Inactive != 1 OR Inactive IS NULL)";
                           break;
                        case 'Employees':
                           $table = "employees";
                           $query = $whereClause." AND (Inactive != 1 OR Inactive IS NULL)";
                           break;
                        default:
                           $table = "employees";
                           $query = $whereClause." AND ".$fld." IS NULL or ".$fld." = ''";
                           $query .= " AND (Inactive != 1 OR Inactive IS NULL)";
                           break;
                     }
                     if ($fld == "PHIC" || $fld == "GSIS" || $fld == "SSS" || $fld == "PAGIBIG") {
                        $query .= " or ".$fld." = 'N/A'";
                     }
                     $rs = SelectEach($table,$query);
                     while ($row = mysqli_fetch_assoc($rs)) {
                        switch ($fld) {
                           case 'PHIC':
                              $td = '<td align="center">'.$row["PHIC"].'</td>';
                              break;
                           case 'SSS':
                              $td = '<td align="center">'.$row["SSS"].'</td>';
                              break;
                           case 'PAGIBIG':
                              $td = '<td align="center">'.$row["PAGIBIG"].'</td>';
                              break;
                           case 'GSIS':
                              $td = '<td align="center">'.$row["GSIS"].'</td>';
                              break;
                           case 'TIN':
                              $td = '<td align="center">'.$row["TIN"].'</td>';
                              break;
                           case 'PicFilename':
                              $td = '<td align="center">'.$row["PicFilename"].'</td>';
                              break;

                           case 'BirthDate':
                              $td = '<td align="center">'.date("F d",strtotime($row["BirthDate"])).'</td>';
                              break;
                           case 'AgencyId':
                              $td = '<td align="center">'.$row["AgencyId"].'</td>';
                              break;
                        }
                        $count++;
                        echo
                        '<tr>
                           <td class="txt-center" style="width:50px;background:#ededed;">'.$count.'</td>
                           <td class="txt-center">'.$row["AgencyId"].'</td>
                           <td>'.$row["LastName"].'</td>
                           <td>'.$row["FirstName"].'</td>
                           <td>'.$row["MiddleName"].'</td>
                           '.$td.'
                        </tr>';
                     }
                  } else {
                     echo "<tbody>";
                     $emp_select = SelectEach("employees","");
                     while ($row = mysqli_fetch_assoc($emp_select)) {
                        $new_qry = "SELECT * FROM tt_empdoc WHERE EmployeesRefId = ".$row["RefId"];
                        $rs = mysqli_query($conn,$new_qry);
                        if (mysqli_num_rows($rs) == 0) {
                           $f = mysqli_fetch_assoc($rs);
                           $count++;
                           echo
                           '
                           <tr>
                              <td class="txt-center" style="width:50px;background:#ededed;">'.$count.'</td>
                              <td class="txt-center">'.$row["RefId"].'</td>
                              <td>'.$row["LastName"].'</td>
                              <td>'.$row["FirstName"].'</td>
                              <td>'.$row["MiddleName"].'</td>
                              '.$td.'
                           </tr>';   
                        }
                        
                     }
                     echo "</tbody>";
                  }
               ?>
            </table>
            <?php
               rptFooter();
            ?>
      </div>
   </body>
</html>
<?php
   mysqli_query($conn,"DROP TEMPORARY TABLE tt_empdoc");
?>