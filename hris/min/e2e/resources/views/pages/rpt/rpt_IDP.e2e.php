<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $row = FindFirst("employees_assessment","WHERE RefId = '".getvalue("refid")."'","*");
   $year = $row["Year"];
   $emprefid = $row["EmployeesRefId"];
   $where = "WHERE RefId = '$emprefid'";
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style type="text/css">
         .gray {background: gray;}
      </style>
   </head>
   <body>
      <div class="container-fluid">
         <?php
            rptHeader("Individual Development Plan");
         ?>
         <br><br>
         <?php
            $rsEmployee = SelectEach("employees",$where);
            if ($rsEmployee) {
               while ($employee_row = mysqli_fetch_assoc($rsEmployee)) {
                  $emprefid      = $employee_row["RefId"];
                  $LastName      = $employee_row["LastName"];
                  $FirstName     = $employee_row["FirstName"];
                  $MiddleName    = $employee_row["MiddleName"];
                  $FullName      = $LastName.", ".$FirstName." ".$MiddleName;
                  $where         = "WHERE EmployeesRefId = '$emprefid' AND Year = '$year'";
                  $check         = FindFirst("individual_competency_data",$where,"RefId");
                  if (intval($check) > 0) {
                     $empinfo = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","`PositionRefId`,`OfficeRefId`");
                     if ($empinfo) {
                        $Position   = getRecord("Position",$empinfo["PositionRefId"],"Name");
                        $Office     = getRecord("Office",$empinfo["OfficeRefId"],"Name");
                     } else {
                        $Position = $Office = "";
                     }   
                  
         ?>
         <div class="row">
            <div class="col-xs-12">
               <table>
                  <tr>
                     <td style="width: 16.66%;">
                     </td>
                     <td style="width: 16.66%;">
                     </td>
                     <td style="width: 16.66%;">
                     </td>
                     <td style="width: 16.66%;">
                     </td>
                     <td style="width: 16.66%;">
                     </td>
                     <td style="width: 16.66%;">
                     </td>
                  </tr>
                  <tr>
                     <td class="gray">Name of Talent</td>
                     <td colspan="5"><b><?php echo $FullName; ?></b></td>
                  </tr>
                  <tr>
                     <td class="gray">Position Title</td>
                     <td colspan="5"><b><?php echo $Position; ?></b></td>
                  </tr>
                  <tr>
                     <td class="gray">Office/Area</td>
                     <td colspan="5"><b><?php echo $Office; ?></b></td>
                  </tr>
                  <tr>
                     <td class="gray">Immediate Supervisor</td>
                     <td colspan="5"><b></b></td>
                  </tr>
                  <tr class="text-center gray">
                     <td rowspan="2">
                        <b>COMPETENCIES TO DEVELOP</b>
                     </td>
                     <td colspan="2">
                        <b>FOCUS AREA</b>
                     </td>
                     <td rowspan="2">
                        <b>PLANNED INTERVENTION</b>
                     </td>
                     <td rowspan="2">
                        <b>IMPLEMENTATION DATE</b>
                     </td>
                     <td rowspan="2">
                        <b>STATUS</b>
                     </td>
                  </tr>
                  <tr class="text-center gray">
                     <td>Level</td>
                     <td>Behaviour</td>
                  </tr>
                  <tr>
                     <?php
                        $rs = SelectEach("individual_competency_data",$where);
                        if ($rs) {
                           while ($row = mysqli_fetch_assoc($rs)) {
                              echo '<tr>';
                              echo '<td>'.getRecord("competency",$row["CompetencyRefId"],"Name").'</td>';
                              echo '<td class="text-center">'.$row["Level"].'</td>';
                              echo '<td class="text-center">'.$row["Behavior"].'</td>';
                              echo '<td class="text-center">'.$row["Requirement"].'</td>';
                              echo '<td class="text-center">'.$row["ImplementationDate"].'</td>';
                              echo '<td class="text-center">'.$row["Status"].'</td>';
                              echo '</tr>';
                           }
                        }
                     ?>
                  </tr>
                  <tr>
                     <td class="text-center">
                        Talent's Signature
                     </td>
                     <td class="text-center">
                        Date
                     </td>
                     <td class="text-center">
                        Department Manager's Signature
                     </td>
                     <td class="text-center">
                        Date
                     </td>
                     <td class="text-center">
                        Deputy Administrator's Signature
                     </td>
                     <td class="text-center">
                        Department Manager's Signature
                     </td>
                  </tr>
                  <tr>
                     <td>&nbsp;</td>
                     <td>&nbsp;</td>
                     <td>&nbsp;</td>
                     <td>&nbsp;</td>
                     <td>&nbsp;</td>
                     <td>&nbsp;</td>
                  </tr>
                  <tr>
                     <td>Applicable copy as shown:</td>
                     <td colspan="5">
                        <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;Talent's Copy
                        &nbsp;&nbsp;&nbsp;
                        <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;DM's Copy
                        &nbsp;&nbsp;&nbsp;
                        <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;DA's Copy
                        &nbsp;&nbsp;&nbsp;
                     </td>
                  </tr>
               </table>
            </div>
         </div>
         <?php
                  }
               }
            }
         ?>
      </div>
   </body>
</html>
