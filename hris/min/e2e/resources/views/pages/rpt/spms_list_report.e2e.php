<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $period     = getvalue("period");
   $division   = getvalue("division");
   $department = getvalue("department");
   $year       = getvalue("year");
   $where = "WHERE RefId > 0";
   if ($period != "") {
      $where .= " AND Semester = '$period'";
   }
   if (intval($division) > 0) {
      $where .= " AND DivisionRefId = '$division'";
   }
   if (intval($department) > 0) {
      $where .= " AND DepartmentRefId = '$department'";
   }
   if (intval($year) > 0) {
      $where .= " AND YearPerformed = '$year'";
   }
   $count = 0;
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style type="text/css">
         
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader("SPMS Report");
         ?>
         <br><br>
         <div class="row">
            <div class="col-xs-12">
               <table style="width: 100%;">
                  <thead>
                     <tr class="colHEADER">
                        <th>#</th>
                        <th>Employee Name</th>
                        <th>Period</th>
                        <th>Year</th>
                        <th>Adjectival</th>
                        <th>Average</th>
                        <th>Rating</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                        $rs = SelectEach("employeesperformance",$where);
                        if ($rs) {
                           while ($row = mysqli_fetch_assoc($rs)) {
                              $emprefid = $row["EmployeesRefId"];
                              $emp_row = FindFirst("employees","WHERE RefId = '$emprefid'","*");
                              if ($emp_row) {
                                 $count++;
                                 $LastName   = $emp_row["LastName"];
                                 $FirstName  = $emp_row["FirstName"];
                                 $MiddleName = $emp_row["MiddleName"];
                                 $ExtName    = $emp_row["ExtName"];
                                 $FullName   = $LastName.", ".$FirstName;
                                 switch ($row["Adjectival"]) {
                                    case '5':
                                       $Adjectival = "Poor";
                                       break;
                                    case '4':
                                       $Adjectival = "Unsatisfactory";
                                       break;
                                    case '3':
                                       $Adjectival = "Satisfactory";
                                       break;
                                    case '2':
                                       $Adjectival = "Very Satisfactory";
                                       break;
                                    case '1':
                                       $Adjectival = "Outstanding";
                                       break;
                                    default:
                                       $Adjectival = "";
                                       break;
                                 }
                                 echo '<tr>';
                                 echo '<td class="text-center">'.$count.'</td>';
                                 echo '<td>'.$FullName.'</td>';
                                 echo '<td class="text-center">'.$row["Semester"].'</td>';
                                 echo '<td class="text-center">'.$row["YearPerformed"].'</td>';
                                 echo '<td>'.$Adjectival.'</td>';
                                 echo '<td class="text-center">'.$row["OverallScore"].'</td>';
                                 echo '<td class="text-center">'.$row["NumericalRating"].'</td>';
                                 echo '</tr>';
                              }
                           }
                        } else {
                           echo '<tr><td colspan="7">No Record Found</td></tr>';
                        }
                     ?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </body>
</html>
