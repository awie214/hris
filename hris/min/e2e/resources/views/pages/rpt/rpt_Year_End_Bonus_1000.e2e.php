<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $year       = getvalue("txtAttendanceYear");
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
   
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         @media print {
            body {
               font-size: 9pt;
            }
            thead {
               font-size: 9pt;  
            }
            tbody {
               font-size: 8pt !important;
            }
            .border-bottom {
               border-bottom: 1px solid black;
            }
         }
      </style>
   </head>
   <body>
      <div class="container-fluid">
         <div class="col-xs-12">
            <div class="row margin-top">
               <div class="col-xs-12 text-center">
                  <b style="font-size: 12pt;">PHILIPPINE INSTITUTE FOR DEVELOPMENT STUDIES</b>
                  <br>
                  18F Three Cyberpod Centris - North Tower 
                  <br>
                  EDSA corner Quezon Avenue, Quezon City
               </div>
            </div>
            <br>
            <div class="row margin-top">
               <div class="col-xs-12 text-center">
                  <h4>LIST OF EMPLOYEES ELIGIBLE FOR YEAR END BONUS</h4>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  <table width="100%">
                     <thead>
                        <tr class="colHEADER">
                           <th>#</th>
                           <th>Employee ID</th>
                           <th>Employee Name</th>
                           <th>Date of Assumption</th>
                           <th>Service Rendered</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           $count = 0;
                           if ($rsEmployees) {
                              $count = 0;
                              while ($row = mysqli_fetch_assoc($rsEmployees)) {
                                 $AgencyId   = $row["AgencyId"];
                                 $MiddleName = $row["MiddleName"];
                                 $FirstName  = $row["FirstName"];
                                 $LastName   = $row["LastName"];
                                 $ExtName    = $row["ExtName"];
                                 $BirthDate  = $row["BirthDate"];
                                 $FullName   = $LastName.", ".$FirstName." ".$ExtName." ".$MiddleName;
                                 $emprefid   = $row["RefId"];
                                 $empinfo    = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","AssumptionDate,EmpStatusRefId");
                                 if ($empinfo) {
                                    $AssumptionDate   = $empinfo["AssumptionDate"];
                                    $category         = getRecord("empstatus",$empinfo["EmpStatusRefId"],"category");
                                    if ($AssumptionDate != "") {
                                       $count++;
                                       $year_start = $year."-01-01";
                                       if (strtotime($AssumptionDate) > strtotime($year_start)) {
                                          $date1   = $AssumptionDate;
                                       } else {
                                          $date1   = $year_start;
                                       }
                                       $date2   = $year."-10-31";
                                       $diff    = abs(strtotime($date2) - strtotime($date1));
                                       $years   = floor($diff / (365*60*60*24));
                                       $months  = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                                       $days    = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
                                       if ($days > 1) {
                                          $service_day = "$days days";
                                       } else {
                                          $service_day = "$days day";
                                       }
                                       if ($months > 1) {
                                          if ($days == 0) $service_month = "$months months";
                                          else $service_month = "$months months and";
                                       } else {
                                          if ($days == 0) $service_month = "$months month";
                                          else $service_month = "$months month and";
                                       }
                                       if ($days == 0) $service_day = "";
                                       if ($months == 0) $service_month = "";
                                       $service = $service_month." ".$service_day;
                                       if ($category == 1) {
                                          echo '<tr>';
                                          echo '<td class="text-center">'.$count.'</td>';
                                          echo '<td class="text-center">'.$AgencyId.'</td>';
                                          echo '<td>'.$FullName.'</td>';
                                          echo '<td class="text-center">'.$AssumptionDate.'</td>';
                                          echo '<td class="text-center">'.$service.'</td>';
                                          echo '</tr>';
                                       }
                                          
                                    }
                                 }
                                    
                              }
                           } else {
                              echo '
                                 <tr>
                                    <td class="text-center" colspan="5">No Employee Searched...</td>
                                 </tr>
                              ';      
                           }
                        ?>
                        
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>