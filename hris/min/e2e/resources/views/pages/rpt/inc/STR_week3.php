<div class="row margin-top" style="page-break-after: always;">
	<div class="col-xs-12">
		<table width="100%" border="1">
			<thead>
				<tr>
					<th rowspan="2">#</th>
					<th rowspan="2">Name of Employee</th>
					<th colspan="5">Totals</th>
					<th colspan="32">Dates</th>
					<th rowspan="4">Indicate Leave Without Pay</th>
				</tr>
				<tr>
					<th>M/L</th>
					<th>S/L</th>
					<th>V/L</th>
					<th colspan="2">UNDERTIME</th>
					<th colspan="4">
						<?php echo chkDay("17",$month,$year); ?>
					</th>
					<th colspan="4">
						<?php echo chkDay("18",$month,$year); ?>
					</th>
					<th colspan="4">
						<?php echo chkDay("19",$month,$year); ?>
					</th>
					<th colspan="4">
						<?php echo chkDay("20",$month,$year); ?>
					</th>
					<th colspan="4">
						<?php echo chkDay("21",$month,$year); ?>
					</th>
					<th colspan="4">
						<?php echo chkDay("22",$month,$year); ?>
					</th>
					<th colspan="4">
						<?php echo chkDay("23",$month,$year); ?>
					</th>
					<th colspan="4">
						<?php echo chkDay("24",$month,$year); ?>
					</th>
				</tr>
				<tr>
					<th></th>
					<th class="text-left"><?php echo monthName($month,1)." 17-24, ".$year?></th>
					<th>DAYS</th>
					<th>DAYS</th>
					<th>DAYS</th>
					<th>HRS.</th>
					<th>MIN.</th>
					<?php
						for ($a=17; $a <= 24; $a++) { 
							echo '
								<th>S/L</th>
								<th>V/L</th>
								<th colspan="2">UTIME</th>
							';
						}
					?>
				</tr>
				<tr>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<?php
						for ($a=17; $a <= 24; $a++) { 
							echo '
								<th>D</th>
								<th>D</th>
								<th>H</th>
								<th>M</th>
							';
						}
					?>
				</tr>
			</thead>
			<tbody>
				<?php
					if ($rsEmployees_week3) {
						$count2 = 0;
						while ($week3 = mysqli_fetch_assoc($rsEmployees_week3)) {
							for ($a=17; $a <= 24; $a++) {
								if ($a == 9) {
									$a = "0".$a; 
								}
								${$a."_SL_week3"} = 0;
								${$a."_VL_week3"} = 0;
							}
							$SL_Total 		= 0;
							$VL_Total 		= 0;
							$HR_UT_Total 	= 0;
							$MIN_UT_Total 	= 0;
							$UT_Total 		= 0;

							
							if ($p_filter_value == "0" || $p_filter_table == "") {
								$emprefid = $week3["RefId"];
							} else {
								$emprefid = $week3["EmployeesRefId"];
							}
							$empstatus 	= FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","EmpStatusRefId");
							if ($cos != $empstatus) {
								$LastName 	= $week3["LastName"];
								$FirstName 	= $week3["FirstName"];
								$MiddleName = $week3["MiddleName"];
								$ExtName 	= $week3["ExtName"];
								$FullName 	= $LastName.", ".$FirstName." ".substr($MiddleName, 0, 1).". ".$ExtName;
								$where 		= "WHERE EmployeesRefId = '$emprefid' AND Month = '$month' AND Year = '$year'";
								$dtr 			= FindFirst("dtr_process",$where,"*");
								if ($dtr) {
									$count2++;
									$SL_Days = $dtr["SL_Days"];
									$VL_Days = $dtr["VL_Days"];

									if ($SL_Days != "") {
										$SL_Arr = explode("|", $SL_Days);
										foreach ($SL_Arr as $SL_key => $SL_value) {
											if ($SL_value != "") {
												if(isset(${$SL_value."_SL_week3"})) {
													${$SL_value."_SL_week3"}++;
													$SL_Total++;
												}
											}
										}
									}

									if ($VL_Days != "") {
										$VL_Arr = explode("|", $VL_Days);
										foreach ($VL_Arr as $VL_key => $VL_value) {
											if ($VL_value != "") {
												if(isset(${$VL_value."_VL_week3"})) {
													${$VL_value."_VL_week3"}++;
													$VL_Total++;
												}
											}
										}
									}

									for ($a=17; $a <= 24; $a++) { 
										if ($a == 9) {
											$a = "0".$a; 
										}
										$UT 	= $dtr[$a."_UT"];
										$hr   = 0;
										$min  = 0;
										if(intval($UT) > 0) {
											$UT_Total+=$UT;
										}
									}
									if ($UT_Total > 0) {
										$total_time_ut = rpt_HoursFormat($UT_Total);
										if ($total_time_ut != "") {
											$total_ut_arr 		= explode(":", $total_time_ut);
											$HR_UT_Total 		= $total_ut_arr[0];
											$MIN_UT_Total 		= $total_ut_arr[1];
										}
									}

									echo '
										<tr>
											<td class="text-center">'.$count2.'</td>
											<td>'.strtoupper($FullName).'</td>
											<td class="text-center">-</td>
											<td class="text-center">'.rmvValue($SL_Total).'</td>
											<td class="text-center">'.rmvValue($VL_Total).'</td>
											<td class="text-center">'.rmvValue($HR_UT_Total).'</td>
											<td class="text-center">'.rmvValue($MIN_UT_Total).'</td>
									';
										for ($a=17; $a <= 24; $a++) { 
											if ($a == 9) {
												$a = "0".$a; 
											}
											$UT 	= $dtr[$a."_UT"];
											$hr   = 0;
											$min  = 0;
											if(intval($UT) > 0) {
												$time_ut = rpt_HoursFormat($UT);
												if ($time_ut != "") {
													$ut_arr 	= explode(":", $time_ut);
													$hr 		= $ut_arr[0];
													$min 		= $ut_arr[1];
												}
											}
											echo '
												<td class="text-center">'.rmvValue(${$a."_SL_week3"}).'</td>
												<td class="text-center">'.rmvValue(${$a."_VL_week3"}).'</td>
												<td class="text-center">'.rmvValue($hr).'</td>
												<td class="text-center">'.rmvValue($min).'</td>
											';
										}
									if (floatval($SL_Total) > 0 || floatval($VL_Total) > 0) {
										$leave_remarks = "WITH Absences";
									} else {
										$leave_remarks = "NO Absences";
									}
									if (intval($UT_Total) > 0) {
										$ut_remarks = "WITH UT";
									} else {
										$ut_remarks = "NO UT";
									}
									$remarks = $ut_remarks." AND ".$leave_remarks;
									echo '<td>'.$remarks.'</td>';
									echo '</tr>';	
								}
							}
						}
					}
				?>
			</tbody>
		</table>
	</div>
</div>