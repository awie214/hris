<div class="row margin-top" style="page-break-after: always;">
   <div class="col-xs-12">
      <table width="100%" border="1">
         <thead>
            <tr>
               <th rowspan="2">#</th>
               <th rowspan="2">Name of Employee</th>
               <th colspan="5">Totals</th>
               <th colspan="32">Dates</th>
               <th rowspan="4">Indicate Leave Without Pay</th>
            </tr>
            <tr>
               <th>M/L</th>
               <th>S/L</th>
               <th>V/L</th>
               <th colspan="2">UNDERTIME</th>
               <th colspan="4">
                  <?php 
                     $day     = date("d",strtotime($new_week));
                     $month   = date("m",strtotime($new_week));
                     $year    = date("Y",strtotime($new_week));
                     echo chkDay($day,$month,$year); 
                  ?>
               </th>
               <th colspan="4">
                  <?php 
                     $day     = date("d",strtotime($new_week." + 1 Day"));
                     $month   = date("m",strtotime($new_week." + 1 Day"));
                     $year    = date("Y",strtotime($new_week." + 1 Day"));
                     echo chkDay($day,$month,$year); 
                  ?>
               </th>
               <th colspan="4">
                  <?php 
                     $day     = date("d",strtotime($new_week." + 2 Day"));
                     $month   = date("m",strtotime($new_week." + 2 Day"));
                     $year    = date("Y",strtotime($new_week." + 2 Day"));
                     echo chkDay($day,$month,$year); 
                  ?>
               </th>
               <th colspan="4">
                  <?php 
                     $day     = date("d",strtotime($new_week." + 3 Day"));
                     $month   = date("m",strtotime($new_week." + 3 Day"));
                     $year    = date("Y",strtotime($new_week." + 3 Day"));
                     echo chkDay($day,$month,$year); 
                  ?>
               </th>
               <th colspan="4">
                  <?php 
                     $day     = date("d",strtotime($new_week." + 4 Day"));
                     $month   = date("m",strtotime($new_week." + 4 Day"));
                     $year    = date("Y",strtotime($new_week." + 4 Day"));
                     echo chkDay($day,$month,$year); 
                  ?>
               </th>
               <th colspan="4">
                  <?php 
                     $day     = date("d",strtotime($new_week." + 5 Day"));
                     $month   = date("m",strtotime($new_week." + 5 Day"));
                     $year    = date("Y",strtotime($new_week." + 5 Day"));
                     echo chkDay($day,$month,$year); 
                  ?>
               </th>
               <th colspan="4">
                  <?php 
                     $day     = date("d",strtotime($new_week." + 6 Day"));
                     $month   = date("m",strtotime($new_week." + 6 Day"));
                     $year    = date("Y",strtotime($new_week." + 6 Day"));
                     echo chkDay($day,$month,$year); 
                  ?>
               </th>
               <th colspan="4">
                  <?php 
                     $day     = date("d",strtotime($new_week." + 7 Day"));
                     $month   = date("m",strtotime($new_week." + 7 Day"));
                     $year    = date("Y",strtotime($new_week." + 7 Day"));
                     echo chkDay($day,$month,$year); 
                  ?>
               </th>
            </tr>
            <tr>
               <th></th>
               <th class="text-left"></th>
               <th>DAYS</th>
               <th>DAYS</th>
               <th>DAYS</th>
               <th>HRS.</th>
               <th>MIN.</th>
               <?php
                  for ($a=1; $a <= 8; $a++) { 
                     echo '
                        <th>S/L</th>
                        <th>V/L</th>
                        <th colspan="2">UTIME</th>
                     ';
                  }
               ?>
            </tr>
            <tr>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <?php
                  for ($a=1; $a <= 8; $a++) { 
                     echo '
                        <th>D</th>
                        <th>D</th>
                        <th>H</th>
                        <th>M</th>
                     ';
                  }
               ?>
            </tr>
         </thead>
         <tbody>
            <?php
               if ($rsEmployees_week2) {
                  $count1 = 0;
                  while ($week1 = mysqli_fetch_assoc($rsEmployees_week2)) {
                     if ($p_filter_value == "0" || $p_filter_table == "") {
                        $emprefid = $week1["RefId"];
                     } else {
                        $emprefid   = $week1["EmployeesRefId"];
                     }
                     for ($a=0; $a <= 7; $a++) { 
                        $day     = date("d",strtotime($new_week." + ".$a." Day"));
                        $month   = date("m",strtotime($new_week." + ".$a." Day"));
                        $year    = date("Y",strtotime($new_week." + ".$a." Day"));
                        ${$month."_".$day."_SL"} = 0;
                        ${$month."_".$day."_VL"} = 0;
                        ${$day."_UT"} = 0;
                        ${$day."_UTHr"} = 0;
                        ${$day."_UTMin"} = 0;
                     }
                     $SL_Total      = 0;
                     $VL_Total      = 0;
                     $HR_UT_Total   = 0;
                     $MIN_UT_Total  = 0;
                     $UT_Total      = 0;

                     
                     
                     $empstatus  = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","EmpStatusRefId");
                     if ($cos == $empstatus) {
                        $LastName   = $week1["LastName"];
                        $FirstName  = $week1["FirstName"];
                        $MiddleName = $week1["MiddleName"];
                        $ExtName    = $week1["ExtName"];
                        $FullName   = $LastName.", ".$FirstName." ".substr($MiddleName, 0, 1).". ".$ExtName;
                        $where      = "WHERE EmployeesRefId = '$emprefid'";
                        $start      = FindFirst("dtr_process",$where." ".$where_start,"`VL_Days`,`Month`,`SL_Days`");
                        $end        = FindFirst("dtr_process",$where." ".$where_end,"`VL_Days`,`Month`,`SL_Days`");
                        if($start) {
                           $VL_start_day     = $start["VL_Days"];
                           $VL_start_month   = $start["Month"];
                           if ($VL_start_day != "") {
                              $VL_arr_start = explode("|", $VL_start_day);
                              foreach ($VL_arr_start as $start_key => $start_value) {
                                 if ($start_value != "") {
                                    if (isset(${$VL_start_month."_".$start_value."_VL"})) {
                                       ${$VL_start_month."_".$start_value."_VL"} = 1;
                                       $VL_Total++;
                                    }
                                 }
                              }   
                           }

                           $SL_start_day     = $start["SL_Days"];
                           $SL_start_month   = $start["Month"];
                           if ($SL_start_day != "") {
                              $SL_arr_start = explode("|", $SL_start_day);
                              foreach ($SL_arr_start as $start_key => $start_value) {
                                 if ($start_value != "") {
                                    if (isset(${$SL_start_month."_".$start_value."_SL"})) {
                                       ${$SL_start_month."_".$start_value."_SL"} = 1;
                                       $SL_Total++;
                                    }
                                 }
                              }   
                           }
                        }
                        if($end) {
                           $VL_end_day     = $end["VL_Days"];
                           $VL_end_month   = $end["Month"];
                           if ($VL_end_day != "") {
                              $VL_arr_end = explode("|", $VL_end_day);
                              foreach ($VL_arr_end as $end_key => $end_value) {
                                 if ($end_value != "") {
                                    if (isset(${$VL_end_month."_".$end_value."_VL"})) {
                                       ${$VL_end_month."_".$end_value."_VL"} = 1;
                                       $VL_Total++;
                                    }
                                 }
                              }   
                           }
                           $SL_end_day     = $end["SL_Days"];
                           $SL_end_month   = $end["Month"];
                           if ($SL_end_day != "") {
                              $SL_arr_end = explode("|", $SL_end_day);
                              foreach ($SL_arr_end as $end_key => $end_value) {
                                 if ($end_value != "") {
                                    if (isset(${$SL_end_month."_".$end_value."_SL"})) {
                                       ${$SL_end_month."_".$end_value."_SL"} = 1;
                                       $SL_Total++;
                                    }
                                 }
                              }   
                           }
                        }

                        for ($a=0; $a <= 7; $a++) { 
                           $day        = date("d",strtotime($new_week." + ".$a." Day"));
                           $month      = date("m",strtotime($new_week." + ".$a." Day"));
                           $year       = date("Y",strtotime($new_week." + ".$a." Day"));
                           $columns    = $day."_UT";
                           $where_dtr  = "WHERE EmployeesRefId = '$emprefid'";
                           $where_dtr  .= "AND Month = '$month' AND Year = '$year'";
                           $UT         = FindFirst("dtr_process",$where_dtr,$columns);
                           if ($UT > 0) {
                              ${$day."_UT"} = $UT;
                              $hr   = 0;
                              $min  = 0;
                              $UT   = ${$day."_UT"};
                              if(intval($UT) > 0) {
                                 $time_ut = rpt_HoursFormat($UT);
                                 if ($time_ut != "") {
                                    $ut_arr  = explode(":", $time_ut);
                                    $hr      = $ut_arr[0];
                                    $min     = $ut_arr[1];
                                    ${$day."_UTHr"} = $hr;
                                    ${$day."_UTMin"} = $min;
                                    $UT_Total+=$UT;
                                 }
                              }
                           }
                        }
                        if ($UT_Total > 0) {
                           $total_time_ut = rpt_HoursFormat($UT_Total);
                           if ($total_time_ut != "") {
                              $total_ut_arr     = explode(":", $total_time_ut);
                              $HR_UT_Total      = $total_ut_arr[0];
                              $MIN_UT_Total     = $total_ut_arr[1];
                           }
                        }
                        $count1++;
                        echo '
                           <tr>
                              <td class="text-center">'.$count1.'</td>
                              <td>'.strtoupper($FullName).'</td>
                              <td class="text-center">-</td>
                              <td class="text-center">'.rmvValue($SL_Total).'</td>
                              <td class="text-center">'.rmvValue($VL_Total).'</td>
                              <td class="text-center">'.rmvValue($HR_UT_Total).'</td>
                              <td class="text-center">'.rmvValue($MIN_UT_Total).'</td>
                        ';
                           for ($a=0; $a <= 7; $a++) { 
                              $day        = date("d",strtotime($new_week." + ".$a." Day"));
                              $month      = date("m",strtotime($new_week." + ".$a." Day"));
                              $year       = date("Y",strtotime($new_week." + ".$a." Day"));
                              echo '
                                 <td class="text-center">'.rmvValue(${$month."_".$day."_SL"}).'</td>
                                 <td class="text-center">'.rmvValue(${$month."_".$day."_VL"}).'</td>
                                 <td class="text-center">'.rmvValue(${$day."_UTHr"}).'</td>
                                 <td class="text-center">'.rmvValue(${$day."_UTMin"}).'</td>
                              ';
                           }
                        if (floatval($SL_Total) > 0 || floatval($VL_Total) > 0) {
                           $leave_remarks = "WITH Absences";
                        } else {
                           $leave_remarks = "NO Absences";
                        }
                        if (intval($UT_Total) > 0) {
                           $ut_remarks = "WITH UT";
                        } else {
                           $ut_remarks = "NO UT";
                        }
                        $remarks = $ut_remarks." AND ".$leave_remarks;
                        echo '<td>'.$remarks.'</td>';
                        echo '</tr>';  
                     }
                  }
               }
            ?>
         </tbody>
      </table>
   </div>
</div>