<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   include 'incSignatory.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName LIMIT 5";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
   $month      = getvalue("txtAttendanceMonth");
   $year       = getvalue("txtAttendanceYear");
   $apptstatus  = FindFirst("apptstatus","WHERE Code = 'SI'","RefId");
   $where      = " ORDER BY EffectivityDate DESC LIMIT 2";
   //AND Month(EffectivityDate) <='$month' AND ApptStatusRefId = '$apptstatus'
?>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style type="text/css">
         @media print {
            .header-space {margin-top: 25%;}
         }
      </style>
   </head>
   <body>
      <div class="container-fluid">
         <div class="row">
            <div class="col-xs-12">
               <?php
                  if ($rsEmployees) {
                     while ($emp_row = mysqli_fetch_assoc($rsEmployees)) {
                        $emprefid      = $emp_row["RefId"];
                        $FirstName     = $emp_row["FirstName"];
                        $LastName      = $emp_row["LastName"];
                        $MiddleName    = $emp_row["MiddleName"];
                        $ExtName       = $emp_row["ExtName"];
                        $CompanyRefId  = $emp_row["CompanyRefId"];
                        $MiddleInitial = substr($MiddleName, 0,1);
                        $FullName      = $FirstName." ".$MiddleInitial.". ".$LastName;
                        $where         = "WHERE EmployeesRefId = '$emprefid' ".$where;
                        $rs            = SelectEach("employeesmovement",$where);
                        $DivisionRefId = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","DivisionRefId");
                        $DivisionRefId = getRecord("division",$DivisionRefId,"Name");
                        $Address       = getRecord("company",$CompanyRefId,"Name");
                        if ($rs) {
                           $arr = array();
                           while ($row = mysqli_fetch_assoc($rs)) {
                              array_push($arr, $row);
                           }
                           json_encode($arr);
                           $New_EffectivityDate = $arr[0]["EffectivityDate"];
                           $New_SG              = getRecord("salarygrade",$arr[0]["SalaryGradeRefId"],"Code");
                           $New_SI              = getRecord("stepincrement",$arr[0]["StepIncrementRefId"],"Name");
                           $New_Salary          = $arr[0]["SalaryAmount"];
                           $Position            = getRecord("position",$arr[0]["PositionRefId"],"Name");
                           $PositionItem        = getRecord("positionItem",$arr[0]["PositionItemRefId"],"Name");

                           $Old_EffectivityDate = $arr[1]["ExpiryDate"];
                           $Old_SG              = getRecord("salarygrade",$arr[1]["SalaryGradeRefId"],"Code");
                           $Old_SI              = getRecord("stepincrement",$arr[1]["StepIncrementRefId"],"Name");
                           $Old_Salary          = $arr[1]["SalaryAmount"];
               ?>
               <div style="page-break-after: always;">
                  <div class="row header-space">
                     <div class="col-xs-12 text-center">
                        <h4>
                           <b>Notice of Salary Step Increment Due to Length of Service</b>
                        </h4>
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-12 text-right">
                        <?php echo date("F d, Y",time()); ?>
                     </div>
                  </div>  
                  <br>
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        <b><?php echo $FullName; ?></b>
                        <br>
                        <b><?php echo $DivisionRefId; ?></b>
                        <br>
                        <b><?php echo $Address; ?></b>
                     </div>
                  </div>  
                  <br>
                  <br>
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        Sir/Madam:
                     </div>
                  </div>
                  <br>
                  <div class="row margin-top">
                     <div class="col-xs-12" style="text-indent: 5%;">
                         Pursuant to Joint Civil Service Commission (CSC) and Department of Budget and Management (DBM) Circular No. 1 s. 1990 implementing Section 13 © of RA No. 6758,your salary as <?php echo $Position; ?> A is hereby adjusted effective <?php echo $New_EffectivityDate; ?> as shown below:
                     </div>
                  </div>
                  <br>
                  <div class="row margin-top">
                     <div class="col-xs-1"></div>
                     <div class="col-xs-7">
                        1. Adjusted Monthly Basic Salary Effective <b><?php echo date("F d, Y",strtotime($New_EffectivityDate)); ?></b>, under the new Salary Schedule: SG <b><u><?php echo $New_SG; ?></u></b> SI <b><u><?php echo $New_SI; ?></u></b>
                     </div>
                     <div class="col-xs-1 text-right">P</div>
                     <div class="col-xs-1 text-right">
                        <b><?php echo number_format($New_Salary,2); ?></b>
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-1"></div>
                     <div class="col-xs-7">
                        2. Actual Monthly Basic Salary as of <b><?php echo date("F d, Y",strtotime($Old_EffectivityDate)); ?></b>: SG <b><u><?php echo $Old_SG; ?></u></b> SI <b><u><?php echo $Old_SI; ?></u></b>
                     </div>
                     <div class="col-xs-1 text-right">P</div>
                     <div class="col-xs-1 text-right">
                        <b><?php echo number_format($Old_Salary,2); ?></b>
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-1"></div>
                     <div class="col-xs-7">
                        3. Monthly Salary Adjustment effective <b><?php echo date("F d, Y",strtotime($New_EffectivityDate)); ?></b> (1-2)
                     </div>
                     <div class="col-xs-1 text-right">P</div>
                     <div class="col-xs-1 text-right">
                        <b><?php echo number_format(($New_Salary - $Old_Salary),2); ?></b>
                     </div>
                  </div>
                  <br>
                  <br>
                  <div class="row margin-top">
                     <div class="col-xs-12" style="text-indent: 5%;">
                        This salary adjustment is subject to review and post-audit, and to appropriatere-adjustment and refund if found not in order.
                     </div>
                  </div>
                  <br>
                  <br>
                  <div class="row margin-top">
                     <div class="col-xs-8"></div>
                     <div class="col-xs-4">
                        Very truly yours,
                     </div>
                  </div>
                  <br>
                  <div class="row margin-top">
                     <div class="col-xs-8"></div>
                     <div class="col-xs-4 text-center">
                        ______________________________
                        <br>
                        Executive Director
                     </div>
                  </div>
                  <br>
                  <br>
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        Position Title: <b><u><?php echo $Position; ?></u></b>
                        <br>
                        Present Salary Grade: <b><u><?php echo $New_SG; ?></u></b>
                        <br>
                        Item No./Unique Item No., FY 2019 Personal Services Itemization and/or Plantilla of Personnel: <b><u><?php echo $PositionItem; ?></u></b>
                        <br><br>
                        CF: GSIS
                     </div>
                  </div>
               </div>
               <?php
                        } else {
                           echo "No Step Increment Found.";
                        }
                     }
                  }
               ?>
            </div>
         </div>
      </div>
   </body>
</html>