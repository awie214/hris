<?php
   require_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   require_once pathClass.'0620RptFunctions.e2e.php';
   require_once pathClass.'DTRFunction.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   //$whereClause .= " LIMIT 10";
   $table = "employees";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   
   $month = getvalue("txtAttendanceMonth");
   $year  = getvalue("txtAttendanceYear");
   $where = "WHERE Month(FiledDate) = '$month' AND Year(FiledDate) = '$year'";
   $where .= " AND Status = 'Approved'";
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row">
            <div class="col-xs-12">
               <?php
                  rptHeader("Employee's on Leave Per Month");
               ?>
            </div>
         </div>
         <br><br>
         <div class="row">
            <div class="col-xs-12">
               <table border="1" width="100%">
                  <thead>
                     <tr>
                        <th style="width: 10%;">#</th>
                        <th style="width: 20%;">Employee ID</th>
                        <th style="width: 20%;">Employee Name</th>
                        <th style="width: 15%;">Filed Date</th>
                        <th style="width: 15%;">Type of Monetization</th>
                        <th style="width: 10%;">VL Amount</th>
                        <th style="width: 10%;">SL Amount</th>
                     </tr>
                  </thead> 
                  <tbody>
                     <?php
                        $rs = SelectEach("employeesleavemonetization",$where);
                        if ($rs) {
                           $count = 0;
                           $check_name = "";
                           while ($row = mysqli_fetch_assoc($rs)) {
                              $count++;
                              $emprefid      = $row["EmployeesRefId"];
                              $whereClause   = "WHERE RefId = '$emprefid'";
                              $employee_row  = FindFirst("employees",$whereClause,"*");
                              if ($employee_row) {
                                 $LastName      = $employee_row["LastName"];
                                 $FirstName     = $employee_row["FirstName"];
                                 $MiddleName    = $employee_row["MiddleName"];
                                 $MiddleInitial = substr($employee_row["MiddleName"], 0,1);
                                 $ExtName       = $employee_row["ExtName"];
                                 $FullName      = $FirstName." ".$MiddleInitial." ".$LastName." ".$ExtName;
                                 $AgencyId      = $employee_row["AgencyId"];
                              } else {
                                 $FullName      = $AgencyId = "";
                              }

                              $FiledDate = $row["FiledDate"];
                              $monetization_type = $row["IsHalf"];
                              if ($monetization_type == 0) {
                                 $monetization_type = "Regular Monetization";
                              } else if ($monetization_type == 1) {
                                 $monetization_type = "50% Monetization";
                              }
                              echo '<tr>';
                                 echo '<td class="text-center">'.$count.'</td>';
                                 echo '<td class="text-center">'.$AgencyId.'</td>';
                                 if ($check_name == $FullName) {
                                    echo '<td>&nbsp;</td>';
                                 } else {
                                    echo '<td>'.$FullName.'</td>';
                                    $check_name = $FullName;
                                 }
                                 echo '<td class="text-center">'.date("F d, Y",strtotime($FiledDate)).'</td>';
                                 echo '<td class="text-center">'.$monetization_type.'</td>';
                                 echo '<td class="text-center">'.number_format($row["VLValue"],3).'</td>';
                                 echo '<td class="text-center">'.number_format($row["SLValue"],3).'</td>';
                              echo '</tr>';
                           }
                        }
                     ?>
                     <tr></tr>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </body>
</html>