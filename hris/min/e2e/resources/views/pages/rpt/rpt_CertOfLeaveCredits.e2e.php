<?php
   require_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   require_once pathClass.'0620RptFunctions.e2e.php';
   require_once pathClass.'DTRFunction.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   //$whereClause .= " LIMIT 10";
   $table = "employees";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   $from = getvalue("txtAttendanceDateFrom");
   $to   = getvalue("txtAttendanceDateTo");
   $month = getvalue("txtAttendanceMonth");
   $year  = getvalue("txtAttendanceYear");
   $from    = $year."-".$month."-01";
   $to      = $year."-".$month."-".cal_days_in_month(CAL_GREGORIAN,$month,$year);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $rs = SelectEach("employees",$whereClause);
            if (mysqli_num_rows($rs) > 0) {
               while ($row = mysqli_fetch_assoc($rs)) {
                  rptHeader(getRptName(getvalue("drpReportKind")));
                  $EmployeesRefId = $row["RefId"];
                  $CompanyRefId   = $row["CompanyRefId"];
                  $BranchRefId    = $row["BranchRefId"];
                  $LastName       = $row["LastName"];
                  $FirstName      = $row["MiddleName"];
                  $MiddleName     = $row["FirstName"];
                  $FullName       = $row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"];
                  $where          = "WHERE CompanyRefId = $CompanyRefId";
                  $where         .= " AND BranchRefId = $BranchRefId";
                  $where         .= " AND EmployeesRefId = $EmployeesRefId";
                  $wherevl        = $where." AND NameCredits = 'VL'";
                  $wherevl       .= " AND EffectivityYear = ".date("Y",time());
                  $wheresl        = $where." AND NameCredits = 'SL'";
                  $wheresl       .= " AND EffectivityYear = ".date("Y",time());

                  if ($p_filter_value == "0" || $p_filter_table == "") {
                     $emprefid = $row["RefId"];
                  } else {
                     $emprefid   = $row["EmployeesRefId"];
                  }
                  $leave = computeCredit($emprefid,"01",$month,$year);
                  if ($leave) {
                     $vl = $leave["VL"];
                     $sl = $leave["SL"];
                  } else {
                     $vl = $sl = 0;
                  }
         ?>
          <div class="row">
            <div class="col-xs-1"></div>
            <div class="col-xs-10">
              <p style="text-indent: 30px;">
                This is to certify that <span style="text-transform: uppercase;"><b><?php echo ("$FullName");?>,</b></span>Commissioner of the Office of the Chairman, has an accumulated Leave Credit Balance of <?php echo $vl+$sl; ?> days as of <?php echo date("d F Y",time()); ?>, broken down as follows: 
               </p>
            </div>
          </div>
          <div class="row margin">
            <div class="col-xs-3"></div>
            <div class="col-xs-2">
              <div class="row">Vacation Leave</div>
              <div class="row margin-top">Sick Leave</div>
              <div class="row margin-top">
                <b><label>TOTAL</label></b>
              </div>
            </div>
            <div class="col-xs-1">
              <div class="row">:</div>
              <div class="row margin-top">:</div>
              <div class="row margin-top">:</div>
            </div>
            <div class="col-xs-2">
              <div class="row">
                 <?php echo $vl; ?>
              </div>
              <div class="row margin-top">
                 <?php echo $sl; ?>
              </div>
              <div class="row margin-top">
                <label>
                   <b><?php echo $vl+$sl; ?></b>
                </label>
              </div>
            </div>
          </div>
          <div class="row margin">
            <div class="col-xs-1"></div>
            <div class="col-xs-10">
              <p>
                This certification is being issued upon the request of <b>Ms/Mr. <?php echo $LastName; ?></b> for whatever legal purpose it may serve.
              </p>
            </div>
          </div>
          <div class="row margin">
            <div class="col-xs-1"></div>
            <div class="col-xs-10">
              <p>
                Issued this <?php echo date("F d, Y",time()); ?>.
              </p>
            </div>
          </div>
          <div class="row margin">
            <div class="col-xs-8"></div>
            <div class="col-xs-4" align="center">
              <p>
                
              </p>
            </div>
          </div>

        <?php
               }
            }
         ?>



      </div>
   </body>
</html>