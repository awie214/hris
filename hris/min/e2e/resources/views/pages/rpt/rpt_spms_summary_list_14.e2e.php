<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $period     = getvalue("period");
   $division   = getvalue("division");
   $office     = getvalue("office");
   $year       = getvalue("year");
   $where = "WHERE RefId > 0";
   if ($period != "") {
      $where .= " AND Semester = '$period'";
   }
   if (intval($division) > 0) {
      $where .= " AND DivisionRefId = '$division'";
   }
   if (intval($office) > 0) {
      $where .= " AND OfficeRefId = '$office'";
   }
   if (intval($year) > 0) {
      $where .= " AND YearPerformed = '$year'";
   }
   $count = 0;
   if (intval($office) > 0) {
      $where_office  = " WHERE RefId = '$office'";  
      $office_name   = getRecord("office",$office,"Name");
   } else {
      $where_office  = "";
      $office_name   = "";
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style type="text/css">
         
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader("SUMMARY LIST OF INDIVIDUAL PERFORMANCE RATINGS");
         ?>
         <br><br>
         <div class="row">
            <div class="col-xs-12">
               <label>Regulation Area <b><u><?php echo $office_name; ?></u></b></label>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-12 text-center">
               <label>Performance Assessment _______________________________</label>
            </div>
         </div>
         <br><br>
         <table width="100%">
            <?php
               $office_rs = SelectEach("office",$where_office);
               if ($office_rs) {
                  while ($office_row = mysqli_fetch_assoc($office_rs)) {
                     $office     = $office_row["Name"];
                     $office_id  = $office_row["RefId"];
                     $where      = "WHERE OfficeRefId = '$office_id'";
                     $where      .= " AND DivisionRefId = 0";
                     $where      .= " AND (EmpStatusRefId = 1 OR EmpStatusRefId = 2 OR EmpStatusRefId = 3)";
                     $where      .= " ORDER BY SalaryAmount DESC";

                     $empinfo    = SelectEach("empinformation",$where);
                     echo '<tr>';
                        echo '
                           <td rowspan="2">
                              '.$office.'
                           </td>
                           <td colspan="2" class="text-center">Rating</td>
                        ';
                     echo '</tr>';
                     echo '
                        <tr>
                           <td class="text-center">Numerical</td>
                           <td class="text-center">Adjectival</td>
                        </tr>
                     ';
                     if ($empinfo) {
                        while ($empinfo_row = mysqli_fetch_assoc($empinfo)) {
                           $emprefid   = $empinfo_row["EmployeesRefId"];
                           $fld        = "`LastName`, `FirstName`, `MiddleName`";
                           $emp_row    = FindFirst("employees","WHERE RefId = '$emprefid'",$fld);
                           if ($emp_row) {
                              $LastName   = $emp_row["LastName"];
                              $MiddleName = $emp_row["MiddleName"];
                              $FirstName  = $emp_row["FirstName"];
                              $FullName   = $FirstName." ".substr($MiddleName, 0,1)." ".$LastName;
                              echo '
                                 <tr>
                                    <td>'.$FullName.'</td>
                                    <td></td>
                                    <td></td>
                                 </tr>
                              ';
                           }
                        }
                     }
                     $div = SelectEach("division","");
                     if ($div) {
                        while ($div_row = mysqli_fetch_assoc($div)) {
                           $div_id     = $div_row["RefId"];
                           $div_name   = $div_row["Name"];
                           $where_dept = "WHERE OfficeRefId = '$office_id'";
                           $where_dept .= " AND DivisionRefId = '$div_id'";
                           $where_dept .= " ORDER BY SalaryAmount DESC";
                           $empinfo_dept = SelectEach("empinformation",$where_dept);
                           if ($empinfo_dept) {
                              echo '<tr>';
                                 echo '
                                    <td style="background:cyan;" colspan=3>
                                       <b>'.$div_name.'</b>
                                    </td>
                                 ';
                              echo '</tr>';
                              while ($dept_row = mysqli_fetch_assoc($empinfo_dept)) {
                                 $emprefid   = $dept_row["EmployeesRefId"];
                                 $fld        = "`LastName`, `FirstName`, `MiddleName`";
                                 $emp_row    = FindFirst("employees","WHERE RefId = '$emprefid'",$fld);
                                 if ($emp_row) {
                                    $LastName   = $emp_row["LastName"];
                                    $MiddleName = $emp_row["MiddleName"];
                                    $FirstName  = $emp_row["FirstName"];
                                    $FullName   = $FirstName." ".substr($MiddleName, 0,1)." ".$LastName;
                                    echo '
                                       <tr>
                                          <td>'.$FullName.'</td>
                                          <td></td>
                                          <td></td>
                                       </tr>
                                    ';
                                 }

                              }
                           }
                        }
                     }
                     echo '
                        <tr>
                           <td colspan="3" style="background:gray;">&nbsp;</td>
                        </tr>
                     ';
                  }
                  
               }
            ?>
         </table>
      </div>
   </body>
</html>
