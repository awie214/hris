<?php
	include_once 'constant.e2e.php';
	require_once pathClass.'0620functions.e2e.php';
	$refid   = getvalue("refid");
	$row 		= FindFirst("spms_performance","WHERE RefId = '$refid'","*");
	if ($row) {
		$Quarter   		= $row["Quarter"];
		$Year 			= $row["Year"];
		$office 			= $row["OfficeRefId"];
		$office_name 	= getRecord("office",$office,"Name");
		$refid 			= $row["RefId"];
	}
?>
<!DOCTYPE html>
<html>
<head>
	<?php
		include_once 'pageHEAD.e2e.php';
	?>
	<title></title>
	<style type="text/css">
		thead {
			text-transform: uppercase;
		}
		.border-l-b-r {
			border-bottom: 2px solid black;
			border-left: 2px solid black;
			border-right: 2px solid black;
		}
		td {
			padding: 2px;
			border: 1px solid black;
		}
		body {
			font-size: 8pt;
		}
	</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<table width="100%">
					<tr>
						<td style="width: 8.33%; padding: 0px; border: none;"></td>
						<td style="width: 8.33%; padding: 0px; border: none;"></td>
						<td style="width: 8.33%; padding: 0px; border: none;"></td>
						<td style="width: 8.33%; padding: 0px; border: none;"></td>
						<td style="width: 8.33%; padding: 0px; border: none;"></td>
						<td style="width: 8.33%; padding: 0px; border: none;"></td>
						<td style="width: 8.33%; padding: 0px; border: none;"></td>
						<td style="width: 8.33%; padding: 0px; border: none;"></td>
						<td style="width: 8.33%; padding: 0px; border: none;"></td>
						<td style="width: 8.33%; padding: 0px; border: none;"></td>
						<td style="width: 8.33%; padding: 0px; border: none;"></td>
						<td style="width: 8.33%; padding: 0px; border: none;"></td>
					</tr>
					<tr>
						<td colspan="12" class="text-center" style="border-bottom: none;">
							<b>Performance Monitoring And Coaching Journal</b>
						</td>
					</tr>
					<tr>
						<td colspan="8" rowspan="5" style="border-top: none;">&nbsp;</td>
						<td>&nbsp;</td>
						<td colspan="3">
							<b>QUARTER</b>
						</td>
					</tr>
					<tr>
						<td></td>
						<td colspan="3">
							<b>
								1<sup>st</sup>
							</b>
						</td>
					</tr>
					<tr>
						<td></td>
						<td colspan="3">
							<b>
								2<sup>nd</sup>
							</b>
						</td>
					</tr>
					<tr>
						<td></td>
						<td colspan="3">
							<b>
								3<sup>rd</sup>
							</b>
						</td>
					</tr>
					<tr>
						<td></td>
						<td colspan="3">
							<b>
								4<sup>th</sup>
							</b>
						</td>
					</tr>
					<tr>
						<td colspan="12">
							<b>Department/Regulatory Area:&nbsp;&nbsp;&nbsp;</b>
							<?php echo $office_name; ?>
						</td>
					</tr>
					<tr>
						<td colspan="12">
							<b>Department Manager:&nbsp;&nbsp;&nbsp;</b>
						</td>
					</tr>
					<tr>
						<td colspan="12">
							<b>Number of Personnel in the Regulation Area:&nbsp;&nbsp;&nbsp;</b>
						</td>
					</tr>
					<tr>
						<td colspan="12">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="12">&nbsp;</td>
					</tr>
					<tr>
						<td rowspan="3" class="text-center" colspan="2">
							<b>Activity</b>
						</td>
						<td class="text-center" colspan="8">
							<b>Mechanisms</b>
						</td>
						<td rowspan="3" class="text-center" colspan="2">
							<b>Remarks</b>
						</td>
					</tr>
					<tr>
						<td class="text-center" colspan="4"><b>Meeting</b></td>
						<td rowspan="2" class="text-center" colspan="2"><b>Memo</b></td>
						<td class="text-center" colspan="2"><b>Others</b></td>
					</tr>
					<tr>
						<td class="text-center" colspan="2"><b>One-in-One</b></td>
						<td class="text-center" colspan="2"><b>Group</b></td>
						<td class="text-center" colspan="2"><b>(Pls. Specify)</b></td>
					</tr>
					<tr>
						<td colspan="12">
							<b>Monitoring</b>
						</td>
					</tr>
					<?php
						$monitoring_rs = SelectEach("performance_details","WHERE performance_id = '$refid' AND type = 'Monitoring'");
						if ($monitoring_rs) {
							while ($mrow = mysqli_fetch_assoc($monitoring_rs)) {
								echo '
									<tr>
										<td colspan="2">'.$mrow["activity"].'</td>
										<td colspan="2">'.$mrow["oneonone"].'</td>
										<td colspan="2">'.$mrow["group"].'</td>
										<td colspan="2">'.$mrow["memo"].'</td>
										<td colspan="2">'.$mrow["others"].'</td>
										<td colspan="2">'.$mrow["remarks"].'</td>
									</tr>
								';
							}
						} else {
							echo '
								<tr>
									<td colspan="12">
										&nbps;
									</td>
								</tr>
							';
						}
					?>
					<tr>
						<td colspan="12">
							<b>Coaching</b>
						</td>
					</tr>
					<?php
						$coaching_rs = SelectEach("performance_details","WHERE performance_id = '$refid' AND type = 'Coaching'");
						if ($coaching_rs) {
							while ($crow = mysqli_fetch_assoc($coaching_rs)) {
								echo '
									<tr>
										<td colspan="2">'.$crow["activity"].'</td>
										<td colspan="2">'.$crow["oneonone"].'</td>
										<td colspan="2">'.$crow["group"].'</td>
										<td colspan="2">'.$crow["memo"].'</td>
										<td colspan="2">'.$crow["others"].'</td>
										<td colspan="2">'.$crow["remarks"].'</td>
									</tr>
								';
							}
						} else {
							echo '
								<tr>
									<td colspan="12">
										&nbps;
									</td>
								</tr>
							';
						}
					?>
					<tr>
						<td colspan="12">
							<b>
								Please indicate the date in the appropriate box when the monitoring was conducted.
							</b>
						</td>
					</tr>
					<tr>
						<td colspan="3" valign="top">
							Conducted by:
							<br><br><br><br>
						</td>
						<td colspan="3" valign="top" class="text-center">
							Date:
						</td>
						<td colspan="3" valign="top">
							Noted by:
							<br><br><br><br>
						</td>
						<td colspan="3" valign="top" class="text-center">
							Date:
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</body>
</html>