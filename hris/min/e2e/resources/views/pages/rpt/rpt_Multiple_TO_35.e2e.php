<?php
   include_once 'pageHEAD.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   $control_number   = getvalue("control_number");
   $where            = "WHERE ControlNumber = '$control_number'";
   $row              = FindFirst("employeesauthority",$where,"*");
   $rs               = SelectEach("employeesauthority",$where);
   if ($row) {
         $Signatory1       = $row["Signatory1"];
         $Signatory2       = $row["Signatory2"];
         $Signatory3       = $row["Signatory3"];

         $s1_row           = FindFirst("signatories","WHERE RefId = '$Signatory1'","*");
         $s2_row           = FindFirst("signatories","WHERE RefId = '$Signatory2'","*");

         $s1_name          = $s1_row["Name"];
         $s1_position      = getRecord("position",$s1_row["PositionRefId"],"Name");

         $s2_name          = $s2_row["Name"];
         $s2_position      = getRecord("position",$s2_row["PositionRefId"],"Name");


   		$authority 		= getRecord("absences",$row["AbsencesRefId"],"Name");
   		$authority 		= strtoupper($authority);
   		$Purpose 		= $row["Remarks"];
   		$Remarks 		= $authority." - ".$row["Remarks"];
   		$Destination 	= $row["Destination"];
   		$emprefid 		= $row["EmployeesRefId"];
   		$FiledDate 		= $row["FiledDate"];
   		$TO_no			= $row["ControlNumber"];
   		$employees = FindFirst("employees","WHERE RefId = '$emprefid'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
   		if ($employees) {
   			$FirstName = $employees["FirstName"];
   			$LastName = $employees["LastName"];
   			$MiddleName = $employees["MiddleName"];
   			$ExtName = $employees["ExtName"];
   			$FullName = $LastName.", ".$FirstName." $ExtName ".$MiddleName;
   		} else {
   			$FullName = "&nbsp;";
   		}
   		$empinformation = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
   		if ($empinformation) {
   			$OfficeRefId = getRecord("office",$empinformation["OfficeRefId"],"Name");
   			$DivisionRefId = getRecord("division",$empinformation["DivisionRefId"],"Name");
   			$PositionRefId = getRecord("position",$empinformation["PositionRefId"],"Name");
   		} else {
   			$OfficeRefId = $DivisionRefId = $PositionRefId = "&nbsp;";
   		}
   		if ($row["Whole"] == 1) {
   			$start_time = $end_time = "Whole Day";
   		} else {
   			$start_time = convertToHoursMins($row["FromTime"]);
   			$end_time = convertToHoursMins($row["ToTime"]);
   		}
   		$from = $row["ApplicationDateFrom"];
   		$to = $row["ApplicationDateTo"];
   		if ($from == $to) {
   			$date = date("F d, Y",strtotime($from));
   		} else {
   			$date = date("F d, Y",strtotime($from))." to ".date("F d, Y",strtotime($to));;
   		}   		
   }
   	$user = getvalue("hUserRefId");
   	$user_row = FindFirst("employees","WHERE RefId = '$user'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
   	if ($user_row) {
      $user_LastName   = $user_row["LastName"];
      $user_FirstName  = $user_row["FirstName"];
   	} else {
      $user_FirstName = $user_LastName = "";
   	}
?>
<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		td {
			border: 2px solid black;
			vertical-align: top;
			padding: 5px;
			font-size: 9pt;
		}
		.data {
			font-size: 10pt;
			text-transform: uppercase;
		}
	</style>
</head>
<body>
	<div class="container-fluid rptBody">
		<div style="page-break-after: always;">
	        <?php
	            rptHeader("Travel Order","HRDMS-T-003");
	        ?>
	        <div class="row margin-top">
	        	<div class="col-xs-12">
	        		<b>TRAVEL ORDER NO: <?php echo $TO_no; ?></b>
	        	</div>
	        </div>
	        <br>
	        <div class="row margin-top">
	        	<div class="col-xs-12">
	        		1. The following personnel of this Agency:<br> <b><u>
                  <?php 
                     while ($n_row = mysqli_fetch_assoc($rs)) {
                        $emprefid = $n_row["EmployeesRefId"];
                        $employees = FindFirst("employees","WHERE RefId = '$emprefid'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
                        if ($employees) {
                           $FirstName = $employees["FirstName"];
                           $LastName = $employees["LastName"];
                           $MiddleName = $employees["MiddleName"];
                           $ExtName = $employees["ExtName"];
                           $FullName      = $FirstName." ".substr($MiddleName, 0, 1).". ".$LastName." ".$ExtName;
                        } else {
                           $FullName = "&nbsp;";
                        }
                        echo $FullName."<br> ";
                     }
                  ?>
               </u></b> are hereby directed to proceed to 
	        		<b><u><?php echo $Destination; ?></u></b> On <b><u><?php echo $date; ?></u></b> for the purpose of 
	        		<b><u><?php echo $Purpose; ?></u></b>
	        		<br>
	        		2. Air / Land travel authorized.
	        		<br>
	        		3. Expenses to be incurred will be in accordance with R.A. No. 3847 per approved Itinerary of Travel and is chargeable against the National Commission on the Role of Filipino Women subject to the availability of funds and the usual accounting and auditing regulations.
	        		<br>
	        		4. Upon completion of travel they shall submit a Certificate of Appearance and Certificate of Travel Completed.
	        	</div>
	        </div>
	        <br>
	        <br>
	        <div class="row margin-top">
	        	<div class="col-xs-6">
	        		RECOMMENDING APPROVAL:
	        		<br><br><br>
	        		<b>
                  <?php echo $s1_name; ?>
               </b>
	        		<br>
	        		<?php echo $s1_position; ?>
	        	</div>
	        	<div class="col-xs-6">
	        		APPROVED:
	        		<br><br><br>
	        		<b>
                  <?php echo $s2_name; ?>
               </b>
               <br>
               <?php echo $s2_position; ?>
	        	</div>
	        </div>
	        <?php spacer(40); ?>
         	<div class="row margin-top" style="display: none;">
            	<div class="col-xs-1"></div>
            	<div class="col-xs-10" style="border: 1px solid black; padding: 5px;">
               		The only CONTROLLED copy of this document is the online version maintained in the HRIS. The user must ensure that this or any other copy of a controlled document is current and complete prior to use. The MASTER copy of this document is with the AFD-HRMD Section. This document is UNCONTROLLED when downloaded and printed.
	               	<br>
	               	<br>
	               	Printed on <?php echo date("F d,Y",time()); ?>
	               	<br>
	               	Printed by <?php echo $user_LastName.", ".$user_FirstName; ?>
            	</div>
         	</div>
	    </div>
    </div>
</body>
</html>