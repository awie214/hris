<?php
   require_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   require_once pathClass.'0620RptFunctions.e2e.php';
   require_once pathClass.'DTRFunction.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $whereClause .= " LIMIT 10";
   $table = "employees";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   $from    = getvalue("txtAttendanceDateFrom");
   $to      = getvalue("txtAttendanceDateTo");
   $month   = getvalue("txtAttendanceMonth");
   $year    = getvalue("txtAttendanceYear");
   $from    = $year."-".$month."-01";
   $to      = $year."-".$month."-".cal_days_in_month(CAL_GREGORIAN,$month,$year);
   if ($dbg) {
      echo $whereClause;
   }
   $WorkDayConversion   = file_get_contents(json."WorkDayConversion.json");
   $WorkDayEQ           = json_decode($WorkDayConversion, true);
   $PerDayHours         = "10";
   $arr_month =[
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
   ];
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid">
      <?php
         $rsEmployees = SelectEach("employees",$whereClause);
         if ($rsEmployees) {
            while ($row = mysqli_fetch_assoc($rsEmployees)) { 
               $new_VL = 0;
               $new_SL = 0;

               $EmployeesRefId = $row["RefId"];
               $CompanyRefId   = $row["CompanyRefId"];
               $BranchRefId    = $row["BranchRefId"];
               $emp_row = FindFirst("empinformation","WHERE EmployeesRefId = ".$row["RefId"],"*");
               if ($emp_row) {
                  $appt    = $emp_row["ApptStatusRefId"];
                  $appt    = getRecord("apptstatus",$appt,"Name");
                  $div     = getRecord("division",$emp_row["DivisionRefId"],"Name");
                  $hired   = date("d F Y",strtotime($emp_row["HiredDate"]));
                  $worksched = $emp_row["WorkScheduleRefId"];
               } else {
                  $appt    = "";
                  $div     = "";
                  $hired   = "";
                  $worksched = "";
               }
               if ($worksched != "") {
                  rptHeader(getRptName("rptLeave_Card"));
      ?>               
               <div class="row" style="padding:10px;">
                  <div class="col-xs-6">
                     <?php 
                        echo "NAME : ".$row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"];  
                     ?>
                  </div>
                  <div class="col-xs-6 text-right">
                     <?php echo "1st Day of Service : ".$hired; ?>
                  </div>
               </div>
               <?php
                  $emprefid   = $EmployeesRefId;
                  $curr_month = date("m",time());
                  include 'inc/leave_card_1000.php';
               ?>
               <p>
                  This is a system generated report. Signature is not required.
               </p>
         <?php 
               }
            }
         }
         ?>
      </div>
   </body>
</html>