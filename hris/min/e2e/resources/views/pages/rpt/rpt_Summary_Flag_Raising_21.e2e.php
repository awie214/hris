<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <table style="width: 100%;">
            <thead>
               <tr>
                  <td colspan="9">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                  </td>
               </tr>
               <tr class="colHEADER">
                  <th>Division</th>
                  <th>Month</th>
                  <th>Required No. of Pax</th>
                  <th>No. of Employees attended</th>
                  <th>% of Attendance</th>
               </tr>
            </thead>
            <tbody>
               <?php 
                  $division_rs = SelectEach("division","");
                  if ($division_rs) {
                     while ($division_row = mysqli_fetch_assoc($division_rs)) {
                        $division = $division_row["Name"];
                        echo '<tr>
                                 <td rowspan="13" class="text-center">'.$division.'</td>
                                 <td>JANUARY</td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                              </tr>
                        ';
                        for ($i=2; $i <= 12 ; $i++) { 
                           if ($i <= 9) $i = "0".$i;
                           $new_date = date("Y",time())."-".$i."-01";
                           echo '
                              <tr>
                                 <td>'.strtoupper(date("F",strtotime($new_date))).'</td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                              </tr>
                           ';
                        }
                        echo '<tr>
                                 <td>TOTAL</td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                              </tr>
                        ';
                     }
                  }
               ?>
            </tbody>
         </table>
      </div>
   </body>
</html>