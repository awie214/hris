<?php
	// We'll be outputting an excel file
	header('Content-type: application/vnd.ms-excel');

	// It will be called file.xls
	header('Content-Disposition: attachment; filename="file.xls"');

	function getRecord($table,$RefId,$fields){
     	include '../conn.e2e.php';
     	if ($RefId) {
        	$sql = "SELECT `$fields` FROM `".strtolower($table)."` where RefId = $RefId";
        	$result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
        	$row = mysqli_fetch_assoc($result);
        	mysqli_close($conn);
        	$numrow = mysqli_num_rows($result);
        	if ($numrow <= 0) {
           	return "";
        	} else {
           	if ($fields == "*") return $row;
           	else {
              	$value = str_replace("(","&#40;",$row[$fields]);
              	$value = str_replace(")","&#41;",$value);
              	$value = str_replace('"',"&#34;",$value);
              	return $value;
           	}
        	}
     	} else return "";
   }

   function FindFirst($table,$whereClause,$fld) {
      include '../conn.e2e.php';
      if ($fld == "*") {
         $sql = "SELECT * FROM `".strtolower($table)."` ".$whereClause." order by refid limit 1";
      } else if (stripos($fld,",") > 0) {
         $sql = "SELECT $fld FROM `".strtolower($table)."` ".$whereClause." order by refid limit 1";
      } else {
         $sql = "SELECT $fld FROM `".strtolower($table)."` ".$whereClause." order by refid limit 1";
      }
      $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
      $row = mysqli_fetch_assoc($result);
      $numrow = mysqli_num_rows($result);
      mysqli_close($conn);
      if ($numrow <= 0) {
         return false;
      } else {
         if ($fld == "*" || stripos($fld,",") > 0) {
            return $row;
         } else {
            return $row[$fld];
         }
      }
   }
   function computeAge($bday,$param = "today"){
      return date_diff(date_create($bday), date_create($param))->y;
   }

   function SelectEach($table,$whereClause) {
      include '../conn.e2e.php';
      $sql = "SELECT * FROM `".strtolower($table)."` ".$whereClause;
      $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
      mysqli_close($conn);
      if (mysqli_num_rows($result) > 0) return $result;
      else return false;
   }

	$where = "WHERE employees.CompanyRefId = 35";
	$where .= " AND employees.BranchRefId = 1";
	$where .= " AND (Inactive != 1 OR Inactive IS NULL)";
	$where .= " ORDER BY LastName";
	$table = "employees";
	$rsEmployees = SelectEach($table,$where);
?>
<!DOCTYPE html>
<html>
   <head>
      <style type="text/css">
         @media print {
            table {
               font-size: 6pt !important;
            }
         }
         th {
            text-align: center; 
            background: gray;
            border: 1px solid black;
         }
         td {
            padding: 5px;
            vertical-align: top;
         }
         .text-center {
         	text-align: center;
         }
      </style>
   </head>
   <body>
      <table width="1000%" border="1">
         <thead>
            <tr>
               <th rowspan="2">#</th>
               <th colspan="4">
                  INCUMBENT
               </th>
               <th rowspan="2">DIVISION</th>
               <th rowspan="2">SECTION</th>
               <th rowspan="2">POSITION TITLE</th>
               <th rowspan="2">ITEM NUMBER </th>
               <th rowspan="2">
                  DATE POSITION WAS CREATED
                  <br>
                  (If not able to indicate past creations just include since 2013 or 2014 up-to-date)
               </th>
               <th rowspan="2">SG</th>
               <th rowspan="2">MONTHLY SALARY OR COST OF SERVICES (Per SG)</th>
               <th rowspan="2">DESIGNATION (AS APPROPRIATE)</th>
               <th rowspan="2">DATE OF DESIGNATION</th>
               <th rowspan="2">SPECIAL ORDER NO.</th>
               <th rowspan="2">Email Address</th>
               <th rowspan="2">STATUS OF EMPLOYMENT</th>
               <th rowspan="2">MODE OF ACCESSION</th>
               <th rowspan="2">DATE FILLED UP</th>
               <th rowspan="2">DATE OF ORIGINAL APPOINTMENT</th>
               <th rowspan="2">DATE OF LAST PROMOTION</th>
               <th rowspan="2">ENTRY DATE IN PCW (First day in service)</th>
               <th rowspan="2">ELIGIBILITY (Specify RA1080 if SW,CPA, etc.)</th>
               <th rowspan="2">HIGHEST EDUCATION COMPLETED</th>
               <th rowspan="2">DEGREE AND COURSE (Specify)</th>
               <th rowspan="2">MASTERS OR DOCTORAL DEGREE (Specify)</th>
               <th rowspan="2">DATE OF BIRTH (MM/DD/YYYY)</th>
               <th rowspan="2">AGE</th>
               <th rowspan="2">SEX</th>
               <th rowspan="2">CIVIL STATUS</th>
               <th rowspan="2">BIR TIN NO.</th>
               <th rowspan="2">RESIDENTIAL ADDRESS</th>
               <th rowspan="2">PERMANENT ADDRESS</th>
               <th rowspan="2">INDICATE WHETHER "SOLO PARENT"</th>
               <th rowspan="2">INDICATE WHETHER "SENIOR CITIZEN"</th>
               <th rowspan="2">INDICATE WHETHER "PWD"</th>
               <th rowspan="2">INDICATE IF MEMBER OF ANY INDIGENOUS GROUP</th>
               <th rowspan="2">CITIZESHIP</th>
               <th rowspan="2">CONTACT NOS.</th>
               <th rowspan="2">EMAIL ADDRESS</th>
               <th rowspan="2">FORMER INCUMBENT</th>
               <th rowspan="2">MODE OF SEPARATION</th>
               <th rowspan="2">DATE VACATED</th>
               <th rowspan="2">REMARKS / STATUS OF VACANT POSITION</th>
            </tr>
            <tr>
               <th>LAST NAME</th>
               <th>FIRST NAME</th>
               <th>MIDDLE NAME</th>
               <th>EXT.</th>
            </tr>
         </thead>
         <tbody>
            <?php
               if ($rsEmployees) {
                  $count = 0;
                  while ($row = mysqli_fetch_assoc($rsEmployees)) {
                     $count++;
                     $emprefid                     = $row["RefId"];
                     $LastName                     = $row["LastName"];
                     $FirstName                    = $row["FirstName"];
                     $MiddleName                   = $row["MiddleName"];
                     $ExtName                      = $row["ExtName"];
                     $Sex                          = $row["Sex"];
                     $CivilStatus                  = $row["CivilStatus"];
                     $TIN                          = $row["TIN"];
                     $BirthDate                    = $row["BirthDate"];
                     $EmailAdd                     = $row["EmailAdd"];
                     $ContactNo                    = $row["MobileNo"];
                     $ResiAddCityRefId             = getRecord("city",$row["ResiAddCityRefId"],"Name");
                     $ResiAddProvinceRefId         = getRecord("province",$row["ResiAddProvinceRefId"],"Name");
                     $ResiHouseNo                  = $row["ResiHouseNo"];
                     $ResiStreet                   = $row["ResiStreet"];
                     $ResiSubd                     = $row["ResiSubd"];
                     $ResiBrgy                     = $row["ResiBrgy"];
                     $PermanentHouseNo             = $row["PermanentHouseNo"];
                     $PermanentStreet              = $row["PermanentStreet"];
                     $PermanentSubd                = $row["PermanentSubd"];
                     $PermanentBrgy                = $row["PermanentBrgy"];
                     $PermanentAddCityRefId        = getRecord("city",$row["PermanentAddCityRefId"],"Name");
                     $PermanentAddProvinceRefId    = getRecord("province",$row["PermanentAddProvinceRefId"],"Name");

                     switch ($CivilStatus) {
                        case 'Ma':
                           $CivilStatus = "Married";
                           break;
                        case 'Si':
                           $CivilStatus = "Single";
                           break;
                     }

                     $where                        = "WHERE EmployeesRefId = '$emprefid'";
                     $empinfo                      = FindFirst("empinformation",$where,"*");
                     if ($empinfo) {
                        $HiredDate                 = $empinfo["HiredDate"];
                        $AgencyRefId               = getRecord("agency",$empinfo["AgencyRefId"],"Name");
                        $PositionItemRefId         = getRecord("positionitem",$empinfo["PositionItemRefId"],"Name");
                        $PositionRefId             = getRecord("position",$empinfo["PositionRefId"],"Name");
                        $SalaryGradeRefId          = getRecord("salarygrade",$empinfo["SalaryGradeRefId"],"Name");
                        $EmpStatusRefId            = getRecord("empstatus",$empinfo["EmpStatusRefId"],"Name");
                        $OfficeRefId               = getRecord("office",$empinfo["OfficeRefId"],"Name");
                        $DepartmentRefId           = getRecord("department",$empinfo["DepartmentRefId"],"Name");
                        $DivisionRefId             = getRecord("division",$empinfo["DivisionRefId"],"Name");
                        $SalaryAmount              = number_format(floatval($empinfo["SalaryAmount"]),2);
                     }

                     $pdsq   = FindFirst("employeespdsq",$where,"*");

            ?>
            <tr>
               <td class="text-center">
                  <?php echo $count; ?>
               </td>
               <td>
                  <?php echo $LastName; ?>
               </td>
               <td>
                  <?php echo $FirstName; ?>
               </td>
               <td>
                  <?php echo $MiddleName; ?>
               </td>
               <td>
                  <?php echo $ExtName; ?>
               </td>
               <td>
                  <?php echo $DivisionRefId; ?>
               </td>
               <td>
                  <?php echo $OfficeRefId; ?>
               </td>
               <td>
                  <?php echo $PositionRefId; ?>
               </td>
               <td class="text-center">
                  <?php echo $PositionItemRefId; ?>
               </td>
               <td>
                  &nbsp;
               </td>
               <td class="text-center">
                  <?php echo $SalaryGradeRefId; ?>
               </td>
               <td class="text-center">
                  <?php echo $SalaryAmount; ?>
               </td>
               <td>
                  <?php echo $PositionRefId; ?>
               </td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td>
                  <?php echo $EmailAdd; ?>
               </td>
               <td>
                  <?php echo $EmpStatusRefId; ?>
               </td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td class="text-center">
                  <?php
                     $code = getRecord("agency",$empinfo["AgencyRefId"],"Code");
                     if ($code != "") {
                        if ($code == "PCW") {
                           $where_empmovement = "WHERE EmployeesRefId = '$emprefid' AND AgencyRefId = '".$empinfo["AgencyRefId"]."'";
                           $rsMovement = SelectEach("employeesmovement",$where_empmovement." ORDER BY EffectivityDate LIMIT 1");
                           if ($rsMovement) {
                              while ($rowMovement = mysqli_fetch_assoc($rsMovement)) {
                                 echo date("F d, Y",strtotime($rowMovement["EffectivityDate"]));
                              }
                           }
                        }
                     }
                  ?>
               </td>
               <td>
                  <?php
                     $eligibility_rs = SelectEach("employeeselegibility",$where);
                     if ($eligibility_rs) {
                        while ($eligibility_row = mysqli_fetch_assoc($eligibility_rs)) {
                           echo "<li>".getRecord("careerservice",$eligibility_row["CareerServiceRefId"],"Name")."</li>";
                        }
                     }
                  ?>
               </td>
               <td class="text-center">
                  <?php
                     $highest_level = "";
                     $highest_rs = SelectEach("employeeseduc",$where." ORDER BY LevelType");
                     if ($highest_rs) {
                        while ($highest_row = mysqli_fetch_assoc($highest_rs)) {
                           $highest_level = $highest_row["LevelType"];
                        }
                     }
                     switch ($highest_level) {
                        case '3':
                           $highest_level_word = "Vocational";
                           break;
                        case '4':
                           $highest_level_word = "College";
                           break;
                        case '5':
                           $highest_level_word = "Master's Degree";
                           break;
                        default:
                           $highest_level_word = "";
                           break;
                     }
                     echo $highest_level_word;
                  ?>
               </td>
               <td>
                  <?php
                     $CourseRefId = FindFirst("employeeseduc",$where." AND LevelType = '$highest_level'","CourseRefId");
                     echo "<li>".getRecord("course",$CourseRefId,"Name")."</li>";
                  ?>
               </td>
               <td>
                  <?php
                     $masters_rs = SelectEach("employeeseduc",$where." AND LevelType = '5'");
                     if ($masters_rs) {
                        while ($masters_row = mysqli_fetch_assoc($masters_rs)) {
                           echo "<li>".getRecord("course",$masters_row["CourseRefId"],"Name")."</li>";
                        }
                     }
                  ?>
               </td>
               <td class="text-center">
                  <?php echo date("m/d/Y",strtotime($BirthDate)); ?>
               </td>
               <td class="text-center">
                  <?php echo computeAge($BirthDate,$param = "today"); ?>
               </td>
               <td class="text-center">
                  <?php echo $Sex; ?>
               </td>
               <td class="text-center">
                  <?php echo $CivilStatus; ?>
               </td>
               <td>
                  <?php echo $TIN; ?>
               </td>
               <td>
                  <?php echo $ResiBrgy." ".$ResiAddCityRefId." ".$ResiAddProvinceRefId; ?>
               </td>
               <td>
                  <?php echo $PermanentBrgy." ".$PermanentAddCityRefId." ".$PermanentAddProvinceRefId; ?>
               </td>
               <td>
                  <?php
                     if ($pdsq["Q7c"] == 1) echo "YES";
                     else echo "NO";
                  ?>
               </td>
               <td>
                  <?php
                     if (computeAge($BirthDate,$param = "today") > 59) echo "YES";
                     else echo "NO";
                  ?>
               </td>
               <td>
                  <?php
                     if ($pdsq["Q7b"] == 1) echo "YES";
                     else echo "NO";
                  ?>
               </td>
               <td>
                  <?php
                     if ($pdsq["Q7a"] == 1) echo "YES";
                     else echo "NO";
                  ?>
               </td>
               
               <td>FILIPINO</td>
               <td>
                  <?php echo $ContactNo; ?>
               </td>
               <td>
                  <?php echo $EmailAdd; ?>
               </td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
            </tr>
            <?php
                  }
               } 
            ?>
         </tbody>
      </table>
   </body>
</html>