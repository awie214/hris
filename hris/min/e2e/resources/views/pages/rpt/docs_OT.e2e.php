<?php
	$refid = $_GET["refid"];
	header("Content-type: application/vnd.ms-word");
   	header("Content-Disposition: attachment;Filename=Annex_A_Authority To Render Overtime Services.doc");
   
   	function FindFirst($table,$where,$fld) {
      	include "../conn.e2e.php";
      	$table = strtolower($table);
      	if ($fld == "*") {
         	$sql  = "SELECT * FROM $table ".$where." LIMIT 1";
      	} else {
         	$sql  = "SELECT `$fld` FROM $table ".$where." LIMIT 1";
      	}
      	$rs      = mysqli_query($conn,$sql) or die(mysqli_error($conn));
      	if ($rs) {
         	if ($fld == "*") {
            	$row = mysqli_fetch_assoc($rs);
            	return $row;
         	} else {
            	$row = mysqli_fetch_assoc($rs);
            	return $row[$fld];
         	}
      	} else {
         	echo $sql;
      	}
   	}
   	function rptHeader($cid) {
   		if ($cid == "2") {
   			echo '
   				<tr>
					<td colspan="2" valign="top">
						<img src="http://10.10.254.237/HRIS/hris/min/e2e/public/images/2/PCCLogoSmall.png" style="width: 150px;">
					</td>
					<td colspan="2" valign="top" style="font-size: 10pt; text-align: right;">
						25F Vertis North Corporate Center I 
		                <br>
		                North Avenue, Quezon City 1105
		                <br>
		                queries@phcc.gov.ph
		                <br>
		                (+632) 7719-PCC (7719-722) 
					</td>
				</tr>
   			';
   		}
   	}
   	$row = FindFirst("overtime_request","WHERE RefId = '$refid'","*");
   	if ($row) {
      	$emprefid = $row["EmployeesRefId"];
      	$FiledDate = $row["FiledDate"];
      	$StartDate = $row["StartDate"];
      	$EndDate = $row["EndDate"];
      	$WithPay = $row["WithPay"];
      	if (intval($WithPay) > 0) {
         	$WithPay = "OT Pay";
      	} else {
         	$WithPay = "CTO";
      	}
      	if ($StartDate == $EndDate) {
         	$date = date("F d, Y",strtotime($StartDate));
      	} else {
         	$date = date("F d, Y",strtotime($StartDate))." to ".date("F d, Y",strtotime($EndDate));;
      	}
      	$FiledDate = date("F d, Y",strtotime($FiledDate));
      	$emp_row = FindFirst("employees","WHERE RefId = '$emprefid'","*");
      	if ($emp_row) {
         	$LastName   = $emp_row["LastName"];
         	$FirstName  = $emp_row["FirstName"];
         	$MiddleName = $emp_row["MiddleName"];
         	$ExtName 	= $emp_row["ExtName"];
         	$FullName = $LastName.", ".$FirstName." $ExtName ".$MiddleName;
      	} else {
         	$FullName = "";
      	}

      	$emp_info = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
      	if ($emp_info) {
         	$OfficeRefId = FindFirst("office","WHERE RefId = ".$emp_info["OfficeRefId"],"Name");
         	$PositionRefId = FindFirst("position","WHERE RefId = ".$emp_info["PositionRefId"],"Name");
     	}
   }
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		table {
			width: 100%;
			padding: 5px;
		}
		.border {
			border: 1px solid black;
		}
		.text-center {
			text-align: center;
		}
		.vtop {
			vertical-align: top
		}
		.pad5 {
			padding: 5px;
		}
		.gray {
			background: #d9d9d9;
			font-weight: 600;
		}
		td {
			padding: 5px;
		}
		body {
			font-family: "Times New Roman";
			font-size: 12pt;
		}
		.textbox {
			border: none;
			border-bottom: 1px solid gray;
			width: 90%;
			font-size: 12pt;
			padding: 5px;
		}
	</style>
</head>
<body>
	<table cellpadding="0" cellspacing="0">
		<?php
			rptHeader(2);
		?>
		<tr>
			<td colspan="4" class="text-center">Annex A<br>AUTHORITY TO RENDER OVERTIME SERVICES<br><br><br><td>
		</tr>
		<tr>
			<td colspan="3"></td>
			<td class="text-center vtop">
				<u><?php echo $FiledDate; ?></u>
				<br>
				Date
			</td>
		</tr>
		<tr>
			<td style="margin-top: 20px;" colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">
				This is to authorize:
				<br>&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<table border="1" cellpadding="0" cellspacing="0">
					<tr align="center">
						<td class="" valign="middle" style="text-align: center !important; background: #d9d9d9;">
							Name
						</td>
						<td class="" style="text-align: center !important; background: #d9d9d9;">
							Office
						</td>
						<td class="" style="text-align: center !important; background: #d9d9d9;">
							Position
						</td>
						<td class="" style="text-align: center !important; background: #d9d9d9;">
							Preferred<br>Remuneration<br>(OT Pay or CTO)
						</td>
					</tr>
					<tr>
						<td class="" style="">
							<?php echo $FullName; ?>
						</td>
						<td class="text-center  pad5" style="">
							<?php echo $OfficeRefId; ?>
						</td>
						<td class="text-center  pad5" style="">
							<?php echo $PositionRefId; ?>
						</td>
						<td class="text-center  pad5" style="">
							<?php echo $WithPay; ?>
						</td>
					</tr>			
				</table>
			</td>
		</tr>
		
		<tr>
			<td style="margin-top: 20px;" colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td class="vtop">
				To render overtime job/services on:
			</td>
			<td colspan="2"></td>
			<td class="text-center vtop">
				<u><?php echo $date; ?></u>
				<br>
				Date
			</td>
		</tr>
		<tr>
			<td style="margin-top: 20px;" colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">
				Specific output to be done/expected output:
			</td>
		</tr>
		<tr>
			<td colspan="2" style="border-bottom: 1px solid black;">
				1. 
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" style="border-bottom: 1px solid black;">
				2. 
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" style="border-bottom: 1px solid black;">
				3. 
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td style="margin-top: 20px;" colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">
				Recommending Approval:
				<br>
				<br>
				<br>
				<br>
				<b>
                  	Immediate Supervisor
               	</b>
			</td>
			<td>
				Approved:
				<br>
				<br>
				<br>
				<br>
				<b>
                  	Head of Office
               	</b>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<p>
					<b>Note</b>: This should be accomplished in two (2) copies prior to rendering overtime work. Please submit the duplicate copy to the Human Resource Development Division at the end of the month along with the Daily Time Record (DTR) and Accomplishment Report.
				</p>
			</td>
		</tr>
	</table>
</body>
</html>