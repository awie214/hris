<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body style="width:7in;">
      <div class="container-fluid rptBody">
         <div class="border1 padd20">
            <?php
               rptHeader("");
            ?>
            <br>
            <div>
               <p class="txt-right">________________</p>
               <p class="txt-right" style="padding-right:.5in;">Date</p>
            </div>
            <br>
            <div class="txt-center">
               <?php echo "<u><b>".strtoupper(getRptName(getvalue("drpReportKind")))."</b></u>" ?>
            </div><br>
            <p style="text-indent:50px;">
            This is to certify that I have rendered my services in the Philippine Institue for Developtment Studies (PIDS) as __________________________ for the period __________________________________
            </p>
            <div>
               <p class="txt-right">_______________________________</p>
               <p class="txt-right" style="padding-right:20px;">(Signature OverPrinted Name)</p>
            </div><br>
            <div>
               <p class="txt-right">_______________________________</p>
               <p class="txt-right" style="padding-right:80px;">Designation</p>
            </div><br>
            <div>
               <p class="txt-left">Certified true correct</p>
               <p class="txt-left">_______________________________</p>
               <p class="txt-left" style="padding-left:20px;">(Signature OverPrinted Name)</p>
            </div>
            <div>
               <p class="txt-left">_______________________________</p>
               <p class="txt-left" style="padding-left:80px;">Designation</p>
            </div>
         </div>

      </div>
      <?php rptFooter(); ?>
   </body>
</html>