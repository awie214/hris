<?php
   include_once 'pageHEAD.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   $refid = getvalue("refid");
   $row = FindFirst("employeesauthority","WHERE RefId = $refid","*");
   if ($row) {
         $Signatory1       = $row["Signatory1"];
         $Signatory2       = $row["Signatory2"];
         $Signatory3       = $row["Signatory3"];
         $FundType         = $row["FundType"];
         $FundDesc         = $row["FundDesc"];
         $FundDesc         = convertBR($FundDesc);

         $s1_row           = FindFirst("signatories","WHERE RefId = '$Signatory1'","*");
         $s2_row           = FindFirst("signatories","WHERE RefId = '$Signatory2'","*");

         $s1_name          = $s1_row["Name"];
         $s1_position      = getRecord("position",$s1_row["PositionRefId"],"Name");

         $s2_name          = $s2_row["Name"];
         $s2_position      = getRecord("position",$s2_row["PositionRefId"],"Name");


         $authority     = getRecord("absences",$row["AbsencesRefId"],"Name");
         $authority     = strtoupper($authority);
         $Purpose       = convertBR($row["Remarks"]);;
         $Remarks       = $authority." - ".$row["Remarks"];
         $Destination   = $row["Destination"];
         $emprefid      = $row["EmployeesRefId"];
         $FiledDate     = $row["FiledDate"];
         $TO_no         = "TO-".date("Y",strtotime($FiledDate))."".date("m",strtotime($FiledDate))."-".str_pad($refid, 3, '0',STR_PAD_LEFT);
         $employees = FindFirst("employees","WHERE RefId = '$emprefid'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
         if ($employees) {
            $FirstName = $employees["FirstName"];
            $LastName = $employees["LastName"];
            $MiddleName = $employees["MiddleName"];
            $ExtName = $employees["ExtName"];
            $FullName = $LastName.", ".$FirstName." $ExtName ".$MiddleName;
         } else {
            $FullName = "&nbsp;";
         }
         $empinformation = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
         if ($empinformation) {
            $OfficeRefId = getRecord("office",$empinformation["OfficeRefId"],"Name");
            $DivisionRefId = getRecord("division",$empinformation["DivisionRefId"],"Name");
            $PositionRefId = getRecord("position",$empinformation["PositionRefId"],"Name");
         } else {
            $OfficeRefId = $DivisionRefId = $PositionRefId = "&nbsp;";
         }
         if ($row["Whole"] == 1) {
            $start_time = $end_time = "Whole Day";
         } else {
            $start_time = convertToHoursMins($row["FromTime"]);
            $end_time = convertToHoursMins($row["ToTime"]);
         }
         $from = $row["ApplicationDateFrom"];
         $to = $row["ApplicationDateTo"];
         if ($from == $to) {
            $date = date("F d, Y",strtotime($from));
         } else {
            $date = date("F d, Y",strtotime($from))." to ".date("F d, Y",strtotime($to));;
         }        
   }
      $user = getvalue("hUserRefId");
      $user_row = FindFirst("employees","WHERE RefId = '$user'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
      if ($user_row) {
      $user_LastName   = $user_row["LastName"];
      $user_FirstName  = $user_row["FirstName"];
      } else {
      $user_FirstName = $user_LastName = "";
      }
?>
<!DOCTYPE html>
<html>
<head>
   <style type="text/css">
      td {vertical-align: top;}
   </style>
   <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
</head>
<body>
   <div class="container-fluid rptBody">
      <div style="page-break-after: always;">
         <?php
            rptHeader("");
         ?>
         <div class="row">
            <div class="col-xs-11 text-right">
               <b><?php echo date("F d, Y",strtotime($FiledDate)); ?></b>
            </div>
            <div class="col-xs-1"></div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-12">
               <b>
                  LOCAL TRAVEL ORDER _________
                  <br>
                  <i>Series of 2018</i>
               </b>
            </div>
         </div>
         <br>
         <div class="row margin-top">
            <div class="col-xs-12">
               Authority to Travel is hereby granted to
               <br>
               <table border="1" width="100%">
                  <tr class="colHEADER">
                     <th class="text-center" style="width: 33.33%;">NAME</th>
                     <th class="text-center" style="width: 33.33%;">POSITION</th>
                     <th class="text-center" style="width: 33.33%;">DIVISION/AGENCY</th>
                  </tr>
                  <tr>
                     <td>
                        <?php
                           echo $FullName;
                        ?>
                     </td>
                     <td class="text-center">
                        <?php
                           echo $PositionRefId;
                        ?>
                     </td>
                     <td class="text-center">
                        <?php
                           echo $DivisionRefId;
                        ?>
                     </td>
                  </tr>
                  <tr class="colHEADER">
                     <th class="text-center">Destination</th>
                     <th class="text-center">Inclusive date of Travel</th>
                     <th class="text-center">Purpose(s) of the Travel</th>
                  </tr>
                  <tr>
                     <td class="text-center">
                        <?php
                           echo $Destination;
                        ?>
                     </td>
                     <td class="text-center">
                        <?php
                           echo $date;
                        ?>
                     </td>
                     <td>
                        <?php
                           echo $Purpose;
                        ?>
                     </td>
                  </tr>
               </table>
            </div>
         </div>
         <br><br>
         <div class="row">
            <div class="col-xs-12">
               <table width="100%">
                  <tr class="colHEADER">
                     <th>Travel Expense to be incurred:</th>
                     <th colspan="3">Appropriation/Fund to which travel expense would be charged to:</th>
                  </tr>
                  <tr>
                     <td style="width: 25%;"></td>
                     <td style="width: 25%;">
                        <?php
                           if ($FundType == 1) {
                              echo '<input type="checkbox" checked disabled>&nbsp;&nbsp;General Fund';
                           } else {
                              echo '<input type="checkbox" disabled>&nbsp;&nbsp;General Fund';
                           }
                        ?>
                     </td>
                     <td style="width: 25%;">
                        <?php
                           if ($FundType == 2) {
                              echo '<input type="checkbox" checked disabled>&nbsp;&nbsp;Project Funds (Pls. Specify)';
                           } else {
                              echo '<input type="checkbox" disabled>&nbsp;&nbsp;Project Funds (Pls. Specify)';
                           }
                        ?>
                     </td>
                     <td style="width: 25%;">
                        <?php
                           if ($FundType == 3) {
                              echo '<input type="checkbox" checked disabled>&nbsp;&nbsp;Others: (e.g sponsor/requesting agency)';
                           } else {
                              echo '<input type="checkbox" disabled>&nbsp;&nbsp;Others: (e.g sponsor/requesting agency)';
                           }
                        ?>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <input type="checkbox">&nbsp;&nbsp;Per Diem
                     </td>
                     <td rowspan="10" valign="top">
                        <?php
                           if ($FundType == 1) {
                              echo $FundDesc;
                           }
                        ?>
                     </td>
                     <td rowspan="10" valign="top">
                        <?php
                           if ($FundType == 2) {
                              echo $FundDesc;
                           }
                        ?>
                     </td>
                     <td rowspan="10" valign="top">
                        <?php
                           if ($FundType == 3) {
                              echo $FundDesc;
                           }
                        ?>
                     </td>
                  </tr>
                  <tr>
                     <td style="text-indent: 20px;">
                        Accomodation
                     </td>
                     
                  </tr>
                  <tr>
                     <td style="text-indent: 20px;">
                        Meals/Food
                     </td>
                     
                  </tr>
                  <tr>
                     <td style="text-indent: 20px;">
                        Incidental Expenses
                     </td>
                     
                  </tr>
                  <tr>
                     <td>
                        <input type="checkbox">&nbsp;&nbsp;Transportation
                     </td>
                     
                  </tr>
                  <tr>
                     <td style="text-indent: 20px;">
                        Official Vehicle
                     </td>
                     
                  </tr>
                  <tr>
                     <td style="text-indent: 20px;">
                        Public Conveyance
                     </td>
                     
                  </tr>
                  <tr>
                     <td style="text-indent: 20px;">
                        Airplane, Bus, Taxi
                     </td>
                     
                  </tr>
                  <tr>
                     <td>
                        <input type="checkbox">&nbsp;&nbsp;Others
                     </td>
                     
                  </tr>
                  <tr>
                     <td>
                        &nbsp;
                     </td>
                     
                  </tr>
               </table>
            </div>
         </div>
         <br><br>
         <div class="row margin-top">
            <div class="col-xs-12">
               Remarks/Special Instructions:
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-12">
               A report of your travel must be submitted to the Agency Head/Supervising Official within 7 days from completion of travel. Liquidation of cash advances should be in accordance with Executive Order No. 298. Rules and Regulation and New Rates of Allowances for Official Local and Foreign Travels of Government Personnel.
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-6">
               <br>
               <br>
               <br>
               RECOMMENDING APPROVAL:
               <br>
               <br>
               <br>
               <b>ROBERT O. DIZON</b>
               <br>
               Executive Director
            </div>
            <div class="col-xs-6">
               <br>
               <br>
               <br>
               APPROVED BY:
               <br>
               <br>
               <br>
               <b>FORTUNATO T. DELA PEÑA</b>
               <br>
               Secretary,DOST
            </div>
         </div>
      </div>
   </div>
</body>
</html>