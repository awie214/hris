<?php
   include_once 'pageHEAD.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   $refid = getvalue("refid");
   $row = FindFirst("employeesauthority","WHERE RefId = $refid","*");
   if ($row) {
         $Signatory1       = $row["Signatory1"];
         $Signatory2       = $row["Signatory2"];
         $Signatory3       = $row["Signatory3"];
         $date_start       = $row["ApplicationDateFrom"];
         $date_end         = $row["ApplicationDateTo"];
         $time_start       = $row["FromTime"];
         $time_end         = $row["ToTime"];
         $ActivityNature   = $row["ActivityNature"];
         $Sponsor          = $row["Sponsor"];
         $EventName        = $row["EventName"];

         $s1_row           = FindFirst("signatories","WHERE RefId = '$Signatory1'","*");
         $s2_row           = FindFirst("signatories","WHERE RefId = '$Signatory2'","*");

         $s1_name          = $s1_row["Name"];
         $s1_position      = getRecord("position",$s1_row["PositionRefId"],"Name");

         $s2_name          = $s2_row["Name"];
         $s2_position      = getRecord("position",$s2_row["PositionRefId"],"Name");


   		$authority 		= getRecord("absences",$row["AbsencesRefId"],"Name");
   		$authority 		= strtoupper($authority);
   		$Purpose 		= $row["Remarks"];
   		$Remarks 		= $authority." - ".$row["Remarks"];
   		$Destination 	= $row["Destination"];
   		$emprefid 		= $row["EmployeesRefId"];
   		$FiledDate 		= $row["FiledDate"];
   		$TO_no         = $row["ControlNumber"];
   		$employees = FindFirst("employees","WHERE RefId = '$emprefid'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
   		if ($employees) {
   			$FirstName = $employees["FirstName"];
   			$LastName = $employees["LastName"];
   			$MiddleName = $employees["MiddleName"];
   			$ExtName = $employees["ExtName"];
   			$FullName = $LastName.", ".$FirstName." $ExtName ".$MiddleName;
   		} else {
   			$FullName = "&nbsp;";
   		}
   		$empinformation = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
   		if ($empinformation) {
   			$OfficeRefId = getRecord("office",$empinformation["OfficeRefId"],"Name");
   			$DivisionRefId = getRecord("division",$empinformation["DivisionRefId"],"Name");
   			$PositionRefId = getRecord("position",$empinformation["PositionRefId"],"Name");
   		} else {
   			$OfficeRefId = $DivisionRefId = $PositionRefId = "&nbsp;";
   		}
   		if ($row["Whole"] == 1) {
   			$start_time = $end_time = "Whole Day";
   		} else {
   			$start_time = convertToHoursMins($row["FromTime"]);
   			$end_time = convertToHoursMins($row["ToTime"]);
   		}
   		$from = $row["ApplicationDateFrom"];
   		$to = $row["ApplicationDateTo"];
   		if ($from == $to) {
   			$date = date("F d, Y",strtotime($from));
   		} else {
   			$date = date("F d, Y",strtotime($from))." to ".date("F d, Y",strtotime($to));;
   		}   	
         if ($date_start == $date_end) {
            $date_range = date("d F Y",strtotime($date_start));   
         } else {
            $date_range = date("d F Y",strtotime($date_start))." to ".date("d F Y",strtotime($date_end));   
         }
         if ($time_start != "") {
            $time_range = HrsFormat($time_start)." - ".HrsFormat($time_end);
         } else {
            $time_range = "Whole Day";
         }	
   }
	$user      = getvalue("hUserRefId");
	$user_row  = FindFirst("employees","WHERE RefId = '$user'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
	if ($user_row) {
      $user_LastName   = $user_row["LastName"];
      $user_FirstName  = $user_row["FirstName"];
	} else {
      $user_FirstName = $user_LastName = "";
	}
?>
<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		td, th {
			border: 2px solid black;
			vertical-align: top;
			padding: 5px;
			font-size: 9pt;
		}
      th {text-align: center;}
		.data {
			font-size: 10pt;
			text-transform: uppercase;
		}
	</style>
</head>
<body>
	<div class="container-fluid rptBody">
		<div style="page-break-after: always;">
	        <?php
	            rptHeader("Office Order","HRDMS-T-003");
	        ?>
	        <div class="row margin-top">
	        	<div class="col-xs-12">
	        		<b>OFFICE ORDER NO: <?php echo $TO_no; ?></b>
               <br>
               Series of <?php echo date("Y",strtotime($FiledDate)); ?>
               <br>
               <br>
               This office order authorizes the following personnel to attend/participate/conduct the specified activity
               <br>
               <br>
               <b>WHAT </b>
               <u><?php echo $EventName; ?></u>
               <br>
               <br>
               <b>NATURE OF ACTIVITY </b> <?php echo $ActivityNature; ?>
               <br>
               <br>
               <b>SPONSORED/CONDUCTED/REQUESTED BY: </b> <?php echo $Sponsor; ?>
	        	</div>
	        </div>
	        <br>
	        <div class="row margin-top">
	        	<div class="col-xs-12">
	        		<table width="100%" border="1">
                  <thead>
                     <tr>
                        <th style="width: 50%;">NAME OF PERSONNEL</th>
                        <th style="width: 50%;">
                           PURPOSE OF ATTENDANCE
                           <br>
                           <i>
                              (specify if participant, resource speaker, observer, facilitator, secretariat,documenter, TA provider, coordinator, etc)
                           </i>
                        </th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>
                           <?php echo $FullName; ?>
                        </td>
                        <td>
                           <?php echo $Purpose; ?>
                        </td>
                     </tr>
                  </tbody>
               </table>
	        	</div>
	        </div>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <b>WHEN:</b> <u><?php echo $date_range; ?></u>
                  <br>
                  <b>WHERE:</b> <u><?php echo $Destination; ?></u>
                  <br>
                  <b>TIME:</b> <u><?php echo $time_range; ?></u>
               </div>
            </div>
            <br>
            <br>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <p>
                     This further allows the above-named personnel to charge the following expense/s, subject to the availability of ______________ funds and usual accounting and auditing requirements:
                  </p>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-1">
               </div>
               <div class="col-xs-11">
                  <input type="checkbox" name="">&nbsp;Registration fee (Amount _________)
                  <br>
                  <input type="checkbox" name="">&nbsp;Transportation expense/allowance
                  <br>
                  <input type="checkbox" name="">&nbsp;Representation allowance
                  <br>
                  <input type="checkbox" name="">&nbsp;Others (please specify e.g. Food allowance, etc)
               </div>
            </div>
            <br>
            <br>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <p>All Office Orders previously issued which are inconsistent with the above are deemed superseded by this Order.</p>
               </div>
            </div>
            <br>
            <br>
	        <div class="row margin-top">
	        	<div class="col-xs-6">
	        		Recommended by:
	        		<br><br><br>
	        		<b>
                  <?php echo $s1_name; ?>
               </b>
               <br>
               <?php echo $s1_position; ?>
	        	</div>
	        	<div class="col-xs-6">
	        		Approved by:
	        		<br><br><br>
	        		<b>
                  <?php echo $s2_name; ?>
               </b>
               <br>
               <?php echo $s2_position; ?>
	        	</div>
	        </div>
         	<div class="row margin-top">
                <div class="col-xs-12">
                    Confirmed Attendance by:
                    <br><br><br>
                    _______________________
                    <br>
                    Signature over printed name
                    <br><br><br>
                    _______________________
                    <br>
                    Position/Designation

    
                </div>
            </div>
	    </div>
        <div class="row">
            <div class="col-xs-12" style="padding: 20px;">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <br><br><br>
                        <h4><b>CERTIFICATE OF APPEARANCE</b></h4>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-xs-12" style="text-indent: 5%; text-align: justify;">
                        This is to certify that of the Philippine Commission on Women attended the _______________________ at ____________ on ______________.<br><br>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-xs-12" style="text-indent: 5%; text-align: justify;">
                        This certification is issued upon the request of the interested party for attendance purposes only.
                        <br><br>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-xs-8">
                    </div>
                    <div class="col-xs-4 text-center">
                        ______________________
                        <br>
                        Signature
                        <br><br>
                        ______________________
                        <br>
                        Position Title
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>