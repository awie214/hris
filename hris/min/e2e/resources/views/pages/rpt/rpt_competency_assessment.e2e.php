<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $count = 0;
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style type="text/css">
         
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader("SUMMARY OF COMPETENCY ASSESSMENT");
         ?>
         <br><br>
         <div class="row">
            <div class="col-xs-12">
               <table style="width: 100%;">
                  <thead>
                     <tr class="colHEADER">
                        <th>(1)</th>
                        <th>(2)</th>
                        <th>(3)</th>
                        <th>(4)</th>
                        <th>(5)</th>
                        <th>(6)</th>
                        <th>(7)</th>
                        <th>(8)</th>
                        <th colspan="2">(9)</th>
                     </tr>
                     <tr class="colHEADER">
                        <th rowspan="2">Name</th>
                        <th rowspan="2">Competencies</th>
                        <th rowspan="2">Competency<br>Type</th>
                        <th rowspan="2">Proficiency<br>Level Required</th>
                        <th rowspan="2">Self<br>Assessment</th>
                        <th rowspan="2">Supervisor's<br>Assessment</th>
                        <th rowspan="2">Average of<br>(5) and (6)</th>
                        <th rowspan="2">Gap Analysis<br>(4-7)</th>
                        <th colspan="2">Needs L&D<br>Intervention</th>
                     </tr>
                     <tr class="colHEADER">
                        <th>Yes</th>
                        <th>No</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                        $rs = SelectEach("employees","WHERE RefId > 0 AND (Inactive != 1 OR Inactive IS NULL)");
                        if ($rs) {
                           while ($row = mysqli_fetch_assoc($rs)) {
                              $check_name    = "";
                              $emprefid      = $row["RefId"];
                              $LastName      = $row["LastName"];
                              $FirstName     = $row["FirstName"];
                              $MiddleName    = $row["MiddleName"];
                              $MiddleInitial = substr($MiddleName, 0,1);
                              $FullName      = $FirstName." ".$MiddleInitial.". ".$LastName;
                              $where_competency = "WHERE EmployeesRefId = '$emprefid'";
                              $competency    = SelectEach("ldmscompetency_assessment",$where_competency);
                              if ($competency) {
                                 $row_count  = mysqli_num_rows($competency);
                                 while ($competency_row = mysqli_fetch_assoc($competency)) {
                                    $LDMSCompetencyRefId = $competency_row["LDMSCompetencyRefId"];
                                    $YearConducted       = $competency_row["YearConducted"];
                                    $SelfAssessment      = $competency_row["SelfAssessment"];
                                    $Supervisor          = $competency_row["Supervisor"];
                                    $IsIntervention      = $competency_row["IsIntervention"];
                                    $where_manual        = "WHERE RefId = '$LDMSCompetencyRefId'";
                                    $comp_row            = FindFirst("ldmscompetency",$where_manual,"*");
                                    $Type                = $comp_row["Type"];
                                    $Name                = $comp_row["Name"];
                                    $Level               = $comp_row["Level"];
                                    $Average             = ($SelfAssessment + $Supervisor) / 2;
                                    $Average             = round($Average);
                                    $Gap                 = $Level - $Average;
                                    echo '<tr>';
                                       if ($check_name == $FullName) {
                                          echo '<td>&nbsp;</td>';
                                       } else {
                                          echo '<td>'.$FullName.'</td>';
                                          $check_name = $FullName;
                                       }
                                       echo '<td>'.$Name.'</td>';
                                       echo '<td class="text-center">'.$Type.'</td>';
                                       echo '<td class="text-center">'.$Level.'</td>';
                                       echo '<td class="text-center">'.$SelfAssessment.'</td>';
                                       echo '<td class="text-center">'.$Supervisor.'</td>';
                                       echo '<td class="text-center">'.$Average.'</td>';
                                       echo '<td class="text-center">'.number_format($Gap,3).'</td>';
                                       echo '<td></td>';
                                       echo '<td></td>';
                                    echo '</tr>';
                                 }
                              }
                           }
                        }
                     ?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </body>
</html>
