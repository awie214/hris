<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <table style="width: 100%;">
            <thead>
               <tr>
                  <td colspan="17">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                  </td>
               </tr>
               <tr class="colHEADER">
                  <th colspan="9" style="padding: 20px;">Department: Department of Science and Technology</th>
                  <th colspan="8">Bureau/Agency: Metals Industry Research and Development Center</th>
               </tr>
               <tr class="colHEADER">
                  <th rowspan="2">ITEM NUMBER</th>
                  <th rowspan="2">POSITION TITLE and <br> SALARY GRADE</th>
                  <th colspan="2">ANNUAL SALARY</th>
                  <th rowspan="2">S<br>T<br>E<br>P</th>
                  <th colspan="2">AREA</th>
                  <th rowspan="2">L<br>E<br>V<br>E<br>L</th>
                  <th rowspan="2">P/P/A<br>ATTRIBUTION</th>
                  <th rowspan="2">NAME OF INCUMBENT</th>
                  <th rowspan="2">S<br>E<br>X</th>
                  <th rowspan="2">DATE<br>OF<br>BIRTH</th>
                  <th rowspan="2">TIN</th>
                  <th rowspan="2">DATE<br>OF<br>ORIGINAL APPOINTMENT</th>
                  <th rowspan="2">DATE OF LAST PROMOTION</th>
                  <th rowspan="2">S<br>T<br>A<br>T<br>U<br>S</th>
                  <th rowspan="2">CIVIL<br>SERVICE<br>ELIGIBILITY</th>
               </tr>
               <tr class="colHEADER">
                  <th>AUTHORIZED</th>
                  <th>ACTUAL</th>
                  <th>C<br>O<br>D<br>E</th>
                  <th>T<br>Y<br>P<br>E</th>
               </tr>
            </thead>
            <tbody>
               <?php
                  for ($i=1; $i <= 10; $i++) { 
                     echo '<tr>';
                     for ($a=1; $a <=17 ; $a++) { 
                        echo '<td>&nbsp;</td>';
                     }
                     echo '</tr>';
                  }
               ?>
            </tbody>
         </table>
         <br><br>
         <div class="row margin-top">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-1">Subtotal</div>
                  <div class="col-xs-2">No. of Filled Positions:</div>
                  <div class="col-xs-1">0</div>
                  <div class="col-xs-1 text-right">0</div>
                  <div class="col-xs-1 text-right">0</div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2">No. of Unfilled Positions:</div>
                  <div class="col-xs-1">0</div>
                  <div class="col-xs-1 text-right">0</div>
                  <div class="col-xs-1 text-right">0</div>
               </div>
               <br>
               <div class="row">
                  <div class="col-xs-1">Grand Total</div>
                  <div class="col-xs-2">No. of Filled Positions:</div>
                  <div class="col-xs-1">0</div>
                  <div class="col-xs-1 text-right">0</div>
                  <div class="col-xs-1 text-right">0</div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2">No. of Unfilled Positions:</div>
                  <div class="col-xs-1">0</div>
                  <div class="col-xs-1 text-right">0</div>
                  <div class="col-xs-1 text-right">0</div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2">No. of Itemized Positions:</div>
                  <div class="col-xs-1">0</div>
                  <div class="col-xs-1 text-right">0</div>
                  <div class="col-xs-1 text-right">0</div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-12">Remarks</div>
               </div>
               <br>
               <br>
               <div class="row margin-top text-center">
                  <div class="col-xs-12">****NOTHING FOLLOWS****</div>
               </div>
               <br>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-4">Department of Budget and Management</div>
                  <div class="col-xs-4">
                     I certify to the correctness of the entries from columns 4 to 17 and that employees whose names appear on the above PSIPOP are the incumbents of the positions.
                  </div>
                  <div class="col-xs-1">&nbsp;</div>
                  <div class="col-xs-3">APPROVED BY:</div>
               </div>
               <div class="row margin-top text-center">
                  <div class="col-xs-4">&nbsp;</div>
                  <div class="col-xs-4"><u>JELLY N. ORTIZ, DPA</u></div>
                  <div class="col-xs-4"><u>ROBERT O. DIZON</u></div>
               </div>
               <div class="row margin-top text-center">
                  <div class="col-xs-4"></div>
                  <div class="col-xs-4">Human Resource Management Officer</div>
                  <div class="col-xs-4">Head of Agency</div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>