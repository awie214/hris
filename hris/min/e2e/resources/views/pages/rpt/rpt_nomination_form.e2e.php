<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $refid               = getvalue("refid");
   $table               = "assigned_courses";
   $row                 = FindFirst($table,"WHERE RefId = '$refid'","*");
   $training            = FindFirst("ldmslndprogram","WHERE RefId = '".$row["LDMSLNDProgramRefId"]."'","*");
   $Name                = $training["Name"];
   $TrainingInstitution = $training["TrainingInstitution"];
   $Cost                = $training["Cost"];
   $StartDate           = $training["StartDate"];
   $EndDate             = $training["EndDate"];
   $Venue               = $training["Venue"];
   $emprefid            = $row["EmployeesRefId"];
   if ($StartDate == "") {
      $Date = "";
   } else {
      if ($StartDate == $EndDate) {
         $Date = date("F d, Y",strtotime($StartDate));
      } else {
         $Date = date("F d, Y",strtotime($StartDate))." to ".date("F d, Y",strtotime($EndDate));;
      }
   }
   $emprow = FindFirst("employees","WHERE RefId = '$emprefid'","`FirstName`,`LastName`,`MiddleName`");
   $LastName      = $emprow["LastName"];
   $FirstName     = $emprow["FirstName"];
   $MiddleName    = $emprow["MiddleName"];
   $FullName      = $LastName.", ".$FirstName." ".$MiddleName;
   $empinfo = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","`PositionRefId`,`OfficeRefId`");
   if ($empinfo) {
      $Position   = getRecord("Position",$empinfo["PositionRefId"],"Name");
      $Office     = getRecord("Office",$empinfo["OfficeRefId"],"Name");
   } else {
      $Position = $Office = "";
   }  
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style type="text/css">
         .gray {background: gray;}
      </style>
   </head>
   <body>
      <div class="container-fluid">
         <?php
            rptHeader("");
         ?>
         <br><br>
         <div class="row">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12">
                     RO-FM-HR-01
                     <br>
                     Rev0
                     <br>
                     Effectivity Date: June 04, 2018
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table border="1" width="100%">
                        <tr>
                           <td colspan="3">
                              <div class="row">
                                 <div class="col-xs-6">
                                    Training Request
                                    <br>
                                    Form No.
                                 </div>
                                 <div class="col-xs-6 textc-center">
                                    <b>NOMINATION FORM</b>
                                 </div>
                              </div>
                           </td>
                           <td colspan="2" valign="top">
                              Date Filed: <b><?php echo date("F d, Y",strtotime($row["FiledDate"])); ?></b>
                           </td>   
                        </tr>
                        <tr>
                           <td colspan="5">
                              <div class="row">
                                 <div class="col-xs-12">
                                    Course Title: <b><?php echo $Name; ?></b>
                                    <br>
                                    Training Institution: <b><?php echo $TrainingInstitution; ?></b>
                                    <br>
                                    Fee: <b><?php echo number_format($Cost,2); ?></b>
                                    <br>
                                    Date Covered: <b><?php echo $Date; ?></b>
                                    <br>
                                    Venue: <b><?php echo $Venue; ?></b>
                                    <br>
                                    <Br>
                                 </div>
                              </div>
                              <div class="row margin-top" style="padding: 5px;">
                                 <div class="col-xs-1">
                                    The
                                 </div>
                                 <div class="col-xs-3" style="border: 1px solid black; font-size: 8pt; padding-top: 5px; padding-bottom: 5px;">
                                    <b><?php echo $Office; ?></b>
                                 </div>
                                 <div class="col-xs-1">
                                    Nominates
                                 </div>
                                 <div class="col-xs-3" style="border: 1px solid black; font-size: 8pt; padding-top: 5px; padding-bottom: 5px;">
                                    <b><?php echo $FullName; ?></b>
                                 </div>
                                 <div class="col-xs-4" style="border: 1px solid black; font-size: 8pt; padding-top: 5px; padding-bottom: 5px;">
                                    <b><?php echo $Position; ?></b>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-1"></div>
                                 <div class="col-xs-3 text-center">
                                    Regulation Area
                                 </div>
                                 <div class="col-xs-1"></div>
                                 <div class="col-xs-3 text-center">
                                    Employee Name
                                 </div>
                                 <div class="col-xs-3 text-center">
                                    Position / SG
                                 </div>
                              </div>
                              <br>
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    to the said course.
                                 </div>
                              </div>
                              <?php bar(); ?>
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    The course will be address the development priorities of the RO talent as identified in his/her Individual Development Plan (IDP) and it will help close competency gaps reflected in his/her Individual Competency Data. 
                                    <br>
                                    Particularly, this course will address the talent's competency gap on:
                                    <br>
                                    (Please encircle appropriate competency)
                                 </div>
                              </div>
                           </td>
                        </tr>
                        <tr valign="top">
                           <td class="">
                              <center>Core</center>
                              <ul>
                                 <li>Concession Area Expertise</li>
                                 <li>Standards of Enforcement And Management</li>
                                 <li>Concessionaire Practices and Assets Evaluation</li>
                                 <li>Policy Formulation</li>
                              </ul>

                           </td>
                           <td class="">
                              <center>Foundation</center>
                              <ul>
                                 <li>Communicating Effectively</li>
                                 <li>Delivering Service Excellence</li>
                                 <li>Exemplifying Professionalism</li>
                                 <li>Displaying Resilience and Emotional Intelligence</li>
                                 <li>Working with Others</li>
                              </ul>
                           </td>
                           <td class="">
                              <center>Organizational</center>
                              <ul>
                                 <li>Exhibiting Computer and Technology Proficiency</li>
                                 <li>Meeting Customer Expectations</li>
                                 <li>Collecting and Analyzing Data and Analysis</li>
                                 <li>Planning and Organizing</li>
                                 <li>Problem Solving and Decision Making</li>
                                 <li>Managing Records and Information</li>
                              </ul>

                           </td>
                           <td class="">
                              <center>Leadership</center>
                              <ul>
                                 <li>Building Relationships</li>
                                 <li>Leading Change</li>
                                 <li>Managing Performance</li>
                                 <li>Nurturing A High Performing Organization</li>
                                 <li>Thinking Strategically and Creatively</li>
                              </ul>

                           </td>
                           <td class="" style="padding: 10px;">
                              <center>Technical</center>
                              <ul>
                                 <li>Accounting</li>
                                 <li>Applying Internal Control</li>
                                 <li>Budget Planning and Monitoring</li>
                                 <li>Clerical Assistance</li>
                                 <li>Employee Benefits and Services</li>
                                 <li>Facility/Equipment Maintenance</li>
                                 <li>Financial Expertise/Acumen</li>
                                 <li>Learning and Development</li>
                                 <li>Legal Acumen and Expertise</li>
                                 <li>Media and Public Relations</li>
                                 <li>Occupational Health, Safety and Security Management</li>
                                 <li>Procurement and Inventory Management</li>
                                 <li>Treasury and Cash Management</li>
                                 <li>Trade and Specialization Fluency</li>
                              </ul>

                           </td>
                        </tr>
                        <tr>
                           <td colspan="5">
                              Endorsed By:
                           </td>
                        </tr>
                        <tr>
                           <td colspan="5">
                              <div class="row">
                                 <div class="col-xs-12 text-center">
                                    <B>PERSONNEL DEVELOPMENT COMMITTEE (PDC) ACTION</B>
                                 </div>
                              </div>
                              <br>
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    The Chief Regulator
                                    <br>
                                    The attendance to this training/course is consistent with the RO's policies on career and personnel development and with the talent's IDP.
                                    <br>
                                    The talent has attended _______ no. of training intervention this year.
                                    <br>
                                    The PDC recommends the attendance of the talent to the said course.
                                    <br><br>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-6 text-center">
                                    ________________________
                                    <br>
                                    Chairperson
                                 </div>
                                 <div class="col-xs-6 text-center">
                                    ________________________
                                    <br>
                                    Member
                                 </div>
                              </div>
                              <br><br>
                              <div class="row margin-top">
                                 <div class="col-xs-6 text-center">
                                    ________________________
                                    <br>
                                    Member, RO TUBIG, 2<sup>nd</sup> Level
                                 </div>
                                 <div class="col-xs-6 text-center">
                                    ________________________
                                    <br>
                                    Member, RO TUBIG, 1<sup>st</sup> Level
                                 </div>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td colspan="5">
                              <div class="row">
                                 <div class="col-xs-12">
                                    <b>FINAL ACTION </b>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-6 text-center">
                                    Approved ________
                                 </div>
                                 <div class="col-xs-6 text-center">
                                    Disapproved ________
                                    &nbsp;&nbsp;&nbsp;
                                    Due to ________________________
                                 </div>
                              </div>
                              <br><Br><Br><br>
                              <div class="row">
                                 <div class="col-xs-12 text-center">
                                    __________________________________
                                    <br>
                                    Chief Regulator
                                 </div>
                              </div>
                           </td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>
