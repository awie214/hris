<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employeesleavemonetization";
   $whereClause = "WHERE Status = 'Approved'";
   $rs = SelectEach($table,$whereClause);
   if ($rs) $rowcount = mysqli_num_rows($rs);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <table border="1">
            <thead>
               <tr>
                  <td colspan="10" class="text-center">
                     <?php rptHeader(getvalue("RptName")); ?>
                  </td>
               </tr>
               <tr class="colHEADER">
                  <th>#</th>
                  <th>NAME</th>
                  <th>VACATION<br>LEAVE</th>
                  <th>SICK<br>LEAVE</th>
                  <th>TOTAL</th>
                  <th>DAY TO BE<br>MONETIZED</th>
                  <th>BASIC<br>SALARY</th>
                  <th>CF</th>
                  <th>AMOUNT</th>
                  <th>JUSTIFICATION</th>
               </tr>
            </thead>
            <tbody>
               <?php
                  if ($rs) {
                     while ($row = mysqli_fetch_assoc($rs)) {
                        $count++;
                        $emprefid = $row["EmployeesRefId"];
                        $row_emp  = FindFirst("employees","WHERE RefId = '$emprefid'", "LastName, FirstName, MiddleName, RefId");
                        $FullName   = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
                        $Salary   = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","SalaryAmount");
                        if ($Salary == "") $Salary = 0;
                        /*$whereVL = "WHERE EmployeesRefId = ".$row_emp["RefId"]." AND NameCredits = 'VL'";
                        $whereSL = "WHERE EmployeesRefId = ".$row_emp["RefId"]." AND NameCredits = 'SL'";
                        $VL = FindFirst("employeescreditbalance",$whereVL,"BeginningBalance");
                        $SL = FindFirst("employeescreditbalance",$whereSL,"BeginningBalance");*/
                        $VL = floatval($row["VLBalance"]);
                        $SL = floatval($row["SLBalance"]);
                        $Total = floatval($VL + $SL);
                        $VLValue = $row["VLValue"];
                        $SLValue = $row["SLValue"];
                        $TotalValue = intval($VLValue + $SLValue);
                        $Amount = ($Salary * 0.0481927) * $TotalValue;
                        if ($row_emp) {
                           echo '
                              <tr>
                                 <td>'.$count.'</td>
                                 <td>'.$FullName.'</td>
                                 <td>'.$row["VLBalance"].'</td>
                                 <td>'.$row["SLBalance"].'</td>
                                 <td>'.$Total.'</td>
                                 <td>'.$TotalValue.'</td>
                                 <td>'.$Salary.'</td>
                                 <td>0.0481927</td>
                                 <td>'.$Amount.'</td>
                                 <td>'.$row["Reason"].'</td>
                              </tr>
                           ';
                        }
                     }
                  }
               ?>
            </tbody>
         </table>
         <?php
               
         ?>
      </div>
   </body>
</html>