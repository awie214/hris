<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $refid               = getvalue("refid");
   $table               = "training_evaluation";
   $row                 = FindFirst($table,"WHERE RefId = '$refid'","*");
   $emprefid            = $row["EmployeesRefId"];
   $emp_row = FindFirst("employees","WHERE RefId = '$emprefid'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
   if ($emp_row) {
      $LastName = $emp_row["LastName"];
      $FirstName = $emp_row["FirstName"];
      $MiddleName = $emp_row["MiddleName"];
      $ExtName = $emp_row["ExtName"];
      $FullName = $LastName.", ".$FirstName." $ExtName ".$MiddleName;
   } else {
      $FullName = "";
   }
   $empinfo_row = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
   if ($empinfo_row) {
      $Office     = getRecord("office",$empinfo_row["OfficeRefId"],"Name");
      $Position   = getRecord("position",$empinfo_row["PositionRefId"],"Name");
      $Division   = getRecord("Division",$empinfo_row["DivisionRefId"],"Name");
   } else {
      $Office     = "";
      $Position   = "";
      $Division   = "";
   }

   $program = FindFirst("ldmslndprogram","WHERE RefId = '".$row["LDMSLNDProgramRefId"]."'","*");

   function rate($value,$idx) {
      if ($value == $idx) {
         echo '<input type="checkbox" checked disabled>';
      } else {
         echo "";
      }
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style type="text/css">
         .gray {background: gray;}
      </style>
   </head>
   <body>
      <div class="container-fluid">
         <div class="row">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12 text-center">
                     <b>LEARNING AND DEVELOPMENT EVALUATION FORM</b>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table width="100%">
                        <tr>
                           <td style="width: 28.28%;"></td>
                           <td style="width: 14.28%;"></td>
                           <td style="width: 14.28%;"></td>
                           <td style="width: 14.28%;"></td>
                           <td style="width: 14.28%;"></td>
                           <td style="width: 14.28%;"></td>
                        </tr>
                        <tr>
                           <td>
                              <b>TITLE OF L & D PROGRAM</b>
                           </td>
                           <td colspan="5"><?php echo $program["Name"]; ?></td>
                        </tr>
                        <tr>
                           <td>
                              <b>DATE CONDUCTED</b>
                           </td>
                           <td colspan="5">
                              <?php echo date("F d, Y",strtotime($program["StartDate"]))." to ".date("F d, Y",strtotime($program["EndDate"])); ?>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <b>LEARNING SERVICE PROVIDER/S</b>
                           </td>
                           <td colspan="5"><?php echo $program["TrainingInstitution"]; ?></td>
                        </tr>
                        <tr>
                           <td>
                              <b>FACILITATOR/S</b>
                           </td>
                           <td colspan="5"><?php echo $program["Facilitator"]; ?></td>
                        </tr>
                        <tr class="colHEADER">
                           <th rowspan="3">TRAINING ASPECTS</th>
                           <th colspan="5">RATINGS</th>
                        </tr>
                        <tr class="colHEADER">
                           <th>1</th>
                           <th>2</th>
                           <th>3</th>
                           <th>4</th>
                           <th>5</th>
                        </tr>
                        <tr class="colHEADER">
                           <th>Poor</th>
                           <th>Fair</th>
                           <th>Satisfactory</th>
                           <th>Very Satisfactory</th>
                           <th>Excellent</th>
                        </tr>
                        <tr>
                           <td colspan="6">
                              <b>I. Design</b>
                           </td>
                        </tr>
                        <tr>
                           <td>Clarity of Training Objectives</td>
                           <td class="text-center"><?php rate($row["Clarity"],"1"); ?></td>
                           <td class="text-center"><?php rate($row["Clarity"],"2"); ?></td>
                           <td class="text-center"><?php rate($row["Clarity"],"3"); ?></td>
                           <td class="text-center"><?php rate($row["Clarity"],"4"); ?></td>
                           <td class="text-center"><?php rate($row["Clarity"],"5"); ?></td>
                        </tr>
                        <tr>
                           <td>Relevance of Content</td>
                           <td class="text-center"><?php rate($row["Relevance"],"1"); ?></td>
                           <td class="text-center"><?php rate($row["Relevance"],"2"); ?></td>
                           <td class="text-center"><?php rate($row["Relevance"],"3"); ?></td>
                           <td class="text-center"><?php rate($row["Relevance"],"4"); ?></td>
                           <td class="text-center"><?php rate($row["Relevance"],"5"); ?></td>
                        </tr>
                        <tr>
                           <td>Sequence Content</td>
                           <td class="text-center"><?php rate($row["Sequence"],"1"); ?></td>
                           <td class="text-center"><?php rate($row["Sequence"],"2"); ?></td>
                           <td class="text-center"><?php rate($row["Sequence"],"3"); ?></td>
                           <td class="text-center"><?php rate($row["Sequence"],"4"); ?></td>
                           <td class="text-center"><?php rate($row["Sequence"],"5"); ?></td>
                        </tr>
                        <tr>
                           <td>Usefulness of Learning Materials</td>
                           <td class="text-center"><?php rate($row["Usefulness"],"1"); ?></td>
                           <td class="text-center"><?php rate($row["Usefulness"],"2"); ?></td>
                           <td class="text-center"><?php rate($row["Usefulness"],"3"); ?></td>
                           <td class="text-center"><?php rate($row["Usefulness"],"4"); ?></td>
                           <td class="text-center"><?php rate($row["Usefulness"],"5"); ?></td>
                        </tr>
                        <tr>
                           <td colspan="6">
                              <b>I. Facilitation</b>
                           </td>
                        </tr>
                        <tr>
                           <td>Mastery of the Subject Matter</td>
                           <td class="text-center"><?php rate($row["Mastery"],"1"); ?></td>
                           <td class="text-center"><?php rate($row["Mastery"],"2"); ?></td>
                           <td class="text-center"><?php rate($row["Mastery"],"3"); ?></td>
                           <td class="text-center"><?php rate($row["Mastery"],"4"); ?></td>
                           <td class="text-center"><?php rate($row["Mastery"],"5"); ?></td>
                        </tr>
                        <tr>
                           <td>Time Management</td>
                           <td class="text-center"><?php rate($row["TimeManagement"],"1"); ?></td>
                           <td class="text-center"><?php rate($row["TimeManagement"],"2"); ?></td>
                           <td class="text-center"><?php rate($row["TimeManagement"],"3"); ?></td>
                           <td class="text-center"><?php rate($row["TimeManagement"],"4"); ?></td>
                           <td class="text-center"><?php rate($row["TimeManagement"],"5"); ?></td>
                        </tr>
                        <tr>
                           <td>Appropriateness of Learning Methodologies</td>
                           <td class="text-center"><?php rate($row["Appropriateness"],"1"); ?></td>
                           <td class="text-center"><?php rate($row["Appropriateness"],"2"); ?></td>
                           <td class="text-center"><?php rate($row["Appropriateness"],"3"); ?></td>
                           <td class="text-center"><?php rate($row["Appropriateness"],"4"); ?></td>
                           <td class="text-center"><?php rate($row["Appropriateness"],"5"); ?></td>
                        </tr>
                        <tr>
                           <td>Professional Conduct</td>
                           <td class="text-center"><?php rate($row["Professional"],"1"); ?></td>
                           <td class="text-center"><?php rate($row["Professional"],"2"); ?></td>
                           <td class="text-center"><?php rate($row["Professional"],"3"); ?></td>
                           <td class="text-center"><?php rate($row["Professional"],"4"); ?></td>
                           <td class="text-center"><?php rate($row["Professional"],"5"); ?></td>
                        </tr>
                        <tr>
                           <td colspan="6">
                              <b>III. Administration</b>
                           </td>
                        </tr>
                        <tr>
                           <td>Learning Environment</td>
                           <td class="text-center"><?php rate($row["Learning"],"1"); ?></td>
                           <td class="text-center"><?php rate($row["Learning"],"2"); ?></td>
                           <td class="text-center"><?php rate($row["Learning"],"3"); ?></td>
                           <td class="text-center"><?php rate($row["Learning"],"4"); ?></td>
                           <td class="text-center"><?php rate($row["Learning"],"5"); ?></td>
                        </tr>
                        <tr>
                           <td>Pre-Course Coordination</td>
                           <td class="text-center"><?php rate($row["Coordination"],"1"); ?></td>
                           <td class="text-center"><?php rate($row["Coordination"],"2"); ?></td>
                           <td class="text-center"><?php rate($row["Coordination"],"3"); ?></td>
                           <td class="text-center"><?php rate($row["Coordination"],"4"); ?></td>
                           <td class="text-center"><?php rate($row["Coordination"],"5"); ?></td>
                        </tr>
                        <tr>
                           <td>Assistance to Participants</td>
                           <td class="text-center"><?php rate($row["Assistance"],"1"); ?></td>
                           <td class="text-center"><?php rate($row["Assistance"],"2"); ?></td>
                           <td class="text-center"><?php rate($row["Assistance"],"3"); ?></td>
                           <td class="text-center"><?php rate($row["Assistance"],"4"); ?></td>
                           <td class="text-center"><?php rate($row["Assistance"],"5"); ?></td>
                        </tr>
                        <tr>
                           <td colspan="6">&nbsp;</td>
                        </tr>
                        <tr>
                           <td>
                              <b>OVERALL RATINGS FOR THE TRAINING</b>
                           </td>
                           <td class="text-center"><?php rate($row["Overall"],"1"); ?></td>
                           <td class="text-center"><?php rate($row["Overall"],"2"); ?></td>
                           <td class="text-center"><?php rate($row["Overall"],"3"); ?></td>
                           <td class="text-center"><?php rate($row["Overall"],"4"); ?></td>
                           <td class="text-center"><?php rate($row["Overall"],"5"); ?></td>
                        </tr>
                        <tr>
                           <td colspan="6">
                              <b>Comment/Remarks</b>
                           </td>
                        </tr>
                        <tr>
                           <td colspan="6">
                              <textarea class="form-input" readonly rows="3" style="resize: none;"><?php echo $row["Comments"]; ?></textarea>
                           </td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>
