<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
   $this_year = date("Y",time());
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $count = 0;
            if ($rsEmployees) {
         ?>
            <table style="width: 100%;">
               <thead>
                  <tr>
                     <td colspan="15">
                        <?php
                           rptHeader(getvalue("RptName"));
                        ?>
                     </td>
                  </tr>
                  <tr class="colHEADER">
                     <th>No.</th>
                     <th style="width: 20%;">DIVISION</th>
                     <th>JAN</th>
                     <th>FEB</th>
                     <th>MAR</th>
                     <th>APR</th>
                     <th>MAY</th>
                     <th>JUN</th>
                     <th>JULY</th>
                     <th>AUG</th>
                     <th>SEPT</th>
                     <th>OCT</th>
                     <th>NOV</th>
                     <th>DEC</th>
                  </tr>
               </thead>
               <tbody>
                  <?php
                     /*while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                        $emprefid = $row_emp["RefId"];
                        $emp_division = FindFirst("empinformation","WHERE EmployeesRefId = $emprefid","DivisionRefId");
                        if ($emp_division != "") {
                           
                        }
                     }*/
                     for ($c=1; $c <=12 ; $c++) { 
                        ${$c."_total"} = 0;
                     }
                     $division_array = array();
                     $division_rs = SelectEach("division","");
                     if ($division_rs) {
                        while ($division_row = mysqli_fetch_assoc($division_rs)) {
                           $division_refid = $division_row["RefId"];
                           $division_array[] = $division_row["Name"]."_".$division_refid;
                           for ($a=1; $a <=12 ; $a++) {
                              ${$division_refid."_".$a} = 0;
                           }
                        }
                     }
                     while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                        $emprefid = $row_emp["RefId"];
                        $emp_division = FindFirst("empinformation","WHERE EmployeesRefId = $emprefid","DivisionRefId");
                        if ($emp_division != "") {
                           $emp_leave = SelectEach("employeesleave","WHERE EmployeesRefId = ".$row_emp["RefId"]." AND Status = 'Approved'");
                           if ($emp_leave) {
                              while ($leave_row = mysqli_fetch_assoc($emp_leave)) {
                                 $year = date("Y",strtotime($leave_row["ApplicationDateFrom"]));
                                 if ($year == $this_year) {
                                    $diff = dateDifference($leave_row["ApplicationDateFrom"],$leave_row["ApplicationDateTo"]) + 1;
                                    $month = intval(date("m",strtotime($leave_row["ApplicationDateFrom"])));
                                    ${$emp_division."_".$month} = ${$emp_division."_".$month} + $diff;
                                    ${$month."_total"} = ${$month."_total"} + $diff;
                                 }
                              }
                           }
                        }
                     }
                     foreach ($division_array as $key => $value) {
                        $count++;
                        $div_refid = explode("_", $value)[1];
                        $div_name  = explode("_", $value)[0];
                        echo '<tr>';
                        echo '<td class="text-center">'.$count.'</td>';
                        echo '<td>'.$div_name.'</td>';
                        for ($c=1; $c <=12 ; $c++) { 
                           echo '<td class="text-center">'.${$div_refid."_".$c}.'</td>';
                        }
                        echo '</tr>';
                     }

                     echo '<tr>';
                     echo '<td colspan="2" class="text-center">TOTAL</td>'; 
                     for ($e=1; $e <=12 ; $e++) { 
                        echo '<td class="text-center">'.${$e."_total"}.'</td>';
                     }
                     echo '</tr>';

                  ?>
               </tbody>
            </table>
         
         <?php
            }
         ?>
      </div>
   </body>
</html>