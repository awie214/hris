<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style type="text/css">
         .gray {background: gray;}
         td {vertical-align: top;}
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12 text-center">
                     <b>MONITORING OF THE L & D INTERVENTIONS PROVIDED TO EMPLOYEES</b>
                     <br>
                     For the Year 2019
                     <br>
                     January - June
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table width="100%">
                        <thead>
                           <tr class="colHEADER">
                              <th rowspan="2" style="width: 18.28%;">
                                 Name of Employee
                              </th>
                              <th rowspan="2" style="width: 18.28%;">
                                 Office
                              </th>
                              <th colspan="3">
                                 L & D Intervention Provided
                              </th>
                              <th rowspan="2" style="width: 10.28%;">
                                 Remarks
                              </th>
                           </tr>
                           <tr class="colHEADER">
                              <th style="width: 14.28%;">Title</th>
                              <th style="width: 14.28%;">Date</th>
                              <th style="width: 10.28%;">Training Hours</th>
                           </tr>
                        </thead>
                        <tbody style="font-size: 8pt;">
                           <?php
                              $rs = SelectEach("employees","WHERE (Inactive != 1 OR Inactive IS NULL) ORDER BY LastName");
                              if ($rs) {
                                 while ($row = mysqli_fetch_assoc($rs)) {
                                    $emprefid      = $row["RefId"];
                                    $LastName      = $row["LastName"];
                                    $FirstName     = $row["FirstName"];
                                    $MiddleName    = $row["MiddleName"];
                                    $ExtName       = $row["ExtName"];
                                    $FullName      = $LastName.", ".$FirstName." $ExtName ".$MiddleName;
                                    $empinfo_row = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","OfficeRefId");
                                    if ($empinfo_row) {
                                       $Office     = getRecord("office",$empinfo_row,"Name");
                                    } else {
                                       $Office     = "";
                                    }
                                    $newrs = SelectEach("assigned_courses","WHERE EmployeesRefId = '$emprefid'");
                                    if ($newrs) {
                                       while ($newrow = mysqli_fetch_assoc($newrs)) {
                                          $LDMSLNDProgramRefId = $newrow["LDMSLNDProgramRefId"];
                                          $Training = FindFirst("ldmslndprogram","WHERE RefId = '$LDMSLNDProgramRefId'","*");
                                          echo '<tr>';
                                          echo '<td>'.$FullName.'</td>';
                                          echo '<td>'.$Office.'</td>';
                                          echo '<td>'.$Training["Name"].'</td>';
                                          echo '<td class="text-center">'.date("F d, Y",strtotime($Training["StartDate"])).' to '.date("F d, Y",strtotime($Training["EndDate"])).'</td>';
                                          echo '<td class="text-center">'.(dateDifference($Training["StartDate"] , $Training["EndDate"]) * 8).' hrs</td>';
                                          echo '<td class="text-center"></td>';
                                          echo '</tr>';      
                                       }
                                    } else {
                                       echo '<tr>';
                                       echo '<td>'.$FullName.'</td>';
                                       echo '<td>'.$Office.'</td>';
                                       echo '<td></td>';
                                       echo '<td></td>';
                                       echo '<td></td>';
                                       echo '<td></td>';
                                       echo '</tr>';   
                                    }
                                 }
                              }
                           ?>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>
