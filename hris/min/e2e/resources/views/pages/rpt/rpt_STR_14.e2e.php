<?php
	require_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   require_once pathClass.'0620RptFunctions.e2e.php';
   require_once pathClass.'DTRFunction.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $whereClause .= " ORDER BY LastName LIMIT 5";
   $table = "employees";
   $rsEmployees_week1 = SelectEach($table,$whereClause);
   $rsEmployees_week2 = SelectEach($table,$whereClause);
   $rsEmployees_week3 = SelectEach($table,$whereClause);
   $rsEmployees_week4 = SelectEach($table,$whereClause);

   $month 	= getvalue("txtAttendanceMonth");
   $year    = getvalue("txtAttendanceYear");
   $period  = monthName($month,1)." 01-".cal_days_in_month(CAL_GREGORIAN,$month,$year).", ".$year;

   $cos = FindFirst("empstatus","WHERE Code = 'COS'","RefId");
   function chkDay($day,$month,$year) {
   	$date = $year."-".$month."-".$day;
   	$curr_day = date("D",strtotime($date));
   	if ($curr_day == "Sat" || $curr_day == "Sun") {
   		return $curr_day;
   	} else {
   		return $day;
   	}
   }
   function rmvValue($value){
   	if ($value > 0) {
   		return $value;
   	} else {
   		return "&nbsp;";
   	}
   }
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php include_once $files["inc"]["pageHEAD"]; ?>
   <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
   <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   <style type="text/css">
   	th, td {
   		font-size: 7pt;
   	}
   	body {
   		background:#ffffff;
		   color:#000;
		   font-size:10pt;
		   font-family:Arial;
		   margin: 0;
   	}
   	@media print {
   		td {
   			font-size: 6pt;
   		}
   	}
   </style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-12 text-center">
						METROPOLITAN WATERWORKS AND SEWERAGE SYSTEM REGULATORY OFFICE
						<br>
						Katipunan Road, Balara, Quezon City
						<br>
						<b>SUMMARY TIME REPORT</b>
						<br>
						For the period <b><?php echo $period; ?></b>
					</div>
				</div>
				<?php
					include_once 'inc/STR_week1.php';
					//echo '<div class="noPrint">'.spacer(100).'</div>';
					include_once 'inc/STR_week2.php';
					//echo '<div class="noPrint">'.spacer(100).'</div>';
					include_once 'inc/STR_week3.php';
					//echo '<div class="noPrint">'.spacer(100).'</div>';
					include_once 'inc/STR_week4.php';
				?>
            <div class="row">
               <div class="col-xs-4">
                  APPLICATIONS/VERIFIED WITH TIME CARDS AND TIME-IN:
                  <br>
                  SHEET AND SUMMARIZED BY
                  <br><br><br>
                  <?php
                     $prepared           = getvalue("signatory_refid");
                     $prepared_row       = FindFirst("employees","WHERE RefId = '$prepared'","`LastName`,`FirstName`,`MiddleName`");
                     $Signatory_FullName  = $prepared_row["FirstName"]." ".substr($prepared_row["MiddleName"], 0,1).". ".$prepared_row["LastName"];
                     $empinfo_signatory   = FindFirst("empinformation","WHERE EmployeesRefId = '$prepared'","PositionRefId");
                     if ($empinfo_signatory > 0) {
                        $Signatory_Position = getRecord("position",$empinfo_signatory,"Name");
                     } else {
                        $Signatory_Position = "";
                     }
                     echo $Signatory_FullName."<br>".$Signatory_Position;
                  ?>
               </div>
               <div class="col-xs-4">
                  CERTIFIED CORRECT:
                  <br><br><br>
                  <?php
                     $prepared           = getvalue("prepared_by_refid");
                     $prepared_row       = FindFirst("employees","WHERE RefId = '$prepared'","`LastName`,`FirstName`,`MiddleName`");
                     $Signatory_FullName  = $prepared_row["FirstName"]." ".substr($prepared_row["MiddleName"], 0,1).". ".$prepared_row["LastName"];
                     $empinfo_signatory   = FindFirst("empinformation","WHERE EmployeesRefId = '$prepared'","PositionRefId");
                     if ($empinfo_signatory > 0) {
                        $Signatory_Position = getRecord("position",$empinfo_signatory,"Name");
                     } else {
                        $Signatory_Position = "";
                     }
                     echo $Signatory_FullName."<br>".$Signatory_Position;
                  ?>
               </div>
               <div class="col-xs-4">
                  APPROVED BY:
                  <br><br><br>
                  <?php
                     $corrected           = getvalue("corrected_by_refid");
                     $corrected_row       = FindFirst("employees","WHERE RefId = '$corrected'","`LastName`,`FirstName`,`MiddleName`");
                     $Signatory_FullName  = $corrected_row["FirstName"]." ".substr($corrected_row["MiddleName"], 0,1).". ".$corrected_row["LastName"];
                     $empinfo_signatory   = FindFirst("empinformation","WHERE EmployeesRefId = '$corrected'","PositionRefId");
                     if ($empinfo_signatory > 0) {
                        $Signatory_Position = getRecord("position",$empinfo_signatory,"Name");
                     } else {
                        $Signatory_Position = "";
                     }
                     echo $Signatory_FullName."<br>".$Signatory_Position;
                  ?>
               </div>
            </div>
            <div class="row margin-top" style="page-break-after: always;">
               <div class="col-xs-12">
                  <textarea class="form-input" style="resize: none;" rows="10"></textarea>
               </div>
            </div>
			</div>
		</div>
	</div>
</body>
</html>