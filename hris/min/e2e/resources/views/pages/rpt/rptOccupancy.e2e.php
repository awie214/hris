<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader(getRptName(getvalue("drpReportKind")));
         ?>
         <p class="txt-center">For the Month of <u><?php echo monthName(date("m",time()),1).", ".date("Y",time()) ?></u> </p>

         <table border="1">
            <tr>
               <th class="padd5" rowspan=2 style="width:*%">AGENCY/POSITION</th>
               <th class="padd5" rowspan=2 style="width:10%">ITEM NO.</th>
               <th class="padd5" rowspan=2 style="width:10%">NO.</th>
               <th class="padd5" rowspan=2 style="width:10%">CES LEVEL</th>
               <th class="padd5" rowspan=2 style="width:8%">SALARY GRADE</th>

               <th class="padd5" colspan=4 style="width:38%">APPOINTEE</th>
               <th class="padd5" colspan=2 style="width:19%">OCCUPANT</th>
            </tr>
            <tr>
               <th class="padd5" style="width:20%">Name</th>
               <th class="padd5" style="width:8%">Status of Appointment</th>
               <th class="padd5" style="width:8%">Date of Effectivity</th>
               <th class="padd5" style="width:*">Present Assignment</th>

               <th class="padd5" style="width:10%">Name</th>
               <th class="padd5" style="width:9%">Position Per Appointment Issued</th>
            </tr>
            <?php for ($j=1;$j<=15;$j++) {?>
               <tr>
                  <td>&nbsp;</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
               <tr>
            <?php } ?>
         </table>
         <div><i>
            <div> * Permanent Status was granted prior tothe effectivity of the Career Executive Eligibility.</div>
            <div> ** All appointments are made by the head of the agency.</div>
            <div> *** CES shal only cover positions appointed by the President (CSC MC No.7s 1010).</div>
         </i></div>
         <br><br>
         <p>
            <div class="row">
               <div class="col-xs-2 txt-right">Prepared By:</div>
               <div class="col-xs-4"></div>
               <div class="col-xs-2 txt-right">Certified correct / Approved By:</div>
               <div class="col-xs-4"></div>
            </div>
            <div class="row">
               <div class="col-xs-2"></div>
               <div class="col-xs-4">________________________</div>
               <div class="col-xs-2"></div>
               <div class="col-xs-3">________________________</div>
               <div class="col-xs-1"></div>
            </div>
         </p>

      </div>
   </body>
</html>