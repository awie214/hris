<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   //include 'incRptParam.e2e.php';
   //include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause = " WHERE (Inactive != 1 OR Inactive IS NULL)";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $count = 0;
            if ($rsEmployees) {
         ?>
         <?php
            rptHeader(getvalue("RptName"));
         ?>
         <center>
         <table style="width: 80%;">
            <thead>
               <tr class="colHEADER">
                  <th style="width: 20%;">Civil Status</th>
                  <th style="width: 20%;">Male</th>
                  <th style="width: 20%;">Female</th>
                  <th style="width: 20%;">No Gender</th>

               </tr>
            </thead>
            <tbody>
               <?php
                  $M_20_Below = 0;
                  $M_21_30    = 0;
                  $M_31_40    = 0;
                  $M_41_50    = 0;
                  $M_51_60    = 0;
                  $M_61_Above = 0;
                  
                  $F_20_Below = 0;
                  $F_21_30    = 0;
                  $F_31_40    = 0;
                  $F_41_50    = 0;
                  $F_51_60    = 0;
                  $F_61_Above = 0;

                  $_20_Below  = 0;
                  $_21_30     = 0;
                  $_31_40     = 0;
                  $_41_50     = 0;
                  $_51_60     = 0;
                  $_61_Above  = 0;
                  
                  $M_NoBirthday = 0;
                  $F_NoBirthday = 0;
                  $_NoBirthday  = 0;

                  $M = 0;
                  $F = 0;
                  $_ = 0;

                  while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                     $BirthDate  = $row_emp["BirthDate"];
                     $Today      = date("Y-m-d",time());
                     $Age        = intval(dateDifference($BirthDate,$Today) / 365);

                     if ($row_emp["BirthDate"] != "") {
                        if ($Age <= 20) {
                           ${$row_emp["Sex"]."_20_Below"}++;
                        } else if ($Age >= 21 && $Age <= 30) {
                           ${$row_emp["Sex"]."_21_30"}++;
                        } else if ($Age >= 31 && $Age <= 40) {
                           ${$row_emp["Sex"]."_31_40"}++;
                        } else if ($Age >= 41 && $Age <= 50) {
                           ${$row_emp["Sex"]."_41_50"}++;
                        } else if ($Age >= 51 && $Age <= 60) {
                           ${$row_emp["Sex"]."_51_60"}++;
                        } else if ($Age >= 61) {
                           ${$row_emp["Sex"]."_61_Above"}++;
                        }
                     } else {
                        if ($row_emp["Sex"] != "") {
                           ${$row_emp["Sex"]."_NoBirthday"}++;
                        } else {
                           $_NoBirthday++;
                        }
                     }
                     if ($row_emp["Sex"] != "") {
                        ${$row_emp["Sex"]}++;
                     } else {
                        $_++;
                     }
                  }
               ?>
               <tr>
                  <td>20 and Below</td>
                  <td class="text-center">
                     <?php echo $M_20_Below; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $F_20_Below; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $_20_Below; ?>
                  </td>
               </tr>
               <tr>
                  <td>21 to 30</td>
                  <td class="text-center">
                     <?php echo $M_21_30; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $F_21_30; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $_21_30; ?>
                  </td>
               </tr>
               <tr>
                  <td>31 to 40</td>
                  <td class="text-center">
                     <?php echo $M_31_40; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $F_31_40; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $_31_40; ?>
                  </td>
               </tr>
               <tr>
                  <td>41 to 50</td>
                  <td class="text-center">
                     <?php echo $M_41_50; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $F_41_50; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $_41_50; ?>
                  </td>
               </tr>
               <tr>
                  <td>51 to 60</td>
                  <td class="text-center">
                     <?php echo $M_51_60; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $F_51_60; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $_51_60; ?>
                  </td>
               </tr>
               <tr>
                  <td>61 and Above</td>
                  <td class="text-center">
                     <?php echo $M_61_Above; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $F_61_Above; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $_61_Above; ?>
                  </td>
               </tr>
               <tr>
                  <td>No Birthday Data</td>
                  <td class="text-center">
                     <?php echo $M_NoBirthday; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $F_NoBirthday; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $_NoBirthday; ?>
                  </td>
               </tr>
               <tr>
                  <td>No Birthday Data And Gender</td>
                  <td colspan="3" class="text-center">
                     <?php echo $_NoBirthday; ?>
                  </td>
               </tr>
               <tr>
                  <td>Total: <?php echo mysqli_num_rows($rsEmployees); ?></td>
                  <td class="text-center">
                     <?php echo $M; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $F; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $_; ?>
                  </td>
               </tr>
            </tbody>
         </table>
         </center>
         <?php
            }
         ?>
      </div>
   </body>
</html>