<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $count = 0;
   $quarter = getvalue("quarter");
   $year    = getvalue("year");
   $where   = "WHERE Year(ServiceStartDate) = '$year'";
   switch ($quarter) {
      case '1':
         $quarter_label = "1st";
         $where .= " AND Month(ServiceStartDate) BETWEEN '01' AND '03'";
         break;
      case '2':
         $quarter_label = "2nd";
         $where .= " AND Month(ServiceStartDate) BETWEEN '04' AND '06'";
         break;
      case '3':
         $quarter_label = "3rd";
         $where .= " AND Month(ServiceStartDate) BETWEEN '07' AND '09'";
         break;
      case '4':
         $quarter_label = "4th";
         $where .= " AND Month(ServiceStartDate) BETWEEN '10' AND '12'";
         break;
      default:
         $where = "";
         $quarter_label = "";
         break;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style type="text/css">
         .gray {background: gray;}
      </style>
   </head>
   <body>
      <div class="container-fluid">
         <?php
            rptHeader("Individual Comptency Data");
         ?>
         <br><br>
         <?php
            $rsEmployee = SelectEach("employees","");
            if ($rsEmployee) {
               while ($employee_row = mysqli_fetch_assoc($rsEmployee)) {
                  $emprefid      = $employee_row["RefId"];
                  $LastName      = $employee_row["LastName"];
                  $FirstName     = $employee_row["FirstName"];
                  $MiddleName    = $employee_row["MiddleName"];
                  $FullName      = $LastName.", ".$FirstName." ".$MiddleName;
                  $where         = "WHERE EmployeesRefId = '$emprefid' AND Year = '$year'";
                  $check         = FindFirst("individual_competency_data",$where,"RefId");
                  if (intval($check) > 0) {
                     $empinfo = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","`PositionRefId`,`OfficeRefId`");
                     if ($empinfo) {
                        $Position   = getRecord("Position",$empinfo["PositionRefId"],"Name");
                        $Office     = getRecord("Office",$empinfo["OfficeRefId"],"Name");
                     } else {
                        $Position = $Office = "";
                     }   
                  
         ?>
         <div class="row">
            <div class="col-xs-12">
               <table>
                  <tr>
                     <td style="width: 16.66%;">
                     </td>
                     <td style="width: 16.66%;">
                     </td>
                     <td style="width: 16.66%;">
                     </td>
                     <td style="width: 16.66%;">
                     </td>
                     <td style="width: 16.66%;">
                     </td>
                     <td style="width: 16.66%;">
                     </td>
                  </tr>
                  <tr>
                     <td class="gray">Name of Talent</td>
                     <td colspan="5"><b><?php echo $FullName; ?></b></td>
                  </tr>
                  <tr>
                     <td class="gray">Position Title</td>
                     <td colspan="3"><b><?php echo $Position; ?></b></td>
                     <td rowspan="2" class="gray text-center">Profile Match</td>
                     <td rowspan="2" class="text-center">0%</td>
                  </tr>
                  <tr>
                     <td class="gray">Office/Area</td>
                     <td colspan="3"><b><?php echo $Office; ?></b></td>
                  </tr>
                  <tr class="text-center">
                     <td>
                        <b>COMPETENCIES REQUIRED OF THE POSITION</b>
                     </td>
                     <td>
                        <b>REQUIRED<br>LEVEL OF<br>PROFICIENCY</b>
                     </td>
                     <td>
                        <b>ASSESSMENT RESULT</b>
                     </td>
                     <td colspan="2">
                        <b>INTERVENTION<br>/DEVELOPMENT<br>REQUIREMENT</b>
                     </td>
                     <td>
                        <b>FOCUS BEHAVIOR</b>
                     </td>
                  </tr>
                  <tr>
                     <?php
                        $rs = SelectEach("individual_competency_data",$where);
                        if ($rs) {
                           while ($row = mysqli_fetch_assoc($rs)) {
                              echo '<tr>';
                              echo '<td>'.getRecord("competency",$row["CompetencyRefId"],"Name").'</td>';
                              echo '<td class="text-center">'.$row["Level"].'</td>';
                              echo '<td class="text-center">'.$row["Result"].'</td>';
                              echo '<td class="text-center" colspan="2">'.$row["Requirement"].'</td>';
                              echo '<td class="text-center">'.$row["Behavior"].'</td>';
                              echo '</tr>';
                           }
                        }
                     ?>
                  </tr>
               </table>
            </div>
         </div>
         <?php
                  }
               }
            }
         ?>
      </div>
   </body>
</html>
