<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $refid      = getvalue("refid");
   $row        = FindFirst("overtime_request","WHERE RefId = '$refid'","*");
   $FiledDate  = date("F d, Y",strtotime($row["FiledDate"]));
   $emprefid   = $row["EmployeesRefId"];
   $StartDate  = $row["StartDate"];
   $EndDate    = $row["EndDate"];
   $emp_row    = FindFirst("employees","WHERE RefId = '$emprefid'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
   if ($emp_row) {
      $LastName = $emp_row["LastName"];
      $FirstName = $emp_row["FirstName"];
      $MiddleName = $emp_row["MiddleName"];
      $ExtName = $emp_row["ExtName"];
      $FullName = $LastName.", ".$FirstName." $ExtName ".$MiddleName;
   } else {
      $FullName = "";
   }

   $Signatory1       = $row["Signatory1"];
   $Signatory2       = $row["Signatory2"];
   $Signatory3       = $row["Signatory3"];

   $s1_row           = FindFirst("signatories","WHERE RefId = '$Signatory1'","*");
   $s2_row           = FindFirst("signatories","WHERE RefId = '$Signatory2'","*");

   $s1_name          = $s1_row["Name"];
   $s1_position      = getRecord("position",$s1_row["PositionRefId"],"Name");

   $s2_name          = $s2_row["Name"];
   $s2_position      = getRecord("position",$s2_row["PositionRefId"],"Name");

   if($StartDate == $EndDate) {
      $date = date("F d, Y",strtotime($StartDate));
   } else {
      $date = date("d",strtotime($StartDate))." - ".date("d F Y",strtotime($EndDate));
   }

   $empinfo_row = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
   if ($empinfo_row) {
      $Office = getRecord("office",$empinfo_row["OfficeRefId"],"Name");
      $Division = getRecord("Division",$empinfo_row["DivisionRefId"],"Name");
      $Position = getRecord("position",$empinfo_row["PositionRefId"],"Name");
   } else {
      $Office = "";
      $Position = "";
      $Division = "";
   }
   $stat = $row["WithPay"];
   if (intval($stat) > 0) {
      $stat = "OT Pay";
   } else {
      $stat = "CTO";
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <script type="text/javascript">
         $(document).ready(function () {
            $("#authority_form, #accomplishment_form, #form_1, #Content").hide();
            <?php
               if (strtotime($EndDate) <= strtotime(date("Y-m-d",time()))) {
                  echo '$("#form_1").hide();'."\n";
                  echo '$("#accomplishment_form").hide();'."\n";
                  echo '$("#authority_form").show();'."\n";
                  echo '$("#Content").show();'."\n";
               } else {
                  echo '$("#Content").hide();'."\n";
                  echo '$("#form_1").show();'."\n";
                  echo '$("#accomplishment_form").show();'."\n";
                  echo '$("#authority_form").hide();'."\n";
               }
            ?>
            $("#authority_form").click(function () {
               $("#form_1").toggle();
            });
            $("#accomplishment_form").click(function () {
               $("#Content").toggle();
            });
         });
      </script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row">
            <div class="col-xs-12 text-right">
               <button class="btn-cls2-sea noPrint" id="authority_form">
                  View Authority Form
               </button>
               <button class="btn-cls2-sea noPrint" id="accomplishment_form">
                  View Accomplishment Form
               </button>
            </div>
         </div>
         <div class="row" style="page-break-after: always;" id="form_1">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12">
                     <?php
                        rptHeader("Authority for Overtime Services");
                     ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table border="2">
                        <tr>
                           <td colspan="3" class="text-center">
                              <b>AUTHORITY FOR OVERTIME SERVICES</b>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <b>Date: <?php echo $date; ?></b>
                           </td>
                           <td colspan="2">
                              <b>Division/Office: <?php echo $Division." / ".$Office; ?></b>
                           </td>
                        </tr>
                        <tr>
                           <td colspan="3">
                              <b>This is to request the following employees to render Overtime Services for the month of <u><?php echo date("F",strtotime($EndDate)); ?></u>.</b> <i>(add additional sheets,if necessary)</i>
                           </td>
                        </tr>
                        <tr>
                           <td style="width: 50%;" class="text-center">
                              <b>Name of Employee/s</b>
                           </td>
                           <td style="width: 25%;" class="text-center">
                              <b>With Pay</b>
                           </td>
                           <td style="width: 25%;" class="text-center">
                              <b>CTO</b>
                           </td>
                        </tr>
                        <tr>
                           <td><b><?php echo $FullName; ?></b></td>
                           <td class="text-center">
                              <?php
                                 if ($row["WithPay"] == 1) {
                                    echo '<input type="checkbox" checked>';
                                 }
                              ?>
                           </td>
                           <td class="text-center">
                              <?php
                                 if ($row["WithPay"] == 0) {
                                    echo '<input type="checkbox" checked>';
                                 }
                              ?>
                           </td>
                        </tr>
                        <?php
                           for ($i=0; $i <= 2; $i++) { 
                              echo '<tr>';
                              echo '<td>&nbsp;</td>';
                              echo '<td></td>';
                              echo '<td></td>';
                              echo '</tr>';
                           }
                        ?>
                        
                        <tr>
                           <td colspan="3">
                              <b>Activities/expected output of the Division/Office to be accomplished within the requested period:</b>
                              <div class="row">
                                 <div class="col-xs-12" style="padding: 20px;">
                                    <?php echo convertBR($row["Justification"]); ?>
                                 </div>
                              </div>
                              <br>
                              <div class="row">
                                 <div class="col-xs-12 text-center">
                                    <b>Recommending Approval:</b>
                                    <br><br><br><br><br>
                                    <b><?php echo $s1_name."<br>".$s1_position; ?></b>
                                 </div>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td colspan="3" class="text-center">
                              <b>CERTIFICATION</b>
                           </td>
                        </tr>
                        <tr>
                           <td rowspan="2">
                              <div class="row">
                                 <div class="col-xs-12">
                                    <input type="checkbox" name="">&nbsp;<b>Funds Available</b>
                                    <br>
                                    <input type="checkbox" name="">&nbsp;<b>Within 5% of the total salaries of authorized positions</b>
                                    <br>
                                    <input type="checkbox" name="">&nbsp;<b>Within 5% of the total aggregate sum of employees basic salary per annum</b>
                                 </div>
                              </div>
                           </td>
                           <td colspan="2" valign="top">
                              <b>Remarks, if any:</b>
                              <br><br><br><br>
                           </td>
                        </tr>
                        <tr>
                           <td colspan="2" valign="top">
                              <b>Certified by:</b>
                              <br><br><br><br>
                              <div class="text-center">
                                 <b><?php echo $s2_name."<br>".$s2_position; ?></b>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td colspan="3" class="text-center">
                              <b>APPROVAL</b>
                           </td>
                        </tr>
                        <tr>
                           <td colspan="3">
                              <div class="row">
                                 <div class="col-xs-2 text-center">
                                 </div>
                                 <div class="col-xs-4 text-center">
                                    <input type="checkbox" name="">&nbsp;<b>Approved</b>
                                 </div>
                                 <div class="col-xs-4 text-center">
                                    <input type="checkbox" name="">&nbsp;<b>Disapproved</b>
                                 </div>
                              </div>
                              <br><br>
                              <div class="row">
                                 <div class="col-xs-12 text-center">
                                    <b>JENNIFER J. TAN</b>
                                    <br>
                                    <b>Director, Administrative and Financial Office</b>
                                    <br>
                                    <b>OIC-Human Resources Management Unit</b>
                                 </div>
                              </div>
                           </td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
         <div class="row" style="page-break-after: always;" id="Content">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12">
                     <?php
                        rptHeader("Authority for Overtime Services");
                     ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table border="2">
                        <tr>
                           <td style="width: 16.66%;"></td>
                           <td style="width: 16.66%;"></td>
                           <td style="width: 16.66%;"></td>
                           <td style="width: 16.66%;"></td>
                           <td style="width: 16.66%;"></td>
                           <td style="width: 16.66%;"></td>
                        </tr>
                        <tr>
                           <td colspan="6" class="text-center">
                              <b>REPORT ON OVERTIME SERVICES RENDERED</b>
                           </td>
                        </tr>
                        <tr>
                           <td colspan="6">
                              <div class="row">
                                 <div class="col-xs-6">
                                    <b>Period Covered: <?php echo $date; ?></b>
                                 </div>
                                 <div class="col-xs-6">
                                    <b>Division/Office: <?php echo $Division." / ".$Office; ?></b>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-6">
                                    <b>Name: <?php echo $FullName; ?></b>
                                 </div>
                                 <div class="col-xs-6">
                                    <b>Position: <?php echo $Position; ?></b>
                                 </div>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td colspan="6">
                              <div class="row">
                                 <div class="col-xs-2">
                                    <b>Purpose:</b>
                                 </div>
                                 <div class="col-xs-10" style="padding-left: 20px; padding-right: 20px; ">
                                    <?php echo convertBR($row["Justification"]); ?>
                                 </div>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td colspan="6" class="text-center">
                              <b>OUTPUT/WORKING HOURS</b>
                           </td>
                        </tr>
                        <tr>
                           <td rowspan="2" class="text-center">
                              <b>Date</b>
                           </td>
                           <td rowspan="2" colspan="3" class="text-center">
                              <b>
                                 Description of Output/Activity
                              </b>
                           </td>
                           <td colspan="2" class="text-center">
                              <b>Overtime Hours</b>
                           </td>
                        </tr>
                        <tr>
                           <td class="text-center"> 
                              <b>Authorized</b>
                           </td>
                           <td class="text-center"> 
                              <b>Actual</b>
                           </td>
                        </tr>
                        <tr>
                           <td class="text-center"><?php echo $date; ?></td>
                           <td colspan="3">
                              <?php echo convertBR($row["Accomplishment"]); ?>
                           </td>
                           <td class="text-center">
                              <?php echo $row["HoursReq"]; ?>
                           </td>
                           <td></td>
                        </tr>
                        <?php
                           for ($i=0; $i <= 2; $i++) { 
                              echo '<tr>';
                              echo '<td>&nbsp;</td>';
                              echo '<td colspan="3"></td>';
                              echo '<td></td>';
                              echo '<td></td>';
                              echo '</tr>';
                           }
                        ?>
                        <tr>
                           <td colspan="2" class="text-center"> 
                              <b>Prepared by:</b>
                              <br><br><br><br>
                              <b><?php echo strtoupper($FullName); ?></b>
                              <br>
                              Signature over Printed Name of Employee
                           </td>
                           <td colspan="2" class="text-center"> 
                              <b>Recommending Approval:</b>
                              <br><br><br><br>
                              <b><?php echo $s1_name."<br>".$s1_position; ?></b>
                              <br>
                              Signature over Printed Name of Division Chief/Office Head
                           </td>
                           <td colspan="2" class="text-center"> 
                              <b>Approved by:</b>
                              <br><br><br><br>
                              <b><?php echo $s2_name."<br>".$s2_position; ?></b>
                              <br>
                              Signature over Printed Name of Approving Official
                           </td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>