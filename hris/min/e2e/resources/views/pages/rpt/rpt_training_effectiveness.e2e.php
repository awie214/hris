<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $count = 0;
   $count_3 = 0;
   $quarter = getvalue("quarter");
   $year    = getvalue("year");
   $where   = "WHERE Year(ServiceStartDate) = '$year'";

   switch ($quarter) {
      case '1':
         $quarter_label = "1st";
         $where .= " AND Month(ServiceStartDate) BETWEEN '01' AND '03'";
         break;
      case '2':
         $quarter_label = "2nd";
         $where .= " AND Month(ServiceStartDate) BETWEEN '04' AND '06'";
         break;
      case '3':
         $quarter_label = "3rd";
         $where .= " AND Month(ServiceStartDate) BETWEEN '07' AND '09'";
         break;
      case '4':
         $quarter_label = "4th";
         $where .= " AND Month(ServiceStartDate) BETWEEN '10' AND '12'";
         break;
      default:
         $where = "";
         $quarter_label = "";
         break;
   }
   $count_rs     = SelectEach("ldmsreturnobligation",$where);
   if ($count_rs) {
      while ($count_row = mysqli_fetch_assoc($count_rs)) {
         $dummy_rating = $count_row["Rating"];
         if (intval($dummy_rating > 3)) $count_3++;
         $count++;
      }
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style type="text/css">
         
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader("Summary of Training Effectiveness Rating");
         ?>
         <div class="row">
            <div class="col-xs-12 text-center">
               Quarter:<b><u><?php echo $quarter_label; ?></u></b>, Year: <b><u><?php echo $year; ?></u></b>
            </div>
         </div>
         <br><br>
         <div class="row">
            <div class="col-xs-12">
               Part I: Summary
            </div>
         </div>
         <div class="row">
            <div class="col-xs-1"></div>
            <div class="col-xs-11">
               D. Total Number of Training Rated by Supervisor: <b><u><?php echo $count; ?></u></b>
               <br>
               E. Total Number of Training Rated with 3 or higher: <b><u><?php echo $count_3; ?></u></b>
            </div>
         </div>
         <div class="row">
            <div class="col-xs-12">
               Part II: Data
               <br><br>
            </div>
         </div>
         <div class="row">
            <div class="col-xs-12">
               <table style="width: 100%;">
                  <thead>
                     <tr class="colHEADER">
                        <th>Name</th>
                        <th>Title of<br>Training/Seminar/Scholarship</th>
                        <th>Rating</th>
                        <th>Rating Equivalent Remarks</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                        $competency    = SelectEach("ldmsreturnobligation",$where);
                        if ($competency) {
                           $check_name                = "";
                           $row_count  = mysqli_num_rows($competency);
                           while ($competency_row = mysqli_fetch_assoc($competency)) {
                              $emprefid                  = $competency_row["EmployeesRefId"];
                              $row                       = FindFirst("employees","WHERE RefId = '$emprefid'","*");
                              if ($row) {
                                 $LastName      = $row["LastName"];
                                 $FirstName     = $row["FirstName"];
                                 $MiddleName    = $row["MiddleName"];
                                 $MiddleInitial = substr($MiddleName, 0,1);
                                 $FullName      = $FirstName." ".$MiddleInitial.". ".$LastName;
                              } else {
                                 $FullName      = "";
                              }

                              $LDMSLNDInterventionRefId  = $competency_row["LDMSLNDInterventionRefId"];
                              $Rating                    = $competency_row["Rating"];
                              $Equivalent                = $competency_row["Equivalent"];
                              $Name                      = $competency_row["Name"];
                              $ServiceStartDate          = $competency_row["ServiceStartDate"];
                              $InterventionStartDate     = $competency_row["InterventionStartDate"];
                              $InterventionEndDate       = $competency_row["InterventionEndDate"];
                              $ServedStartDate           = $competency_row["ServedStartDate"];
                              $ReturnService             = $competency_row["ReturnService"];
                              $where_intervention        = "WHERE RefId = '$LDMSLNDInterventionRefId'";
                              $comp_row                  = FindFirst("ldmslndintervention",$where_intervention,"*");
                              $Name                      = $comp_row["Name"];
                              switch ($Rating) {
                                 case "1":
                                    $Equivalent = "No visible improvement on the part of the employee or the training program/seminar is of no help to the effective discharge of his/her job/functions.";
                                    break;
                                 case "2":
                                    $Equivalent = "The training program/seminar is somehow helpful to the effective discharge of his/her job/functions.";
                                    break;
                                 case "3":
                                    $Equivalent = "Satisfactorily contributed to the effective discharge of his/her job/functions.";
                                    break;
                                 case "4":
                                    $Equivalent = "Very satisfactorily contributed to the effective discharge of his/her job/functions.";
                                    break;
                                 case "5":
                                    $Equivalent = "Outstandingly contributed to the effective discharge of his/her job/functions.";
                                    break;
                                 default:
                                    $Equivalent = "";
                                    break;
                              }
                              echo '<tr>';
                                 if ($check_name == $FullName) {
                                    echo '<td>&nbsp;</td>';
                                 } else {
                                    echo '<td>'.$FullName.'</td>';
                                    $check_name = $FullName;
                                 }
                                 echo '<td>'.$Name.'</td>';
                                 echo '<td class="text-center">'.$Rating.'</td>';
                                 echo '<td>'.$Equivalent.'</td>';
                              echo '</tr>';
                           }
                        }
                     ?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </body>
</html>
