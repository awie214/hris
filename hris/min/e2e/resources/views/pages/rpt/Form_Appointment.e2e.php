<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">

      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            if ($rsEmployees) {
               while ($row = mysqli_fetch_assoc($rsEmployees)) {
                  $LastName   = $row["LastName"];
                  $FirstName  = $row["FirstName"];
                  $FullName   = $LastName.", ".$FirstName;
         ?>
         <div class="row" style="page-break-after: always;">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12">
                     <b><i>CS FORM No. 33-A</i></b>
                     <br>
                     Revised 2017
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12" style="background: gray; padding: 20px;">
                     <div class="row" style="border: 1px solid black; padding: 10px; background: white; ">
                        <div class="col-xs-12">
                           <div class="row">
                              <div class="col-xs-12">
                                 Mr./Mrs./Ms.: <?php echo $FullName; ?>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <p style="text-indent: 20px;">
                                    You are hereby appointed as ________<u>(Position Title)</u>_______ (SG/JG/PG__) under ____<u>(Permanent, Temporary etc)</u>_____ status at the _____<u>(Office/Department/Unit)</u>_____ with a compensation rate of ___________________ (P _________) pesos per month.   
                                 </p>
                                 <p style="text-indent: 20px;">
                                    The nature of appointment is ______________________ vice _____________________________________, who ______________________ with Plantilla Item No. ____________ Page _____________.
                                 </p>
                                 <br>
                                 <p style="text-indent: 20px;">
                                    This appointment shall take effect on the date of signing by the appointing officer/authority.
                                 </p>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12 text-right">
                                 Very truly yours,
                                 <br>
                                 <br>
                                 _________________________________
                                 <br>
                                 Appointing Officer/Authority
                                 <br>
                                 <br>
                                 _________________________________
                                 <br>
                                 Date of Signing
                              </div>
                           </div>
                        </div>
                     </div>
                     <br>
                     <br>
                     <div class="row" style="border: 1px solid black; padding: 10px; background: white; ">
                        <div class="col-xs-12">
                           <div class="row">
                              <div class="col-xs-12">
                                 <b>Certification</b>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <p style="text-indent: 20px;">
                                 This is to certify that all requirements and supporting papers pursuant to CSC MC No. ________ have been complied with, reviewed and found to be in order.
                              </p>
                              <p style="text-indent: 20px;">
                                 The position was published at ___________________________ from ___________ to _________, 20_____ and posted in _____________________________________ from___________ to__________, 20_____ in consonance with RA No. 7041. The assessment by the Human Resource Merit Promotion and Selection Board (HRMPSB) started on ______________, 20_____.
                              </p>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12 text-right">
                                 <br>
                                 <br>
                                 _________________________________
                                 <br>
                                 Highest Ranking HRMO
                              </div>
                           </div>
                        </div>
                     </div>
                     <br>
                     <br>
                     <div class="row" style="border: 1px solid black; padding: 10px; background: white; ">
                        <div class="col-xs-12">
                           <div class="row">
                              <div class="col-xs-12">
                                 <b>Certification</b>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <p style="text-indent: 20px;">
                                    This      is     to       certify      that       the      appointee      has     been     screened     and     found     qualified by the majority of the HRMPSB during the deliberation held on __________________. 
                                 </p>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12 text-right">
                                 <br>
                                 _________________________________
                                 <br>
                                 Chairperson, HRMPSB
                              </div>
                           </div>
                        </div>
                     </div>
                     <br>
                     <br>
                     <div class="row" style="border: 1px solid black; padding: 10px; background: white; ">
                        <div class="col-xs-12">
                           <div class="row">
                              <div class="col-xs-12">
                                 <b>CSC Notation</b>
                                 <br>
                              </div>
                           </div>
                           <div class="row margin-top" style="padding: 10px;">
                              <div class="col-xs-12">
                                 <div class="row margin-top" style="border-bottom: 1px solid black;">
                                    <div class="col-xs-12">
                                       &nbsp;
                                    </div>
                                 </div>
                                 <div class="row margin-top" style="border-bottom: 1px solid black;">
                                    <div class="col-xs-12">
                                       &nbsp;
                                    </div>
                                 </div>
                                 <div class="row margin-top" style="border-bottom: 1px solid black;">
                                    <div class="col-xs-12">
                                       &nbsp;
                                    </div>
                                 </div>
                                 <div class="row margin-top" style="border-bottom: 1px solid black;">
                                    <div class="col-xs-12">
                                       &nbsp;
                                    </div>
                                 </div>
                                 <div class="row margin-top" style="border-bottom: 1px solid black;">
                                    <div class="col-xs-12">
                                       &nbsp;
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <br>
                     <br>
                     <div class="row" style="border: 1px solid black; padding: 10px; background: white; ">
                        <div class="col-xs-12">
                           <div class="row">
                              <div class="col-xs-12">
                                 <p style="text-indent: 20px;">
                                    <b>
                                       ANY ERASURE OR ALTERATION ON THE CSC ACTION SHALL NULLIFY OR INVALIDATE THIS APPOINTMENT EXCEPT IF THE ALTERATION WAS AUTHORIZED BY THE COMMISSION.
                                    </b>   
                                 </p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <br>
                     <br>
                     <div class="row" style="border: 1px solid black; padding: 10px; background: white; ">
                        <div class="col-xs-12">
                           <div class="row">
                              <div class="col-xs-6">
                                 Original Copy - for the Appointee
                                 <br>
                                 Original Copy - for the Civil Service Commission
                                 <br>
                                 Original Copy - for the Agency
                              </div>
                              <div class="col-xs-6 text-center">
                                 Acknowledgement
                                 <br>
                                 Received original/photocopy of appointment on ________
                                 <br>
                                 ______________________________
                                 <br>
                                 Appointee
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <?php
               }
            }
         ?>
         
      </div>
   </body>
</html>