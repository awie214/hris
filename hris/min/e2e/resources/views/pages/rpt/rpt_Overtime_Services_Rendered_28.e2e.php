<?php
   
?>
<!DOCTYPE html>
<html>
   <head>
      <?php
         include_once 'pageHEAD.e2e.php';
      ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style>
         /*.bottomline {border-bottom:2px solid black;}*/
         .bold {font-size:600;}
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row">
            <div class="col-xs-8">
               <div class="row">
                  <div class="col-xs-12">
                     <?php
                        rptHeader("Authority for Overtime Services");
                     ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table border="2">
                        <tr>
                           <td style="width: 16.66%;"></td>
                           <td style="width: 16.66%;"></td>
                           <td style="width: 16.66%;"></td>
                           <td style="width: 16.66%;"></td>
                           <td style="width: 16.66%;"></td>
                           <td style="width: 16.66%;"></td>
                        </tr>
                        <tr>
                           <td colspan="6" class="text-center">
                              <b>REPORT ON OVERTIME SERVICES RENDERED</b>
                           </td>
                        </tr>
                        <tr>
                           <td colspan="6">
                              <div class="row">
                                 <div class="col-xs-6">
                                    <b>Period Covered:</b>
                                 </div>
                                 <div class="col-xs-6">
                                    <b>Division/Office:</b>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-6">
                                    <b>Name:</b>
                                 </div>
                                 <div class="col-xs-6">
                                    <b>Position:</b>
                                 </div>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td colspan="6">
                              <div class="row">
                                 <div class="col-xs-2">
                                    <b>Purpose:</b>
                                 </div>
                                 <div class="col-xs-10" style="padding-left: 20px; padding-right: 20px; ">
                                    <br>
                                    <?php bar(); ?>
                                    <br>
                                    <?php bar(); ?>
                                    <br>
                                    <?php bar(); ?>
                                 </div>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td colspan="6" class="text-center">
                              <b>OUTPUT/WORKING HOURS</b>
                           </td>
                        </tr>
                        <tr>
                           <td rowspan="2" class="text-center">
                              <b>Date</b>
                           </td>
                           <td rowspan="2" colspan="3" class="text-center">
                              <b>
                                 Description of Output/Activity
                              </b>
                           </td>
                           <td colspan="2" class="text-center">
                              <b>Overtime Hours</b>
                           </td>
                        </tr>
                        <tr>
                           <td class="text-center"> 
                              <b>Authorized</b>
                           </td>
                           <td class="text-center"> 
                              <b>Actual</b>
                           </td>
                        </tr>
                        <?php
                           for ($i=0; $i <= 9; $i++) { 
                              echo '<tr>';
                              echo '<td>&nbsp;</td>';
                              echo '<td colspan="3"></td>';
                              echo '<td></td>';
                              echo '<td></td>';
                              echo '</tr>';
                           }
                        ?>
                        <tr>
                           <td colspan="2" class="text-center"> 
                              <b>Prepared by:</b>
                              <br><br><br><br>
                              Signature over Printed Name of Employee
                           </td>
                           <td colspan="2" class="text-center"> 
                              <b>Recommending Approval:</b>
                              <br><br><br><br>
                              Signature over Printed Name of Division Chief/Office Head
                           </td>
                           <td colspan="2" class="text-center"> 
                              <b>Approved by:</b>
                              <br><br><br><br>
                              Signature over Printed Name of Approving Official
                           </td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>