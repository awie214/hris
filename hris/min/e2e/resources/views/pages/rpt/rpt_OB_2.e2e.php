<?php
   include_once 'pageHEAD.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   $refid = getvalue("refid");
   $row = FindFirst("employeesauthority","WHERE RefId = $refid","*");
   if ($row) {
   		$Signatory1       = $row["Signatory1"];
        $Signatory2       = $row["Signatory2"];
        $Signatory3       = $row["Signatory3"];

        $s1_row           = FindFirst("signatories","WHERE RefId = '$Signatory1'","*");
        $s2_row           = FindFirst("signatories","WHERE RefId = '$Signatory2'","*");

        $s1_name          = $s1_row["Name"];
        $s1_position      = getRecord("position",$s1_row["PositionRefId"],"Name");

        $s2_name          = $s2_row["Name"];
        $s2_position      = getRecord("position",$s2_row["PositionRefId"],"Name");
        
   		$authority = getRecord("absences",$row["AbsencesRefId"],"Name");
   		$authority = strtoupper($authority);
   		$Remarks = $authority." - ".$row["Remarks"];
   		$Destination = $row["Destination"];
   		$emprefid = $row["EmployeesRefId"];
   		$employees = FindFirst("employees","WHERE RefId = '$emprefid'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
   		if ($employees) {
   			$FirstName = $employees["FirstName"];
   			$LastName = $employees["LastName"];
   			$MiddleName = $employees["MiddleName"];
   			$ExtName = $employees["ExtName"];
   			$FullName = $LastName.", ".$FirstName." $ExtName ".$MiddleName;
   		} else {
   			$FullName = "&nbsp;";
   		}
   		$empinformation = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
   		if ($empinformation) {
   			$OfficeRefId = getRecord("office",$empinformation["OfficeRefId"],"Name");
   			$DivisionRefId = getRecord("division",$empinformation["DivisionRefId"],"Name");
   			$PositionRefId = getRecord("position",$empinformation["PositionRefId"],"Name");
   		} else {
   			$OfficeRefId = $DivisionRefId = $PositionRefId = "&nbsp;";
   		}
   		if ($row["Whole"] == 1) {
   			$start_time = $end_time = "Whole Day";
   		} else {
   			$start_time = convertToHoursMins($row["FromTime"]);
   			$end_time = convertToHoursMins($row["ToTime"]);
   		}
   		$from = $row["ApplicationDateFrom"];
   		$to = $row["ApplicationDateTo"];
   		if ($from == $to) {
   			$date = date("F d, Y",strtotime($from));
   		} else {
   			$date = date("F d, Y",strtotime($from))." to ".date("F d, Y",strtotime($to));;
   		}

   		
   }
?>
<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		td {
			border: 2px solid black;
			vertical-align: top;
			padding: 5px;
			font-size: 9pt;
		}
		.data {
			font-size: 10pt;
			text-transform: uppercase;
		}
	</style>
</head>
<body>
	<div class="container-fluid rptBody">
		<div style="page-break-after: always;">
	        <?php
	            rptHeader($authority." SLIP");
	        ?>
	        <div class="row">
	         	<div class="col-xs-12">
	         		<table width="100%">
	         			<tr>
	         				<td>
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Date:
	         							<br>
	         							<span class="data">
		         							<?php
		         								echo $date;
		         							?>
	         							</span>
	         						</div>
	         					</div>
	         				</td>
	         				<td>
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Time Of Departure
	         							<br>
	         							<span class="data">
		         							<?php
		         								echo $start_time;
		         							?>
	         							</span>
	         						</div>
	         					</div>
	         				</td>
	         				<td>
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Expect Time of Return:
	         							<br>
	         							<span class="data">
		         							<?php
		         								echo $end_time;
		         							?>
	         							</span>
	         						</div>
	         					</div>
	         				</td>
	         			</tr>
	         			<tr>
	         				<td>
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Printed Name and Signature of <br>
	         							Official/Employee:
	         							<br>
	         							<span class="data">
		         							<?php
		         								echo $FullName;
		         							?>
	         							</span>
	         						</div>
	         					</div>
	         				</td>
	         				<td>
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Position Title:
	         							<br>
	         							<span class="data">
		         							<?php
		         								echo $PositionRefId;
		         							?>
	         							</span>
	         						</div>
	         					</div>
	         				</td>
	         				<td>
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Office/Division:
	         							<br>
	         							<span class="data">
		         							<?php
		         								echo $OfficeRefId."<br>".$DivisionRefId;
		         							?>
	         							</span>
	         						</div>
	         					</div>
	         				</td>
	         			</tr>
	         			<tr>
	         				<td colspan="2">
	         					Destination and Purpose of Official Business
	         				</td>
	         				<td rowspan="2">
	         					Recommending Approval:
	         					<?php spacer(100); ?>
	         				</td>
	         			</tr>
	         			<tr>
	         				<td rowspan="4" colspan="2" valign="top" style="min-height: 90px;">
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Destination:
	         							<br>
	         							<?php
	         								if ($Destination != "") {
	         									echo "<b>".$Destination."</b>";
	         								} else {
	         									spacer(90);
	         								}
	         							?>
	         						</div>
	         					</div>
	         					<div class="row margin-top">
	         						<div class="col-xs-12">
	         							Purpose:
	         							<br>
	         							<?php echo "<b>".$Remarks."</b>"; ?>
	         						</div>
	         					</div>
	         				</td>
	         			</tr>
	         			<tr>
	         				<td class="text-center">Immediate Supervisor</td>
	         			</tr>
	         			<tr>
	         				<td>
	         					Approved:
	         					<?php spacer(100); ?>
	         				</td>
	         			</tr>
	         			<tr>
	         				<td class="text-center">Next Higher Official</td>
	         			</tr>
	         			<tr>
	         				<td colspan="2" valign="top">
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Recorded in Security Logbook:
	         						</div>
	         					</div>
	         					<div class="row margin-top">
	         						<div class="col-xs-6">
	         							Time Left Office:
	         						</div>
	         						<div class="col-xs-6 text-right">
	         							________________________________
	         						</div>
	         					</div>
	         					<div class="row margin-top">
	         						<div class="col-xs-6">
	         							Time Returned:
	         						</div>
	         						<div class="col-xs-6 text-right">
	         							________________________________
	         						</div>
	         					</div>
	         					<div class="row margin-top">
	         						<div class="col-xs-12 text-center">
	         							<?php spacer(80); ?>
	         							<br>
	         							Security Officer:
	         						</div>
	         					</div>
	         				</td>
	         				<td valign="top">
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Recorded by HRDD:
	         						</div>
	         					</div>
	         					<div class="row margin-top">
	         						<div class="col-xs-12">
	         							____ Leave Card
	         						</div>
	         					</div>
	         					<div class="row margin-top">
	         						<div class="col-xs-12">
	         							____ Others:
	         						</div>
	         					</div>
	         					<div class="row margin-top">
	         						<div class="col-xs-12 text-center">
	         							<?php spacer(80); ?>
	         							_______________________
	         						</div>
	         					</div>
	         				</td>
	         			</tr>
	         		</table>
	         	</div>
	        </div>
	    </div>
    </div>
</body>
</html>