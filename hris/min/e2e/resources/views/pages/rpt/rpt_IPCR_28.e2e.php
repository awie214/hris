<?php
	include_once 'constant.e2e.php';
	require_once pathClass.'0620functions.e2e.php';
	$ipcr_refid   = getvalue("refid");
	$row = FindFirst("spms_ipcr","WHERE RefId = '$ipcr_refid'","*");
	if ($row) {
		$emprefid 	= $row["EmployeesRefId"];
		$emp_row 	= FindFirst("employees","WHERE RefId = '$emprefid'","*");
		$LastName 	= $emp_row["LastName"];
		$FirstName 	= $emp_row["FirstName"];
		$MiddleName = $emp_row["MiddleName"];
		$ExtName 	= $emp_row["ExtName"];
		$FullName 	= $FirstName." ".$MiddleName." ".$LastName." ".$ExtName; 
		$FullName 	= strtoupper($FullName);

		$empinfo_row = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
		if ($empinfo_row) {
			$Division = strtoupper(getRecord("division",$empinfo_row["DivisionRefId"],"Name"));
			$Position = strtoupper(getRecord("Position",$empinfo_row["PositionRefId"],"Name"));
		} else {
			$Division = $Position = "";
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<?php
		include_once 'pageHEAD.e2e.php';
	?>
	<title></title>
	<style type="text/css">
		thead {
			text-transform: uppercase;
		}
		td {
			padding: 2px;
		}
	</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-12" style="border: 2px solid black;">
						<div class="row">
							<div class="col-xs-12 text-center" style="border-bottom: 2px solid black; padding: 10px;">
								<b>INDIVIDUAL PERFORMANCE COMMITMENT AND REVIEW (IPCR)</b>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 text-center" style="border-bottom: 2px solid black;">
								I,<b><?php echo $FullName; ?></b>, <b><?php echo $Position; ?></b> of <b><?php echo $Division; ?></b> commit to deliver and agree to be rated on the attainment of the following targets in accordance with the indicated measures for the period JANUARY - DECEMBER 2017
								<br>
								<br>
								<u><b><?php echo $FullName; ?></b></u>
								<br>
								Signature Over Printed Name
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12" style="border-bottom: 2px solid black;">
								<b>Recommending</b>
								<br>
								<br>
								<br>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12" style="padding: 5px;">
								<table style="width: 100%" border="1">
                           <thead>
                              <tr>
                                 <th class="text-center" rowspan="2" style="width: 15%;">
                                    Objective
                                 </th>
                                 <th class="text-center" rowspan="2" style="width: 5%;">
                                    %
                                 </th>
                                 <th class="text-center" style="width: 20%;">
                                    Success Indicator<br>(SI)
                                 </th>
                                 <th class="text-center" rowspan="2" style="width: 10%;">
                                    SI TYPE<br>(Qt Qi T)
                                 </th>
                                 <th class="text-center" colspan="4">
                                    Performance<br>Period<br>(<i>please tick applicable<br>periods</i>)
                                 </th>
                                 <th class="text-center" rowspan="2" style="width: 15%;">
                                    Rating Matrix
                                 </th>
                                 <th class="text-center" rowspan="2" style="width: 15%;">
                                    Remarks
                                 </th>
                              </tr>
                              <tr>
                                 <th class="text-center" style="font-size: 8pt;">for an equivalent rating of 3</th>
                                 <th class="text-center" style="width: 5%;">Q1</th>
                                 <th class="text-center" style="width: 5%;">Q2</th>
                                 <th class="text-center" style="width: 5%;">Q3</th>
                                 <th class="text-center" style="width: 5%;">Q4</th>
                              </tr>
                           </thead>
                           <tbody>
                           	<tr>
	                              <td colspan="10" style="background: #999999;">
	                                 <b>STRATEGIC PRIORITIES (If Applicable)</b>
	                              </td>
	                           </tr>
	                           <?php
	                           	$sp_rs = SelectEach("ipcr_strategic_priorities","WHERE spms_ipcr_id = '$ipcr_refid'");
	                           	if ($sp_rs) {
	                           		while ($sp_row = mysqli_fetch_assoc($sp_rs)) {
	                           			echo '<tr>';
	                           			echo '<td>'.$sp_row["objective"].'</td>';
	                           			echo '<td class="text-center">'.$sp_row["percent"].'</td>';
	                           			echo '<td class="text-center">'.$sp_row["success_indicator"].'</td>';
	                           			echo '<td class="text-center">'.$sp_row["sitype"].'</td>';
	                           			if ($sp_row["q1"] == 1) {
	                           				echo '<td class="text-center"><input type="checkbox" checked disabled></td>';	
	                           			} else {
	                           				echo '<td class="text-center"><input type="checkbox" disabled></td>';
	                           			}
	                           			if ($sp_row["q2"] == 1) {
	                           				echo '<td class="text-center"><input type="checkbox" checked disabled></td>';	
	                           			} else {
	                           				echo '<td class="text-center"><input type="checkbox" disabled></td>';
	                           			}
	                           			if ($sp_row["q2"] == 1) {
	                           				echo '<td class="text-center"><input type="checkbox" checked disabled></td>';	
	                           			} else {
	                           				echo '<td class="text-center"><input type="checkbox" disabled></td>';
	                           			}
	                           			if ($sp_row["q2"] == 1) {
	                           				echo '<td class="text-center"><input type="checkbox" checked disabled></td>';	
	                           			} else {
	                           				echo '<td class="text-center"><input type="checkbox" disabled></td>';
	                           			}
	                           			echo '<td class="text-center">'.$sp_row["rating"].'</td>';
	                           			echo '<td>'.$sp_row["remarks"].'</td>';
	                           			echo '</tr>';
	                           		}
	                           	} else {
	                           		echo '<tr>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '</tr>';
	                           	}
	                           ?>
	                           <tr>
	                              <td colspan="10" style="background: #999999;">
	                                 <b>CORE FUNCTIONS</b>
	                              </td>
	                           </tr>
	                           <?php
	                           	$cf_rs = SelectEach("ipcr_core_functions","WHERE spms_ipcr_id = '$ipcr_refid'");
	                           	if ($cf_rs) {
	                           		while ($cf_row = mysqli_fetch_assoc($cf_rs)) {
	                           			echo '<tr>';
	                           			echo '<td>'.$cf_row["objective"].'</td>';
	                           			echo '<td class="text-center">'.$cf_row["percent"].'</td>';
	                           			echo '<td class="text-center">'.$cf_row["success_indicator"].'</td>';
	                           			echo '<td class="text-center">'.$cf_row["sitype"].'</td>';
	                           			if ($cf_row["q1"] == 1) {
	                           				echo '<td class="text-center"><input type="checkbox" checked disabled></td>';	
	                           			} else {
	                           				echo '<td class="text-center"><input type="checkbox" disabled></td>';
	                           			}
	                           			if ($cf_row["q2"] == 1) {
	                           				echo '<td class="text-center"><input type="checkbox" checked disabled></td>';	
	                           			} else {
	                           				echo '<td class="text-center"><input type="checkbox" disabled></td>';
	                           			}
	                           			if ($cf_row["q2"] == 1) {
	                           				echo '<td class="text-center"><input type="checkbox" checked disabled></td>';	
	                           			} else {
	                           				echo '<td class="text-center"><input type="checkbox" disabled></td>';
	                           			}
	                           			if ($cf_row["q2"] == 1) {
	                           				echo '<td class="text-center"><input type="checkbox" checked disabled></td>';	
	                           			} else {
	                           				echo '<td class="text-center"><input type="checkbox" disabled></td>';
	                           			}
	                           			echo '<td class="text-center">'.$cf_row["rating"].'</td>';
	                           			echo '<td>'.$cf_row["remarks"].'</td>';
	                           			echo '</tr>';
	                           		}
	                           	} else {
	                           		echo '<tr>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '</tr>';
	                           	}
	                           ?>
	                           <tr>
	                              <td colspan="10" style="background: #999999;">
	                                 <b>SUPPORT FUNCTIONS (If Applicable)</b>
	                              </td>
	                           </tr>
	                           <?php
	                           	$sf_rs = SelectEach("ipcr_support_functions","WHERE spms_ipcr_id = '$ipcr_refid'");
	                           	if ($sf_rs) {
	                           		while ($sf_row = mysqli_fetch_assoc($sf_rs)) {
	                           			echo '<tr>';
	                           			echo '<td>'.$sf_row["objective"].'</td>';
	                           			echo '<td class="text-center">'.$sf_row["percent"].'</td>';
	                           			echo '<td class="text-center">'.$sf_row["success_indicator"].'</td>';
	                           			echo '<td class="text-center">'.$sf_row["sitype"].'</td>';
	                           			if ($sf_row["q1"] == 1) {
	                           				echo '<td class="text-center"><input type="checkbox" checked disabled></td>';	
	                           			} else {
	                           				echo '<td class="text-center"><input type="checkbox" disabled></td>';
	                           			}
	                           			if ($sf_row["q2"] == 1) {
	                           				echo '<td class="text-center"><input type="checkbox" checked disabled></td>';	
	                           			} else {
	                           				echo '<td class="text-center"><input type="checkbox" disabled></td>';
	                           			}
	                           			if ($sf_row["q2"] == 1) {
	                           				echo '<td class="text-center"><input type="checkbox" checked disabled></td>';	
	                           			} else {
	                           				echo '<td class="text-center"><input type="checkbox" disabled></td>';
	                           			}
	                           			if ($sf_row["q2"] == 1) {
	                           				echo '<td class="text-center"><input type="checkbox" checked disabled></td>';	
	                           			} else {
	                           				echo '<td class="text-center"><input type="checkbox" disabled></td>';
	                           			}
	                           			echo '<td class="text-center">'.$sf_row["rating"].'</td>';
	                           			echo '<td>'.$sf_row["remarks"].'</td>';
	                           			echo '</tr>';
	                           		}
	                           	} else {
	                           		echo '<tr>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '<td>&nbsp;</td>';
	                           		echo '</tr>';
	                           	}
	                           ?>
                           </tbody>
                        </table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>