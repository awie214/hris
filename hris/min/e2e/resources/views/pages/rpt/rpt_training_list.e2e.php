<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $count = 0;
   $quarter = getvalue("quarter");
   $year    = getvalue("year");
   $where   = "WHERE Year(ServiceStartDate) = '$year'";
   switch ($quarter) {
      case '1':
         $quarter_label = "1st";
         $where .= " AND Month(ServiceStartDate) BETWEEN '01' AND '03'";
         break;
      case '2':
         $quarter_label = "2nd";
         $where .= " AND Month(ServiceStartDate) BETWEEN '04' AND '06'";
         break;
      case '3':
         $quarter_label = "3rd";
         $where .= " AND Month(ServiceStartDate) BETWEEN '07' AND '09'";
         break;
      case '4':
         $quarter_label = "4th";
         $where .= " AND Month(ServiceStartDate) BETWEEN '10' AND '12'";
         break;
      default:
         $where = "";
         $quarter_label = "";
         break;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style type="text/css">
         .gray {background: gray;}
      </style>
   </head>
   <body>
      <div class="container-fluid">
         <?php
            rptHeader("List of Proposed Training Programs for the Period January to June 2019");
         ?>
         <br><br>
         <div class="row">
            <div class="col-xs-12">
               <table style="width: 100%;">
                  <thead>
                     <tr class="colHEADER">
                        <th>Training Course</th>
                        <th>Fee</th>
                        <th>Provider</th>
                        <th style="width: 10%;">Venue</th>
                        <th style="width: 10%;">Period</th>
                        <th style="width: 6%;">Year</th>
                        <th style="width: 6%;">Start Date</th>
                        <th style="width: 8%;">End Date</th>
                        <th style="width: 15%;">Type of Training</th>
                        <th style="width: 10%;">Proposed Attendees</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                        $rs = SelectEach("ldmslndprogram","");
                        if ($rs) {
                           while ($row = mysqli_fetch_assoc($rs)) {
                              $Name                = $row["Name"];
                              $TrainingInstitution = $row["TrainingInstitution"];
                              $TrainingType        = $row["TrainingType"];
                              $Semester            = $row["Semester"];
                              $Year                = $row["Year"];
                              $ProposedAttendees   = $row["ProposedAttendees"];
                              $Venue               = $row["Venue"];
                              $StartDate           = $row["StartDate"];
                              $EndDate             = $row["EndDate"];
                              $Cost                = $row["Cost"];
                              echo '
                                 <tr valign="top" style="font-size: 8pt;">
                                    <td>'.$Name.'</td>
                                    <td class="text-center">'.number_format($Cost,2).'</td>
                                    <td>'.$TrainingInstitution.'</td>
                                    <td>'.$Venue.'</td>
                                    <td class="text-center">'.$Semester.'</td>
                                    <td class="text-center">'.$Year.'</td>
                                    <td class="text-center">'.$StartDate.'</td>
                                    <td class="text-center">'.$EndDate.'</td>
                                    <td>'.$TrainingType.'</td>
                                    <td>'.$ProposedAttendees.'</td>
                                 </tr>
                              ';
                           }
                        }
                     ?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </body>
</html>
