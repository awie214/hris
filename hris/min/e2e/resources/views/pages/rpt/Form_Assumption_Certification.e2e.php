<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">

      </style>
   </head>
   <body>
      <div class="container-fluid rptBody" style="font-size: 12pt;">
         <?php
            if ($rsEmployees) {
               while ($row = mysqli_fetch_assoc($rsEmployees)) {
                  $LastName   = $row["LastName"];
                  $FirstName  = $row["FirstName"];
                  $FullName   = $LastName.", ".$FirstName;
         ?>
         <div class="row" style="page-break-after: always;">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12">
                     <b><i>CS FORM No. 4</i></b>
                     <br>
                     Series of 2017
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                  </div>
               </div>
               <br>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <p style="text-indent: 20px;">
                        This is to certify that Ms/Mr. <u><?php echo $FullName; ?></u> has assumed the duties and responsibilities as ______________ of ______________ effective ______________.
                     </p>
                     <p style="text-indent: 20px;">
                        This certification is issued in connection with the issuance of the appointment of Ms/Mr ______________ as ______________.
                     </p>
                     <p style="text-indent: 20px;">
                        Done this ____ day of __________ ________ in ______________.

                     </p>
                     <br>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12 text-right">
                     _________________________________________
                     <br>
                     Head of Office/Department/Unit
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     Date: <?php echo date("F d, Y",time()); ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     Attested by:
                     <br>
                     <br>
                     _________________________________________
                     <br>
                     Highest Ranking HRMO
                  </div>
               </div>
               <br>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-6">
                     201 file<br>Admin<br>COA<br>CSC
                  </div>
                  <div class="col-xs-6">
                     <div class="row" style="border: 1px solid black; ">
                        <div class="col-xs-12">
                           <b><i>For submission to CSCFO within 30 days from  the date of assumption of the appointee</i></b>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <?php
               }
            }
         ?>
         
      </div>
   </body>
</html>