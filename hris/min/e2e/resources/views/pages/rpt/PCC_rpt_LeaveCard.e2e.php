  <?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>

      <style type="text/css">
        td {
          font-style: font-family('Verdana');
          font-size: 7px;
        }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
        <?php 
          include 'inc_rpt_1stSem.e2e.php';
          include 'inc_rpt_2ndSem.e2e.php';
        ?>
      </div>
   </body>
</html>