<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <table border="1">
            <thead>
               <tr>
                  <td colspan="5" class="text-center">
                     <?php rptHeader(getvalue("RptName")); ?>
                     AS OF January 01 2018<br>&nbsp;
                  </td>
               </tr>
               <tr class="colHEADER">
                  <th>#</th>
                  <th>NAME</th>
                  <th>VL/FL</th>
                  <th>SL</th>
                  <th>TOTAL</th>
               </tr>
            </thead>
            <tbody>
               <?php
                  $count = 0;
                  if ($rsEmployees) {
                  while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                     $count++;
                     $FullName   = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
                     $VL = FindFirst("employeescreditbalance","WHERE EmployeesRefId = ".$row_emp["RefId"]." AND NameCredits = 'VL'","BeginningBalance");
                     $SL = FindFirst("employeescreditbalance","WHERE EmployeesRefId = ".$row_emp["RefId"]." AND NameCredits = 'SL'","BeginningBalance");
                     $VL = floatval($VL);
                     $SL = floatval($SL);
                     $Total = floatval($VL + $SL);
                     echo '
                        <tr>
                           <td class="text-center">'.$count.'</td>
                           <td style="padding-left:10px;">'.$FullName.'</td>
                           <td class="text-center">'.$VL.'</td>
                           <td class="text-center">'.$SL.'</td>
                           <td class="text-center">'.$Total.'</td>
                        </tr>
                     ';
                     }
                  }
               ?>
            </tbody>
         </table>
         <br>
         <div class="row margin-top">
            <div class="col-xs-12">
               * Tentative computation of leave credits subject to audit
            </div>
         </div>
         <?php
               
         ?>
      </div>
   </body>
</html>