<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   //$whereClause = "LIMIT 10";
   $table = "employees";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);

   if ($dbg) {
      echo $whereClause;
   }
   $year  = getvalue("txtAttendanceYear");
   $month = getvalue("txtAttendanceMonth");
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $errmsg = "";
            rptHeader(getRptName(getvalue("drpReportKind")));
            if ($rsEmployees && $errmsg == "")
            {
         ?>
         <p class="txt-center">YEAR : <u><?php echo monthName($month,1).", ".$year; ?></u></p>
         <br>
         <br>

         <table border="1" style="width: 100%;">
            <tr>
               <th>Employee Name</th>
               <th>VL</th>
               <th>FL</th>
               <th>SL</th>
               <th>SPL</th>
            </tr>
            <?php 
               while ($row = mysqli_fetch_assoc($rsEmployees) ) {
                  if ($p_filter_value == "0" || $p_filter_table == "") {
                     $emprefid = $row["RefId"];
                  } else {
                     $emprefid   = $row["EmployeesRefId"];
                  }
                  $vl = $sl = $fl = $spl = 0;
                  /*$dtr = FindFirst("dtr_process","WHERE EmployeesRefId = '$emprefid' AND Month = '$month' AND Year = '$year'","*");
                  if ($dtr) {
                     $vl      = $dtr["VL_Used"];
                     $sl      = $dtr["SL_Used"];
                     $fl_day  = $dtr["FL_Days"]; 
                     $spl     = $dtr["SPL_Used"];
                     $fl      = 0;
                     if($fl_day != "") {
                        $fl_arr = explode("|", $fl_day);
                        foreach ($fl_arr as $key => $value) {
                           if ($value != "") $fl++;
                        }
                     }
                  } else {
                     $vl = $sl = $fl = $spl = 0;
                  }*/
                  $arr = computeCredit($emprefid,"01",$month,$year);
                  if ($arr) {
                     $vl = $arr["VL"];
                     $fl = $arr["FL"];
                     $sl = $arr["SL"];
                     $spl = $arr["SPL"];
                     if ($vl < 0) $vl = 0;
                     if ($sl < 0) $sl = 0;
                     if ($fl < 0) $fl = 0;
                     if ($spl < 0) $spl = 0;
                  }
            ?>
            <tr>
               <td class="pad-left"><?php echo $row['LastName'].', '.$row['FirstName'].', '.$row['MiddleName'];?></td>
               <td class="txt-center"><?php echo number_format($vl,3); ?></td>
               <td class="txt-center"><?php echo number_format($fl,3); ?></td>
               <td class="txt-center"><?php echo number_format($sl,3); ?></td>
               <td class="txt-center"><?php echo number_format($spl,3); ?></td>
            </tr>
            <?php
               }
               echo "RECORD COUNT : ".mysqli_num_rows($rsEmployees);
            }else {
               echo '<div>NO RECORD QUERIED base on your criteria!!!</div>';
               echo '<div>'.$errmsg.'</div>';
            }
            ?>

         </table>
         <p>
            <div class="row">
               <div class="col-xs-2 txt-right">Prepared By:</div>
               <div class="col-xs-4"></div>
               <div class="col-xs-2 txt-right">Approved By:</div>
               <div class="col-xs-4"></div>
            </div>
            <div class="row">
               <div class="col-xs-2"></div>
               <div class="col-xs-4">________________________</div>
               <div class="col-xs-2"></div>
               <div class="col-xs-3">________________________</div>
               <div class="col-xs-1"></div>
            </div>
         </p>

      </div>
   </body>
</html>