<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $count = $total_count = 0;
   $quarter = getvalue("quarter");
   $year    = getvalue("year");
   $where   = "WHERE Year(EndDate) = '$year'";

   switch ($quarter) {
      case '1':
         $quarter_label = "1st";
         $where .= " AND Month(EndDate) BETWEEN '01' AND '03'";
         break;
      case '2':
         $quarter_label = "2nd";
         $where .= " AND Month(EndDate) BETWEEN '04' AND '06'";
         break;
      case '3':
         $quarter_label = "3rd";
         $where .= " AND Month(EndDate) BETWEEN '07' AND '09'";
         break;
      case '4':
         $quarter_label = "4th";
         $where .= " AND Month(EndDate) BETWEEN '10' AND '12'";
         break;
      default:
         $where .= "";
         $quarter_label = "Yearly";
         break;
   }
   $sql_total     = "SELECT SUM(Cost) as total FROM ldmslndintervention ".$where;
   $total_result  = mysqli_query($conn,$sql_total);
   if ($total_result) {
      $total_row     = mysqli_fetch_assoc($total_result);
      $total = $total_row["total"];
   } else {
      $total = 0;
   }
   $emp_count     = SelectEach("ldmslndintervention",$where." GROUP BY EmployeesRefId");
   if ($emp_count) {
      while ($emp_row = mysqli_fetch_assoc($emp_count)) {
         $count++;
      }
   }

   $emp_count     = SelectEach("employees","WHERE (Inactive != 1 OR Inactive IS NULL)");
   if ($emp_count) {
      while ($emp_row = mysqli_fetch_assoc($emp_count)) {
         $total_count++;
      }
   }
   

?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style type="text/css">
         
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader("Summary of L&D Interventions");
         ?>
         <div class="row">
            <div class="col-xs-12 text-center">
               Quarter:<b><u><?php echo $quarter_label; ?></u></b>, Year: <b><u><?php echo $year; ?></u></b>
            </div>
         </div>
         <br><br>
         <div class="row">
            <div class="col-xs-12">
               Part I: Summary
            </div>
         </div>
         <div class="row">
            <div class="col-xs-1"></div>
            <div class="col-xs-11">
               A. Total Number of Active Employees in the Quarter: <b><u><?php echo $total_count; ?></u></b>
               <br>
               B. Total Number of Active Employees in the Quarter with at least 1 L&D Intervention: <b><u><?php echo $count; ?></u></b>
               <br>
               C. Total Cost Involved:<b><u>P <?php echo number_format($total,2); ?></u></b>
            </div>
         </div>
         <div class="row">
            <div class="col-xs-12">
               Part II: Data
               <br><br>
            </div>
         </div>
         <div class="row">
            <div class="col-xs-12">
               <table style="width: 100%;">
                  <thead>
                     <tr class="colHEADER">
                        <th style="width: 15%;">Name</th>
                        <th style="width: 25%; ">Number of L&D <br>Interventions</th>
                        <th style="width: 25%; ">Provider</th>
                        <th style="width: 10%; ">Start Date</th>
                        <th style="width: 10%; ">End Date</th>
                        <th style="width: 15%; ">Cost</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                        $new_rs        = SelectEach("ldmslndintervention",$where." ORDER BY StartDate");
                        if ($new_rs) {
                           $row_count  = mysqli_num_rows($new_rs);
                           while ($new_row = mysqli_fetch_assoc($new_rs)) {
                              $emprefid   = $new_row["EmployeesRefId"];
                              $row        = FindFirst("employees","WHERE RefId = '$emprefid'","*");
                              if ($row) {
                                 $check_name    = "";
                                 $emprefid      = $row["RefId"];
                                 $LastName      = $row["LastName"];
                                 $FirstName     = $row["FirstName"];
                                 $MiddleName    = $row["MiddleName"];
                                 $MiddleInitial = substr($MiddleName, 0,1);
                                 $FullName      = $FirstName." ".$MiddleInitial.". ".$LastName;
                              } else {
                                 $FullName      = "";
                              }

                              $Name       = $new_row["Name"];
                              $StartDate  = $new_row["StartDate"];
                              $EndDate    = $new_row["EndDate"];
                              $Provider   = $new_row["Provider"];
                              $Cost       = $new_row["Cost"];
                              echo '<tr style="height:80px;" valign="top">';
                                 if ($check_name == $FullName) {
                                    echo '<td>&nbsp;</td>';
                                 } else {
                                    echo '<td>'.$FullName.'</td>';
                                    $check_name = $FullName;
                                 }
                                 echo '<td>'.$Name.'</td>';
                                 echo '<td>'.$Provider.'</td>';
                                 echo '<td class="text-center">'.date("F d, Y",strtotime($StartDate)).'</td>';
                                 echo '<td class="text-center">'.date("F d, Y",strtotime($EndDate)).'</td>';
                                 echo '<td class="text-center">'.number_format(intval($Cost),2).'</td>';
                              echo '</tr>';
                           }
                        } else {
                           echo '<tr>';
                              echo '<td colspan="6">No Record Found</td>';
                           echo '</tr>';
                        }
                     ?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </body>
</html>
