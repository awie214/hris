<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $count = 0;
            if ($rsEmployees) {
               while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
         ?>
         <div style="page-break-after: always;">
            <table style="width: 100%;">
               <thead>
                  <tr>
                     <td colspan="8" class="text-center">
                        <?php
                           rptHeader(getvalue("RptName"));
                        ?>
                     </td>
                  </tr>
                  <tr>
                     <td colspan="8">&nbsp;</td>
                  </tr>
                  <tr>
                     <td colspan="8">
                        <div class="row">
                           <div class="col-xs-1"><label>NAME:</label></div>
                           <div class="col-xs-2 text-center" style="border-bottom: 1px solid black;">
                              <?php echo rptDefaultValue($row_emp["LastName"]); ?>
                           </div>
                           <div class="col-xs-2 text-center" style="border-bottom: 1px solid black;">
                              <?php echo rptDefaultValue($row_emp["FirstName"]); ?>
                           </div>
                           <div class="col-xs-2 text-center" style="border-bottom: 1px solid black;">
                              <?php echo rptDefaultValue($row_emp["MiddleName"]); ?>
                           </div>
                           <div class="col-xs-5">
                              &nbsp;(If married woman, give also full maiden name)
                           </div>
                        </div>
                     </td>
                  </tr>
                  <tr>
                     <td colspan="8">
                        <div class="row">
                           <div class="col-xs-1">&nbsp;</div>
                           <div class="col-xs-2 text-center">
                              (Surname)
                           </div>
                           <div class="col-xs-2 text-center">
                              (Given Name)
                           </div>
                           <div class="col-xs-2 text-center">
                              (Middle Name)
                           </div>
                        </div>
                     </td>
                  </tr>
                  <tr>
                     <td colspan="8">&nbsp;</td>
                  </tr>
                  <tr>
                     <td colspan="8">
                        <div class="row">
                           <div class="col-xs-1"><label>BIRTH:</label></div>
                           <div class="col-xs-2 text-center" style="border-bottom: 1px solid black;">
                              <?php echo rptDefaultValue(date("m/d/Y",strtotime($row_emp["BirthDate"]))); ?>
                           </div>
                           <div class="col-xs-4 text-center" style="border-bottom: 1px solid black;">
                              <?php echo rptDefaultValue($row_emp["BirthPlace"]); ?>
                           </div>
                           <div class="col-xs-5">
                              Date herein should be checked from birth or baptismal certificate or some other reliable documents.
                           </div>
                        </div>
                     </td>
                  </tr>
                  <tr>
                     <td colspan="8">
                        <div class="row">
                           <div class="col-xs-1">&nbsp;</div>
                           <div class="col-xs-2 text-center">
                              (Date)
                           </div>
                           <div class="col-xs-4 text-center">
                              (Place Of Birth)
                           </div>
                        </div>
                     </td>
                  </tr>
                  <tr>
                     <td colspan="8">&nbsp;</td>
                  </tr>
                  <tr>
                     <td colspan="8">
                        <p>
                           This is to certify that the employee named herein above actually rendered services in this Office as shown by the service record below, each line of which is supported by appointment and other papers actually issued by this Office and approved by the authorities otherwise indicated.
                        </p>
                     </td>
                  </tr>
                  <tr>
                     <td colspan="8">&nbsp;</td>
                  </tr>
                  <tr class="colHEADER">
                     <th colspan="2">SERVICES<br>(Inclusive Dates)</th>
                     <th colspan="3">RECORDS OF APPOINTMENT</th>
                     <th rowspan="2" style="width: 15%;">Station/Place<br>of Assignment</th>
                     <th rowspan="2" style="width: 10%;">Leave of<br>Absence<br>Without Pay</th>
                     <th rowspan="2" style="width: 15%;">Remarks<br>Separation<br>(Reference)</th>
                  </tr>
                  <tr class="colHEADER">
                     <th style="width: 10%;">FROM</th>
                     <th style="width: 10%;">TO</th>
                     <th style="width: 20%;">DESIGNATIONS</th>
                     <th style="width: 10%;">STATUS</th>
                     <th style="width: 10%;">SALARY</th>
                  </tr>
               </thead>
               <tbody>
                  <?php
                     $emprefid = $row_emp["RefId"];
                     $emp_movement = SelectEach("employeesmovement","WHERE EmployeesRefId = $emprefid");
                     if ($emp_movement) {
                        while ($emp_movement_row = mysqli_fetch_assoc($emp_movement)) {
                  ?>
                     <tr>
                        <td class="text-center">
                           <?php 
                              if ($emp_movement_row["EffectivityDate"] != "") {
                                 echo rptDefaultValue(date("m/d/Y",strtotime($emp_movement_row["EffectivityDate"])));   
                              }
                           ?>
                        </td>
                        <td class="text-center">
                           <?php 
                              if ($emp_movement_row["ExpiryDate"] != "") {
                                 echo rptDefaultValue(date("m/d/Y",strtotime($emp_movement_row["ExpiryDate"]))); 
                              }
                           ?>
                        </td>
                        <td>
                           <?php echo rptDefaultValue($emp_movement_row["PositionRefId"],"position"); ?>
                        </td>
                        <td class="text-center">
                           <?php echo rptDefaultValue($emp_movement_row["EmpStatusRefId"],"empstatus"); ?>
                        </td>
                        <td class="text-right">
                           <?php
                              $SalaryAmount = intval($emp_movement_row["SalaryAmount"]);
                              if ($SalaryAmount != "") {
                                 echo "Php ".number_format($SalaryAmount,2);
                              } else {
                                 echo "Php ".number_format(0,2);
                              }
                           ?>
                        </td>
                        <td class="text-center">
                           <?php echo rptDefaultValue($emp_movement_row["DivisionRefId"],"division"); ?>
                        </td>
                        <td class="text-center">
                           <?php
                              $LWOP = intval($emp_movement_row["LWOP"]);
                              if ($LWOP > 0) {
                                 echo $LWOP;   
                              }
                           ?>
                        </td>
                        <td>
                           <?php echo rptDefaultValue($emp_movement_row["Cause"]); ?>
                        </td>
                     </tr>

                  <?php
                        }
                        echo '
                           <tr>
                              <td class="text-center" colspan="8">--NOTHING FOLLOWS--</td>
                           </tr>
                        ';
                     } else {
                        echo '
                           <tr>
                              <td class="text-center" colspan="8">--NO SERVICE RECORD--</td>
                           </tr>
                        ';
                     }
                  ?>
               </tbody>
            </table>
            <div class="row margin-top">
               <div class="col-xs-12">
                  Issued in compliance with Executive Order No. 54 dated August 10, 1954, and in accordance with Circular, No. 58, dated August 10,1954 of the System.
               </div>
            </div>
            <br><br>
            <div class="row margin-top">
               <div class="col-xs-6">
                  DATE: <b><?php echo date("m/d/Y",time()); ?></b>
               </div>
               <div class="col-xs-6">
                  CERTIFIED CORRECT: ______________________________________
               </div>
            </div>
         </div>
         <?php
               }
            }
         ?>
      </div>
   </body>
</html>