<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $count = 0;
   $quarter = getvalue("quarter");
   $year    = getvalue("year");
   $where   = "WHERE Year(ServiceStartDate) = '$year'";
   switch ($quarter) {
      case '1':
         $quarter_label = "1st";
         $where .= " AND Month(ServiceStartDate) BETWEEN '01' AND '03'";
         break;
      case '2':
         $quarter_label = "2nd";
         $where .= " AND Month(ServiceStartDate) BETWEEN '04' AND '06'";
         break;
      case '3':
         $quarter_label = "3rd";
         $where .= " AND Month(ServiceStartDate) BETWEEN '07' AND '09'";
         break;
      case '4':
         $quarter_label = "4th";
         $where .= " AND Month(ServiceStartDate) BETWEEN '10' AND '12'";
         break;
      default:
         $where = "";
         $quarter_label = "";
         break;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style type="text/css">
         .gray {background: gray;}
      </style>
   </head>
   <body>
      <div class="container-fluid">
         <?php
            rptHeader("Competency Table");
         ?>
         <br><br>
         <div class="row">
            <div class="col-xs-12">
               <?php
                  $rs = SelectEach("competency","LIMIT 5");
                  if ($rs) {
                     while ($row = mysqli_fetch_assoc($rs)) {
                        $Name = $row["Name"];
                        $Type = $row["Type"];                     
                        $Definition = $row["Definition"];
               ?>
               <table style="width: 100%;">
                  <tr>
                     <td class="gray">COMPETENCY</td>
                     <td colspan="3"><?php echo $Name; ?></td>
                     <td class="gray"><?php echo $Type; ?></td>
                  </tr>
                  <tr>
                     <td class="gray">DEFINITION</td>
                     <td colspan="4"><?php echo $Definition; ?></td>
                  </tr>
                  <tr>
                     <td class="gray">Proficiency</td>
                     <td class="gray">Level 1</td>
                     <td class="gray">Level 2</td>
                     <td class="gray">Level 3</td>
                     <td class="gray">Level 4</td>
                  </tr>
                  <tr>
                     <td>Indicator</td>
                     <td valign="top">
                        <ul>
                        <?php
                           for ($a=1; $a <= 10; $a++) { 
                              if ($row["LevelOne".$a] != "") echo "<li>".$row["LevelOne".$a]."</li>";
                           }
                        ?>
                        </ul>
                     </td>
                     <td valign="top">
                        <ul>
                        <?php
                           for ($b=1; $b <= 10; $b++) { 
                              if ($row["LevelTwo".$b] != "") echo "<li>".$row["LevelTwo".$b]."</li>";
                           }
                        ?>
                        </ul>
                     </td>
                     <td valign="top">
                        <ul>
                        <?php
                           for ($c=1; $c <= 10; $c++) { 
                              if ($row["LevelThree".$c] != "") echo "<li>".$row["LevelThree".$c]."</li>";
                           }
                        ?>
                        </ul>
                     </td>
                     <td valign="top">
                        <ul>
                        <?php
                           for ($d=1; $d <= 10; $d++) { 
                              if ($row["LevelFour".$d] != "") echo "<li>".$row["LevelFour".$d]."</li>";
                           }
                        ?>
                        </ul>
                     </td>
                  </tr>
               </table>
               <br><br>
               <?php
                     }
                  }
               ?>
            </div>
         </div>
      </div>
   </body>
</html>
