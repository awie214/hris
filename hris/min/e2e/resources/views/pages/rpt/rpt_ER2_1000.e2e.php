<?php
   require_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   require_once pathClass.'0620RptFunctions.e2e.php';
   require_once pathClass.'DTRFunction.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $whereClause .= " LIMIT 10";
   $table = "employees";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   $from = getvalue("txtAttendanceDateFrom");
   $to   = getvalue("txtAttendanceDateTo");
   $month = getvalue("txtAttendanceMonth");
   $year  = getvalue("txtAttendanceYear");
   $from    = $year."-".$month."-01";
   $to      = $year."-".$month."-".cal_days_in_month(CAL_GREGORIAN,$month,$year);
   if ($dbg) {
      echo $whereClause;
   }
   
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row">
            <div class="col-xs-12 text-center">
               <b>PLEASE READ INSTRUCTION AT THE BACK BEFORE ACCOMPLISHING THIS FORM</b>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-12">
               <table border="2" width="100%">
                  <tr>
                     <td colspan="7" valign="bottom">
                        <div class="row">
                           <div class="col-xs-12">
                              <div class="col-xs-1">
                                 <img src="https://www.eliz-j.com/wp-content/uploads/2013/03/philhealth.jpg" width="80px">
                              </div>
                              <div class="col-xs-4">
                                 <b>PHILHEALTH</b>
                                 <br>
                                 <b>REPORT OF EMPLOYEE-MEMBERS</b>
                              </div>
                              <div class="col-xs-5">
                                 <div class="row">
                                    <div class="col-xs-12 text-center">
                                       (CHECK APPLICABLE BOX)
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-xs-12">
                                       <input type="checkbox" name="">&nbsp;&nbsp;INITIAL LIST (Attach to PhilHealth Form Er1)
                                       <br>
                                       <input type="checkbox" name="">&nbsp;&nbsp;SUBSEQUENT LIST
                                    </div>
                                 </div>
                              </div>
                              <div class="col-xs-2 text-right" style="font-weight: 1000; font-size: 25pt; margin-top: 10px;">
                                 Er2
                              </div>
                           </div>
                        </div>
                     </td>
                  </tr>
                  <tr>
                     <td colspan="5">
                        <b>NAME OF EMPLOYER/FIRM:</b>
                     </td>
                     <td colspan="2">
                        <b>EMPLOYER NO.</b>
                     </td>
                  </tr>
                  <tr>
                     <td colspan="3">
                        <b>ADDRESS:</b>
                     </td>
                     <td colspan="4">
                        <b>E-MAIL ADDRESS:</b>
                     </td>
                  </tr>
                  <tr style="font-size: 7pt;">
                     <td class="text-center" style="width: 15%; font-weight: 600;">
                        PHILHEALTH
                        <br>
                        SSS/GSIS
                        <br>
                        NUMBER
                     </td>
                     <td class="text-center" style="width: 15%; font-weight: 600;">
                        NAME OF EMPLOYEE
                     </td>
                     <td class="text-center" style="width: 15%; font-weight: 600;">
                        POSITION
                     </td>
                     <td class="text-center" style="width: 10%; font-weight: 600;">
                        SALARY
                     </td>
                     <td class="text-center" style="width: 10%; font-weight: 600;">
                        DATE OF EMPLOYMENT
                     </td>
                     <td class="text-center" style="width: 15%; font-weight: 600;">
                        (DO NOT FILL)
                        <br>
                        EFF. DATE OF
                        COVERAGE
                     </td>
                     <td class="text-center" style="width: 20%; font-weight: 600;">
                        PREVIOUS EMPLOYER
                        <br>
                        (IF ANY)
                     </td>
                  </tr>
                  <?php 
                     
                     $where = "WHERE Month(AssumptionDate) = '$month' AND Year(AssumptionDate) = '$year'";
                     $rs    = SelectEach("empinformation",$where);
                     if ($rs) {
                        while ($row = mysqli_fetch_assoc($rs)) {
                           $emprefid      = $row["EmployeesRefId"];
                           $SalaryAmount  = $row["SalaryAmount"];
                           $PositionRefId = $row["PositionRefId"];
                           $Position      = getRecord("position",$PositionRefId,"Name");
                           $AssumptionDate = $row["AssumptionDate"];
                           $employee_row = FindFirst("employees","WHERE RefId = '$emprefid'","*");
                           if ($employee_row) {
                              $LastName      = $employee_row["LastName"];
                              $FirstName     = $employee_row["FirstName"];
                              $MiddleName    = $employee_row["MiddleName"];
                              $MiddleInitial = substr($employee_row["MiddleName"], 0,1);
                              $ExtName       = $employee_row["ExtName"];
                              $FullName      = $FirstName." ".$MiddleInitial." ".$LastName." ".$ExtName;
                              $PHIC          = $employee_row["PHIC"];
                           } else {
                              $FullName      = "";
                              $PHIC          = "";
                           }
                           echo '
                              <tr style="font-size: 7pt;">
                                 <td class="text-center">'.$PHIC.'</td>
                                 <td>'.$FullName.'</td>
                                 <td>'.$Position.'</td>
                                 <td class="text-right">'.number_format($SalaryAmount,3).'</td>
                                 <td class="text-center">'.date("F d, Y",strtotime($AssumptionDate)).'</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                              </tr>
                           ';
                        }
                     } else {
                        for ($i=1; $i <=10 ; $i++) { 
                           echo '
                              <tr>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                              </tr>
                           ';
                        }
                     }
                  ?>
                  <tr style="font-size: 9pt;">
                     <td colspan="3">
                        <b>TOTAL NO. LISTED ABOVE:</b>
                     </td>
                     <td colspan="2" class="text-center" valign="bottom">
                        <b>PAGE <u>1</u> OF <u>1</u> SHEETS</b>
                     </td>
                     <td colspan="2" class="text-center" valign="bottom">
                        <br>
                        __________________________
                        <br>
                        <b>SIGNATURE OVER PRINTED NAME</b>
                     </td>
                  </tr>
               </table>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-12 text-center">
               <b>TO BE ACCOMPLISHED IN DUPLICATE</b>
            </div>
         </div>
         <?php spacer(40); ?>
         <div class="row" style="page-break-before: always;">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12 text-center">
                     INSTRUCTIONS
                  </div>
               </div>
               <br>
               <div class="row">
                  <div class="col-xs-12">
                     <ol>
                        <li>
                           An employer who is not yet registered with PhilHealth will submit this form in two (2) copies together with the "Employer Data Record", in two (2) copies also.
                        </li>
                        <li>
                           An employer already registered with PhilHealth will submit this form in two (2) copies to PhilHealth to report (a) newly hired employee(s). The PhilHealth Number of the employee (which was shown to the Employer) should be written in the first column of this form.
                        </li>
                        <li>
                           ALL COLUMNS SHALL BE FILLED CORRECTLY,except the column with the heading "EFF. DATE OF COVERAGE".
                        </li>
                        <li>
                           IT IS IMPORTANT THAT YOU INDICATE YOUR REGISTERED NAME AND EMPLOYER NUMBER IN YOUR REMITTANCE (PhilHealth Form RF1) ACCURATELY. OTHERWISE, YOUR PAYMENT CAN NOT BE CREDITED TO YOUR ACCOUNT.
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>
