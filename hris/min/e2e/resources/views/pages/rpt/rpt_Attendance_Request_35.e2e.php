<?php
   include_once 'pageHEAD.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   $refid = getvalue("refid");
   $row = FindFirst("attendance_request","WHERE RefId = $refid","*");
   if ($row) {
   		$type = $row["Type"];
   		$emprefid = $row["EmployeesRefId"];
   		$employees = FindFirst("employees","WHERE RefId = '$emprefid'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
   		if ($employees) {
   			$FirstName 	= $employees["FirstName"];
   			$LastName 	= $employees["LastName"];
   			$MiddleName = $employees["MiddleName"];
   			$ExtName 	= $employees["ExtName"];
   			$FullName = $LastName.", ".$FirstName." $ExtName ".$MiddleName;
   		} else {
   			$FullName = "&nbsp;";
   		}
   		$Signatory1 = getRecord("signatories",$leave_row["Signatory1"],"Name");
   		$Signatory1_pos = getRecord("signatories",$leave_row["Signatory1"],"PositionRefId");
   		$empinformation = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
   		if ($empinformation) {
   			$OfficeRefId = getRecord("office",$empinformation["OfficeRefId"],"Name");
   			$DivisionRefId = getRecord("division",$empinformation["DivisionRefId"],"Name");
   			$PositionRefId = getRecord("position",$empinformation["PositionRefId"],"Name");
   		} else {
   			$OfficeRefId = $DivisionRefId = $PositionRefId = "&nbsp;";
   		}
   		$filed_date = date("F d, Y",strtotime($row["FiledDate"]));
   		$applied_date = date("F d, Y",strtotime($row["AppliedDateFor"]));
   }
?>
<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		td {
			border: 2px solid black;
			vertical-align: top;
			padding: 5px;
			font-size: 9pt;
		}
		.data {
			font-size: 10pt;
			text-transform: uppercase;
			font-weight: 600;
		}
	</style>
</head>
<body>
	<div class="container-fluid rptBody">
		<div style="page-break-after: always;">
	        <?php
	            rptHeader("Request for Attendance Registration/Correction","HRMDS-T-007");
	        ?>
	        <div class="row">
	         	<div class="col-xs-12">
	         		<table width="100%">
	         			<tr>
							<td style="width: 8.33%; padding: 0px; border: none;"></td>
							<td style="width: 8.33%; padding: 0px; border: none;"></td>
							<td style="width: 8.33%; padding: 0px; border: none;"></td>
							<td style="width: 8.33%; padding: 0px; border: none;"></td>
							<td style="width: 8.33%; padding: 0px; border: none;"></td>
							<td style="width: 8.33%; padding: 0px; border: none;"></td>
							<td style="width: 8.33%; padding: 0px; border: none;"></td>
							<td style="width: 8.33%; padding: 0px; border: none;"></td>
							<td style="width: 8.33%; padding: 0px; border: none;"></td>
							<td style="width: 8.33%; padding: 0px; border: none;"></td>
							<td style="width: 8.33%; padding: 0px; border: none;"></td>
							<td style="width: 8.33%; padding: 0px; border: none;"></td>
						</tr>
	         			<tr>
	         				<td colspan="3">
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Name of Employee:
	         							<br>
	         							<span class="data">
		         							<?php
		         								echo $FullName;
		         							?>
	         							</span>
	         						</div>
	         					</div>
	         				</td>
	         				<td colspan="3">
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Position Title:
	         							<br>
	         							<span class="data">
		         							<?php
		         								echo $PositionRefId;
		         							?>
	         							</span>
	         						</div>
	         					</div>
	         				</td>
	         				<td colspan="3">
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Office/Division:
	         							<br>
	         							<span class="data">
		         							<?php
		         								echo $OfficeRefId."<br>".$DivisionRefId;
		         							?>
	         							</span>
	         						</div>
	         					</div>
	         				</td>
	         				<td colspan="3">
	         					<div class="row">
	         						<div class="col-xs-12">
	         							DATE FILED:
	         							<br>
	         							<span class="data">
		         							<?php
		         								echo $filed_date;
		         							?>
	         							</span>
	         						</div>
	         					</div>
	         				</td>
	         			</tr>
	         			<tr>
	         				<td colspan="12">
	         					<div class="row">
	         						<div class="col-xs-12 text-center">
	         							<span class="data">
		         							Details of the Request
	         							</span>
	         						</div>
	         					</div>
	         				</td>
	         			</tr>
	         			<tr>
	         				<td class="text-center" colspan="4">
	         					Date of Attendance to<br>be Corrected
	         				</td>
	         				<td class="text-center">
	         					AM<br>Time<br>In
	         				</td>
	         				<td class="text-center">
	         					PM<br>Break<br>Out
	         				</td>
	         				<td class="text-center">
	         					PM<br>Break<br>In
	         				</td>
	         				<td class="text-center">
	         					PM<br>Time<br>Out
	         				</td>
	         				<td class="text-center" colspan="4">
	         					Reason for the Request
	         				</td>
	         			</tr>
	         			<tr>
	         				<td class="text-center" colspan="4">
	         					<span class="data">
         							<?php
         								echo $applied_date;
         							?>
     							</span>
	         				</td>
	         				<td class="text-center">
	         					&nbsp;
	         				</td>
	         				<td class="text-center">
	         					&nbsp;
	         				</td>
	         				<td>
	         					&nbsp;
	         				</td>
	         				<td>
	         					&nbsp;
	         				</td>
	         				<td class="text-center" colspan="4">
	         					
	         				</td>
	         			</tr>
	         		</table>
	         	</div>
	        </div>
	        <?php spacer(40); ?>
	        <div class="row">
	        	<div class="col-xs-4">
	        		Prepared by:
	        		<br><br>
	        		<br>
	        		<b>
	        			<?php echo $FullName; ?>
	        		</b>
	        		<br>
	        		<?php echo $PositionRefId; ?>
	        	</div>
	        	<div class="col-xs-4">
	        		Noted by:
	        		<br>
	        		<i>Time in (AM) and Time Out (PM)</i>
	        		<br><br>
	        		<b>____________________</b>
	        		<br>
	        		Guard on Duty
	        	</div>
	        	<div class="col-xs-4">
	        		Recommended by:
	        		<br><br><br>
	        		<b>
	        			<?php echo $Signatory1; ?>
	        		</b>
	        		<br>
	        		<?php echo $Signatory1_pos; ?>
	        	</div>
	        </div>
	    </div>
    </div>
</body>
</html>