<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php rptHeader(getRptName(getvalue("drpReportKind"))); ?>
         <p class="txt-center">For the Month of <u><?php echo monthName(date("m",time()),1).", ".date("Y",time()) ?></u></p>
         <div class="row">
            <div class="col-xs-2"></div>
            <div class="col-xs-8">
               <div class="row">
                  <div class="col-xs-3">
                     <div class="row">
                        <label>Name of Employee</label>
                     </div>
                     <div class="row margin-top">
                        <label>
                           Position
                        </label>
                     </div>
                     <div class="row margin-top">
                        <label>
                           Office
                        </label>
                     </div>
                     <div class="row margin-top">
                        <label>
                           Basic Salary
                        </label>
                     </div>
                     <div class="row margin-top">
                        <label>
                           Tax Code
                        </label>
                     </div>
                     <div class="row margin-top">
                        <label>
                           Assumption
                        </label>
                     </div>
                     <div class="row margin-top">
                        <label>
                           No. of Working Days
                        </label>
                     </div>
                     <div class="row margin-top">
                        <label>
                           LBP S/A #
                        </label>
                     </div>
                     <div class="row margin-top">
                        <label>
                           Rep Allow
                        </label>
                     </div>
                     <div class="row margin-top">
                        <label>
                           Transpo Allow
                        </label>
                     </div>
                     <div class="row margin-top">
                        <label>
                           Actual % RATA
                        </label>
                     </div>
                  </div>
                  <div class="col-xs-1">
                     <div class="row"><label>:</label></div>
                     <div class="row margin-top"><label>:</label></div>
                     <div class="row margin-top"><label>:</label></div>
                     <div class="row margin-top"><label>:</label></div>
                     <div class="row margin-top"><label>:</label></div>
                     <div class="row margin-top"><label>:</label></div>
                     <div class="row margin-top"><label>:</label></div>
                     <div class="row margin-top"><label>:</label></div>
                     <div class="row margin-top"><label>:</label></div>
                     <div class="row margin-top"><label>:</label></div>
                     <div class="row margin-top"><label>:</label></div>
                  </div>
                  <div class="col-xs-8">
                     <?php
                       /* $rs = mysqli_query($conn,"SELECT * FROM empinformation WHERE EmployeesRefId = 1");
                        if (mysqli_num_rows($rs) > 0){
                           while ($row = mysqli_fetch_assoc($rs)){*/
                     ?>
                     <div class="row">
                        <?php
                          /* $refid = $row["EmployeesRefId"];
                           $result = mysqli_query($conn,"SELECT * FROM employees WHERE RefId = $refid");
                           if (mysqli_num_rows($result) > 0){
                              while ($row = mysqli_fetch_assoc($result)){
                                 echo $row["LastName"].", ".$row["FirstName"];
                              }
                           }*/
                        ?>
                     </div>
                     <div class="row margin-top">
                        <?php
                           //echo getRecord("position",$row["PositionRefId"],"Name");
                        ?>
                     </div>
                     <div class="row margin-top">
                        <?php
                           //echo getRecord("office",$row["OfficeRefId"],"Name");
                        ?>
                     </div>
                     <div class="row margin-top">
                        
                     </div>
                     <div class="row margin-top">
                        
                     </div>
                     <div class="row margin-top">
                        
                     </div>
                     <div class="row margin-top">
                        
                     </div>
                     <div class="row margin-top">
                        
                     </div>
                     <div class="row margin-top">
                        
                     </div>
                     <div class="row margin-top">
                        
                     </div>
                     <div class="row margin-top">
                        
                     </div>
                     <?php 
                          /* }
                        }*/
                     ?>
                  </div>
               </div>
               <?php spacer(20);?>
               <div class="row">
                  <label>LAST PAYMENT OF SALARY FOR THE PERIOD COVERING 1-3 October 2017</label>
               </div>
               <div class="row">
                  <div class="col-xs-6"></div>
                  <div class="col-xs-6">
                     <div class="row">
                        (Less)
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-6">
                     <div class="row" style="padding: 10px;">
                        <div class="col-xs-6">
                           <div class="row">
                              <label>Basic</label>
                           </div>
                           <div class="row margin-top">
                              <label>PERA</label>
                           </div>
                           <div class="row margin-top text-right">
                              <label>Total</label>
                           </div>
                        </div>
                        <div class="col-xs-2">
                           <div class="row"><label>:</label></div>
                           <div class="row margin-top"><label>:</label></div>
                           <div class="row margin-top"><label>:</label></div>
                        </div>
                        <div class="col-xs-4 text-right">
                           <div class="row">
                              <label>10 000.00</label>
                           </div>
                           <div class="row margin-top" style="border-bottom: 1px solid black;">
                              <label>10 000.00</label>
                           </div>
                           <div class="row margin-top text-right">
                              <label>20 000.00</label>
                           </div>
                        </div>
                     </div>
                     <?php spacer(30); ?>
                     <div class="row" style="padding: 10px;">
                        <div class="col-xs-6">
                           <div class="row">
                              <label>RA</label>
                           </div>
                           <div class="row margin-top">
                              <label>TA</label>
                           </div>
                           <div class="row margin-top text-right">
                              <label>Total</label>
                           </div>
                        </div>
                        <div class="col-xs-2">
                           <div class="row"><label>:</label></div>
                           <div class="row margin-top"><label>:</label></div>
                           <div class="row margin-top"><label>:</label></div>
                        </div>
                        <div class="col-xs-4 text-right">
                           <div class="row">
                              <label>-</label>
                           </div>
                           <div class="row margin-top" style="border-bottom: 1px solid black;">
                              <label>-</label>
                           </div>
                           <div class="row margin-top text-right">
                              <label>-</label>
                           </div>
                        </div>
                     </div>
                     <?php spacer(10); ?>
                     <div class="row" style="padding: 10px;">
                        <div class="col-xs-6">
                           <div class="row text-right">
                              <label>Gross Pay</label>
                           </div>
                        </div>
                        <div class="col-xs-2">
                           <div class="row"><label>:</label></div>
                        </div>
                        <div class="col-xs-4">
                           <div class="row text-right"><label>20 000.00</label></div>
                        </div>
                     </div>
                     <div class="row" style="padding: 10px;">
                        <div class="col-xs-6">
                           <div class="row text-right">
                              <label>Net Pay</label>
                           </div>
                        </div>
                        <div class="col-xs-2">
                           <div class="row"><label>:</label></div>
                        </div>
                        <div class="col-xs-4">
                           <div class="row text-right"><label>20 000.00</label></div>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-6">
                     <div class="row" style="padding: 10px;">
                        <div class="col-xs-6">
                           <div class="row">
                              <label>W/ Tax</label>
                           </div>
                           <div class="row margin-top">
                              <label>GSIS</label>
                           </div>
                           <div class="row margin-top">
                              <label>Philhealth</label>
                           </div>
                           <div class="row margin-top">
                              <label>Pag-ibig</label>
                           </div>
                           <div class="row margin-top text-right">
                              <label>Total Ded</label>
                           </div>
                        </div>
                        <div class="col-xs-2">
                           <div class="row"><label>:</label></div>
                           <div class="row margin-top"><label>:</label></div>
                           <div class="row margin-top"><label>:</label></div>
                           <div class="row margin-top"><label>:</label></div>
                           <div class="row margin-top"><label>:</label></div>
                        </div>
                        <div class="col-xs-4 text-right">
                           <div class="row">
                              <label>10 000.00</label>
                           </div>
                           <div class="row margin-top">
                              <label>10 000.00</label>
                           </div>
                           <div class="row margin-top">
                              <label>10 000.00</label>
                           </div>
                           <div class="row margin-top" style="border-bottom: 1px solid black;">
                              <label>10 000.00</label>
                           </div>
                           <div class="row margin-top text-right">
                              <label>40 000.00</label>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <div class="row">
                        <b><u>Last Salary Attachments:</u></b>
                     </div>
                     <div class="row margin-top">
                        1. October 1-4, 2017 Daily Time Record
                     </div>
                     <div class="row margin-top">
                        2. Official Clearance
                     </div>
                     <?php spacer(10); ?>
                     <div class="row margin-top">
                        Certified Correct
                     </div>
                     <?php spacer(20); ?>
                     <div class="row margin-top">
                        <label>ANTONIA LYNNELY L. BAUTISTA</label><br>
                        Chief Admin Officer, HRDD
                     </div>
                  </div>

               </div>
            </div>
            
         </div>
      </div>
   </body>
</html>