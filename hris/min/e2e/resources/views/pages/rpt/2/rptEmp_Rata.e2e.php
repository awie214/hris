<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <!--<table>
            <thead>
               <tr>
                  <th>
                  </th>
               </tr>   
            </thead>
            <tbody>
               <tr>
                  <td></td>
               </tr>   
            </tbody>
            <tfoot>
               <tr>
                  <td></td>
               </tr>   
            </tfoot>
         </table>-->   
         <table>
            <thead>
               <tr>
                  <th colspan="11" align="center" style="text-align:center;">
                     <?php
                        rptHeader(getRptName(getvalue("drpReportKind")));
                     ?>
                     <p class="txt-center">For the Month of <u><?php echo monthName(date("m",time()),1).", ".date("Y",time()) ?></u></p>
                  </th>
                  
               </tr>   
               <tr class="colHEADER" align="center">
                  <th rowspan="2">ID</th>
                  <th rowspan="2">EMPLOYEE NAME</th>
                  <th rowspan="2">DESIGNATION</th>
                  <th rowspan="2">REPRESENTATION</th>
                  <th rowspan="2">TRANSPORTATION</th>
                  <th rowspan="2">LEAVE FILED FOR <u><?php echo monthName(date("m",time()),-1) ?></u> 2017</th>
                  <th rowspan="2">NO. OF WORKDAYS OF <br>ACTUAL WORK PERFORMANCE OF THE PREVIOUS MONTH</th>
                  <th rowspan="2">PERCENTAGE OF ACTUAL RATA</th>
                  <th colspan="3">NET</th>
               </tr>
               <tr class="colHEADER" align="center">
                  <th>REPRESENTATION</th>
                  <th>TRANSPORTATION</th>
                  <th>TOTAL</th>
               </tr>
            </thead>
            <tbody>
                  <?php 
                     $rs = SelectEach("empinformation","GROUP BY RefId LIMIT 10");

                     if ($rs){
                        while($row = mysqli_fetch_assoc($rs)){
                  ?>
                  <tr>
                  <td><?php echo $row["EmployeesRefId"]; ?></td>
                  <td><?php echo getRecord("employees",$row["EmployeesRefId"],"FirstName"); ?></td>
                  <td><?php echo getRecord("position",$row["PositionRefId"],"Name"); ?></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  </tr>
                  <?php
                        }
                     }
                  ?>
                  <tr>
                     <td colspan="11">&nbsp;</td>
                  </tr>
                  <tr>
                     <td colspan="11">*RATA Schedule follows:</td>
                  </tr>     
                  <tr>
                     <td colspan="3" align="center">
                        Number of Workdays of Actual Work Performance in a Month
                     </td>
                     <td colspan="3" align="center">
                        Actual RATA for a month
                     </td>
                     <td colspan="5">
                        *Certification: This is to certify that Dir. Arnold Roy D. Tenorio was not included in the RATA payroll for the said month as he assumed office effective 4 September 2017.
                     </td>
                  </tr>
                  <tr align="center">
                     <td colspan="3">1 to 5</td>
                     <td colspan="3">25% of the Monthly RATA</td>
                     <td colspan="5"></td>
                  </tr>
                  <tr align="center">
                     <td colspan="3">6 to 11</td>
                     <td colspan="3">50% of the Monthly RATA</td>
                     <td colspan="5"></td>
                  </tr>
                  <tr align="center">
                     <td colspan="3">12 to 16</td>
                     <td colspan="3">75% of the Monthly RATA</td>
                     <td colspan="5"></td>
                  </tr>
                  <tr align="center">
                     <td colspan="3">17 and more</td>
                     <td colspan="3">100% of the Monthly RATA</td>
                     <td colspan="5"></td>
                  </tr>
                  <tr>
                     <td colspan="3">Certified Correct:</td>
                     <td colspan="3">Approved for Payment:</td>
                     <td colspan="2">Certified: Supporting documents complete and proper, and cash available in the amount of Php ____________________</td>
                     <td colspan="3">Certified: Each employee whose name appears on the payroll has been paid the amount as indicated opposite his/her name.</td>
                  </tr>
                  <tr>
                     <td colspan="3">ANTONIA LYNNELY L. BAUTISTA</td>
                     <td colspan="3">GWEN GRECIA-DE VERA</td>
                     <td colspan="2">CAROLYN V. AQUINO</td>
                     <td colspan="3">FLERIDA J. GIRON</td>
                  </tr>
                  <tr>
                     <td colspan="3">Chief Admin Officer, HRDD</td>
                     <td colspan="3">Executive Director</td>
                     <td colspan="2">Accountant III, FPMO</td>
                     <td colspan="3">Cashier III, ALO</td>
                  </tr>
            </tbody>
            <tfoot>
               <tr>
                  <td colspan="13">
                     <?php rptFooter(); ?>
                  </td>
               </tr>   
            </tfoot>
         </table>   
      </div>
   </body>
</html>