<?php
   //header('Content-type: application/vnd.ms-excel');

   // It will be called file.xls
   //header('Content-Disposition: attachment; filename="file.xls"');
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   include 'incSignatory.e2e.php';
   $table = "employees";
   $month = getvalue("txtAttendanceMonth");
   $year  = getvalue("txtAttendanceYear");
   $whereClause .= " ORDER BY LastName LIMIT 25";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style type="text/css">
         body {
            font-family:Arial;
         }
         @media print {
            @page {
               size: landscape;
            }
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12">
                     Form A as of February 2019 
                     <br>
                     <b>Agency Name:</b>&nbsp;&nbsp;
                     Philippine Institute for Development Studies
                     <br>
                     <b>Agency BP Number:</b>&nbsp;&nbsp;
                     BP1000011921
                     <br>
                     <br>
                     <b>FOR AGENCY REMITTANCE ADVICE</b>
                     <br>
                     <br>
                     <b>FORM A.</b>List of New Members/re-employed/reinstated
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table style="width: 100%;">
                        <thead>
                           <tr class="colHEADER" style="font-size: 8pt; font-weight: 400;">
                              <th rowspan="2">Last Name</th>
                              <th rowspan="2">First Name</th>
                              <th>Suffix</th>
                              <th>Middle<br>Name</th>
                              <th colspan="5">MAILING ADDRESS</th>
                              <th>Cellular Phone No.</th>
                              <th>Email Address</th>
                              <th rowspan="2">SEX</th>
                              <th rowspan="2">Civil Status</th>
                              <th>Date of Birth</th>
                              <th rowspan="2">Place of birth</th>
                              <th rowspan="2">BASIC SALARY MONTHLY</th>
                              <th colspan="2">EFFECTIVITY DATE</th>
                              <th rowspan="2">Status of Employment</th>
                              <th rowspan="2">Position</th>
                              <th rowspan="2">Remarks</th>
                           </tr>   
                           <tr class="colHEADER" style="font-size: 8pt; font-weight: 400;">
                              <th>(Please enter N/A if not supplied)</th>
                              <th>(Please enter N/A if not supplied)</th>
                              <th>Street</th>
                              <th>City</th>
                              <th>Region</th>
                              <th>Country<br>(e.g Philippines)</th>
                              <th>Postal Code</th>
                              <th>(Please enter N/A if not supplied)</th>
                              <th>(a@a.aa or N/A)</th>
                              <th>(MM/DD/YYYY)</th>
                              <th>FROM<br>(MM/DD/YYYY)</th>
                              <th>TO<br>(MM/DD/YYYY)</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php
                              /*while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                                 $FullName   = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
                                 $ResiStreet = $row_emp["ResiStreet"];
                                 $ResiSubd   = $row_emp["ResiSubd"];
                                 $ResiBrgy   = $row_emp["ResiBrgy"];
                                 $MobileNo   = $row_emp["MobileNo"];
                                 $EmailAdd   = $row_emp["EmailAdd"];
                                 $BirthDate  = $row_emp["BirthDate"];
                                 $BirthPlace = $row_emp["BirthPlace"];
                                 $ResiAddCityRefId      = getRecord("city",$row_emp["ResiAddCityRefId"],"Name");
                                 $ResiAddProvinceRefId  = getRecord("province",$row_emp["ResiAddProvinceRefId"],"Name");
                                 $ResiAddress = "";
                                 if ($ResiStreet != "") $ResiAddress .= "$ResiStreet, ";
                                 if ($ResiSubd != "") $ResiAddress .= "$ResiSubd, ";
                                 if ($ResiBrgy != "") $ResiAddress .= "$ResiBrgy, ";
                                 if ($ResiAddCityRefId != "") $ResiAddress .= "$ResiAddCityRefId, ";
                                 if ($ResiAddProvinceRefId != "") $ResiAddress .= "$ResiAddProvinceRefId";
                                 $CivilStat = $row_emp['CivilStatus']; 
                                 switch ($CivilStat) {
                                    case "Si":
                                       $CivilStat = 'Single';
                                    break;
                                    case "Ma":
                                       $CivilStat = 'Married';
                                    break;
                                    case "An":
                                       $CivilStat = 'Annulled';
                                    break;
                                    case "Wi":
                                       $CivilStat = 'Widowed';
                                    break;
                                    case "Se":
                                       $CivilStat = 'Separated';
                                    break;
                                    case "Ot":
                                       $CivilStat = 'Others';
                                    break;
                                 }
                                 $Gender = $row_emp["Sex"];
                                 if ($Gender == "M") {
                                    $Gender = "Male";
                                 } else if ($Gender == "F") {
                                    $Gender = "Female";
                                 } else {
                                    $Gender = "";
                                 }
                                 if ($BirthDate != "") {
                                    $BirthDate = date("m/d/Y",strtotime($BirthDate));
                                 } else {
                                    $BirthDate = "";
                                 }
                                 $emp_info = FindFirst("empinformation","WHERE EmployeesRefId = ".$row_emp["RefId"],"*");
                                 if ($emp_info) {
                                    $Position      = rptDefaultValue($emp_info["PositionRefId"],"position");
                                    $ApptStatus    = rptDefaultValue($emp_info["ApptStatusRefId"],"apptstatus");
                                    $SalaryAmount  = number_format($emp_info["SalaryAmount"],2);
                                    $Assumption    = date("m/d/Y",strtotime($emp_info["AssumptionDate"]));
                                 } else {
                                    $Position      = "";
                                    $ApptStatus    = "";
                                    $Assumption    = "";
                                    $SalaryAmount  = "0.00";
                                 }
                                 echo '<tr>';
                                    echo '
                                       <td>'.$row_emp["LastName"].'</td>
                                       <td>'.$row_emp["FirstName"].'</td>
                                       <td>'.$row_emp["ExtName"].'</td>
                                       <td>'.$row_emp["MiddleName"].'</td>
                                       <td>'.$ResiAddress.'</td>
                                       <td>'.$MobileNo.'</td>
                                       <td>'.$EmailAdd.'</td>
                                       <td>'.$Gender.'</td>
                                       <td>'.$CivilStat.'</td>
                                       <td class="text-center">'.$BirthDate.'</td>
                                       <td>'.$BirthPlace.'</td>
                                       <td class="text-right">P '.$SalaryAmount.'</td>
                                       <td>'.$Assumption.'</td>
                                       <td>'.$Position.'</td>
                                       <td>'.$ApptStatus.'</td>
                                       <td>BP1000011921</td>
                                       <td>Philippine Institute for Development Studies</td>
                                    ';
                                 echo '</tr>';
                              }*/
                           ?>
                        </tbody>
                     </table>
                  </div>
               </div>
               <br>
               <br>
               <span>
                  * Please indicate complete residential address with house number, street and barangay, if any.              
               </span>
               <br>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-6">
                     Prepared by:
                     <br><br><br>
                     <b><u><?php echo $prepared_by; ?></u></b>
                     <br>
                     <?php echo $prepared_by_position; ?>
                  </div>
                  <div class="col-xs-6">
                     Certified Correct:
                     <br><br><br>
                     <b><u><?php echo $corrected_by; ?></u></b>
                     <br>
                     <?php echo $corrected_by_position; ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>