<?php
	include_once 'constant.e2e.php';
	require_once pathClass.'0620functions.e2e.php';
	$emprefid 	= getvalue("refid");
	$emp_row 	= FindFirst("employees","WHERE RefId = '$emprefid'","*");
	$LastName 	= $emp_row["LastName"];
	$FirstName 	= $emp_row["FirstName"];
	$MiddleName = $emp_row["MiddleName"];
	$ExtName 	= $emp_row["ExtName"];
	$FullName 	= $FirstName." ".$MiddleName." ".$LastName." ".$ExtName; 
	$FullName 	= strtoupper($FullName);

	$empinfo_row = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
	if ($empinfo_row) {
		$Division 		= strtoupper(getRecord("division",$empinfo_row["DivisionRefId"],"Name"));
		$Position 		= strtoupper(getRecord("Position",$empinfo_row["PositionRefId"],"Name"));
		$SalaryGrade 	= strtoupper(getRecord("SalaryGrade",$empinfo_row["SalaryGradeRefId"],"Name"));
		$SalaryAmount 	= number_format($empinfo_row["SalaryAmount"],2);
		$Office 		= strtoupper(getRecord("Office",$empinfo_row["OfficeRefId"],"Name"));
	} else {
		$Division = $Position = $SalaryGrade = $Office = "";
		$SalaryAmount = "0.00";
	}
?>
<!DOCTYPE html>
<html>
<head>
	<?php
		include_once 'pageHEAD.e2e.php';
	?>
	<title></title>
	<style type="text/css">
		thead {
			text-transform: uppercase;
		}
		th {
            text-align: center;
            vertical-align: top;
            background: #d9d9d9;
            font-size: 8pt;
        }
		td {
			padding: 2px;
			vertical-align: top;
		}
	</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-12 text-center" style="border-bottom: 2px solid black; padding: 10px;">
						<b>CSC LEARNING APPLICATION PLAN</b>
						<br>
						(LAP Form)
					</div>
				</div>
				<br>
				<div class="row margin-top">
					<div class="col-xs-12" style="border: 2px solid black; padding: 5px;">
						<div class="row">
							<div class="col-xs-6">
								Learner: <?php echo $FullName; ?>
							</div>
							<div class="col-xs-6">
								Office: <?php echo $Office; ?>
							</div>
						</div>
						<div class="row margin-top">
							<div class="col-xs-6">
								Title of Intervention:
							</div>
							<div class="col-xs-6">
								Date Conducted:
							</div>
						</div>
						<div class="row margin-top">
							<div class="col-xs-12">
								Specific Compentency Targets to Develop/Enhance:
								<br>
								________________________________________________
							</div>
						</div>
					</div>
				</div>
				<br>
				<div class="row margin-top">
					<div class="col-xs-12">
						<table border="2" width="100%">
							<thead>
                                <tr>
                                    <th style="width: 20%;">
                                       <u>Learning Goals</u>
                                       <br>
                                       <br>
                                       What skills, knowledge and attitudes do I require to achieve competency target?
                                       (MUST be a SMART objective)
                                    </th>
                                    <th style="width: 20%;">
                                       <u>Current Status</u>
                                       <br>
                                       <br>
                                       What level of skills, knowledge and attitudes do I have now with respect to 
                                       this learning goal?
                                    </th>
                                    <th style="width: 20%;">
                                       <u>Learning Strategies</u>
                                       <br>
                                       <br>
                                       How will I reach my learning goal?
                                    </th>
                                    <th style="width: 20%;">
                                       <u>Required Resources</u>
                                       <br>
                                       <br>
                                       What resources do I need to achieve this learning goal?
                                    </th>
                                    <th style="width: 20%;">
                                       <u>Key Performance Indicator</u>
                                       <br>
                                       <br>
                                       How can I demonstrate to myself and others that I have achieved this learning goals?
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            	<?php
                            		for ($i=1; $i <= 5 ; $i++) { 
                            			echo '
                            				<tr>
			                            		<td>&nbsp;</td>
			                            		<td>&nbsp;</td>
			                            		<td>&nbsp;</td>
			                            		<td>&nbsp;</td>
			                            		<td>&nbsp;</td>
			                            	</tr>
                            			';
                            		}
                            	?>
                            </tbody>
						</table>
					</div>
				</div>
				<br>
				<div class="row margin-top">
					<div class="col-xs-12" style="border: 2px solid black; padding: 5px;">
						<div class="row">
							<div class="col-xs-2 text-center">
								<br>
								Signature
								<br>
								<br>
								<br>
								Date
							</div>
							<div class="col-xs-3 text-center">
								<br>
								_______________________
								<br>
								Head of Office
								<br>
								<br>
								_______________________
							</div>
							<div class="col-xs-3 text-center">
								<br>
								_______________________
								<br>
								Supervisor
								<br>
								<br>
								_______________________
							</div>
							<div class="col-xs-3 text-center">
								<br>
								_______________________
								<br>
								Learner
								<br>
								<br>
								_______________________
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>