<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $count = 0;
            if ($rsEmployees) {
         ?>
         
         <table style="width: 100%;">
            <thead>
               <tr>
                  <td colspan="5">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                  </td>
               </tr>
               <tr class="colHEADER">
                  <th>Sequence</th>
                  <th>Employee Name</th>
                  <th>Employee ID</th>
                  <th>Length of Service</th>
                  <th>Sex</th>
               </tr>
            </thead>
            <tbody>
               <?php
                  while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                     $count++;
                     $HiredDate  = "";
                     $FullName   = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
                     $HiredDate    = FindFirst("empinformation","WHERE EmployeesRefId = ".$row_emp["RefId"],"HiredDate");
                     $Dummy = $HiredDate;
                     switch ($row_emp["Sex"]) {
                        case 'F':
                           $Sex = "Female";
                           break;
                        case 'M':
                           $Sex = "Male";
                           break;
                        default:
                           $Sex = "";
                           break;
                     }
                     if ($HiredDate == "") {
                        $Service_Length = "";
                     } else {
                        $date2   = $HiredDate;
                        $date1   = date("Y-m-d",time());
                        $diff    = abs(strtotime($date2) - strtotime($date1));

                        $years   = floor($diff / (365*60*60*24));
                        $months  = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                        $days    = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
                        $Service_Length = $years." years and ".$months." months";
                     }
                     echo '
                        <tr>
                           <td class="text-center">'.$count.'</td>
                           <td>'.strtoupper($FullName).'</td>
                           <td class="text-center">'.$row_emp["AgencyId"].'</td>
                           <td class="text-center">'.$Service_Length.'</td>
                           <td class="text-center">'.$Sex.'</td>
                        </tr>
                     ';   
                     
                  }
               ?>
               
            </tbody>
         </table>
         <?php
            } else {
         ?>
         <?php
            rptHeader(getvalue("RptName"));
         ?>
         <table border="1" style="width: 100%;">
            <thead>
               <tr>
                  <th>Sequence</th>
                  <th>Employee Name</th>
                  <th>Employee ID</th>
                  <th>Length of Service</th>
                  <th>Sex</th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td colspan="5"> No Record Found.</td>
               </tr>
            </tbody>
         </table>
         <?php
            }
         ?>
      </div>
   </body>
</html>