<?php
   $month = getvalue("txtAttendanceMonth");
   $year  = getvalue("txtAttendanceYear");
   
?>
<!DOCTYPE html>
<html>
   <head>
      <?php
         include_once 'pageHEAD.e2e.php';
      ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style>
         /*.bottomline {border-bottom:2px solid black;}*/
         .bold {font-size:600;}
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12">
                     <?php
                        rptHeader("Authority for Overtime Services");
                     ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table border="2">
                        <tr>
                           <td style="width: 8.33%;"></td>
                           <td style="width: 8.33%;"></td>
                           <td style="width: 8.33%;"></td>
                           <td style="width: 8.33%;"></td>
                           <td style="width: 8.33%;"></td>
                           <td style="width: 8.33%;"></td>
                           <td style="width: 8.33%;"></td>
                           <td style="width: 8.33%;"></td>
                           <td style="width: 8.33%;"></td>
                           <td style="width: 8.33%;"></td>
                           <td style="width: 8.33%;"></td>
                           <td style="width: 8.33%;"></td>
                        </tr>
                        <tr>
                           <td colspan="12" class="text-center">
                              <b>STATEMENT OF OVERTIME SERVICES</b>
                           </td>
                        </tr>
                        <tr>
                           <td colspan="6">
                              <div class="row">
                                 <div class="col-xs-2"></div>
                                 <div class="col-xs-10">
                                    <input type="checkbox" name="">&nbsp;<b>With Pay</b>
                                    <br>
                                    <input type="checkbox" name="">&nbsp;<b>COC</b>
                                 </div>
                              </div>
                           </td>
                           <td colspan="6" valign="top">
                              <b>Period Covered:</b>
                           </td>
                        </tr>
                        <tr>
                           <td colspan="6">
                              <b>Name:</b>
                           </td>
                           <td colspan="6">
                              <b>Division/Office:</b>
                           </td>
                        </tr>
                        <tr>
                           <td colspan="6">
                              <b>Position:</b>
                           </td>
                           <td colspan="6">
                              <b>Monthly Salary:</b>
                           </td>
                        </tr>
                        <tr>
                           <td colspan="12" class="text-center"><b>WEEKDAYS</b></td>
                        </tr>
                        <tr>
                           <td rowspan="2" colspan="2" class="text-center">
                              <b>DATE</b>
                           </td>
                           <td rowspan="2" colspan="2" class="text-center">
                              <b>DAY</b>
                           </td>
                           <td colspan="2" class="text-center">
                              <b>MORNING</b>
                           </td>
                           <td colspan="2" class="text-center">
                              <b>AFTERNOON</b>
                           </td>
                           <td colspan="2" class="text-center">
                              <b>EVENING</b>
                           </td>
                           <td colspan="2" class="text-center">
                              <b>OT HOURS</b>
                           </td>
                        </tr>
                        <tr>
                           <td class="text-center">
                              <b>IN</b>
                           </td>
                           <td class="text-center">
                              <b>OUT</b>
                           </td>
                           <td class="text-center">
                              <b>IN</b>
                           </td>
                           <td class="text-center">
                              <b>OUT</b>
                           </td>
                           <td class="text-center">
                              <b>IN</b>
                           </td>
                           <td class="text-center">
                              <b>OUT</b>
                           </td>
                           <td class="text-center">
                              <b>HOURS</b>
                           </td>
                           <td class="text-center">
                              <b>MINUTES</b>
                           </td>
                        </tr>
                        <?php
                           for ($i=0; $i <= 9; $i++) { 
                              echo '<tr>';
                              echo '<td colspan="2">&nbsp;</td>';
                              echo '<td colspan="2"></td>';
                              echo '<td></td>';
                              echo '<td></td>';
                              echo '<td></td>';
                              echo '<td></td>';
                              echo '<td></td>';
                              echo '<td></td>';
                              echo '<td></td>';
                              echo '<td></td>';
                              echo '</tr>';
                           }
                        ?>
                        <tr>
                           <td colspan="12" class="text-center"><b>WEEKENDS</b></td>
                        </tr>
                        <tr>
                           <td rowspan="2" colspan="2" class="text-center">
                              <b>DATE</b>
                           </td>
                           <td rowspan="2" colspan="2" class="text-center">
                              <b>DAY</b>
                           </td>
                           <td colspan="2" class="text-center">
                              <b>MORNING</b>
                           </td>
                           <td colspan="2" class="text-center">
                              <b>AFTERNOON</b>
                           </td>
                           <td colspan="2" class="text-center">
                              <b>EVENING</b>
                           </td>
                           <td colspan="2" class="text-center">
                              <b>OT HOURS</b>
                           </td>
                        </tr>
                        <tr>
                           <td class="text-center">
                              <b>IN</b>
                           </td>
                           <td class="text-center">
                              <b>OUT</b>
                           </td>
                           <td class="text-center">
                              <b>IN</b>
                           </td>
                           <td class="text-center">
                              <b>OUT</b>
                           </td>
                           <td class="text-center">
                              <b>IN</b>
                           </td>
                           <td class="text-center">
                              <b>OUT</b>
                           </td>
                           <td class="text-center">
                              <b>HOURS</b>
                           </td>
                           <td class="text-center">
                              <b>MINUTES</b>
                           </td>
                        </tr>
                        <?php
                           for ($i=0; $i <= 9; $i++) { 
                              echo '<tr>';
                              echo '<td colspan="2">&nbsp;</td>';
                              echo '<td colspan="2"></td>';
                              echo '<td></td>';
                              echo '<td></td>';
                              echo '<td></td>';
                              echo '<td></td>';
                              echo '<td></td>';
                              echo '<td></td>';
                              echo '<td></td>';
                              echo '<td></td>';
                              echo '</tr>';
                           }
                        ?>
                        <tr>
                           <td colspan="6">
                              <b>TOTAL COC HOURS EARNED</b>
                           </td>
                           <td colspan="6">
                              <b>OT HOURS EARNED WITH PAY</b>
                           </td>
                        </tr>
                        <tr>
                           <td colspan="6">
                              <div class="row">
                                 <div class="col-xs-12">
                                    Weekday: Hours/+Minutes Rendered x 1.00
                                    <br>
                                    Weekend: Hours/+Minutes Rendered x 1.50
                                 </div>
                              </div>
                           </td>
                           <td colspan="6">
                              <div class="row">
                                 <div class="col-xs-12">
                                    Weekday: (Monthly Salary/22/8) x 1.25 x No. of Hours Rendered
                                    <br>
                                    Weekend: (Monthly Salary/22/8) x 1.50 x No. of Hours Rendered
                                 </div>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td colspan="3">
                              <b>Weekdays</b>
                           </td>
                           <td colspan="3"></td>
                           <td colspan="3">
                              <b>Weekdays</b>
                           </td>
                           <td colspan="3"></td>
                        </tr>
                        <tr>
                           <td colspan="3">
                              <b>Weekends</b>
                           </td>
                           <td colspan="3"></td>
                           <td colspan="3">
                              <b>Weekends</b>
                           </td>
                           <td colspan="3"></td>
                        </tr>
                        <tr>
                           <td colspan="3">
                              <b>TOTAL</b>
                           </td>
                           <td colspan="3"></td>
                           <td colspan="3">
                              <b>TOTAL</b>
                           </td>
                           <td colspan="3"></td>
                        </tr>
                        <tr>
                           <td rowspan="3" colspan="3"></td>
                           <td colspan="3">
                              <b>A Total OT Pay for Weekdays</b>
                           </td>
                           <td colspan="3"></td>
                           <td rowspan="3" colspan="3"></td>
                        </tr>
                        <tr>
                           <td colspan="3">
                              <b>B Total OT Pay for Weekends</b>
                           </td>
                           <td colspan="3"></td>
                        </tr>
                        <tr>
                           <td colspan="3">
                              <b>C Total OT Pay (A + B)</b>
                           </td>
                           <td colspan="3"></td>
                        </tr>
                        <tr>
                           <td colspan="12">
                              <div class="row">
                                 <div class="col-xs-12">
                                    I hereby certify that the services have been rendered under my direct supervision and the time indicated aboved has been checked against the employee's Daily Time Record.
                                 </div>
                              </div>
                              <br><br>
                              <div class="row">
                                 <div class="col-xs-8"></div>
                                 <div class="col-xs-4 text-center">
                                    <b>JENNIFER J. TAN</b>
                                    <br>
                                    <b>Director, Administrative and Financial Office</b>
                                    <br>
                                    <b>OIC-Human Resources Management Unit</b>
                                    <br>
                                    <b>Date:_________________</b>
                                 </div>
                              </div>
                           </td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>