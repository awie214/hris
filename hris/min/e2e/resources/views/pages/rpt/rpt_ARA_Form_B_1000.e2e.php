<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   include 'incSignatory.e2e.php';
   $table = "employees";
   $transfer = FindFirst("apptstatus","WHERE Code = 'TRANSFER'","RefId");
   $transfer = intval($transfer);
   $month = getvalue("txtAttendanceMonth");
   $year  = getvalue("txtAttendanceYear");
?>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style type="text/css">
         body {
            font-family:Arial;
         }
         @media print {
            @page {
               size: landscape;
            }
         }
      </style>
   </head>
   <body>
      <div class="container-fluid">
         <div class="row">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12">
                     Form A as of February 2019 
                     <br>
                     <b>Agency Name:</b>&nbsp;&nbsp;
                     Philippine Institute for Development Studies
                     <br>
                     <b>Agency BP Number:</b>&nbsp;&nbsp;
                     BP1000011921
                     <br>
                     <br>
                     <b>FOR AGENCY REMITTANCE ADVICE</b>
                     <br>
                     <br>
                     <b>FORM B.</b> List of transferees
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table style="width: 100%;">
                        <thead>
                           <tr class="colHEADER" valign="bottom">
                              <th rowspan="2">Member BP<br>Number</th>
                              <th rowspan="2">Last Name</th>
                              <th rowspan="2">First Name</th>
                              <th rowspan="2">Suffix</th>
                              <th rowspan="2">Middle<br>Name</th>
                              <th colspan="2">Date of Transfer</th>
                              <th rowspan="2">Salary</th>
                              <th rowspan="2">Position</th>
                              <th rowspan="2">Status of<br>Employment</th>
                           </tr>   
                           <tr class="colHEADER" valign="bottom">
                              <th>From</th>
                              <th>To</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php
                              if ($transfer > 0) {
                                 $where = "WHERE ApptStatusRefId = '$transfer'";
                                 $where .= " AND Month(EffectivityDate) = '$month'";
                                 $where .= " AND Year(EffectivityDate) = '$year'";
                                 $rs    = SelectEach("employeesmovement",$where);
                                 if ($rs) {
                                    while ($row = mysqli_fetch_assoc($rs)) {
                                       $emprefid = $row["EmployeesRefId"];
                                       $From     = $row["EffectivityDate"];
                                       $row_emp  = FindFirst("employees","WHERE RefId = '$emprefid'","*");
                                       $FullName   = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
                                       $ResiStreet = $row_emp["ResiStreet"];
                                       $ResiSubd   = $row_emp["ResiSubd"];
                                       $ResiBrgy   = $row_emp["ResiBrgy"];
                                       $MobileNo   = $row_emp["MobileNo"];
                                       $EmailAdd   = $row_emp["EmailAdd"];
                                       $BirthDate  = $row_emp["BirthDate"];
                                       $BirthPlace = $row_emp["BirthPlace"];
                                       $GSIS       = $row_emp["GSIS"];
                                       $ResiAddCityRefId      = getRecord("city",$row_emp["ResiAddCityRefId"],"Name");
                                       $ResiAddProvinceRefId  = getRecord("province",$row_emp["ResiAddProvinceRefId"],"Name");
                                       $ResiAddress = "";
                                       if ($ResiStreet != "") $ResiAddress .= "$ResiStreet, ";
                                       if ($ResiSubd != "") $ResiAddress .= "$ResiSubd, ";
                                       if ($ResiBrgy != "") $ResiAddress .= "$ResiBrgy, ";
                                       if ($ResiAddCityRefId != "") $ResiAddress .= "$ResiAddCityRefId, ";
                                       if ($ResiAddProvinceRefId != "") $ResiAddress .= "$ResiAddProvinceRefId";
                                       $CivilStat = $row_emp['CivilStatus']; 
                                       switch ($CivilStat) {
                                          case "Si":
                                             $CivilStat = 'Single';
                                          break;
                                          case "Ma":
                                             $CivilStat = 'Married';
                                          break;
                                          case "An":
                                             $CivilStat = 'Annulled';
                                          break;
                                          case "Wi":
                                             $CivilStat = 'Widowed';
                                          break;
                                          case "Se":
                                             $CivilStat = 'Separated';
                                          break;
                                          case "Ot":
                                             $CivilStat = 'Others';
                                          break;
                                       }
                                       $Gender = $row_emp["Sex"];
                                       if ($Gender == "M") {
                                          $Gender = "Male";
                                       } else if ($Gender == "F") {
                                          $Gender = "Female";
                                       } else {
                                          $Gender = "";
                                       }
                                       if ($BirthDate != "") {
                                          $BirthDate = date("m/d/Y",strtotime($BirthDate));
                                       } else {
                                          $BirthDate = "";
                                       }
                                       $emp_info = FindFirst("empinformation","WHERE EmployeesRefId = ".$row_emp["RefId"],"*");
                                       if ($emp_info) {
                                          $Position      = rptDefaultValue($emp_info["PositionRefId"],"position");
                                          $ApptStatus    = rptDefaultValue($emp_info["ApptStatusRefId"],"apptstatus");
                                          $SalaryAmount  = number_format($emp_info["SalaryAmount"],2);
                                          $Assumption    = date("m/d/Y",strtotime($emp_info["AssumptionDate"]));
                                       } else {
                                          $Position      = "";
                                          $ApptStatus    = "";
                                          $Assumption    = "";
                                          $SalaryAmount  = "0.00";
                                       }
                                       echo '<tr>';
                                          echo '
                                             <td class="text-center">'.$GSIS.'</td>
                                             <td>'.$row_emp["LastName"].'</td>
                                             <td>'.$row_emp["FirstName"].'</td>
                                             <td>'.$row_emp["ExtName"].'</td>
                                             <td>'.$row_emp["MiddleName"].'</td>
                                             <td class="text-center">'.date("m-d-Y",strtotime($From)).'</td>
                                             <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                             <td class="text-right">P '.$SalaryAmount.'</td>
                                             <td>'.$Position.'</td>
                                             <td>'.$ApptStatus.'</td>
                                          ';
                                       echo '</tr>';
                                    }
                                 } else {
                                    echo '<tr>';
                                    echo '<td colspan="10">No Record Found</td>';
                                    echo '</tr>';
                                 }
                              }
                           ?>
                        </tbody>
                     </table>
                  </div>
               </div>
               <br>
               <br>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-6">
                     Prepared by:
                     <br><br><br>
                     <b><u><?php echo $prepared_by; ?></u></b>
                     <br>
                     <?php echo $prepared_by_position; ?>
                  </div>
                  <div class="col-xs-6">
                     Certified Correct:
                     <br><br><br>
                     <b><u><?php echo $corrected_by; ?></u></b>
                     <br>
                     <?php echo $corrected_by_position; ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>