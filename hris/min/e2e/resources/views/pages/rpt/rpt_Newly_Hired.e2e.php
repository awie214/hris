<?php
	
?>
<!DOCTYPE html>
<html>
   	<head>
      	<?php include_once $files["inc"]["pageHEAD"]; ?>
      	<link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      	<style>
         	td {vertical-align:top;}
      	</style>
   	</head>
   	<body>
      	<div class="container-fluid rptBody">
      		<div class="row">
      			<div class="col-xs-12">
      				<?php
			            rptHeader("NEWLY HIRED EMPLOYEES","HRDMS-R-020");
			         ?>
      				<table style="width: 100%" border="1">
      					<thead>
      						<tr class="colHEADER">
      							<th>#</th>
	      						<th>Employee Name</th>
                           <th>Sex</th>
                           <th>Employee ID</th>
                           <th>Employment Status</th>
	      						<th>Date Started/Hired</th>	
                           <th>Position Title</th>
                           <th>Assigned Division</th>
      						</tr>
      						
      					</thead>
      					<tbody>
      						<?php
      							$count = 0;
      							$past_month = date("Y",time());
	                            $newly_hired_rs = SelectEach("empinformation","WHERE Year(HiredDate) = '$past_month'");
	                            if ($newly_hired_rs) {
	                                while ($newly_hired_row = mysqli_fetch_assoc($newly_hired_rs)) {
	                                    $emprefid           = $newly_hired_row["EmployeesRefId"];
	                                    $hired_date         = $newly_hired_row["HiredDate"];
                                       $empstatus          = $newly_hired_row["EmpStatusRefId"];
                                       $position           = $newly_hired_row["PositionRefId"];
                                       $division           = $newly_hired_row["DivisionRefId"];
                                       $position           = getRecord("position",$position,"Name");
                                       $division           = getRecord("division",$division,"Name");
                                       $empstatus          = getRecord("empstatus",$empstatus,"Name");
	                                    $fld = "`LastName`,`FirstName`,`MiddleName`,`AgencyId`, `Sex`";
	                                    $row_emp = FindFirst("employees","WHERE RefId = '$emprefid'",$fld);
	                                    if ($row_emp) {
	                                    	$count++;
	                                    	$AgencyId = $row_emp["AgencyId"];
                                          $Sex      = $row_emp["Sex"];
	                                    	$FullName = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
	                                    	echo '<tr>';
	                                    		echo '
	                                    			<td class="text-center">'.$count.'</td>
                                                <td>'.$FullName.'</td>
                                                <td class="text-center">'.$Sex.'</td>
	                                    			<td class="text-center">'.$AgencyId.'</td>
	                                    			<td class="text-center">'.$empstatus.'</td>
	                                    			<td class="text-center">'.date("d M Y",strtotime($hired_date)).'</td>
                                                <td class="text-center">'.$position.'</td>
                                                <td class="text-center">'.$division.'</td>
	                                    		';
	                                    	echo '</tr>';
	                                    }
	                                }
	                            }
      						?>
      					</tbody>
      				</table>
      			</div>
      		</div>
      	</div>
    </body>
</html>