<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $count = 0;
            if ($rsEmployees) {
         ?>
         
         <table style="width: 100%;">
            <thead>
               <tr>
                  <td colspan="4">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                  </td>
               </tr>
               <tr class="colHEADER">
                  <th>Sequence</th>
                  <th>Employee Name</th>
                  <th>Employee ID</th>
                  <th>Details</th>
               </tr>
            </thead>
            <tbody>
               <?php
                  while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                     
                     $FullName   = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
                     $check_pdsq = FindFirst("employeespdsq","WHERE EmployeesRefId = ".$row_emp["RefId"],"*");
                     if ($check_pdsq) {
                        if ($check_pdsq["Q7c"] == 1) {
                           $count++;
                           echo '
                              <tr>
                                 <td class="text-center">'.$count.'</td>
                                 <td>'.strtoupper($FullName).'</td>
                                 <td class="text-center">'.$row_emp["AgencyId"].'</td>
                                 <td class="text-center">'.$check_pdsq["Q7cexp"].'</td>
                              </tr>
                           ';
                        }
                     }
                  }
               ?>
               
            </tbody>
         </table>
         <?php
            } else {
         ?>
         <?php
            rptHeader(getvalue("RptName"));
         ?>
         <table border="1" style="width: 100%;">
            <thead>
               <tr>
                  <th>Sequence</th>
                  <th>Employee Name</th>
                  <th>Employee ID</th>
                  <th>Details</th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td colspan="4"> No Record Found.</td>
               </tr>
            </tbody>
         </table>
         <?php
            }
         ?>
      </div>
   </body>
</html>