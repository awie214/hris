<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   function countRating($column,$refid) {
      require "conn.e2e.php";
      $sql = "SELECT `$column` FROM training_evaluation WHERE LDMSLNDProgramRefId = '$refid'";
      $rs  = mysqli_query($conn,$sql);
      if ($rs) {
         $num_row = mysqli_num_rows($rs);
         if ($num_row > 0) {
            $total = 0;
            while ($row = mysqli_fetch_assoc($rs)) {
               $total += $row[$column];
            }
            $count = $total / $num_row;
         } else {
            $count = 0;   
         }
      } else {
         $count = 0;
      }
      return $count;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style type="text/css">
         .gray {background: gray;}
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12 text-center">
                     <b>BUDGET UTILIZATION REPORT ON TRAININGS CONDUCTED</b>
                     <br>
                     For the Year 2019
                     <br>
                     January - June
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table width="100%">
                        <thead>
                           <tr class="colHEADER">
                              <th>
                                 TITLE OF THE TRAINING/PROGRAM
                              </th>
                              <th>
                                 Date Conducted
                              </th>
                              <th>
                                 Approved Budget
                              </th>
                              <th>
                                 Amount Utilized
                              </th>
                              <th>Remarks</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php
                              $rs = SelectEach("ldmslndprogram"," ORDER BY StartDate DESC");
                              if ($rs) {
                                 while ($row = mysqli_fetch_assoc($rs)) {
                                    $Name       = $row["Name"];
                                    $StartDate  = $row["StartDate"];
                                    $EndDate    = $row["EndDate"];
                                    echo '<tr valign="top">';
                                    echo '<td>'.$Name.'</td>';
                                    echo '<td>'.date("F d, Y",strtotime($StartDate)).' to '.date("F d, Y",strtotime($EndDate)).'</td>';
                                    echo '<td class="text-center">'.number_format($row["Cost"]).'</td>';
                                    echo '<td></td>';
                                    echo '<td></td>';
                                    echo '</tr>';
                                 }
                              }
                           ?>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>
