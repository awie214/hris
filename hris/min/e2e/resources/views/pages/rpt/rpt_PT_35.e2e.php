<?php
   include_once 'pageHEAD.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   $refid = getvalue("refid");
   $row = FindFirst("employeesauthority","WHERE RefId = $refid","*");
   if ($row) {
         $Signatory1       = $row["Signatory1"];
         $Signatory2       = $row["Signatory2"];
         $Signatory3       = $row["Signatory3"];

         $s1_row           = FindFirst("signatories","WHERE RefId = '$Signatory1'","*");
         $s2_row           = FindFirst("signatories","WHERE RefId = '$Signatory2'","*");

         $s1_name          = $s1_row["Name"];
         $s1_position      = getRecord("position",$s1_row["PositionRefId"],"Name");

         $s2_name          = $s2_row["Name"];
         $s2_position      = getRecord("position",$s2_row["PositionRefId"],"Name");
          
   		$authority 		= getRecord("absences",$row["AbsencesRefId"],"Name");
   		$authority 		= strtoupper($authority);
   		$Purpose 		= $row["Remarks"];
   		$Remarks 		= $authority." - ".$row["Remarks"];
   		$Destination 	= $row["Destination"];
   		$emprefid 		= $row["EmployeesRefId"];
   		$FiledDate 		= $row["FiledDate"];
   		$TO_no			= "TO-".date("Y",strtotime($FiledDate))."".date("m",strtotime($FiledDate))."-".str_pad($refid, 3, '0',STR_PAD_LEFT);
   		$employees = FindFirst("employees","WHERE RefId = '$emprefid'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
   		if ($employees) {
   			$FirstName = $employees["FirstName"];
   			$LastName = $employees["LastName"];
   			$MiddleName = $employees["MiddleName"];
   			$ExtName = $employees["ExtName"];
   			$FullName = $LastName.", ".$FirstName." $ExtName ".$MiddleName;
   		} else {
   			$FullName = "&nbsp;";
   		}
   		$empinformation = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
   		if ($empinformation) {
   			$OfficeRefId = getRecord("office",$empinformation["OfficeRefId"],"Name");
   			$DivisionRefId = getRecord("division",$empinformation["DivisionRefId"],"Name");
   			$PositionRefId = getRecord("position",$empinformation["PositionRefId"],"Name");
   		} else {
   			$OfficeRefId = $DivisionRefId = $PositionRefId = "&nbsp;";
   		}
   		if ($row["Whole"] == 1) {
   			$start_time = $end_time = "Whole Day";
   		} else {
   			$start_time = convertToHoursMins($row["FromTime"]);
   			$end_time = convertToHoursMins($row["ToTime"]);
   		}
   		$from = $row["ApplicationDateFrom"];
   		$to = $row["ApplicationDateTo"];
   		if ($from == $to) {
   			$date = date("F d, Y",strtotime($from));
   		} else {
   			$date = date("F d, Y",strtotime($from))." to ".date("F d, Y",strtotime($to));;
   		}   		
   }
   	$user = getvalue("hUserRefId");
   	$user_row = FindFirst("employees","WHERE RefId = '$user'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
   	if ($user_row) {
      $user_LastName   = $user_row["LastName"];
      $user_FirstName  = $user_row["FirstName"];
   	} else {
      $user_FirstName = $user_LastName = "";
   	}
?>
<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		td {
			border: 2px solid black;
			vertical-align: top;
			padding: 5px;
			font-size: 9pt;
		}
		.data {
			font-size: 10pt;
			text-transform: uppercase;
		}
        @media print {
            .header-space {margin-top: 30%;}
        }
	</style>
</head>
<body>
	<div class="container-fluid rptBody">
		<div style="page-break-after: always;">
	        <div class="row">
                <div class="col-xs-12 header-space">
                    <div class="row">
                        <div class="col-xs-12">
                            <b>
                                <?php echo date("d F Y",strtotime($FiledDate)); ?>
                            </b>
                            <br><br><br>
                        </div>
                    </div>
                    <div class="row margin-top">
                        <div class="col-xs-12">
                            <b>AUTHORITY TO TRAVEL</b>
                            <br>
                            <b>PCW No. 19-01-01</b>
                            <br>
                            <b>Series of 2019</b>
                            <br><br>
                        </div>
                    </div>
                    <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                            <b>
                            <?php
                                echo '
                                    AUTHORIZING THE PERSONAL TRAVEL OF '.strtoupper($FullName).', '.strtoupper($PositionRefId).', FROM '.strtoupper($date).' TO '.strtoupper($Destination).'.
                                ';
                            ?>
                            </b>
                            <br><br>
                        </div>
                    </div>
                    <div class="row margin-top">
                        <div class="col-xs-12">
                            <?php
                                echo '
                                    Authority is hereby given to '.strtoupper($FullName).', '.strtoupper($PositionRefId).' of the '.strtoupper($DivisionRefId).' of the Philippine Commission on Women (PCW), to travel to the '.$Destination.' from '.$date.'.
                                ';
                            ?>
                            <br><br>
                            The said travel, being personal in nature, shall entail no cost to the Philippine government.
                            <br><br>
                            <?php echo $LastName; ?> shall be considered on leave for the duration of her travel.
                        </div>
                    </div>
                    <div class="row margin-top">
                        <div class="col-xs-11 text-right">
                           <b>
                              <?php echo $s2_name; ?>
                           </b>
                           <br>
                           <?php echo $s2_position; ?>
                        </div>
                        <div class="col-xs-1">&nbsp;</div>
                    </div>
                </div>
            </div>
	    </div>
    </div>
</body>
</html>