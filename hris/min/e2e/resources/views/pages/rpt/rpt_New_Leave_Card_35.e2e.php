<?php
   require_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   require_once pathClass.'0620RptFunctions.e2e.php';
   require_once pathClass.'DTRFunction.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   //$whereClause .= " LIMIT 10";
   $year             = getvalue("txtAttendanceYear");
   $table            = "employees";
   $rsEmployees      = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   $arr_month =[
     "January",
     "February",
     "March",
     "April",
     "May",
     "June",
     "July",
     "August",
     "September",
     "October",
     "November",
     "December"
   ]; 
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         td {
            vertical-align: top;
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
      <?php
         $rsEmployees = SelectEach("employees",$whereClause);
         if ($rsEmployees) {
            while ($row = mysqli_fetch_assoc($rsEmployees)) {
               $LastName         = $row["LastName"];
               $FirstName        = $row["FirstName"];
               $MiddleName       = $row["MiddleName"];
               $ExtName          = $row["ExtName"];
               $EmployeesRefId   = $row["RefId"];
               $CompanyRefId     = $row["CompanyRefId"];
               $BranchRefId      = $row["BranchRefId"];
               $FullName         = $LastName.", ".$FirstName." ".$ExtName." ".$MiddleName;
               $emp_row = FindFirst("empinformation","WHERE EmployeesRefId = ".$row["RefId"],"*");
               if ($emp_row) {
                  $Position   = getRecord("Position",$emp_row["PositionRefId"],"Name");
                  $Division   = getRecord("Division",$emp_row["DivisionRefId"],"Name");
                  $Office     = getRecord("Office",$emp_row["OfficeRefId"],"Name");
                  $Salary     = number_format(intval($emp_row["SalaryAmount"]),2);
                  $Annual     = intval($emp_row["SalaryAmount"]) * 12;
                  $Annual     = number_format($Annual,2);
               } else {
                  $Position   = "";
                  $Division   = "";
                  $Office     = "";
                  $Salary     = "";
                  $Annual     = "";
               }
               $AsOf = $SL_Bal = $VL_Bal = $start = "";
               $where_credit  = "WHERE EmployeesRefId = '$EmployeesRefId'";
               $where_credit  .= " AND EffectivityYear = '$year'";
               $credits_rs    = SelectEach("employeescreditbalance",$where_credit);
               if ($credits_rs) {
                  while ($credits_row = mysqli_fetch_assoc($credits_rs)) {
                     $credit_name   = $credits_row["NameCredits"];
                     $AsOf          = $credits_row["BegBalAsOfDate"];
                     $AsOf          = date("F d, Y",strtotime($AsOf));
                     $start         = date("Y-m-d",strtotime($AsOf." + 1 Days"));
                     switch ($credit_name) {
                        case 'VL':
                           $VL_Bal  = $credits_row["BeginningBalance"];
                           break;
                        case 'SL':
                           $SL_Bal = $credits_row["BeginningBalance"];
                           break;
                     }
                  }
               }
      ?>
      <div style="page-break-after: always;">
         <?php
            rptHeader("Record of Leave of Absence");
         ?>
         <div class="row">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-4">
                     Employee Name: <b><u><?php echo $FullName; ?></u></b>
                  </div>
                  <div class="col-xs-8">
                     Division/Unit/Office: <b><u><?php echo $Division." / ".$Office; ?></u></b>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-4">
                     Position: <b><u><?php echo $Position; ?></u></b>
                  </div>
                  <div class="col-xs-4">
                     Annual Salary: <b><u>P <?php echo $Annual; ?></u></b>
                  </div>
                  <div class="col-xs-4">
                     Monthly Salary: <b><u>P <?php echo $Salary; ?></u></b>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-4">
                     Leave Credits Earned as of: <b><u><?php echo $AsOf; ?></u></b>
                  </div>
                  <div class="col-xs-4">
                     Vacation Leave: <b><u><?php echo $VL_Bal; ?></u></b>
                  </div>
                  <div class="col-xs-4">
                     Sick Leave: <b><u><?php echo $SL_Bal; ?></u></b>
                  </div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table width="100%">
                        <thead>
                           <tr class="colHEADER">
                              <th rowspan="2">MONTH</th>
                              <th rowspan="2">DAYS<br>OF LEAVE</th>
                              <th colspan="3">LEAVE WITH PAY</th>
                              <th colspan="2">LEAVE WITHOUT PAY</th>
                              <th colspan="2">LEAVE EARNED</th>
                              <th colspan="2">BALANCE</th>
                              <th rowspan="2">POSTED BY</th>
                           </tr>
                           <tr class="colHEADER">
                              <th>UNDERTIMES<br>Number of Hrs./Mins.</th>
                              <th>Vacation</th>
                              <th>Sick</th>
                              <th>Vacation</th>
                              <th>Sick</th>
                              <th>Vacation</th>
                              <th>Sick</th>
                              <th>Vacation</th>
                              <th>Sick</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php
                              if ($start == "") {
                                 echo '<tr><td colspan="12">No Data Yet</td></tr>';
                              } else {
                                 $start_month   = date("m",strtotime($start));
                                 for ($a=intval($start_month); $a <= 12; $a++) { 
                                    $VL_date    = "";
                                    $SL_date    = "";
                                    $UT_date    = "";
                                    $Tdy_date   = "";
                                    if ($a <= 9) $a = "0".$a;
                                    $dtr_where     = "WHERE EmployeesRefId = '$EmployeesRefId'";
                                    $dtr_where     .= " AND Month = '$a'";
                                    $dtr_where     .= " AND Year = '$year'";   
                                    $dtr_row       = FindFirst("dtr_process",$dtr_where,"*");
                                    if ($dtr_row) {
                                       $Selected_Month         = $arr_month[intval($a) - 1];
                                       $Total_Tardy_Hr         = $dtr_row["Total_Tardy_Hr"];
                                       $Total_Undertime_Hr     = $dtr_row["Total_Undertime_Hr"];
                                       $Total_Absent_Count     = $dtr_row["Total_Absent_Count"];
                                       $VL_Earned              = $dtr_row["VL_Earned"];
                                       $SL_Earned              = $dtr_row["SL_Earned"];
                                       $Tardy_Deduction_EQ     = $dtr_row["Tardy_Deduction_EQ"];
                                       $Undertime_Deduction_EQ = $dtr_row["Undertime_Deduction_EQ"];
                                       $VL_Used                = $dtr_row["VL_Used"];
                                       $VL_Days                = $dtr_row["VL_Days"];
                                       $SL_Used                = $dtr_row["SL_Used"];
                                       $SL_Days                = $dtr_row["SL_Days"];
                                       $Total_Tardy_UT         = $Total_Tardy_Hr + $Total_Undertime_Hr;
                                       $Total_Deduct           = $Tardy_Deduction_EQ 
                                                                 + $Undertime_Deduction_EQ;
                                       $VL_Bal                 -= $Total_Deduct;
                                       $VL_Bal                 -= $VL_Used;
                                       $VL_Bal                 += $VL_Earned;
                                       $SL_Bal                 -= $SL_Used;
                                       $SL_Bal                 += $SL_Earned;

                                       if ($VL_Days != "") {
                                          $VL_Days_arr = explode("|", $VL_Days);
                                          foreach ($VL_Days_arr as $vl_key => $vl_value) {
                                             $VL_date  .= $vl_value." ";
                                          }
                                          $VL_date .= " (VL)";
                                       }
                                       if ($SL_Days != "") {
                                          $SL_Days_arr = explode("|", $SL_Days);
                                          foreach ($SL_Days_arr as $sl_key => $sl_value) {
                                             $SL_date  .= $sl_value." ";
                                          }
                                          $SL_date .= " (SL)";
                                       }

                                       for ($b=1; $b <= 31; $b++) { 
                                          if ($b <= 9) $b = "0".$b;
                                          if (intval($dtr_row[$b."_UT"]) > 0) {
                                             $UT_date .= $b." (".rpt_HoursFormat($dtr_row[$b."_UT"]).") ";
                                          }
                                       }
                                       for ($c=1; $c <= 31; $c++) { 
                                          if ($c <= 9) $c = "0".$c;
                                          if (intval($dtr_row[$c."_Late"]) > 0) {
                                             $Tdy_date .= $c." (".rpt_HoursFormat($dtr_row[$c."_Late"]).") ";
                                          }
                                       }
                                       echo '<tr>';
                                          echo '<td>'.$Selected_Month.'</td>';
                                          echo '<td>'.$VL_date.' '.$SL_date.'</td>';
                                          echo '<td>'.$UT_date.' '.$Tdy_date.'</td>';
                                          echo '<td class="text-center">'.$VL_Used.'</td>';
                                          echo '<td class="text-center">'.$SL_Used.'</td>';
                                          echo '<td></td>';
                                          echo '<td></td>';
                                          echo '<td class="text-center">'.$VL_Earned.'</td>';
                                          echo '<td class="text-center">'.$SL_Earned.'</td>';
                                          echo '<td class="text-center">'.number_format($VL_Bal,3).'</td>';
                                          echo '<td class="text-center">'.number_format($SL_Bal,3).'</td>';
                                          echo '<td></td>';
                                       echo '</tr>';
                                    }
                                 }
                                 
                              }
                           ?>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php 
            }
         }
      ?>
      </div>
   </body>
</html>