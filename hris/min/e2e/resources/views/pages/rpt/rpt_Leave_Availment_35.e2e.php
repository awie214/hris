<?php
   include_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   $leave_refid                  = getvalue("refid");
   $spl_type                     = "";
   $personnel_officer            = "";
   $authorized_official          = "";
   $approver                     = "";
   $personnel_officer_position   = "";
   $authorized_official_position = "";
   $approver_position            = "";
   $e_signature                  = path."images/35/rjmmayrena-esign-v2.png";
   
   $leave_row     = FindFirst("employeesleave","WHERE RefId = $leave_refid","*");
   if ($leave_row) {
      $emprefid   = $leave_row["EmployeesRefId"];
      $leave_type = $leave_row["LeavesRefId"];
      $Status     = $leave_row["Status"];
      $spl_type   = $leave_row["SPLType"];
      $leave_name = FindFirst("leaves","WHERE RefId = $leave_type","*");
      $name       = $leave_name["Name"];
      $code       = $leave_name["Code"];
      $RD_leave   = $leave_name["RestDay"];
      $FiledDate  = date("d F Y",strtotime($leave_row["FiledDate"]));
      $to         = date("d F Y",strtotime($leave_row["ApplicationDateTo"]));
      $from       = date("d F Y",strtotime($leave_row["ApplicationDateFrom"]));
      $Signatory1 = getRecord("signatories",$leave_row["Signatory1"],"Name");
      $Signatory2 = getRecord("signatories",$leave_row["Signatory2"],"Name");
      $Signatory3 = getRecord("signatories",$leave_row["Signatory3"],"Name");
      $Signatory1_pos = getRecord("signatories",$leave_row["Signatory1"],"PositionRefId");
      $Signatory2_pos = getRecord("signatories",$leave_row["Signatory2"],"PositionRefId");
      $Signatory3_pos = getRecord("signatories",$leave_row["Signatory3"],"PositionRefId");

      $personnel_officer   = $Signatory1;
      $authorized_official = $Signatory2;
      $approver            = $Signatory3;

      $personnel_officer_position   = getRecord("position",$Signatory1_pos,"Name");
      $authorized_official_position = getRecord("position",$Signatory2_pos,"Name");
      $approver_position            = getRecord("position",$Signatory3_pos,"Name");

      
      
      if ($to == $from) {
         $Inclusive_Date = $from;
      } else {
         $Inclusive_Date = $from." to ".$to;   
      }
      
      


      if ($RD_leave == 1) {
         $day_count = FindFirst("leavepolicy","WHERE LeavesRefId = '$leave_type'","Value");
         $day_count = intval($day_count);
      } else {
         $day_count  = count_leave($emprefid,$leave_row["ApplicationDateFrom"],$leave_row["ApplicationDateTo"]);   
      }
       
      //if ($day_count == 0) $day_count = 1;
      $emp_row       = FindFirst("employees","WHERE RefId = $emprefid","*");
      if ($emp_row) {
         $LastName      = $emp_row["LastName"];
         $FirstName     = $emp_row["FirstName"];
         $MiddleName    = $emp_row["MiddleName"];   
         $ExtName       = $emp_row["ExtName"];
         $MiddleInitial = substr($MiddleName, 0,1);
         $FullName      = $FirstName." ".$MiddleInitial.". ".$LastName;
         $cid           = $emp_row["CompanyRefId"];
      } else {
         $FullName = $ExtName = $LastName = $FirstName = $MiddleName = "&nbsp;";
      }
      
      $empinfo_row   = FindFirst("empinformation","WHERE EmployeesRefId = $emprefid","*");

      if ($empinfo_row) {
         $Office        = getRecord("office",$empinfo_row["OfficeRefId"],"Name");
         $Agency        = getRecord("agency",$empinfo_row["AgencyRefId"],"Name");
         $Position      = getRecord("position",$empinfo_row["PositionRefId"],"Name");
         $Division      = getRecord("Division",$empinfo_row["DivisionRefId"],"Name");
         $SalaryAmount  = "₱ ".number_format($empinfo_row["SalaryAmount"],2);
         
         $DivisionRefId = $empinfo_row["DivisionRefId"];
      } else {
         $Office        = "&nbsp;";
         $Agency        = "&nbsp;";
         $Position      = "&nbsp;";
         $Division      = "&nbsp;";
         $SalaryAmount  = "&nbsp;";
         $DivisionRefId = 0;
      }

      /*$where_credit     = "WHERE EmployeesRefId = '$emprefid'";
      $where_credit     .= " AND NameCredits = 'VL'";
      $where_credit     .= "  AND EffectivityYear = '".date("Y",strtotime($to))."'";
      $credit_detail    = FindFirst("employeescreditbalance",$where_credit,"*");
      if ($credit_detail) {
         $selected_month = date("m",strtotime($to));
         $year          = $credit_detail["EffectivityYear"]; 
         $balance_date  = $credit_detail["BegBalAsOfDate"];
         $from          = date("m",strtotime($balance_date." + 1 Day"));
         if (intval(date("m",strtotime($to))) >= 1) {
            $to            = date("m",strtotime($to)) - 1;   
         } else {
            $to            = 12;
         }

         $credit_arr    = computeCredit($emprefid,$from,$to,$year);
         $num_of_days   = cal_days_in_month(CAL_GREGORIAN,$selected_month,$year);
         $BegBalDate    = "$year-$selected_month-$num_of_days";
         $BegBalDate    = date("F d, Y",strtotime($BegBalDate));
         $VL            = $credit_arr["VL"];
         $SL            = $credit_arr["SL"];
         $SPL           = $credit_arr["SPL"];
         $total_leave   = $VL + $SL;
      } else {
         $VL = $SL = $total_leave = $SPL = 0;
         $BegBalDate = "";
      }*/
      $VL = $SL = $total_leave = $SPL = 0;
      /*$BegBalDate = "";
      $where_credit     = "WHERE EmployeesRefId = '$emprefid'";
      $where_credit     .= "  AND EffectivityYear = '".date("Y",strtotime($to))."'";
      $credit_detail    = SelectEach("employeescreditbalance",$where_credit);
      if ($credit_detail) {
         while ($credit_arr = mysqli_fetch_assoc($credit_detail)) {
            $credit_name = $credit_arr["NameCredits"];
            $credit_bal  = $credit_arr["BeginningBalance"];
            switch ($credit_name) {
               case 'VL':
                  $VL = $credit_bal;
                  break;
               case 'SL':
                  $SL = $credit_bal;
                  break;
               case 'SPL':
                  $SPL = $credit_bal;
                  break;
            }
            $BegBalDate = date("d F Y",strtotime($credit_arr["BegBalAsOfDate"]));
         }
      }*/
      $credit_detail    = FindLast("employeescreditbalance","WHERE EmployeesRefId = '$emprefid' AND NameCredits = 'VL' AND EffectivityYear = '".date("Y",strtotime($leave_row["ApplicationDateTo"]))."'","*");
      if ($credit_detail) {
         $year          = $credit_detail["EffectivityYear"]; 
         $balance_date  = $credit_detail["BegBalAsOfDate"];
         $from          = date("m",strtotime($balance_date." + 1 Day"));
         if (intval(date("m",strtotime($leave_row["ApplicationDateTo"]))) > 1) {
            $to            = date("m",strtotime($leave_row["ApplicationDateTo"])) - 1;   
         } else {
            $to            = 0;
         }
         $credit_arr    = computeCredit($emprefid,$from,$to,$year);
         if ($to == 0) $to = 12;
         if ($to <= 9) {
            
            $selected_month = "0".$to;   
         } else {
            $selected_month = $to;
         }
         $num_of_days   = cal_days_in_month(CAL_GREGORIAN,$selected_month,$year);
         $BegBalDate    = "$year-$selected_month-$num_of_days";
         $BegBalDate    = date("F d, Y",strtotime($BegBalDate));
         $VL            = $credit_arr["VL"];
         $SL            = $credit_arr["SL"];
         $total_leave   = $VL + $SL;
      } else {
         $VL = $SL = $total_leave = 0;
         $BegBalDate = "";
      }
      $total_leave   = $VL + $SL;
   }
   

?>
<!DOCTYPE HTML>
<html>
   <head>
      <?php 
         $file = "Availment of Leave";
         include_once 'pageHEAD.e2e.php';
         
      ?>
      <style type="text/css">
         .form-input {text-align: center;}
         @media print {
            body {
               font-size: 8pt;
            }
            label {
               font-size: 9pt !important;
            }
            .form-input {
               border: none;
               padding: 0px;
               font-weight: 400;
               text-decoration: underline;
               text-align: center;
            }
         }
         .e_signature {
            width: 100px;
            height: 100px;
            position: absolute;
            margin-left: 33%;
            top: 115px;
         }
         .watermark {
            position:fixed;
            right:5px;
            opacity:0.5;
            z-index:99;
            color:gray;
            border-radius: 10px;
            font-size: 15pt;
            text-transform: uppercase;
            font-weight: 600;
         }
      </style>
      <script type="text/javascript">
         $(document).ready(function () {
            //$("[type*='checkbox']").prop("disabled",true);
            EmployeeAutoComplete("employees","personnel_officer");
            EmployeeAutoComplete("employees","authorized_official");
            EmployeeAutoComplete("employees","approver");
            $("#personnel_officer, #authorized_official, #approver").blur(function () {
               var value = $(this).val();
               arr = value.split("-");
               $(this).val(arr[1]);
            });
            <?php
               switch ($code) {
                  case 'VL':
                     echo '$("#VL_chk").prop("checked",true);';
                     $name = "";
                     break;
                  case 'SL':
                     echo '$("#SL_chk").prop("checked",true);';
                     $name = "";
                     break;
                  case 'ML':
                     echo '$("#ML_chk").prop("checked",true);';
                     $name = "";
                     break;
                  default:
                     echo '$("#OT_chk").prop("checked",true);';
                     break;
               }
               if ($Status == "Approved") {
                  echo '$("#is_approve").prop("checked",true);';
                  echo '$(".watermark").hide();';
               } else {
                  echo '$(".watermark").show();';
                  
                  echo '$("#watermark_data").html("'.$Status.'");';
               }
               if ($spl_type != "") {
                  switch ($spl_type) {
                     case 'PM':
                        $name = "SPL - Personal Milestone";
                        break;
                     case 'PO':
                        $name = "SPL - Parental Obligation";
                        break;
                     case 'Fil':
                        $name = "SPL - Fillial";
                        break;
                     case 'DomE':
                        $name = "SPL - Domestic Emergencies";
                        break;
                     case 'PTrn':
                        $name = "SPL - Personal Transactions";
                        break;
                     case 'C':
                        $name = "SPL - Calamity";
                        break;
                  }
               }
               echo '$("#other_leave").html("'.$name.'");';
            ?>

         });
      </script>
   </head>
   <body>
      <div class="container-fluid">
         <div class="row watermark">
            <div class="col-xs-12">
               <span id="watermark_data"></span>
            </div>
         </div>
         <div class="watermark">
            
         </div>
         <div class="row">
            <div class="col-xs-12">
               CSC Form 6
               <br>
               Revised 1998
            </div>
         </div>
         <div class="row text-center" style="border:1px solid black; border-bottom: 1px solid white;">
            <div class="col-xs-12">
               <h3>APPLICATION FOR LEAVE</h3>
            </div>
         </div>
         <div class="row" style="border:1px solid black;margin-bottom:40px;">
            <div class="col-xs-12">
               <div class="row" style="border-bottom:1px solid black;">
                  <div class="col-xs-3" style="border-right:1px solid black;">
                     <label>1. Office/Agency</label>
                     <br>
                     <?php echo $Division; ?>
                  </div>
                  <div class="col-xs-9">
                     <div class="row">
                        <div class="col-xs-2">
                           <label>2. Name</label>
                           <br>
                           &nbsp;
                        </div>
                        <div class="col-xs-10">
                           <div class="row">
                              <div class="col-xs-4">
                                 <label>(Last)</label>
                                 <br>
                                 <b><?php echo $LastName; ?></b>
                              </div>
                              <div class="col-xs-4">
                                 <label>(First)</label>
                                 <br>
                                 <b><?php echo $FirstName." ".$ExtName; ?></b>
                              </div>
                              <div class="col-xs-4">
                                 <label>(Middle)</label>
                                 <br>
                                 <b><?php echo $MiddleName; ?></b>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row" style="border-bottom:1px solid black;">
                  <div class="col-xs-3" style="border-right:1px solid black;">
                     <label>3. Date of Filing</label>
                     <br>
                     <?php echo $FiledDate; ?>
                  </div>
                  <div class="col-xs-9">
                     <div class="row">
                        <div class="col-xs-6" style="border-right:1px solid black;">
                           <label>4. Position</label>
                           <br>
                           <?php echo $Position; ?>
                        </div>
                        <div class="col-xs-6">
                           <label>5. Salary</label>
                           <br>
                           <?php echo $SalaryAmount; ?>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row text-center" style="border-bottom:1px solid black;">
                  <label>DETAILS OF APPLICATION</label>
               </div>
               <div class="row" style="border-bottom:1px solid black;">
                  <div class="col-xs-6">
                     <div class="row">
                        <div class="col-xs-12">
                           <label>6. a) Type of Leave:</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row">
                              <input type="checkbox" id="VL_chk">&nbsp;<label>Vacation</label>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-11">
                                 <div class="row">
                                    <input type="checkbox">&nbsp;<label>To seek employment</label>
                                 </div>
                                 <div class="row margin-top">
                                    <input type="checkbox">&nbsp;<label>Others (Specify)</label>
                                    <u><input type="text" name="" id="" class="form-input"></u>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row margin-top">
                              <input type="checkbox" id="SL_chk">&nbsp;<label>Sick</label>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox" id="ML_chk">&nbsp;<label>Maternity</label>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox" id="OT_chk">&nbsp;<label>Others (Specify)</label>
                              <label><u id="other_leave"></u></label>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-6" style="border-left:1px solid black;">
                     <div class="row">
                        <div class="col-xs-12">
                           <label>6. b) Where Leave will be Spent:</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row margin-top">
                              <label>(1)&nbsp;In case of Vacation Leave:</label>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox">&nbsp;<label>Within the Philippines</label>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox">&nbsp;<label>Abroad (Specify)</label>
                              <!-- <label>__________________________________________</label> -->
                              <u><input type="text" name="" id="" class="form-input"></u>
                           </div>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row margin-top">
                              <label>(2)&nbsp;In case of Sick Leave:</label>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox">&nbsp;<label>In hospital (Specify)</label>
                              <br>
                              <!-- <label>__________________________________________</label> -->
                              <u><input type="text" name="" id="" class="form-input"></u>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox">&nbsp;<label>Out-Patient (Specify)</label>
                              <br>
                              <!-- <label>__________________________________________</label> -->
                              <u><input type="text" name="" id="" class="form-input"></u>
                           </div>
                           <br>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row" style="border-bottom:1px solid black;">
                  <div class="col-xs-6">
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           <label>6. c) # of Working Day/s Applied for:</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <label>
                              <?php
                                 echo '<u>'.$day_count.' day/s</u>';
                              ?>
                           </label>
                           <br>
                           <label>Inclusive Date/s:</label>
                           <br>
                           <label><u><?php echo $Inclusive_Date; ?></u></label>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-6" style="border-left:1px solid black;">
                     <div class="row">
                        <div class="col-xs-12">
                           <label>6. d) Commutation:</label>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row">
                              <div class="col-xs-6">
                                 <input type="checkbox">&nbsp;<label>Requested</label>
                              </div>
                              <div class="col-xs-6">
                                 <input type="checkbox">&nbsp;<label>Not Requested</label>
                              </div>
                           </div>
                           <div class="row margin-top text-center">
                              <br><br><br>
                              <label><u><?php echo $FullName; ?></u></label>
                              <br>
                              <label>(Signature of Applicant)</label>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row text-center" style="border-bottom:1px solid black;">
                  <label>DETAILS OF ACTION ON APPLICATION</label>
               </div>
               <div class="row" style="border-bottom:1px solid black;">
                  <div class="col-xs-6" style="border-right:1px solid black;">
                     <div class="row">
                        <div class="col-xs-12">
                           <label>7. a) Certification of Leave Credits as of:</label>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <?php
                              /*if ($BegBalDate != "") {
                                 echo '<label><u>'.$BegBalDate.'</u></label>';
                              } else {
                                 echo '<label>__________________________________________</label>';      
                              }*/

                           ?>
                           <label><u>31 December 2019</u></label>
                           <br>
                           <div class="row margin-top" style="border:1px solid black;">
                              <div class="col-xs-3 text-center" style="border-right:1px solid black;">
                                 <div class="row" style="border-bottom:1px solid black;">
                                    <label><b>SPL</b></label>
                                 </div>
                                 <div class="row margin-top" style="border-bottom:1px solid black;">
                                    <label>
                                       <?php echo $SPL; ?>
                                    </label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>Day/s</label>
                                 </div>
                              </div>
                              <div class="col-xs-3 text-center" style="border-right:1px solid black;">
                                 <div class="row" style="border-bottom:1px solid black;">
                                    <label><b>Vacation</b></label>
                                 </div>
                                 <div class="row margin-top" style="border-bottom:1px solid black;">
                                    <label>
                                       <?php echo $VL; ?>
                                    </label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>Day/s</label>
                                 </div>
                              </div>
                              <div class="col-xs-3 text-center" style="border-right:1px solid black;">
                                 <div class="row" style="border-bottom:1px solid black;">
                                    <label><b>Sick</b></label>
                                 </div>
                                 <div class="row margin-top" style="border-bottom:1px solid black;">
                                    <label>
                                       <?php echo $SL; ?>
                                    </label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>Day/s</label>
                                 </div>
                              </div>
                              <div class="col-xs-3 text-center">
                                 <div class="row" style="border-bottom:1px solid black;">
                                    <label><b>Total</b></label>
                                 </div>
                                 <div class="row margin-top" style="border-bottom:1px solid black;">
                                    <label>
                                       <?php echo $total_leave; ?>
                                    </label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>Day/s</label>
                                 </div>
                              </div>
                           </div>
                           <?php spacer(15);?>
                           <div class="row text-center">
                              <?php spacer(50);?>
                              <!-- <label>__________________________________________</label> -->
                              <img class="e_signature" src="<?php echo $e_signature; ?>">
                              <input type="text" 
                                     name="personnel_officer" 
                                     id="personnel_officer" 
                                     class="form-input" 
                                     value="<?php echo $personnel_officer; ?>"
                                     disabled
                              >
                              <br>
                              <?php echo $personnel_officer_position; ?>
                              <br>
                              <label>(Personnel Officer)</label>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-6">
                     <div class="row">
                        <div class="col-xs-12">
                           <label>7. b) Recommendation</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row">
                              <input type="checkbox" id="is_approve"><label>&nbsp;Approval</label>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox"><label>&nbsp;Disapproval due to</label>
                              <br>
                              <label>__________________________________________</label>
                              <br>
                              <label>__________________________________________</label>
                              <br>
                              <label>__________________________________________</label>
                           </div>
                        </div>
                     </div>
                     <?php spacer(40);?>
                     <div class="row text-center">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <!-- <label>__________________________________________</label> -->
                           <input type="text" 
                                  name="authorized_official" 
                                  id="authorized_official" 
                                  class="form-input" 
                                  value="<?php echo $authorized_official; ?>" 
                                  disabled
                           >
                           <br>
                           <?php echo $authorized_official_position; ?>
                           <br>
                           <label>(Authorized Official)</label>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-6" style="border-right:1px solid black;">
                     <div class="row">
                        <div class="col-xs-12">
                           <label>7. c) APPROVED FOR:</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row">
                              <div class="col-xs-6">
                                 <div class="row">
                                    <label>____________________</label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>____________________</label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>&nbsp;</label>
                                 </div>
                              </div>
                              <div class="col-xs-6">
                                 <div class="row">
                                    <label>day/s with pay</label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>day/s without pay</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-6">
                     <div class="row">
                        <div class="col-xs-12">
                           <label>7. d) DISAPPROVED DUE TO:</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10 text-center">
                           <label>_____________________________________</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10 text-center">
                           <label>_____________________________________</label>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-xs-3"></div>
            <div class="col-xs-6 text-center">
               <input type="text" 
                      name="approver" 
                      id="approver" 
                      class="form-input" 
                      value="<?php echo $approver; ?>" 
                      disabled
               >
               <br>
               <?php echo $approver_position; ?>
               <br>
               <label>(Name & Signature of Authorized Official)</label>
            </div>
         </div>
      </div>
   </body>
</html>