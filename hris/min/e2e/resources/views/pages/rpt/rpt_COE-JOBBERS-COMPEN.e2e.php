<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader(getRptName(getvalue("drpReportKind")));
         ?>
          <div class="row">
            <div class="col-xs-1"></div>
            <div class="col-xs-10">
              <p style="text-indent: 30px;">
                This is to certify that the Philippine Competition Commission engaged the services of   <b>MR. FRANCIS B. FORTIN</b> as Driver from 5 June 2017 to present. As such, he receives a daily rate of PhP 780.20.
               </p>
            </div>
          </div>
          <div class="row margin-top">
            <div class="col-xs-1"></div>
            <div class="col-xs-10">
              <p style="text-indent: 30px;">
                This certification is being issued upon the request of <b>Mr. Fortin</b> for whatever legal purpose it may serve.
               </p>
            </div>
          </div>
          <div class="row margin">
            <div class="col-xs-1"></div>
            <div class="col-xs-10">
              <p>
                Issued this 16th day of November 2017, in Pasig City, Philippines.
              </p>
            </div>
          </div>
          <div class="row margin">
            <div class="col-xs-8"></div>
            <div class="col-xs-4">
              <p>
                <label>KENNETH V. TANATE</label><br>
                Director IV<br>
                Administrative Office
              </p>
            </div>
          </div>



      </div>
      <?php rptFooter(); ?>
   </body>
</html>