<?php
   include_once 'pageHEAD.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   $refid         = getvalue("refid");
   $row           = FindFirst("employeescto","WHERE RefId = '$refid'","*");
   $e_signature   = path."images/35/rjmmayrena-esign-v2.png";
   if ($row) {
      $emprefid = $row["EmployeesRefId"];
      $employees = FindFirst("employees","WHERE RefId = '$emprefid'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
      if ($employees) {
         $FirstName  = $employees["FirstName"];
         $LastName   = $employees["LastName"];
         $MiddleName = $employees["MiddleName"];
         $ExtName    = $employees["ExtName"];
         $FullName = $FirstName." ".substr($MiddleName, 0,1).". ".$LastName;
      } else {
         $FullName = "&nbsp;";
      }
      $empinformation = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
      if ($empinformation) {
         $OfficeRefId = getRecord("office",$empinformation["OfficeRefId"],"Name");
         $DivisionRefId = getRecord("division",$empinformation["DivisionRefId"],"Name");
         $PositionRefId = getRecord("position",$empinformation["PositionRefId"],"Name");
      } else {
         $OfficeRefId = $DivisionRefId = $PositionRefId = "&nbsp;";
      }
      $FiledDate = date("d F Y",strtotime($row["FiledDate"]));
      $Hours = $row["Hours"];
      $from = $row["ApplicationDateFrom"];
      $to = $row["ApplicationDateTo"];
      if ($from == $to) {
         $date = date("d F Y",strtotime($from));
      } else {
         $day_count  = count_leave($emprefid,$from,$to); 
         $diff = dateDifference($from,$to);
         $diff = $diff + 1;
         $Hours = $day_count * $Hours;
         $date = date("d F Y",strtotime($from))." to ".date("d F Y",strtotime($to));;
      }
      $status = $row["Status"];
      $Signatory1       = $row["Signatory1"];
      $Signatory2       = $row["Signatory2"];
      $Signatory3       = $row["Signatory3"];

      $s1_row           = FindFirst("signatories","WHERE RefId = '$Signatory1'","*");
      $s2_row           = FindFirst("signatories","WHERE RefId = '$Signatory2'","*");

      $s1_name          = $s1_row["Name"];
      $s1_position      = getRecord("position",$s1_row["PositionRefId"],"Name");

      $s2_name          = $s2_row["Name"];
      $s2_position      = getRecord("position",$s2_row["PositionRefId"],"Name");
   }
   $DivisionRefId = strtoupper($DivisionRefId);
   $PositionRefId = strtoupper($PositionRefId);
   $OfficeRefId = strtoupper($OfficeRefId);
?>
<!DOCTYPE html>
<html>
   <head>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style>
         .border-left-bot {
            border-left: 1px solid black;
            border-bottom: 1px solid black;
         }
         .border-left-bot-right {
            border-left: 1px solid black;
            border-bottom: 1px solid black;
            border-right: 1px solid black;
         }
         .border-top {
            border-top: 1px solid black;
         }
         .border-left {
            border-left: 1px solid black;
         }
         .border-right {
            border-right: 1px solid black;
         }
         .gray {
            background: gray;
         }
         .e_signature {
            width: 100px;
            height: 100px;
            position: absolute;
            margin-left: 0%;
            top: -30px;
            right: 40%;
         }
      </style>
      <script type="text/javascript">
         $(document).ready(function () {
            <?php 
               if ($status == "Approved") {
                  echo '$("#approved").prop("checked",true);';
               } else if ($status == "Cancelled") {
                  echo '$("#disapproved").prop("checked",true);';
               } else {
                  echo '$("#disapproved").prop("checked",false);';
                  echo '$("#approved").prop("checked",false);';
               }
            ?>
         });
      </script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader("APPLICATION FOR AVAILMENT OF COMPENSATORY TIME-OFF (CTO)");
         ?>
         <div class="row margin-top">
            <div class="col-xs-12" style="padding: 10px;">
               <div class="row">
                  <div class="col-xs-6 border-left-bot border-top">
                     Name of Employee:
                     <br>
                     <b>
                        <?php
                           echo $FullName;
                        ?>   
                     </b>
                     
                  </div>
                  <div class="col-xs-6 border-left-bot-right border-top">
                     Position:
                     <br>
                     <b>
                        <?php
                           echo $PositionRefId;
                        ?>
                     </b>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-4 border-left-bot" style="min-height: 70px;">
                     Office:
                     <br>
                     <b>
                        <?php
                           echo $OfficeRefId
                        ?>
                     </b>
                  </div>
                  <div class="col-xs-4 border-left-bot" style="min-height: 70px;">
                     Division:
                     <br>
                     <span style="font-size: 8pt;">
                        <b>
                           <?php
                              echo $DivisionRefId
                           ?>
                        </b>
                     </span>
                  </div>
                  <div class="col-xs-4 border-left-bot-right" style="min-height: 70px;">
                     Date of Filing:
                     <br>
                     <b>
                        <?php
                           echo $FiledDate
                        ?>
                     </b>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-12 gray text-center border-left border-right">
                     <b>
                        DETAILS OF APPLICATION
                     </b>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-6 border-left-bot text-center border-top">
                     Number of Hours Applied For
                     <br>
                     <b>
                        <?php
                           echo $Hours;
                        ?>
                     </b>
                  </div>
                  <div class="col-xs-6 border-left-bot-right text-center border-top">
                     Inclusive Date/s
                     <br>
                     <b>
                        <?php
                           echo $date;
                        ?>
                     </b>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-6 border-left-bot">
                     <div class="row">
                        <div class="col-xs-12">
                           Requested by:
                           <?php spacer(50); ?>
                           <br>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12 text-center">
                           <b><u><?php echo $FullName; ?></u></b>
                           <br>
                           Signature over name of applicant
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-6 border-left-bot-right">
                     <div class="row">
                        <div class="col-xs-12">
                           Recommending Approval by:
                           <?php spacer(50); ?>
                           <br>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12 text-center">
                           <b>
                              <u>
                                 <?php echo $s1_name; ?>
                              </u>
                           </b>
                           <br>
                           Immediate Supervisor
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-12 gray text-center border-left border-right">
                     <b>
                        DETAILS OF ACTION APPLICATION
                     </b>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-6 border-left-bot border-top" style="min-height: 200px;">
                     <div class="row">
                        <div class="col-xs-12">
                           CERTIFICATION OF COMPENSATORY OVERTIME <br>CREDITS (COC) as of: ________________________,________________________
                           <br>
                        </div>
                     </div>
                     <br>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           <br>
                           <br>
                           <br>
                        </div>
                     </div>
                     <br>
                     <div class="row margin-top">
                        <div class="col-xs-12 text-center">
                           <img class="e_signature" src="<?php echo $e_signature; ?>">
                           <br>
                           <u>ROSE JEAN M. MAYRENA</u>
                           <br>
                           HR Officer
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-6 border-left-bot-right border-top" style="min-height: 200px;">
                     <div class="row">
                        <div class="col-xs-12">
                           APPROVAL
                           <br>
                        </div>
                     </div>
                     <br>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-11">
                           <input type="checkbox" name="approved" id="approved" disabled>&nbsp; Approval
                           <br>
                           <input type="checkbox" name="disapproved" id="disapproved" disabled>&nbsp; Disapproval due to
                           <br>
                           __________________________________________
                        </div>
                     </div>
                     <br>
                     <div class="row margin-top">
                        <div class="col-xs-12 text-center">
                           <br>
                           <b>
                              <u>
                                 <?php echo $s2_name; ?>
                              </u>
                           </b>
                           <br>
                           Head Of Office
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row border-left-bot-right">
                  <div class="col-xs-12">
                     1. The CTO may be availed of in the blocks of four (4) and eight (8) hours.
                     <br>
                     2. The employee may use the CTO continously up to a maximum five (5) consecutive days per single availment, or on
                     staggered basis within the year.
                     <br>
                     3. The employee must first obtain approval from the head of office regarding the schedule of availment of CTO.
                     <br>
                     4. Attach approved Certificate of COC Earned (prescribed form under Joint CSC-DBM Circular No. 2, series of 2004)
                     for validation purposes.
                  </div>
               </div>
            </div>
         </div>

         
      </div>
   </body>
</html>