<?php
   require_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   require_once pathClass.'0620RptFunctions.e2e.php';
   require_once pathClass.'DTRFunction.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $whereClause .= " LIMIT 10";
   $table = "employees";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   $from = getvalue("txtAttendanceDateFrom");
   $to   = getvalue("txtAttendanceDateTo");
   $month = getvalue("txtAttendanceMonth");
   $year  = getvalue("txtAttendanceYear");
   $from    = $year."-".$month."-01";
   $to      = $year."-".$month."-".cal_days_in_month(CAL_GREGORIAN,$month,$year);
   if ($dbg) {
      echo $whereClause;
   }
   
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
      <?php
         $rsEmployees = SelectEach("employees",$whereClause);
         if ($rsEmployees) {
            while ($row = mysqli_fetch_assoc($rsEmployees)) { 
               $emprefid      = $row["RefId"];
               $LastName      = $row["LastName"];
               $FirstName     = $row["FirstName"];
               $MiddleName    = $row["MiddleName"];
               $MiddleInitial = substr($row["MiddleName"], 0,1);
               $ExtName       = $row["ExtName"];
               $FullName      = $FirstName." ".$MiddleInitial." ".$LastName." ".$ExtName;
               $where         = "WHERE EmployeesRefId = '$emprefid' AND NameCredits = 'VL'";
               $start_date    = FindFirst("employeescreditbalance",$where,"BegBalAsOfDate");
               if (!$start_date) {
                  $start_date = date("Y-m-d",time());
               }


               $where_coc     = "WHERE EmployeesRefId = '$emprefid' AND NameCredits = 'OT' AND EffectivityYear = '$year'";
               $coc_rs        = FindFirst("employeescreditbalance",$where_coc,"EffectivityYear, BeginningBalance");
               if (!$coc_rs) {
                  $coc_year      = date("Y",time());
                  $coc_balance   = 0;
               } else {
                  $coc_balance   = $coc_rs["BeginningBalance"];
                  $coc_year      = $coc_rs["EffectivityYear"];
               }

               $start_month         = date("m",strtotime($start_date." + 1 Day"));
               $end_month           = 12;
               $year                = date("Y",time());
               $arr_credit          = computeCredit($emprefid,$start_month,$end_month,$year,$year);
               $accum_balance       = intval($arr_credit["OT"]);
               $balance             = intval($coc_balance);
               $hrs_min             = convertToHoursMins($balance);
               $accum_hrs_min       = convertToHoursMins($accum_balance);
               if ($hrs_min != "") {
                  $hrs           = explode(":", $hrs_min)[0];
                  $min           = explode(":", $hrs_min)[1];
                  $hrs_val       = convertNumberToWord($hrs);
                  $min_val       = convertNumberToWord($min);   
               } else {
                  $hrs           = 0;
                  $min           = 0;
                  $hrs_val       = convertNumberToWord(0);
                  $min_val       = convertNumberToWord(0);   
               }


               if ($accum_hrs_min != "") {
                  $accum_hrs           = explode(":", $accum_hrs_min)[0];
                  $accum_min           = explode(":", $accum_hrs_min)[1];
                  $accum_hrs_val       = convertNumberToWord($accum_hrs);
                  $accum_min_val       = convertNumberToWord($accum_min);   
               } else {
                  $accum_hrs           = 0;
                  $accum_min           = 0;
                  $accum_hrs_val       = convertNumberToWord(0);
                  $accum_min_val       = convertNumberToWord(0);   
               }
               
               if (intval($accum_hrs) > 1) {
                  if (intval($accum_min) > 0) {
                     $OT            = $accum_hrs_val. " (".intval($accum_hrs).") hours and $accum_min_val (".intval($accum_min).") minutes";   
                  } else {
                     $OT            = $accum_hrs_val. " (".intval($accum_hrs).") hours";   
                  }
               } else {
                  if (intval($accum_min) > 0) {
                     $OT            = $accum_hrs_val. " (".intval($accum_hrs).") hour and $accum_min_val (".intval($accum_min).") minutes";   
                  } else {
                     $OT            = $accum_hrs_val. " (".intval($accum_hrs).") hour";   
                  }
               }
               ${"Arr_".$emprefid}     = array();
               $overtime_request = SelectEach("overtime_request","WHERE EmployeesRefId = '$emprefid' AND Status = 'Approved' AND (WithPay = '0' OR WithPay IS NULL) AND Year(StartDate) = '$year' ORDER BY StartDate");
               if ($overtime_request) {
                  $date_str      = "";
                  $dummy_date_ot = "";
                  while ($ot_row = mysqli_fetch_assoc($overtime_request)) {
                     $start      = $ot_row["StartDate"];
                     $end        = $ot_row["EndDate"];
                     $ot_month   = date("m",strtotime($start));
                     $ot_date    = $year."-".$ot_month."-01";
                     if ($dummy_date_ot == "") {
                        $dummy_date_ot = $ot_date;
                     }
                     if ($dummy_date_ot != $ot_date) {
                        $date_str = "";
                     }

                     ${"Arr_".$emprefid}[strtotime($start)] = [
                        "From"=>$start,
                        "To"=>$end,
                        "Code"=>"OT",
                        "Value"=>0
                     ];
                  }
               }
               $where_cto = "WHERE EmployeesRefId = '$emprefid'";
               $where_cto .= " AND ApplicationDateFrom <= '".date("Y-m-d",time())."'";
               $where_cto .= " AND Year(ApplicationDateFrom) = '$year'";
               $where_cto .= " AND Status = 'Approved' ORDER BY ApplicationDateFrom";
               $employees_cto = SelectEach("employeescto",$where_cto);
               if ($employees_cto) {
                  while ($cto_row = mysqli_fetch_assoc($employees_cto)) {
                     $day_rate      = "1.25";
                     $date_str      = "";
                     $start         = $cto_row["ApplicationDateFrom"];
                     $end           = $cto_row["ApplicationDateTo"];
                     $cto_month     = date("m",strtotime($start));
                     $cto_date      = $year."-".$cto_month."-01";
                     $cto           = $cto_row["Hours"];
                     $cto_diff      = dateDifference($start,$end) + 1;
                     

                     $where         = "WHERE Month(StartDate) = '".date("m",strtotime($start))."'";
                     $suspension_rs    = SelectEach("officesuspension",$where);
                     if ($suspension_rs) {
                        while ($row = mysqli_fetch_assoc($suspension_rs)) {
                           $Start      = $row["StartDate"];
                           $End        = $row["EndDate"];
                           $StartTime  = $row["StartTime"];
                           if ($start == $end) {
                              if ($start == $Start) {
                                 if ($StartTime >= 720) {
                                    $cto = $cto / 2;
                                 }   
                              }   
                           } else {
                              $diff = dateDifference($start,$end);
                              for ($i=0; $i <= $diff ; $i++) { 
                                 $temp_date = date('Y-m-d', strtotime($start . ' +'.$i.' day'));
                                 if ($temp_date == $Start) {
                                    if ($StartTime >= 720) {
                                       $cto = $cto / 2;
                                    }
                                 }
                              }
                           }
                        }
                     }
                     $cto           = $cto * $cto_diff;
                     ${"Arr_".$emprefid}[strtotime($start)] = [
                        "From"=>$start,
                        "To"=>$end,
                        "Code"=>"CTO",
                        "Value"=>$cto
                     ];
                  }
               }
               $dtr_process = SelectEach("dtr_process","WHERE EmployeesRefId = '$emprefid' AND Year = '$year'");
               if ($dtr_process) {
                  while ($dtr_row = mysqli_fetch_assoc($dtr_process)) {
                     $coc_month  = $dtr_row["Month"];
                     $coc        = $dtr_row["Total_COC_Hr"];
                     if ($coc > 2400) {
                        $coc = 2400;
                     }
                     $coc_date   = $year."-".$coc_month."-01";
                     ${"COC_Arr_".$emprefid}[$coc_date]["Earned"] = $coc;
                  }
               }

               ksort(${"Arr_".$emprefid});
               /*echo "<pre>";
               echo json_encode(${"CTO_Arr_".$emprefid},JSON_PRETTY_PRINT);
               echo "</pre>";*/

      ?>
         <div class="row" style="page-break-after: always;">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12 text-center">
                     <h3>Certificate of COC Earned</h3>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12 text-center">
                     <p>
                        This certificate entitles <b><?php echo $FullName; ?></b> to <b><?php echo $OT; ?></b> of Compensatory Overtime Credits.
                     </p>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-8"></div>
                  <div class="col-xs-4 text-center">
                     <br><br><br>
                     ANDREA S. AGCAOILI
                     <br>
                     Deparment Manager III
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     Date Issued: <?php echo date("F d,Y",time()); ?>
                     <br>
                     Valid Until: ___________________________________
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table border="2" width="100%">
                        <thead>
                           <tr>
                              <th>Date</th>
                              <th>No. of Hours Earn<br>COCs/Beginning Balance</th>
                              <th>Date of CTO</th>
                              <th>Used COCs</th>
                              <th>Remaining COCs</th>
                              <th>Remarks</th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr>
                              <td>CY <?php echo $coc_year; ?> Beginning Balance</td>
                              <td class="text-center">
                                 <?php
                                    if (intval($hrs) > 1) {
                                       echo intval($hrs)." hrs, ".intval($min)." mins";
                                    } else {
                                       echo intval($hrs)." hr, ".intval($min)." mins";   
                                    }
                                 ?>
                              </td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td>&nbsp;</td>
                           </tr>
                           <?php
                              $check_month = '';
                              $counted = 0;
                              foreach (${"Arr_".$emprefid} as $key => $arr) {
                                 $From    = $arr["From"];
                                 $To      = $arr["To"];
                                 $Code    = $arr["Code"];
                                 $Value   = $arr["Value"];
                                 $coc     = 0;
                                 //echo date("Y-m-t", strtotime($From)).' -> '.$emprefid.'  '.$From.' '.$Code.'<br>';
                                 
                                 if ($Code == "OT") {
                                    $curr_month = date("m",strtotime($From));
                                    $curr_year  = date("Y",strtotime($From));
                                    $where_dtr  = "WHERE EmployeesRefId = '$emprefid'";
                                    $where_dtr  .= " AND Month = '$curr_month'";
                                    $where_dtr  .= " AND Year = '$curr_year'";
                                    $str_balance = "";

                                    if ($check_month == '') {
                                       $check_month = $From;
                                    } else {
                                       if (date("m",strtotime($check_month)) == date("m",strtotime($From))) {
                                          $counted = 1;
                                       } else {
                                          $counted = 0;
                                       }
                                    }
                                    if ($counted == 0) {
                                       $dtr_process = FindFirst("dtr_process",$where_dtr,"Total_COC_Hr");
                                       if ($dtr_process) {
                                          $coc = $dtr_process;
                                          if ($coc >= 2400) $coc = 2400;

                                          $converted_balance   = convertToHoursMins($coc);
                                          $converted_arr       = explode(":", $converted_balance);
                                          $hrs_new_balance     = intval($converted_arr[0]);
                                          $min_new_balance     = intval($converted_arr[1]);

                                          if ($hrs_new_balance > 1) {
                                             $str_balance .= $hrs_new_balance." hrs, ";
                                          } else {
                                             $str_balance .= $hrs_new_balance." hr, ";
                                          }

                                          if ($min_new_balance > 1) {
                                             $str_balance .= $min_new_balance." mins";
                                          } else {
                                             $str_balance .= $min_new_balance." min";
                                          }
                                       }   
                                    }
                                    $check_month = $From;
                                 }
                                 if ($From == $To) {
                                    $Date = date("F d",strtotime($From));
                                 } else {
                                    $Date = date("F d",strtotime($From))." - ".date("d",strtotime($To));
                                 }
                                 echo '<tr>';
                                 if ($Code == "OT") {
                                    echo '<td>'.$Date.'</td>';
                                    echo '<td class="text-center">'.$str_balance.'</td>';
                                 } else {
                                    echo '<td>&nbsp;</td>';
                                    echo '<td class="text-center"></td>';
                                 }
                                 
                                 if ($Code == "CTO") {
                                    echo '<td>'.$Date.'</td>';
                                 } else {
                                    echo '<td>&nbsp;</td>';
                                 }
                                 if ($Code == "CTO") {
                                    echo '<td class="text-center">'.$Value.' Hrs</td>';
                                 } else {
                                    echo '<td>&nbsp;</td>';
                                 }
                                 
                                 $dummy_value = $Value * 60;
                                 $coc_balance -= $dummy_value;
                                 $coc_balance += $coc;


                                 $converted_balance   = convertToHoursMins($coc_balance);
                                 $converted_arr       = explode(":", $converted_balance);
                                 $hrs_new_balance     = intval($converted_arr[0]);
                                 $min_new_balance     = intval($converted_arr[1]);
                                 $str_balance         = "";

                                 if ($hrs_new_balance > 1) {
                                    $str_balance .= $hrs_new_balance." hrs, ";
                                 } else {
                                    $str_balance .= $hrs_new_balance." hr, ";
                                 }

                                 if ($min_new_balance > 1) {
                                    $str_balance .= $min_new_balance." mins";
                                 } else {
                                    $str_balance .= $min_new_balance." min";
                                 }


                                 echo '<td class="text-center">'.$str_balance.'</td>';
                                 echo '<td class="text-center"></td>';
                                 echo '</tr>';
                              }  
                           ?>
                        </tbody>
                     </table>
                  </div>
               </div>
               <br><br>
               <div class="row margin-top">
                  <div class="col-xs-6">
                     Approved by:
                  </div>
                  <div class="col-xs-6">
                     Claimed by:
                  </div>
               </div>
               <br><br>
               <div class="row margin-top">
                  <div class="col-xs-6 text-center">
                     ANDREA S. AGCAOILI
                     <br>
                     Deparment Manager III
                     <br>
                     _____________________
                     <br>
                     Date
                  </div>
                  <div class="col-xs-6 text-center">
                     _____________________
                     <br>
                     Administrative Officer
                     <br>
                     _____________________
                     <br>
                     Date
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <br>
                     <br>
                     <b>Note:</b>
                     <br>
                     <b>
                        <i>
                           The COCs should be used as time-off within the year these are earned until the immediately succeeding year. Thereafter, any unutilized COC's are deemed forfeited.
                           <br>
                           (Section 5.5.2 of CSC-DBM joint Circular No. 2-A, s. 2005 dated July 1, 2005)
                        </i>
                     </b>
                  </div>
               </div>
            </div>
         </div>
      <?php
            }  
         }
      ?>
      </div>
   </body>
</html>
