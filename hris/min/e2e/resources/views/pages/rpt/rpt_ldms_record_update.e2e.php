<?php
	include_once 'constant.e2e.php';
	require_once pathClass.'0620functions.e2e.php';
	$emprefid 	= getvalue("refid");
	$emp_row 	= FindFirst("employees","WHERE RefId = '$emprefid'","*");
	$LastName 	= $emp_row["LastName"];
	$FirstName 	= $emp_row["FirstName"];
	$MiddleName = $emp_row["MiddleName"];
	$ExtName 	= $emp_row["ExtName"];
	$FullName 	= $FirstName." ".$MiddleName." ".$LastName." ".$ExtName; 
	$FullName 	= strtoupper($FullName);

	$empinfo_row = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
	if ($empinfo_row) {
		$Division 		= strtoupper(getRecord("division",$empinfo_row["DivisionRefId"],"Name"));
		$Position 		= strtoupper(getRecord("Position",$empinfo_row["PositionRefId"],"Name"));
		$SalaryGrade 	= strtoupper(getRecord("SalaryGrade",$empinfo_row["SalaryGradeRefId"],"Name"));
		$SalaryAmount 	= number_format($empinfo_row["SalaryAmount"],2);
		$Office 		= strtoupper(getRecord("Office",$empinfo_row["OfficeRefId"],"Name"));
	} else {
		$Division = $Position = $SalaryGrade = $Office = "";
		$SalaryAmount = "0.00";
	}
?>
<!DOCTYPE html>
<html>
<head>
	<?php
		include_once 'pageHEAD.e2e.php';
	?>
	<title></title>
	<style type="text/css">
		thead {
			text-transform: uppercase;
		}
		th {
            text-align: center;
            vertical-align: top;
            background: #d9d9d9;
            font-size: 8pt;
        }
		td {
			padding: 2px;
			vertical-align: top;
		}
	</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-12 text-center" style="border-bottom: 2px solid black; padding: 10px;">
						<b>HR RECORD UPDATE FORM</b>
					</div>
				</div>
				<br>
				<div class="row margin-top">
					<div class="col-xs-12" style="padding: 5px;">
						<div class="row">
							<div class="col-xs-12 text-right">
								Date: ________________________
							</div>
						</div>
						<div class="row margin-top">
							<div class="col-xs-12">
								Name of Official/Employee: <?php echo $FullName; ?>
							</div>
						</div>
						<div class="row margin-top">
							<div class="col-xs-12">
								Office/Division: <?php echo $Office; ?>/<?php echo $Division; ?>
							</div>
						</div>
					</div>
				</div>
				<br>
				<div class="row margin-top">
					<div class="col-xs-12">
						<b><h4>A. EDUCATION (starting with the most recent)</h4></b>
					</div>
				</div>
				<div class="row margin-top">
					<div class="col-xs-12">
						<table border="2" width="100%">
							<thead>
                                <tr>
                                    <th style="width: 33.33%;">
                                       Cource Title/Degree
                                    </th>
                                    <th style="width: 33.33%;">
                                       Date Finished/Graduated
                                    </th>
                                    <th style="width: 33.33%;">
                                       School/Intuitions
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            	<?php
                            		for ($i=1; $i <= 3; $i++) { 
                            			echo '
                            				<tr>
			                            		<td>&nbsp;</td>
			                            		<td>&nbsp;</td>
			                            		<td>&nbsp;</td>
			                            	</tr>
                            			';
                            		}
                            	?>
                            </tbody>
						</table>
					</div>
				</div>
				<br>
				<div class="row margin-top">
					<div class="col-xs-12">
						<b><h4>B. ELIGIBILITY (starting with the most recent)</h4></b>
					</div>
				</div>
				<div class="row margin-top">
					<div class="col-xs-12">
						<table border="2" width="100%">
							<thead>
                                <tr>
                                    <th style="width: 33.33%;">
                                       Type of Eligibilty Obtained
                                    </th>
                                    <th style="width: 33.33%;">
                                       Date Obtained
                                    </th>
                                    <th style="width: 33.33%;">
                                       Issuing Office
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            	<?php
                            		for ($b=1; $b <= 3; $b++) { 
                            			echo '
                            				<tr>
			                            		<td>&nbsp;</td>
			                            		<td>&nbsp;</td>
			                            		<td>&nbsp;</td>
			                            	</tr>
                            			';
                            		}
                            	?>
                            </tbody>
						</table>
					</div>
				</div>
				<br>
				<div class="row margin-top">
					<div class="col-xs-12">
						<b><h4>C. WORK EXPERIENCE (starting with the most recent)</h4></b>
					</div>
				</div>
				<div class="row margin-top">
					<div class="col-xs-12">
						<table border="2" width="100%">
							<thead>
                                <tr>
                                    <th style="width: 20%;">
                                       Position
                                    </th>
                                    <th style="width: 20%;">
                                       Agency/Company
                                    </th>
                                    <th style="width: 20%;">
                                       Department
                                    </th>
                                    <th style="width: 20%;">
                                       Division
                                    </th>
                                    <th style="width: 20%;">
                                       Inclusive Dates
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            	<?php
                            		for ($c=1; $c <= 3; $c++) { 
                            			echo '
                            				<tr>
			                            		<td>&nbsp;</td>
			                            		<td>&nbsp;</td>
			                            		<td>&nbsp;</td>
			                            		<td>&nbsp;</td>
			                            		<td>&nbsp;</td>
			                            	</tr>
                            			';
                            		}
                            	?>
                            </tbody>
						</table>
					</div>
				</div>
				<br>
				<div class="row margin-top">
					<div class="col-xs-12">
						<b><h4>D. TRAINING (starting with the most recent)</h4></b>
					</div>
				</div>
				<div class="row margin-top">
					<div class="col-xs-12">
						<table border="2" width="100%">
							<thead>
                                <tr>
                                    <th style="width: 20%;">
                                       Program<br>Training<br>Course
                                    </th>
                                    <th style="width: 20%;">
                                       Date Attended
                                    </th>
                                    <th style="width: 20%;">
                                       No. Of Hours
                                    </th>
                                    <th style="width: 20%;">
                                       Training<br>Institutional/<br>Sponsoring<br>Institution
                                    </th>
                                    <th style="width: 20%;">
                                       Compentencies<br>Acquired
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            	<?php
                            		for ($d=1; $d <= 3; $d++) { 
                            			echo '
                            				<tr>
			                            		<td>&nbsp;</td>
			                            		<td>&nbsp;</td>
			                            		<td>&nbsp;</td>
			                            		<td>&nbsp;</td>
			                            		<td>&nbsp;</td>
			                            	</tr>
                            			';
                            		}
                            	?>
                            </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>