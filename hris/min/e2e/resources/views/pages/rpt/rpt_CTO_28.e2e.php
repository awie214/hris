<?php
   include_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   $leave_refid         = getvalue("refid");
   $spl_type            = "";
   $personnel_officer   = "";
   $authorized_official = "";
   $approver            = "";
   $personnel_officer_position   = "";
   $authorized_official_position = "";
   $approver_position            = "";
   
   $leave_row     = FindFirst("employeescto","WHERE RefId = $leave_refid","*");
   if ($leave_row) {
      $emprefid   = $leave_row["EmployeesRefId"];
      $hrs        = $leave_row["Hours"];
      $name       = "CTO (".$hrs." hrs)";
      $code       = "CTO";
      $FiledDate  = date("d F Y",strtotime($leave_row["FiledDate"]));
      $to         = date("d F Y",strtotime($leave_row["ApplicationDateTo"]));
      $from       = date("d F Y",strtotime($leave_row["ApplicationDateFrom"]));


      $Signatory1 = getRecord("signatories",$leave_row["Signatory1"],"Name");
      $Signatory2 = getRecord("signatories",$leave_row["Signatory2"],"Name");
      $Signatory3 = getRecord("signatories",$leave_row["Signatory3"],"Name");
      $Signatory1_pos = getRecord("signatories",$leave_row["Signatory1"],"PositionRefId");
      $Signatory2_pos = getRecord("signatories",$leave_row["Signatory2"],"PositionRefId");
      $Signatory3_pos = getRecord("signatories",$leave_row["Signatory3"],"PositionRefId");

      $personnel_officer   = $Signatory1;
      $authorized_official = $Signatory2;
      $approver            = $Signatory3;

      $personnel_officer_position   = getRecord("position",$Signatory1_pos,"Name");
      $authorized_official_position = getRecord("position",$Signatory2_pos,"Name");
      $approver_position            = getRecord("position",$Signatory3_pos,"Name");


      if ($to == $from) {
         $Inclusive_Date = $from;
      } else {
         $Inclusive_Date = $from." to ".$to;   
      }
      
      $Status     = $leave_row["Status"];
      $day_count  = count_leave($emprefid,$leave_row["ApplicationDateFrom"],$leave_row["ApplicationDateTo"]); 
      $emp_row       = FindFirst("employees","WHERE RefId = $emprefid","*");
      if ($emp_row) {
         $LastName      = $emp_row["LastName"];
         $FirstName     = $emp_row["FirstName"];
         $MiddleName    = $emp_row["MiddleName"];   
         $ExtName       = $emp_row["ExtName"];
         $MiddleInitial = substr($MiddleName, 0,1);
         $FullName      = $FirstName." ".$MiddleInitial.". ".$LastName;
         $cid           = $emp_row["CompanyRefId"];
      } else {
         $FullName = $ExtName = $LastName = $FirstName = $MiddleName = "&nbsp;";
      }
      
      $empinfo_row   = FindFirst("empinformation","WHERE EmployeesRefId = $emprefid","*");

      if ($empinfo_row) {
         $Office        = getRecord("office",$empinfo_row["OfficeRefId"],"Name");
         $Agency        = getRecord("agency",$empinfo_row["AgencyRefId"],"Name");
         $Position      = getRecord("position",$empinfo_row["PositionRefId"],"Name");
         $Division      = getRecord("Division",$empinfo_row["DivisionRefId"],"Name");
         $SalaryAmount  = "₱ ".number_format($empinfo_row["SalaryAmount"],2);
         
         $DivisionRefId = $empinfo_row["DivisionRefId"];
      } else {
         $Office        = "&nbsp;";
         $Agency        = "&nbsp;";
         $Position      = "&nbsp;";
         $Division      = "&nbsp;";
         $SalaryAmount  = "&nbsp;";
         $DivisionRefId = 0;
      }
      
   }
   

?>
<!DOCTYPE HTML>
<html>
   <head>
      <?php 
         $file = "Availment of Leave";
         include_once 'pageHEAD.e2e.php';
         
      ?>
      <link rel="stylesheet" href="<?php echo path("js/autocomplete/css/jquery-ui.css"); ?>" type="text/css" />
      <script type="text/javascript" src="<?php echo path("js/autocomplete/jquery-ui.js") ?>"></script>
      <style type="text/css">
         .form-input {text-align: center;}
         @media print {
            body {
               font-size: 8pt;
            }
            label {
               font-size: 9pt !important;
            }
            .form-input {
               border: none;
               padding: 0px;
               font-weight: 400;
               text-decoration: underline;
               text-align: center;
            }
         }
      </style>
      <script type="text/javascript">
         $(document).ready(function () {
            //$("[type*='checkbox']").prop("disabled",true);
            EmployeeAutoComplete("employees","personnel_officer");
            EmployeeAutoComplete("employees","authorized_official");
            EmployeeAutoComplete("employees","approver");
            $("#personnel_officer, #authorized_official, #approver").blur(function () {
               var value = $(this).val();
               arr = value.split("-");
               $(this).val(arr[1]);
            });
            <?php
               switch ($code) {
                  case 'VL':
                     if ($VLPlace != "") {
                        echo '$("#inAbroad").prop("checked",true);';
                        echo '$("#VLPlace").html("'.$VLPlace.'");';
                     } else {
                        echo '$("#inPhil").prop("checked",true);';   
                     }
                     echo '$("#VL_chk").prop("checked",true);';
                     if ($is_fl == 1) echo '$("#is_forceleave").html("<u>FORCE LEAVE</u>");';
                     $name = "";
                     break;
                  case 'SL':
                     echo '$("#SL_chk").prop("checked",true);';
                     $name = "";
                     break;
                  case 'ML':
                     echo '$("#ML_chk").prop("checked",true);';
                     $name = "";
                     break;
                  default:
                     echo '$("#OT_chk").prop("checked",true);';
                     break;
               }
               if ($Status == "Approved") {
                  echo '$("#is_approve").prop("checked",true);';
               }
               if ($spl_type != "") {
                  switch ($spl_type) {
                     case 'PM':
                        $name = "SPL - Personal Milestone";
                        break;
                     case 'PO':
                        $name = "SPL - Parental Obligation";
                        break;
                     case 'Fil':
                        $name = "SPL - Fillial";
                        break;
                     case 'DomE':
                        $name = "SPL - Domestic Emergencies";
                        break;
                     case 'PTrn':
                        $name = "SPL - Personal Transactions";
                        break;
                     case 'C':
                        $name = "SPL - Calamity";
                        break;
                  }
               }
               echo '$("#other_leave").html("'.$name.'");';
            ?>

         });
      </script>
   </head>
   <body>
      <div class="container-fluid">
         <div class="row">
            <div class="col-xs-12">
               CSC Form 6
               <br>
               Revised 1998
            </div>
         </div>
         <div class="row text-center" style="border:1px solid black; border-bottom: 1px solid white;">
            <div class="col-xs-12">
               <h3>APPLICATION FOR LEAVE</h3>
            </div>
         </div>
         <div class="row" style="border:1px solid black;margin-bottom:40px;">
            <div class="col-xs-12">
               <div class="row" style="border-bottom:1px solid black;">
                  <div class="col-xs-3" style="border-right:1px solid black;">
                     <label>1. Office/Agency</label>
                     <br>
                     <?php echo $Division; ?>
                  </div>
                  <div class="col-xs-9">
                     <div class="row">
                        <div class="col-xs-2">
                           <label>2. Name</label>
                           <br>
                           &nbsp;
                        </div>
                        <div class="col-xs-10">
                           <div class="row">
                              <div class="col-xs-4">
                                 <label>(Last)</label>
                                 <br>
                                 <b><?php echo $LastName; ?></b>
                              </div>
                              <div class="col-xs-4">
                                 <label>(First)</label>
                                 <br>
                                 <b><?php echo $FirstName." ".$ExtName; ?></b>
                              </div>
                              <div class="col-xs-4">
                                 <label>(Middle)</label>
                                 <br>
                                 <b><?php echo $MiddleName; ?></b>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row" style="border-bottom:1px solid black;">
                  <div class="col-xs-3" style="border-right:1px solid black;">
                     <label>3. Date of Filing</label>
                     <br>
                     <?php echo $FiledDate; ?>
                  </div>
                  <div class="col-xs-9">
                     <div class="row">
                        <div class="col-xs-6" style="border-right:1px solid black;">
                           <label>4. Position</label>
                           <br>
                           <?php echo $Position; ?>
                        </div>
                        <div class="col-xs-6">
                           <label>5. Salary</label>
                           <br>
                           <?php echo $SalaryAmount; ?>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row text-center" style="border-bottom:1px solid black;">
                  <label>DETAILS OF APPLICATION</label>
               </div>
               <div class="row" style="border-bottom:1px solid black;">
                  <div class="col-xs-6">
                     <div class="row">
                        <div class="col-xs-12">
                           <label>6. a) Type of Leave:</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row">
                              <input type="checkbox" id="VL_chk" disabled>&nbsp;<label>Vacation</label>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-11">
                                 <div class="row">
                                    <input type="checkbox" disabled>&nbsp;<label>To seek employment</label>
                                 </div>
                                 <div class="row margin-top">
                                    <input type="checkbox" disabled>&nbsp;<label>Others (Specify)</label>
                                    <u><input type="text" name="" id="" class="form-input" readonly></u>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row margin-top">
                              <input type="checkbox" id="SL_chk" disabled>&nbsp;<label>Sick</label>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox" id="ML_chk" disabled>&nbsp;<label>Maternity</label>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox" id="OT_chk" disabled>&nbsp;<label>Others (Specify)</label>
                              <label><u id="other_leave"></u></label>
                           </div>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           <label>6. c) # of Working Day/s Applied for:</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <label>
                              <?php
                                 echo '<u>'.$day_count.' day/s</u>';
                              ?>
                           </label>
                           <br>
                           <label>Inclusive Date/s:</label>
                           <br>
                           <label><u><?php echo $Inclusive_Date; ?></u></label>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-6" style="border-left:1px solid black;">
                     <div class="row">
                        <div class="col-xs-12">
                           <label>6. b) Where Leave will be Spent:</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row margin-top">
                              <label>(1)&nbsp;In case of Vacation Leave:</label>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox" id="inPhil">&nbsp;<label>Within the Philippines</label>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox" id="inAbroad">&nbsp;<label>Abroad (Specify)</label>
                              <br>
                              <label id="VLPlace" style="color: black;">__________________________________________</label>
                           </div>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row margin-top">
                              <label>(2)&nbsp;In case of Sick Leave:</label>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox">&nbsp;<label>In hospital (Specify)</label>
                              <br>
                              <!-- <label>__________________________________________</label> -->
                              <u><input type="text" name="" id="" class="form-input"></u>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox">&nbsp;<label>Out-Patient (Specify)</label>
                              <br>
                              <!-- <label>__________________________________________</label> -->
                              <u><input type="text" name="" id="" class="form-input"></u>
                           </div>
                        </div>
                     </div>
                     <?php spacer(6); ?>
                     <div class="row">
                        <div class="col-xs-12">
                           <label>6. d) Commutation:</label>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row">
                              <div class="col-xs-6">
                                 <input type="checkbox" disabled checked>&nbsp;<label>Requested</label>
                              </div>
                              <div class="col-xs-6">
                                 <input type="checkbox" disabled>&nbsp;<label>Not Requested</label>
                              </div>
                           </div>
                           <div class="row margin-top text-center">
                              <br><br><br>
                              <label><u><?php echo $FullName; ?></u></label>
                              <br>
                              <label>(Signature of Applicant)</label>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row text-center" style="border-bottom:1px solid black;">
                  <label>DETAILS OF ACTION ON APPLICATION</label>
               </div>
               <div class="row" style="border-bottom:1px solid black;">
                  <div class="col-xs-6" style="border-right:1px solid black;">
                     <div class="row">
                        <div class="col-xs-12">
                           <label>7. a) Certification of Leave Credits as of:</label>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <label>__________________________________________</label>
                           <br>
                           <div class="row margin-top" style="border:1px solid black;">
                              <div class="col-xs-4 text-center" style="border-right:1px solid black;">
                                 <div class="row" style="border-bottom:1px solid black;">
                                    <label>Vacation</label>
                                 </div>
                                 <div class="row margin-top" style="border-bottom:1px solid black;">
                                    <label>
                                       
                                    </label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>Day/s</label>
                                 </div>
                              </div>
                              <div class="col-xs-4 text-center" style="border-right:1px solid black;">
                                 <div class="row" style="border-bottom:1px solid black;">
                                    <label>Sick</label>
                                 </div>
                                 <div class="row margin-top" style="border-bottom:1px solid black;">
                                    <label>

                                    </label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>Day/s</label>
                                 </div>
                              </div>
                              <div class="col-xs-4 text-center">
                                 <div class="row" style="border-bottom:1px solid black;">
                                    <label>Total</label>
                                 </div>
                                 <div class="row margin-top" style="border-bottom:1px solid black;">
                                    <label>
                                       
                                    </label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>Day/s</label>
                                 </div>
                              </div>
                           </div>
                           <?php spacer(40);?>
                           <div class="row text-center">
                              <!-- <label>__________________________________________</label> -->
                              <input type="text" 
                                     name="personnel_officer" 
                                     id="personnel_officer" 
                                     class="form-input" 
                                     value="<?php echo $personnel_officer; ?>"
                                     disabled
                              >
                              <br>
                              <?php echo $personnel_officer_position; ?>
                              <br>
                              <label>(Personnel Officer)</label>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-6">
                     <div class="row">
                        <div class="col-xs-12">
                           <label>7. b) Recommendation</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row">
                              <input type="checkbox" id="is_approve"><label>&nbsp;Approval</label>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox"><label>&nbsp;Disapproval due to</label>
                              <br>
                              <label>__________________________________________</label>
                              <br>
                              <label>__________________________________________</label>
                           </div>
                        </div>
                     </div>
                     <?php spacer(40);?>
                     <div class="row text-center">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <!-- <label>__________________________________________</label> -->
                           <input type="text" 
                                  name="authorized_official" 
                                  id="authorized_official" 
                                  class="form-input" 
                                  value="<?php echo $authorized_official; ?>" 
                                  disabled
                           >
                           <br>
                           <?php echo $authorized_official_position; ?>
                           <br>
                           <label>(Authorized Official)</label>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-6" style="border-right:1px solid black;">
                     <div class="row">
                        <div class="col-xs-12">
                           <label>7. c) APPROVED FOR:</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row">
                              <div class="col-xs-8">
                                 <div class="row">
                                    <label>____________________</label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>____________________</label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>&nbsp;</label>
                                 </div>
                              </div>
                              <div class="col-xs-4">
                                 <div class="row">
                                    <label>day/s with pay</label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>day/s without pay</label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>Others (Specify)</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-6">
                     <div class="row">
                        <div class="col-xs-12">
                           <label>7. d) DISAPPROVED DUE TO:</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10 text-center">
                           <label>_____________________________________</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10 text-center">
                           <label>_____________________________________</label>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-xs-3"></div>
            <div class="col-xs-6 text-center">
               <input type="text" 
                      name="approver" 
                      id="approver" 
                      class="form-input" 
                      value="<?php echo $approver; ?>" 
                      disabled
               >
               <br>
               <?php echo $approver_position; ?>
               <br>
               <label>(Name & Signature of Authorized Official)</label>
            </div>
         </div>
      </div>
   </body>
</html>