<?php
   
?>
<!DOCTYPE html>
<html>
   <head>
      <?php
         include_once 'pageHEAD.e2e.php';
      ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style>
         /*.bottomline {border-bottom:2px solid black;}*/
         .bold {font-size:600;}
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row">
            <div class="col-xs-10">
               <div class="row">
                  <div class="col-xs-12">
                     <?php
                        rptHeader("Authority for Overtime Services");
                     ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table border="2">
                        <tr>
                           <td style="width: 12.5%;"></td>
                           <td style="width: 12.5%;"></td>
                           <td style="width: 12.5%;"></td>
                           <td style="width: 12.5%;"></td>
                           <td style="width: 12.5%;"></td>
                           <td style="width: 12.5%;"></td>
                           <td style="width: 12.5%;"></td>
                           <td style="width: 12.5%;"></td>
                        </tr>
                        <tr>
                           <td colspan="8" class="text-center">
                              <b>
                                 SUMMARY REPORT OF COMPENSATORY OVERTIME CREDITS (COC's) AND COMPENSATORY TIME-OFFS (CTO's)
                                 <br>
                                 (Main Office)
                              </b>
                           </td>
                        </tr>
                        <tr>
                           <td colspan="3">
                              <b>Name:</b>
                           </td>
                           <td colspan="3">
                              <b>Division/Office:</b>
                           </td>
                           <td colspan="2">
                              <b>For CY:</b>
                           </td>
                        </tr>
                        <tr>
                           <td class="text-center">
                              <b>Month</b>
                           </td>
                           <td class="text-center">
                              <b>COC Balance from previous Month</b>
                           </td>
                           <td class="text-center">
                              <b>COC Earned by end of the month (Maximum of 40 Hours)</b>
                           </td>
                           <td class="text-center">
                              <b>Total COC</b>
                           </td>
                           <td class="text-center">
                              <b>COC Applied by end of the month</b>
                           </td>
                           <td class="text-center">
                              <b>Inclusive Dates</b>
                           </td>
                           <td class="text-center">
                              <b>Balance* (Maximum of 120 Hours)</b>
                           </td>
                           <td class="text-center">
                              <b>Remarks</b>
                           </td>
                        </tr>
                        <?php
                           for ($i=1; $i <= 12; $i++) { 
                              echo '<tr>';
                              echo '<td class="text-center"><b>'.monthName($i,1).'</b></td>';
                              echo '<td></td>';
                              echo '<td></td>';
                              echo '<td></td>';
                              echo '<td></td>';
                              echo '<td></td>';
                              echo '<td></td>';
                              echo '<td></td>';
                              echo '</tr>';
                           }
                        ?>
                        <tr>
                           <td colspan="4">
                              <div class="row">
                                 <div class="col-xs-12">
                                    Prepared by:
                                    <br>
                                    <br>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-6"></div>
                                 <div class="col-xs-6 text-center">
                                    __________________________
                                    <br>
                                    Signature over Printed Name of Employee
                                 </div>
                              </div>
                           </td>
                           <td colspan="4">
                              <div class="row">
                                 <div class="col-xs-12">
                                    Noted by:
                                    <br>
                                    <br>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-6"></div>
                                 <div class="col-xs-6 text-center">
                                    __________________________
                                    <br>
                                    Signature over Printed Name of Approving Official
                                 </div>
                              </div>
                           </td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>