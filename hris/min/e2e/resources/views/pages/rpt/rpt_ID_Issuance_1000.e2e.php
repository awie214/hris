<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         @media print {
            body {
               font-size: 9pt;
            }
            thead {
               font-size: 9pt;  
            }
            tbody {
               font-size: 8pt !important;
            }
            .border-bottom {
               border-bottom: 1px solid black;
            }
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="col-xs-12">
            <div style="page-break-after: always;">
               <?php
                  while ($row = mysqli_fetch_assoc($rsEmployees) ) {
                     if ($p_filter_value == "0" || $p_filter_table == "") {
                        $emprefid = $row["RefId"];
                     } else {
                        $emprefid   = $row["EmployeesRefId"];
                     }
                     $emp_row = FindFirst("employees","WHERE RefId = '$emprefid'","*");
                     if ($emp_row) {
                        $FullName   = $emp_row["LastName"].", ".$emp_row["FirstName"]." ".substr($emp_row["MiddleName"], 0,1);
                        $AgencyId   = $emp_row["AgencyId"];
                        $PHIC       = $emp_row["PHIC"];
                        $TelNo      = $emp_row["TelNo"];
                        $BloodType  = getRecord("bloodtype",$emp_row["BloodTypeRefId"],"Name");
                        $PermanentHouseNo          = $emp_row["PermanentHouseNo"];
                        $PermanentStreet           = $emp_row["PermanentStreet"];
                        $PermanentSubd             = $emp_row["PermanentSubd"];
                        $PermanentBrgy             = $emp_row["PermanentBrgy"];
                        $PermanentAddCityRefId     = getRecord("city",$emp_row["PermanentAddCityRefId"],"Name");
                        $PermanentAddProvinceRefId = getRecord("province",$emp_row["PermanentAddProvinceRefId"],"Name");
                        $Address    = $PermanentHouseNo." ".$PermanentStreet." ".$PermanentBrgy." ".$PermanentAddCityRefId." ".$PermanentAddProvinceRefId;
                     } else {
                        $FullName = $AgencyId = $PHIC = $Address = $TelNo = $BloodType = "";
                     }
               ?>
               <!-- <div class="row">
                  <div class="col-xs-12 text-right">
                     <i>
                        AFD-AD-QF-34
                        <br>
                        Rev.00/05-01-2017
                     </i>
                  </div>
               </div>
               <br> -->
               <div class="row margin-top">
                  <div class="col-xs-12 text-center">
                     <b style="font-size: 12pt;">PHILIPPINE INSTITUTE FOR DEVELOPMENT STUDIES</b>
                     <br>
                     18F Three Cyberpod Centris - North Tower 
                     <br>
                     EDSA corner Quezon Avenue, Quezon City
                  </div>
               </div>
               <br>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-12 text-center" style="border-top: 1px solid black; border-bottom: 1px solid black; padding: 10px; font-size: 10pt;">
                     <b>REQUEST OF ISSUANCE OF OFFICE I.D.</b>
                  </div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-2">
                     <b>Name (Pls Print) :</b>
                  </div>
                  <div class="col-xs-4" style="border-bottom: 1px solid black;"><?php echo strtoupper($FullName); ?></div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-2">
                     <b>I.D No :</b>
                  </div>
                  <div class="col-xs-4" style="border-bottom: 1px solid black;"><?php echo $AgencyId ?></div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-2">
                     <b>Philhealth No. :</b>
                  </div>
                  <div class="col-xs-4" style="border-bottom: 1px solid black;"><?php echo $PHIC ?></div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-2">
                     <b>Home Address :</b>
                  </div>
                  <div class="col-xs-4" style="border-bottom: 1px solid black;"><?php echo $Address ?></div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-2">
                     
                  </div>
                  <div class="col-xs-4" style="border-bottom: 1px solid black;">&nbsp;</div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-2">
                     
                  </div>
                  <div class="col-xs-4" style="border-bottom: 1px solid black;">&nbsp;</div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-2">
                     <b>Telephone No. :</b>
                  </div>
                  <div class="col-xs-4" style="border-bottom: 1px solid black;"><?php echo $TelNo ?></div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-2">
                     <b>Blood Type :</b>
                  </div>
                  <div class="col-xs-4" style="border-bottom: 1px solid black;"><?php echo $BloodType ?></div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-2">
                     <b>Signature :</b>
                  </div>
                  <div class="col-xs-4" style="border-bottom: 1px solid black;">&nbsp;</div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-2">
                     <b>In case of Emergeny, please notify:</b>
                  </div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-2">
                     <b>Name (Pls. Print) :</b>
                  </div>
                  <div class="col-xs-4" style="border-bottom: 1px solid black;">&nbsp;</div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-2">
                     <b>Home Address:</b>
                  </div>
                  <div class="col-xs-4" style="border-bottom: 1px solid black;">&nbsp;</div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-2">
                     
                  </div>
                  <div class="col-xs-4" style="border-bottom: 1px solid black;">&nbsp;</div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-2">
                     
                  </div>
                  <div class="col-xs-4" style="border-bottom: 1px solid black;">&nbsp;</div>
               </div>
               <?php
                  }
               ?> 
            </div>
         </div>
      </div>
   </body>
</html>