<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   function countRating($column,$refid) {
      require "conn.e2e.php";
      $sql = "SELECT `$column` FROM training_evaluation WHERE LDMSLNDProgramRefId = '$refid'";
      $rs  = mysqli_query($conn,$sql);
      if ($rs) {
         $num_row = mysqli_num_rows($rs);
         if ($num_row > 0) {
            $total = 0;
            while ($row = mysqli_fetch_assoc($rs)) {
               $total += $row[$column];
            }
            $count = $total / $num_row;
         } else {
            $count = 0;   
         }
      } else {
         $count = 0;
      }
      return $count;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style type="text/css">
         .gray {background: gray;}
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12 text-center">
                     <b>SUMMARY OF PARTICIPANTS' FEEDBACK<br>ON THE CONDUCT OF L&D INTERVENTIONS</b>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table width="100%">
                        <thead>
                           <tr class="colHEADER">
                              <th rowspan="2">
                                 TITLE OF THE TRAINING/PROGRAM
                              </th>
                              <th rowspan="2">
                                 Date Conducted
                              </th>
                              <th colspan="11">
                                 Summary of Responses
                              </th>
                              <th rowspan="2">
                                 Remarks
                              </th>
                           </tr>
                           <tr class="colHEADER">
                              <th>Clarity of Training Objectives</th>
                              <th>Relevance of Content</th>
                              <th>Sequence Content</th>
                              <th>Usefulness of Learning Materials</th>
                              <th>Mastery of the Subject Matter</th>
                              <th>Time Management</th>
                              <th>Appropriateness of Learning Methodologies</th>
                              <th>Professional Conduct</th>
                              <th>Learning Environment</th>
                              <th>Pre-Course Coordination</th>
                              <th>Assistance to Participants</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php
                              $rs = SelectEach("ldmslndprogram"," ORDER BY StartDate DESC LIMIT 15");
                              if ($rs) {
                                 while ($row = mysqli_fetch_assoc($rs)) {
                                    $Name       = $row["Name"];
                                    $StartDate  = $row["StartDate"];
                                    $EndDate    = $row["EndDate"];
                                    echo '<tr valign="top">';
                                    echo '<td>'.$Name.'</td>';
                                    echo '<td>'.date("F d, Y",strtotime($StartDate)).' to '.date("F d, Y",strtotime($EndDate)).'</td>';
                                    echo '<td class="text-center">'.countRating("Clarity",$row["RefId"]).'</td>';
                                    echo '<td class="text-center">'.countRating("Relevance",$row["RefId"]).'</td>';
                                    echo '<td class="text-center">'.countRating("Sequence",$row["RefId"]).'</td>';
                                    echo '<td class="text-center">'.countRating("Usefulness",$row["RefId"]).'</td>';
                                    echo '<td class="text-center">'.countRating("Mastery",$row["RefId"]).'</td>';
                                    echo '<td class="text-center">'.countRating("TimeManagement",$row["RefId"]).'</td>';
                                    echo '<td class="text-center">'.countRating("Appropriateness",$row["RefId"]).'</td>';
                                    echo '<td class="text-center">'.countRating("Professional",$row["RefId"]).'</td>';
                                    echo '<td class="text-center">'.countRating("Learning",$row["RefId"]).'</td>';
                                    echo '<td class="text-center">'.countRating("Coordination",$row["RefId"]).'</td>';
                                    echo '<td class="text-center">'.countRating("Assistance",$row["RefId"]).'</td>';
                                    echo '<td></td>';
                                    echo '</tr>';
                                 }
                              }
                           ?>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>
