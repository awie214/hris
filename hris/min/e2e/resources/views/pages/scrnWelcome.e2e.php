<?php
   include 'conn.e2e.php';

   if (getvalue("hCompanyID") == "2") {
      $disabled = "disabled";
   } else {
      $disabled = "";
   }
   $disabled = "";
   $cid        = getvalue("hCompanyID");
   $EmpRefId   = getvalue("hEmpRefId");
?>
<!DOCTYPE>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script type="text/javascript" src="<?php echo $_SESSION["login"] ?>"></script>
      <script type="text/javascript" src="<?php echo $path; ?>js/jsSHAver2/src/sha.js"></script>
      <link rel="stylesheet" href="<?php echo $path."/css/sideBar.css"; ?>">
      <style type="text/css">
         #PIS_Header, #AMS_Header, #PMS_Header, #myrequest, #SPMS_Header {cursor: pointer;}
      </style>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_Dashboard"); ?>"></script>
      <style type="text/css">
         .request {
            cursor: pointer;
         }
         .request:hover {
            text-decoration: underline;
         }
         .govt_logo {
            width: 15%;
            margin-right:10px;
            padding: 20px;
            border-radius: 5px;
         }
         .govt_logo:hover {
            box-shadow: 3px 3px 3px 3px gray;
            transition: 0.5s;
            background: #dcdee2;
            cursor: pointer;
         }
         a {
            text-decoration: none;
         }

      </style>
      <script type="text/javascript">
         $(document).ready(function () {
            <?php
               if ($cid == "21") echo '$("#spms_canvas").hide();';
            ?>
         });
      </script>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <nav class="navbar navbar-fixed-top">
            <div class="sysNameHolder sysBG" style="border-bottom:3px solid #fff;">
               <?php
                  $TRNBTN = 0;
                  $title = "";
                  $Logout = true;
                  include $files["inc"]["hdr"];
               ?>
               <span class="sysName">
                  <?php
                     echo $settings["Title"];
                  ?>
               </span>
            </div>
         </nav>
         <div style="margin-top:60px;">
            <?php
               if (!$isUser) {
                  doSideBarMain();
            ?>
                  <div class="container-fluid" id="mainScreen">
                     <?php doTitleBar("DASHBOARD / REMINDERS"); ?>
                     <div class="row">
                        <div class="col-xs-6">
                           <?php
                              $newly_hired      = 0;
                              $emp_count        = 0;
                              $emp_bday         = 0;
                              $three            = 0;
                              $fivebelow        = 0;
                              $six_ten          = 0;
                              $eleven_twenty    = 0;
                              $twentyone_thirty = 0;
                              $thirtyup         = 0;
                              $curr_date        = date("Y",time());
                              $audit_trail      = 0;

                              $audit_trail_rs = SelectEach("updates201","WHERE TrnDate >= '".date("Y-m-d",time())."' and TrnDate <= '".date("Y-m-d",time())."'");
                              if ($audit_trail_rs) {
                                 $audit_trail = mysqli_num_rows($audit_trail_rs);
                              }

                              $emp_count = SelectEach("employees","WHERE (Inactive != 1 OR Inactive IS NULL)");
                              if (mysqli_num_rows($emp_count) > 0) {
                                 $emp_count = mysqli_num_rows($emp_count);
                              }
                              //----------------------------------------------------
                              //----------------------------------------------------
                              //----------------------------------------------------
                              $emp_bday_where = "WHERE MONTH(BirthDate) = '".date("m",time())."'";
                              $emp_bday_where .= " AND (Inactive != 1 OR Inactive IS NULL)";
                              $rs = SelectEach("employees",$emp_bday_where);
                              if ($rs) {
                                 $emp_bday = mysqli_num_rows($rs);
                              } else {
                                 $emp_bday = 0;
                              }
                              //----------------------------------------------------
                              //----------------------------------------------------
                              //----------------------------------------------------

                              $past_month = date("Y",time());
                              $newly_hired_rs = SelectEach("empinformation","INNER JOIN employees WHERE Year(empinformation.HiredDate) = '$past_month' AND employees.RefId = empinformation.EmployeesRefId");
                              if ($newly_hired_rs) {
                                 while ($newly_hired_row = mysqli_fetch_assoc($newly_hired_rs)) {
                                    $newly_hired++;
                                 }
                              }
                              //----------------------------------------------------
                              //----------------------------------------------------
                              //----------------------------------------------------

                              $six_ten_where          = "WHERE YEAR(HiredDate) >= '2008' AND YEAR(HiredDate) < 2013";
                              $eleven_twenty_where    = "WHERE YEAR(HiredDate) >= '1998' AND YEAR(HiredDate) < 2008";
                              $twentyone_thirty_where = "WHERE YEAR(HiredDate) >= '1988' AND YEAR(HiredDate) < 1998";
                              $thirtyup_where         = "WHERE YEAR(HiredDate) <= '1987'";
                              $fivebelow_where        = "WHERE YEAR(HiredDate) >= '2013'";
                              $three_where        = "WHERE YEAR(HiredDate) == '2016'";

                              $years_rs = SelectEach("empinformation","WHERE HiredDate != ''");
                              if ($years_rs) {
                                 while ($year_row = mysqli_fetch_assoc($years_rs)) {
                                    $hired = date("Y",strtotime($year_row["HiredDate"]));
                                    $diff = $curr_date - $hired;
                                    $diff = $diff + 1;
                                    if ($diff == 3) {
                                       $three++;
                                    } else if ($diff <= 5) {
                                       $fivebelow++;
                                    } else if ($diff >= 6 && $diff <= 10) {
                                       $six_ten++;
                                    } else if ($diff >= 11 && $diff <= 20) {
                                       $eleven_twenty++;
                                    } else if ($diff >= 21 && $diff <= 30) {
                                       $twentyone_thirty++;
                                    } else if ($diff >= 30) {
                                       $thirtyup++;
                                    }
                                 }
                              }
                           ?>

                           <?php spacer(5) ?>
                           <div class="panel-group">
                              <div class="panel panel-default">
                                 <div class="panel-heading">TOTAL NUMBER OF EMPLOYMENT</div>
                                 <div class="panel-body">
                                    <ul class="list-group">
                                       <li class="list-group-item counts--" onclick="showEmp('Employees','Name',0);">
                                          TOTAL ACTIVE EMPLOYEES
                                          <span class="badge"><?php echo $emp_count; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" onclick="clickRpt('rpt_Newly_Hired','');">
                                          NEWLY HIRED
                                          <span class="badge"><?php echo $newly_hired; ?></span>
                                       </li>
                                       
                                    </ul>
                                 </div>
                              </div>
                              <div class="panel panel-default">
                                 <div class="panel-heading ">NOTIFICATION FOR</div>
                                 <div class="panel-body">
                                    <ul class="list-group">
                                       <li class="list-group-item counts--" onclick="showEmp('BirthDate','With Birthdays this month',0);">
                                          BIRTHDAY CELEBRANTS FOR THE MONTH
                                          <span class="badge"><?php echo $emp_bday; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" onclick="clickRpt('rptAuditTrails&txtTrnFrom=<?php echo date("Y-m-d",time())?>&txtTrnTo=<?php echo date("Y-m-d",time())?>','');">
                                          EMPLOYEES WITH PDS UPDATES
                                          <span class="badge"><?php echo $audit_trail; ?></span>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="panel panel-default">
                                 <div class="panel-heading ">LOYALTY</div>
                                 <div class="panel-body">
                                    <ul class="list-group">
                                       <!-- <li class="list-group-item counts--" 
                                           onclick="clickRpt('rpt_Loyalty','WHERE YEAR(HiredDate) = \'2016\'');"
                                       >
                                          3 YEARS
                                          <span class="badge"><?php echo $three; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" 
                                           onclick="clickRpt('rpt_Loyalty','WHERE YEAR(HiredDate) >= \'$fivebelow_date\'');"
                                       >
                                          5 YEARS BELOW
                                          <span class="badge"><?php echo $fivebelow; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" 
                                           onclick="clickRpt('rpt_Loyalty','WHERE YEAR(HiredDate) >= \'1998\' AND YEAR(HiredDate) < \'2008\'');"
                                       >
                                          6 - 10 YEARS
                                          <span class="badge"><?php echo $six_ten; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" 
                                           onclick="clickRpt('rpt_Loyalty','WHERE YEAR(HiredDate) >= \'1988\' AND YEAR(HiredDate) < \'1998\'');"
                                       >
                                          11 - 20 YEARS
                                          <span class="badge"><?php echo $eleven_twenty; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" 
                                           onclick="clickRpt('rpt_Loyalty','WHERE YEAR(HiredDate) >= \'2008\' AND YEAR(HiredDate) < \'2013\'');"
                                       >
                                          21 - 30 YEARS
                                          <span class="badge"><?php echo $twentyone_thirty; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" 
                                           onclick="clickRpt('rpt_Loyalty','WHERE YEAR(HiredDate) <= \'1998\'');"
                                       >
                                          30 YEARS UP
                                          <span class="badge"><?php echo $thirtyup; ?></span>
                                       </li> -->
                                       <li class="list-group-item counts--" 
                                           onclick="clickRpt('rpt_LoyaltyList','');"
                                       >
                                          Loyalty Report
                                          
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-6 padd5">
                           <div class="panel panel-default">
                              <div class="panel-heading ">PENDING APPROVALS:</div>
                              <div class="panel-body">
                                 <ul class="list-group">
                                    <li class="list-group-item counts--" onclick="gotoscrn('amsAppvLeaveAvail','');">
                                       Leave Applications:
                                       <?php 
                                          $leave_count = SelectEach("employeesleave","WHERE Status IS NULL");
                                          if ($leave_count) {
                                             $leave_count = mysqli_num_rows($leave_count);
                                          } else {
                                             $leave_count = 0;
                                          }
                                       ?>
                                       <span class="badge"><?php echo $leave_count; ?></span>
                                    </li>
                                    <li class="list-group-item counts--" onclick="gotoscrn('amsAppvOfficeAuthority','');">
                                       Office Authority Applications:
                                       <?php 
                                          $authority_count = SelectEach("employeesauthority","WHERE Status IS NULL");
                                          if ($authority_count) {
                                             $authority_count = mysqli_num_rows($authority_count);
                                          } else {
                                             $authority_count = 0;
                                          }
                                       ?>
                                       <span class="badge"><?php echo $authority_count; ?></span>
                                    </li>
                                    <li class="list-group-item counts--" onclick="gotoscrn('amsAppvCTOAvail','');">
                                       CTO Applications:
                                       <?php 
                                          $CTO_count = SelectEach("employeescto","WHERE Status IS NULL");
                                          if ($CTO_count) {
                                             $CTO_count = mysqli_num_rows($CTO_count);
                                          } else {
                                             $CTO_count = 0;
                                          }
                                       ?>
                                       <span class="badge"><?php echo $CTO_count; ?></span>
                                    </li>
                                    <li class="list-group-item counts--" onclick="gotoscrn('amsAppvMonetization','');">
                                       Leave Monetization Applications:
                                       <?php 
                                          $monetization_count = SelectEach("employeesleavemonetization","WHERE Status IS NULL");
                                          if ($monetization_count) {
                                             $monetization_count = mysqli_num_rows($monetization_count);
                                          } else {
                                             $monetization_count = 0;
                                          }
                                       ?>
                                       <span class="badge"><?php echo $monetization_count; ?></span>
                                    </li>
                                    <li class="list-group-item counts--" onclick="gotoscrn('amsAppvAttendanceRegistration','');">
                                       Attendance Registration Request:
                                       <?php 
                                          $attendancereq_count = SelectEach("attendance_request","WHERE Status IS NULL");
                                          if ($attendancereq_count) {
                                             $attendancereq_count = mysqli_num_rows($attendancereq_count);
                                          } else {
                                             $attendancereq_count = 0;
                                          }
                                       ?>
                                       <span class="badge"><?php echo $attendancereq_count; ?></span>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- Modal -->
                  <div class="modal fade border0" id="prnModal" role="dialog">
                     <div class="modal-dialog border0" style="padding:0px;width:97%;height:92%;">

                        <div class="mypanel border0" style="height:100%;">
                           <div class="panel-top bgSilver">
                              <a href="#" data-toggle="tooltip" data-placement="top" title="Print Now" id="btnPRINTNOW">
                                 <i class="fa fa-print" aria-hidden="true"></i>
                              </a>
                              <label class="close" data-dismiss="modal">&times;</button>
                           </div>
                           <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                        </div>
                     </div>
                  </div>
            <?php

               } else {
                  /*
                     USER SIDE SCRN WELCOME
                  */
                  //spacer(25);
            ?>
                  <!-- <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <ul class="nav navbar-nav">
                           <li>
                              <a href="#" data-toggle="dropdown">
                                 Personal Information
                                 <span class="caret"></span>
                                 <ul class="dropdown-menu">
                                    <li><a href="screen_sections.php">Sections</a></li>
                                 </ul>
                              </a>
                           </li>
                           <li><a href="#">Personal Information</a></li>
                           <li><a href="#">Attendance Management</a></li>
                           <li><a href="#">Payroll Management</a></li>
                           <li><a href="#">Strategic Performance</a></li>
                           <li><a href="#">Learning And Development</a></li>
                        </ul>
                     </div>
                  </nav> -->
                  
                  <?php
                     include 'user_side_dashboard.e2e.php';
                     if (getvalue("hCompanyID") == "35") {
                  ?>
                  <div class="row margin-top" style="margin-top: 100px;">
                     <div class="col-xs-12">
                        <div class="container">
                           <div class="row">
                              <div class="col-xs-12">
                                 <img src="<?php echo $path; ?>modules/govt_logo/birlogo.png" class="govt_logo" id="bir">
                                 <img src="<?php echo $path; ?>modules/govt_logo/gsislogo.jpg" class="govt_logo" id="gsis">
                                 <img src="<?php echo $path; ?>modules/govt_logo/pagibiglogo.png" class="govt_logo" id="pagibig">
                                 <img src="<?php echo $path; ?>modules/govt_logo/phiclogo.png" class="govt_logo" id="phic">
                                 <img src="<?php echo $path; ?>modules/govt_logo/ssslogo.png" class="govt_logo" id="sss">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php
                     }
                  ?>
                  <!-- Modal -->
                  <div class="modal fade" id="prnModal" role="dialog">
                     <div class="modal-dialog" style="height:90%;width:80%">
                        <div class="mypanel" style="height:100%;">
                           <div class="panel-top bgSilver">
                              <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                                 <i class="fa fa-print" aria-hidden="true"></i>
                              </a>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                           </div>
                           <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                        </div>
                     </div>
                  </div>
                  
            <?php
               }
               footer();
               include "varHidden.e2e.php";
               doHidden("hRptFile","DashboardRpt","");
            ?>
         </div>
      </form>
      
   </body>
</html>


