<?php
   require_once "constant.e2e.php";
   require_once "inc_model.e2e.php";
   require_once "incUtilitiesJS.e2e.php";
?>
   <div class="container">
      <div class="row" id="EntryScrn">
         <div class="col-xs-1"></div>
         <div class="col-xs-10">
            <?php if ($ScrnMode != 1) { ?>
               <div class="row">
                  <ul class="nav nav-pills">
                     <li class="active" style="font-size:12pt;font-weight:600;">
                        <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                        <?php echo $refid; ?>
                        </span></a>
                     </li>
                  </ul>
               </div>
            <?php } ?>
            <div class="row">
               <div class="col-xs-6">
                  <!--<div class="row margin-top10">
                     <div class="form-group">
                        <label  for="inputs">CODE:</label><br>
                        <input class="form-input saveFields-- uCase-- mandatory" type="text" placeholder="Code" <?php //if ($ScrnMode == 2) { echo "disabled"; } else { echo $disabled; }  ?>
                           id="inputs" name="char_Code" style='width:100%' autofocus>
                     </div>
                  </div>
                  <div class="row">
                     <div class="form-group">
                        <label  for="inputs">NAME:</label><br>
                        <input class="form-input saveFields-- mandatory"
                               type="text" placeholder="name"
                               id="inputs" name="char_Name"
                               style='width:100%'>
                     </div>
                  </div>-->
                  <div class="row">
                     <div class="col-xs-6">
                        <div class="row">
                           <div class="form-group">
                              <label  for="inputs">Status:</label><br>
                              <select class="form-input saveFields-- mandatory" name="char_Status">
                                 <option value="a">1</option>
                                 <option value="b">2</option>
                              </select>
                           </div>
                        </div>
                        <div class="row">
                           <div class="form-group">
                              <label  for="inputs">Exemption:</label><br>
                              <input class="form-input saveFields-- number-- mandatory"
                                     type="text" placeholder=""
                                     id="inputs" name="deci_Exemption"
                                     style='width:100%'>
                           </div>
                        </div>
                        <div class="row">
                           <div class="form-group">
                              <label  for="inputs">1:</label><br>
                              <input class="form-input saveFields-- number-- mandatory"
                                     type="text" placeholder=""
                                     id="inputs" name="sint_SalaryBracketLevel1"
                                     style='width:100%'>
                           </div>
                        </div>
                        <div class="row">
                           <div class="form-group">
                              <label  for="inputs">2:</label><br>
                              <input class="form-input saveFields-- number-- mandatory"
                                     type="text" placeholder=""
                                     id="inputs" name="sint_SalaryBracketLevel2"
                                     style='width:100%'>
                           </div>
                        </div>
                        <div class="row">
                           <div class="form-group">
                              <label  for="inputs">3:</label><br>
                              <input class="form-input saveFields-- number-- mandatory"
                                     type="text" placeholder=""
                                     id="inputs" name="sint_SalaryBracketLevel3"
                                     style='width:100%'>
                           </div>
                        </div>
                     </div>
                     <div class="col-xs-5" style="margin-left:10px;">
                        <div class="row">
                           <div class="form-group">
                              <label  for="inputs">4:</label><br>
                              <input class="form-input saveFields-- number-- mandatory"
                                     type="text" placeholder=""
                                     id="inputs" name="sint_SalaryBracketLevel4"
                                     style='width:100%'>
                           </div>
                        </div>
                        <div class="row">
                           <div class="form-group">
                              <label  for="inputs">5:</label><br>
                              <input class="form-input saveFields-- number-- mandatory"
                                     type="text" placeholder=""
                                     id="inputs" name="sint_SalaryBracketLevel5"
                                     style='width:100%'>
                           </div>
                        </div>
                        <div class="row">
                           <div class="form-group">
                              <label  for="inputs">6:</label><br>
                              <input class="form-input saveFields-- number-- mandatory"
                                     type="text" placeholder=""
                                     id="inputs" name="sint_SalaryBracketLevel6"
                                     style='width:100%'>
                           </div>
                        </div>
                        <div class="row">
                           <div class="form-group">
                              <label  for="inputs">7:</label><br>
                              <input class="form-input saveFields-- number-- mandatory"
                                     type="text" placeholder=""
                                     id="inputs" name="sint_SalaryBracketLevel7"
                                     style='width:100%'>
                           </div>
                        </div>
                        <div class="row">
                           <div class="form-group">
                              <label  for="inputs">8:</label><br>
                              <input class="form-input saveFields-- number-- mandatory"
                                     type="text" placeholder=""
                                     id="inputs" name="sint_SalaryBracketLevel8"
                                     style='width:100%'>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="form-group">
                        <label for="inputs">REMARKS:</label>
                        <textarea class="form-input saveFields--" rows="5" name="char_Remarks" <?php echo $disabled; ?>
                        placeholder="remarks"><?php echo $remarks; ?></textarea>
                     </div>
                  </div>
               </div>

            </div>
         </div>
         <div class="col-xs-1"></div>
      </div>
   </div>