<?php
   function insertIconBenefits() {
      echo
      '<a href="javascript:void(0);">
         <i class="fa fa-plus-square" aria-hidden="true" id="benefitInsert" title="INSERT NEW" style="color:white;"></i>
      </a>';
   }
   function dobtnBenefits() {
      echo
      '<hr>
      <div class="row">
         <div class="col-xs-12 txt-center">';
               createButton("Save","btnLocSaveItem","btn-cls4-sea","fa-floppy-o","");
               createButton("Cancel","btnLocCancelItem","btn-cls4-red","fa-undo","");
         echo
         '</div>
      </div>';
   }

?>


<script>
   $(document).ready(function () {
      $("#benefitInsert").click(function () {
         $("#hmode").val("ADD");
         $(".saveFields--").val("");
         $("#benefitTableSet").modal();
      });
   });
</script>
<div class="mypanel">
   <div class="row margin-top" id="newBenefits">
      <div class="col-xs-12">
         <div class="panel-top">
            <?php insertIconBenefits(); ?> <label>INSERT NEW BENEFITS</label>
         </div> 
         <div class="panel-mid">
            <?php
               $table = "employees";
               $tableHdr = ["Effectivity Date","Allowance/Benefits","Description","Pay Period","Tax Type","Amount"];
               $tableFld = ["","","","","","",];
               $sql = "SELECT * FROM `$table` WHERE RefId = 0 ORDER BY RefId Desc LIMIT 100";
               $action = ["true","true","true"];
               doGridTable($table,
                           $tableHdr,
                           $tableFld,
                           $sql,
                           $action,
                           "Benefits");
            ?>
         </div>
         <div class="panel-bottom"></div>
      </div>
   </div>
</div>
<!--modal-->
<div class="modal fade modalFieldEntry--" id="benefitTableSet" role="dialog">
   <div class="modal-dialog" style="width:75%;">
      <div class="mypanel" style="height:100%;">
         <div class="panel-top bgSea">
            <span id="modalTitle" style="font-size:11pt;">NEW BENEFITS AND ALLOWANCE INFORMATION</span>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="panel-mid">
            <div class="row" style="padding:10px;">
               <div class="col-xs-12">
                  <div class="row">
                     <div class="col-xs-3 label">
                        <label>Effectivity Date:</label>
                     </div>
                     <div class="col-xs-3">
                        <input type="text" name="Effectivity" class="form-input date-- saveFields--">
                     </div>
                     <div class="col-xs-3 label">
                        <input type="checkbox" name="chkWithEndDate">&nbsp;<label>With End Date</label>
                     </div>
                     <div class="col-xs-3">
                        <input type="text" name="WithEndDate" class="form-input date-- saveFields--">
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-3 label">
                        <label>Allowance:</label>
                     </div>
                     <div class="col-xs-9">
                        <input type="text" name="Allowance" class="form-input saveFields--">
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-3 label">
                        <label>Description:</label>
                     </div>
                     <div class="col-xs-9">
                        <input type="text" name="Description" class="form-input saveFields--">
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-3 label">
                        <label>Pay Period:</label>
                     </div>
                     <div class="col-xs-3">
                        <select class="form-input" name="BAPayPeriod">
                           <option></option>
                           <option>Both</option>
                           <option>Monthly</option>
                           <option>Weekly</option>
                           <option>First Half</option>
                           <option>Second Half</option>
                        </select>
                     </div>
                     <div class="col-xs-3 label">
                        <label>Amount:</label>
                     </div>
                     <div class="col-xs-3">
                        <input type="text" name="Amount" class="form-input number-- saveFields--">
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-3 label">
                        <label>Pay Rate:</label>
                     </div>
                     <div class="col-xs-3">
                        <select class="form-input" name="BAPayRate">
                           <option></option>
                           <option>Both</option>
                           <option>Monthly</option>
                           <option>Weekly</option>
                           <option>First Half</option>
                           <option>Second Half</option>
                        </select>
                     </div>
                     <div class="col-xs-3 label">
                        <label>Tax Type:</label>
                     </div>
                     <div class="col-xs-3">
                        <select class="form-input" name="taxType">
                           <option></option>
                           <option>Taxable</option>
                           <option>Non-Taxable</option>
                        </select>
                     </div>
                  </div>
                  <?php dobtnBenefits(); ?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>




