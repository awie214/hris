<?php
   error_reporting(E_ALL);
   ini_set('display_errors', 1);
   session_start();
   include_once "conn.e2e.php";
   include_once "constant.e2e.php";
   include_once pathClass.'0620functions.e2e.php';
   include_once pathClass.'SysFunctions.e2e.php';
   include_once pathClass.'0620TrnData.e2e.php';

   $moduleContent = file_get_contents(json.'amsEmployee.json');
   $module        = json_decode($moduleContent, true); 
   $trn = new Transaction();
   //echo json.'pmsEmployee.json';
   //$user = $_POST["hUser"];

   $CompanyID = getvalue("hCompanyID");
   $BranchID = getvalue("hBranchID");
   $UserRefId = getvalue("hUserRefId");
   $WHERE = "WHERE CompanyRefId = ".$CompanyID." AND BranchRefId = ".$BranchID;

   function fnupdateAMSEmployee() {
      $conn       = $GLOBALS["conn"];
      $module     = $GLOBALS["module"];
      $emprefid   = getvalue("hEmpRefId");
      $elem1      = "TabName";
      $elem2      = "AllTabs";
      $tab        = $module[$elem1][$elem2]["dbField"];
      $table      = $module[$elem1][$elem2]["dbTable"];
      $fldnval    = "";
      $fld        = "";
      $val        = "";
      $js         = "";
      if (intval($emprefid) > 0) {
         $empinfo_refid = FindFirst("empinformation","WHERE EmployeesRefId = '".$emprefid."'","RefId");
         if ($empinfo_refid) {
            if ($emprefid > 0) {
               foreach ($tab as $key => $value) {
                  $fieldname = $value[0];
                  $objvalue = getvalue($value[1]);
                  if ($value[3] == "int" || $value[3] == "deci") {
                     $objvalue = setValue_RefId($objvalue);
                  }
                  if ($value[3] == "date") {
                     if ($objvalue != "") {
                        $fldnval .= "`$fieldname` = '".$objvalue."', ";
                        $fld .= "`$fieldname`, ";
                        $val .= "'$objvalue', ";
                     }
                  } else {
                     $fldnval .= "`$fieldname` = '".$objvalue."', ";
                     $fld .= "`$fieldname`, ";
                     $val .= "'$objvalue', ";   
                  }
               }
               $AMSRefId = FindFirst($table,"WHERE EmployeesRefId = ".$emprefid,"RefId");
               if ($AMSRefId > 0) {
                  $result = f_SaveRecord("EDITSAVE",$table,$fldnval,$AMSRefId);
                  if ($result != "") {
                     echo $result;
                     return false;
                  } else {
                     $js .= 'alert("Record Successfully Updated");';
                  }
               } else {
                  $result = f_SaveRecord("NEWSAVE",$table,$fld,$val);
                  if (!is_numeric($result)) {
                     echo $result;
                     return false;
                  } else {
                     $js .= 'alert("Record Successfully Saved");';  
                  }
               }
               $emp_fldnval         = "BiometricsID = '".getvalue("char_BiometricsID")."', ";
               $emp_result          = f_SaveRecord("EDITSAVE","employees",$emp_fldnval,$emprefid);
               if ($emp_result != "") {
                  echo $emp_result;
                  return false;
               }

               $empinfo_fldnval     = "WorkScheduleRefId = '".getvalue("sint_WorkScheduleRefId")."', ";
               $empinfo_result      = f_SaveRecord("EDITSAVE","empinformation",$empinfo_fldnval,$empinfo_refid);
               if ($empinfo_result != "") {
                  echo $empinfo_result;
                  return false;
               }
               $js .= '
                  $("#updateRow").hide();
                  $("[class*=\'saveFields--\']").attr("disabled",true);
                  $("#chkAttRef").prop("checked",false);
                  ';
               echo $js;
            }   
         } else {
            echo '$.notify("Employee Has No Employement Info");';
         }
      } else {
         echo '$.notify("No Employee Selected");';
      }
   }
   function fngetEmpRecord() {
      $conn = $GLOBALS["conn"];
      $module = $GLOBALS["module"];
      $empRefid = getvalue("emprefid");
      $elem1 = "TabName";
      $elem2 = "AllTabs";
      $tab = $module[$elem1][$elem2]["dbField"];
      foreach ($tab as $key => $value) {
         $fieldname = $value[0];
         $objName = $value[1];
         echo 'setValueByName("'.$objName.'","");'."\n";
      }
      $EmpRefId = getvalue("EmpRefId");
      if ($EmpRefId) {
         $LastName = mysqli_real_escape_string($conn,getRecord("employees",$EmpRefId,"LastName"));
         $FirstName = mysqli_real_escape_string($conn,getRecord("employees",$EmpRefId,"FirstName"));
         $MiddleName = mysqli_real_escape_string($conn,getRecord("employees",$EmpRefId,"MiddleName"));
         $ExtName = mysqli_real_escape_string($conn,getRecord("employees",$EmpRefId,"ExtName"));
         $BirthDate = mysqli_real_escape_string($conn,getRecord("employees",$EmpRefId,"BirthDate"));
         echo '
            setValueByName("RefId","'.$EmpRefId.'");
            setValueByName("char_LastName","'.$LastName.'");
            setValueByName("char_FirstName","'.$FirstName.'");
            setValueByName("char_MiddleName","'.$MiddleName.'");
            setValueByName("char_ExtName","'.$ExtName.'");
            setValueByName("date_BirthDate","'.$BirthDate.'");
         '; 
         $AMSRow = FindFirst("ams_employees","WHERE EmployeesRefId = ".$EmpRefId,"*");
         if ($AMSRow) {
            foreach ($tab as $key => $value) {
               $fieldname = $value[0];
               $objName = $value[1];
               echo 'setValueByName("'.$objName.'","'.mysqli_real_escape_string($conn,$AMSRow[$fieldname]).'");'."\n";
            }
         } else {
            $Employees = FindFirst("employees","WHERE RefId = $EmpRefId","*");
            $where = "WHERE CompanyRefId = ".getvalue("hCompanyID")." AND BranchRefId = ".getvalue("hBranchID")." AND EmployeesRefId = $EmpRefId";
            if ($Employees) {
               $EmpInfo = FindFirst("empinformation",$where,"*");
               if ($EmpInfo) {
                  $data = array_merge($Employees,$EmpInfo);
                  foreach ($tab as $key => $value) {
                     $fieldname = $value[0];
                     $objName = $value[1];
                     if (isset($data[$fieldname])) {
                        echo 'setValueByName("'.$objName.'","'.mysqli_real_escape_string($conn,$data[$fieldname]).'");'."\n";   
                     }
                  }
               }
            }
         }
      }
   }
   
   
   /*DONT MODIFY HERE*/
   $funcname = "fn".getvalue("fn");
   $params   = getvalue("params");
   if (!empty($funcname)) {
      $funcname($params);
   } else {
      echo 'alert("Error... No Function defined");';
   }
?>