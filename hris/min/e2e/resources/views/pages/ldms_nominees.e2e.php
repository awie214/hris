<?php 
   $module = module("AssignedCourse"); 
   $table = "assigned_courses";
   $sizeCol = "col-xs-6";
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript">
         $(document).ready(function () {
            remIconDL();
            $("#LocSAVE").click(function () {
               if (saveProceed() == 0) {
                  $(this).attr("type","submit"); 
               }
            });
            <?php
               if (isset($_GET["errmsg"])) {
                  echo '$.notify("'.$_GET["errmsg"].'");';
               }
            ?>
         });
         
         function afterDelete() {
            alert("Successfully Deleted");
            gotoscrn($("#hProg").val(),"");
         }
      </script>
   </head>
   <body>
      <form name="xForm" method="post" action="postMultiple.e2e.php">
         <?php $sys->SysHdr($sys,"ldms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("Nominees"); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                        <div id="divList">
                           <div class="mypanel">
                              <div class="panel-top">List of Nominees</div>
                              <div class="panel-mid">
                                 <span id="spGridTable">
                                    <table class="table table-order-column table-striped table-bordered table-hover" id="gridTable">
                                       <thead>
                                          <tr style="color:#fff;font-weight:600;" class="text-center">
                                             <th style="width: 20%;">ACTION</th>
                                             <th>Employee Name</th>
                                             <th>Trainings/Programs</th>
                                             <th>Covered Date</th>
                                             <th>Provider</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <?php
                                             $rs = SelectEach($table,"");
                                             if ($rs) {
                                                while ($row = mysqli_fetch_assoc($rs)) {
                                                   $refid = $row["RefId"];
                                                   $emprefid = $row["EmployeesRefId"];
                                                   $emprow = FindFirst("employees","WHERE RefId = '$emprefid'","`FirstName`,`LastName`,`MiddleName`");
                                                   $LastName      = $emprow["LastName"];
                                                   $FirstName     = $emprow["FirstName"];
                                                   $MiddleName    = $emprow["MiddleName"];
                                                   $FullName      = $LastName.", ".$FirstName." ".$MiddleName;
                                                   $program = FindFirst("ldmslndprogram","WHERE RefId = '".$row["LDMSLNDProgramRefId"]."'","*");
                                                   if ($program) {
                                                      echo '<tr>';
                                                      echo '<td nowrap class="cellAction" style="padding:var(--grid_padding_td);">';
                                                      echo
                                                      '
                                                      <a style="text-decoration:none;cursor:pointer;margin-right:10px" title="Edit This Record">
                                                         <img src="'.img("edit.png").'" onclick="viewInfo('.$refid.',2,\'\');">
                                                      </a>
                                                      <a style="text-decoration:none;cursor:pointer" onclick="deleteRecord('.$refid.');"
                                                         title="Delete This Record" id="del_'.$refid.'">
                                                         <img src="'.img("delete.png").'">
                                                      </a>
                                                      ';
                                                      echo '</td>';
                                                      echo '<td nowrap>'.$FullName.'</td>';
                                                      echo '<td>'.$program["Name"].'</td>';
                                                      if ($program["StartDate"] != "") {
                                                         echo '<td>'.date("F d, Y",strtotime($program["StartDate"]))." to ".date("F d, Y",strtotime($program["EndDate"])).'</td>';   
                                                      } else {
                                                         echo '<td></td>';
                                                      }
                                                      
                                                      echo '<td>'.$program["TrainingInstitution"].'</td>';
                                                      echo '</tr>';  
                                                   }
                                                }
                                             }
                                          ?>
                                       </tbody>
                                    </table>
                                 </span>
                              </div>
                              <div class="panel-bottom">
                                 <?php
                                    btnINRECLO([true,true,false]);
                                    echo '
                                       <!--<button type="button"
                                            class="btn-cls4-lemon trnbtn"
                                            id="btnPRINT" name="btnPRINT">
                                          <i class="fa fa-print" aria-hidden="true"></i>&nbsp;
                                          PRINT
                                       </button>-->
                                    ';
                                 ?>
                              </div>
                           </div>
                        </div>
                        <div id="divView">
                           <div class="row">
                              <div class="col-xs-6">
                                 <div class="mypanel">
                                    <div class="panel-top">
                                       <span id="ScreenMode">ASSIGNED COURSES
                                    </div>
                                    <div class="panel-mid">
                                       <div id="EntryScrn">
                                          <div class="row margin-top">
                                             <div class="<?php echo $sizeCol; ?>">
                                                <div class="row" id="badgeRefId">
                                                   <div class="col-xs-6">
                                                      <ul class="nav nav-pills">
                                                         <li class="active" style="font-size:12pt;font-weight:600;">
                                                            <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                                                            </span></a>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                                <?php
                                                   $attr = ["br"=>true,
                                                            "type"=>"text",
                                                            "row"=>true,
                                                            "name"=>"date_FiledDate",
                                                            "col"=>"4",
                                                            "id"=>"FiledDate",
                                                            "label"=>"Date File",
                                                            "class"=>"saveFields-- mandatory date--",
                                                            "style"=>"",
                                                            "other"=>""];
                                                   $form->eform($attr);
                                                ?>
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <label>Course Title</label>
                                                      <?php
                                                         createSelect("LDMSLNDProgram",
                                                                   "sint_LDMSLNDProgramRefId",
                                                                   "",100,
                                                                   "Name",
                                                                   "Select Course","required");
                                                      ?>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <label class="control-label" for="inputs">Endorsed By:</label><br>
                                                      <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_EndorsedBy"
                                                          id="char_EndorsedBy">
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-xs-6 padd5 adminUse--">
                                                <label>Employees Selected</label>
                                                <select multiple id="EmpSelected" name="EmpSelected" class="form-input" style="height:250px;">
                                                </select>
                                                <p>Double Click the items to remove in the List</p>
                                                <input type="hidden" class="saveFields--" name="hEmpSelected">
                                             </div>
                                          </div>
                                       </div>
                                       <?php
                                          spacer(10);
                                          echo
                                          '<button type="button" class="btn-cls4-sea"
                                                   name="btnLocSAVE" id="LocSAVE">
                                             <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                             &nbsp;Save
                                          </button>';
                                          btnSACABA([false,true,true]);
                                       ?>
                                    </div>
                                    <div class="panel-bottom"></div>
                                 </div>
                              </div>

                              <div class="col-xs-6 adminUse--">
                                 <div class="mypanel">
                                    <div class="panel-top">Employees List</div>
                                    <div class="panel-mid">
                                       <?php include_once "incEmpListSearchCriteria.e2e.php" ?>
                                       <!-- <a href="javascript:void(0);" title="Search Employees">
                                          <i class="fa fa-search" aria-hidden="true"></i>
                                       </a>-->
                                       <div id="empList" style="max-height:300px;overflow:auto;">
                                          <h4>No Employees Selected</h4>
                                       </div>
                                    </div>
                                    <div class="panel-bottom">
                                       <a href="javascript:void(0);" id="clearEmpSelected">Clear Employees Selected</a>&nbsp;
                                    </div>
                                 </div>
                              </div>

                           </div>
                        </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varJSON.e2e.php";
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>

   </body>
</html>