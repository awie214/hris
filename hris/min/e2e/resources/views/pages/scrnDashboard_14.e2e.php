<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <!-- <script language="JavaScript" src="<?php echo jsCtrl("ctrl_Dashboard"); ?>"></script> -->
      <style type="text/css">
         .card {
            border: 1px solid black; 
            border-radius: 5px; 
            background: #0067a7; 
            color: white;
            padding: 10px;
            font-weight: 600;
         }
         .card:hover {
            transition: 0.2s;
            box-shadow: 3px 3px 3px gray;
         }
         .card-body {
            margin-top: 5px;
            border: 1px solid gray; 
            background: #fffce4; 
            color: black;
            border-radius: 5px;
            padding: 8px;
         }
         .card-body:hover {
            transition: 0.5s;
            -ms-transform: rotate(-3deg);
            -webkit-transform: rotate(-3deg);
            transform: rotate(-3deg);
         }
      </style>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar($modTitle); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" style="padding: 10px;">
                     <div class="row">
                        <div class="col-xs-2">
                           <div class="row card">
                              <div class="col-xs-12">
                                 <div class="row text-center">
                                    <div class="col-xs-12">
                                       No. of Employees
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-2"></div>
                                    <div class="col-xs-8 text-center card-body">
                                       10
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-1"></div>
                        <div class="col-xs-2">
                           <div class="row card">
                              <div class="col-xs-12">
                                 <div class="row text-center">
                                    <div class="col-xs-12">
                                       Male Employees
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-2"></div>
                                    <div class="col-xs-8 text-center card-body">
                                       10
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-1"></div>
                        <div class="col-xs-2">
                           <div class="row card">
                              <div class="col-xs-12">
                                 <div class="row text-center">
                                    <div class="col-xs-12">
                                       Female Employees
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-2"></div>
                                    <div class="col-xs-8 text-center card-body">
                                       10
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-1"></div>
                        <div class="col-xs-2">
                           <div class="row card">
                              <div class="col-xs-12">
                                 <div class="row text-center">
                                    <div class="col-xs-12">
                                       Birthday Celebrants
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-2"></div>
                                    <div class="col-xs-8 text-center card-body">
                                       10
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <br><br>
                     <div class="row">
                        <div class="col-xs-2">
                           <div class="row card">
                              <div class="col-xs-12">
                                 <div class="row text-center">
                                    <div class="col-xs-12">
                                       Plantilla
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-2"></div>
                                    <div class="col-xs-8 text-center card-body">
                                       10
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-1"></div>
                        <div class="col-xs-2">
                           <div class="row card">
                              <div class="col-xs-12">
                                 <div class="row text-center">
                                    <div class="col-xs-12">
                                       Non-Plantilla
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-2"></div>
                                    <div class="col-xs-8 text-center card-body">
                                       10
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-1"></div>
                        <div class="col-xs-5">
                           <div class="row card">
                              <div class="col-xs-12">
                                 <div class="row text-center">
                                    <div class="col-xs-12">
                                       Employees at Retirement Age
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-4"></div>
                                    <div class="col-xs-4 text-center card-body">
                                       10
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <br>
                     
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade border0" id="prnModal" role="dialog">
               <div class="modal-dialog border0" style="padding:0px;width:97%;height:92%;">

                  <div class="mypanel border0" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Print Now" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <label class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varHidden.e2e.php";
               doHidden("hRptFile","DashboardRpt","");
            ?>
         </div>
      </form>
      <!-- <script language="JavaScript" src="<?php echo jsCtrl("ctrl_Dashboard"); ?>"></script> -->
   </body>
</html>



