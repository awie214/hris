<?php
   require_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   require_once pathClass.'0620RptFunctions.e2e.php';
   require_once pathClass.'DTRFunction.e2e.php';
   require_once "conn.e2e.php";
   $whereClause = "WHERE RefId = ".getvalue('hUserRefId');
   $year = date("Y",time());
   $arr_month =[
     "January",
     "February",
     "March",
     "April",
     "May",
     "June",
     "July",
     "August",
     "September",
     "October",
     "November",
     "December"
   ];
?>
<!DOCTYPE html>
<html>
   <head>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script type="text/javascript">
         $(document).ready(function () {
            $("#btnPrint").click(function () {
               var head = $("head").html();
               printDiv('div_CONTENT',head);
            });
         });
      </script>
      <style type="text/css">
         th {
            text-align: center;
         }
         @page { size: landscape; }
      </style>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis") ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar ("Leave Card"); ?>
            <div class="container-fluid margin-top">
               <button type="button" id="btnPrint" class="btn-cls4-lemon">PRINT</button>
               <div class="row">
                  <div class="col-sm-11" id="div_CONTENT">
                     <?php
                        $rsEmployees = SelectEach("employees",$whereClause);
                        if ($rsEmployees) {
                           while ($row = mysqli_fetch_assoc($rsEmployees)) {
                              $vl_earned_carried = 0; 
                              $sl_earned_carried = 0; 
                              $EmployeesRefId = $row["RefId"];
                              $CompanyRefId   = $row["CompanyRefId"];
                              $BranchRefId    = $row["BranchRefId"];
                              $emp_row = FindFirst("empinformation","WHERE EmployeesRefId = ".$row["RefId"],"*");
                              if ($emp_row) {
                                 $appt    = $emp_row["ApptStatusRefId"];
                                 $appt    = getRecord("apptstatus",$appt,"Name");
                                 $div     = getRecord("division",$emp_row["DivisionRefId"],"Name");
                                 $hired   = date("d F Y",strtotime($emp_row["HiredDate"]));
                                 $worksched = $emp_row["WorkScheduleRefId"];
                                 $office = getRecord("office",$emp_row["OfficeRefId"],"Name");
                              } else {
                                 $appt    = "";
                                 $div     = "";
                                 $hired   = "";
                                 $worksched = "";
                                 $office = "";
                              }
                              rptHeader(getRptName("rptLeave_Card"));
                     ?>               
                              <div class="row" style="padding:10px;">
                                 <div class="col-xs-6">
                                    <?php 
                                       echo "NAME : ".$row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"];  
                                    ?>
                                 </div>
                                 <div class="col-xs-1 text-right">Office:</div>
                                 <div class="col-xs-5">
                                    <b><?php echo $office; ?></b>
                                 </div>
                                 <div class="col-xs-6"></div>
                                 <div class="col-xs-1 text-right">Department:</div>
                                 <div class="col-xs-5">
                                    <b><?php echo $div; ?></b>
                                 </div>
                                 
                              </div>
                              <table border="1" width="100%">
                                 <thead>
                                    <tr class="colHEADER">
                                       <th rowspan="3" style="width: 15%;">Period Covered</th>
                                       <th rowspan="3" style="width: 15%;">Particular</th>
                                       <th colspan="6">Vacation Leave</th>
                                       <th colspan="4">Sick Leave</th>
                                       <th rowspan="3">AVAILABLE<br>SPL</th>
                                       <th rowspan="3">Remarks</th>
                                    </tr>
                                    <tr class="colHEADER">
                                       <th rowspan="2">EARNED</th>
                                       <th rowspan="2">ABSENCE</th>
                                       <th colspan="2">UT W/Pay</th>
                                       <th rowspan="2">BALANCE</th>
                                       <th rowspan="2">ABSENCE<br>UT W/O PAY</th>
                                       <th rowspan="2">EARNED</th>
                                       <th rowspan="2">ABSENCE<br>UT W/PAY</th>
                                       <th rowspan="2">BALANCE</th>
                                       <th rowspan="2">ABSENCE<br>UT W/O PAY</th>
                                    </tr>
                                    <tr class="colHEADER">
                                       <th>HOURS</th>
                                       <th>MINS</th>
                                    </tr>
                                 </thead>   
                                 <tbody>
                                    <?php
                                       //return false;
                                       $whereVL    = "WHERE EmployeesRefId = ".$row['RefId']; 
                                       $whereVL    .= " AND EffectivityYear = '$year'";
                                       $whereVL    .= " AND NameCredits = 'VL'"; 
                                       $row_vl     = FindFirst("employeescreditbalance",$whereVL,"*");
                                       if ($row_vl) {
                                          $vl         = $row_vl["BeginningBalance"];
                                          $AsOf       = $row_vl["BegBalAsOfDate"];
                                          $start_date = date("Y-m-d",strtotime( "$AsOf + 1 day" ));
                                       } else {
                                          $vl         = 0;
                                          $AsOf       = date("Y-m",time())."-01";
                                          $start_date = date("Y-m-d",strtotime( "$AsOf - 1 day" ));
                                       }

                                       

                                       $whereSL    = "WHERE EmployeesRefId = ".$row['RefId']; 
                                       $whereSL    .= " AND EffectivityYear = '$year'";
                                       $whereSL    .= " AND NameCredits = 'SL'"; 
                                       $row_sl     = FindFirst("employeescreditbalance",$whereSL,"*");  
                                       if ($row_sl) {
                                          $sl = $row_sl["BeginningBalance"];
                                       } else {
                                          $sl = 0;
                                       }

                                       $whereSPL    = "WHERE EmployeesRefId = ".$row['RefId']; 
                                       $whereSPL    .= " AND EffectivityYear = '$year'";
                                       $whereSPL    .= " AND NameCredits = 'SPL'"; 
                                       $row_spl     = FindFirst("employeescreditbalance",$whereSPL,"*");  
                                       if ($row_spl) {
                                          $spl = $row_spl["BeginningBalance"];
                                       } else {
                                          $spl = 0;
                                       }

                                       //initial balance
                                       ${"VLBal_".$EmployeesRefId} = $vl;
                                       ${"SLBal_".$EmployeesRefId} = $sl;
                                       ${"SPLBal_".$EmployeesRefId} = $spl;

                                       echo '
                                          <tr>
                                             <td colspan="6">Beginning Balance As Of '.date("F d Y",strtotime($AsOf)).'</td>
                                             <td class="text-center">'.$vl.'</td>
                                             <td colspan="3"></td>
                                             <td class="text-center">'.$sl.'</td>
                                             <td colspan="3"></td>
                                          </tr>
                                       ';
                                       $from          = date("m",strtotime($start_date));
                                       for ($i=intval($from)-1; $i <= date("m",time()) - 2 ; $i++) {
                                          $sl_days    = "";
                                          $vl_days    = "";
                                          $spl_days   = "";
                                          $SPL_Used   = 0;
                                          $idx        = $i+1;
                                          if ($idx <= 9) $idx = "0".$idx;
                                          $month      = $idx;
                                          $where_dtr  = "WHERE EmployeesRefId = $EmployeesRefId";
                                          $where_dtr  .= " AND Month = '$month'";
                                          $where_dtr  .= " AND Year = '$year'";
                                          $arr_empDTR = FindFirst("dtr_process",$where_dtr,"*");
                                          if ($arr_empDTR) {
                                             $where_monetization = "WHERE EmployeesRefId = '$EmployeesRefId'";
                                             $where_monetization .= " AND MONTH(FiledDate) = '".$month."'";
                                             $where_monetization .= " AND YEAR(FiledDate) = '".$year."'";
                                             $where_monetization .= " AND Status = 'Approved'";
                                             $monetize   = FindFirst("employeesleavemonetization",$where_monetization,"*");
                                             if ($monetize) {
                                                $vl_monetize = $monetize["VLValue"];
                                                $sl_monetize = $monetize["SLValue"];
                                                $remarks_monetize = "Monetized ".floatval($vl_monetize)." (VL) AND ".floatval($sl_monetize)." (SL)";
                                             } else {
                                                $vl_monetize = $sl_monetize = 0;
                                                $remarks_monetize = "";
                                             }

                                             $TotalUT       = ToHours($arr_empDTR["Total_Undertime_Hr"]);
                                             $TotalTardy    = ToHours($arr_empDTR["Total_Tardy_Hr"]);
                                             $TotalDeduct   = ToHours($arr_empDTR["Total_Undertime_Hr"] 
                                                                      + $arr_empDTR["Total_Tardy_Hr"]);
                                             $UT_EQ         = $arr_empDTR["Undertime_Deduction_EQ"];
                                             $Absent_Count  = $arr_empDTR["Total_Absent_Count"];
                                             $Tardy_EQ      = $arr_empDTR["Tardy_Deduction_EQ"];
                                             $Total_Deduct  = $UT_EQ + $Tardy_EQ + $Absent_Count;

                                             $SL_Used       = $arr_empDTR["SL_Used"] + $sl_monetize;
                                             $SL_Earned     = $arr_empDTR["SL_Earned"];
                                             $SL_Days       = $arr_empDTR["SL_Days"];

                                             $VL_Used       = $arr_empDTR["VL_Used"] + $vl_monetize;
                                             $VL_Days       = $arr_empDTR["VL_Days"];
                                             $VL_Earned     = $arr_empDTR["VL_Earned"];

                                             $SPL_Days      = $arr_empDTR["SPL_Days"];


                                             

                                             if ($SPL_Days != "") {
                                                $spl_arr = explode("|", $SPL_Days);
                                                foreach ($spl_arr as $spl_key => $spl_value) {
                                                   if ($spl_value != "") {
                                                      $spl_days .= $spl_value.", ";
                                                      $SPL_Used++;
                                                      ${"SPLBal_".$EmployeesRefId} -= 1;
                                                   }
                                                }
                                                $spl_days .= " (SPL)";
                                             }
                                             
                                             if ($SL_Used > 0) {
                                                $sl_arr = explode("|", $SL_Days);
                                                foreach ($sl_arr as $sl_key => $sl_value) {
                                                   if ($sl_value != "") $sl_days .= $sl_value.", ";
                                                }
                                                $sl_days .= " (SL)";
                                             }
                                             if ($VL_Used > 0) {
                                                $vl_arr = explode("|", $VL_Days);
                                                foreach ($vl_arr as $vl_key => $vl_value) {
                                                   if ($vl_value != "") $vl_days .= $vl_value.", ";
                                                }
                                                $vl_days .= " (VL)";
                                             }

                                             if ($TotalDeduct == "") {
                                                $Hrs  = 0;
                                                $Mins = 0;
                                             } else {
                                                $arr_TotalDeduct = explode(":", $TotalDeduct);
                                                if (count($arr_TotalDeduct) > 1) {
                                                   $Hrs  = $arr_TotalDeduct[0];
                                                   $Mins = $arr_TotalDeduct[1];  
                                                } else {
                                                   $Hrs  = 0;
                                                   $Mins = 0;
                                                }
                                             }
                                             $Hrs  = getEquivalent(($Hrs * 60),"workinghrsconversion");
                                             $Mins = getEquivalent($Mins,"workinghrsconversion");
                                             echo '<tr>';
                                             echo  '<td class="text-center">'.$arr_month[$i].'</td>';
                                             echo '<td>';
                                                   if ($VL_Used > 0) echo $vl_days;
                                                   if ($SL_Used > 0) echo $sl_days;
                                                   if ($SPL_Days != "") echo $spl_days;
                                             echo '</td>';      
                                             echo '<td class="text-center">';
                                                   echo number_format($VL_Earned,3);     
                                             echo '</td>';
                                             echo '<td class="text-center">';
                                                   echo number_format($Absent_Count + $VL_Used,3);
                                             echo '</td>';
                                             echo '<td class="text-center">';
                                                   echo number_format($Hrs,3);
                                             echo '</td>';
                                             echo '<td class="text-center">';
                                                   echo number_format($Mins,3);
                                             echo '</td>';
                                             echo '<td class="text-center">';
                                                   ${"VLBal_".$EmployeesRefId} -= $Total_Deduct;
                                                   ${"VLBal_".$EmployeesRefId} -= $VL_Used;
                                                   if (${"VLBal_".$EmployeesRefId} < 0) {
                                                      $vl_wop = abs(${"VLBal_".$EmployeesRefId});
                                                      ${"VLBal_".$EmployeesRefId} = 0;
                                                   } else {
                                                      $vl_wop = 0;
                                                   }

                                                   ${"VLBal_".$EmployeesRefId} += $VL_Earned;
                                                   echo number_format(${"VLBal_".$EmployeesRefId},3);
                                             echo '</td>';
                                             echo '<td class="text-center">';
                                                   echo number_format($vl_wop,3);
                                             echo '</td>';
                                             echo '<td class="text-center">';
                                                   echo number_format($SL_Earned,3);
                                             echo '</td>';
                                             echo '<td class="text-center">';
                                                   echo number_format($SL_Used,3);
                                             echo '</td>';
                                             echo '<td class="text-center">';
                                                   ${"SLBal_".$EmployeesRefId} -= $SL_Used;
                                                   ${"SLBal_".$EmployeesRefId} += $SL_Earned;
                                                   echo number_format(${"SLBal_".$EmployeesRefId},3);
                                             echo '</td>';
                                             echo '<td class="text-center">';
                                                   
                                             echo '</td>';
                                             echo '<td class="text-center">';
                                                   echo number_format(${"SPLBal_".$EmployeesRefId},3);
                                             echo '</td>';
                                             echo '<td style="padding-left:5px;">';
                                                   $remAbsent = "";
                                                   $Absent_arr = explode("|", $arr_empDTR["Days_Absent"]);
                                                   foreach ($Absent_arr as $key => $value) {
                                                      if ($value != "") {
                                                         $remAbsent = $remAbsent.$value.", ";
                                                      }
                                                   }
                                                   echo $remarks_monetize;
                                                   
                                             echo '</td>';      
                                             echo '
                                                </tr>
                                             ';
                                          }
                                       }
                                    ?>
                                    <tr>
                                       <td>&nbsp;</td>
                                       <td class="text-center">BALANCE CARRIED FORWARD</td>
                                       <td class="text-center">
                                          <?php
                                             echo number_format(${"VLBal_".$EmployeesRefId},3);
                                          ?>
                                       </td>
                                       <td colspan="5"></td>
                                       <td class="text-center">
                                          <?php
                                             echo number_format(${"SLBal_".$EmployeesRefId},3);
                                          ?>
                                       </td>
                                       <td colspan="5"></td>
                                    </tr>
                                 </tbody>   
                              </table>
                              <p>
                                 This is a system generated report. Signature is not required.
                              </p>
                     <?php 
                           }
                        }
                     ?>
                  </div>
               </div>
            </div>
            <?php
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>