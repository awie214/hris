<?php 
   $module = module("AssignedCourse"); 
   $emprefid = $_GET["hEmpRefId"];
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript">
         $(document).ready(function () {
            remIconDL();
            $("#LocSAVE").click(function () {
               if (saveProceed() == 0) {
                  $(this).attr("type","submit"); 
               }
            });
            <?php
               if (isset($_GET["errmsg"])) {
                  echo '$.notify("'.$_GET["errmsg"].'");';
               }
            ?>
         });
         
         function afterDelete() {
            alert("Successfully Deleted");
            gotoscrn($("#hProg").val(),"");
         }
      </script>
   </head>
   <body>
      <form name="xForm" method="post" action="postMultiple.e2e.php">
         <?php $sys->SysHdr($sys,"ldms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("Applied Trainings"); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                        <div id="divList">
                           <div class="mypanel">
                              <div class="panel-top">List of Applied Trainings</div>
                              <div class="panel-mid">
                                 <span id="spGridTable">
                                    <?php
                                       $table = "assigned_courses";
                                       $sizeCol = "col-xs-6";
                                       $gridTableHdr_arr = ["File Date", "Employees Name", "Course", "Status"];
                                       $gridTableFld_arr = ["FiledDate", "EmployeesRefId", "LDMSLNDProgramRefId", "Remarks"];
                                       $sql = "SELECT * FROM `assigned_courses` WHERE EmployeesRefId = '$emprefid' ORDER BY FiledDate DESC";
                                       $Action = [false,true,true,true];
                                       doGridTable($table,
                                                   $gridTableHdr_arr,
                                                   $gridTableFld_arr,
                                                   $sql,
                                                   $Action,
                                                   "gridTable");
                                    ?>
                                 </span>
                              </div>
                              <div class="panel-bottom">
                                 <?php
                                    btnINRECLO([true,true,false]);
                                    echo '
                                       <!--<button type="button"
                                            class="btn-cls4-lemon trnbtn"
                                            id="btnPRINT" name="btnPRINT">
                                          <i class="fa fa-print" aria-hidden="true"></i>&nbsp;
                                          PRINT
                                       </button>-->
                                    ';
                                 ?>
                              </div>
                           </div>
                        </div>
                        <div id="divView">
                           <div class="row">
                              <div class="col-xs-6">
                                 <div class="mypanel">
                                    <div class="panel-top">
                                       <span id="ScreenMode">ASSIGNED COURSES
                                    </div>
                                    <div class="panel-mid">
                                       <div id="EntryScrn">
                                          <div class="row margin-top">
                                             <div class="<?php echo $sizeCol; ?>">
                                                <div class="row" id="badgeRefId">
                                                   <div class="col-xs-6">
                                                      <ul class="nav nav-pills">
                                                         <li class="active" style="font-size:12pt;font-weight:600;">
                                                            <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                                                            </span></a>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <label class="control-label" for="inputs">Date Filed:</label><br>
                                                      <input class="form-input saveFields-- mandatory date--" 
                                                          type="text"
                                                          name="FiledDate"
                                                          id="FiledDate" readonly value="<?php echo date("Y-m-d",time()); ?>">
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <label class="control-label" for="inputs">Course Title:</label><br>
                                                      <select class="form-input saveFields--" name="LDMSLNDProgramRefId" id="LDMSLNDProgramRefId">
                                                         <option value="">Select Training</option>
                                                         <?php
                                                            $rs2 = SelectEach("ldmslndprogram","");
                                                            if ($rs) {
                                                               while ($row2 = mysqli_fetch_assoc($rs2)) {
                                                                  $refid = $row2["RefId"];
                                                                  $title = $row2["Name"];
                                                                  echo '<option value="'.$refid.'">'.$title.'</option>';
                                                               }
                                                            }
                                                         ?>
                                                         
                                                      </select>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <label class="control-label" for="inputs">Endorsed By:</label><br>
                                                      <input class="form-input saveFields-- mandatory" 
                                                          type="text"
                                                          name="char_EndorsedBy"
                                                          id="char_EndorsedBy">
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <?php
                                          spacer(10);
                                          echo
                                          '<button type="button" class="btn-cls4-sea"
                                                   name="btnLocSAVE" id="LocSAVE">
                                             <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                             &nbsp;Save
                                          </button>';
                                          btnSACABA([false,true,true]);
                                       ?>
                                    </div>
                                    <div class="panel-bottom"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varJSON.e2e.php";
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
   <script type="text/javascript">
      function selectMe(refid){
         $("#rptContent").attr("src","blank.htm");
         var rptFile = "rpt_nomination_form";
         var url = "ReportCaller.e2e.php?file=" + rptFile;
         url += "&refid=" + refid;
         url += "&" + $("[name='hgParam']").val();
         $("#prnModal").modal();
         $("#rptContent").attr("src",url);
      }
   </script>
</html>