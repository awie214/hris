<?php
   require_once "constant.e2e.php";
   require_once pathClass.'0620RptFunctions.e2e.php';
   $table = "employees";
   $service_record_arr = array();
   $EmployeesRefId = getvalue("hEmpRefId");
   $rsEmployees = SelectEach("employees","WHERE RefId = '$EmployeesRefId'");
   
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script type="text/javascript">
         $(document).ready(function () {
            $("#btnPrint").click(function () {
               var head = $("head").html();
               printDiv('div_CONTENT',head);
            });
         });
      </script>
      <style type="text/css">
         @media print {
            body {
               font-size: 9pt;
            }
            thead {
               font-size: 9pt;  
            }
            tbody {
               font-size: 8pt !important;
            }
            td {
               font-size: 8pt !important;
            }

            /*2480 pixels x 3508*/
         }
         body {
            background:#ffffff;
            color:#000;
            font-size:10pt;
            font-family:Arial;
         }
      </style>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar ("SERVICE RECORD"); ?>
            <div class="container-fluid margin-top">
               <button type="button" id="btnPrint" class="btn-cls4-lemon">PRINT</button>
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div class="container-fluid">
                        <?php
                           if ($rsEmployees) {
                              while ($row = mysqli_fetch_assoc($rsEmployees)) {
                                 $emprefid   = $row["RefId"];
                                 $LastName   = $row["LastName"];
                                 $FirstName  = $row["FirstName"];
                                 $MiddleName = $row["MiddleName"];
                                 $ExtName    = $row["ExtName"];
                                 $BirthDate  = $row["BirthDate"];
                                 $BirthPlace = $row["BirthPlace"];
                                 if ($BirthDate != "") {
                                    $BirthDate = date("F d, Y",strtotime($BirthDate));
                                 }
                        ?>
                        <div class="row" style="page-break-after: always;">
                           <div class="col-xs-12">
                              
                              <div class="row margin-top">
                                 <div class="col-xs-4 text-right">
                                    <img src="../../../public/images/35/report_logo.png" style="width:40%;">
                                 </div>
                                 <div class="col-xs-4 text-center">
                                    Republic of the Philippines
                                    <br>
                                    Philippine Commission on Women
                                    <br>
                                    1145 J.P. Laurel St., San Miguel Manila
                                    <br>
                                    Tel. No. (02) 735-4767 | Fax No. (02) 736-4449
                                    <br>
                                    Email: oed@pcw.gov.ph | website: www.pcw.gov.ph
                                    <br>
                                    <br>
                                    <h4><b>SERVICE RECORD</b></h4>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-1">
                                    NAME
                                 </div>
                                 <div class="col-xs-2">
                                    <u><?php echo $LastName; ?></u>
                                    <br>
                                    (Surname)
                                 </div>
                                 <div class="col-xs-3">
                                    <u><?php echo $FirstName; ?></u>
                                    <br>
                                    (Given Name)
                                 </div>
                                 <div class="col-xs-2">
                                    <u><?php echo $MiddleName; ?></u>
                                    <br>
                                    (Middle Name)
                                 </div>
                                 <div class="col-xs-3">
                                    (If married woman, give also full maiden name)
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-1">
                                    BIRTH
                                 </div>
                                 <div class="col-xs-2">
                                    <u><?php echo $BirthDate; ?></u>
                                    <br>
                                    (Date)
                                 </div>
                                 <div class="col-xs-3">
                                    <u><?php echo $BirthPlace; ?></u>
                                    <br>
                                    (Place)
                                 </div>
                                 <div class="col-xs-2">
                                    <u><?php echo $MiddleName; ?></u>
                                    <br>
                                    (Middle Name)
                                 </div>
                                 <div class="col-xs-3">
                                    Data herein should be checked from birth or
                                    baptismal certificate or some other reliable
                                    documents
                                 </div>
                              </div>
                              <br>
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    This is to certify that the above named employee actually rendered services in this Office as shown by the "SERVICE RECORD" below. Each line of which is supported by appointments and other papers actually issued and
                                    approved by the authorities concerned
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    <table width="100%">
                                       <thead>
                                          <tr class="colHEADER">
                                             <th colspan="2">Service<br>(Inclusive Dates)</th>
                                             <th colspan="3">Record of Appointment</th>
                                             <th rowspan="2" style="width: 10%;">Remarks</th>
                                             <th rowspan="2" style="width: 10%;">Office<br>Entity/Division/Station/Place<br>of Assignment</th>
                                             <th rowspan="2" style="width: 8%;">Branch<br>(3)</th>
                                             <th rowspan="2" style="width: 8%;">L/V ABS<br>W/O Pay</th>
                                             <th colspan="2">Separation(4)</th>
                                          </tr>
                                          <tr class="colHEADER">
                                             <th style="width: 10%;">From</th>
                                             <th style="width: 10%;">To</th>
                                             <th style="width: 10%;">Designation</th>
                                             <th style="width: 10%;">Status(1)</th>
                                             <th style="width: 8%;">Annual Salary(2)</th>
                                             <th style="width: 8%;">Date</th>
                                             <th style="width: 8%;">Cause</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <?php
                                             $work_exp = SelectEach("employeesworkexperience","WHERE EmployeesRefId = '$emprefid' ORDER BY WorkStartDate DESC");
                                             if ($work_exp) {
                                                while ($exp_row = mysqli_fetch_assoc($work_exp)) {
                                                   $From = $exp_row["WorkStartDate"];
                                                   $To   = $exp_row["WorkEndDate"];
                                                   $Present = $exp_row["Present"];
                                                   if ($Present == "1") {
                                                      $To = "PRESENT";
                                                   } else {
                                                      if ($To != "") {
                                                         $To = date("F d,Y",strtotime($To));   
                                                      } else {
                                                         $To = "";
                                                      }
                                                      
                                                   }
                                                   if ($From != "") {
                                                      $From = date("F d,Y",strtotime($From));
                                                   } else {
                                                      $From = "";
                                                   }
                                                   $Position = getRecord("position",$exp_row["PositionRefId"],"Name");
                                                   $EmpStatus = getRecord("EmpStatus",$exp_row["EmpStatusRefId"],"Name");
                                                   $Salary = number_format($exp_row["SalaryAmount"],2);
                                                   echo '
                                                   <tr>
                                                      <td>'.$From.'</td>
                                                      <td>'.$To.'</td>
                                                      <td>'.$Position.'</td>
                                                      <td>'.$EmpStatus.'</td>
                                                      <td>'.$Salary.'</td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                   </tr>
                                                   ';
                                                }
                                             } else {
                                                for ($i=1; $i <= 5 ; $i++) { 
                                                   echo '
                                                   <tr>
                                                      <td>&nbsp;</td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                   </tr>
                                                   ';
                                                }   
                                             }
                                          ?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                              <br>
                              <div class="row margin-top">
                                 <div class="col-xs-1"></div>
                                 <div class="col-xs-11">
                                    Issued in compliance with Executive Order No. 54 dated August 10, 1954 and accordance with the Circular No. 58 dated August 10, 1954 of the system
                                 </div>
                              </div>
                              <br>
                              <div class="row margin-top">
                                 <div class="col-xs-6">
                                    <u><?php echo date("F d, Y",time()); ?></u>
                                    <br>
                                    Date
                                 </div>
                                 <div class="col-xs-6">
                                    Certified Correct:
                                    <br>
                                    <br>
                                    <b><u>JUANA DELA CRUZ</u></b>
                                    <br>
                                    Head of Office
                                    <br>
                                    <b><u>Executive Director</u></b>
                                    <br>
                                    Designation
                                 </div>
                              </div>
                           </div>
                        </div>
                        <?php
                              }
                           }
                        ?>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>



