<?php
   $tablePrefix = "employees";
   $tabTitle = ["Personal Information",
                "Family Background",
                "Children",
                "Educational Background",
                "Elegibility","Work Experience",
                "Voluntary Work","Training Programs",
                "Other Information","PDS Questions",
                "Reference"];
   $tabTable = [$tablePrefix,
                $tablePrefix."family",
                $tablePrefix."child",
                $tablePrefix."educ",
                $tablePrefix."elegibility",
                $tablePrefix."workexperience",
                $tablePrefix."voluntary",
                $tablePrefix."training",
                $tablePrefix."otherinfo",
                $tablePrefix."pdsq",
                $tablePrefix."reference"];
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_201Updates"); ?>"></script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar($modTitle); ?>
            <div class="container-fluid margin-top10">
                  <div class="row">
                     <div class="col-xs-12" id="div_CONTENT">
                        <!--
                        <div class="row">
                           <div class="col-xs-12">
                              <?php
                                 /*
                                 echo createButton("Edit 201 File",
                                 "btnEDIT",
                                 "btn-cls4-sea trnbtn",
                                 "fa-pencil-square-o",
                                 "");*/
                              ?>
                           </div>
                        </div>
                        <?php //spacer(10); ?>
                        <div class="row" id="201List">
                           <div class="col-xs-12">
                              <div class="mypanel">
                                 <div class="panel-top" id="setList">
                                    201 UPDATES
                                 </div>
                                 <div class="panel-mid-litebg">
                                 </div>
                              </div>
                           </div>
                        </div>
                        -->

                        <div id="DataEntry">
                           <?php if ($GLOBALS['UserCode']!="COMPEMP")
                           { ?>
                           <div class="mypanel margin-top">
                              <div class="panel-top" id="setList">
                                 <i class="fa fa-sort-desc" aria-hidden="true" id="tableHdr"></i>&nbsp;
                                 EMPLOYEES LIST
                              </div>
                              <div class="panel-mid-litebg" id="tableDtl">
                                 <div class="row">
                                    <div class="col-xs-12">
                                       <div id="spGridTable">
                                          <?php
                                             $gridTableHdr = "Employees ID#|Last Name|First Name";
                                             $gridTableFld = "AgencyId|LastName|FirstName";
                                             $_SESSION["module_table"] = strtolower($tablePrefix);
                                             $_SESSION["module_sql"] = "SELECT * FROM `".strtolower($tablePrefix)."` ORDER BY RefId Desc LIMIT 500";
                                             $_SESSION["list_ShowAction"] = [false,false,false,true];
                                             $_SESSION["module_gridTableHdr_arr"] = explode("|",$gridTableHdr);
                                             $_SESSION["module_gridTableFld_arr"] = explode("|",$gridTableFld);
                                             $_SESSION["module_gridTable_ID"] = "gridTable";
                                             doGridTable($tablePrefix,
                                                         $_SESSION["module_gridTableHdr_arr"],
                                                         $_SESSION["module_gridTableFld_arr"],
                                                         $_SESSION["module_sql"],
                                                         $_SESSION["list_ShowAction"],
                                                         $_SESSION["module_gridTable_ID"]);
                                          ?>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="panel-bottom"></div>
                           </div>
                           <?php
                              spacer(5);
                           } else {
                              /*createButton("Request To Update",
                                 "btnRequestUpdate201",
                                 "btn-cls2-tree trnbtn",
                                 "fa-pencil-square-o",
                                 "");
                              spacer(5);*/
                           }
                           ?>
                           <div class="mypanel">
                              <div class="panel-top">
                                 <div class="row">
                                    <div class="col-xs-2 txt-right">
                                       EMPLOYEE NAME:
                                    </div>
                                    <div class="col-xs-3">
                                       <input type="text" class="form-input saveFields--" name="txtEmpName" disabled >
                                    </div>
                                    <div class="col-xs-2 txt-right">
                                       EMPLOYEE ID:
                                    </div>
                                    <div class="col-xs-2">
                                       <input type="text" class="form-input" name="txtEmpId" disabled >
                                    </div>
                                 </div>
                              </div>
                              <div class="panel-mid-litebg" id="dataEntry">
                                 <div class="row">
                                    <div class="col-xs-2">
                                       <div class="one-edge-shadow" style="background:white;padding:10px;border:1px solid var(--border);">
                                          <label>MODULE:</label><br>
                                          <select name="201Tabs" id="201Tabs" class="form-input saveFields--" disabled>
                                             <option value="">-- Select --</option>
                                             <?php
                                                for ($j=0;$j<count($tabTitle);$j++) {
                                                   echo
                                                   '<option value="'.$tabTable[$j].'">'.$tabTitle[$j].'</option>';
                                                }
                                             ?>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="col-xs-10" style="padding-right:10px;">
                                       <div class="row margin-top one-edge-shadow" style="padding-bottom:5px;" id="idTabDetails"></div>
                                    </div>
                                 </div>
                                 <br>
                              </div>
                              <div class="panel-bottom"></div>
                           </div>
                        </div>
                     </div>
                  </div>
            </div>
            <!-- Modal -->
            <div class="modal fade border0" id="modalAddNew" role="dialog">
               <div class="modal-dialog modal-md">
                  <div class="mypanel">
                     <div class="panel-top">
                        ADD NEW RECORD (<span id="dbtbl"></span>)
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <div class="panel-mid txt-center" id="addNewEntry">
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               $table = "updates201";
               include "varHidden.e2e.php";
            ?>
         </div>
         <input type="hidden" id="hEmployeesRefId" class="saveFields--">
         <input type="hidden" id="hCurrentIdx">
         <input type="hidden" id="dummyTable">
      </form>
   </body>
</html>



