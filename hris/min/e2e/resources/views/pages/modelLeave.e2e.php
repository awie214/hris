<?php
   require_once "constant.e2e.php";
   require_once "inc_model.e2e.php";
   require_once "incUtilitiesJS.e2e.php";
?>
   <div class="container" id="EntryScrn">
      <div class="row" id="EntryScrn">
         <div class="col-xs-6">
            <?php if ($ScrnMode != 1) { ?>
               <div class="row">
                  <ul class="nav nav-pills">
                     <li class="active" style="font-size:12pt;font-weight:600;">
                        <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                        <?php echo $refid; ?>
                        </span></a>
                     </li>
                  </ul>
               </div>
            <?php } ?>
            <div class="row margin-top10">
               <div class="form-group">
                  <label class="control-label" for="inputs">CODE:</label><br>
                  <input class="form-input saveFields-- uCase-- mandatory" type="text" placeholder="Code"
                     id="inputs" name="char_Code" style='width:75%' autofocus>
               </div>
            </div>
            <div class="row">
               <div class="form-group">
                  <label class="control-label" for="inputs">NAME:</label><br>
                  <input class="form-input saveFields-- mandatory"
                         type="text" placeholder="name"
                         id="inputs" name="char_Name"
                         style='width:75%'>
               </div>
            </div>
            <div class="row" style="display: none;">
               <div class="form-group">
                  <label class="control-label" for="inputs">REST DAY INCLUDED:</label><br>
                  <select class="form-input saveFields--" name="sint_RestDay" id="sint_RestDay">
                     <option value="0">NO</option>
                     <option value="1">YES</option>
                  </select>
               </div>
            </div>
            <div class="row">
               <div class="form-group">
                  <label class="control-label" for="inputs">REMARKS:</label>
                  <textarea class="form-input saveFields--" rows="5" name="char_Remarks" <?php echo $disabled; ?>
                  placeholder="remarks"><?php echo $remarks; ?></textarea>
               </div>
            </div>
         </div>
      </div>
   </div>


