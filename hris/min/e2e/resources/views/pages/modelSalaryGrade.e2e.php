<?php
   require_once "constant.e2e.php";
   require_once "inc_model.e2e.php";
   require_once "incUtilitiesJS.e2e.php";
?>
   <div class="container-fluid" id="EntryScrn">
      <div class="row">
         <div class="col-xs-12">
            <?php if ($ScrnMode != 1) { ?>
               <div class="row">
                  <ul class="nav nav-pills">
                     <li class="active" style="font-size:12pt;font-weight:600;">
                        <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                        <?php echo $refid; ?>
                        </span></a>
                     </li>
                  </ul>
               </div>
            <?php } ?>
            <!--<div class="row">
               <div class="form-group">
                  <label class="control-label" for="inputs">NAME:</label><br>
                  <input class="form-input saveFields--" type="text" placeholder="name" <?php //echo $disabled; ?>
                     id="inputs" name="char_Name" style='width:75%' value="<?php //echo $name; ?>">
               </div>
            </div>
            -->
               <div class="row">
                  <div class="col-xs-4">
                     <label>NAME:</label><br>
                     <input type="text" class="form-input saveFields-- mandatory" name="char_Name" autofocus>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-1">
                     <label>Step 1:</label><br>
                     <input type="text" class="form-input saveFields-- number--" name="deci_Step1">
                  </div>
                  <div class="col-xs-1">
                     <label>Step 2</label><br>
                     <input type="text" class="form-input saveFields-- number--" name="deci_Step2">
                  </div>
                  <div class="col-xs-1">
                     <label>Step 3</label><br>
                     <input type="text" class="form-input saveFields-- number--" name="deci_Step3">
                  </div>
                  <div class="col-xs-1">
                     <label>Step 4</label><br>
                     <input type="text" class="form-input saveFields-- number--" name="deci_Step4">
                  </div>
                  <div class="col-xs-1">
                     <label>Step 5:</label><br>
                     <input type="text" class="form-input saveFields-- number--" name="deci_Step5">
                  </div>
                  <div class="col-xs-1">
                     <label>Step 6:</label><br>
                     <input type="text" class="form-input saveFields-- number--" name="deci_Step6">
                  </div>
                  <div class="col-xs-1">
                     <label>Step 7:</label><br>
                     <input type="text" class="form-input saveFields-- number--" name="deci_Step7">
                  </div>
                  <div class="col-xs-1">
                     <label>Step 8:</label><br>
                     <input type="text" class="form-input saveFields-- number--" name="deci_Step8">
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-1">
                     <label>Eff Date 1:</label><br>
                     <input type="text" class="form-input saveFields-- date--" name="date_EffectivityDateS1">
                  </div>
                  <div class="col-xs-1">
                     <label>Eff Date 2:</label><br>
                     <input type="text" class="form-input saveFields-- date--" name="date_EffectivityDateS2">
                  </div>
                  <div class="col-xs-1">
                     <label>Eff Date 3:</label><br>
                     <input type="text" class="form-input saveFields-- date--" name="date_EffectivityDateS3">
                  </div>
                  <div class="col-xs-1">
                     <label>Eff Date 4:</label><br>
                     <input type="text" class="form-input saveFields-- date--" name="date_EffectivityDateS4">
                  </div>
                  <div class="col-xs-1">
                     <label>Eff Date 5:</label><br>
                     <input type="text" class="form-input saveFields-- date--" name="date_EffectivityDateS5">
                  </div>
                  <div class="col-xs-1">
                     <label>Eff Date 6:</label><br>
                     <input type="text" class="form-input saveFields-- date--" name="date_EffectivityDateS6">
                  </div>
                  <div class="col-xs-1">
                     <label>Eff Date 7:</label><br>
                     <input type="text" class="form-input saveFields-- date--" name="date_EffectivityDateS7">
                  </div>
                  <div class="col-xs-1">
                     <label>Eff Date 8:</label><br>
                     <input type="text" class="form-input saveFields-- date--" name="date_EffectivityDateS8">
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-6 form-group">
                     <label class="control-label" for="inputs">REMARKS:</label>
                     <textarea class="form-input saveFields--" rows="5" name="char_Remarks"
                     placeholder="remarks"><?php echo $remarks; ?></textarea>
                  </div>
               </div>
         </div>
      </div>
   </div>