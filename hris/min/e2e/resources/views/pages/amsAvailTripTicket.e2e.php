<?php $module = module("AvailmentAuthority"); ?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript">
         $(document).ready(function () {
            $("[name='sint_AbsencesRefId']").addClass("mandatory");
            $("#btnPRINT").click(function () {
               popPDS("OB_2","");
            });
            $("#LocSAVE").click(function () {
               if (saveProceed() == 0) {
                  $(this).attr("type","submit"); 
               }
            });
            remIconDL();
            $("#destination").show();
            $("#time_canvas").hide();
            $("[name='sint_Whole']").change(function () {
               var value = $(this).val();
               if (value > 0) {
                  $("#time_canvas").hide();
               } else {
                  $("#time_canvas").show();
               }
            });
            <?php
               if (isset($_GET["errmsg"])) {
                  echo '$.notify("'.$_GET["errmsg"].'");';
               }
            ?>
            $("#btnPrintMultiple").click(function () {
               var control_number       = $("#control_number").val();
               if (control_number != "") {
                  $("#rptContent").attr("src","blank.htm");
                  var rptFile = "rpt_Trip_Ticket_14";
                  var url = "ReportCaller.e2e.php?file=" + rptFile;
                  url += "&control_number=" + control_number;
                  url += "&" + $("[name='hgParam']").val();
                  $("#prnModal").modal();
                  $("#rptContent").attr("src",url);         
               } else {
                  $.notify("Ticket Number is Required");
               }
               
            });
         });
         function getCtrlNo(value) {
            $.get("trn.e2e.php",
            {
               fn:"getCtrlNo",
               refid:value
            },
            function(data,status){
               if (status == 'success') {
                  eval(data);
               }
            });
         }
         function afterDelete() {
            alert("Successfully Deleted");
            gotoscrn($("#hProg").val(),"");
         }
         function selectMe(refid) {
            $("#rptContent").attr("src","blank.htm");
            var rptFile = "rpt_OB_2";
            var url = "ReportCaller.e2e.php?file=" + rptFile;
            url += "&refid=" + refid;
            url += "&diff_report=1";
            url += "&" + $("[name='hgParam']").val();
            $("#prnModal").modal();
            $("#rptContent").attr("src",url);
         }
      </script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="postMultiple.e2e.php">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar($paramTitle); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                        <div id="divList">
                           <div class="mypanel">
                              <div class="panel-top">List of Trip Ticket Application</div>
                              <div class="panel-mid">
                                 <span id="spGridTable">
                                    <?php
                                          $sizeCol = "col-xs-6";
                                          if ($UserCode == "COMPEMP") {
                                             $sizeCol = "col-xs-12";
                                             $gridTableHdr_arr = ["Office Authority","File Date", "Application Date From", "Application Date To", "Status"];
                                             $gridTableFld_arr = ["AbsencesRefId","FiledDate", "ApplicationDateFrom", "ApplicationDateTo", "Status"];
                                             $sql = "SELECT * FROM $table WHERE CompanyRefId = $CompanyId AND BranchRefId = $BranchId AND EmployeesRefId = $EmployeesRefId ORDER BY FiledDate DESC";
                                             $Action = [false,true,false,true];
                                          }
                                          doGridTable($table,
                                                      $gridTableHdr_arr,
                                                      $gridTableFld_arr,
                                                      $sql,
                                                      $Action,
                                                      "gridTable");
                                    ?>
                                 </span>
                                 <?php
                                       btnINRECLO([true,true,false]);
                                       /*echo '
                                          <button type="button"
                                               class="btn-cls4-lemon trnbtn"
                                               id="btnPRINT" name="btnPRINT">
                                             <i class="fa fa-print" aria-hidden="true"></i>&nbsp;
                                             PRINT
                                          </button>
                                       ';*/
                                 ?>
                                 <?php
                                    if ($UserCode != "COMPEMP") {
                                 ?>
                                 <button type="button" class="btn-cls4-tree" id="btnPrintMultiple">
                                    <i class="fa fa-print"></i>&nbsp;&nbsp;Print Trip Ticket
                                 </button>
                                 <label>Control Number:&nbsp;&nbsp;</label>
                                 <select type="text" name="control_number" id="control_number" class="form-input" style="width: 30%;">
                                    <option value="">Select Ticket No</option>
                                    <?php
                                       $rs = SelectEach("employeesauthority","GROUP BY ControlNumber");
                                       if ($rs) {
                                          while ($ctrl_row = mysqli_fetch_assoc($rs)) {
                                             if ($ctrl_row["ControlNumber"] != "") {
                                                echo '<option value="'.$ctrl_row["ControlNumber"].'">'.$ctrl_row["ControlNumber"].'</option>';   
                                             }
                                          }
                                       }
                                    ?>
                                 </select>
                                 <?php
                                    }
                                 ?>
                              </div>
                              <div class="panel-bottom"></div>
                           </div>
                        </div>
                        <div id="divView">
                           <div class="row">
                              <div class="col-xs-6">
                                 <div class="mypanel">
                                    <div class="panel-top">
                                       <span id="ScreenMode">AVAILING</span> <?php echo strtoupper($ScreenName); ?>
                                    </div>
                                    <div class="panel-mid">
                                       <div id="EntryScrn">
                                          <div class="row margin-top">
                                             <div class="<?php echo $sizeCol; ?>">
                                                <div class="row" id="badgeRefId">
                                                   <div class="col-xs-6">
                                                      <ul class="nav nav-pills">
                                                         <li class="active" style="font-size:12pt;font-weight:600;">
                                                            <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                                                            </span></a>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                                <?php
                                                   $attr = ["br"=>true,
                                                            "type"=>"text",
                                                            "row"=>true,
                                                            "name"=>"date_FiledDate",
                                                            "col"=>"6",
                                                            "id"=>"FiledDate",
                                                            "label"=>"Date of Filing",
                                                            "class"=>"saveFields-- mandatory date--",
                                                            "style"=>"",
                                                            "other"=>""];
                                                   $form->eform($attr);
                                                   spacer(10);
                                                   /*$attr = array(
                                                      "row"=>true,
                                                      "col"=>"12",
                                                      "label"=>"Office Authority",
                                                      "table"=>"Absences",
                                                   );
                                                   createFormFK($attr);*/  
                                                   echo '
                                                      <div class="row margin-top">
                                                         <div class="col-xs-12">
                                                         <label>Office Authority</label>
                                                   ';
                                                   createSelect("Absences",
                                                                "sint_AbsencesRefId",
                                                                "",100,"Name","Select Office Authority","required",false);
                                                   echo '
                                                         </div>
                                                      </div>
                                                   ';
                                                   ?>
                                                   <div class="row margin-top">
                                                      <div class="col-xs-12">
                                                         <div class="row">
                                                            <div class="col-xs-12">
                                                               <label>Inclusive Date</label>
                                                            </div>
                                                         </div>
                                                         <div class="row">
                                                            <div class="col-xs-6">
                                                               <label>From</label>
                                                               <br>
                                                               <input type="text" class="form-input mandatory saveFields-- date--" name="date_ApplicationDateFrom" id="date_ApplicationDateFrom" required="true">
                                                            </div>
                                                            <div class="col-xs-6">
                                                               <label>To</label>
                                                               <br>
                                                               <input type="text" class="form-input mandatory saveFields-- date--" name="date_ApplicationDateTo" id="date_ApplicationDateTo" required="true">
                                                            </div>
                                                         </div>

                                                      </div>
                                                   </div>
                                                   <div class="row margin-top">
                                                      <div class="col-xs-6">
                                                         <label>Whole Day?</label>
                                                         <select class="form-input saveFields--" name="sint_Whole" id="sint_Whole">
                                                            <option value="">Select --</option>
                                                            <option value="0">NO</option>
                                                            <option value="1">YES</option>
                                                         </select>
                                                      </div>
                                                   </div>
                                                   <?php
                                                   echo
                                                   '
                                                   <div id="time_canvas">
                                                      <label>Time</label><br>
                                                      <div class="row margin-top">
                                                         <div class="col-xs-12">
                                                            <div class="col-xs-6">
                                                            <label>From Time</label>
                                                            ';
                                                               //timePicker("FromTime","From Time");
                                                            drpTime("FromTime",8,30,0);
                                                            echo
                                                            '</div>
                                                            <div class="col-xs-6">
                                                            <label>To Time</label>
                                                            ';
                                                               //timePicker("ToTime","To Time");
                                                            drpTime("ToTime",8,30,0);
                                                            echo
                                                            '</div>';

                                                      echo
                                                      '
                                                         </div>
                                                      </div>
                                                   </div>
                                                   ';

                                                ?>
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <div class="row">
                                                         <div class="col-xs-12">
                                                            <label>Trip Ticket Number:</label>
                                                            <input type="text" class="form-input saveFields--" name="char_ControlNumber" id="char_ControlNumber">
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <div class="row">
                                                         <div class="col-xs-12">
                                                            <label>Driver:</label>
                                                            <select class="form-input saveFields--" name="char_Driver" id="char_Driver">
                                                               <option value="">Select Driver</option>
                                                               <?php
                                                                  $driver = SelectEach("employees","WHERE (Inactive != 1 OR Inactive IS NULL) ORDER BY LastName");
                                                                  if ($driver) {
                                                                     while ($driver_row = mysqli_fetch_assoc($driver)) {
                                                                        $d_lastname = $driver_row["LastName"];
                                                                        $d_firstname = $driver_row["FirstName"];
                                                                        $d_middlename = $driver_row["MiddleName"];
                                                                        $d_initial = substr($d_middlename, 0, 1);
                                                                        $d_full = $d_firstname." ".$d_initial.". ".$d_lastname;
                                                                        $d_full = $d_lastname.", ".$d_firstname." ".$d_initial.".";
                                                                        echo '<option value="'.$d_full.'">'.$d_full.'</option>';
                                                                     }
                                                                  }
                                                               ?>
                                                               
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <div class="row">
                                                         <div class="col-xs-12">
                                                            <label>Fuel Ticket Number:</label>
                                                            <input type="text" class="form-input saveFields--" name="char_FuelTicketNo" id="char_FuelTicketNo">
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <div class="row">
                                                         <div class="col-xs-12">
                                                            <label>Vehicle Type:</label>
                                                            <input type="text" class="form-input saveFields--" name="char_VehicleType" id="char_VehicleType">
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <div class="row">
                                                         <div class="col-xs-12">
                                                            <label>Plate No:</label>
                                                            <input type="text" class="form-input saveFields--" name="char_PlateNo" id="char_PlateNo">
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <div class="row">
                                                         <div class="col-xs-12">
                                                            <label>Regulation Area:</label>
                                                            <select class="form-input saveFields--" name="char_RegulationArea" id="char_RegulationArea">
                                                               <option value="0">Select Regulation Area</option>
                                                               <option value="FR">Financial Regulation</option>
                                                               <option value="TR">Technical Regulation</option>
                                                               <option value="CSR">Customer Service Regulation</option>
                                                               <option value="ALA">Administrative and Legal Affair</option>
                                                               <option value="OCR">Office of the Chief Regulator</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <div class="row">
                                                         <div class="col-xs-12">
                                                            <label>Department:</label>
                                                            <?php
                                                               createSelect("Division",
                                                                            "sint_DivisionRefId",
                                                                            "",100,"Name","Select Division","");
                                                            ?>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <div class="form-group">
                                                         <label class="control-label" for="inputs">Other Passenger/s:</label>
                                                         <textarea class="form-input saveFields--" rows="2" name="char_OtherPassenger" placeholder="Other Passenger"></textarea>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="row margin-top" id="destination">
                                                   <div class="col-xs-12">
                                                      <label class="control-label" for="inputs">Destination:</label>
                                                      <textarea class="form-input saveFields--" rows="2" name="char_Destination" placeholder="Destination"></textarea>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <label class="control-label" for="inputs">Purpose:</label>
                                                      <textarea class="form-input saveFields--" rows="5" name="char_Remarks" placeholder="Purpose"></textarea>
                                                   </div>
                                                </div>
                                                <div class="row">
                                                   <div class="col-xs-12">
                                                      <label class="control-label" for="inputs">Request By:</label>
                                                      <?php
                                                         createSelect("signatories",
                                                                      "sint_Signatory1",
                                                                      "",100,"Name","Select Signatory","",false);
                                                      ?>
                                                   </div>
                                                </div>
                                                <div class="row">
                                                   <div class="col-xs-12">
                                                      <label class="control-label" for="inputs">Approved By:</label>
                                                      <?php
                                                         createSelect("signatories",
                                                                      "sint_Signatory2",
                                                                      "",100,"Name","Select Signatory","",false);
                                                      ?>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-xs-6 padd5 adminUse--">
                                                <label>Employees Selected</label>
                                                <select multiple id="EmpSelected" name="EmpSelected" class="form-input" style="height:250px;">
                                                </select>
                                                <p>Double Click the items to remove in the List</p>
                                                <input type="hidden" class="saveFields--" name="hEmpSelected">
                                             </div>
                                          </div>
                                       </div>
                                       <?php
                                          spacer(10);
                                          echo
                                          '<button type="button" class="btn-cls4-sea"
                                                   name="btnLocSAVE" id="LocSAVE">
                                             <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                             &nbsp;Save
                                          </button>';
                                          btnSACABA([false,true,true]);
                                       ?>
                                    </div>
                                    <div class="panel-bottom"></div>
                                 </div>
                              </div>

                              <div class="col-xs-6 adminUse--">
                                 <div class="mypanel">
                                    <div class="panel-top">Employees List</div>
                                    <div class="panel-mid">
                                       <?php include_once "incEmpListSearchCriteria.e2e.php" ?>
                                       <!-- <a href="javascript:void(0);" title="Search Employees">
                                          <i class="fa fa-search" aria-hidden="true"></i>
                                       </a>-->
                                       <div id="empList" style="max-height:300px;overflow:auto;">
                                          <h4>No Employees Selected</h4>
                                       </div>
                                    </div>
                                    <div class="panel-bottom">
                                       <a href="javascript:void(0);" id="clearEmpSelected">Clear Employees Selected</a>&nbsp;
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varJSON.e2e.php";
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>

   </body>
</html>