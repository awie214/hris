      <div id="EntryWorkExp_<?php echo $j; ?>" class="entry201">
         <input type="checkbox" id="exp_<?php echo $j; ?>" 
         name="chkWorkExp_<?php echo $j; ?>" 
         class="enabler-- workexp_fpreview" 
         refid="" 
         fldName="date_WorkStartDate_<?php echo $j; ?>,
         date_WorkEndDate_<?php echo $j; ?>,
         sint_PositionRefId_<?php echo $j; ?>,
         sint_AgencyRefId_<?php echo $j; ?>,
         deci_SalaryAmount_<?php echo $j; ?>,
         sint_JobGradeRefId_<?php echo $j; ?>,
         sint_SalaryGradeRefId_<?php echo $j; ?>,
         sint_EmpStatusRefId_<?php echo $j; ?>,
         sint_isGovtService_<?php echo $j; ?>,
         char_PayGroup_<?php echo $j; ?>,
         sint_PayrateRefId_<?php echo $j; ?>,
         char_LWOP_<?php echo $j; ?>,
         date_ExtDate_<?php echo $j; ?>,
         date_SeparatedDate_<?php echo $j; ?>,
         char_Reason_<?php echo $j; ?>,
         char_Remarks_<?php echo $j; ?>,
         sint_isServiceRecord_<?php echo $j; ?>,
         sint_StepIncrementRefId_<?php echo $j; ?>,
         sint_Present_<?php echo $j; ?>"
         idx="<?php echo $j; ?>"
         unclick="CancelAddRow">
         <input type="hidden" name="workexperienceRefId_<?php echo $j;?>" value="">
         <label for="exp_<?php echo $j; ?>" class="btn-cls2-sea"><b>EDIT WORK EXPERIENCE #<?php echo $j; ?></b></label>

         <div>
            <div class="row margin-top">
               <div class="col-sm-3">
                  <label>Govt. Service:</label><br>
                  <select class="form-input saveFields--" 
                        idx="<?php echo $j; ?>" 
                        id="sint_isGovtService_<?php echo $j; ?>" 
                        name="sint_isGovtService_<?php echo $j; ?>" <?php echo $disabled; ?>
                        title='Govt. Service' onchange="isGovt(this.name);">
                     <option value="">is Govt. Service?</option>
                     <option value=0>NO</option>
                     <option value=1>YES</option>
                  </select>
               </div>
               <!-- 
               <div class="col-sm-3">
                  <label>Service Record:</label><br>
                  <select title="Service Record" 
                        class="form-input saveFields--" 
                        id="ServiceRecord_<?php echo $j; ?>" 
                        name="sint_isServiceRecord_<?php echo $j; ?>" <?php echo $disabled; ?>
                  >
                     <option value="">is Service Record?</option>
                     <option value="0">NO</option>
                     <option value="1">YES</option>
                  </select>
               </div>
               -->
               <div class="col-sm-4">
                  <div class="row" id="datework">
                     <div class="col-xs-5">
                        <label>Work Start Date:</label><br>
                        <input type="text" class="form-input date-- saveFields-- mandatory datefrom valDate--" placeholder="Work Start Date"
                           id="WorkStartDate_<?php echo $j; ?>" name="date_WorkStartDate_<?php echo $j; ?>" <?php echo $disabled; ?>
                           title="Work Start Date">
                     </div>
                     <div class="col-xs-7">
                        <label>Work End Date:</label><br>
                        <input type="text" class="form-input date-- saveFields-- mandatory dateto" for="WorkStartDate_<?php echo $j; ?>" placeholder="Work End Date"
                        id="WorkEndDate_<?php echo $j; ?>" name="date_WorkEndDate_<?php echo $j; ?>" <?php echo $disabled; ?>
                        title="Work End Date">
                        <?php doChkPresent($j,6); ?>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-4">
                  <label>Position:</label><br>
                  <?php
                     createSelect2("Position",
                                    "sint_PositionRefId_".$j,
                                    "",100,"Name","SELECT POSITION",$disabled." title='Position Title'","WHERE IsPrivate = 0 OR IsPrivate IS NULL");
                  ?>
               </div>
               <div class="col-xs-4">
                  <label>Agency:</label><br>
                  <?php
                     createSelect2("Agency",
                                    "sint_AgencyRefId_".$j,
                                    "",100,"Name","SELECT AGENCY",$disabled." title='Dept / Agency / Office'","WHERE IsPrivate = 0 OR IsPrivate IS NULL");
                  ?>
               </div>
               <div class="col-xs-4">
                  <label>Salary Amount:</label><br>
                  <input type="text" class="form-input decimal-- saveFields-- mandatory" placeholder="Salary"
                  id="SalaryAmount_<?php echo $j; ?>" name="deci_SalaryAmount_<?php echo $j; ?>" <?php echo $disabled; ?>
                  title='Salary Amount'>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-sm-4">
                  <label>Salary Grade:</label><br>
                  <?php
                     createSelect("SalaryGrade",
                                 "sint_SalaryGradeRefId_".$j,
                                 "",100,"Name","SELECT SALARY GRADE",$disabled." title='Salary Grade'",false);
                  ?>
               </div>
               <?php 
                  if (isset($settings)) {
                     $lgShowJG = $settings["show.JobGrade"];
                  } else {
                     $lgShowJG = $GLOBALS["settings"]["show.JobGrade"];
                  } 
               if ($lgShowJG) {
               ?>
                  <div class="col-sm-4">
                     <label>Job Grade:</label><br>
                     <?php
                        $sql = "SELECT * FROM `jobgrade` ORDER BY Name + 0";
                        $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
                        if ($result) {
                           echo '<select partner="sint_SalaryGradeRefId_'.$j.'" id="sint_JobGradeRefId_'.$j.'" name="sint_JobGradeRefId_'.$j.'" class="form-input saveFields-- rptCriteria-- createSelect--">'."\n";
                           echo '<option value="0">SELECT JOB GRADE</option>'."\n";
                           while($row = mysqli_fetch_assoc($result)) {
                              if ($row["Name"] != "") {
                                 echo '<option value="'.$row["RefId"].'"';
                                 echo '>'.$row["Name"].'</option>'."\n";
                              }
                           }
                           echo '</select>'."\n";
                        }
                     ?>
                  </div>
               <?php } ?>
               <div class="col-sm-4">
                  <label>Step Increment:</label><br>
                  <?php
                     createSelect("StepIncrement",
                                 "sint_StepIncrementRefId_".$j,
                                 "",100,"Name","SELECT STEP INCREMENT",
                                 $disabled." title='Step Increment'",false);
                  ?>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-sm-4" id="getEmpStat_<?php echo $j;?>">
                  <label>Emp. Status:</label><br>
                  <?php
                     createSelect2("EmpStatus",
                                 "sint_EmpStatusRefId_".$j,
                                 "",100,"Name","SELECT EMPLOYMENT STATUS",$disabled." title='Employment Status'","WHERE IsPrivate = 0 OR IsPrivate IS NULL",false);
                  ?>
               </div>
            </div>
         </div>
         
         <!--
         <div class="row margin-top">
            <div class="col-sm-2">
               <select class="form-input saveFields-- mandatory" idx="<?php echo $j; ?>"
                  id="sint_isGovtService_<?php echo $j; ?>" name="sint_isGovtService_<?php echo $j; ?>" <?php echo $disabled; ?>
                  title="Govt. Service" onChange="isGovt(this.name);">
                  <option value="">is Govt. Service?</option>
                  <option value=0>NO</option>
                  <option value=1>YES</option>
               </select>
            </div>
            <div class="col-sm-4 txt-center">
               <select title="Service Record" class="form-input saveFields--" id="ServiceRecord_<?php echo $j; ?>" name="sint_isServiceRecord_<?php echo $j; ?>" <?php echo $disabled; ?>
               >
                  <option value="">is Service Record?</option>
                  <option value="0">NO</option>
                  <option value="1">YES</option>
               </select>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-3">
               <div class="row" id="datework">
                  <div class="col-xs-4">
                     <input type="text" class="form-input date-- saveFields-- mandatory datefrom valDate--" placeholder="Work Start Date"
                        id="WorkStartDate_<?php echo $j; ?>" name="date_WorkStartDate_<?php echo $j; ?>" <?php echo $disabled; ?>
                        title="Work Start Date">
                  </div>
                  <div class="col-xs-8">
                     <input type="text" class="form-input date-- saveFields-- mandatory dateto" for="WorkStartDate_<?php echo $j; ?>" placeholder="Work End Date"
                     id="WorkEndDate_<?php echo $j; ?>" name="date_WorkEndDate_<?php echo $j; ?>" <?php echo $disabled; ?>
                     title="Work End Date">
                     <?php doChkPresent($j,6); ?>
                  </div>
               </div>
            </div>
            <div class="col-xs-3">
               <?php
                  createSelect2("Position",
                               "sint_PositionRefId_".$j,
                               "",100,"Name","SELECT POSITION",$disabled." title='Position Title'","WHERE IsPrivate = 0 OR IsPrivate IS NULL");
               ?>
            </div>
            <div class="col-xs-3">
               <?php
                  createSelect2("Agency",
                               "sint_AgencyRefId_".$j,
                               "",100,"Name","SELECT AGENCY",$disabled." title='Dept / Agency / Office'","WHERE IsPrivate = 0 OR IsPrivate IS NULL");
               ?>
            </div>
            <div class="col-xs-3">
               <input type="text" class="form-input decimal-- saveFields-- mandatory" placeholder="Salary"
               id="SalaryAmount_<?php echo $j; ?>" name="deci_SalaryAmount_<?php echo $j; ?>" <?php echo $disabled; ?>
               title='Salary Amount'>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-2">
               <?php
                  createSelect("SalaryGrade",
                               "sint_SalaryGradeRefId_".$j,
                               "",100,"Name","SELECT SALARY GRADE",$disabled." title='Salary Grade'");
               ?>
            </div>
            <?php 
               if (isset($settings)) {
                  $lgShowJG = $settings["show.JobGrade"];
               } else {
                  $lgShowJG = $GLOBALS["settings"]["show.JobGrade"];
               } 
            if ($lgShowJG) {
            ?>
               <div class="col-xs-2">
                  <?php
                     $sql = "SELECT * FROM `jobgrade` ORDER BY Name";
                     $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
                     if ($result) {
                        echo '<select partner="sint_SalaryGradeRefId_'.$j.'" id="sint_JobGradeRefId_'.$j.'" name="sint_JobGradeRefId_'.$j.'" class="form-input saveFields-- rptCriteria-- createSelect--">'."\n";
                        echo '<option value="0">SELECT JOB GRADE</option>'."\n";
                        while($row = mysqli_fetch_assoc($result)) {
                           if ($row["Name"] != "") {
                              echo '<option value="'.$row["RefId"].'"';
                              echo '>'.$row["Name"].'</option>'."\n";
                           }
                        }
                        echo '</select>'."\n";
                     }
                  ?>
               </div>
            <?php } ?>
            <div class="col-xs-2">
               <?php
                  createSelect("StepIncrement",
                               "sint_StepIncrementRefId_".$j,
                               "",100,"Name","SELECT STEP INCREMENT",$disabled." title='Step Increment'");
               ?>
            </div>

            <div class="col-xs-2" id="getEmpStat_<?php echo $j;?>">
               <?php
                  createSelect2("EmpStatus",
                               "sint_EmpStatusRefId_".$j,
                               "",100,"Name","SELECT EMPLOYMENT STATUS",$disabled." title='Employment Status'","WHERE IsPrivate = 0 OR IsPrivate IS NULL");
               ?>
            </div>

            <div class="col-xs-2">
               <select class="form-input saveFields--" id="PayGroup_<?php echo $j; ?>" name="char_PayGroup_<?php echo $j; ?>" <?php echo $disabled; ?>
               title='Payment Group'>
                  <option value="">SELECT PAY GROUP</option>
                  <?php
                     foreach ($_SESSION["PayGroup"] as $e) {
                        echo '<option value="'.$e.'">'.$e.'</option>';
                     }
                  ?>
               </select>
            </div>
            <div class="col-xs-2">
               <?php
                  createSelect("PayRate",
                               "sint_PayrateRefId_".$j,
                               "",100,"Name","SELECT PAY RATE",$disabled." title='Pay Rate'");
               ?>
            </div>

         </div>
         <div class="row margin-top">
            <div class="col-xs-2">
               <input type="text" class="form-input saveFields--" placeholder="LWOP"
               id="LWOP_<?php echo $j; ?>" name="char_LWOP_<?php echo $j; ?>" <?php echo $disabled; ?>
               title='Leave with out payment'>
            </div>
            <div class="col-xs-2">
               <input type="text" class="form-input date-- saveFields--" placeholder="Date Extended"
               id="ExtDate_<?php echo $j; ?>" name="date_ExtDate_<?php echo $j; ?>" <?php echo $disabled; ?>
               title='Date Extended'>
            </div>
            <div class="col-xs-2">
               <input type="text" class="form-input date-- saveFields--" placeholder="Date Separated"
               id="SeparatedDate_<?php echo $j; ?>" name="date_SeparatedDate_<?php echo $j; ?>" <?php echo $disabled; ?>
               title='Date Separation'>
            </div>
            <div class="col-xs-3">
               <input type="text" class="form-input saveFields--" placeholder="Reasons"
               id="Reason_<?php echo $j; ?>" name="char_Reason_<?php echo $j; ?>" <?php echo $disabled; ?>
               title='Reasons'>
            </div>
            <div class="col-xs-3">
               <input type="text" class="form-input saveFields--" placeholder="Remarks"
               id="Remarks_<?php echo $j; ?>" name="char_Remarks_<?php echo $j; ?>" <?php echo $disabled; ?>
               title='Remarks'>
            </div>
         </div>
         -->
      </div>
      

