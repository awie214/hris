<div class="mypanel margin-top" id="EntryChildrensInfo">
   <div class="panel-top" for="contentChildrens">
      &nbsp;
   </div>
   <div class="panel-mid-litebg" id="contentChildrens">
      <?php
         $createEntry = 0;
         if (getvalue("hUserGroup") == "COMPEMP") {
            $rs = SelectEach("employeeschild","WHERE CompanyRefId = $CompanyId AND BranchRefId = $BranchId AND EmployeesRefId = $EmployeesRefId");
            $j = 0;
            if ($rs) {
               $createEntry = mysqli_num_rows($rs);

            } else {
               $createEntry = 1;
            }
         } else {
            $createEntry = 1;
         }
         for ($j=1;$j<=$createEntry;$j++) {
      ?>
            <div id="EntryChildrens_<?php echo $j; ?>" class="entry201">
               <input type="checkbox" idx="<?php echo $j; ?>" id="childrens_<?php echo $j; ?>"
                      name="chkChild_<?php echo $j; ?>" class="enabler-- child_fpreview" refid="" 
                      fldName="char_FullName_<?php echo $j; ?>,
                      date_BirthDate_<?php echo $j; ?>,
                      char_Gender_<?php echo $j; ?>" 
                      unclick="CancelAddRow">
               <label for="childrens_<?php echo $j; ?>" class="btn-cls2-sea"><b>EDIT CHILDREN #<?php echo $j; ?></b></label>
               <!-- <button class="btn-cls2-red" 
                       onclick="deletePDS(this.id,'ChildrenRefId_','employeeschild','EntryChildrens_');" 
                       type="button"
                       id="rmvChildren_<?php echo $j; ?>">
                  REMOVE
               </button> -->
               <input type="hidden" name="ChildrenRefId_<?php echo $j; ?>" id="ChildrenRefId_<?php echo $j; ?>">
               <div class="row margin-top">
                  <div class="col-xs-6">
                     <div class="form-group">
                        <label class="control-label" for="inputs">FULL NAME</label><br>
                        <input class="form-input saveFields-- mandatory uCase--" type="text" tabname="Children"
                        id="childLastName_<?php echo $j; ?>" name="char_FullName_<?php echo $j; ?>" disabled>
                     </div>
                  </div>
                  <div class="col-xs-2">
                     <div class="form-group">
                        <label class="control-label" for="inputSFName">DATE OF BIRTH #<?php echo $j; ?>:</label><br>
                        <input type="text" class="form-input saveFields-- date-- valDate--" tabname="Children"
                        id="childBDate_<?php echo $j; ?>" name="date_BirthDate_<?php echo $j; ?>" readonly>

                     </div>
                  </div>
                  <div class="col-xs-2">
                     <div class="form-group">
                        <label class="control-label" for="inputs">SEX</label><br>
                        <?php createSelectGender("char_Gender_".$j,"","disabled") ?>
                     </div>
                  </div>
               </div>
            </div>
      <?php
         }
      ?>

   </div>
   <div class="panel-bottom bgSilver"><a href="javascript:void(0);" id="addRowChildren" class="addRow">Add Row</a></div>
</div>
<script language="javascript">
   $("[name*='char_Gender_']").addClass("mandatory");
   $("[name*='char_Gender_']").attr("tabname","Children");
</script>
