<?php
   session_start();
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require 'conn.e2e.php';
   $msg = "";

   function parseItems($dbtable,$paramName,$id) {
      $returnString = "";
      $arr = explode("»",getvalue($paramName));
      for ($h=0;$h<count($arr) - 1;$h++) {
         $returnString .= $arr[$h]."text|sint_".$dbtable."RefId|".$id."!»";
      }
      return $returnString;
   }
   $dbtable = strtolower(getvalue("dbtable"));

   $tab1_entry = getvalue("tabPersonalInfo");
   $last_insert_id = "";
   $last_insert_id = saveRecord($dbtable,$tab1_entry,false);
   if (strpos($last_insert_id,"Saving Error:") <= 0)  { // No Error
      $msg .= "Save OK > ".$dbtable;
      $msg .= "\n";
      $returnString = "";
      $entryFamilyBG = getvalue("entryFamilyBG")."text|sint_".$dbtable."RefId|".$last_insert_id."!";
      if (getvalue("entryFamilyBG") != "") {
         $returnString = saveRecord($dbtable."family",$entryFamilyBG,false);
         if ($returnString != "") {
            $msg .= $returnString;
         } else {
            $msg .= "Save OK > Family";
            $msg .= "\n";
         }
      }
      if (getvalue("entryChildrens") != "") {
         $returnString = saveRecord($dbtable."child",parseItems($dbtable,"entryChildrens",$last_insert_id),true);
         if ($returnString != "") {
            $msg .= $returnString;
         } else {
            $msg .= "Save OK > Childrens";
            $msg .= "\n";
         }
      }

      if (getvalue("entryElementary") != "") {
         $returnString = saveRecord($dbtable."educ",parseItems($dbtable,"entryElementary",$last_insert_id),true);
         if (!empty($returnString)) {
            $msg .= $returnString;
         } else {
            $msg .= "Save OK > Educ -- Elementary";
            $msg .= "\n";
         }
      }

      if (getvalue("entrySecondary") != "") {
         $returnString = saveRecord($dbtable."educ",parseItems($dbtable,"entrySecondary",$last_insert_id),true);
         if ($returnString != "") {
            $msg .= $returnString;
         } else {
            $msg .= "Save OK > Educ -- Secondary";
            $msg .= "\n";
         }
      }

      if (getvalue("entryVocational") != "") {
         $returnString = saveRecord($dbtable."educ",parseItems($dbtable,"entryVocational",$last_insert_id),true);
         if ($returnString != "") {
            $msg .= $returnString;
         } else {
            $msg .= "Save OK > Educ -- Vocational";
            $msg .= "\n";
         }
      }
      if (getvalue("entryCollege") != "") {
         $returnString = saveRecord($dbtable."educ",parseItems($dbtable,"entryCollege",$last_insert_id),true);
         if ($returnString != "") {
            $msg .= $returnString;
         } else {
            $msg .= "Save OK > Educ -- College";
            $msg .= "\n";
         }
      }

      if (getvalue("entryGrad") != "") {
         $returnString = saveRecord($dbtable."educ",parseItems($dbtable,"entryGrad",$last_insert_id),true);
         if ($returnString != "") {
            $msg .= $returnString;
         } else {
            $msg .= "Save OK > Educ -- Graduates";
            $msg .= "\n";
         }
      }

      if (getvalue("entryEligibility") != "") {
         $returnString = saveRecord($dbtable."elegibility",parseItems($dbtable,"entryEligibility",$last_insert_id),true);
         if ($returnString != "") {
            $msg .= $returnString;
         } else {
            $msg .= "Save OK > Eligibility";
            $msg .= "\n";
         }
      }

      if (getvalue("entryWorkExp") != "") {
         $returnString = saveRecord($dbtable."workexperience",parseItems($dbtable,"entryWorkExp",$last_insert_id),true);
         if ($returnString != "") {
            $msg .= $returnString;
         } else {
            $msg .= "Save OK > Work Experience";
            $msg .= "\n";
         }
      }

      if (getvalue("entryVolWork") != "") {
         $returnString = saveRecord($dbtable."voluntary",parseItems($dbtable,"entryVolWork",$last_insert_id),true);
         if ($returnString != "") {
            $msg .= $returnString;
         } else {
            $msg .= "Save OK > Voluntary Works";
            $msg .= "\n";
         }
      }

      if (getvalue("entryTrainProg") != "") {
         $returnString = saveRecord($dbtable."training",parseItems($dbtable,"entryTrainProg",$last_insert_id),true);
         if ($returnString != "") {
            $msg .= $returnString;
         } else {
            $msg .= "Save OK > Training";
            $msg .= "\n";
         }
      }

      if (getvalue("entryOtherInfo") != "") {
         $returnString = saveRecord($dbtable."otherinfo",parseItems($dbtable,"entryOtherInfo",$last_insert_id),true);
         if ($returnString != "") {
            $msg .= $returnString;
         } else {
            $msg .= "Save OK > Other Informations";
            $msg .= "\n";
         }
      }

      if (getvalue("entryPDSQ") != "") {
         $entryPDSQ = getvalue("entryPDSQ")."text|sint_".$dbtable."RefId|".$last_insert_id."!";
         $returnString = saveRecord($dbtable."pdsq",$entryPDSQ,false);
         if ($returnString != "") {
            $msg .= $returnString;
         } else {
            $msg .= "Save OK > PDSQ";
            $msg .= "\n";
         }
      }

      if (getvalue("entryReference") != "") {
         $returnString = saveRecord($dbtable."reference",parseItems($dbtable,"entryReference",$last_insert_id),true);
         if ($returnString != "") {
            $msg .= $returnString;
         } else {
            $msg .= "Save OK > References";
            $msg .= "\n";
         }
      }
   } else {
      $msg .= $last_insert_id." Err... No PDSID --";
      $msg .= "\n";
   }
   echo trim($msg);
   mysqli_close($conn);

?>