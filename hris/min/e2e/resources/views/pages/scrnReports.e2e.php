<?php
   $incRptScrn="";
   $incRptScrn=getvalue("hRptScrn");
?>
<!DOCTYPE html>
<html>
   <head>

      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style>
            option.optHeader {
               background-color:#444444;
               color:#ffffff;
               font-size:11pt;
               font-weight:600;
            }
            option {
               padding-left:20px;
            }
      </style>
      <link rel="stylesheet" href="<?php echo path("js/autocomplete/css/jquery-ui.css"); ?>" type="text/css" />
      <script type="text/javascript" src="<?php echo path("js/autocomplete/jquery-ui.js") ?>"></script>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <div class="row panel-top" style="padding-left:0px;border-radius:0;">
               <div class="col-xs-6">
                  <a href="javascript:void(0)" class="mbar" id="titleBarIcons" onclick="openNav();">
                     <i class="fa fa-chevron-left" aria-hidden="true"></i>
                  </a>&nbsp;
                  <?php echo strtoupper($modTitle); ?>
               </div>
               <div class="col-xs-5">
                  Today is <span id="TimeDate"></span>
               </div>
               <div class="col-xs-1 txt-right">
                  <?php if (getvalue("hRptScrn") != "") { ?>
                     <button type="button" class="close" aria-label="Close" onclick="closeSCRN('model');">
                        <span aria-hidden="true" style="color:white;">&times;</span>
                     </button>
                  <?php } ?>
                  <a href="#" id="mHome" style="color:white;">
                     <i class="fa fa-home" aria-hidden="true" title="Home/Exit Module"></i>
                  </a>
               </div>
            </div>
            <div class="container-fluid margin-top10" id="rptCriteria">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div class="mypanel" id="ReportHead">
                        <div class="panel-top">Report Type</div>
                        <div class="panel-mid-litebg">
                           <label>Choose Kind Of Report:</label>
                           <p>
                              <select class="form-input rptCriteria-- " id="ReportKind" sys="pis" name="drpReportKind" style="width:50%;font-family:12pt;">
                                 <option value="XX" disabled class="optHeader">COMPANY REPORTS / FORM</option>
                                 <?php
                                    $crit = "WHERE `CompanyRefId` = $CompanyId ";
                                    $crit .= "AND `BranchRefId` = $BranchId ";
                                    /*$crit .= "AND `CompanyCode` = '$CompanyCode' ";*/
                                    $crit .= "AND `SystemRefId` = '1'";
                                    $rs = SelectEach("reports",$crit);
                                    $htm = "yes";
                                    if ($rs) {
                                       while ($r = mysqli_fetch_array($rs)) {
                                          echo
                                          '<option for="'.$r["For"].'" ext="'.$r["ExtType"].'" own="'.$r["Owner"].'" value="'.$r["Filename"].'" rptname="'.$r["Name"].'">'.$r["Name"].'</option>'."\n";
                                       }
                                    }

                                 ?>
                                 <option value="XX" disabled class="optHeader">DEFAULT REPORTS / FORM</option>
                                 <?php
                                    $crit = "WHERE `CompanyRefId` = 0 ";
                                    $crit .= "AND `BranchRefId` = 0 ";
                                    $crit .= "AND `SystemRefId` = '1'";
                                    $rs = SelectEach("reports",$crit);
                                    $htm = "yes";
                                    if ($rs) {
                                       while ($r = mysqli_fetch_array($rs)) {
                                          echo
                                          '<option for="'.$r["For"].'" ext="'.$r["ExtType"].'" own="'.$r["Owner"].'" value="'.$r["Filename"].'" rptname="'.$r["Name"].'">'.$r["Name"].'</option>'."\n";
                                       }
                                    }
                                    //echo $crit;
                                 ?>
                              </select>
                           </p>
                           <?php
                              bar();
                              echo "<h5><label>Search Criteria</label></h5>";
                              require_once "incEmpSearchCriteria.e2e.php";
                              bar();
                           ?>
                           <div class="row">
                              <div class="col-xs-2">
                                 <p>
                                    <label>For The Year of:</label>
                                    <select name="txtAttendanceYear" class="form-input rptCriteria--">
                                       <?php
                                          $backyear  = date("Y",time());
                                          for ($i=0; $i <= 3 ; $i++) { 
                                             echo '<option value="'.($backyear-$i).'">'.($backyear-$i).'</option>';
                                          }
                                       ?>
                                    </select>
                                 </p>
                              </div>
                              <div class="col-xs-2">
                                 <p>
                                    <label>For The Month of:</label><br>
                                    <select name="txtAttendanceMonth" class="form-input rptCriteria--">
                                       <?php
                                          $arr_month =[
                                                        "January",
                                                        "February",
                                                        "March",
                                                        "April",
                                                        "May",
                                                        "June",
                                                        "July",
                                                        "August",
                                                        "September",
                                                        "October",
                                                        "November",
                                                        "December"
                                                      ];
                                          $curr_month = date("F",time());
                                          foreach ($arr_month as $key => $value) {
                                             $idx = $key+1;
                                             if ($idx <= 9) $idx = "0".$idx;
                                             if ($curr_month == $value) {
                                                $selected = "selected";
                                             } else {
                                                $selected = "";
                                             }
                                             echo '<option value="'.$idx.'" '.$selected.'>'.$value.'</option>';
                                          }
                                       ?>
                                    </select>
                                 </p>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-xs-12" id="RptMainHolder">
                              </div>
                           </div>
                        </div>
                        <div class="panel-bottom"></div>
                     </div>
                  </div>
               </div>
               <?php spacer(10)?>
               <div>
                  <div style="text-align:center;">
                     <button type="button"
                          class="btn-cls4-sea trnbtn"
                          id="btnGENERATE" name="btnGENERATE">
                        <i class="fa fa-file" aria-hidden="true"></i>
                        &nbsp;GENERATE REPORT
                     </button>
                     <button type="button"
                          class="btn-cls4-red trnbtn"
                          id="btnEXIT" name="btnEXIT">
                        <i class="fa fa-times" aria-hidden="true"></i>
                        &nbsp;EXIT
                     </button>
                  </div>
               </div>
               <!-- Modal -->
               <div class="modal fade" id="prnModal" role="dialog">
                  <div class="modal-dialog" style="height:90%;width:80%">
                     <div class="mypanel" style="height:100%;">
                        <div class="panel-top bgSilver">
                           <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                              <i class="fa fa-print" aria-hidden="true"></i>
                           </a>
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                     </div>
                  </div>
               </div>
               <?php modalEmpLkUp(); ?>
            </div>
            <?php
               footer();
               include "varHidden.e2e.php";
            ?>
            <input type="hidden" id="hRptFile" value="<?php echo getvalue("hRptFile")?>">
         </div>
      </form>
   </body>
   <script type="text/javascript">
      $(document).ready(function () {
         EmployeeAutoComplete("employees","signatory");
         EmployeeAutoComplete("employees","prepared_by");
         EmployeeAutoComplete("employees","corrected_by");
         $("#signatory").blur(function () {
            var value = $(this).val();
            arr = value.split("-");
            $("#signatory_refid").val(arr[0]);
            $(this).val(arr[1]);
         });
         $("#prepared_by").blur(function () {
            var value = $(this).val();
            arr = value.split("-");
            $("#prepared_by_refid").val(arr[0]);
            $(this).val(arr[1]);
         });
         $("#corrected_by").blur(function () {
            var value = $(this).val();
            arr = value.split("-");
            $("#corrected_by_refid").val(arr[0]);
            $(this).val(arr[1]);
         });
      });
   </script>
</html>