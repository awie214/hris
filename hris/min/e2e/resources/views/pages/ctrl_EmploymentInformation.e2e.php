<?php
   session_start ();
   require_once "conn.e2e.php";
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   $table = "empinformation";
   $task     = getvalue("t");
   $emprefid = getvalue("emprefid");
   $_SESSION["SelectedEMP"] = $emprefid;   

   if ($task == "viewRecord") {
      $criteria  = " WHERE EmployeesRefId = $emprefid";
      $result = FindFirst($table,$criteria,"*");
      if ($result) {
         $criteria  = " WHERE RefId = $emprefid";
         $rs_Emp = FindFirst("employees",$criteria,"LastName, FirstName, Inactive");
         $result["SalaryAmount"] = number_format($result["SalaryAmount"],2);
         echo json_encode(array_merge($rs_Emp,$result));
      } else {
         echo false;
      }
   }
   $conn->close();
?>