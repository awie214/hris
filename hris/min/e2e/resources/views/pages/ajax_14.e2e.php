<?php
	error_reporting(E_ALL);
   	ini_set('display_errors', 1);
	session_start();
	include_once "constant.e2e.php";
	include_once pathClass.'0620functions.e2e.php';
	$user = getvalue("hUser");

	$funcname = getvalue("fn");
	$params   = getvalue("params");
	if (!empty($funcname)) {
   	$funcname($params);
	} else {
   	echo 'alert("Error... No Function defined");';
	}
   function getPM() {
      $refid   = getvalue("refid");
      $row     = FindFirst("spms_performance","WHERE RefId = '$refid'","*");
      if ($ipcr_row) {
         echo '$("#sint_Quarter").val("'.$row["Quarter"].'");'."\n";
         echo '$("#sint_Year").val("'.$row["Year"].'");'."\n";
         echo '$("#sint_OfficeRefId").val("'.$row["OfficeRefId"].'");'."\n";
      }
   }
	function getIPCR() {
   	$refid 	= getvalue("refid");
   	$ipcr_row = FindFirst("spms_ipcr","WHERE RefId = '$refid'","*");
   	if ($ipcr_row) {
   		echo '$("#sint_Semester").val("'.$ipcr_row["Semester"].'");'."\n";
   		echo '$("#sint_Year").val("'.$ipcr_row["Year"].'");'."\n";
   		echo '$("#Supervisor_Rating").val("'.$ipcr_row["Supervisor_Rating"].'");'."\n";
   		echo '$("#Final_Rating").val("'.$ipcr_row["Final_Rating"].'");'."\n";
   	} 
	}
	function getOPCR() {
	   	$refid 	= getvalue("refid");
	   	$ipcr_row = FindFirst("spms_opcr","WHERE RefId = '$refid'","*");
	   	if ($ipcr_row) {
	   		echo '$("#sint_Semester").val("'.$ipcr_row["Semester"].'");'."\n";
	   		echo '$("#sint_Year").val("'.$ipcr_row["Year"].'");'."\n";
	   		echo '$("#Adjectival_Rating").val("'.$ipcr_row["Adjectival_Rating"].'");'."\n";
	   		echo '$("#Final_Rating").val("'.$ipcr_row["Final_Rating"].'");'."\n";
	   		echo '$("#Overall_Rating").val("'.$ipcr_row["Overall_Rating"].'");'."\n";
	   		echo '$("#sint_OfficeRefId").val("'.$ipcr_row["OfficeRefId"].'");'."\n";
	   	} 
	}

   function selectRating($value) {
      $str = '';
      switch ($value) {
         case '0':
            $str .= '<option value="0" selected>0</option>';
            $str .= '<option value="1">1</option>';
            $str .= '<option value="2">2</option>';
            $str .= '<option value="3">3</option>';
            $str .= '<option value="4">4</option>';
            $str .= '<option value="5">5</option>';
            break;
         case '1':
            $str .= '<option value="0">0</option>';
            $str .= '<option value="1" selected>1</option>';
            $str .= '<option value="2">2</option>';
            $str .= '<option value="3">3</option>';
            $str .= '<option value="4">4</option>';
            $str .= '<option value="5">5</option>';
            break;
         case '2':
            $str .= '<option value="0">0</option>';
            $str .= '<option value="1">1</option>';
            $str .= '<option value="2" selected>2</option>';
            $str .= '<option value="3">3</option>';
            $str .= '<option value="4">4</option>';
            $str .= '<option value="5">5</option>';
            break;
         case '3':
            $str .= '<option value="0">0</option>';
            $str .= '<option value="1">1</option>';
            $str .= '<option value="2">2</option>';
            $str .= '<option value="3" selected>3</option>';
            $str .= '<option value="4">4</option>';
            $str .= '<option value="5">5</option>';
            break;
         case '4':
            $str .= '<option value="0">0</option>';
            $str .= '<option value="1">1</option>';
            $str .= '<option value="2">2</option>';
            $str .= '<option value="3">3</option>';
            $str .= '<option value="4" selected>4</option>';
            $str .= '<option value="5">5</option>';
            break;
         case '5':
            $str .= '<option value="0">0</option>';
            $str .= '<option value="1">1</option>';
            $str .= '<option value="2">2</option>';
            $str .= '<option value="3">3</option>';
            $str .= '<option value="4">4</option>';
            $str .= '<option value="5" selected>5</option>';
            break;
      }
      return $str;
   }

   function getPM_detail() {
      $refid   = getvalue("refid");
      $rs      = SelectEach("performance_details","WHERE performance_id = '$refid'");
      if ($rs) {
         $count   = 0;
         $str     = "";
         while ($row = mysqli_fetch_assoc($rs)) {
            $count++;
            $type       = $row["type"];
            $activity   = $row["activity"];
            $oneonone   = $row["oneonone"];
            $memo       = $row["memo"];
            $others     = $row["others"];
            $remarks    = $row["remarks"];
            $group      = $row["group"];
            $str .= '
               <div id="EntryPM_'.$count.'" class="entry201">
                  <div class="row margin-top">
                     <div class="col-xs-6">
                        <label>Performance Monitoring #'.$count.' </label>
                        <input type="hidden" name="refid_'.$count.'" id="refid_'.$count.'" value="'.$row["RefId"].'">
                     </div>
                  </div>
                  <?php bar(); ?>
                  <div class="row margin-top">
                     <div class="col-xs-6">
                        <label>Type:</label>
                        <select class="form-input" name="type_'.$count.'"
                                id="type_'.$count.'">';
                                 if ($type == "Monitoring") {
                                    $str .= '
                                       <option value="Monitoring" selected>Monitoring</option>
                                       <option value="Coaching">Coaching</option>
                                    ';
                                 } else {
                                    $str .= '
                                       <option value="Monitoring">Monitoring</option>
                                       <option value="Coaching" selected>Coaching</option>
                                    ';
                                 }
            $str .= '
                        </select>
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-6">
                        <label>Activity</label>
                        <textarea class="form-input" rows="4" name="activity_'.$count.'" id="activity_'.$count.'">'.htmlentities($activity).'</textarea>
                     </div>
                     <div class="col-xs-6">
                        <label>One-in-One</label>
                        <textarea class="form-input" rows="4" name="oneonone_'.$count.'" id="oneonone_'.$count.'">'.htmlentities($oneonone).'</textarea>
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-6">
                        <label>Group</label>
                        <textarea class="form-input" rows="4" name="group_'.$count.'" id="group_'.$count.'">'.htmlentities($group).'</textarea>
                     </div>
                     <div class="col-xs-6">
                        <label>Memo</label>
                        <textarea class="form-input" rows="4" name="memo_'.$count.'" id="memo_'.$count.'">'.htmlentities($memo).'</textarea>
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-6">
                        <label>Others (Pls. Specify)</label>
                        <textarea class="form-input" rows="4" name="others_'.$count.'" id="others_'.$count.'">'.htmlentities($others).'</textarea>
                     </div>
                     <div class="col-xs-6">
                        <label>REMARKS</label>
                        <textarea class="form-input" rows="4" name="remarks_'.$count.'" id="remarks_'.$count.'">'.htmlentities($remarks).'</textarea>
                     </div>
                  </div>
               </div>
               <br>
            ';
         }
         echo $str;
      }
   }
	function getOPCR_detail() {
	   	$refid 	= getvalue("refid");
	   	$rs  	= SelectEach("opcr_details","WHERE spms_opcr_id = '$refid'");
	   	if ($rs) {
	   		$count = 0;
	   		$str   = "";
	   		while ($row = mysqli_fetch_assoc($rs)) {
	   			$count++;
	   			$str .= '
	   				<div id="EntryIPCR_'.$count.'" class="entry201">
                       <div class="row margin-top">
                          <div class="col-xs-6">
                             <label>OPCR #'.$count.' </label>
                             <input type="hidden" name="refid_'.$count.'" id="refid_'.$count.'" value="'.$row["RefId"].'">
                          </div>
                       </div>
                       <?php bar(); ?>
                       <div class="row margin-top">
                          <div class="col-xs-6">
                             <label>OUTPUT</label>
                             <textarea class="form-input" rows="4" name="output_'.$count.'" id="output_'.$count.'">'.$row["output"].'</textarea>
                          </div>
                          <div class="col-xs-6">
                             <label>SUCCESS INDICATORS (TARGETS+MEASURES)</label>
                             <textarea class="form-input" rows="4" name="si_'.$count.'" id="si_'.$count.'">'.htmlentities($row["success_indicator"]).'</textarea>
                          </div>
                       </div>
                       <div class="row margin-top">
                         <div class="col-xs-6">
                            <label>ALLOTTED BUDGET</label>
                            <textarea class="form-input" rows="4" name="budget_'.$count.'" id="budget_'.$count.'">'.htmlentities($row["allotted_budget"]).'</textarea>
                         </div>
                         <div class="col-xs-6">
                            <label>Division/Individuals Accountable</label>
                            <textarea class="form-input" rows="4" name="accountable_'.$count.'" id="accountable_'.$count.'">'.htmlentities($row["accountable"]).'</textarea>
                         </div>
                      </div>
                       <div class="row margin-top">
                          <div class="col-xs-6">
                             <label>ACTUAL ACCOMPLISHMENTS</label>
                             <textarea class="form-input" rows="6" name="accomplishment_'.$count.'" id="accomplishment_'.$count.'">'.htmlentities($row["accomplishment"]).'</textarea>
                          </div>
                          <div class="col-xs-3">
                             <div class="row">
                                <div class="col-xs-4"></div>
                                <div class="col-xs-8 text-center">
                                   <label>Rating</label>
                                </div>
                             </div>
                             <div class="row margin-top">
                                <div class="col-xs-4 text-center">
                                   <label>Q<sup>1</sup></label>
                                </div>
                                <div class="col-xs-8">
                                   <input type="text" class="form-input" name="q1_'.$count.'" id="q1_'.$count.'" value="'.$row["q1"].'">
                                </div>
                             </div>
                             <div class="row margin-top">
                                <div class="col-xs-4 text-center">
                                   <label>E<sup>2</sup></label>
                                </div>
                                <div class="col-xs-8">
                                   <input type="text" class="form-input" name="e2_'.$count.'" id="e2_'.$count.'" value="'.$row["e2"].'">
                                </div>
                             </div>
                             <div class="row margin-top">
                                <div class="col-xs-4 text-center">
                                   <label>T<sup>3</sup></label>
                                </div>
                                <div class="col-xs-8">
                                   <input type="text" class="form-input" name="t3_'.$count.'" id="t3_'.$count.'" value="'.$row["t3"].'">
                                </div>
                             </div>
                             <div class="row margin-top">
                                <div class="col-xs-4 text-center">
                                   <label>A<sup>4</sup></label>
                                </div>
                                <div class="col-xs-8">
                                   <input type="text" class="form-input" name="a4_'.$count.'" id="a4_'.$count.'" value="'.$row["a4"].'">
                                </div>
                             </div>
                          </div>
                       </div>
                       <div class="row margin-top">
                          <div class="col-xs-6">
                             <label>REMARKS</label>
                             <textarea class="form-input" rows="5" name="remarks_'.$count.'" id="remarks_'.$count.'">'.htmlentities($row["remarks"]).'</textarea>
                          </div>
                       </div>
                    </div>
                    <br>
	   			';
	   		}
	   	}
	   	echo $str;
	}
	function getIPCR_detail() {
	   	$refid 	= getvalue("refid");
	   	$rs  	= SelectEach("ipcr_details","WHERE spms_ipcr_id = '$refid'");
	   	if ($rs) {
	   		$count = 0;
	   		$str   = "";
	   		while ($row = mysqli_fetch_assoc($rs)) {
	   			$count++;
               $refid               = $row["RefId"];
               $type                = $row["type"];
               $type                = $row["type"];
               $output              = $row["output"];
               $success_indicator   = $row["success_indicator"];
               $accomplishment      = $row["accomplishment"];
               $quality             = $row["quality"];
               $effectiveness       = $row["effectiveness"];
               $timeliness          = $row["timeliness"];
               $average             = $row["average"];
               $q1                  = $row["q1"];
               $q2                  = $row["q2"];
               $q3                  = $row["q3"];
               $q4                  = $row["q4"];
               $q5                  = $row["q5"];
               $e1                  = $row["e1"];
               $e2                  = $row["e2"];
               $e3                  = $row["e3"];
               $e4                  = $row["e4"];
               $e5                  = $row["e5"];
               $t1                  = $row["t1"];
               $t2                  = $row["t2"];
               $t3                  = $row["t3"];
               $t4                  = $row["t4"];
               $t5                  = $row["t5"];
               $str .= '
                  <div id="EntryIPCR_'.$count.'" class="entry201">
                     <div class="row margin-top">
                        <div class="col-xs-6">
                           <label>#'.$count.' </label>
                           <input type="hidden" name="refid_'.$count.'" id="refid_'.$count.'" value="'.$refid.'">
                        </div>
                     </div>
                     <?php bar(); ?>
                     <div class="row margin-top">
                        <div class="col-xs-4">
                           <label>Select Type:</label>
                           <select class="form-input" name="type_'.$count.'" id="type_'.$count.'" value="'.$type.'">
                              <option value="Core Function">Core Function</option>
                              <option value="Strategic Function">Strategic Function</option>
                              <option value="Support Function">Support Function</option>
                              <option value="Other">Other</option>
                           </select>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           <label>OUTPUT</label>
                           <textarea class="form-input" rows="4" name="output_'.$count.'" id="output_'.$count.'">'.$output.'</textarea>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           <label>SUCCESS INDICATORS (TARGETS+MEASURES)</label>
                           <textarea class="form-input" rows="4" name="si_'.$count.'" id="si_'.$count.'">'.$success_indicator.'</textarea>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-3">
                           <label>Quality:</label>
                           <select class="form-input" name="quality_'.$count.'" id="quality_'.$count.'">
                           '.selectRating($quality).'
                           </select>
                        </div>
                        <div class="col-xs-3">
                           <label>Effectiveness:</label>
                           <select class="form-input" name="effectiveness_'.$count.'" id="effectiveness_'.$count.'">
                           '.selectRating($effectiveness).'
                           </select>
                        </div>
                        <div class="col-xs-3">
                           <label>Timeliness:</label>
                           <select class="form-input" name="timeliness_'.$count.'" id="timeliness_'.$count.'">
                           '.selectRating($timeliness).'
                           </select>
                        </div>
                        <div class="col-xs-3">
                           <label>Average:</label>
                           <input type="text" class="form-input" name="average_'.$count.'" id="average_'.$count.'" value="'.$average.'">
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           <label>Actual Accomplishments</label>
                           <textarea class="form-input" rows="4" name="accomplishment_'.$count.'" id="accomplishment_'.$count.'">'.$accomplishment.'</textarea>
                        </div>
                     </div>
                     <br>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           <label>Quality:</label>
                        </div>
                     </div>
                     <?php bar(); ?>
                     <div class="row margin-top">
                        <div class="col-xs-2">
                           <label>Rating 1:</label>
                           <textarea class="form-input" rows="2" name="q1_'.$count.'" id="q1_'.$count.'">'.$q1.'</textarea>
                        </div>
                        <div class="col-xs-2">
                           <label>Rating 2:</label>
                           <textarea class="form-input" rows="2" name="q2_'.$count.'" id="q2_'.$count.'">'.$q2.'</textarea>
                        </div>
                        <div class="col-xs-2">
                           <label>Rating 3:</label>
                           <textarea class="form-input" rows="2" name="q3_'.$count.'" id="q3_'.$count.'">'.$q3.'</textarea>
                        </div>
                        <div class="col-xs-2">
                           <label>Rating 4:</label>
                           <textarea class="form-input" rows="2" name="q4_'.$count.'" id="q4_'.$count.'">'.$q4.'</textarea>
                        </div>
                        <div class="col-xs-2">
                           <label>Rating 5:</label>
                           <textarea class="form-input" rows="2" name="q5_'.$count.'" id="q5_'.$count.'">'.$q5.'</textarea>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           <label>Effectiveness</label>
                        </div>
                     </div>
                     <?php bar(); ?>
                     <div class="row margin-top">
                        <div class="col-xs-2">
                           <label>Rating 1:</label>
                           <textarea class="form-input" rows="2" name="e1_'.$count.'" id="e1_'.$count.'">'.$e1.'</textarea>
                        </div>
                        <div class="col-xs-2">
                           <label>Rating 2:</label>
                           <textarea class="form-input" rows="2" name="e2_'.$count.'" id="e2_'.$count.'">'.$e2.'</textarea>
                        </div>
                        <div class="col-xs-2">
                           <label>Rating 3:</label>
                           <textarea class="form-input" rows="2" name="e3_'.$count.'" id="e3_'.$count.'">'.$e3.'</textarea>
                        </div>
                        <div class="col-xs-2">
                           <label>Rating 4:</label>
                           <textarea class="form-input" rows="2" name="e4_'.$count.'" id="e4_'.$count.'">'.$e4.'</textarea>
                        </div>
                        <div class="col-xs-2">
                           <label>Rating 5:</label>
                           <textarea class="form-input" rows="2" name="e5_'.$count.'" id="e5_'.$count.'">'.$e5.'</textarea>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           <label>Timeliness:</label>
                        </div>
                     </div>
                     <?php bar(); ?>
                     <div class="row margin-top">
                        <div class="col-xs-2">
                           <label>Rating 1:</label>
                           <textarea class="form-input" rows="2" name="t1_'.$count.'" id="t1_'.$count.'">'.$t1.'</textarea>
                        </div>
                        <div class="col-xs-2">
                           <label>Rating 2:</label>
                           <textarea class="form-input" rows="2" name="t2_'.$count.'" id="t2_'.$count.'">'.$t2.'</textarea>
                        </div>
                        <div class="col-xs-2">
                           <label>Rating 3:</label>
                           <textarea class="form-input" rows="2" name="t3_'.$count.'" id="t3_'.$count.'">'.$t3.'</textarea>
                        </div>
                        <div class="col-xs-2">
                           <label>Rating 4:</label>
                           <textarea class="form-input" rows="2" name="t4_'.$count.'" id="t4_'.$count.'">'.$t4.'</textarea>
                        </div>
                        <div class="col-xs-2">
                           <label>Rating 5:</label>
                           <textarea class="form-input" rows="2" name="t5_'.$count.'" id="t5_'.$count.'">'.$t5.'</textarea>
                        </div>
                     </div>
                  </div>
                  <br>
               ';
	   			/*$str .= '
	   				<div id="EntryIPCR_'.$count.'" class="entry201">
                       <div class="row margin-top">
                          <div class="col-xs-6">
                             <label>IPCR #'.$count.' </label>
                             <input type="hidden" name="refid_'.$count.'" id="refid_'.$count.'" value="'.$row["RefId"].'">
                          </div>
                       </div>
                       <?php bar(); ?>
                       <div class="row margin-top">
                          <div class="col-xs-6">
                             <label>OUTPUT</label>
                             <textarea class="form-input" rows="4" name="output_'.$count.'" id="output_'.$count.'">'.$row["output"].'</textarea>
                          </div>
                          <div class="col-xs-6">
                             <label>SUCCESS INDICATORS (TARGETS+MEASURES)</label>
                             <textarea class="form-input" rows="4" name="si_'.$count.'" id="si_'.$count.'">'.htmlentities($row["success_indicator"]).'</textarea>
                          </div>
                       </div>
                       <div class="row margin-top">
                          <div class="col-xs-6">
                             <label>ACTUAL ACCOMPLISHMENTS</label>
                             <textarea class="form-input" rows="6" name="accomplishment_'.$count.'" id="accomplishment_'.$count.'">'.htmlentities($row["accomplishment"]).'</textarea>
                          </div>
                          <div class="col-xs-3">
                             <div class="row">
                                <div class="col-xs-4"></div>
                                <div class="col-xs-8 text-center">
                                   <label>Employee Rating</label>
                                </div>
                             </div>
                             <div class="row margin-top">
                                <div class="col-xs-4 text-center">
                                   <label>Q<sup>1</sup></label>
                                </div>
                                <div class="col-xs-8">
                                   <input type="text" class="form-input" name="q1_'.$count.'" id="q1_'.$count.'" value="'.$row["q1"].'">
                                </div>
                             </div>
                             <div class="row margin-top">
                                <div class="col-xs-4 text-center">
                                   <label>E<sup>2</sup></label>
                                </div>
                                <div class="col-xs-8">
                                   <input type="text" class="form-input" name="e2_'.$count.'" id="e2_'.$count.'" value="'.$row["e2"].'">
                                </div>
                             </div>
                             <div class="row margin-top">
                                <div class="col-xs-4 text-center">
                                   <label>T<sup>3</sup></label>
                                </div>
                                <div class="col-xs-8">
                                   <input type="text" class="form-input" name="t3_'.$count.'" id="t3_'.$count.'" value="'.$row["t3"].'">
                                </div>
                             </div>
                             <div class="row margin-top">
                                <div class="col-xs-4 text-center">
                                   <label>A<sup>4</sup></label>
                                </div>
                                <div class="col-xs-8">
                                   <input type="text" class="form-input" name="a4_'.$count.'" id="a4_'.$count.'" value="'.$row["a4"].'">
                                </div>
                             </div>
                          </div>
                          <div class="col-xs-3">
                             <div class="row">
                                <div class="col-xs-4"></div>
                                <div class="col-xs-8 text-center">
                                   <label>Supervisor Rating</label>
                                </div>
                             </div>
                             <div class="row margin-top">
                                <div class="col-xs-4 text-center">
                                   <label>Q<sup>1</sup></label>
                                </div>
                                <div class="col-xs-8">
                                   <input type="text" class="form-input" name="'.$count.'" id="'.$count.'">
                                </div>
                             </div>
                             <div class="row margin-top">
                                <div class="col-xs-4 text-center">
                                   <label>E<sup>2</sup></label>
                                </div>
                                <div class="col-xs-8">
                                   <input type="text" class="form-input" name="'.$count.'" id="'.$count.'">
                                </div>
                             </div>
                             <div class="row margin-top">
                                <div class="col-xs-4 text-center">
                                   <label>T<sup>3</sup></label>
                                </div>
                                <div class="col-xs-8">
                                   <input type="text" class="form-input" name="'.$count.'" id="'.$count.'">
                                </div>
                             </div>
                             <div class="row margin-top">
                                <div class="col-xs-4 text-center">
                                   <label>A<sup>4</sup></label>
                                </div>
                                <div class="col-xs-8">
                                   <input type="text" class="form-input" name="'.$count.'" id="'.$count.'">
                                </div>
                             </div>
                          </div>
                       </div>
                       <div class="row margin-top">
                          <div class="col-xs-6">
                             <label>REMARKS</label>
                             <textarea class="form-input" rows="5" name="remarks_'.$count.'" id="remarks_'.$count.'">'.htmlentities($row["remarks"]).'</textarea>
                          </div>
                       </div>
                    </div>
                    <br>
	   			';*/
	   		}
	   	}
	   	echo $str;
	}

   function savePM() {
      $emprefid   = getvalue("hUserRefId");
      $office     = getvalue("sint_OfficeRefId");
      $quarter    = getvalue("sint_Quarter");
      $year       = getvalue("sint_Year");
      $count      = getvalue("count");
      $pm_refid   = getvalue("sint_PMRefId");
      $error      = 0;
      $table      = "spms_performance";
      $table2     = "performance_details";
      if (intval($pm_refid) > 0) {
         $fldnval = "OfficeRefId = '$office',";
         $fldnval .= "Quarter = '$quarter',";
         $fldnval .= "Year = '$year',";
         $update  = f_SaveRecord("EDITSAVE",$table,$fldnval,$pm_refid);
         if ($update == "") {
            for ($x=1; $x <= $count; $x++) { 
               $refid = $_POST["refid_".$x];
               if (intval($refid) > 0) {
                  $type       = realEscape($_POST["type_".$x]);
                  $activity   = realEscape($_POST["activity_".$x]);
                  $oneonone   = realEscape($_POST["oneonone_".$x]);
                  $memo       = realEscape($_POST["memo_".$x]);
                  $others     = realEscape($_POST["others_".$x]);
                  $remarks    = realEscape($_POST["remarks_".$x]);
                  $group      = realEscape($_POST["group_".$x]);
                  if ($activity != "") {
                     $new_fldnval = "type = '$type',";   
                     $new_fldnval .= "activity = '$activity',";
                     $new_fldnval .= "oneonone = '$oneonone',";
                     $new_fldnval .= "memo = '$memo',";
                     $new_fldnval .= "`group` = '$group',";
                     $new_fldnval .= "`others` = '$others',";
                     $new_fldnval .= "`remarks` = '$remarks',";
                     $new_update = f_SaveRecord("EDITSAVE",$table2,$new_fldnval,$refid);
                     if ($new_update != "") {
                        $error++;
                        echo 'alert("Error in Updating Details.");';
                        return false;
                     }
                  }
                  
               } else {
                  $new_fld = "`performance_id`,`type`,`activity`,`oneonone`,`memo`,`group`,`others`,`remarks`,";
                  $type       = realEscape($_POST["type_".$x]);
                  $activity   = realEscape($_POST["activity_".$x]);
                  $oneonone   = realEscape($_POST["oneonone_".$x]);
                  $memo       = realEscape($_POST["memo_".$x]);
                  $others     = realEscape($_POST["others_".$x]);
                  $remarks    = realEscape($_POST["remarks_".$x]);
                  $group      = realEscape($_POST["group_".$x]);
                  if ($activity != "") {
                     $new_val = "'$pm_refid', '$type', '$activity', '$oneonone', '$memo', '$group',";
                     $new_val .= "'$others', '$remarks',";
                     $save    = f_SaveRecord("NEWSAVE",$table2,$new_fld,$new_val);
                     if (!is_numeric($save)) {
                        $error++;
                        echo 'alert("Error in Saving Details");';
                        return false;
                     }
                  }
               }
               
            }
         } else {
            $error++;
            echo 'alert("Error in Updating");';
            return false;
         }
         if ($error == 0) {
            echo 'afterEdit();';
         }
      } else {
         $fld = "EmployeesRefId, OfficeRefId, Quarter, Year,";
         $val = "'$emprefid', '$office', '$quarter', '$year', ";
         $new_refid = f_SaveRecord("NEWSAVE",$table,$fld,$val);
         if (is_numeric($new_refid)) {
            for ($a=1; $a <= $count; $a++) { 
               $new_fld = "`performance_id`,`type`,`activity`,`oneonone`,`memo`,`group`,`others`,`remarks`,";
               $type       = realEscape($_POST["type_".$a]);
               $activity   = realEscape($_POST["activity_".$a]);
               $oneonone   = realEscape($_POST["oneonone_".$a]);
               $memo       = realEscape($_POST["memo_".$a]);
               $others     = realEscape($_POST["others_".$a]);
               $remarks    = realEscape($_POST["remarks_".$a]);
               $group      = realEscape($_POST["group_".$a]);
               if ($activity != "") {
                  $new_val = "'$new_refid', '$type', '$activity', '$oneonone', '$memo', '$group',";
                  $new_val .= "'$others', '$remarks',";
                  $save    = f_SaveRecord("NEWSAVE",$table2,$new_fld,$new_val);
                  if (!is_numeric($save)) {
                     $error++;
                     echo 'alert("Error in Saving Details");';
                     return false;
                  }
               }
            }
         } else {
            $error++;
            echo 'alert("Error in Saving");';
            return false;
         }
         if ($error == 0) {
            echo 'afterSave();';
         }
      }
   }
	function saveIPCR(){
	   	$emprefid 			= getvalue("sint_EmployeesRefId");
	   	$Division 			= FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","DivisionRefId");
	   	$Division 			= intval($Division);
	   	$count 				= getvalue("count");
	   	$quarter  			= getvalue("sint_Semester");
	   	$year 	 			= getvalue("sint_Year");
	   	$Final_Rating 		= getvalue("Final_Rating");
	   	$Supervisor_Rating 	= getvalue("Supervisor_Rating");
	   	$error 	 			= 0;
	   	$ipcr_refid 		= getvalue("sint_IPCRRefId");
	   	$Final_Rating 		= intval($Final_Rating);
	   	$Supervisor_Rating 	= intval($Supervisor_Rating);
	   	// var_dump($_POST);
	   	// return false;
	   	if (intval($ipcr_refid) == 0) {
	   		$ipcr_fld = "";
		   	$ipcr_val = "";
		   	$ipcr_fld .= "`EmployeesRefId`, `DivisionRefId`, `Semester`, `Year`, `Final_Rating`, `Supervisor_Rating`, ";
		   	$ipcr_val .= "'$emprefid', '$Division', '$quarter', '$year', '$Final_Rating', '$Supervisor_Rating',";
		   	$spms_ipcr_id = f_SaveRecord("NEWSAVE","spms_ipcr",$ipcr_fld,$ipcr_val);
		   	if (is_numeric($spms_ipcr_id)) {
		   		for ($a=1; $a <= $count; $a++) { 
		   			$output 	= realEscape($_POST["output_".$a]); 
		   			if ($output != "") {
		   				$ipcr_detail_fld 	      = "";
			   			$ipcr_detail_val 	      = "";
			   			$si 				         = realEscape($_POST["si_".$a]);
                     $type                   = realEscape($_POST["type_".$a]);
			   			$accomplishment 	      = realEscape($_POST["accomplishment_".$a]);
			   			$success_indicator 	   = realEscape($_POST["si_".$a]);
			   			$quality 				   = realEscape($_POST["quality_".$a]);
			   			$effectiveness 			= realEscape($_POST["effectiveness_".$a]);
			   			$timeliness 				= realEscape($_POST["timeliness_".$a]);
			   			$average 				   = realEscape($_POST["average_".$a]);
                     $q1                     = realEscape($_POST["q1_".$a]); 
                     $q2                     = realEscape($_POST["q2_".$a]); 
                     $q3                     = realEscape($_POST["q3_".$a]); 
                     $q4                     = realEscape($_POST["q4_".$a]); 
                     $q5                     = realEscape($_POST["q5_".$a]); 
                     $e1                     = realEscape($_POST["e1_".$a]); 
                     $e2                     = realEscape($_POST["e2_".$a]); 
                     $e3                     = realEscape($_POST["e3_".$a]); 
                     $e4                     = realEscape($_POST["e4_".$a]); 
                     $e5                     = realEscape($_POST["e5_".$a]); 
                     $t1                     = realEscape($_POST["t1_".$a]); 
                     $t2                     = realEscape($_POST["t2_".$a]); 
                     $t3                     = realEscape($_POST["t3_".$a]); 
                     $t4                     = realEscape($_POST["t4_".$a]); 
                     $t5                     = realEscape($_POST["t5_".$a]); 
			   			$ipcr_detail_fld 	.= "`spms_ipcr_id`, `output`, `accomplishment`, `quality`, `effectiveness`, `timeliness`,";
			   			$ipcr_detail_fld 	.= "success_indicator, `average`, `type`, `q1`, `q2`, `q3`, `q4`, `q5`, ";
                     $ipcr_detail_fld  .= "`e1`, `e2`, `e3`, `e4`, `e5`, ";
                     $ipcr_detail_fld  .= "`t1`, `t2`, `t3`, `t4`, `t5`, ";
			   			$ipcr_detail_val 	.= "'$spms_ipcr_id', '$output', '$accomplishment', '$quality', '$effectiveness',";
			   			$ipcr_detail_val 	.= "'$timeliness', '$success_indicator', '$average', '$type',";
                     $ipcr_detail_val  .= "'$q1', '$q2', '$q3', '$q4', '$q5', ";
                     $ipcr_detail_val  .= "'$e1', '$e2', '$e3', '$e4', '$e5', ";
                     $ipcr_detail_val  .= "'$t1', '$t2', '$t3', '$t4', '$t5', ";
			   			$save_sp 			= f_SaveRecord("NEWSAVE",
			   								  			   "ipcr_details",
			   								  			   $ipcr_detail_fld,
			   								  			   $ipcr_detail_val);
					   	if (!is_numeric($save_sp)) {
					   		$error++;
					   	}
		   			}
			   	}	
		   	} else {
		   		$error++;
		   	}
		   	if ($error == 0) {
		   		echo 'afterSave();';
		   	} else {
		   		echo "Error";
		   	}	
	   	} else {
	   		$ipcr_fldnval 	= "Semester = '$quarter', Year = '$year',";
	   		$ipcr_fldnval   .= "Supervisor_Rating = '$Supervisor_Rating', ";
	   		$ipcr_fldnval   .= "Final_Rating = '$Final_Rating', ";
	   		$update_ipcr 	= f_SaveRecord("EDITSAVE","spms_ipcr",$ipcr_fldnval,$ipcr_refid);
	   		if ($update_ipcr == "") {
	   			for ($a=1; $a <= $count; $a++) { 
	   				$refid 		= realEscape($_POST["refid_".$a]); 
		   			$output 	= realEscape($_POST["output_".$a]); 
		   			if ($refid != "") {
		   				if ($output != "") {
			   				$ipcr_detail_fldnval = "";
				   			$si                     = realEscape($_POST["si_".$a]);
                        $type                   = realEscape($_POST["type_".$a]);
                        $accomplishment         = realEscape($_POST["accomplishment_".$a]);
                        $success_indicator      = realEscape($_POST["si_".$a]);
                        $quality                = realEscape($_POST["quality_".$a]);
                        $effectiveness          = realEscape($_POST["effectiveness_".$a]);
                        $timeliness             = realEscape($_POST["timeliness_".$a]);
                        $average                = realEscape($_POST["average_".$a]);
                        $q1                     = realEscape($_POST["q1_".$a]); 
                        $q2                     = realEscape($_POST["q2_".$a]); 
                        $q3                     = realEscape($_POST["q3_".$a]); 
                        $q4                     = realEscape($_POST["q4_".$a]); 
                        $q5                     = realEscape($_POST["q5_".$a]); 
                        $e1                     = realEscape($_POST["e1_".$a]); 
                        $e2                     = realEscape($_POST["e2_".$a]); 
                        $e3                     = realEscape($_POST["e3_".$a]); 
                        $e4                     = realEscape($_POST["e4_".$a]); 
                        $e5                     = realEscape($_POST["e5_".$a]); 
                        $t1                     = realEscape($_POST["t1_".$a]); 
                        $t2                     = realEscape($_POST["t2_".$a]); 
                        $t3                     = realEscape($_POST["t3_".$a]); 
                        $t4                     = realEscape($_POST["t4_".$a]); 
                        $t5                     = realEscape($_POST["t5_".$a]); 
				   			$ipcr_detail_fldnval = "`output` = '$output', `accomplishment` = '$accomplishment', ";
				   			$ipcr_detail_fldnval .= "`quality` = '$quality', `effectiveness` = '$effectiveness', ";
                        $ipcr_detail_fldnval .= "`timeliness` = '$timeliness', `average` = '$average', ";
				   			$ipcr_detail_fldnval .= "`success_indicator` = '$success_indicator',";
                        $ipcr_detail_fldnval .= "`q1` = '$q1', `q2` = '$q2', `q3` = '$q3',";
                        $ipcr_detail_fldnval .= "`q4` = '$q4', `q5` = '$q5',";
                        $ipcr_detail_fldnval .= "`e1` = '$e1', `e2` = '$e2', `e3` = '$e3',";
                        $ipcr_detail_fldnval .= "`e4` = '$e4', `e5` = '$e5',";
                        $ipcr_detail_fldnval .= "`t1` = '$t1', `t2` = '$t2', `t3` = '$t3',";
                        $ipcr_detail_fldnval .= "`t4` = '$t4', `t5` = '$t5',";
				   			$save_sp 			= f_SaveRecord("EDITSAVE",
				   								  			   "ipcr_details",
				   								  			   $ipcr_detail_fldnval,
				   								  			   $refid);
						   	if ($save_sp != "") {
						   		$error++;
						   	}
			   			}	
		   			} else {
		   				if ($output != "") {
			   				$ipcr_detail_fld 	= "";
				   			$ipcr_detail_val 	= "";
                        $si                     = realEscape($_POST["si_".$a]);
                        $type                   = realEscape($_POST["type_".$a]);
                        $accomplishment         = realEscape($_POST["accomplishment_".$a]);
                        $success_indicator      = realEscape($_POST["si_".$a]);
                        $quality                = realEscape($_POST["quality_".$a]);
                        $effectiveness          = realEscape($_POST["effectiveness_".$a]);
                        $timeliness             = realEscape($_POST["timeliness_".$a]);
                        $average                = realEscape($_POST["average_".$a]);
                        $q1                     = realEscape($_POST["q1_".$a]); 
                        $q2                     = realEscape($_POST["q2_".$a]); 
                        $q3                     = realEscape($_POST["q3_".$a]); 
                        $q4                     = realEscape($_POST["q4_".$a]); 
                        $q5                     = realEscape($_POST["q5_".$a]); 
                        $e1                     = realEscape($_POST["e1_".$a]); 
                        $e2                     = realEscape($_POST["e2_".$a]); 
                        $e3                     = realEscape($_POST["e3_".$a]); 
                        $e4                     = realEscape($_POST["e4_".$a]); 
                        $e5                     = realEscape($_POST["e5_".$a]); 
                        $t1                     = realEscape($_POST["t1_".$a]); 
                        $t2                     = realEscape($_POST["t2_".$a]); 
                        $t3                     = realEscape($_POST["t3_".$a]); 
                        $t4                     = realEscape($_POST["t4_".$a]); 
                        $t5                     = realEscape($_POST["t5_".$a]); 
                        $ipcr_detail_fld  .= "`spms_ipcr_id`, `output`, `accomplishment`, `quality`, `effectiveness`, `timeliness`,";
                        $ipcr_detail_fld  .= "success_indicator, `average`, `type`, `q1`, `q2`, `q3`, `q4`, `q5`, ";
                        $ipcr_detail_fld  .= "`e1`, `e2`, `e3`, `e4`, `e5`, ";
                        $ipcr_detail_fld  .= "`t1`, `t2`, `t3`, `t4`, `t5`, ";
                        $ipcr_detail_val  .= "'$ipcr_refid', '$output', '$accomplishment', '$quality', '$effectiveness',";
                        $ipcr_detail_val  .= "'$timeliness', '$success_indicator', '$average', '$type',";
                        $ipcr_detail_val  .= "'$q1', '$q2', '$q3', '$q4', '$q5', ";
                        $ipcr_detail_val  .= "'$e1', '$e2', '$e3', '$e4', '$e5', ";
                        $ipcr_detail_val  .= "'$t1', '$t2', '$t3', '$t4', '$t5', ";
				   			$save_sp 			= f_SaveRecord("NEWSAVE",
				   								  			   "ipcr_details",
				   								  			   $ipcr_detail_fld,
				   								  			   $ipcr_detail_val);
						   	if (!is_numeric($save_sp)) {
						   		$error++;
						   	}
			   			}
		   			}
			   	}	
	   		} else {
	   			$error++;
	   		}
	   		if ($error == 0) {
		   		echo 'afterEdit();';
		   	} else {
		   		echo "Error";
		   	}
	   	}   	
	}
	function saveOPCR(){
	   	$Office 			= getvalue("sint_OfficeRefId");
	   	$count 				= getvalue("count");
	   	$quarter  			= getvalue("sint_Semester");
	   	$year 	 			= getvalue("sint_Year");
	   	$Final_Rating 		= getvalue("Final_Rating");
	   	$Adjectival_Rating 	= getvalue("Adjectival_Rating");
	   	$Overall_Rating 	= getvalue("Overall_Rating");
	   	$error 	 			= 0;
	   	$ipcr_refid 		= getvalue("sint_IPCRRefId");
	   	$Final_Rating 		= intval($Final_Rating);
	   	$Adjectival_Rating 	= intval($Adjectival_Rating);
	   	$Overall_Rating 	= intval($Overall_Rating);
	   	// var_dump($_POST);
	   	// return false;
	   	if (intval($ipcr_refid) == 0) {
	   		$ipcr_fld = "";
		   	$ipcr_val = "";
		   	$ipcr_fld .= "`OfficeRefId`, `Semester`, `Year`, `Final_Rating`, `Adjectival_Rating`, `Overall_Rating`, ";
		   	$ipcr_val .= "'$Office', '$quarter', '$year', '$Final_Rating', '$Adjectival_Rating', '$Overall_Rating',";
		   	$spms_ipcr_id = f_SaveRecord("NEWSAVE","spms_opcr",$ipcr_fld,$ipcr_val);
		   	if (is_numeric($spms_ipcr_id)) {
		   		for ($a=1; $a <= $count; $a++) { 
		   			$output 	= realEscape($_POST["output_".$a]); 
		   			if ($output != "") {
		   				$ipcr_detail_fld 	= "";
			   			$ipcr_detail_val 	= "";
			   			$si 				= realEscape($_POST["si_".$a]);
			   			$accomplishment 	= realEscape($_POST["accomplishment_".$a]);
			   			$accountable 		= realEscape($_POST["accountable_".$a]);
			   			$allotted_budget 	= realEscape($_POST["budget_".$a]);
			   			$success_indicator 	= realEscape($_POST["si_".$a]);
			   			$q1 				= realEscape($_POST["q1_".$a]);
			   			$e2 				= realEscape($_POST["e2_".$a]);
			   			$t3 				= realEscape($_POST["t3_".$a]);
			   			$a4 				= realEscape($_POST["a4_".$a]);
			   			$remarks 			= realEscape($_POST["remarks_".$a]);
			   			$ipcr_detail_fld 	.= "spms_opcr_id, output, accomplishment, q1, e2, t3, a4, remarks,";
			   			$ipcr_detail_fld 	.= "success_indicator, allotted_budget, accountable, ";
			   			$ipcr_detail_val 	.= "'$spms_ipcr_id', '$output', '$accomplishment', '$q1', '$e2',";
			   			$ipcr_detail_val 	.= "'$t3', '$a4', '$remarks', '$success_indicator',";
			   			$ipcr_detail_val 	.= "'$allotted_budget', '$accountable',";
			   			$save_sp 			= f_SaveRecord("NEWSAVE",
			   								  			   "opcr_details",
			   								  			   $ipcr_detail_fld,
			   								  			   $ipcr_detail_val);
					   	if (!is_numeric($save_sp)) {
					   		$error++;
					   	}
		   			}
			   	}	
		   	} else {
		   		$error++;
		   	}
		   	if ($error == 0) {
		   		echo 'afterSave();';
		   	} else {
		   		echo "Error";
		   	}	
	   	} else {
	   		$ipcr_fldnval 	= "Semester = '$quarter', Year = '$year',";
	   		$ipcr_fldnval   .= "Adjectival_Rating = '$Adjectival_Rating', ";
	   		$ipcr_fldnval   .= "Overall_Rating = '$Overall_Rating', ";
	   		$ipcr_fldnval   .= "Final_Rating = '$Final_Rating', ";
	   		$update_ipcr 	= f_SaveRecord("EDITSAVE","spms_opcr",$ipcr_fldnval,$ipcr_refid);
	   		if ($update_ipcr == "") {
	   			for ($a=1; $a <= $count; $a++) { 
	   				$refid 		= realEscape($_POST["refid_".$a]); 
		   			$output 	= realEscape($_POST["output_".$a]); 
		   			if ($refid != "") {
		   				if ($output != "") {
			   				$ipcr_detail_fldnval = "";
				   			$si 				= realEscape($_POST["si_".$a]);
				   			$accomplishment 	= realEscape($_POST["accomplishment_".$a]);
				   			$accountable 		= realEscape($_POST["accountable_".$a]);
				   			$allotted_budget 	= realEscape($_POST["budget_".$a]);
				   			$success_indicator 	= realEscape($_POST["si_".$a]);
				   			$q1 				= realEscape($_POST["q1_".$a]);
				   			$e2 				= realEscape($_POST["e2_".$a]);
				   			$t3 				= realEscape($_POST["t3_".$a]);
				   			$a4 				= realEscape($_POST["a4_".$a]);
				   			$remarks 			= realEscape($_POST["remarks_".$a]);
				   			$ipcr_detail_fldnval = "output = '$output', accomplishment = '$accomplishment', ";
				   			$ipcr_detail_fldnval .= "q1 = '$q1', e2 = '$e2', t3 = '$t3', a4 = '$a4', ";
				   			$ipcr_detail_fldnval .= "remarks = '$remarks', ";
				   			$ipcr_detail_fldnval .= "allotted_budget = '$allotted_budget', ";
				   			$ipcr_detail_fldnval .= "accountable = '$accountable', ";
				   			$ipcr_detail_fldnval .= "success_indicator = '$success_indicator',";
				   			$save_sp 			= f_SaveRecord("EDITSAVE",
				   								  			   "ipcr_details",
				   								  			   $ipcr_detail_fldnval,
				   								  			   $refid);
						   	if ($save_sp != "") {
						   		$error++;
						   	}
			   			}	
		   			} else {
		   				if ($output != "") {
			   				$ipcr_detail_fld 	= "";
				   			$ipcr_detail_val 	= "";
				   			$si 				= realEscape($_POST["si_".$a]);
				   			$accomplishment 	= realEscape($_POST["accomplishment_".$a]);
				   			$accountable 		= realEscape($_POST["accountable_".$a]);
				   			$allotted_budget 	= realEscape($_POST["budget_".$a]);
				   			$success_indicator 	= realEscape($_POST["si_".$a]);
				   			$q1 				= realEscape($_POST["q1_".$a]);
				   			$e2 				= realEscape($_POST["e2_".$a]);
				   			$t3 				= realEscape($_POST["t3_".$a]);
				   			$a4 				= realEscape($_POST["a4_".$a]);
				   			$remarks 			= realEscape($_POST["remarks_".$a]);
				   			$ipcr_detail_fld 	.= "spms_opcr_id, output, accomplishment, q1, e2, t3, a4, remarks,";
				   			$ipcr_detail_fld 	.= "success_indicator, allotted_budget, accountable, ";
				   			$ipcr_detail_val 	.= "'$ipcr_refid', '$output', '$accomplishment', '$q1', '$e2',";
				   			$ipcr_detail_val 	.= "'$t3', '$a4', '$remarks', '$success_indicator',";
				   			$ipcr_detail_val 	.= "'$allotted_budget', '$accountable',";
				   			$save_sp 			= f_SaveRecord("NEWSAVE",
				   								  			   "opcr_details",
				   								  			   $ipcr_detail_fld,
				   								  			   $ipcr_detail_val);
						   	if (!is_numeric($save_sp)) {
						   		$error++;
						   	}
			   			}
		   			}
			   	}	
	   		} else {
	   			$error++;
	   		}
	   		if ($error == 0) {
		   		echo 'afterEdit();';
		   	} else {
		   		echo "Error";
		   	}
	   	}   	
	}
?>