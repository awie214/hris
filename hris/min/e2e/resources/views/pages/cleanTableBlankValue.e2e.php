<?php
   include "constant.e2e.php";
   include "conn.e2e.php";  
   include pathClass."0620functions.e2e.php";

   function cleanUp($table) {
      $table = strtolower($table);
      echo "Working Table > $table<br>";
      $rs = SelectEach($table,"WHERE Name = ''");
      if ($rs) {
         while ($row = mysqli_fetch_assoc($rs)) {
            echo "<li>". $row["RefId"]. " - ".$row["Name"];
            $count = mysqli_query($GLOBALS["conn"],"DELETE FROM `$table` WHERE RefId = ".$row["RefId"]);
            if ($count) {
               echo "<br>".$row["Name"]." Deleted...<br>";
            } else {
               echo "<br>".$row["Name"]." Delet Error...<br>";
            }
         }
      }
   }
   cleanUp("schools");
   cleanUp("city");
   cleanUp("province");
   cleanUp("course");
   cleanUp("holiday");
   cleanUp("leaves");
   cleanUp("occupations");
   cleanUp("office");
   cleanUp("organization");
   cleanUp("requirements");
?>