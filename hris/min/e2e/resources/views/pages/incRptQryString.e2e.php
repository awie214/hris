<?php

   /*=============================================*/
   $CivilStatus      = "";
   $searchCriteria   = "";
   $_male = $_female = $_sex_undef = $_single = $_married = $_annulled = $_widowed = 0;
   $_separated = $_others = $_civil_undef = $total_sex = $total_civil = 0;
   $table = "employees";
   if ($p_filter_value == "0" || $p_filter_table == "") {
      $whereClause = "where $table.CompanyRefId = ".$CompanyId." AND $table.BranchRefId = ".$BranchId;
   } else {
      $column = $p_filter_table."RefId";
      $whereClause = "INNER JOIN empinformation WHERE $table.RefId = empinformation.EmployeesRefId";
      $whereClause .= " and $table.CompanyRefId = ".$CompanyId." AND $table.BranchRefId = ".$BranchId;
      $whereClause .= " and empinformation.$column = '$p_filter_value'";
      $column_value = getRecord($p_filter_table,$p_filter_value,"Name");
      if ($p_filter_table == "EmpStatus") {
         $searchCriteria .= "<br> Employment Status is $column_value";
      } else {
         $searchCriteria .= "<br> $p_filter_table is $column_value";   
      }
      
   }
   
   
   
   // if ($REFID != ""){
   //    $whereClause .= " and $table.RefId = ".$REFID."";
   //    $searchCriteria .= "EMPLOYEES REFID ".$REFID;
   // }
   if ($EMPID != "") {
      $whereClause .= " and $table.AgencyId = '".$EMPID."'";
      $searchCriteria .= "<br>Employee ID like ".$EMPID;
   }
   if ($p_LastName != "") {
      $whereClause .= " and $table.LastName LIKE '".$p_LastName."%'";
      $searchCriteria .= "<br>Last Name begins with ".$p_LastName;
   }
   if ($p_FirstName != "") {
      $whereClause .= " and $table.FirstName LIKE '".$p_FirstName."%'";
      if ($searchCriteria == "")
         $searchCriteria .= "First Name begins with ".$p_FirstName;
      else
         $searchCriteria .= "<br>First Name begins with ".$p_FirstName;
   }
   if ($p_MiddleName != "") {
      $whereClause .= " and $table.MiddleName LIKE '".$p_MiddleName."%'";
      if ($searchCriteria == "")
         $searchCriteria .= "Middle Name begins with ".$p_MiddleName;
      else
         $searchCriteria .= "<br>Middle Name begins with ".$p_MiddleName;
   }
   if ($p_CivilStatus != "") {
      $whereClause .= " and $table.CivilStatus = '$p_CivilStatus'";
      switch ($p_CivilStatus) {
         case "Si":
            if ($searchCriteria == "")
               $searchCriteria .= "Civil Status is Single";
            else
               $searchCriteria .= "<br>Civil Status is Single";
         break;
         case "Ma":
            if ($searchCriteria == "")
               $searchCriteria .= "Civil Status is Married";
            else
               $searchCriteria .= "<br>Civil Status is Married";
         break;
         case "An":
            if ($searchCriteria == "")
               $searchCriteria .= "Civil Status is Annulled";
            else
               $searchCriteria .= "<br>Civil Status is Annulled";
         break;
         case "Wi":
            if ($searchCriteria == "")
               $searchCriteria .= "Civil Status is Widowed";
            else
               $searchCriteria .= "<br>Civil Status is Widowed";
         break;
         case "Se":
            if ($searchCriteria == "")
               $searchCriteria .= "Civil Status is Separated";
            else
               $searchCriteria .= "<br>Civil Status is Separated";
         break;
         case "Ot":
            if ($searchCriteria == "")
               $searchCriteria .= "Civil Status is Other";
            else
               $searchCriteria .= "<br>Civil Status is Other";
         break;
      }
   }
   if ($p_Sex != "") {
      $whereClause .= " and $table.Sex = '$p_Sex'";
      switch ($p_Sex) {
         case "M":
            if ($searchCriteria == "")
               $searchCriteria .= "Sex is Male";
            else
               $searchCriteria .= "<br>Sex is Male";
         break;
         case "F":
            if ($searchCriteria == "")
               $searchCriteria .= "Sex is Female";
            else
               $searchCriteria .= "<br>Sex is Female";
         break;
      }
   }
   // if ($p_DOBFrom != "" && $p_DOBTo != "") {
   //    $whereClause .= " and BirthDate BETWEEN '$p_DOBFrom' AND '$p_DOBTo'";
   //    if ($searchCriteria == "")
   //       $searchCriteria .= "Birth Date BETWEEN '$p_DOBFrom' AND '$p_DOBTo'";
   //    else
   //       $searchCriteria .= "<br>Birth Date BETWEEN '$p_DOBFrom' AND '$p_DOBTo'";
   // }
   // if ($p_HeightFrom != ""){
   //    $whereClause .= " and Height >= $p_HeightFrom";
   //    if ($searchCriteria == "")
   //       $searchCriteria .= "Height Above or Equal $p_HeightFrom";
   //    else
   //       $searchCriteria .= "<br>Height Above or Equal $p_HeightFrom";
   // }
   // if ($p_HeightTo != ""){
   //    $whereClause .= " and Height <= $p_HeightTo";
   //    if ($searchCriteria == "")
   //       $searchCriteria .= "Height Below or Equal $p_HeightTo";
   //    else
   //       $searchCriteria .= "<br>Height Below or Equal $p_HeightTo";
   // }
   // if ($p_WeightFrom != ""){
   //    $whereClause .= " and Weight >= $p_WeightFrom";
   //    if ($searchCriteria == "")
   //       $searchCriteria .= "Weight Above or Equal $p_WeightFrom";
   //    else
   //       $searchCriteria .= "<br>Weight Above or Equal $p_WeightFrom";
   // }
   // if ($p_WeightTo != ""){
   //    $whereClause .= " and Weight <= $p_WeightTo";
   //    if ($searchCriteria == "")
   //       $searchCriteria .= "Weight Below or Equal $p_WeightTo";
   //    else
   //       $searchCriteria .= "<br>Weight Below or Equal $p_WeightTo";
   // }
   // if ($p_RProvince > 0){
   //    $whereClause .= " and ResiAddProvinceRefId = $p_RProvince";
   //    if ($searchCriteria == "")
   //       $searchCriteria .= "Province $p_RProvince";
   //    else
   //       $searchCriteria .= "<br>Province $p_RProvince";
   // }
   // if ($p_RCity > 0){
   //    $whereClause .= " and ResiAddCityRefId = $p_RCity";
   //    if ($searchCriteria == "")
   //       $searchCriteria .= "City $p_RCity";
   //    else
   //       $searchCriteria .= "<br>City $p_RCity";
   // }
   if ($p_Inactive == 0) {
      $whereClause .= " AND (Inactive != 1 OR Inactive IS NULL)";
   } else {
      $whereClause .= " AND Inactive = 1";
   }


   if ($signatory != "") {
      $emp_row_signatory   = FindFirst("employees","WHERE RefId = '$signatory'","`LastName`,`FirstName`,`MiddleName`");
      $Signatory_FullName  = $emp_row_signatory["FirstName"]." ".substr($emp_row_signatory["MiddleName"], 0,1)." ".$emp_row_signatory["LastName"];
      $empinfo_signatory   = FindFirst("empinformation","WHERE EmployeesRefId = '$signatory'","PositionRefId");
      if ($empinfo_signatory > 0) {
         $Signatory_Position = getRecord("position",$empinfo_signatory,"Name");
      } else {
         $Signatory_Position = "";
      }
   } else {
      $Signatory_Position = $Signatory_FullName = "&nbsp;";
   }
?> 