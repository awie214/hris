<?php
   $emprefid      = getvalue("hEmpRefId");
   $where_access  = "WHERE EmployeesRefId = '".$emprefid."'";
   $division      = FindFirst("usermanagement",$where_access,"DivisionRefId");
   $division      = intval($division);
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_approval") ?>"></script>
      <script type="text/javascript">
         $(document).ready(function () {
            $("[id*='viewReport_']").each(function () {
               $(this).click(function () {
                  var idx = $(this).attr("id").split("_")[1];
                  viewReport(idx);
               });
            });
         });
         function viewReport(refid){
            $("#rptContent").attr("src","blank.htm");
            var cid = $("#hCompanyID").val();
            var rptFile = "rpt_Overtime_" + cid;
            if (cid == 1000) rptFile = "rpt_Overtime_2";
            var url = "ReportCaller.e2e.php?file=" + rptFile;
            url += "&refid=" + refid;
            url += "&" + $("[name='hgParam']").val();
            $("#prnModal").modal();
            $("#rptContent").attr("src",url);
         }
      </script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php
               doTitleBar("For Aprroval > Overtime");
               spacer(5);

            ?>
            <div class="row">
               <div class="col-sm-12">
                  <?php
                     $EmpRefId = getvalue("txtRefId");
                     $attr = ["empRefId"=>getvalue("txtRefId"),
                              "empLName"=>getvalue("txtLName"),
                              "empFName"=>getvalue("txtFName"),
                              "empMName"=>getvalue("txtMidName")];
                     $EmpRefId = EmployeesSearch($attr);
                     bar();
                     /*$sql = "SELECT *,overtime_request.RefId as asRefId FROM employees
                       INNER JOIN overtime_request
                       ON employees.CompanyRefId = ".$CompanyId."
                         AND employees.BranchRefId = ".$BranchId;*/
                     $sql = "SELECT *,overtime_request.RefId as asRefId FROM overtime_request
                     INNER JOIN employees 
                     ON overtime_request.CompanyRefId = employees.CompanyRefId
                     AND overtime_request.BranchRefId = employees.BranchRefId
                     AND overtime_request.EmployeesRefId = employees.RefId";

                     if (getvalue("txtLName") != "") {
                        $sql .= " AND employees.LastName LIKE '".getvalue("txtLName")."%'";
                     }
                     if (getvalue("txtFName") != "") {
                        $sql .= " AND employees.FirstName LIKE '".getvalue("txtFName")."%'";
                     }
                     if (getvalue("txtMidName") != "") {
                        $sql .= " AND employees.MiddleName LIKE '".getvalue("txtMidName")."%'";
                     }
                     if (getvalue("txtRefId") != "") {
                        $sql .= " AND overtime_request.EmployeesRefId = '".getvalue("txtRefId")."'";
                     } 
                     $sql .= " AND overtime_request.Status IS NULL ORDER BY StartDate LIMIT 100";
                     //echo $sql;  
                     
                     $rs = mysqli_query($conn,$sql) or die(mysqli_error($conn));

                  ?>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-1"></div>
               <div class="col-sm-10 padd5">
                  <?php
                     if ($rs) {
                        while ($row = mysqli_fetch_array($rs)) {
                           $refid = $row["asRefId"];
                           $rsEmp = FFirstRefId("employees",$row["EmployeesRefId"],"*");
                           $empinformation = FindFirst('empinformation',"WHERE EmployeesRefId = ".$rsEmp["RefId"],"*");
                           $style   = "";
                           $info    = array_merge($rsEmp,$empinformation);
                           if ($division > 0) {
                              if ($division != $empinformation["DivisionRefId"]) {
                                 $style = 'display: none;';
                              }
                           }
                  ?>
                           <div class="mypanel pull-left padd5" style="margin:5px;width:40%; <?php echo $style; ?>" id="card_<?php echo $refid; ?>">
                              <div class="panel-top">
                                 <div class="row">
                                    <div class="col-xs-6">
                                       REF ID:&nbsp;<?php echo $refid; ?>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                       <?php
                                          if (getvalue("hCompanyID") == 21) {
                                             echo '<u id="viewReport_'.$refid.'">View Application</u>';
                                          }
                                       ?>
                                       
                                    </div>
                                 </div>
                              </div>
                              <div class="panel-mid">
                                 <div class="row txt-right" style="margin-right:10px;">
                                    <label>DATE FILE:</label><span style="margin-left:15px;"><?php echo $row["FiledDate"];?></span>
                                 </div>
                                 <?php
                                    
                                    echo '
                                    <div class="row margin-top padd5">
                                       <div class="row margin-top">
                                          <div class="col-sm-4 txt-center">
                                             <div class="border" style="height:1.5in;width:1.3in;">
                                                <img src="'.img($rsEmp['CompanyRefId']."/EmployeesPhoto/".$rsEmp['PicFilename']).'" style="width:100%;height:100%;">
                                             </div>
                                          </div>
                                          <div class="col-sm-8 txt-center padd5">';
                                             $templ->btn_apprvReject(2,$refid);
                                          echo    
                                          '</div>
                                       </div>
                                       <div class="row margin-top">   
                                          <div class="col-sm-12">';
                                             $templ->doEmployeeInfo($info);
                                    echo       
                                          '</div>   
                                       </div>   
                                    </div>';
                                    bar();  
                                 ?>
                                 <div class="row">
                                    <div class="col-sm-6">
                                       <label>START DATE:</label>
                                          <br>
                                       <span><?php echo date("F d, Y",strtotime($row["StartDate"]));?></span>
                                    </div>
                                    <div class="col-sm-6">
                                       <label>END DATE:</label>
                                       <br>
                                       <span><?php echo date("F d, Y",strtotime($row["EndDate"]));?></span>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-sm-6">
                                       <label>FROM TIME:</label>
                                          <br>
                                       <span>&nbsp;<?php echo convertToHoursMins($row["FromTime"]);?></span>
                                    </div>
                                    <div class="col-sm-6">
                                       <label>TO TIME:</label>
                                       <br>
                                       <span>&nbsp;<?php echo convertToHoursMins($row["ToTime"]);?></span>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-sm-6">
                                       <label>STATUS:</label>
                                       <br>
                                       <?php
                                          if ($row['WithPay'] == 1){
                                             echo 'OT PAY';
                                          }else {
                                             echo 'COC';
                                          }
                                       ?>
                                    </div>
                                 </div>
                              </div>
                              <div class="panel-bottom"></div>
                           </div>
                        <?php
                        }
                     } else {
                        alert("Information","No For Approval");
                     }
                  ?>
               </div>
               <div class="col-sm-1"></div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               $table = "overtime_request";
               modalReject();
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>

   </body>
</html>