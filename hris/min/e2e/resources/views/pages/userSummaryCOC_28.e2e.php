<?php
   $Division      = "";
   $Position      = "";
   $Office        = "";
   $SalaryAmount  = "";
   $Division      = "";
   $Position      = "";
   $Office        = "";
   $SalaryAmount  = "";
   $SalaryAmount  = "";
   $FullName      = ""; 
   $coc_weekdays  = 0;
   $coc_weekends  = 0;
   $ot_weekdays   = 0;
   $ot_weekends   = 0;

   if (isset($_POST["year_"])) {
      $y = $_POST["year_"];
   } else {
      $y = date("Y",time());
   }

   $emprefid   = getvalue("hEmpRefId");
   $name       = FindFirst("employees","WHERE RefId = '$emprefid'","LastName,FirstName,MiddleName,ExtName");
   if ($name) {
      $LastName   = $name["LastName"];
      $FirstName  = $name["FirstName"];
      $MiddleName = $name["MiddleName"];
      $ExtName    = $name["ExtName"];
      $MiddleName = substr($MiddleName, 0, 1);
      $FullName   = $LastName.", ".$FirstName." ".$MiddleName.". ".$ExtName;
   }

   $flds       = "DivisionRefId,PositionRefId,OfficeRefId,SalaryAmount";
   $info       = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'",$flds);
   if ($info) {
      $Division      = $info["DivisionRefId"];
      $Position      = $info["PositionRefId"];
      $Office        = $info["OfficeRefId"];
      $SalaryAmount  = $info["SalaryAmount"];
      $Division      = getRecord("division",$Division,"Name");
      $Position      = getRecord("Position",$Position,"Name");
      $Office        = getRecord("Office",$Office,"Name");
      $SalaryAmount  = floatval($SalaryAmount);
      $SalaryAmount  = number_format($SalaryAmount,2);
   }
?>
<!DOCTYPE>
<html>
<head>
   <title></title>
   <?php include_once $files["inc"]["pageHEAD"]; ?>
   <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
   <script type="text/javascript">
      $(document).ready(function () {
         $("#btnPrint").click(function () {
            var head = $("head").html();
            printDiv('div_CONTENT',head);
         });
         <?php
            if (isset($_POST["year_"])) {
               $y = $_POST["year_"];
            } else {
               $y = date("Y",time());
            }     
            echo '$("#year_").val("'.$y.'");';        
         ?>

      });
   </script>
   <style type="text/css">
      #not_avail {
         font-weight: 900;
         font-size: 15pt;
         padding-left: 9%;
      }
      @media print {
         body {
            font-size: 9pt;
         }
         .footer_div {
            position: fixed;
            right: 0;
            bottom: 0;
            left: 0;
            padding: 3px;
            font-size: 8pt;
         }
      }
      .bold {font-size:600;}
   </style>
</head>
<body onload = "indicateActiveModules();">
   <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
      <?php $sys->SysHdr($sys,"pis"); ?>
      <div class="container-fluid" id="mainScreen">
         <?php doTitleBar (""); ?>
         <div class="container-fluid margin-top">
            <br><br>
            <div class="row">
               <div class="col-xs-2">
                  <select class="form-input" id="year_" name="year_">
                     <option value="0">Select Year</option>
                     <?php
                        $start   = date("Y",time()) - 1;
                        $end     = date("Y",time());
                        for ($a=$start; $a <= $end ; $a++) { 
                           echo '<option value="'.$a.'">'.$a.'</option>';
                        }
                     ?>
                  </select>
               </div>
               <div class="col-xs-3">
                  <button type="submit" id="" class="btn-cls4-sea">SUBMIT</button>      
                  <button type="button" id="btnPrint" class="btn-cls4-lemon">PRINT</button>      
               </div>
            </div>
            <br><br>
            <div class="row">
               <div class="col-xs-12" id="div_CONTENT">
                  <div class="container-fluid">
                     <div class="row">
                        <div class="col-xs-12">
                           <div class="row">
                              <div class="col-xs-12">
                                 <?php
                                    rptHeader("Authority for Overtime Services");
                                 ?>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <table border="2">
                                    <tr>
                                       <td style="width: 12.5%;"></td>
                                       <td style="width: 12.5%;"></td>
                                       <td style="width: 12.5%;"></td>
                                       <td style="width: 12.5%;"></td>
                                       <td style="width: 12.5%;"></td>
                                       <td style="width: 12.5%;"></td>
                                       <td style="width: 12.5%;"></td>
                                       <td style="width: 12.5%;"></td>
                                    </tr>
                                    <tr>
                                       <td colspan="8" class="text-center">
                                          <b>
                                             SUMMARY REPORT OF COMPENSATORY OVERTIME CREDITS (COC's) AND COMPENSATORY TIME-OFFS (CTO's)
                                             <br>
                                             (Main Office)
                                          </b>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="3" valign="top">
                                          <b>Name: <?php echo $FullName; ?></b>
                                       </td>
                                       <td colspan="3" valign="top">
                                          <b>Division/Office: <?php echo $Division." / ".$Office; ?></b>
                                       </td>
                                       <td colspan="2" valign="top">
                                          <b>For CY: <?php echo $y; ?></b>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td class="text-center">
                                          <b>Month</b>
                                       </td>
                                       <td class="text-center">
                                          <b>COC Balance from previous Month</b>
                                       </td>
                                       <td class="text-center">
                                          <b>COC Earned by end of the month (Maximum of 40 Hours)</b>
                                       </td>
                                       <td class="text-center">
                                          <b>Total COC</b>
                                       </td>
                                       <td class="text-center">
                                          <b>COC Applied by end of the month</b>
                                       </td>
                                       <td class="text-center">
                                          <b>Inclusive Dates</b>
                                       </td>
                                       <td class="text-center">
                                          <b>Balance* (Maximum of 120 Hours)</b>
                                       </td>
                                       <td class="text-center">
                                          <b>Remarks</b>
                                       </td>
                                    </tr>
                                    <?php
                                       $prev_coc = 0;
                                       for ($i=1; $i <= 12; $i++) { 
                                          $month = $i;
                                          if ($month <= 9) $month = "0".$month;
                                          $where = "WHERE EmployeesRefId = '$emprefid' AND Month = '$month' AND Year = '$y'";
                                          $dtr   = FindFirst("dtr_process",$where,"*");
                                          if ($dtr) {
                                             $coc        = $dtr["Total_COC_Hr"];
                                             $used_coc   = $dtr["COC_Used"];
                                          } else {
                                             $coc = $used_coc = 0;
                                          }



                                          echo '<tr>';
                                          echo '<td class="text-center"><b>'.monthName($i,1).'</b></td>';
                                          echo '<td class="text-center">'.rpt_HoursFormat($prev_coc).'</td>';
                                          echo '<td class="text-center">'.rpt_HoursFormat($coc).'</td>';
                                          echo '<td class="text-center">'.rpt_HoursFormat($prev_coc + $coc).'</td>';
                                          echo '<td class="text-center">'.rpt_HoursFormat($used_coc).'</td>';
                                          echo '<td></td>';
                                          $coc_balance = ($prev_coc + $coc) - $used_coc;
                                          if ($coc_balance > 0) {
                                             $coc_balance = rpt_HoursFormat($coc_balance);
                                          } else {
                                             if ($coc_balance != "") $coc_balance = "(".rpt_HoursFormat(abs($coc_balance)).")";
                                          }
                                          echo '<td class="text-center">'.$coc_balance.'</td>';
                                          echo '<td></td>';
                                          echo '</tr>';

                                          $prev_coc += $coc;
                                          $prev_coc -= $used_coc;
                                       }
                                    ?>
                                    <tr>
                                       <td colspan="4">
                                          <div class="row">
                                             <div class="col-xs-12">
                                                Prepared by:
                                                <br>
                                                <br>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-6"></div>
                                             <div class="col-xs-6 text-center">
                                                __________________________
                                                <br>
                                                Signature over Printed Name of Employee
                                             </div>
                                          </div>
                                       </td>
                                       <td colspan="4">
                                          <div class="row">
                                             <div class="col-xs-12">
                                                Noted by:
                                                <br>
                                                <br>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-6"></div>
                                             <div class="col-xs-6 text-center">
                                                __________________________
                                                <br>
                                                Signature over Printed Name of Approving Official
                                             </div>
                                          </div>
                                       </td>
                                    </tr>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php
            footer();
            include "varHidden.e2e.php";
         ?>
      </div>
   </form>
</body>
</html>
