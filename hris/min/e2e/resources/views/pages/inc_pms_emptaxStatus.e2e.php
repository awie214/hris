<script>
   $(document).ready(function () {
      $(".newDataLibrary").hide();
      $(".createSelect--").css("width","100%");
   
   });
</script>
<div class="mypanel" id="emptaxStatus">
   <div class="row margin-top" style="height:200px;overflow:auto;">
      <table border="1" width="100%">
         <tr  class="txt-center" style="padding:5px;">
            <td><label>Deduction Name</label></td>
            <td><label>Amount</label></td>
            <td><label>Date Start</label></td>
            <td><label>Date End</label></td>
            <td><label>Terminated</label></td>
         </tr>
      <?php
         $rs = SelectEach("employeeschild","");
         if ($rs) {
            $recordNum = mysqli_num_rows($rs);
            while ($row = mysqli_fetch_array($rs)) {
      ?>
         <tr>
            <td><?php echo $row["FullName"]?></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
         </tr>
      <?php 
            }
         }
      ?>
      </table>
   </div>
   <?php spacer(20);?>
   <div class="mypanel">
      <div class="panel-top">Detail</div>
      <div class="panel-mid">
         <div class="row">
            <div class="col-xs-12">
               <input type="checkbox" class="enabler--" name="chkTaxStatus"  for="emptaxStatus">
               <label id="enable">Enable Fields</label>
            </div> 
         </div>
         <?php bar(); ?>
         <div class="row margin-top">
            <div class="col-xs-6"></div>
            <div class="col-xs-3">
               <div class="row">
                  <div class="col-xs-6">
                     <label>Effectivity Date</label>
                  </div>
                  <div class="col-xs-6">
                     <input type="text" name="TSEffectivityDate" class="saveFields-- form-input date--">
                  </div>
               </div>
            </div>
            <div class="col-xs-3">
               <div class="row">
                  <div class="col-xs-6 label">
                     <label>Tax Status</label>
                  </div>
                  <div class="col-xs-6">
                     <select class="saveFields-- form-input" name="TSTaxStatus">
                        <option></option>
                        <option></option>
                     </select>
                  </div>
               </div>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-12">
               <div class="row margin-top txt-center">
                  <label>Childrens Names and Birthdays</label>
               </div>
               <div class="row margin-top txt-center">
                  <div class="col-xs-10"></div>
                  <div class="col-xs-2">Waived</div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2 label">
                     <label>Last Name</label>
                  </div>
                  <div class="col-xs-2">
                        <input type="text" name="LastName1" class="saveFields-- form-input">
                     </div>
                  <div class="col-xs-2">
                     <input type="text" name="LastName2" class="saveFields-- form-input">
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="LastName3" class="saveFields-- form-input">
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="LastName4" class="saveFields-- form-input">
                  </div>
                  <div class="col-xs-2 txt-center">
                     <input type="checkbox" name="chkWaived1">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2 label">
                     <label>First Name</label>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="FirstName1" class="saveFields-- form-input">
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="FirstName2" class="saveFields-- form-input">
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="FirstName3" class="saveFields-- form-input">
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="FirstName4" class="saveFields-- form-input">
                  </div>
                  <div class="col-xs-2 txt-center">
                     <input type="checkbox" name="chkWaived2">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2 label">
                     <label>Middle Name</label>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="MiddleName1" class="saveFields-- form-input">
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="MiddleName2" class="saveFields-- form-input">
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="MiddleName3" class="saveFields-- form-input">
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="MiddleName4" class="saveFields-- form-input">
                  </div>
                  <div class="col-xs-2 txt-center">
                     <input type="checkbox" name="chkWaived3">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2 label">
                     <label>Ext. Name</label>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="ExtName1" class="saveFields-- form-input">
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="ExtName2" class="saveFields-- form-input">
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="ExtName3" class="saveFields-- form-input">
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="ExtName4" class="saveFields-- form-input">
                  </div>
                  <div class="col-xs-2 txt-center">
                     <input type="checkbox" name="chkWaived4">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2 label">
                     <label>Birthdate</label>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="BirthDate1" class="saveFields-- form-input date--">
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="BirthDate2" class="saveFields-- form-input date--">
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="BirthDate3" class="saveFields-- form-input date--">
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="BirthDate4" class="saveFields-- form-input date--">
                  </div>
                  <div class="col-xs-2 txt-center">
                     <input type="checkbox" name="chkWaived5">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>