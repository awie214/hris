<?php
	require_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';

   	function setValue($rs,$fields) {
      	if (empty($rs[$fields])) {
         	echo 'N/A';
      	} else {
         	if ($fields == "EmailAdd") {
            	echo $rs[$fields];
         	} else {
            	if ($rs[$fields] == "1901-01-01") {
               		echo 'N/A';
	            } else {
	               	echo strtoupper($rs[$fields]);
	            }
         	}
      	}
   	}
   	function validateDate($date, $format = 'Y-m-d'){
      	$d = DateTime::createFromFormat($format, $date);
      	return $d && $d->format($format) == $date;
   	}
   	function dispValue($str) {
      	if (trim($str) == "") {
         	echo "N/A";
      	} else {
         	echo strtoupper($str);
      	}
   	}

   	function dispVal($str) {
      	if (trim($str) == "" || empty($str)) {
         	return "N/A";
      	} else {
            if (validateDate($str)) {
            		$str_arr = explode("-", $str);
            		$str = $str_arr[1]."/".$str_arr[2]."/".$str_arr[0];
            		return $str;
         	} else {
            		return $str;
         	}
      	}
   	}

   	for ($i=1; $i <=12 ; $i++) { 
   		$map["[[q".$i."a]]"] = "<input type='checkbox' disabled>";
   		$map["[[q".$i."b]]"] = "<input type='checkbox' disabled>";
   		$map["[[q".$i."exp]]"] = "&nbsp;";
   	}
   	$map["[[q4date]]"] = "&nbsp;";
   	$map["[[q4case]]"] = "&nbsp;";





   /*RUNNING*/
   	$diag = 0;
   	$empRefId = getvalue("emprefid");
   	$MasterFile = true;
   	$rsPersonInfo = FindFirst("employees","WHERE RefId = $empRefId","*");
   	$rsEmpFamily = FindFirst("employeesfamily","WHERE EmployeesRefId = $empRefId","*");
   	$rsEmpChild = SelectEach("employeeschild","WHERE EmployeesRefId = $empRefId ORDER BY BirthDate");
   	$rsEmpEducElem = FindFirst("employeeseduc","WHERE LevelType = 1 AND EmployeesRefId = $empRefId","*");
   	$rsEmpEducSeco = FindFirst("employeeseduc","WHERE LevelType = 2 AND EmployeesRefId = $empRefId","*");
   	$rsEmpEducVoca = SelectEach("employeeseduc","WHERE LevelType = 3 AND EmployeesRefId = $empRefId ORDER BY DateFrom DESC LIMIT 2");
   	$rsEmpEducColl = SelectEach("employeeseduc","WHERE LevelType = 4 AND EmployeesRefId = $empRefId ORDER BY DateFrom DESC LIMIT 4");
   	$rsEmpEducGrad = SelectEach("employeeseduc","WHERE LevelType = 5 AND EmployeesRefId = $empRefId ORDER BY DateFrom DESC LIMIT 3");
   	$rsEmpEligibility = SelectEach("employeeselegibility","WHERE EmployeesRefId = $empRefId ORDER BY ExamDate DESC LIMIT 7");
   	$rsEmpWorkExp = SelectEach("employeesworkexperience","WHERE EmployeesRefId = $empRefId ORDER BY WorkStartDate DESC LIMIT 25");
   	$rsEmpVoluntary = SelectEach("employeesvoluntary","WHERE EmployeesRefId = $empRefId ORDER BY StartDate DESC");
   	$rsEmpTraining = SelectEach("employeestraining","WHERE EmployeesRefId = $empRefId ORDER BY StartDate DESC");
   	$rsEmpOtherInfo = SelectEach("employeesotherinfo","WHERE EmployeesRefId = $empRefId ORDER BY RefId");
   	$rsEmpReference = SelectEach("employeesreference","WHERE EmployeesRefId = $empRefId ORDER BY RefId");
   	$rsPDSQ = FindFirst("employeespdsq","WHERE EmployeesRefId = $empRefId","*");

   	
	$map["[[S]]"] 				= "<input type='checkbox' disabled>";
	$map["[[CS]]"] 				= "<input type='checkbox' disabled>";
	$map["[[FI]]"] 				= "<input type='checkbox' disabled>";	
	$map["[[DU]]"] 				= "<input type='checkbox' disabled>";	
	$map["[[b]]"] 				= "<input type='checkbox' disabled>";	
	$map["[[n]]"] 				= "<input type='checkbox' disabled>";	
	$map["[[CSexp]]"] 			= "&nbsp;";
	$map["[[DUC]]"] 			= "&nbsp;";

	for ($i=1; $i <=5 ; $i++) { 
		$map["[[6$i]]"] = "<input type='checkbox' disabled>";
		$map["[[S$i]]"] = "<input type='checkbox' disabled>";
	}
	if ($rsPersonInfo["Sex"] == "M") {
		$map["[[S1]]"] = "<input type='checkbox' disabled checked>";
	} else {
		$map["[[S2]]"] = "<input type='checkbox' disabled checked>";
	}
	switch ($rsPersonInfo["CivilStatus"]) {
		case "Si":
			$map["[[61]]"]		= "<input type='checkbox' disabled checked>";
			break;
		case "Ma":
			$map["[[62]]"] 		= "<input type='checkbox' disabled checked>";			
			break;
		case "Wi":
			$map["[[63]]"] 		= "<input type='checkbox' disabled checked>";
			break;
		case "Se":
			$map["[[64]]"] 		= "<input type='checkbox' disabled checked>";
			break;
		case "Ot":
			$map["[[65]]"] 		= "<input type='checkbox' disabled checked>";
			break;
	}
	

	if ($rsPersonInfo["isFilipino"] == 1) {
		$map["[[FI]]"] 				= "<input type='checkbox' disabled checked>";	
	} else {
		$map["[[DU]]"] 				= "<input type='checkbox' disabled checked>";	
	}
	if ($rsPersonInfo["isByBirthOrNatural"] == 1) {
		$map["[[b]]"] 				= "<input type='checkbox' disabled checked>";	
	} else if ($rsPersonInfo["isByBirthOrNatural"] == 0) {
		$map["[[n]]"] 				= "<input type='checkbox' disabled checked>";
		$map["[[DUC]]"] 			= dispVal(getRecord("country",$rsPersonInfo["CountryCitizenRefId"],"Name"));	
	}
	
	
	$map["[[LN]]"] 				= dispVal($rsPersonInfo["LastName"]);
	$map["[[FN]]"] 				= dispVal($rsPersonInfo["FirstName"]);
	$map["[[MN]]"] 				= dispVal($rsPersonInfo["MiddleName"]);
	$map["[[EN]]"] 				= dispVal($rsPersonInfo["ExtName"]);
	$map["[[BD]]"] 				= dispVal($rsPersonInfo["BirthDate"]);
	$map["[[BP]]"] 				= dispVal($rsPersonInfo["BirthPlace"]);
	$map["[[ResiHouseNo]]"] 	= dispVal($rsPersonInfo["ResiHouseNo"]);
	$map["[[ResiStreet]]"] 		= dispVal($rsPersonInfo["ResiStreet"]);
	$map["[[ResiSubd]]"] 		= dispVal($rsPersonInfo["ResiSubd"]);
	$map["[[ResiBrgy]]"] 		= dispVal($rsPersonInfo["ResiBrgy"]);
	$map["[[ResiCity]]"] 		= dispVal(getRecord("city",$rsPersonInfo["ResiAddCityRefId"],"Name"));
	$map["[[ResiProvince]]"] 	= dispVal(getRecord("province",$rsPersonInfo["ResiAddProvinceRefId"],"Name"));
	$map["[[ResiZipCode]]"] 	= dispVal(getRecord("city",$rsPersonInfo["ResiAddCityRefId"],"ZipCode"));
	$map["[[PermaHouseNo]]"] 	= dispVal($rsPersonInfo["PermanentHouseNo"]);
	$map["[[PermaStreet]]"] 	= dispVal($rsPersonInfo["PermanentStreet"]);
	$map["[[PermaSubd]]"] 		= dispVal($rsPersonInfo["PermanentSubd"]);
	$map["[[PermaBrgy]]"] 		= dispVal($rsPersonInfo["PermanentBrgy"]);
	$map["[[PermaCity]]"] 		= dispVal(getRecord("city",$rsPersonInfo["PermanentAddCityRefId"],"Name"));
	$map["[[PermaProvince]]"] 	= dispVal(getRecord("province",$rsPersonInfo["PermanentAddProvinceRefId"],"Name"));
	$map["[[PermaZipCode]]"] 	= dispVal(getRecord("city",$rsPersonInfo["PermanentAddCityRefId"],"ZipCode"));
	$map["[[Height]]"] 			= dispVal($rsPersonInfo["Height"]);
	$map["[[Weight]]"] 			= dispVal($rsPersonInfo["Weight"]);
	$map["[[BT]]"] 				= dispVal(getRecord("bloodtype",$rsPersonInfo["BloodTypeRefId"],"Name"));
	$map["[[GSIS]]"] 			= dispVal($rsPersonInfo["GSIS"]);
	$map["[[PAGIBIG]]"] 		= dispVal($rsPersonInfo["PAGIBIG"]);
	$map["[[TIN]]"] 			= dispVal($rsPersonInfo["TIN"]);
	$map["[[SSS]]"] 			= dispVal($rsPersonInfo["SSS"]);
	$map["[[PHIC]]"] 			= dispVal($rsPersonInfo["PHIC"]);
	$map["[[AID]]"] 			= dispVal($rsPersonInfo["AgencyId"]);
	$map["[[TEL]]"] 			= dispVal($rsPersonInfo["TelNo"]);
	$map["[[MOB]]"] 			= dispVal($rsPersonInfo["MobileNo"]);
	$map["[[EMAIL]]"] 			= dispVal($rsPersonInfo["EmailAdd"]);
	$map["[[gid]]"] 			= dispVal($rsPersonInfo["GovtIssuedID"]);
	$map["[[gidno]]"] 			= dispVal($rsPersonInfo["GovtIssuedIDNo"]);
	$map["[[gidd]]"] 			= dispVal($rsPersonInfo["GovtIssuedIDDate"]);
	$map["[[gidp]]"] 			= dispVal($rsPersonInfo["GovtIssuedIDPlace"]);


   	if ($rsEmpChild) {
   		$i = 0;
   		while($row = mysqli_fetch_assoc($rsEmpChild)) {
   			$i++;
   			$map["[[CH$i]]"] 		= dispVal($row["FullName"]);	
   			$map["[[CHBD$i]]"] 		= dispVal($row["BirthDate"]);	
   		}
   	}
   	if ($rsEmpEducElem) {
		$elem_dateto = $rsEmpEducElem["DateTo"];
      	$elem_school = getRecord("schools",$rsEmpEducElem["SchoolsRefId"],"Name");
      	$elem_course = getRecord("course",$rsEmpEducElem["CourseRefId"],"Name");
      	$elem_datefrom = $rsEmpEducElem["DateFrom"];
      	$elem_highest = $rsEmpEducElem["HighestGrade"];
      	$elem_year = $rsEmpEducElem["YearGraduated"];
      	$elem_honor = $rsEmpEducElem["Honors"];
      	if ($elem_dateto == "2999-12-31" || $rsEmpEducElem["Present"] == 1) $elem_dateto = "PRESENT";

		$map["[[es1]]"]		= dispVal($elem_school);
		$map["[[ec1]]"]		= dispVal($elem_course);
		$map["[[ef1]]"]			= dispVal($elem_datefrom);
		$map["[[et1]]"]			= dispVal($elem_dateto);
		$map["[[el1]]"]			= dispVal($elem_highest);
		$map["[[ey1]]"]			= dispVal($elem_year);
		$map["[[eh1]]"]			= dispVal($elem_honor);
   	}

   	if ($rsEmpEducSeco) {
      	$secondary_school = getRecord("schools",$rsEmpEducSeco["SchoolsRefId"],"Name");
      	$secondary_course = getRecord("course",$rsEmpEducSeco["CourseRefId"],"Name");
      	$secondary_datefrom = $rsEmpEducSeco["DateFrom"];
      	$secondary_dateto = $rsEmpEducSeco["DateTo"];
      	$secondary_highest = $rsEmpEducSeco["HighestGrade"];
      	$secondary_year = $rsEmpEducSeco["YearGraduated"];
      	$secondary_honor = $rsEmpEducSeco["Honors"];

      	if ($secondary_dateto == "2999-12-31" || $rsEmpEducSeco["Present"] == 1) $secondary_dateto = "PRESENT";
      	$map["[[ss1]]"]	= dispVal($secondary_school);
		$map["[[sc1]]"]	= dispVal($secondary_course);
		$map["[[sf1]]"]		= dispVal($secondary_datefrom);
		$map["[[st1]]"]		= dispVal($secondary_dateto);
		$map["[[sl1]]"]		= dispVal($secondary_highest);
		$map["[[sy1]]"]		= dispVal($secondary_year);
		$map["[[sh1]]"]		= dispVal($secondary_honor);

   	}
   	if ($rsEmpEducVoca) {
      	$i = 0;
      	while ($row = mysqli_fetch_assoc($rsEmpEducVoca)) {
         	$i++;
         	$vocational_dateto = $row["DateTo"];
         	if ($vocational_dateto == "2999-12-31" || $row["Present"] == 1) $vocational_dateto = "PRESENT";
         	$map["[[vs$i]]"]		= dispVal(getRecord("schools",$row["SchoolsRefId"],"Name"));
         	$map["[[vc$i]]"]		= dispVal(getRecord("course",$row["CourseRefId"],"Name"));
         	$map["[[vf$i]]"]			= dispVal($row["DateFrom"]);
         	$map["[[vt$i]]"]			= dispVal($vocational_dateto);
         	$map["[[vl$i]]"]			= dispVal($row["HighestGrade"]);
         	$map["[[vy$i]]"]			= dispVal($row["YearGraduated"]);
         	$map["[[vh$i]]"]			= dispVal($row["Honors"]);
      	}

   	}

   	if ($rsEmpEducColl) {
      	$i = 0;
      	while ($row = mysqli_fetch_assoc($rsEmpEducColl)) {
	        $i++;
	        $college_dateto = $row["DateTo"];
	        if ($college_dateto == "2999-12-31" || $row["Present"] == 1) $college_dateto = "PRESENT";
         	$map["[[cs$i]]"]		= dispVal(getRecord("schools",$row["SchoolsRefId"],"Name"));
         	$map["[[cc$i]]"]		= dispVal(getRecord("course",$row["CourseRefId"],"Name"));
         	$map["[[cf$i]]"]			= dispVal($row["DateFrom"]);
         	$map["[[ct$i]]"]			= dispVal($college_dateto);
         	$map["[[cl$i]]"]			= dispVal($row["HighestGrade"]);
         	$map["[[cy$i]]"]			= dispVal($row["YearGraduated"]);
         	$map["[[ch$i]]"]			= dispVal($row["Honors"]);
      	}
   	}
   	
   	if ($rsEmpEducGrad) {
      	$i = 0;
      	while ($row = mysqli_fetch_assoc($rsEmpEducGrad)) {
         	$i++;
         	$graduate_dateto = $row["DateTo"];
         	if ($graduate_dateto == "2999-12-31" || $row["Present"] == 1) $graduate_dateto = "PRESENT";
         	$map["[[gs$i]]"]		= dispVal(getRecord("schools",$row["SchoolsRefId"],"Name"));
         	$map["[[gc$i]]"]		= dispVal(getRecord("course",$row["CourseRefId"],"Name"));
         	$map["[[gf$i]]"]			= dispVal($row["DateFrom"]);
         	$map["[[gt$i]]"]			= dispVal($graduate_dateto);
         	$map["[[gl$i]]"]			= dispVal($row["HighestGrade"]);
         	$map["[[gy$i]]"]			= dispVal($row["YearGraduated"]);
         	$map["[[gh$i]]"]				= dispVal($row["Honors"]);
      	}
   	}

   	if ($rsEmpEligibility) {
      	$i = 0;
      	while ($row = mysqli_fetch_array($rsEmpEligibility)) {
         	$i++;
         	$map["[[EL_$i]]"]					=	dispVal(getRecord("CareerService",$row["CareerServiceRefId"],"Name"));
         	$map["[[EL_RA$i]]"]					=	dispVal($row["Rating"]);
         	$map["[[EL_DA$i]]"]					=	dispVal($row["ExamDate"]);
         	$map["[[EL_PL$i]]"]					=	dispVal($row["ExamPlace"]);
         	$map["[[EL_N$i]]"]					=	dispVal($row["LicenseNo"]);
         	$map["[[EL_VAL$i]]"]				=	dispVal($row["LicenseReleasedDate"]);
      	}
   	}
	if ($rsEmpWorkExp) {
      	$i = 0;
      	while ($row = mysqli_fetch_array($rsEmpWorkExp)) {
         	$i++;
         	if (is_numeric($row["SalaryAmount"])) {
	            $salary = number_format($row["SalaryAmount"],2);
         	} else {
            	$salary = $row["SalaryAmount"];
         	}
         	if ($row["Present"] == 1) {
            	$work_end_date = "PRESENT";
         	} else {
            	$work_end_date = $row["WorkEndDate"];
         	}
         	
         	$disp = "";
         	if ($row["SalaryGradeRefId"] > 0) {
            	$disp = dispVal(getRecord("SalaryGrade",$row["SalaryGradeRefId"],"Name"))." - ".dispVal(getRecord("StepIncrement",$row["StepIncrementRefId"],"Name"));
            	$map["[[WE_A$i]]"] = $disp;
         	} else {
            	$disp = dispVal(getRecord("JobGrade",$row["JobGradeRefId"],"Name"))." - ".dispVal(getRecord("StepIncrement",$row["StepIncrementRefId"],"Name"));
            	$map["[[WE_A$i]]"] = $disp;
         	}
         
         	if ($row["isGovtService"]) {
            	$map["[[WE_G$i]]"] = "YES";
         	}
         	else {
            	$map["[[WE_G$i]]"] = "NO";
         	}
            $map["[[WE_DF$i]]"] 		= dispVal($row["WorkStartDate"]);
         	$map["[[WE_DT$i]]"] 		= dispVal($work_end_date);
         	$map["[[WE_P$i]]"] 			= dispVal(getRecord("Position",$row["PositionRefId"],"Name"));
         	$map["[[WE_DAO$i]]"] 		= dispVal(getRecord("Agency",$row["AgencyRefId"],"Name"));
         	$map["[[WE_SAL$i]]"] 		= dispVal($salary);
         	$map["[[WE_SI$i]]"] 		= dispVal(getRecord("EmpStatus",$row["EmpStatusRefId"],"Name"));
      	}
   	}


   	if ($rsEmpVoluntary) {
      	$i = 0;
      	while ($row = mysqli_fetch_array($rsEmpVoluntary)) {
         	$i++;
         	$map["[[VOL_ORG$i]]"] 		= dispVal(getRecord("Organization",$row["OrganizationRefId"],"Name"));
	        $map["[[VOL_DF$i]]"] 		= dispVal($row["StartDate"]);
	        $map["[[VOL_DT$i]]"] 		= dispVal($row["EndDate"]);
	        $map["[[VOL_HR$i]]"] 		= dispVal($row["NumofHrs"]);
	        $map["[[VOL_NW$i]]"] 		= dispVal($row["WorksNature"]);
      	}
   	}

   	if ($rsEmpTraining) {
      	$i = 0;
      	while ($row = mysqli_fetch_array($rsEmpTraining)) {
         	$i++;
         	$string3_L = trim(preg_replace('/\s+/', ' ',dispVal(getRecord("Seminars",$row["SeminarsRefId"],"Name"))));
         	$string3_U = trim(preg_replace('/\s+/', ' ',dispVal(getRecord("Sponsor",$row["SponsorRefId"],"Name"))));
         	$map["[[TP_$i]]"] 		= dispVal($string3_L);
         	$map["[[TP_DF$i]]"] 	= dispVal($row["StartDate"]);
         	$map["[[TP_DT$i]]"] 	= dispVal($row["EndDate"]);
         	$map["[[TP_HR$i]]"] 	= dispVal($row["NumofHrs"]);
         	$map["[[TP_LD$i]]"] 	= dispVal(getRecord("seminarclass",$row["SeminarClassRefId"],'Name'));
         	$map["[[TP_CS$i]]"] 	= dispVal($string3_U);
      	}
   	}

   	if ($rsEmpOtherInfo) {
      	$i = 0;
      	while ($row = mysqli_fetch_array($rsEmpOtherInfo)) {
         	$i++;
         	$map["[[OI_SS$i]]"] 	= dispVal($row["Skills"]);
         	$map["[[OI_R$i]]"] 		= dispVal($row["Recognition"]);
         	$map["[[OI_ORG$i]]"] 	= dispVal($row["Affiliates"]);
      	}
   	}

   	if ($rsEmpReference) {
      	$j = 0;
      	while ($row = mysqli_fetch_array($rsEmpReference)) {
         	$j++;
         $map["[[rn$j]]"] = dispVal($row["Name"]);
         $map["[[ra$j]]"] = dispVal($row["Address"]);
         $map["[[rt$j]]"] = dispVal($row["ContactNo"]);
      	}
   	}

   	

   	if ($rsPDSQ["Q1a"] == 1) {
   		$map["[[q1a]]"] = "<input type='checkbox' disabled checked>";
   		$map["[[q1exp]]"] = dispVal($rsPDSQ["Q1aexp"]);
   	} else {
   		$map["[[q1b]]"] = "<input type='checkbox' disabled checked>";
   	}
   	if ($rsPDSQ["Q1b"] == 1) {
   		$map["[[q2a]]"] = "<input type='checkbox' disabled checked>";
   		$map["[[q2exp]]"] = dispVal($rsPDSQ["Q1bexp"]);
   	} else {
   		$map["[[q2b]]"] = "<input type='checkbox' disabled checked>";
   	}



   	if ($rsPDSQ["Q2a"] == 1) {
   		$map["[[q3a]]"] = "<input type='checkbox' disabled checked>";
   		$map["[[q3exp]]"] = dispVal($rsPDSQ["Q2aexp"]);
   	} else {
   		$map["[[q3b]]"] = "<input type='checkbox' disabled checked>";
   	}


   	if ($rsPDSQ["Q2b"] == 1) {
   		$map["[[q4a]]"] = "<input type='checkbox' disabled checked>";
   		$map["[[q4exp]]"] = dispVal($rsPDSQ["Q2bexp"]);
   		$map["[[q4date]]"] = dispVal($rsPDSQ["Q2DateFiled"]);
   		$map["[[q4case]]"] = dispVal($rsPDSQ["Q2CaseStatus"]);
   	} else {
   		$map["[[q4b]]"] = "<input type='checkbox' disabled checked>";
   	}


   	if ($rsPDSQ["Q3a"] == 1) {
   		$map["[[q5a]]"] = "<input type='checkbox' disabled checked>";
   		$map["[[q5exp]]"] = dispVal($rsPDSQ["Q1aexp"]);
   	} else {
   		$map["[[q5b]]"] = "<input type='checkbox' disabled checked>";
   	}
   	if ($rsPDSQ["Q4a"] == 1) {
   		$map["[[q6a]]"] = "<input type='checkbox' disabled checked>";
   		$map["[[q6exp]]"] = dispVal($rsPDSQ["Q4aexp"]);
   	} else {
   		$map["[[q6b]]"] = "<input type='checkbox' disabled checked>";
   	}
   	if ($rsPDSQ["Q5a"] == 1) {
   		$map["[[q7a]]"] = "<input type='checkbox' disabled checked>";
   		$map["[[q7exp]]"] = dispVal($rsPDSQ["Q5aexp"]);
   	} else {
   		$map["[[q7b]]"] = "<input type='checkbox' disabled checked>";
   	}
   	if ($rsPDSQ["Q5b"] == 1) {
   		$map["[[q8a]]"] = "<input type='checkbox' disabled checked>";
   		$map["[[q8exp]]"] = dispVal($rsPDSQ["Q5bexp"]);
   	} else {
   		$map["[[q8b]]"] = "<input type='checkbox' disabled checked>";
   	}
   	if ($rsPDSQ["Q6a"] == 1) {
   		$map["[[q9a]]"] = "<input type='checkbox' disabled checked>";
   		$map["[[q9exp]]"] = dispVal($rsPDSQ["Q6aexp"]);
   	} else {
   		$map["[[q9b]]"] = "<input type='checkbox' disabled checked>";
   	}
   	if ($rsPDSQ["Q7a"] == 1) {
   		$map["[[q10a]]"] = "<input type='checkbox' disabled checked>";
   		$map["[[q10exp]]"] = dispVal($rsPDSQ["Q7aexp"]);
   	} else {
   		$map["[[q10b]]"] = "<input type='checkbox' disabled checked>";
   	}
   	if ($rsPDSQ["Q7b"] == 1) {
   		$map["[[q11a]]"] = "<input type='checkbox' disabled checked>";
   		$map["[[q11exp]]"] = dispVal($rsPDSQ["Q7bexp"]);
   	} else {
   		$map["[[q11b]]"] = "<input type='checkbox' disabled checked>";
   	}
   	if ($rsPDSQ["Q7c"] == 1) {
   		$map["[[q12a]]"] = "<input type='checkbox' disabled checked>";
   		$map["[[q12exp]]"] = dispVal($rsPDSQ["Q7cexp"]);
   	} else {
   		$map["[[q12b]]"] = "<input type='checkbox' disabled checked>";
   	}
?>