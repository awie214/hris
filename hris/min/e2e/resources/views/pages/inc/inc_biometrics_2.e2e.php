<?php
	include 'conn.e2e.php';
	$bio_arr = array();
	$biometricsID = strtolower($biometricsID);
	$sql = "SELECT * FROM USERINFO where NAME = '$biometricsID'";
	$rs = mysqli_query($bio_conn,$sql);
	if ($rs) {
		while ($row = mysqli_fetch_assoc($rs)) {
			$count = 0;
			$userID = $row["USERID"];
			$where_zk = "WHERE USERID = $userID";
			//$where_zk .= " AND VERIFYCODE = 1";
			$new_sql = "SELECT * FROM CHECKINOUT ".$where_zk;
			$new_rs = mysqli_query($bio_conn,$new_sql);
			if ($new_rs) {
				while ($n_row = mysqli_fetch_assoc($new_rs)) {
					$count++;
					if (get_today_minute(strtotime($n_row["CHECKTIME"])) > 240) {
						$date = date("Y-m-d",strtotime($n_row["CHECKTIME"]));
						$bio_arr[$userID][$date][$count] = $n_row["CHECKTIME"];	
					}
				}
			}
		}
	}
	foreach ($bio_arr as $key => $value) {
		foreach ($value as $nkey => $nvalue) {
			$first 	= reset($nvalue);
			$last 	= end($nvalue);
			$utc_f 	= strtotime($first);
   			$val_f 	= get_today_minute($utc_f);

   			$utc_l 	= strtotime($last);
   			$val_l 	= get_today_minute($utc_l);
			
			$utc 	= strtotime($first);
   			$d 		= date("Y-m-d",$utc);
   			if (count($nvalue) > 1) {
   				if (isset($arr["ARR"][$d]["AttendanceDate"])) {
					if ($arr["ARR"][$d]["AttendanceDate"] == $d) {
						if (get_today_minute($utc_f) <= 720 || get_today_minute($utc_l) <= 720) {
							if ($arr["ARR"][$d]["TimeIn"] == "") {
					    		$arr["ARR"][$d]["TimeIn"] = $val_f;
					    		$arr["ARR"][$d]["UTC"] = $utc_f;
					    	}
						}
			    		if (get_today_minute($utc_f) >= 721 || get_today_minute($utc_l) >= 721) {
					    	if ($arr["ARR"][$d]["TimeOut"] == "") {
					    		$arr["ARR"][$d]["TimeOut"] = $val_l;
					    		$arr["ARR"][$d]["UTC"] = $utc_l;
					    	}
					    }	
			    	}	
				}	
   			} else {
   				$new_value = reset($nvalue);
   				$new_utc = strtotime($new_value);
   				$new_value = get_today_minute($new_utc);
   				if ($new_value <= 720) {
   					if (isset($arr["ARR"][$d]["AttendanceDate"])) {
						if ($arr["ARR"][$d]["AttendanceDate"] == $d) {	
							if ($arr["ARR"][$d]["TimeIn"] == "") {
					    		$arr["ARR"][$d]["TimeIn"] = $new_value;
					    		$arr["ARR"][$d]["UTC"] = $new_utc;
					    	}
						}
					}
   				} else {
   					if (isset($arr["ARR"][$d]["AttendanceDate"])) {
   						if ($arr["ARR"][$d]["AttendanceDate"] == $d) {	
							if ($arr["ARR"][$d]["TimeOut"] == "") {
					    		$arr["ARR"][$d]["TimeOut"] = $val_l;
					    		$arr["ARR"][$d]["UTC"] = $utc_l;
					    	}
					    }
					}
   				}
   			}					
		}
	}
	
?>