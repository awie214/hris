<?php
	include_once 'mdbcn.e2e.php';
	if (isset($connection)) {
	    $query = "SELECT USERID, Badgenumber, Name FROM USERINFO";
	    $result = $connection->query($query)->fetchAll(PDO::FETCH_ASSOC);
	    if ($result) {
	        foreach($result as $row) {
	           	switch ($CompanyID) {
	              	case "2":
	                	$mdb_field = "Name";
	              		break;
	              	default:
	              		$mdb_field = "Badgenumber";
	              		break;
	           	}

	           	if ($row[$mdb_field] == $biometricsID) {
	              	$userID = $row["USERID"];
	              	break;
	           	}
	        }
	    }
	    $query = "SELECT USERID, CHECKTIME, CHECKTYPE, VERIFYCODE, SENSORID, sn FROM CHECKINOUT WHERE USERID = ".$userID." ORDER BY CHECKTIME";
		$result = $connection->query($query)->fetchAll(PDO::FETCH_ASSOC);
	    foreach($result as $row) {
	       	$utc 				= strtotime($row["CHECKTIME"]);
	       	$d 					= date("Y-m-d",$utc);
	    	$mdbTime 			= date("H:i",$utc);
	    	$mdbDate 			= date("Y-m-d",$utc);
			$fld 				= "";
			$val 				= "";
	    	switch ($row["CHECKTYPE"]) {
	          	case "I":
	          		$fld 		= $KEntry_json["FieldTimeIn"];
	          		$val 		= get_today_minute($utc);
	             	break;
	          	case "0":
	          		$fld 		= $KEntry_json["FieldLunchOut"];
	          		$val 		= get_today_minute($utc);
	             	break;
	          	case "1":
	          		$fld 		= $KEntry_json["FieldLunchIn"];
	          		$val 		= get_today_minute($utc);
	             	break;
	          	case "O":
	          		$fld 		= $KEntry_json["FieldTimeOut"];
	          		$val 		= get_today_minute($utc);
	            	break;
	    	}
	    	if (isset($arr["ARR"][$d]["AttendanceDate"])) {
	    		if ($arr["ARR"][$d]["AttendanceDate"] == $d) {
	    			if (isset($arr["ARR"][$d][$fld])) {
	    				if ($arr["ARR"][$d][$fld] == "") {
				    		$arr["ARR"][$d][$fld] = $val;
				    		$arr["ARR"][$d]["UTC"] = $utc;
				    	}		
	    			}
		    	}	
	    	}
		}
	}
?>