<div class="row margin-top">
   <div class="col-xs-6 text-center">
      <b>USED LEAVE/s</b>
      <br>
      <b>FOR THE YEAR</b>
   </div>
   <div class="col-xs-6 text-center">
      <b>BALANCE AS OF</b>
      <br>
      <u>
         <?php echo date("F d, Y",strtotime("last day of previous month")); ?>
      </u>
   </div>
</div>
<div class="row margin-top">
   <div class="col-xs-6">
      <?php
         $curr_year        = date("Y",time());
         $leaves_rs = SelectEach("leaves","");
         if ($leaves_rs) {
            while ($leave_row = mysqli_fetch_assoc($leaves_rs)) {
               $count         = 0;
               $name          = $leave_row["Name"];
               $code          = $leave_row["Code"];
               $leave_refid   = $leave_row["RefId"];
               $where_leave   = "WHERE EmployeesRefId = '$EmpRefId' AND LeavesRefId = '$leave_refid'";
               $where_leave   .= " AND Status = 'Approved' AND YEAR(ApplicationDateFrom) = '$curr_year'";
               $count_leave   = SelectEach("employeesleave",$where_leave);
               if ($count_leave) {
                  while($emp_leave  = mysqli_fetch_assoc($count_leave)) {
                     $from          = $emp_leave["ApplicationDateFrom"];
                     $to            = $emp_leave["ApplicationDateTo"];
                     $days_of_leave = count_leave($EmpRefId,$from,$to);
                     $count         = $count + $days_of_leave;
                  }
               }
               echo '
               <div class="row margin-top">
                  <div class="col-xs-9">
                     '.strtoupper($name).'
                  </div>
                  <div class="col-xs-3 text-center">
                     <span class="badge">'.$count.'</span>
                  </div>
               </div>
               ';
            }
         }
      ?>
   </div>
   <div class="col-xs-6">
      <?php
         $EmpStatusRefId   = FindFirst("empinformation","WHERE EmployeesRefId = '$EmpRefId'","EmpStatusRefId");
         $category         = getRecord("empstatus",$EmpStatusRefId,"category");
         $where_balance    = "WHERE EmployeesRefId = '$EmpRefId' AND EffectivityYear = '$curr_year'";
         $credit_detail    = FindLast("employeescreditbalance",$where_balance,"*");
         if ($credit_detail) {
            $year             = $credit_detail["EffectivityYear"]; 
            $balance_date     = $credit_detail["BegBalAsOfDate"];
            $from             = date("m",strtotime($balance_date." + 1 Day"));
            $to               = "12";
            $to               = intval(date("m",time())) - 1;
            
            $arr_credit       = computeCredit($EmpRefId,$from,$to,$year,$year);
            foreach ($arr_credit as $xkey => $xvalue) {
               if ($xvalue < 0) $xvalue = 0;
               if ($xkey == "OT") {
                  if ($xvalue > 0) {
                     $xvalue = convertToHoursMins($xvalue);
                  } else {
                     $xvalue = 0;
                  }
                  echo '
                  <div class="row margin-top">
                     <div class="col-xs-6">
                        '.strtoupper($xkey).'
                     </div>
                     <div class="col-xs-6">
                        <span class="badge">'.$xvalue.'</span>
                     </div>
                  </div>
                  ';
               } else {
                  if ($category == 1) {
                     if ($credit_detail["CompanyRefId"] == 35) {
                        if ($xkey == "FL") {
                           $xkey = "VL (Force Leave)";
                        }
                     }
                     echo '
                     <div class="row margin-top">
                        <div class="col-xs-6">
                           '.strtoupper($xkey).'
                        </div>
                        <div class="col-xs-6">
                           <span class="badge">'.$xvalue.'</span>
                        </div>
                     </div>
                     ';   
                  }
               }
            }
         } else {
            echo '
            <div class="row margin-top">
               <div class="col-xs-12 text-center">
                  NO SET BALANCE<br>FOR THE YEAR
               </div>
            </div>
            ';   
         }
      ?>
   </div>
</div>