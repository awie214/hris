<?php
   $Division      = "";
   $Position      = "";
   $Office        = "";
   $SalaryAmount  = "";
   $Division      = "";
   $Position      = "";
   $Office        = "";
   $SalaryAmount  = "";
   $SalaryAmount  = "";
   $FullName      = ""; 
   $coc_weekdays  = 0;
   $coc_weekends  = 0;
   $ot_weekdays   = 0;
   $ot_weekends   = 0;

   if (isset($_POST["month_"])) {
      $m = $_POST["month_"];
   } else {
      $m = intval(date("m",time()));
   }
   if (isset($_POST["year_"])) {
      $y = $_POST["year_"];
   } else {
      $y = date("Y",time());
   }
   $month_name = monthName($m,1);
   $emprefid   = getvalue("hEmpRefId");
   $name       = FindFirst("employees","WHERE RefId = '$emprefid'","LastName,FirstName,MiddleName,ExtName");
   if ($name) {
      $LastName   = $name["LastName"];
      $FirstName  = $name["FirstName"];
      $MiddleName = $name["MiddleName"];
      $ExtName    = $name["ExtName"];
      $MiddleName = substr($MiddleName, 0, 1);
      $FullName   = $LastName.", ".$FirstName." ".$MiddleName.". ".$ExtName;
   }

   $flds       = "DivisionRefId,PositionRefId,OfficeRefId,SalaryAmount";
   $info       = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'",$flds);
   if ($info) {
      $Division      = $info["DivisionRefId"];
      $Position      = $info["PositionRefId"];
      $Office        = $info["OfficeRefId"];
      $SalaryAmount  = $info["SalaryAmount"];
      $Division      = getRecord("division",$Division,"Name");
      $Position      = getRecord("Position",$Position,"Name");
      $Office        = getRecord("Office",$Office,"Name");
      $SalaryAmount  = floatval($SalaryAmount);
      $SalaryAmount  = number_format($SalaryAmount,2);
   }
   if ($m <= 9) $m = "0".$m;
   $where = "WHERE EmployeesRefId = '$emprefid' AND Month = '$m' AND Year = '$y'";
   $dtr = FindFirst("dtr_process",$where,"Overtime_Remarks");
?>
<!DOCTYPE>
<html>
<head>
   <title></title>
   <?php include_once $files["inc"]["pageHEAD"]; ?>
   <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
   <script type="text/javascript">
      $(document).ready(function () {
         $("#btnPrint").click(function () {
            var head = $("head").html();
            printDiv('div_CONTENT',head);
         });
         <?php
            if (isset($_POST["month_"])) {
               $m = $_POST["month_"];
            } else {
               $m = intval(date("m",time()));
            }

            if (isset($_POST["year_"])) {
               $y = $_POST["year_"];
            } else {
               $y = date("Y",time());
            }
            echo '$("#month_").val("'.$m.'");';        
            echo '$("#year_").val("'.$y.'");';        
         ?>

      });
   </script>
   <style type="text/css">
      #not_avail {
         font-weight: 900;
         font-size: 15pt;
         padding-left: 9%;
      }
      @media print {
         body {
            font-size: 9pt;
         }
         .footer_div {
            position: fixed;
            right: 0;
            bottom: 0;
            left: 0;
            padding: 3px;
            font-size: 8pt;
         }
      }
      .bold {font-size:600;}
   </style>
</head>
<body onload = "indicateActiveModules();">
   <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
      <?php $sys->SysHdr($sys,"pis"); ?>
      <div class="container-fluid" id="mainScreen">
         <?php doTitleBar (""); ?>
         <div class="container-fluid margin-top">
            <br><br>
            <div class="row">
               <div class="col-xs-2">
                  <select class="form-input" id="month_" name="month_">
                     <option value="0">Select Month</option>
                     <option value="1">January</option>
                     <option value="2">February</option>
                     <option value="3">March</option>
                     <option value="4">April</option>
                     <option value="5">May</option>
                     <option value="6">June</option>
                     <option value="7">July</option>
                     <option value="8">August</option>
                     <option value="9">September</option>
                     <option value="10">October</option>
                     <option value="11">November</option>
                     <option value="12">December</option>
                  </select>
               </div>
               <div class="col-xs-2">
                  <select class="form-input" id="year_" name="year_">
                     <option value="0">Select Year</option>
                     <?php
                        $start   = date("Y",time()) - 1;
                        $end     = date("Y",time());
                        for ($a=$start; $a <= $end ; $a++) { 
                           echo '<option value="'.$a.'">'.$a.'</option>';
                        }
                     ?>
                  </select>
               </div>
               <div class="col-xs-3">
                  <button type="submit" id="" class="btn-cls4-sea">SUBMIT</button>      
                  <button type="button" id="btnPrint" class="btn-cls4-lemon">PRINT</button>      
               </div>
            </div>
            <br><br>
            <div class="row">
               <div class="col-xs-12" id="div_CONTENT">
                  <div class="container-fluid">
                     <div class="row">
                        <div class="col-xs-12">
                           <div class="row">
                              <div class="col-xs-12">
                                 <?php
                                    rptHeader("Authority for Overtime Services");
                                 ?>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <table border="2">
                                    <tr class="ruler">
                                       <td style="width: 8.33%;"></td>
                                       <td style="width: 8.33%;"></td>
                                       <td style="width: 8.33%;"></td>
                                       <td style="width: 8.33%;"></td>
                                       <td style="width: 8.33%;"></td>
                                       <td style="width: 8.33%;"></td>
                                       <td style="width: 8.33%;"></td>
                                       <td style="width: 8.33%;"></td>
                                       <td style="width: 8.33%;"></td>
                                       <td style="width: 8.33%;"></td>
                                       <td style="width: 8.33%;"></td>
                                       <td style="width: 8.33%;"></td>
                                    </tr>
                                    <tr>
                                       <td colspan="12" class="text-center">
                                          <b>STATEMENT OF OVERTIME SERVICES</b>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="6">
                                          <div class="row">
                                             <div class="col-xs-2"></div>
                                             <div class="col-xs-10">
                                                <input type="checkbox" name="">&nbsp;<b>With Pay</b>
                                                <br>
                                                <input type="checkbox" name="">&nbsp;<b>COC</b>
                                             </div>
                                          </div>
                                       </td>
                                       <td colspan="6" valign="top">
                                          <b>Period Covered: <?php echo $month_name." ".$y; ?></b>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="6" valign="top">
                                          <b>Name: <?php echo $FullName; ?></b>
                                       </td>
                                       <td colspan="6" valign="top">
                                          <b>Division/Office: <?php echo $Division." / ".$Office; ?></b>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="6">
                                          <b>Position: <?php echo $Position; ?></b>
                                       </td>
                                       <td colspan="6">
                                          <b>Monthly Salary: P <?php echo $SalaryAmount; ?></b>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="12" class="text-center"><b>WEEKDAYS</b></td>
                                    </tr>
                                    <tr>
                                       <td rowspan="2" colspan="2" class="text-center">
                                          <b>DATE</b>
                                       </td>
                                       <td rowspan="2" colspan="2" class="text-center">
                                          <b>DAY</b>
                                       </td>
                                       <td colspan="2" class="text-center">
                                          <b>MORNING</b>
                                       </td>
                                       <td colspan="2" class="text-center">
                                          <b>AFTERNOON</b>
                                       </td>
                                       <td colspan="2" class="text-center">
                                          <b>EVENING</b>
                                       </td>
                                       <td colspan="2" class="text-center">
                                          <b>OT HOURS</b>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td class="text-center">
                                          <b>IN</b>
                                       </td>
                                       <td class="text-center">
                                          <b>OUT</b>
                                       </td>
                                       <td class="text-center">
                                          <b>IN</b>
                                       </td>
                                       <td class="text-center">
                                          <b>OUT</b>
                                       </td>
                                       <td class="text-center">
                                          <b>IN</b>
                                       </td>
                                       <td class="text-center">
                                          <b>OUT</b>
                                       </td>
                                       <td class="text-center">
                                          <b>HOURS</b>
                                       </td>
                                       <td class="text-center">
                                          <b>MINUTES</b>
                                       </td>
                                    </tr>
                                    <?php
                                       if ($dtr != "") {
                                          $overtime = explode("|", $dtr);
                                          foreach ($overtime as $akey => $avalue) {
                                             if ($avalue != "") {
                                                $obj = explode("_", $avalue);
                                                $type       = $obj[0];
                                                $date       = $obj[1];
                                                $timein     = $obj[2];
                                                $lunchout   = $obj[3];
                                                $lunchin    = $obj[4];
                                                $timeout    = $obj[5];
                                                $time       = $obj[6];
                                                $hrs_min    = rpt_HoursFormat($time);
                                                $hrs        = explode(":", $hrs_min)[0];
                                                $min        = explode(":", $hrs_min)[1];
                                                if (
                                                   date("l",strtotime($date)) != "Sunday" &&
                                                   date("l",strtotime($date)) != "Saturday"
                                                ) {
                                                   if ($type == "COC") $coc_weekdays += $time;
                                                   if ($type == "OT") $ot_weekdays += $time;
                                                   echo '<tr>';
                                                   echo '<td colspan="2">'.date("F d, Y",strtotime($date)).'</td>';
                                                   echo '<td colspan="2">'.date("l",strtotime($date)).'</td>';
                                                   echo '<td class="text-center">'.HrsFormat($timein).'</td>';
                                                   echo '<td class="text-center">'.HrsFormat($lunchout).'</td>';
                                                   echo '<td class="text-center">'.HrsFormat($lunchin).'</td>';
                                                   echo '<td class="text-center">'.HrsFormat($timeout).'</td>';
                                                   echo '<td></td>';
                                                   echo '<td></td>';
                                                   echo '<td class="text-center">'.$hrs.'</td>';
                                                   echo '<td class="text-center">'.$min.'</td>';
                                                   echo '</tr>';
                                                }
                                             }
                                          }
                                       } else {
                                          for ($i=0; $i <= 4; $i++) { 
                                             echo '<tr>';
                                             echo '<td colspan="2">&nbsp;</td>';
                                             echo '<td colspan="2"></td>';
                                             echo '<td></td>';
                                             echo '<td></td>';
                                             echo '<td></td>';
                                             echo '<td></td>';
                                             echo '<td></td>';
                                             echo '<td></td>';
                                             echo '<td></td>';
                                             echo '<td></td>';
                                             echo '</tr>';
                                          }   
                                       }
                                    ?>
                                    <tr>
                                       <td colspan="12" class="text-center"><b>WEEKENDS</b></td>
                                    </tr>
                                    <tr>
                                       <td rowspan="2" colspan="2" class="text-center">
                                          <b>DATE</b>
                                       </td>
                                       <td rowspan="2" colspan="2" class="text-center">
                                          <b>DAY</b>
                                       </td>
                                       <td colspan="2" class="text-center">
                                          <b>MORNING</b>
                                       </td>
                                       <td colspan="2" class="text-center">
                                          <b>AFTERNOON</b>
                                       </td>
                                       <td colspan="2" class="text-center">
                                          <b>EVENING</b>
                                       </td>
                                       <td colspan="2" class="text-center">
                                          <b>OT HOURS</b>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td class="text-center">
                                          <b>IN</b>
                                       </td>
                                       <td class="text-center">
                                          <b>OUT</b>
                                       </td>
                                       <td class="text-center">
                                          <b>IN</b>
                                       </td>
                                       <td class="text-center">
                                          <b>OUT</b>
                                       </td>
                                       <td class="text-center">
                                          <b>IN</b>
                                       </td>
                                       <td class="text-center">
                                          <b>OUT</b>
                                       </td>
                                       <td class="text-center">
                                          <b>HOURS</b>
                                       </td>
                                       <td class="text-center">
                                          <b>MINUTES</b>
                                       </td>
                                    </tr>
                                    <?php
                                       if ($dtr != "") {
                                          $overtime = explode("|", $dtr);
                                          foreach ($overtime as $akey => $avalue) {
                                             if ($avalue != "") {
                                                $obj = explode("_", $avalue);
                                                $type       = $obj[0];
                                                $date       = $obj[1];
                                                $timein     = $obj[2];
                                                $lunchout   = $obj[3];
                                                $lunchin    = $obj[4];
                                                $timeout    = $obj[5];
                                                $time       = $obj[6];
                                                $hrs_min    = rpt_HoursFormat($time);
                                                $hrs        = explode(":", $hrs_min)[0];
                                                $min        = explode(":", $hrs_min)[1];
                                                if (
                                                   date("l",strtotime($date)) == "Sunday" ||
                                                   date("l",strtotime($date)) == "Saturday"
                                                ) {
                                                   if ($type == "COC") $coc_weekends += $time;
                                                   if ($type == "OT") $ot_weekends += $time;
                                                   echo '<tr>';
                                                   echo '<td colspan="2">'.date("F d, Y",strtotime($date)).'</td>';
                                                   echo '<td colspan="2">'.date("l",strtotime($date)).'</td>';
                                                   echo '<td class="text-center">'.HrsFormat($timein).'</td>';
                                                   echo '<td class="text-center">'.HrsFormat($lunchout).'</td>';
                                                   echo '<td class="text-center">'.HrsFormat($lunchin).'</td>';
                                                   echo '<td class="text-center">'.HrsFormat($timeout).'</td>';
                                                   echo '<td></td>';
                                                   echo '<td></td>';
                                                   echo '<td class="text-center">'.$hrs.'</td>';
                                                   echo '<td class="text-center">'.$min.'</td>';
                                                   echo '</tr>';
                                                }
                                             }
                                          }
                                       } else {
                                          for ($i=0; $i <= 4; $i++) { 
                                             echo '<tr>';
                                             echo '<td colspan="2">&nbsp;</td>';
                                             echo '<td colspan="2"></td>';
                                             echo '<td></td>';
                                             echo '<td></td>';
                                             echo '<td></td>';
                                             echo '<td></td>';
                                             echo '<td></td>';
                                             echo '<td></td>';
                                             echo '<td></td>';
                                             echo '<td></td>';
                                             echo '</tr>';
                                          }   
                                       }
                                    ?>
                                    <tr>
                                       <td colspan="6">
                                          <b>TOTAL COC HOURS EARNED</b>
                                       </td>
                                       <td colspan="6">
                                          <b>OT HOURS EARNED WITH PAY</b>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="6">
                                          <div class="row">
                                             <div class="col-xs-12">
                                                Weekday: Hours/+Minutes Rendered x 1.00
                                                <br>
                                                Weekend: Hours/+Minutes Rendered x 1.50
                                             </div>
                                          </div>
                                       </td>
                                       <td colspan="6">
                                          <div class="row">
                                             <div class="col-xs-12">
                                                Weekday: (Monthly Salary/22/8) x 1.25 x No. of Hours Rendered
                                                <br>
                                                Weekend: (Monthly Salary/22/8) x 1.50 x No. of Hours Rendered
                                             </div>
                                          </div>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="3">
                                          <b>Weekdays</b>
                                       </td>
                                       <td colspan="3" class="text-center">
                                          <b><?php echo rpt_HoursFormat($coc_weekdays); ?></b>
                                       </td>
                                       <td colspan="3">
                                          <b>Weekdays</b>
                                       </td>
                                       <td colspan="3" class="text-center">
                                          <b><?php echo rpt_HoursFormat($ot_weekdays); ?></b>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="3">
                                          <b>Weekends</b>
                                       </td>
                                       <td colspan="3" class="text-center">
                                          <b><?php echo rpt_HoursFormat($coc_weekends); ?></b>
                                       </td>
                                       <td colspan="3">
                                          <b>Weekends</b>
                                       </td>
                                       <td colspan="3" class="text-center">
                                          <b><?php echo rpt_HoursFormat($ot_weekends); ?></b>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="3">
                                          <b>TOTAL</b>
                                       </td>
                                       <td colspan="3" class="text-center">
                                          <b><?php echo rpt_HoursFormat($coc_weekdays + $coc_weekends); ?></b>
                                       </td>
                                       <td colspan="3">
                                          <b>TOTAL</b>
                                       </td>
                                       <td colspan="3" class="text-center">
                                          <b><?php echo rpt_HoursFormat($ot_weekdays + $ot_weekends); ?></b>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td rowspan="3" colspan="3"></td>
                                       <td colspan="3">
                                          <b>A Total OT Pay for Weekdays</b>
                                       </td>
                                       <td colspan="3"></td>
                                       <td rowspan="3" colspan="3"></td>
                                    </tr>
                                    <tr>
                                       <td colspan="3">
                                          <b>B Total OT Pay for Weekends</b>
                                       </td>
                                       <td colspan="3"></td>
                                    </tr>
                                    <tr>
                                       <td colspan="3">
                                          <b>C Total OT Pay (A + B)</b>
                                       </td>
                                       <td colspan="3"></td>
                                    </tr>
                                    <tr>
                                       <td colspan="12">
                                          <div class="row">
                                             <div class="col-xs-12">
                                                I hereby certify that the services have been rendered under my direct supervision and the time indicated aboved has been checked against the employee's Daily Time Record.
                                             </div>
                                          </div>
                                          <br><br>
                                          <div class="row">
                                             <div class="col-xs-8"></div>
                                             <div class="col-xs-4 text-center">
                                                <b>JENNIFER J. TAN</b>
                                                <br>
                                                <b>Director, Administrative and Financial Office</b>
                                                <br>
                                                <b>OIC-Human Resources Management Unit</b>
                                                <br>
                                                <b>Date:_________________</b>
                                             </div>
                                          </div>
                                       </td>
                                    </tr>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php
            footer();
            include "varHidden.e2e.php";
         ?>
      </div>
   </form>
</body>
</html>
