<?php 
   $emprefid = $_GET["hEmpRefId"];
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
   </head>
   <body>
      <form action="uploadPCR.php" method="post" enctype="multipart/form-data">
         <?php $sys->SysHdr($sys,"spms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("PERFORMANCE COMMITMENT REVIEW"); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div id="divList">
                        <div class="mypanel">
                           <div class="panel-top">LIST OF PERFORMANCE COMMITMENT REVIEW</div>
                           <div class="panel-mid">
                              <span id="spGridTable">
                                 <?php
                                    if ($UserCode == "COMPEMP") {
                                       $sql = "SELECT * FROM `spms_pcr` WHERE EmployeesRefId = '$emprefid'";
                                       $Action = [false,false,false,true];
                                    } else {
                                       $sql = "SELECT * FROM `spms_pcr`";   
                                       $Action = [false,true,true,true];
                                    }
                                    $table = "spms_pcr";
                                    $sizeCol = "col-xs-6";
                                    $gridTableHdr_arr = ["Employees Name", "Semester", "Year"];
                                    $gridTableFld_arr = ["EmployeesRefId", "Semester", "Year"];

                                    
                                    
                                    doGridTable($table,
                                                $gridTableHdr_arr,
                                                $gridTableFld_arr,
                                                $sql,
                                                $Action,
                                                "gridTable");
                                 ?>
                              </span>
                           </div>
                           <div class="panel-bottom">
                              <?php
                                 if ($UserCode != "COMPEMP") {
                                    btnINRECLO([true,true,false]);
                                 }
                              ?>
                           </div>
                        </div>
                     </div>
                     <div id="divView">
                        <div class="row" id="EntryScrn">
                           <div class="col-xs-12">
                              <div class="mypanel">
                                 <div class="panel-top">
                                    <span id="ScreenMode">ADD NEW PERFORMANCE COMMITMENT REVIEW
                                 </div>
                                 <div class="panel-mid">
                                    <div class="row">
                                       <div class="col-xs-12">
                                          <div class="row">
                                             <div class="col-xs-6">
                                                <label class="control-label" for="inputs">Employee Name:</label><br>
                                                <select class="form-input saveFields--" name="sint_EmployeesRefId" id="sint_EmployeesRefId">
                                                   <option value="0">Select Employee</option>
                                                   <?php
                                                      $rs = SelectEach("employees"," ORDER BY LastName");
                                                      while ($row = mysqli_fetch_assoc($rs)) {
                                                         $refid         = $row["RefId"];
                                                         $LastName      = $row["LastName"];
                                                         $FirstName     = $row["FirstName"];
                                                         $MiddleName    = $row["MiddleName"];
                                                         $FullName      = $LastName.", ".$FirstName." ".substr($MiddleName, 0, 1);
                                                         echo '<option value="'.$refid.'">'.$FullName.'</option>';
                                                      }
                                                   ?>
                                                </select>
                                             </div>
                                             <div class="col-xs-6">
                                                <label class="control-label" for="inputs">Position:</label><br>
                                                <?php
                                                   createSelect("Position",
                                                                "sint_PositionRefId",
                                                                "",100,"Name","Select Position","");
                                                ?>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="col-xs-6">
                                                <label class="control-label" for="inputs">Department:</label><br>
                                                <?php
                                                   createSelect("Department",
                                                                "sint_DepartmentRefId",
                                                                "",100,"Name","Select Department","");
                                                ?>
                                             </div>
                                             <div class="col-xs-6">
                                                <label class="control-label" for="inputs">Division:</label><br>
                                                <?php
                                                   createSelect("Division",
                                                                "sint_DivisionRefId",
                                                                "",100,"Name","Select Division","");
                                                ?>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="col-xs-12">
                                                <div class="row margin-top">
                                                   <div class="col-xs-4">
                                                      <label>Type:</label>
                                                      <select class="form-input saveFields--" name="char_PCRType">
                                                         <option value="">Select Type</option>
                                                         <option value="IPCR-RS">IPCR-RS</option>
                                                         <option value="IPCR-SD">IPCR-SD</option>
                                                         <option value="DivPCR">DivPCR</option>
                                                         <option value="DPCR">DPCR</option>
                                                         <option value="OPCR">OPCR</option>
                                                         <option value="FPCR">FPCR</option>
                                                      </select>
                                                   </div>
                                                   <div class="col-xs-4">
                                                      <label>Semester:</label>
                                                      <select class="form-input saveFields--" name="char_Semester">
                                                         <option value="January - June">January - June</option>
                                                         <option value="July - December">July - December</option>
                                                      </select>
                                                   </div>
                                                   <div class="col-xs-4">
                                                      <label>Year:</label>
                                                      <select class="form-input saveFields-- mandatory" 
                                                             name="date_YearPerformed"
                                                             id="date_YearPerformed">
                                                             <?php
                                                               for ($i=(date("Y",time()) - 5); $i <= (date("Y",time()) + 5); $i++) { 
                                                                  echo '<option value="'.$i.'">'.$i.'</option>';
                                                               }
                                                             ?>
                                                      </select>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-4">
                                                      <label>Average:</label><br>
                                                      <input type="text" class="form-input saveFields-- number--" name="bint_OverallScore" placeholder="LIMIT IS 5">
                                                   </div>
                                                   <div class="col-xs-4">
                                                      <label>Rating:</label><br>
                                                      <input type="text" class="form-input saveFields-- number--" name="bint_NumericalRating" readonly>
                                                   </div>
                                                   <div class="col-xs-4">
                                                      <label>Adjectival:</label><br>
                                                      <select class="form-input saveFields-- mandatory" name="char_Adjectival" readonly>
                                                         <option value="5">Outstanding</option>
                                                         <option value="4">Very Satisfactory</option>
                                                         <option value="3">Satisfactory</option>
                                                         <option value="2">Unsatisfactory</option>
                                                         <option value="1">Poor</option>
                                                      </select>
                                                   </div>
                                                   
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-4">
                                                      <label>File:</label><br>
                                                      <input type="file" class="form-input saveFields--" name="char_File" required>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel-bottom">
                                    <button type="submit" class="btn-cls4-sea" name="submit" id="submit">SAVE</button>
                                    <?php
                                       btnSACABA([false,true,true]);
                                    ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
   <script type="text/javascript">
      $(document).ready(function () {
         <?php
            if (isset($_GET["err"])) echo '$.notify("'.$_GET["err"].'");';
         ?>
         remIconDL();

         $("#sint_EmployeesRefId").change(function () {
            var emprefid = $(this).val();
            if (emprefid > 0) {
               $.get("trn.e2e.php",
               {
                  emprefid: emprefid,
                  fn: "getEmpinfo"
               },
               function(data,status) {
                  if (status == "success") {
                     try {
                        eval(data);
                     } catch (e) {
                        if (e instanceof SyntaxError) {
                           alert(e.message);
                        }
                     }
                  }
               });  
            }
         });
         $("[name='bint_OverallScore']").blur(function () {
            var val = $(this).val(); 
            if (parseFloat(val) > 5) {
               alert("The limit is 1 to 5 only");
               $("[name='bint_OverallScore'], [name='bint_NumericalRating'], [name='char_Adjectival']").val("");
               return false;
            } else {
               val = Math.trunc(val);
               $("[name='bint_NumericalRating']").val(val);
               $("[name='char_Adjectival']").val(val);   
            }
            
         });
      });
      function afterNewSave() {
         alert("Successfully Saved");
         gotoscrn($("#hProg").val(),"");
      }
      function selectMe(refid){
         $("#rptContent").attr("src","blank.htm");
         var rptFile = "view_PDF";
         var url = "ReportCaller.e2e.php?file=" + rptFile;
         url += "&refid=" + refid;
         url += "&" + $("[name='hgParam']").val();
         $("#prnModal").modal();
         $("#rptContent").attr("src",url);
      }
   </script>
</html>