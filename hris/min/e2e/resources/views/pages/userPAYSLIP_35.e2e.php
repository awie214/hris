<?php
   function getAmount($table,$where) {
      $result = pms_FindFirst($table,$where,"amount");
      if ($result) {
         return $result;
      } else {
         return 0;
      }
   }
   if (isset($_POST["month_"])) {
      $m = $_POST["month_"];
   } else {
      $m = intval(date("m",time()));
   }
   if (isset($_POST["year_"])) {
      $y = $_POST["year_"];
   } else {
      $y = date("Y",time());
   }
   $month_name = monthName($m,1);
   $pera_id    = 0;
   $rata1_id   = 0;
   $rata2_id   = 0;
   $sa_id      = 0;
   $la_id      = 0;
   $hazard_id  = 0;
   $emprefid   = getvalue("hEmpRefId");
   $AgencyId   = FindFirst("employees","WHERE RefId = '$emprefid'","AgencyId");
   $emp_id     = pms_FindFirst("pms_employees","WHERE employee_number = '$AgencyId'","id");
   $row_emp    = FindFirst("employees","WHERE RefId = '$emprefid'","`LastName`,`FirstName`,`MiddleName`,`ExtName`");
   if ($row_emp) {
      $FullName = $row_emp["LastName"]." ".$row_emp["FirstName"]." ".substr($row_emp["MiddleName"], 0, 1);
   } else {
      $FullName = "";
   }
   $emp_info   = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","`PositionRefId`,`OfficeRefId`,`DivisionRefId`");
   if ($emp_info) {
      $Position = $emp_info["PositionRefId"];
      $Position = getRecord("Position",$Position,"Name");
      $Office   = $emp_info["OfficeRefId"];
      $Office   = getRecord("Office",$Office,"Name");
      $Division = $emp_info["DivisionRefId"];
      $Division = getRecord("Division",$Division,"Name");
   } else {
      $Position = $Office = $Division = "";
   }
   $benefits_rs   = pms_SelectEach("pms_benefits","");
   if ($benefits_rs) {
      while ($benefit_row = mysqli_fetch_assoc($benefits_rs)) {
         $code = $benefit_row["code"];
         switch ($code) {
            case 'PERA':
               $pera_id    = $benefit_row["id"];
               break;
            case 'RATA1':
               $rata1_id   = $benefit_row["id"];
               break;
            case 'RATA2':
               $rata2_id   = $benefit_row["id"];
               break;
            case 'HP':
               $hazard_id  = $benefit_row["id"];
               break;
            case 'SA':
               $sa_id      = $benefit_row["id"]; 
               break;
            case 'LA':
               $la_id      = $benefit_row["id"];
               break;
         }
      }
   }
   

   $trn_where     = "WHERE employee_id = '$emp_id' AND year = '$y' AND month = '$m'";
   $trn_row       = pms_FindFirst("pms_transactions",$trn_where,"*");

   $pera_where    = $trn_where." AND benefit_id = '$pera_id'";
   $rata1_where   = $trn_where." AND benefit_id = '$rata1_id'";
   $rata2_where   = $trn_where." AND benefit_id = '$rata2_id'";
   $sa_where      = $trn_where." AND benefit_id = '$sa_id'";
   $la_where      = $trn_where." AND benefit_id = '$la_id'";
   $hazard_where  = $trn_where." AND benefit_id = '$hazard_id'";

   
   if ($trn_row) {
      $basic      = $trn_row["actual_basicpay_amount"];
      $gsis       = $trn_row["gsis_ee_share"];
      $tax_amount = $trn_row["tax_amount"];
      $total_loan = $trn_row["total_loan"];
      $total_otherdeduct = $trn_row["total_otherdeduct"];
      $all_deduct = $total_loan + $total_otherdeduct;

      $pera       = getAmount("pms_benefitinfo_transactions",$pera_where);
      $rata1      = getAmount("pms_benefitinfo_transactions",$rata1_where);
      $rata2      = getAmount("pms_benefitinfo_transactions",$rata2_where);
      $hazard     = getAmount("pms_benefitinfo_transactions",$hazard_where);
      $la         = getAmount("pms_benefitinfo_transactions",$la_where);
      $sa_row     = pms_FindFirst("pms_benefitinfo_transactions",$la_where,"*");
      if ($sa_row) {
         $sa_amount     = $sa_row["amount"];
         $sa_UT_amount  = $sa_row["sala_undertime_amount"];
         $sa_Ab_amount  = $sa_row["sala_absent_amount"];
      } else {
         $sa_amount     = 0;
         $sa_UT_amount  = 0;
         $sa_Ab_amount  = 0;
      }

      $payroll_info = pms_FindFirst("pms_payroll_information","WHERE employee_id = '$emp_id'","*");
      if ($payroll_info) {
         $pagibig          = $payroll_info["pagibig_contribution"];
         $phic             = $payroll_info["philhealth_contribution"];
         $pagibig_personal = $payroll_info["pagibig_personal"];
         $pagibig2         = $payroll_info["pagibig2"];
      } else {
         $pagibig          = 0;
         $phic             = 0;
         $pagibig_personal = 0;
         $pagibig2         = 0;
      }
   } else {
      $total_loan    = 0;
      $pagibig_personal = 0;
      $pagibig2         = 0;
      $deduct        = 0;
      $pagibig       = 0;
      $phic          = 0;
      $tax_amount    = 0;
      $gsis          = 0;
      $basic         = 0;
      $pera          = 0;
      $rata1         = 0;
      $rata2         = 0;
      $hazard        = 0;
      $la            = 0;
      $sa_row        = 0;
      $sa_amount     = 0;
      $sa_UT_amount  = 0;
      $sa_Ab_amount  = 0;
      $all_deduct    = 0;
   }
   $rata          = floatval($rata1) + floatval($rata2); 
   $gross         = $basic + $pera + $rata + $hazard + $sa_amount + $la;
   $total_deduct  = $total_loan + $gsis + $phic + $pagibig + $tax_amount + $pagibig2 + $pagibig_personal;
   $net_income    = $gross - $total_deduct;
   $mandatory_deduct = $gsis + $phic + $pagibig + $tax_amount;
?>
<!DOCTYPE>
<html>
<head>
	<title></title>
	<?php include_once $files["inc"]["pageHEAD"]; ?>
	<script type="text/javascript">
      $(document).ready(function () {
         $("#btnPrint").click(function () {
            var head = $("head").html();
            printDiv('div_CONTENT',head);
         });
         <?php
            if (isset($_POST["month_"])) {
               $m = $_POST["month_"];
            } else {
               $m = intval(date("m",time()));
            }

            if (isset($_POST["year_"])) {
               $y = $_POST["year_"];
            } else {
               $y = date("Y",time());
            }
            echo '$("#month_").val("'.$m.'");';        
            echo '$("#year_").val("'.$y.'");';        
         ?>
      });
   </script>
   <style type="text/css">
      td {
         padding: 3px;
         font-size: 10pt;
      }
      body {
         font-size: 10pt;
      }
      @media print {
         body, td, h3 {
            padding: 1px;
            font-size: 9pt;
         }
      }
   </style>
</head>
<body onload = "indicateActiveModules();">
   <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
      <?php $sys->SysHdr($sys,"pis"); ?>
      <div class="container-fluid" id="mainScreen">
         <?php doTitleBar ("PAYROLL"); ?>
         <div class="container-fluid margin-top">
            <br><br>
            <div class="row">
               <div class="col-xs-2">
                  <select class="form-input" id="month_" name="month_">
                     <option value="0">Select Month</option>
                     <option value="1">January</option>
                     <option value="2">February</option>
                     <option value="3">March</option>
                     <option value="4">April</option>
                     <option value="5">May</option>
                     <option value="6">June</option>
                     <option value="7">July</option>
                     <option value="8">August</option>
                     <option value="9">September</option>
                     <option value="10">October</option>
                     <option value="11">November</option>
                     <option value="12">December</option>
                  </select>
               </div>
               <div class="col-xs-2">
                  <select class="form-input" id="year_" name="year_">
                     <option value="0">Select Year</option>
                     <?php
                        $start   = date("Y",time()) - 1;
                        $end     = date("Y",time());
                        for ($a=$start; $a <= $end ; $a++) { 
                           echo '<option value="'.$a.'">'.$a.'</option>';
                        }
                     ?>
                  </select>
               </div>
               <div class="col-xs-3">
                  <button type="submit" id="" class="btn-cls4-sea">SUBMIT</button>      
                  <button type="button" id="btnPrint" class="btn-cls4-lemon">PRINT</button>      
               </div>
            </div>
            <br><br>
            <div class="row">
               <div class="col-xs-10" id="div_CONTENT">
                  <div class="container-fluid rptBody">
                     <div class="row">
                        <div class="col-xs-6">
                           <div class="row">
                           	<div class="col-xs-12">
                           		<?php rptHeader("Payroll Payment Slip","HRDMS-R-004"); ?>
                           	</div>
                           </div>
                           <table border="1" style="border: 2px solid black; width: 100%;">
                              <tr>
                                 <td style="width: 25%;">
                                    <b>Pay Period:</b>
                                 </td>
                                 <td style="width: 75%;">
                                    <?php
                                       echo $month_name." ".$y;
                                    ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <b>Employee Name:</b>
                                 </td>
                                 <td>
                                    <?php
                                       echo $FullName;
                                    ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <b>Employee No:</b>
                                 </td>
                                 <td>
                                    <?php
                                       echo $AgencyId;
                                    ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <b>Division:</b>
                                 </td>
                                 <td>
                                    <?php
                                       echo $Division;
                                    ?>
                                 </td>
                              </tr>
                           </table>
                           <br>
                           <table border="1" style="border: 2px solid black; width: 100%;">
                              <tr>
                                 <td colspan="2" class="text-center">
                                    <b>EARNINGS</b>
                                 </td>
                              </tr>
                              <tr>
                                 <td style="width: 75%;">
                                    <b>Salary Basic:</b>
                                 </td>
                                 <td style="width: 25%;" class="text-right">
                                    <?php
                                       echo number_format($basic,2);
                                    ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <b>Salary Differential:</b>
                                 </td>
                                 <td>
                                    <?php
                                       
                                    ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <b>Gross Earnings:</b>
                                 </td>
                                 <td class="text-right">
                                    <?php
                                       echo number_format($basic,2);
                                    ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <b>Personnel Economic Relief Allowance:</b>
                                 </td>
                                 <td class="text-right">
                                    <?php
                                       echo number_format($pera,2);
                                    ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td class="text-right">
                                    <b>TOTAL EARNINGS:</b>
                                 </td>
                                 <td class="text-right">
                                    <?php
                                       echo number_format($gross,2);
                                    ?>
                                 </td>
                              </tr>
                           </table>
                           <br>
                           <table border="1" style="border: 2px solid black; width: 100%;">
                              <tr>
                                 <td colspan="2" class="text-center">
                                    <b>DEDUCTIONS</b>
                                 </td>
                              </tr>
                              <tr>
                                 <td style="width: 75%;">
                                    <b>BIR Withholding Tax:</b>
                                 </td>
                                 <td style="width: 25%;" class="text-right">
                                    <?php
                                       echo number_format($tax_amount,2);
                                    ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <b>GSIS Life & Ret. Contribution:</b>
                                 </td>
                                 <td class="text-right">
                                    <?php
                                       echo number_format($gsis,2);
                                    ?>
                                 </td>
                              </tr>
                              <?php
                                 $pms_loan = pms_SelectEach("pms_loaninfo_transactions",$trn_where);
                                 if ($pms_loan) {
                                    while ($loan_row = mysqli_fetch_assoc($pms_loan)) {
                                       $loan_id = $loan_row["loan_id"];
                                       $loan_name = pms_FindFirst("pms_loans","WHERE id = '$loan_id'","name");
                                       echo '<tr>';
                                       echo '<td>&nbsp;&nbsp;&nbsp;<b>';
                                       echo strtoupper($loan_name);
                                       echo '</b></td>';
                                       echo '<td class="text-right">';
                                       echo number_format($loan_row["amount"],2);
                                       echo '</td>';
                                       echo '</tr>';
                                    }
                                 }
                              ?>
                              <tr>
                                 <td>
                                    <b>HDMF Contribution:</b>
                                 </td>
                                 <td class="text-right">
                                    <?php
                                       echo number_format($pagibig,2);
                                    ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <b>Pagibig Personal:</b>
                                 </td>
                                 <td class="text-right">
                                    <?php
                                       echo number_format($pagibig_personal,2);
                                    ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <b>MP2:</b>
                                 </td>
                                 <td class="text-right">
                                    <?php
                                       echo number_format($pagibig2,2);
                                    ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <b>PHILHEALTH DEDUCTIONS:</b>
                                 </td>
                                 <td class="text-right">
                                    <?php
                                       echo number_format($phic,2);
                                    ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td class="text-right">
                                    <b>TOTAL DEDUCTIONS:</b>
                                 </td>
                                 <td class="text-right">
                                    <?php
                                       echo number_format($total_deduct,2);
                                    ?>
                                 </td>
                              </tr>
                           </table>
                           <br>
                           <table border="1" style="border: 2px solid black; width: 100%;">
                              <tr>
                                 <td style="width: 75%;" class="text-right">
                                    <b>MONTHLY PAY:</b>
                                 </td>
                                 <td style="width: 25%;" class="text-right">
                                    <?php
                                       echo number_format(($gross - $total_deduct),2);
                                    ?>
                                 </td>
                              </tr>
                           </table>
                           <br>
                           <?php
                              $net_income    = $gross - $total_deduct;
                              $check         = intval($net_income) % 2;
                              $first_half    = $net_income - $check; 
                              $first_half    = intval($first_half);
                              $second_half   = $net_income - ($first_half / 2 );
                           ?>
                           <table border="1" style="border: 2px solid black; width: 50%;">
                              <tr>
                                 <td style="width: 33.33%;" class="text-right">
                                    <b>1st PAY:</b>
                                 </td>
                                 <td style="width: 33.33%;">&nbsp;</td>
                                 <td style="width: 33.33%" class="text-right">
                                    <?php
                                       echo number_format((intval($first_half) / 2 ),2); 
                                    ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td style="width: 33.33%;" class="text-right">
                                    <b>2nd PAY:</b>
                                 </td>
                                 <td style="width: 33.33%;">&nbsp;</td>
                                 <td style="width: 33.33%" class="text-right">
                                    <?php
                                       echo number_format($second_half,2); 
                                    ?>
                                 </td>
                              </tr>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php
            footer();
            include "varHidden.e2e.php";
         ?>
      </div>
   </form>
</body>
</html>