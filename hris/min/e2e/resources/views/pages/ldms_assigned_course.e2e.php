<?php 
   $module = module("AssignedCourse"); 
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript">
         $(document).ready(function () {
            remIconDL();
            $("#LocSAVE").click(function () {
               if (saveProceed() == 0) {
                  $(this).attr("type","submit"); 
               }
            });
            <?php
               if (isset($_GET["errmsg"])) {
                  echo '$.notify("'.$_GET["errmsg"].'");';
               }
            ?>
         });
         
         function afterDelete() {
            alert("Successfully Deleted");
            gotoscrn($("#hProg").val(),"");
         }
      </script>
   </head>
   <body>
      <form name="xForm" method="post" action="postMultiple.e2e.php">
         <?php $sys->SysHdr($sys,"ldms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("Assigned Courses"); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                        <div id="divList">
                           <div class="mypanel">
                              <div class="panel-top">List of Assigned Courses</div>
                              <div class="panel-mid">
                                 <span id="spGridTable">
                                    <?php
                                       $table = "assigned_courses";
                                       $sizeCol = "col-xs-6";
                                       $gridTableHdr_arr = ["File Date", "Employees Name", "Course", "Status"];
                                       $gridTableFld_arr = ["FiledDate", "EmployeesRefId", "LDMSLNDProgramRefId", "Remarks"];
                                       $sql = "SELECT * FROM `assigned_courses` ORDER BY FiledDate DESC";
                                       $Action = [false,true,true,false];
                                       doGridTable($table,
                                                   $gridTableHdr_arr,
                                                   $gridTableFld_arr,
                                                   $sql,
                                                   $Action,
                                                   "gridTable");
                                    ?>
                                 </span>
                              </div>
                              <div class="panel-bottom">
                                 <?php
                                    btnINRECLO([true,true,false]);
                                    echo '
                                       <!--<button type="button"
                                            class="btn-cls4-lemon trnbtn"
                                            id="btnPRINT" name="btnPRINT">
                                          <i class="fa fa-print" aria-hidden="true"></i>&nbsp;
                                          PRINT
                                       </button>-->
                                    ';
                                 ?>
                              </div>
                           </div>
                        </div>
                        <div id="divView">
                           <div class="row">
                              <div class="col-xs-6">
                                 <div class="mypanel">
                                    <div class="panel-top">
                                       <span id="ScreenMode">ASSIGNED COURSES
                                    </div>
                                    <div class="panel-mid">
                                       <div id="EntryScrn">
                                          <div class="row margin-top">
                                             <div class="<?php echo $sizeCol; ?>">
                                                <div class="row" id="badgeRefId">
                                                   <div class="col-xs-6">
                                                      <ul class="nav nav-pills">
                                                         <li class="active" style="font-size:12pt;font-weight:600;">
                                                            <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                                                            </span></a>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                                <?php
                                                   $attr = ["br"=>true,
                                                            "type"=>"text",
                                                            "row"=>true,
                                                            "name"=>"date_FiledDate",
                                                            "col"=>"4",
                                                            "id"=>"FiledDate",
                                                            "label"=>"Date File",
                                                            "class"=>"saveFields-- mandatory date--",
                                                            "style"=>"",
                                                            "other"=>""];
                                                   $form->eform($attr);
                                                ?>
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <label>Course Title</label>
                                                      <?php
                                                         createSelect("LDMSLNDProgram",
                                                                   "sint_LDMSLNDProgramRefId",
                                                                   "",100,
                                                                   "Name",
                                                                   "Select Course","required");
                                                      ?>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <label>Status</label>
                                                      <select class="form-input saveFields--" name="char_Remarks" id="char_Remarks">
                                                         <option value="">Select Status</option>
                                                         <option value="Completed">Completed</option>
                                                         <option value="Pending for Requirements">Pending for Requirements</option>
                                                         <option value="Ongoing">Ongoing</option>
                                                      </select>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-xs-6 padd5 adminUse--">
                                                <label>Employees Selected</label>
                                                <select multiple id="EmpSelected" name="EmpSelected" class="form-input" style="height:250px;">
                                                </select>
                                                <p>Double Click the items to remove in the List</p>
                                                <input type="hidden" class="saveFields--" name="hEmpSelected">
                                             </div>
                                          </div>
                                       </div>
                                       <?php
                                          spacer(10);
                                          echo
                                          '<button type="button" class="btn-cls4-sea"
                                                   name="btnLocSAVE" id="LocSAVE">
                                             <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                             &nbsp;Save
                                          </button>';
                                          btnSACABA([false,true,true]);
                                       ?>
                                    </div>
                                    <div class="panel-bottom"></div>
                                 </div>
                              </div>

                              <div class="col-xs-6 adminUse--">
                                 <div class="mypanel">
                                    <div class="panel-top">Employees List</div>
                                    <div class="panel-mid">
                                       <?php include_once "incEmpListSearchCriteria.e2e.php" ?>
                                       <!-- <a href="javascript:void(0);" title="Search Employees">
                                          <i class="fa fa-search" aria-hidden="true"></i>
                                       </a>-->
                                       <div id="empList" style="max-height:300px;overflow:auto;">
                                          <h4>No Employees Selected</h4>
                                       </div>
                                    </div>
                                    <div class="panel-bottom">
                                       <a href="javascript:void(0);" id="clearEmpSelected">Clear Employees Selected</a>&nbsp;
                                    </div>
                                 </div>
                              </div>

                           </div>
                        </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varJSON.e2e.php";
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>

   </body>
</html>