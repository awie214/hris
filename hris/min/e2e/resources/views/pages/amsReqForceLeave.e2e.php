<?php 
   $module = module("ReqForceLeave"); 
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript">
         $(document).ready(function () {
            $("[name='hSaveType']").val("Multiple");
            $("#cancelLeave").click(function () {
               $("#modalReject").modal();
            });
            $("[name='btnPostReason']").click(function () {
               var reason = $("[name='char_Reason']").val();
               if (reason == "") {
                  alert("Please Input why do you need to cancel this leave");
                  return false;
               } else {
                  if (confirm("Do you want to cancel this leave?")) {
                     cancelLeave($("#hRefId").val(),reason);
                  }   
               }
               
            });
         });
         function cancelLeave(refid,reason) {
            $.post("trn.e2e.php",
            {
               fn:"cancelLeave",
               leaverefid:refid,
               reason:reason
            },
            function(data,status){
               if (status == 'success') {
                  eval(data);
                  gotoscrn($("#hProg").val(),"");
               }
            });
         }
         function afterDelete() {
            alert("Successfully Deleted");
            gotoscrn($("#hProg").val(),"");
         }
      </script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="postMultiple.e2e.php">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar($paramTitle); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div id="divList">
                        <div class="mypanel">
                           <div class="panel-top">List for Cancellation of Leave</div>
                           <div class="panel-mid">
                              <span id="spGridTable">
                                 <?php
                                    $sizeCol = "col-sm-6";
                                    if ($UserCode == "COMPEMP") {
                                       $sizeCol = "col-sm-12";
                                       $gridTableHdr_arr = ["Leave Type","Application Date From", "Application Date To", "Status"];
                                       $gridTableFld_arr = ["LeavesRefId","ApplicationDateFrom", "ApplicationDateTo", "Status"];
                                       $sql = "SELECT * FROM $table WHERE CompanyRefId = $CompanyId AND BranchRefId = $BranchId AND EmployeesRefId = $EmployeesRefId ORDER BY FiledDate";
                                       $Action = [true,true,false,false];
                                    }
                                    doGridTable($table,
                                                $gridTableHdr_arr,
                                                $gridTableFld_arr,
                                                $sql,
                                                $Action,
                                                "gridTable");
                                 ?>
                              </span>
                              <?php
                                    btnINRECLO([false,true,false]);
                              ?>
                           </div>
                           <div class="panel-bottom"></div>
                        </div>
                     </div>
                     <div id="divView">
                        <div class="row">
                           <div class="col-xs-6">
                              <div class="mypanel">
                                 <div class="panel-top">
                                    <span id="ScreenMode">AVAILING</span> <?php echo strtoupper($ScreenName); ?>
                                 </div>
                                 <div class="panel-mid">
                                    <div id="EntryScrn">
                                       <div class="row margin-top">
                                          <div class="<?php echo $sizeCol; ?>">
                                                <!-- <div class="row" id="badgeRefId">
                                                   <div class="col-xs-6">
                                                      <ul class="nav nav-pills">
                                                         <li class="active" style="font-size:12pt;font-weight:600;">
                                                            <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                                                            </span></a>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div> -->
                                                <?php
                                                   $attr = ["br"=>true,
                                                            "type"=>"text",
                                                            "row"=>true,
                                                            "name"=>"date_FiledDate",
                                                            "col"=>"4",
                                                            "id"=>"FiledDate",
                                                            "label"=>"Date File",
                                                            "class"=>"saveFields-- mandatory date--",
                                                            "style"=>"",
                                                            "other"=>""];
                                                   $form->eform($attr);

                                                   echo
                                                   '<div class="row margin-top">
                                                   <div class="col-xs-12">
                                                   <label>Leave</label><br>';

                                                   createSelect("leaves",
                                                                "sint_LeavesRefId",
                                                                "",100,
                                                                "Name",
                                                                "Select Leave","required");

                                                   echo
                                                   '</div></div>';
                                                   echo '
                                                      <div class="row margin-top">
                                                         <div class="col-xs-12">
                                                            <select name="char_SPLType" id="h_spl" class="form-input saveFields--">
                                                               <option value="PM">Personal Milestone</option>
                                                               <option value="PO">Parental Obligation</option>
                                                               <option value="Fil">Fillial</option>
                                                               <option value="DomE">Domestic Emergencies</option>
                                                               <option value="PTrn">Personal Transactions</option>
                                                               <option value="C">Calamity</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   ';
                                                   echo
                                                   '<div class="row margin-top">
                                                      <div class="col-xs-12">
                                                         <label>Application Date</label><br>
                                                         <div class="row">';
                                                            $attr = ["br"=>true,
                                                                     "type"=>"text",
                                                                     "row"=>false,
                                                                     "name"=>"date_ApplicationDateFrom",
                                                                     "col"=>"6",
                                                                     "id"=>"ApplicationDateFrom",
                                                                     "label"=>"From",
                                                                     "class"=>"saveFields-- mandatory date--",
                                                                     "style"=>"",
                                                                     "other"=>"required"];
                                                            $form->eform($attr);
                                                            $attr = ["br"=>true,
                                                                     "type"=>"text",
                                                                     "row"=>false,
                                                                     "name"=>"date_ApplicationDateTo",
                                                                     "col"=>"6",
                                                                     "id"=>"ApplicationDateTo",
                                                                     "label"=>"To",
                                                                     "class"=>"saveFields-- mandatory date--",
                                                                     "style"=>"",
                                                                     "other"=>"required"];
                                                            $form->eform($attr);
                                                   echo
                                                   '
                                                         </div>
                                                      </div>
                                                   </div>';
                                                ?>
                                                <div class="margin-top">
                                                   <input id="fleave" type="checkbox" name="sint_isForceLeave" value="0" class="saveFields--">
                                                   <label for="fleave" >Force Leave</label>
                                                   <!--<input type="hidden" name="sint_isForceLeave" value="0">-->
                                                </div>
                                                <div class="margin-top">
                                                   <input id="incSATSUN" type="checkbox" name="sint_IncludeSatSun" value="0" class="saveFields--">
                                                   <label for="incSATSUN">Include Saturday And Sunday</label>
                                                   <!--<input type="hidden" name="sint_IncludeSunSat" value="0">-->
                                                </div>
                                                <!--
                                                <div class="margin-top">
                                                   <label>Balance:</label>
                                                   <input type="text" class="form-input number-- saveFields--" name="deci_Balance">
                                                </div>
                                                -->
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <div class="form-group">
                                                         <label class="control-label" for="inputs">Remarks:</label>
                                                         <textarea class="form-input saveFields--" rows="5" name="char_Remarks" placeholder="remarks"></textarea>
                                                      </div>
                                                   </div>
                                                </div>
                                          </div>
                                          <div class="col-xs-6 padd5 adminUse--">
                                             <label>Employees Selected</label>
                                             <select multiple id="EmpSelected" name="EmpSelected" class="form-input saveFields--" style="height:250px;">
                                             </select>
                                             <p>Double Click the items to remove in the List</p>
                                             <input type="hidden" class="saveFields--" name="hEmpSelected">
                                          </div>
                                       </div>
                                    </div>
                                    <?php
                                       spacer(10);
                                       echo
                                       '<button type="submit" class="btn-cls4-sea"
                                                name="btnLocSAVE" id="LocSAVE">
                                          <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                          &nbsp;Save
                                       </button>';
                                       btnSACABA([false,true,true]);
                                       echo
                                       '<button type="button" class="btn-cls4-tree"
                                                name="cancelLeave" id="cancelLeave">
                                          <i class="fa fa-close" aria-hidden="true"></i>
                                          &nbsp;Request Cancellation
                                       </button>';
                                    ?>
                                 </div>
                                 <div class="panel-bottom"></div>
                              </div>
                           </div>

                           <div class="col-xs-6 adminUse--">
                              <div class="mypanel">
                                 <div class="panel-top">Employees List</div>
                                 <div class="panel-mid">
                                    <?php include_once "incEmpListSearchCriteria.e2e.php" ?>
                                    <!-- <a href="javascript:void(0);" title="Search Employees">
                                       <i class="fa fa-search" aria-hidden="true"></i>
                                    </a>-->
                                    <div id="empList" style="max-height:300px;overflow:auto;">
                                       <h4>No Employees Selected</h4>
                                    </div>
                                 </div>
                                 <div class="panel-bottom">
                                    <a href="javascript:void(0);" id="clearEmpSelected">Clear Employees Selected</a>&nbsp;
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varJSON.e2e.php";
               include "varHidden.e2e.php";
               modalReject();
            ?>
         </div>
      </form>
   </body>
</html>
<?php 
   /*

<div class="row">
   <div class="col-xs-6">
      <div class="mypanel">
         <div class="panel-top">
            <span id="ScreenMode">REQUESTING</span> <?php echo strtoupper($ScreenName); ?>
         </div>
         <div class="panel-mid">
            <div id="EntryScrn">
               <div class="row margin-top">
                  <div class="<?php echo $sizeCol; ?>">
                     <div class="row" id="badgeRefId">
                        <div class="col-xs-6">
                           <ul class="nav nav-pills">
                              <li class="active" style="font-size:12pt;font-weight:600;">
                                 <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                                 </span></a>
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-4">
                           <label for="FiledDate">Date File:</label><br>
                           <input type="text" name="date_FiledDate" id="FiledDate" 
                           class="form-input saveFields-- mandatory date--" placeholder="MANDATORY" 
                           readonly="readonly" data-date-format="yyyy-mm-dd">
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-6">
                           <label for="EffectivityYear">Effectivity Year:</label><br>
                           <select name="sint_EffectivityYear" id="EffectivityYear" 
                              class="form-input saveFields-- mandatory">
                              <option value="0" selected>--SELECT YEAR--</option>
                              <?php 
                                 $yr = date("Y",time());
                                 for ($j=0; $j<=5; $j++) {
                                    echo '<option value="'.($yr+$j).'">'.($yr+$j).'</option>';
                                 }
                              ?>
                           </select>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           <div class="form-group">
                              <label class="control-label" for="inputs">Remarks:</label>
                              <textarea class="form-input saveFields--" rows="5" name="char_Remarks" placeholder="remarks"></textarea>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-6 padd5 adminUse--">
                     <label>Employees Selected</label>
                     <select multiple id="EmpSelected" name="EmpSelected" class="form-input saveFields--" style="height:250px;">
                     </select>
                     <p>Double Click the items to remove in the List</p>
                     <input type="hidden" class="saveFields--" name="hEmpSelected">
                  </div>
               </div>
            </div>
            <?php
               spacer(10);
               echo
               '<button type="submit" class="btn-cls4-sea"
                        name="btnLocSAVE" id="LocSAVE">
                  <i class="fa fa-floppy-o" aria-hidden="true"></i>
                  &nbsp;Save
               </button>';
               btnSACABA([false,true,true]);
            ?>
         </div>
         <div class="panel-bottom"></div>
      </div>
   </div>
   <div class="col-xs-6 adminUse--">
      <div class="mypanel">
         <div class="panel-top">Employees List</div>
         <div class="panel-mid">
            <?php include_once "incEmpListSearchCriteria.e2e.php" ?>
            <div id="empList" style="max-height:300px;overflow:auto;">
               <h4>No Employees Selected</h4>
            </div>
         </div>
         <div class="panel-bottom">
            <a href="javascript:void(0);" id="clearEmpSelected">Clear Employees Selected</a>&nbsp;
         </div>
      </div>
   </div>
</div>
*/
?>