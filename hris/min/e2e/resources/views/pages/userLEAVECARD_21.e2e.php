<?php
   require_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   require_once pathClass.'0620RptFunctions.e2e.php';
   require_once pathClass.'DTRFunction.e2e.php';
   require_once "conn.e2e.php";
   $whereClause = "WHERE RefId = ".getvalue('hUserRefId');
   $rsEmployees = SelectEach("employees",$whereClause);
   $year = date("Y",time());
   $arr_month =[
     "January",
     "February",
     "March",
     "April",
     "May",
     "June",
     "July",
     "August",
     "September",
     "October",
     "November",
     "December"
   ];
?>
<!DOCTYPE html>
<html>
   <head>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script type="text/javascript">
         $(document).ready(function () {
            $("#btnPrint").click(function () {
               var head = $("head").html();
               printDiv('div_CONTENT',head);
            });
         });
      </script>
      <style type="text/css">
         th {
            text-align: center;
         }
         @page { size: landscape; }
      </style>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis") ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar ("Leave Card"); ?>
            <div class="container-fluid margin-top">
               <button type="button" id="btnPrint" class="btn-cls4-lemon">PRINT</button>
               <div class="row">
                  <div class="col-sm-11" id="div_CONTENT">
                     <?php
                        if ($rsEmployees) {
                           while ($row = mysqli_fetch_assoc($rsEmployees)) {
                              $EmployeesRefId = $row["RefId"];
                              $FullName   = $row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"];
                              $emp_info   = FindFirst("empinformation","WHERE EmployeesRefId = ".$row["RefId"],"*");
                              if ($emp_info) {
                                 $Division = rptDefaultValue($emp_info["DivisionRefId"],"division");
                                 $HiredDate = $emp_info["HiredDate"];
                              } else {
                                 $Division = "";
                                 $HiredDate = "";
                              }
                     ?>
                     <div class="row" style="page-break-after: always;">
                        <div class="col-xs-12">
                           <table style="width: 100%;">
                              <thead>
                                 <tr>
                                    <td colspan="2">
                                       <img src="upl/21/leave_card_logo.gif" height="25">
                                    </td>
                                    <td colspan="12" class="text-center" style="font-weight: 700; font-size: 20pt;">
                                       EMPLOYEE'S LEAVE RECORD
                                    </td>
                                    <td colspan="3" class="text-center" style="font-weight: 700; font-size: 15pt;">
                                       YEAR &nbsp;&nbsp;&nbsp;
                                       <u><span style="color: red;"><?php echo $year; ?></span></u>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td colspan="17">&nbsp;</td>
                                 </tr>
                                 <tr>
                                    <td colspan="4" class="margin-top">
                                       NAME:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                       <u><?php echo strtoupper($FullName); ?>   </u>
                                    </td>
                                    <td></td>
                                    <td colspan="4">EMPLOYEE ID NO:</td>
                                    <td colspan="3" class="text-center">
                                       <u><?php echo $row["AgencyId"]; ?></u>
                                    </td>
                                    <td colspan="5">&nbsp;</td>
                                 </tr>
                                 <tr>
                                    <td colspan="4" class="margin-top">
                                       DIVISION: <u><?php echo $Division; ?></u>
                                    </td>
                                    <td>
                                       <td colspan="4">1ST DAY OF SERVICE:</td>
                                    </td>
                                    <td colspan="3" class="text-center">
                                        <u><?php echo $HiredDate; ?></u>
                                    </td>
                                    <td></td>
                                    <td colspan="4">
                                       LAST DAY OF SERVICE: 
                                    </td>
                                 </tr>
                                 <tr class="colHEADER">
                                    <th rowspan="2" style="width: 3%;">MOS.</th>
                                    <th rowspan="2" style="width: 15%;">DESCRIPTION</th>
                                    <th rowspan="2" style="width: 7%;">DATE/S COVERED</th>
                                    <th rowspan="2" style="width: 3%;">DAYS/<br>MINS.</th>
                                    <th rowspan="2" style="width: 3%;">NO. OF<br>TARDY<br>UT</th>
                                    <th colspan="4">VACATION LEAVE CREDITS</th>
                                    <th colspan="4">SICK LEAVE CREDITS</th>
                                    <th colspan="3">OTHER LEAVE DETAILS</th>
                                    <th rowspan="2" style="width: 15%;">REMARKS</th>
                                 </tr>
                                 <tr class="colHEADER">
                                    <th style="width: 4%;">ABS/T/<br>UTwith<br>PAY</th>
                                    <th style="width: 4%;">EARNED</th>
                                    <th style="width: 4%;">BAL.</th>
                                    <th style="width: 4%;">w/o PAY</th>
                                    <th style="width: 4%;">ABS/T/<br>UTwith<br>PAY</th>
                                    <th style="width: 4%;">EARNED</th>
                                    <th style="width: 4%;">BAL.</th>
                                    <th style="width: 4%;">w/o PAY</th>
                                    <th style="width: 15%;">TYPES OF LEAVES</th>
                                    <th style="width: 7%;">DATE/S COVERED</th>
                                    <th style="width: 4%;">NO. OF<br>DAYS</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php
                                    $where_credits    = "WHERE EmployeesRefId = '$EmployeesRefId'";
                                    $where_credits    .= " AND EffectivityYear = '$year'";
                                    $credits_rs       = SelectEach("employeescreditbalance",$where_credits);
                                    if ($credits_rs) {
                                       while ($credits_row = mysqli_fetch_assoc($credits_rs)) {
                                          switch ($credits_row["NameCredits"]) {
                                             case 'VL':
                                                $vl = $credits_row["BeginningBalance"];
                                                break;
                                             case 'SL':
                                                $sl = $credits_row["BeginningBalance"];
                                                break;
                                          }
                                          $AsOf = $credits_row["BegBalAsOfDate"];
                                       }
                                       $start_date = date("Y-m-d",strtotime( "$AsOf + 1 day" ));
                                    } else {
                                       $vl = 0;
                                       $sl = 0;
                                       $AsOf = date("Y-m",time())."-01";
                                       $start_date = date("Y-m-d",strtotime( "$AsOf - 1 day" ));
                                    }
                                    ${"VLBal_".$EmployeesRefId} = $vl;
                                    ${"SLBal_".$EmployeesRefId} = $sl;

                                    $from = date("m",strtotime($start_date));
                                 ?>
                                 <tr>
                                    <td colspan="7">Beginning Balance As Of <?php echo date("F d, Y",strtotime($AsOf)); ?></td>
                                    <td class="text-center">
                                       <?php
                                          echo $vl;
                                       ?>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-center">
                                       <?php
                                          echo $sl;
                                       ?>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                 </tr>
                                 <?php
                                    for ($i=intval($from)-1; $i <= 12 - 1; $i++) {
                                       $idx = $i+1;
                                       if ($idx <= 9) $idx = "0".$idx;
                                       $month      = $idx;
                                       $where_dtr  = "WHERE EmployeesRefId = $EmployeesRefId";
                                       $where_dtr  .= " AND Month = '$month'";
                                       $where_dtr  .= " AND Year = '$year'";
                                       $arr_empDTR = FindFirst("dtr_process",$where_dtr,"*");
                                       if ($arr_empDTR) {
                                          $where_monetization = "WHERE EmployeesRefId = '$EmployeesRefId'";
                                          $where_monetization .= " AND MONTH(FiledDate) = '".$month."'";
                                          $where_monetization .= " AND YEAR(FiledDate) = '".$year."'";
                                          $where_monetization .= " AND Status = 'Approved'";
                                          $monetize   = FindFirst("employeesleavemonetization",$where_monetization,"*");
                                          if ($monetize) {
                                             $vl_monetize = $monetize["VLValue"];
                                             $sl_monetize = $monetize["SLValue"];
                                             $remarks_monetize = "Monetized ".floatval($vl_monetize)." (VL) AND ".floatval($sl_monetize)." (SL)";
                                          } else {
                                             $vl_monetize = $sl_monetize = 0;
                                             $remarks_monetize = "";
                                          }
                                          $VL_Days       = $arr_empDTR["VL_Days"];
                                          $VL_Used       = $arr_empDTR["VL_Used"];
                                          $SL_Days       = $arr_empDTR["SL_Days"];
                                          $SL_Used       = $arr_empDTR["SL_Used"];
                                          $Tardy_Min     = $arr_empDTR["Total_Tardy_Hr"];
                                          $UT_Min        = $arr_empDTR["Total_Undertime_Hr"];
                                          $Tardy_Count   = $arr_empDTR["Total_Tardy_Count"];
                                          $UT_Count      = $arr_empDTR["Total_Undertime_Count"];
                                          $Tardy_EQ      = $arr_empDTR["Tardy_Deduction_EQ"];
                                          $UT_EQ         = $arr_empDTR["Undertime_Deduction_EQ"];
                                          $vl_earned     = $arr_empDTR["VL_Earned"];
                                          $sl_earned     = $arr_empDTR["SL_Earned"];
                                          $vl_days_obj   = "";
                                          $sl_days_obj   = "";
                                          $tdy_days_obj  = "";
                                          $ut_days_obj   = "";
                                          ${"VL_count_".$arr_empDTR["Month"]} = $VL_Used;
                                          ${"SL_count_".$arr_empDTR["Month"]} = $SL_Used;
                                          if ($VL_Days != "") {
                                             $VL_Days_arr = explode("|", $VL_Days);
                                             foreach ($VL_Days_arr as $key => $value) {
                                                if ($value != "") $vl_days_obj .= $value.", ";
                                             }
                                          }
                                          if ($SL_Days != "") {
                                             $SL_Days_arr = explode("|", $SL_Days);
                                             foreach ($SL_Days_arr as $xkey => $xvalue) {
                                                if ($xvalue != "") $sl_days_obj .= $xvalue.", ";
                                             }
                                          } 

                                          for ($tdy=1; $tdy <= 31; $tdy++) { 
                                             if ($tdy <= 9) $tdy = "0".$tdy;
                                             if(isset($arr_empDTR[$tdy."_Late"])){
                                                if (intval($arr_empDTR[$tdy."_Late"]) > 0) {
                                                   $tdy_days_obj .= $tdy.",";
                                                }
                                             }
                                          }
                                          for ($ut=1; $ut <= 31; $ut++) { 
                                             if ($ut <= 9) $ut = "0".$ut;
                                             if(isset($arr_empDTR[$ut."_UT"])){
                                                if (intval($arr_empDTR[$ut."_UT"]) > 0) {
                                                   $ut_days_obj .= $ut.",";
                                                }
                                             }
                                          }
                                 ?>
                                 <tr>
                                    <td rowspan="7" style="" valign="middle" b>
                                       <?php echo strtoupper($arr_month[$i]); ?>
                                    </td>
                                    <td>Beg. Balance</td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center">
                                       <?php echo number_format(${"VLBal_".$EmployeesRefId},3); ?>
                                    </td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center">
                                       <?php echo number_format(${"SLBal_".$EmployeesRefId},3); ?>
                                    </td>
                                    <td class="text-center">--</td>
                                    <td>Unauthorized Leave</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                 </tr>
                                 <?php
                                    ${"SLBal_".$EmployeesRefId} -= $SL_Used;
                                 ?>
                                 <tr>
                                    <td>Sick Leave</td>
                                    <td>
                                       <?php
                                          if ($sl_days_obj != "") {
                                             echo $sl_days_obj;
                                          }
                                       ?>
                                    </td>
                                    <td class="text-center">
                                       <?php echo $SL_Used; ?>
                                    </td>
                                    <td class="text-center">
                                       <?php echo $SL_Used; ?>
                                    </td>
                                    <td class="text-center">
                                       <?php echo number_format(intval($SL_Used),3); ?>
                                    </td>
                                    <td class="text-center"></td>
                                    <td class="text-center">
                                       <?php echo number_format(${"VLBal_".$EmployeesRefId},3); ?>
                                    </td>
                                    <td class="text-center" style="background: #d9d9d9;"></td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">
                                       <?php echo number_format(${"SLBal_".$EmployeesRefId},3); ?>
                                    </td>
                                    <td class="text-center">--</td>
                                    <td>Forced Leave</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                 </tr>
                                 <?php
                                    ${"VLBal_".$EmployeesRefId} -= $VL_Used;
                                 ?>
                                 <tr>
                                    <td>Vacation Leave</td>
                                    <td>
                                       <?php
                                          if ($vl_days_obj != "") {
                                             echo $vl_days_obj;
                                          }
                                       ?>
                                    </td>
                                    <td class="text-center">
                                       <?php echo $VL_Used; ?>
                                    </td>
                                    <td class="text-center">
                                       <?php echo $VL_Used; ?>
                                    </td>
                                    <td class="text-center">
                                       <?php echo number_format(intval($VL_Used),3); ?>
                                    </td>
                                    <td class="text-center"></td>
                                    <td class="text-center">
                                       <?php echo number_format(${"VLBal_".$EmployeesRefId},3); ?>
                                    </td>
                                    <td class="text-center">--</td>
                                    <td class="text-center" style="background: #d9d9d9;"></td>
                                    <td class="text-center" style="background: #d9d9d9;"></td>
                                    <td class="text-center" style="background: #d9d9d9;"></td>
                                    <td class="text-center" style="background: #d9d9d9;"></td>
                                    <td>Special Leave Privileges</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                 </tr>
                                 <?php
                                    ${"VLBal_".$EmployeesRefId} -= $Tardy_EQ;
                                 ?>
                                 <tr>
                                    <td style="text-indent: 20px;">►Tardy</td>
                                    <td>
                                       <?php
                                          if ($tdy_days_obj != "") {
                                             echo $tdy_days_obj." (".$Tardy_Min.")";
                                          }
                                       ?>
                                    </td>
                                    <td class="text-center">
                                       <?php echo intval($Tardy_Min);  ?>
                                    </td>
                                    <td class="text-center">
                                       <?php echo intval($Tardy_Count);  ?>
                                    </td>
                                    <td class="text-center">
                                       <?php echo number_format(floatval($Tardy_EQ),3);  ?>
                                    </td>
                                    <td class="text-center"></td>
                                    <td class="text-center">
                                       <?php echo number_format(${"VLBal_".$EmployeesRefId},3); ?>
                                    </td>
                                    <td class="text-center">--</td>
                                    <td class="text-center" style="background: #d9d9d9;"></td>
                                    <td class="text-center" style="background: #d9d9d9;"></td>
                                    <td class="text-center" style="background: #d9d9d9;"></td>
                                    <td class="text-center" style="background: #d9d9d9;"></td>
                                    <td>Study Leave</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                 </tr>
                                 <?php
                                    ${"VLBal_".$EmployeesRefId} -= $UT_EQ;
                                 ?>
                                 <tr>
                                    <td style="text-indent: 20px;">►Undertime</td>
                                    <td>
                                       <?php
                                          if ($ut_days_obj != "") {
                                             echo $ut_days_obj." (".$UT_Min.")";
                                          }
                                       ?>
                                    </td>
                                    <td class="text-center">
                                       <?php echo intval($UT_Min);  ?>
                                    </td>
                                    <td class="text-center">
                                       <?php echo intval($UT_Count);  ?>
                                    </td>
                                    <td class="text-center">
                                       <?php echo number_format(floatval($UT_EQ),3);  ?>
                                    </td>
                                    <td class="text-center"></td>
                                    <td class="text-center">
                                       <?php echo number_format(${"VLBal_".$EmployeesRefId},3); ?>
                                    </td>
                                    <td class="text-center">--</td>
                                    <td class="text-center" style="background: #d9d9d9;"></td>
                                    <td class="text-center" style="background: #d9d9d9;"></td>
                                    <td class="text-center" style="background: #d9d9d9;"></td>
                                    <td class="text-center" style="background: #d9d9d9;"></td>
                                    <td>Maternity / Paternity Leave</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                 </tr>
                                 <?php
                                    ${"VLBal_".$EmployeesRefId} += $vl_earned;
                                    ${"SLBal_".$EmployeesRefId} += $sl_earned;
                                 ?>
                                 <tr>
                                    <td>Leave Credits & w/o Pay</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">
                                       <?php echo number_format(floatval($vl_earned),3); ?>
                                    </td>
                                    <td class="text-center">
                                       <?php echo number_format(${"VLBal_".$EmployeesRefId},3); ?>
                                    </td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">
                                       <?php echo number_format(floatval($sl_earned),3); ?>
                                    </td>
                                    <td class="text-center">
                                       <?php echo number_format(${"SLBal_".$EmployeesRefId},3); ?>
                                    </td>
                                    <td class="text-center">--</td>
                                    <td>Solo Parent Leave</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       Monetization
                                    </td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                    <td>Others</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                    <td class="text-center">--</td>
                                 </tr>
                                 <?php
                                       }
                                    }
                                 ?>
                              </tbody>
                           </table>
                        </div>
                     </div>
                     <?php
                           }
                        }
                     ?>
                  </div>
               </div>
            </div>
            <?php
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>