<?php
	error_reporting(E_ALL);
   ini_set('display_errors', 1);
   session_start();
   include_once "constant.e2e.php";
   include_once pathClass.'0620functions.e2e.php';
   include_once pathClass.'SysFunctions.e2e.php';
   include_once pathClass.'0620TrnData.e2e.php';
   
   $trn = new Transaction();
   $user = getvalue("hUser");

   function getTrainingDate() {
      $refid = getvalue("value");
      if ($refid > 0) {
         $row = FindFirst("ldmslndprogram","WHERE RefId = '$refid'","*");
         if ($row) {
            echo '$("#date_ApplicationDateFrom").val("'.$row["StartDate"].'");';
            echo '$("#date_ApplicationDateTo").val("'.$row["EndDate"].'");';
            
         }
      }
   }

   function deleteICD() {
      include 'conn.e2e.php';
      $refid = getvalue("refid");
      $check = FindFirst("employees_assessment","WHERE RefId = '$refid'","*");
      if ($check) {
         $emprefid   = $check["EmployeesRefId"];
         $year       = $check["Year"];
         $where      = "WHERE EmployeesRefId = '$emprefid' AND Year = '$year'";
         $sql        = "DELETE FROM `individual_competency_data` ".$where;
         $rs         = mysqli_query($conn,$sql);
         if ($rs) {
            $sql     = "DELETE FROM `employees_assessment` WHERE RefId = '$refid'";
            $result  = mysqli_query($conn,$sql);
            if ($result) {
               echo "afterDelete();";
            } else {
               echo "alert('Error in Deleting');";
            }
         }
      }
   }
   function getFacilitatorInstitution() {
      $refid = getvalue("refid");
      $row   = getRecord("provider",$refid,"TrainingInstitution");
      echo '$("#char_TrainingInstitution").val("'.$row.'");';
   }
   function getCompetency() {
   	$refid = getvalue("refid");
   	$row   = FindFirst("competency","WHERE RefId = '$refid'","*");
   	if ($row) {
   		$Type = $row["Type"];
   		$Definition = $row["Definition"];
   		echo '$("#char_Type").val("'.$Type.'");';
   		echo '$("#char_Definition").val("'.$Definition.'");';
   	}
   }
   function getFacilitator() {
      $refid = getvalue("refid");
      $row   = FindFirst("ldmslndprogram","WHERE RefId = '$refid'","*");
      if ($row) {
         echo '$("#char_Facilitator").val("'.$row["Facilitator"].'");';
      }
   }
   

   function getCompetencyIndicator() {
   	$refid = getvalue("refid");
   	$level = getvalue("level");
   	switch ($level) {
   		case '1':
   			$level = "One";
   			break;
   		case '2':
   			$level = "Two";
   			break;
   		case '3':
   			$level = "Three";
   			break;
   		case '4':
   			$level = "Four";
   			break;
   		default:
   			$level = "";
   			break;
   	}
   	if ($level != "") {
   		$row   = FindFirst("competency","WHERE RefId = '$refid'","*");
	   	if ($row) {
	   		$One 		= $row["Level".$level."1"];
	   		$Two 		= $row["Level".$level."2"];
	   		$Three 	= $row["Level".$level."3"];
	   		$Four 	= $row["Level".$level."4"];
	   		$Five 	= $row["Level".$level."5"];
	   		$Six 		= $row["Level".$level."6"];
	   		$Seven 	= $row["Level".$level."7"];
	   		if ($One != "") $One = "- ".$One;
	   		if ($Two != "") $Two = "- ".$Two;
	   		if ($Three != "") $Three = "- ".$Three;
	   		if ($Four != "") $Four = "- ".$Four;
	   		if ($Five != "") $Five = "- ".$Five;
	   		if ($Six != "") $Six = "- ".$Six;
	   		if ($Seven != "") $Seven = "- ".$Seven;
	   		$str     = $One."\n\n".$Two."\n\n".$Three."\n\n".$Four."\n\n".$Five."\n\n".$Six."\n\n".$Seven;
	   		echo $str;
	   	}	
   	}
   }

   function getIndividualData(){
      $emprefid = getvalue("emprefid");
      $year     = getvalue("year");
      $rs = SelectEach("individual_competency_data","WHERE EmployeesRefId = '$emprefid' AND Year = '$year'");
      if ($rs) {
         $arr = array();
         while ($row = mysqli_fetch_assoc($rs)) {
            array_push($arr, $row);
         }
         echo json_encode($arr);
      }
   }
   function getCompetencyPosition(){
      $emprefid = getvalue("emprefid");
      $position = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","PositionRefId");
      $position = intval($position);
      if ($position) {
         $rs = SelectEach("position_competency","WHERE PositionRefId = '$position'");
         if ($rs) {
            $arr = array();
            while ($row = mysqli_fetch_assoc($rs)) {
               array_push($arr, $row);
            }
            echo json_encode($arr);
         }
      }
   }

   function getCompetencyInfo() {
      $refid = getvalue("refid");
      $table = getvalue("table");
      $row   = FindFirst($table,"WHERE RefId = '$refid'","*");
      if ($row) {
         /*foreach ($row as $key => $value) {
            $row[$key] = realEscape($value);
         }
         print_r($row);*/
         echo json_encode($row);   
      } else {
         echo "No Record Found.";
      }
      
   }

   function saveNomination() {
      $FiledDate = getvalue("FiledDate");
      $LDMSLNDProgramRefId = getvalue("LDMSLNDProgramRefId");
      $EndorsedBy = getvalue("EndorsedBy");
      $EmployeesRefId = getvalue("EmployeesRefId");
      $table = "assigned_courses";
      $Flds = "FiledDate,EmployeesRefId,LDMSLNDProgramRefId,EndorsedBy,";
      $Vals = "'$FiledDate','$EmployeesRefId','$LDMSLNDProgramRefId','$EndorsedBy',";
      $save = f_SaveRecord("NEWSAVE",$table,$Flds,$Vals);
      if (!is_numeric($save)) {
         echo '$.notify("Error");';
         return false;
      } else {
         echo 'afterNewSave();';
      }
   }

   function saveIDC() {
      $obj        = getvalue("obj");
      $mode       = getvalue("mode");
      $emprefid   = getvalue("emprefid");
      $year       = getvalue("year");
      $position   = getvalue("position");
      $office     = getvalue("office");
      $obj_arr    = explode("_", $obj);
      $err        = 0;
      $where      = "WHERE EmployeesRefId = '$emprefid' AND Year = '$year'";
      $check      = FindFirst("employees_assessment",$where,"RefId");
      if (intval($check) > 0) {
         echo 'recordExist();';
      } else {
         $nFld = "`EmployeesRefId`, `Year`, `PositionRefId`, `OfficeRefId`, ";
         $nVal = "'$emprefid','$year', '$position', '$office', ";
         $nSave = f_SaveRecord("NEWSAVE","employees_assessment",$nFld,$nVal);
         if (is_numeric($nSave)) {
            foreach ($obj_arr as $key => $value) {
               if ($value != "") {
                  $new_arr          = explode("|", $value);
                  $CompetencyRefId  = $new_arr[0];
                  $Result           = $new_arr[1];
                  $Requirement      = $new_arr[2];
                  $Behavior         = $new_arr[3];
                  $Level            = $new_arr[4];
                  $Flds             = "`EmployeesRefId`, `Year`, `Result`, `Requirement`, `Behavior`, `Level`, `CompetencyRefId`,";
                  $Vals             = "'$emprefid', '$year', '$Result', '$Requirement', '$Behavior', '$Level', '$CompetencyRefId',";
                  /*$where            = "PositionRefId = '$position' AND CompetencyRefId = '$CompetencyRefId'";
                  $row              = FindFirst("position_competency",$where,"*");*/
                  $save = f_SaveRecord("NEWSAVE","individual_competency_data",$Flds,$Vals);
                  if (!is_numeric($save)) {
                     echo '$.notify("Error");';
                     $err++;
                     return false;

                  }
               }
            }   
         }
         
         if ($err == 0) {
            echo 'afterNewSave();';
         }   
      }
      
   }

   function saveIDP() {
      $obj        = getvalue("obj");
      $mode       = getvalue("mode");
      $emprefid   = getvalue("emprefid");
      $year       = getvalue("year");
      $position   = getvalue("position");
      $office     = getvalue("office");
      $obj_arr    = explode("_", $obj);
      $err        = 0;
      foreach ($obj_arr as $key => $value) {
         if ($value != "") {
            $new_arr             = explode("|", $value);
            $refid               = $new_arr[0];
            $ImplementationDate  = $new_arr[1];
            $Status              = $new_arr[2];
            $fldnval             = "`ImplementationDate` = '$ImplementationDate', ";
            $fldnval             .= "`Status` = '$Status', ";
            $update = f_SaveRecord("EDITSAVE","individual_competency_data",$fldnval,$refid);
            if ($update != "") {
               echo '$.notify("Error");';
               return false;
            }
         }
      }
      if ($err == 0) {
         echo 'afterNewSave();';
      }
   }

   function getCompetencyName(){
      $refid = getvalue("refid");
      $Name   = FindFirst("competency","WHERE RefId = '$refid'","Name");
      echo $Name;
   }

   function saveCompetency() {
      $refid         = getvalue("refid");
      $behavior      = getvalue("behavior");
      $result        = getvalue("result");
      $requirement   = getvalue("requirement");
      $refid         = intval($refid);
      if ($refid > 0) {
         $fldnval = "Behavior = '$behavior', Result = '$result', Requirement = '$requirement',";
         $update = f_SaveRecord("EDITSAVE","position_competency",$fldnval,$refid);
         if ($update == "") {
            echo 'afterNewSave()';
         } else {
            echo 'alert("Error");';
         }
      } else {
         echo 'alert("No Ref. Id");';
      }
   }

   function getParticipants() {
      include_once 'conn.e2e.php';
      $refid = getvalue("refid");
      $table = "assigned_courses";
      $sql   = "SELECT employees.FirstName, employees.LastName, employees.AgencyId FROM $table INNER JOIN employees";
      $sql   .= " WHERE LDMSLNDProgramRefId = '$refid' AND employees.RefId = assigned_courses.EmployeesRefId";
      $rs    = mysqli_query($conn,$sql);
      if ($rs) {
         if (mysqli_num_rows($rs) > 0) {
            $arr = array();
            while ($row = mysqli_fetch_assoc($rs)) {
               array_push($arr, $row);
            }
            echo json_encode($arr);   
         }
      }
   }

   /*DONT MODIFY HERE*/
   $funcname = getvalue("fn");
   $params   = getvalue("params");
   if (!empty($funcname)) {
      $funcname($params);
   } else {
      echo 'alert("Error... No Function defined");';
   }
?>