<?php
   session_start();
   include_once "conn.e2e.php";
   include_once "constant.e2e.php";
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   $issuesRefId = getvalue("id");

   $criteria  = " WHERE RefId = $issuesRefId LIMIT 1";
   $rsIssues = f_Find("issues",$criteria);
   if ($rsIssues) {
      $data = mysqli_fetch_assoc($rsIssues);
      echo
      '<div style="padding-right:5px;">
         <div class="row padd5 border1" style="background:#fcfdff;">
            <form action="postComment.e2e.php" method="post" enctype="multipart/form-data" id="formupload">
               <div style="padding-left:20px;">
                  <div>
                     <h4><b>Title/Issues : </b><span class="fontB14">['.$data["RefId"].'] '.$data["Issue"].'</span></h4>
                  </div>
                  <div>
                     <h5><b>Description : </b>'.$data["Description"].'</h5>
                  </div>
                  <div>
                     <b>Created By : </b>
                     '.strtoupper($data["CreatedBy"]).'
                     <b style="margin-left:10px;">Posting Date : </b>
                     '.($data["LastUpdateDate"]." ".$data["LastUpdateTime"]).'
                  </div>
                  <div>';
                     if (
                           $data["CreatedBy"] == $_SESSION['sess_user'] ||
                           $data["CreatedBy"] == "admin" ||
                           $data["CreatedBy"] == "erwin"
                        ) {
                        echo
                        '<div class="dropdown">
                           <b>Status : </b><span id="issuesStatus">'.strtoupper($data["Status"]).'</span>&nbsp;
                           <a href="#" class="dropdown-toggle link" data-toggle="dropdown">
                              <i class="fa fa-chevron-down" aria-hidden="true" style="font-size:9pt;"></i>
                           </a>
                           <ul class="dropdown-menu">
                              <li class="dropdown-header">Change Status</li>
                              <li><a href="#" class="chnStatus" id="Close">Close</a></li>
                              <li><a href="#" class="chnStatus" id="Cancel">Cancel</a></li>
                              <li><a href="#" class="chnStatus" id="Resolve">Resolve</a></li>
                              <li><a href="#" class="chnStatus" id="In-Progress">In Progress</a></li>
                              <li><a href="#" class="chnStatus" id="Re-Open">Re-Open</a></li>
                           </ul>
                        </div>';
                     } else {
                        echo
                        '<div><b>Status : </b>'.strtoupper($data["Status"]).'</div>';
                     }
                     echo
                     '<div><b>Currently Working By : </b><span id="WorkingBy">'.strtoupper($data["CurrentlyWorkingBy"]).'</span></div>';
                  echo
                  '</div>';
                  if (!empty($data["ImageFile"])) {
                     echo
                     '<div class="row padd5">
                        <img src="'.pathPublic.'images/Comments/'.$data["ImageFile"].'" style="width:100%;">
                     </div>';
                  }
               echo
               '</div>';
               bar();
               echo
               '<div class="row padd5" id="Comments">
                  <p id="message"></p>
                  <div class="form-group">
                     <textarea class="form-input saveFields--" rows="2" name="char_Comments" placeholder="Reply Here"></textarea>
                  </div>
                  <div>
                     <label>Forward To:</label>';
                     $where = "where CompanyRefId = ".$_SESSION['CompanyId']." AND BranchRefId = ".$_SESSION['BranchId']." AND sysgrouprefid = 2";
                     createSelectSysUser("drpForward",$where);
                  echo
                  '</div>
                  <input type="hidden" class="saveFields--" name="char_CommentBy" value="'.$_SESSION['sess_user'].'">
                  <input type="hidden" class="saveFields--" name="sint_issuesRefId" value="'.$issuesRefId.'">
                  <input type="hidden" class="saveFields--" name="hPostNewComment" value="new">
                  <input type="hidden" class="form-input saveFields--" name="fn" value="PostNewComment">
                  <div style="text-align:right;margin-top:10px;">
                     <label for="imageComment" style="padding:5px;border:1px solid #888;">
                        <i class="fa fa-picture-o" aria-hidden="true" title="Attach Image" onmouseover="this.style.cursor=\'pointer\';"></i>
                     </label>
                     <input type="file" name="imageComment" id="imageComment" style="display:none;" accept="image/*">
                     <a href="javascript:void(0);" id="aSEND" style="margin-left:3px;padding:5px;border:1px solid #888"><i class="fa fa-paper-plane" aria-hidden="true" title="Send Reply"></i></a>
                  </div>
               </div>
            </form> ';

      $criteria  = " WHERE issuesRefId = $issuesRefId ORDER BY RefId Desc";
      $recordSet = f_Find("issuescomments",$criteria);
      if ($recordSet) {
         $rowcount = mysqli_num_rows($recordSet);
         if ($rowcount > 0) {
            echo '<h5>'.$rowcount.' Comment(s)</h5>';
            while ($row = mysqli_fetch_assoc($recordSet)) {
               echo
               '<div class="padd5" style="border-bottom:1px solid #aaa" id="Comments_'.$row["RefId"].'">
                  <div><b>'.$row["CommentBy"].':</b>&nbsp;&nbsp;'.$row["Comments"].'</div>
                  <div>'.($row["LastUpdateDate"]." ".$row["LastUpdateTime"]).'</div>';
                  if (!empty($row["PictureComments"])) {
                     echo
                     '<div style="margin-top:5px;">
                        <a href="javascript:void(0);"><img src="'.pathPublic.'images/Comments/'.$row["PictureComments"].'" style="width:100%;" class="imgComments"></a>
                     </div>';
                  }
               echo
               '</div>';

            }
         }
      }
      echo
      '</div>
      </div>';
   }
   $conn->close();
?>
      <script language="JavaScript">
         $(document).ready(function() {
            $(".imgComments").each(function () {
               $(this).click(function () {
                  viewLargePhoto($(this).attr("src"));
               });
            });

            $(window).click(function(event) {
               var modal = document.getElementById('ebmModal');
               if (event.target == modal) {
                  $("#ebmModal").fadeOut(150);
               }
            });
            $(".ModalClose").click(function() {
               $("#ebmModal").fadeOut(150);
            });
         });
         function viewLargePhoto(src) {
            $("#ebmModal").fadeIn(500);
            $("#bigIMG").attr("src",src);
         }
      </script>
<script language="JavaScript" src="<?php echo jsCtrl("ctrl_Issues"); ?>"></script>
