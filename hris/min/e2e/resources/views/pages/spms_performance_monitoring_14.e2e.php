<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_AfterTrn"); ?>"></script>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_PM_14"); ?>"></script>
      <style type="text/css">
         .td-input {
            padding: 2px;
         }
      </style>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"spms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("PERFORMANCE MONITORING"); ?>
            <div class="container-fluid margin-top10">
               <div class="row" id="spmsList">
                  <div class="col-xs-12">
                     <div class="panel-top">
                        LIST OF PERFORMANCE MONITORING
                        <span id="selectedEmployees">&nbsp;</span>
                     </div>
                     <div class="panel-mid">
                        <div id="spGridTable">
                           <?php
                              $action = [true,true,true,false];
                              $sql = "SELECT * FROM `spms_performance` ORDER BY RefId Desc";
                              $gridTableHdr_arr = ["Office", "Quarter","Year"];
                              $gridTableFld_arr = ["OfficeRefId", "Quarter","Year"];
                              doGridTable($table,
                                          $gridTableHdr_arr,
                                          $gridTableFld_arr,
                                          $sql,
                                          $action,
                                          $_SESSION["module_gridTable_ID"]);
                           ?>
                        </div>
                     </div>
                     <div class="panel-bottom">
                        <button type="button"
                             class="btn-cls4-sea trnbtn"
                             id="btnINSERTSPMS" name="btnINSERTSPMS">
                           <i class="fa fa-file" aria-hidden="true"></i>
                           &nbsp;Insert New
                        </button>
                     </div>
                  </div>
               </div>
               <div class="row" id="insert_spms">
                  <div class="col-xs-12">
                     <div class="panel-top">ADD NEW</div>
                     <div class="panel-mid" id="contentPM">
                        <div class="row">
                           <div class="col-xs-4">
                              <label>Office</label>
                              <?php
                                 createSelect("Office",
                                              "sint_OfficeRefId",
                                              "",100,"Name","Select Section","");
                              ?>
                           </div>
                           <div class="col-xs-4">
                              <label>Quarter</label>
                              <select class="form-input" name="sint_Quarter" id="sint_Quarter">
                                 <option value="">Select Quarter</option>
                                 <option value="1">1st Quarter</option>
                                 <option value="2">2nd Quarter</option>
                                 <option value="3">3rd Quarter</option>
                                 <option value="4">4th Quarter</option>
                              </select>
                           </div>
                           <div class="col-xs-4">
                              <label>Year</label>
                              <select class="form-input" name="sint_Year" id="sint_Year">
                                 <?php
                                    $past_year = date("Y",time()) - 2;
                                    $curr_year = date("Y",time());
                                    $future_year = date("Y",time()) + 2;
                                    for ($i=$past_year; $i <= $future_year ; $i++) { 
                                       if ($i == $curr_year) {
                                          echo '<option value="'.$i.'" selected>'.$i.'</option>';   
                                       } else {
                                          echo '<option value="'.$i.'">'.$i.'</option>';
                                       }
                                       
                                    }
                                 ?>
                              </select>
                           </div>
                        </div>
                        <br>
                        <div id="canvas">
                           <?php
                              for ($x=1; $x <=1; $x++) { 
                           ?>
                           <div id="EntryPM_<?php echo $x; ?>" class="entry201">
                              <div class="row margin-top">
                                 <div class="col-xs-6">
                                    <label>Performance Monitoring #<?php echo $x; ?> </label>
                                    <input type="hidden" name="refid_<?php echo $x; ?>" id="refid_<?php echo $x; ?>">
                                 </div>
                              </div>
                              <?php bar(); ?>
                              <div class="row margin-top">
                                 <div class="col-xs-6">
                                    <label>Type:</label>
                                    <select class="form-input" name="type_<?php echo $x; ?>"
                                            id="type_<?php echo $x; ?>">
                                       <option value="Monitoring">Monitoring</option>
                                       <option value="Coaching">Coaching</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-6">
                                    <label>Activity</label>
                                    <textarea class="form-input" rows="4" name="activity_<?php echo $x; ?>" id="activity_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-6">
                                    <label>One-in-One</label>
                                    <textarea class="form-input" rows="4" name="oneonone_<?php echo $x; ?>" id="oneonone_<?php echo $x; ?>"></textarea>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-6">
                                    <label>Group</label>
                                    <textarea class="form-input" rows="4" name="group_<?php echo $x; ?>" id="group_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-6">
                                    <label>Memo</label>
                                    <textarea class="form-input" rows="4" name="memo_<?php echo $x; ?>" id="memo_<?php echo $x; ?>"></textarea>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-6">
                                    <label>Others (Pls. Specify)</label>
                                    <textarea class="form-input" rows="4" name="others_<?php echo $x; ?>" id="others_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-6">
                                    <label>REMARKS</label>
                                    <textarea class="form-input" rows="4" name="remarks_<?php echo $x; ?>" id="remarks_<?php echo $x; ?>"></textarea>
                                 </div>
                              </div>
                           </div>
                           <br>
                           <?php
                              }
                           ?>   
                        </div>
                        
                     </div>
                     <div class="panel-bottom">
                        <button type="button" id="addPM" class="btn-cls4-tree">
                           <i class="fa fa-plus"></i>&nbsp;ADD ROW
                        </button>
                        <input type="hidden" name="sint_PMRefId" id="sint_PMRefId" value="0">
                        <input type="hidden" name="sint_EmployeesRefId" id="sint_EmployeesRefId" value="0">
                        <!-- hUserRefId -->
                        <input type="hidden" name="count" id="count">
                        <input type="hidden" name="fn" id="fn" value="savePM">
                        <button type="button" id="btnSAVEPM" class="btn-cls4-sea">
                           <i class="fa fa-save"></i>&nbsp;SAVE
                        </button>
                        <button type="button" id="btnCANCELPM" class="btn-cls4-red">
                           <i class="fa fa-times"></i>&nbsp;CANCEL
                        </button>
                        <button type="button" id="btnEDITPM" class="btn-cls4-sea">
                           <i class="fa fa-save"></i>&nbsp;UPDATE
                        </button>
                        <button type="button" id="btnBACKPM" class="btn-cls4-red">
                           <i class="fa fa-backward"></i>&nbsp;BACK
                        </button>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               $table = "spms_performance";
               include_once ("varHidden.e2e.php");
            ?>
         </div>
      </form>
   </body>
   <script type="text/javascript">
      $(document).ready(function () {
         $("#addPM").click(function() {
            AddRows("EntryPM_","contentPM","refid_");
         });
      });
      function AddRows(parentId,gparentId,refid) {
         // $.notify(parentId + "1");
         // $.notify(gparentId + "2");
         // $.notify(refid + "3");
         var rowHTML    = "";
         var newAppends = "";
         var appends    = "";
         var rowLength  = $("[id*='"+parentId+"']").length;
         appends        = $("#"+parentId+rowLength).html();
         
         newAppends = appends.split("_"+rowLength).join("_"+(rowLength+1));
         newAppends = newAppends.split("#"+rowLength).join("#"+(rowLength+1));
         newAppends = newAppends.split('idx="' + rowLength + '"').join('idx="'+ (rowLength + 1) +'"');
         newAppends = newAppends.split('unclick="CancelAddRow"').join('onClick=removeRow("'+parentId+(rowLength+1)+'");');
         newAppends = newAppends.split('focus="validateGrade"').join('onChange=validateGrade2($(this).attr(\'partner\')); onFocus=validateGrade2($(this).attr(\'partner\'));');
         newAppends = newAppends.split('click="HighestGrade_validation"').join('onClick=HighestGrade_validation($(this).attr(\'name\'));');
         rowHTML += '<div id="'+parentId+(rowLength+1)+'" class="entry201">';
         rowHTML += newAppends;
         rowHTML += '</div>';
         $("#"+gparentId).append(rowHTML);
         $("[name='"+refid+(rowLength + 1)+"']").val("");
         if (rowHTML.indexOf("date--") > 0) {
            $("#" + parentId+(rowLength+1) + " input[class*='date--']").each(function () {
               $(this).keypress(fnDateClass($(this).attr("name")));
            });
         }
         if (rowHTML.indexOf("alpha--") > 0) {
            $("#" + parentId+(rowLength+1) + " input[class*='alpha--']").each(function () {
               var e = arguments[0];
               fnAlphaClass($(this).attr("name"),e);
            });
         }
         if (rowHTML.indexOf("number--") > 0) {
            $("#" + parentId+(rowLength+1) + " input[class*='number--']").each(function () {
               var e = arguments[0];
               fnNumberClass($(this).attr("name"),e);
            });
         }
         if (rowHTML.indexOf("valDate--") > 0) {
            $("#" + parentId+(rowLength+1) + " input[class*='valDate--']").each(function () {
               $(this).keypress(fnValDate($(this).attr("name")));
            });
         }
         if (rowHTML.indexOf('rowid="') > 0) {
            $("#" + parentId+(rowLength+1) + " input[class*='enabler--']").each(function () {
               $(this).attr("refid","");
               $("[name='"+$(this).attr("rowid")+"']").val("");
            });
         }
         if (rowHTML.indexOf('presentbox--') > 0) {
            $("#" + parentId+(rowLength+1) + " input[class*='presentbox--']").each(function () {
               $(this).attr("checked",false);
               $(this).click(function () {
                  sint_Present_Click($(this).attr("id"));
               });
            });
         }
         $("#refid_"+(rowLength+1)).val("");
         $("#activity_"+(rowLength+1)).val("");
         $("#oneonone_"+(rowLength+1)).val("");
         $("#memo_"+(rowLength+1)).val("");
         $("#others_"+(rowLength+1)).val("");
         $("#remarks_"+(rowLength+1)).val("");
         // After Add Row
         //$("#"+parentId+(rowLength+1)).append(btnCan);
         $("#" +parentId+(rowLength+1)+ " .saveFields--").prop("disabled",false);
         $("#" +parentId+(rowLength+1)+ "> .enabler--").prop("checked",true);
         showBtnUpd();
      }
   </script>
</html>