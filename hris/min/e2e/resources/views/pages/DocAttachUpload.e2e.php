<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once 'constant.e2e.php';
   require_once 'conn.e2e.php';
?>

<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
   </head>
   <body>
      <form name="xForm"
            id="xForm"
            action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"
            method="post"
            enctype="multipart/form-data">

         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
         <?php

            $target_dir = "uploads/";
            $new_target_dir = path."images/".getvalue("hCompanyID")."/EmpDocument/";
            $new_target_dir_olds = path."images/".getvalue("hCompanyID")."/EmpDocument/olds/";
            $uploadOk = 1;
            $msg = "";
            $doc = "";
            $rsDocType = SelectEach("attachdoctype","");


            if ($rsDocType) {
               while ($row = mysqli_fetch_assoc($rsDocType)) {
                  $doc = $doc.$row["Code"].",";
               }
            }
            $DocTypeArr = explode(",",$doc);
            //$DocTypeArr = ["TOR","TrainCert","BirthCert","MarriageCert","COE"];
            // Check if image file is a actual image or fake image

            if (
                  //isset($_POST["submit"]) &&
                  //$_POST["submit"] == "Upload"
                  isset($_POST["hSubmit"]) &&
                  $_POST["hSubmit"] == "YES"
               ) {

               $timestamp = time();
               $date_today = date("Ymd",$timestamp);
               $curr_time  = date("his",$timestamp);
               for ($j=0;$j<(count($DocTypeArr) - 1);$j++) {
                  $docType = "file".$DocTypeArr[$j];

                  if ($_FILES[$docType]["tmp_name"] != "") {

                     $old_filename = basename($_FILES[$docType]["name"]);
                     $imageFileType = pathinfo($target_dir.$old_filename,PATHINFO_EXTENSION);
                     $nFileName = $DocTypeArr[$j];

                     if (strtolower($imageFileType) != "jpg" &&
                         strtolower($imageFileType) != "png" &&
                         strtolower($imageFileType) != "jpeg" &&
                         strtolower($imageFileType) != "gif") {
                        $check = getimagesize($_FILES[$docType]["tmp_name"]);
                     } else {
                        $check = 1;
                     }

                     //$new_filename = $nFileName."_".getvalue("txtRefId")."_".$date_today.$curr_time.".".$imageFileType;
                     $new_filename = $nFileName."_".getvalue("txtRefId").".".$imageFileType;
                     $target_file = $new_target_dir . $new_filename;

                     /*if($check !== false) {*/
                        $uploadOk = 1;
                        // Check file size
                        if ($_FILES[$docType]["size"] > 3000000) {
                            $msg .= "<br>Sorry, your file is too large.";
                            $uploadOk = 0;
                        }
                        // Allow certain file formats
                        if (strtolower($imageFileType) != "jpg" &&
                            strtolower($imageFileType) != "png" &&
                            strtolower($imageFileType) != "jpeg" &&
                            strtolower($imageFileType) != "gif" &&
                            strtolower($imageFileType) != "pdf")
                        {
                            $msg .= "<br>Please your image file in ".$DocTypeArr[$j].", only JPG, JPEG, PNG, GIF & PDF files are allowed.";
                            $uploadOk = 0;
                        }

                        // Check if $uploadOk is set to 0 by an error
                        if ($uploadOk == 0) {
                            $msg .= "<br>Sorry, your file was not uploaded. [".$DocTypeArr[$j]."]";
                        // if everything is ok, try to upload file
                        }
                        else {
                           if (file_exists($target_file)) {
                              $source = $target_file;
                              $dest = $new_target_dir_olds.$nFileName."_".$date_today."_".$curr_time.".".$imageFileType;
                              /*echo "<br>SOURCE : " .  $source;
                              echo "<br>DESTIN : " .  $dest;*/
                              copy($source, $dest);
                              //unlink($source);
                           }

                           if (move_uploaded_file($_FILES[$docType]["tmp_name"], $target_file)) {
                              $msg .= "<br>The file ". basename( $_FILES[$docType]["name"]). " has been uploaded.";
                           } else {
                              $msg .= "<br>$target_file";
                              $msg .= "<br>".$_FILES[$docType]["tmp_name"];

                              $msg .= "<br>Sorry, there was an error uploading your file. [".$DocTypeArr[$j]."]";
                           }
                        }

                     /*}*/
                  }
               }
            }
         ?>
         <?php doTitleBar(getvalue("paramTitle")); ?>
         <div class="container-fluid margin-top10">
            <?php
               if ($GLOBALS["UserCode"]!="COMPEMP")
               {
                  $attr = ["empRefId"=>getvalue("txtRefId"),
                           "empLName"=>getvalue("txtLName"),
                           "empFName"=>getvalue("txtFName"),
                           "empMName"=>getvalue("txtMidName")];
                  $emp_refid = EmployeesSearch($attr);
               } else {
                  $emp_refid = getvalue("hEmpRefId");
                  $rowEMP = FFirstRefId("employees",$emp_refid,"*");
                  $emp_LastName = $rowEMP["LastName"];
                  $emp_FirstName = $rowEMP["FirstName"];
                  $emp_MiddleName = $rowEMP["MiddleName"];
                  doHidden("txtRefId",$emp_refid,"");
            ?>
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        <span class="parentBadge-tree">
                           EMP. REF.ID :<span class="badge childBadge-tree"><?php echo $emp_refid; ?></span>
                        </span>
                        <span class="parentBadge-tree">
                           LAST NAME :<span class="badge childBadge-tree"><?php echo $emp_LastName; ?></span>
                        </span>
                        <span class="parentBadge-tree">
                           FIRST NAME :<span class="badge childBadge-tree"><?php echo $emp_FirstName; ?></span>
                        </span>
                        <span class="parentBadge-tree">
                           MIDDLE NAME :<span class="badge childBadge-tree"><?php echo $emp_MiddleName; ?></span>
                        </span>
                     </div>
                  </div>
                  <br>
            <?php
               }
            ?>
            <div class="row">
               <div class="col-xs-12" id="div_CONTENT">
                  <?php
                     if ($msg !== "") {
                        alert("Information",$msg);
                     }
                  ?>
                        <div class="mypanel">
                           <div class="panel-top bgTree">Select image/pdf to upload:</div>
                           <div class="panel-mid-litebg">
                              <?php
                                 $rsDocType = SelectEach("AttachDocType","");
                                 if ($rsDocType) {
                                    while($row = mysqli_fetch_assoc($rsDocType)) {
                                       echo
                                       '<div class="border1 padd15 margin-top">
                                          <label>'.$row["Name"].':</label>
                                          <input type="file" name="file'.$row["Code"].'" id="file'.$row["Code"].'">
                                       </div>'."\n";
                                    }
                                 }
                              ?>
                           </div>
                           <div class="panel-bottom">
                              <input type="hidden" id="hSubmit" name="hSubmit" value="YES">
                              <button type="submit"
                                      value="Upload"
                                      class="btn-cls4-sea trnbtn"
                                      name="btnSUBMIT">
                                 <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;
                                 SUBMIT
                              </button>
                              <?php
                                 //createButton("SUBMIT","btnSUBMIT","btn-cls4-sea","fa-floppy-o","onclick='submitUploadForm()'");
                                 createButton("CANCEL","btnUplCANCEL","btn-cls4-red","fa-undo","");
                              ?>
                           </div>
                        </div>
               </div>
            </div>
         </div>
         <?php
            footer();
            include "varHidden.e2e.php";
         ?>
         </div>
      </form>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_Upload"); ?>"></script>
   </body>
</html>







