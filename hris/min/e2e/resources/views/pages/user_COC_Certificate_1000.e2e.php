<?php
   require_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   require_once pathClass.'0620RptFunctions.e2e.php';
   require_once pathClass.'DTRFunction.e2e.php';
   require_once "conn.e2e.php";
   $whereClause = "WHERE RefId = ".getvalue('hUserRefId');
   $year = date("Y",time());
   $arr_month =[
     "January",
     "February",
     "March",
     "April",
     "May",
     "June",
     "July",
     "August",
     "September",
     "October",
     "November",
     "December"
   ];
?>
<!DOCTYPE html>
<html>
   <head>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script type="text/javascript">
         $(document).ready(function () {
            $("#btnPrint").click(function () {
               var head = $("head").html();
               printDiv('div_CONTENT',head);
            });
         });
      </script>
      <style type="text/css">
         th {
            text-align: center;
         }
         @page { size: landscape; }
      </style>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis") ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar ("Certificate of COC Earned"); ?>
            <div class="container-fluid margin-top">
               <button type="button" id="btnPrint" class="btn-cls4-lemon">PRINT</button>
               <div class="row">
                  <div class="col-sm-11" id="div_CONTENT">
                     <?php
                        $rsEmployees = SelectEach("employees",$whereClause);
                        if ($rsEmployees) {
                           while ($row = mysqli_fetch_assoc($rsEmployees)) { 
                              $emprefid      = $row["RefId"];
                              $LastName      = $row["LastName"];
                              $FirstName     = $row["FirstName"];
                              $MiddleName    = $row["MiddleName"];
                              $MiddleInitial = substr($row["MiddleName"], 0,1);
                              $ExtName       = $row["ExtName"];
                              $FullName      = $FirstName." ".$MiddleInitial." ".$LastName." ".$ExtName;
                              $where         = "WHERE EmployeesRefId = '$emprefid' AND NameCredits = 'VL'";
                              $start_date    = FindFirst("employeescreditbalance",$where,"BegBalAsOfDate");
                              if (!$start_date) {
                                 $start_date = date("Y-m-d",time());
                              }


                              $where_coc     = "WHERE EmployeesRefId = '$emprefid' AND NameCredits = 'OT' AND EffectivityYear = '$year'";
                              $coc_rs        = FindFirst("employeescreditbalance",$where_coc,"EffectivityYear, BeginningBalance");
                              if (!$coc_rs) {
                                 $coc_year      = date("Y",time());
                                 $coc_balance   = 0;
                              } else {
                                 $coc_balance   = $coc_rs["BeginningBalance"];
                                 $coc_year      = $coc_rs["EffectivityYear"];
                              }

                              $start_month         = date("m",strtotime($start_date." + 1 Day"));
                              $end_month           = 12;
                              $year                = date("Y",time());
                              $arr_credit          = computeCredit($emprefid,$start_month,$end_month,$year,$year);
                              $accum_balance       = intval($arr_credit["OT"]);
                              $balance             = intval($coc_balance);
                              $hrs_min             = convertToHoursMins($balance);
                              $accum_hrs_min       = convertToHoursMins($accum_balance);
                              if ($hrs_min != "") {
                                 $hrs           = explode(":", $hrs_min)[0];
                                 $min           = explode(":", $hrs_min)[1];
                                 $hrs_val       = convertNumberToWord($hrs);
                                 $min_val       = convertNumberToWord($min);   
                              } else {
                                 $hrs           = 0;
                                 $min           = 0;
                                 $hrs_val       = convertNumberToWord(0);
                                 $min_val       = convertNumberToWord(0);   
                              }


                              if ($accum_hrs_min != "") {
                                 $accum_hrs           = explode(":", $accum_hrs_min)[0];
                                 $accum_min           = explode(":", $accum_hrs_min)[1];
                                 $accum_hrs_val       = convertNumberToWord($accum_hrs);
                                 $accum_min_val       = convertNumberToWord($accum_min);   
                              } else {
                                 $accum_hrs           = 0;
                                 $accum_min           = 0;
                                 $accum_hrs_val       = convertNumberToWord(0);
                                 $accum_min_val       = convertNumberToWord(0);   
                              }
                              
                              if (intval($accum_hrs) > 1) {
                                 if (intval($accum_min) > 0) {
                                    $OT            = $accum_hrs_val. " (".intval($accum_hrs).") hours and $accum_min_val (".intval($accum_min).") minutes";   
                                 } else {
                                    $OT            = $accum_hrs_val. " (".intval($accum_hrs).") hours";   
                                 }
                              } else {
                                 if (intval($accum_min) > 0) {
                                    $OT            = $accum_hrs_val. " (".intval($accum_hrs).") hour and $accum_min_val (".intval($accum_min).") minutes";   
                                 } else {
                                    $OT            = $accum_hrs_val. " (".intval($accum_hrs).") hour";   
                                 }
                              }
                              ${"COC_Arr_".$emprefid} = array();
                              ${"CTO_Arr_".$emprefid} = array();
                              $overtime_request = SelectEach("overtime_request","WHERE EmployeesRefId = '$emprefid' AND Status = 'Approved' AND (WithPay = '0' OR WithPay IS NULL) ORDER BY StartDate");
                              if ($overtime_request) {
                                 $date_str      = "";
                                 $dummy_date_ot = "";
                                 while ($ot_row = mysqli_fetch_assoc($overtime_request)) {
                                    $start      = $ot_row["StartDate"];
                                    $end        = $ot_row["EndDate"];
                                    $ot_month   = date("m",strtotime($start));
                                    $ot_date    = $year."-".$ot_month."-01";
                                    if ($dummy_date_ot == "") {
                                       $dummy_date_ot = $ot_date;
                                    }
                                    if ($dummy_date_ot != $ot_date) {
                                       $date_str = "";
                                    }
                                    if (date("Y",strtotime($start)) == $year) {
                                       if ($start == $end) {
                                          $ot_day        = date("d",strtotime($start));
                                          $date_str      .= $ot_day.", ";
                                       } else {
                                          $ot_start_day  = date("d",strtotime($start));
                                          $ot_end_day    = date("d",strtotime($end));
                                          $date_str      .= $ot_start_day." - ".$ot_end_day.", ";
                                       }
                                       ${"COC_Arr_".$emprefid}[$ot_date]["Dates"] = $date_str;   
                                    }
                                    $dummy_date_ot = $ot_date;
                                    
                                 }
                              }
                              $where_cto = "WHERE EmployeesRefId = '$emprefid'";
                              $where_cto .= " AND ApplicationDateFrom <= '".date("Y-m-d",time())."'";
                              $where_cto .= " AND Status = 'Approved' ORDER BY ApplicationDateFrom";
                              $employees_cto = SelectEach("employeescto",$where_cto);
                              if ($employees_cto) {
                                 while ($cto_row = mysqli_fetch_assoc($employees_cto)) {
                                    $date_str = "";
                                    $start         = $cto_row["ApplicationDateFrom"];
                                    $end           = $cto_row["ApplicationDateTo"];
                                    $cto_month     = date("m",strtotime($start));
                                    $cto_date      = $year."-".$cto_month."-01";
                                    $cto           = $cto_row["Hours"] * 60;
                                    if (date("Y",strtotime($start)) == $year) {
                                       if ($start == $end) {
                                          $cto_date    = $start;
                                          $cto_day     = date("d",strtotime($start));
                                          $date_str   .= $cto_day.", ";
                                       } else {
                                          $cto_date         = $start;
                                          $cto_diff         = dateDifference($start,$end) + 1;
                                          $cto              = $cto * $cto_diff;
                                          $cto_start_day    = date("d",strtotime($start));
                                          $cto_end_day      = date("d",strtotime($end));
                                          $date_str         .= $cto_start_day." - ".$cto_end_day.", ";
                                       }
                                       ${"CTO_Arr_".$emprefid}[$cto_date]["Dates"]   = $date_str;
                                       ${"CTO_Arr_".$emprefid}[$cto_date]["Used"]    = $cto;
                                    }
                                 }
                              }
                              $dtr_process = SelectEach("dtr_process","WHERE EmployeesRefId = '$emprefid' AND Year = '$year'");
                              if ($dtr_process) {
                                 while ($dtr_row = mysqli_fetch_assoc($dtr_process)) {
                                    $coc_month  = $dtr_row["Month"];
                                    $coc        = $dtr_row["Total_COC_Hr"];
                                    $coc_date   = $year."-".$coc_month."-01";
                                    ${"COC_Arr_".$emprefid}[$coc_date]["Earned"] = $coc;
                                 }
                              }

                              ksort(${"COC_Arr_".$emprefid});
                              ksort(${"CTO_Arr_".$emprefid});
                              /*echo "<pre>";
                              echo json_encode(${"CTO_Arr_".$emprefid},JSON_PRETTY_PRINT);
                              echo "</pre>";*/

                     ?>
                        <div class="row" style="page-break-after: always;">
                           <div class="col-xs-12">
                              <div class="row">
                                 <div class="col-xs-12 text-center">
                                    <h3>Certificate of COC Earned</h3>
                                 </div>
                              </div>
                              <br><br><br>
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    Employee Name: <b><?php echo $FullName; ?></b>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    <table border="2" width="100%">
                                       <thead>
                                          <tr>
                                             <th>Date</th>
                                             <th>No. of Hours Earn<br>COCs/Beginning Balance</th>
                                             <th>Date of CTO</th>
                                             <th>Used COCs</th>
                                             <th>Remaining COCs</th>
                                             <th>Remarks</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <tr>
                                             <td>CY <?php echo $coc_year; ?> Beginning Balance</td>
                                             <td class="text-center">
                                                <?php
                                                   if (intval($hrs) > 1) {
                                                      echo intval($hrs)." hrs, ".intval($min)." mins";
                                                   } else {
                                                      echo intval($hrs)." hr, ".intval($min)." mins";   
                                                   }
                                                ?>
                                             </td>
                                             <td></td>
                                             <td></td>
                                             <td></td>
                                             <td>&nbsp;</td>
                                          </tr>
                                          <?php
                                             //$balance
                                             $new_balance = "";
                                             foreach (${"COC_Arr_".$emprefid} as $key => $value) {
                                                $month = date("F",strtotime($key));
                                                if (isset($value["Dates"])) {
                                                   $dates   = $value["Dates"]; 
                                                } else {
                                                   $dates   = "";
                                                }
                                                if (isset($value["Earned"])) {
                                                   $earning = $value["Earned"];   
                                                } else {
                                                   $earning = 0;
                                                }
                                                $arr_converted = convertToHoursMins($earning);



                                                if ($new_balance == "") {
                                                   $new_balance = $earning + $balance;
                                                } else {
                                                   $new_balance += $earning; 
                                                }
                                                if ($earning > 0) {
                                                   $converted_balance   = convertToHoursMins($new_balance);
                                                   $converted_arr       = explode(":", $converted_balance);
                                                   $hrs_new_balance     = intval($converted_arr[0]);
                                                   $min_new_balance     = intval($converted_arr[1]);
                                                   $str_balance         = "";

                                                   if ($hrs_new_balance > 1) {
                                                      $str_balance .= $hrs_new_balance." hrs, ";
                                                   } else {
                                                      $str_balance .= $hrs_new_balance." hr, ";
                                                   }

                                                   if ($min_new_balance > 1) {
                                                      $str_balance .= $min_new_balance." mins";
                                                   } else {
                                                      $str_balance .= $min_new_balance." min";
                                                   }


                                                   $earning_arr         = explode(":", $arr_converted);
                                                   $hrs_earning         = intval($earning_arr[0]);
                                                   $min_earning         = intval($earning_arr[1]);
                                                   $str_earning         = "";

                                                   if ($hrs_earning > 1) {
                                                      $str_earning .= $hrs_earning." hrs, ";
                                                   } else {
                                                      $str_earning .= $hrs_earning." hr, ";
                                                   }

                                                   if ($min_earning > 1) {
                                                      $str_earning .= $min_earning." mins";
                                                   } else {
                                                      $str_earning .= $min_earning." min";
                                                   }




                                                   echo '<tr>';
                                                   echo '
                                                      <td>'.$month." ".$dates.'</td>
                                                      <td class="text-center">'.$str_earning.'</td>
                                                      <td></td>
                                                      <td></td>
                                                      <td class="text-center">'.$str_balance.'</td>
                                                      <td></td>
                                                   ';
                                                   echo '</tr>';   
                                                }
                                             }
                                             foreach (${"CTO_Arr_".$emprefid} as $nkey => $nvalue) {
                                                $month = date("F",strtotime($nkey));
                                                if (date("F",strtotime($nkey)) == $month) {
                                                   $new_month           = date("F",strtotime($nkey));
                                                   $new_date            = $nvalue["Dates"];
                                                   $cto_used            = $nvalue["Used"];
                                                   $new_balance         = $new_balance - $cto_used;
                                                   if ($new_balance > 0) {
                                                      $converted_balance   = convertToHoursMins($new_balance);
                                                      $converted_arr       = explode(":", $converted_balance);
                                                      $hrs_new_balance     = intval($converted_arr[0]);
                                                      $min_new_balance     = intval($converted_arr[1]);
                                                      $str_balance         = "";

                                                      if ($hrs_new_balance > 1) {
                                                         $str_balance .= $hrs_new_balance." hrs, ";
                                                      } else {
                                                         $str_balance .= $hrs_new_balance." hr, ";
                                                      }
                                                      if ($min_new_balance > 1) {
                                                         $str_balance .= $min_new_balance." mins";
                                                      } else {
                                                         $str_balance .= $min_new_balance." min";
                                                      }   
                                                   }
                                                   


                                                   $converted_cto       = convertToHoursMins($cto_used);
                                                   $cto_arr             = explode(":", $converted_cto);
                                                   $hrs_cto             = intval($cto_arr[0]);
                                                   $min_cto             = intval($cto_arr[1]);
                                                   $str_cto             = $hrs_cto." hrs";
                                                   echo '<tr>';
                                                   echo '
                                                      <td></td>
                                                      <td></td>
                                                      <td>'.$new_month." ".$new_date.'</td>
                                                      <td class="text-center">'.$str_cto.'</td>
                                                      <td class="text-center">'.$str_balance.'</td>
                                                      <td></td>
                                                   ';
                                                   echo '</tr>';           
                                                }
                                             }   
                                          ?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                     <?php
                           }  
                        }
                     ?>
                  </div>
               </div>
            </div>
            <?php
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>