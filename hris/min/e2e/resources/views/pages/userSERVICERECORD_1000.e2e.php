<?php
   require_once "constant.e2e.php";
   require_once pathClass.'0620RptFunctions.e2e.php';
   $table = "employees";
   $service_record_arr = array();
   $EmployeesRefId = getvalue("hEmpRefId");
   $rsEmployees = SelectEach("employees","WHERE RefId = '$EmployeesRefId'");
   $Signatory_Position = $Signatory_FullName = "&nbsp;";
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script type="text/javascript">
         $(document).ready(function () {
            $("#btnPrint").click(function () {
               var head = $("head").html();
               printDiv('div_CONTENT',head);
            });
         });
      </script>
      <style type="text/css">
         @media print {
            body {
               font-size: 9pt;
            }
            thead {
               font-size: 9pt;  
            }
            tbody {
               font-size: 8pt !important;
            }
            td {
               font-size: 8pt !important;
            }

            /*2480 pixels x 3508*/
         }
         body {
            background:#ffffff;
            color:#000;
            font-size:10pt;
            font-family:Arial;
         }
      </style>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar ("SERVICE RECORD"); ?>
            <div class="container-fluid margin-top">
               <button type="button" id="btnPrint" class="btn-cls4-lemon">PRINT</button>
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div class="container-fluid rptBody">
                        <?php
                           $count = 0;
                           if ($rsEmployees) {
                              while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                                 $page_count    = 0;
                                 $empid         = $row_emp["RefId"];
                                 $MiddleInitial = substr($row_emp["MiddleName"], 0,1);
                                 $FullName      = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$MiddleInitial;
                                 if ($row_emp["Sex"] == "F") {
                                    if ($row_emp["CivilStatus"] == "Ma") {
                                       $MaidenName = $row_emp["MiddleName"];
                                    } else {
                                       $MaidenName = "";
                                    }
                                 } else {
                                    $MaidenName = "";
                                 }
                                 $emprefid      = $row_emp["RefId"];
                                 $where         = "WHERE EmployeesRefId = $emprefid ORDER BY EffectivityDate";
                                 $emp_movement  = SelectEach("employeesmovement",$where);
                                 $record_count  = mysqli_num_rows($emp_movement);
                                 if ($record_count <= 11) {
                                    $page_count = 1;
                                 } else {
                                    for ($i=1; $i <= $record_count; $i++) { 
                                       $trigger = $i % 11;
                                       if ($trigger == 0) $page_count++;
                                    }
                                    $check = $page_count * 11;
                                    if ($record_count > $check) $page_count++;
                                 }
                                 include 'rpt/inc/inc_service_record_1000.php';
                        ?>
                        
                        <?php
                              }
                           } else {
                              echo "No Result For Criteria $searchCriteria";
                           }
                        ?>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>



