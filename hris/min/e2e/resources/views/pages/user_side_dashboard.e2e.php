<?php
   if (isset($settings["PIS"])) {
      $PIS_Name = $settings["PIS"];
   } else {
      $PIS_Name = "PERSONAL INFORMATION SYSTEM (PIS)";
   }
   if (isset($settings["AMS"])) {
      $AMS_Name = $settings["AMS"];
   } else {
      $AMS_Name = "ATTENDANCE MANAGEMENT SYSTEM (AMS)";
   }
   if (isset($settings["PMS"])) {
      $PMS_Name = $settings["PMS"];
   } else {
      $PMS_Name = "PAYROLL MANAGEMENT SYSTEM (PMS)";
   }
?>
<div class="row" style="margin:0px;">
   <div class="col-xs-6" style="margin:0px;">
      <?php
         include 'conn.e2e.php';
         $sql = "SELECT * FROM `employees` WHERE RefId = ".getvalue("hEmpRefId");
         $rs = mysqli_query($conn,$sql);
         if (mysqli_num_rows($rs) > 0)
         {
            $row = mysqli_fetch_assoc($rs)

      ?>
         <div class="row">
            <div class="col-xs-6 text-center">
               <p>
               <?php
                  if ($row['PicFilename'] != "") {
                     if (file_exists(img($row['CompanyRefId']."/EmployeesPhoto/".$row['PicFilename']))) {
                        echo '<img src="'.img($row['CompanyRefId']."/EmployeesPhoto/".$row['PicFilename']).'" style="width:150px;">';
                     } else {
                        echo '<img src="'.img("nopic.png").'" style="width:150px;">';
                     }
                  } else {
                     echo '<img src="'.img("nopic.png").'" style="width:150px;">';
                  }
               ?>
               </p>
               <p>
               <button type="button" class="btn-cls2-red" id="btnChangePW" name="btnChangePW">Change Password</button>
               </p>
            </div>
            <div class="col-xs-6">
               <?php
                  $result = FindFirst('empinformation',"WHERE EmployeesRefId = ".$row["RefId"],"*");
                  $info = array_merge($row,$result);
                  echo '
                     <div class="row" style="font-size:10pt;">
                        <div class="col-xs-12">
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <b>Employee Name:</b>
                                 <br>
                                 '.$info["LastName"].", ".$info["FirstName"].'&nbsp;
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <b>Plantilla Item No:</b>
                                 <br>
                                 '.getRecord("positionitem",$info["PositionItemRefId"],"Name").'&nbsp;
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <b>Position:</b>
                                 <br>
                                 '.getRecord("position",$info["PositionRefId"],"Name").'&nbsp;
                              </div>
                           </div>';

                           if (getvalue("hCompanyID") == "35") {
                              echo '
                                 <div class="row margin-top">
                                    <div class="col-xs-12">
                                       <b>Section:</b>
                                       <br>
                                       '.getRecord("office",$info["OfficeRefId"],"Name").'&nbsp;
                                    </div>
                                 </div>
                              ';
                           } else {
                              echo '
                                 <div class="row margin-top">
                                    <div class="col-xs-12">
                                       <b>Office:</b>
                                       <br>
                                       '.getRecord("office",$info["OfficeRefId"],"Name").'&nbsp;
                                    </div>
                                 </div>
                              ';
                           }


                           echo '
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <b>Division:</b>
                                 <br>
                                 '.getRecord("division",$info["DivisionRefId"],"Name").'&nbsp;
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <b>Employee ID:</b>
                                 <br>
                                 '.$info["AgencyId"].'&nbsp;
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <b>Emp Status:</b>
                                 <br>
                                 '.getRecord("empstatus",$info["EmpStatusRefId"],"Name").'&nbsp;
                              </div>
                           </div>
                        </div>
                     </div>
                  ';
               ?>
            </div>
         </div>
      <?php
         }
      ?>
   </div>
   <div class="col-xs-1"></div>
   <div class="col-xs-4">
      <div id="panelReminders">
         <div class="row">
            <div class="col-xs-12">
               <div class="panel-top" for="PIS_menu"id="PIS_Header">
                  <?php echo $PIS_Name; ?>
               </div>
               <div class="panel-mid" style="border-bottom: 1px solid black;" id="PIS_menu">
                  <div class="row">
                     <div class="col-xs-12">
                        <button type="button"
                             class="Menu btn-cls2-tree"
                             pre="scrn"
                             route="201File"
                                id="userPDS">
                           <li><u>PERSONAL DATA SHEET (PDS)</u></li>
                        </button>
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        <button type="button"
                             class="Menu btn-cls2-tree"
                             pre="user"
                             route="Work_Exp_Attachment" <?php echo $disabled; ?>>
                           <li><u>WORK EXPERIENCE ATTACHMENT</u></li>
                        </button>
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        <button type="button"
                             class="Menu btn-cls2-tree"
                             pre="scrn"
                             route="EmpAttach" <?php echo $disabled; ?>>
                           <li><u>201 FILE ATTACHMENTS</u></li>
                        </button>
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        <button type="button"
                             class="Menu btn-cls2-tree"
                             pre="scrn"
                             route="201Update" <?php echo $disabled; ?>>
                           <li><u>201 FILE UPDATE</u></li>
                        </button>
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        <?php
                           //if (file_exists("userSERVICERECORD_".$cid.".e2e.php")) {
                        ?>
                        <!-- <button type="button"
                             class="Menu btn-cls2-tree"
                             pre="user"
                             route="SERVICERECORD_<?php echo $cid; ?>" <?php echo $disabled; ?>>
                           <li><u>SERVICE RECORD</u></li> -->
                        </button>
                        <?php //} ?>
                        <button type="button"
                             class="Menu btn-cls2-tree"
                             pre="scrn"
                             route="EmpMovement" <?php echo $disabled; ?>>
                           <li><u>SERVICE RECORD</u></li>
                        </button>
                     </div>
                  </div>
                  
               </div>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-12">
            <div class="panel-top" id="AMS_Header">
               <?php echo $AMS_Name; ?>
            </div>
               <div class="panel-mid" style="border-bottom: 1px solid black;" id="AMS_menu">
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        <button type="button" 
                             class="Menu btn-cls2-tree"
                             pre="ams"
                             route="TrnDTR"
                             id="userDTR">
                        <li><u>DAILY TIME RECORD (DTR)</u></li>
                     </button>
                     </div>
                  </div>
                  <?php
                     switch (getvalue("hCompanyID")) {
                        case '14':
                           echo '
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    <button type="button"
                                         class="Menu btn-cls2-tree"
                                         pre="user"
                                         route="LEAVECARD_14">
                                       <li><u>LEAVE CARD</u></li>
                                    </button>
                                 </div>
                              </div>
                           ';
                           break;
                        case '21':
                           echo '
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    <button type="button"
                                         class="Menu btn-cls2-tree"
                                         pre="user"
                                         route="LEAVECARD_21">
                                       <li><u>LEAVE CARD</u></li>
                                    </button>
                                 </div>
                              </div>
                           ';
                           break;
                        case '35':
                           echo '
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    <button type="button"
                                         class="Menu btn-cls2-tree"
                                         pre="user"
                                         route="LEAVECARD_35">
                                       <li><u>LEAVE CARD</u></li>
                                    </button>
                                 </div>
                              </div>
                           ';
                           break;
                        case '1000':
                           echo '
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    <button type="button"
                                         class="Menu btn-cls2-tree"
                                         pre="user"
                                         route="_COC_Certificate_1000">
                                       <li><u>COC Certificate</u></li>
                                    </button>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    <button type="button"
                                         class="Menu btn-cls2-tree"
                                         pre="user"
                                         route="_Service_Rendered_1000">
                                       <li><u>Certificate of Service Rendered</u></li>
                                    </button>
                                 </div>
                              </div>
                           ';
                           break;
                        default:
                           echo '
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    <button type="button"
                                         class="Menu btn-cls2-tree"
                                         pre="ams"
                                         route="LeaveCard">
                                       <li><u>LEAVE CARD</u></li>
                                    </button>
                                 </div>
                              </div>
                           ';         
                           break;
                     }
                  ?>
                  
                  <?php bar();?>
                  <!-- <div class="row margin-top">
                     <div class="col-xs-12">
                        <button type="button"
                             class="Menu btn-cls2-tree"
                             pre="ams"
                             route="ReqChangeShift" <?php //echo $disabled; ?>>
                           <li><u>REQUEST FOR CHANGE SHIFT</u></li>
                        </button>
                     </div>
                  </div> -->
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        <button type="button"
                             class="Menu btn-cls2-tree"
                             pre="ams"
                             route="ReqOvertime">
                           <li><u>REQUEST FOR OVERTIME</u></li>
                        </button>
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        <button type="button"
                             class="Menu btn-cls2-tree"
                             pre="ams"
                             route="ReqForceLeave">
                           <li><u>REQUEST FOR CANCELLATION OF LEAVE</u></li>
                        </button>
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        <button type="button"
                             class="Menu btn-cls2-tree"
                             pre="user"
                             route="ReqAttendanceRegistration">
                           <li><u>REQUEST FOR ATTENDANCE REGISTRATION</u></li>
                        </button>
                     </div>
                  </div>
                  <?php bar();?>
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        <button type="button"
                             class="Menu btn-cls2-tree"
                             pre="user"
                             route="AvailLeave">
                           <li><u>APPLICATION FOR LEAVE</u></li>
                        </button>
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        <button type="button"
                             class="Menu btn-cls2-tree"
                             pre="ams"
                             route="AvailCTO">
                           <li><u>APPLICATION FOR CTO</u></li>
                        </button>
                     </div>
                  </div>
                  <?php
                      if (getvalue("hCompanyID") != "35") {
                  ?>
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        <button type="button"
                             class="Menu btn-cls2-tree"
                             pre="ams"
                             route="AvailAuthority">
                           <li><u>APPLICATION FOR OFFICE AUTHORITY</u></li>
                        </button>
                     </div>
                  </div>
                  <?php } ?>
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        <button type="button"
                             class="Menu btn-cls2-tree"
                             pre="user"
                             route="AvailMonetization">
                           <li><u>APPLICATION FOR LEAVE MONETIZATION</u></li>
                        </button>
                     </div>
                  </div>
                  <?php
                      if (getvalue("hCompanyID") == "28") {
                  ?>
                  <?php bar();?>
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        <button type="button"
                             class="Menu btn-cls2-tree"
                             pre="user"
                             route="OvertimeStatement_28">
                           <li><u>Overtime Statement (ANNEX C)</u></li>
                        </button>
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        <button type="button"
                             class="Menu btn-cls2-tree"
                             pre="user"
                             route="SummaryCOC_28">
                           <li><u>Summary of Overtime (ANNEX D)</u></li>
                        </button>
                     </div>
                  </div>
                  <?php } ?>
               </div>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-12">
               <div class="panel-top" id="PMS_Header">
                  <?php echo $PMS_Name; ?>
               </div>
               <div class="panel-mid" style="border-bottom: 1px solid black;" id="PMS_menu">
                  <?php
                     switch (getvalue("hCompanyID")) {
                        case '14':
                           echo '
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    <button type="button"
                                         class="Menu btn-cls2-tree"
                                         pre="user"
                                         route="PAYSLIP_14"
                                        >
                                       <li><u>PAYSLIP</u></li>
                                    </button>
                                 </div>
                              </div>
                           ';
                           break;
                        case '2':
                           echo '
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    <button type="button"
                                         class="Menu btn-cls2-tree"
                                         pre="user"
                                         route="PAYSLIP_35" 
                                        '.$disabled.'>
                                       <li><u>PAYSLIP</u></li>
                                    </button>
                                 </div>
                              </div>
                           ';
                           break;
                        case '28':
                           echo '
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    <button type="button"
                                         class="Menu btn-cls2-tree"
                                         pre="user"
                                         route="PAYSLIP_28"
                                        >
                                       <li><u>PAYSLIP</u></li>
                                    </button>
                                 </div>
                              </div>
                           ';
                           break;
                        case '21':
                           echo '
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    <button type="button"
                                         class="Menu btn-cls2-tree"
                                         pre="user"
                                         route="PAYSLIP_21"
                                        >
                                       <li><u>PAYSLIP</u></li>
                                    </button>
                                 </div>
                              </div>
                           ';
                           break;
                        case '35':
                           echo '
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    <button type="button"
                                         class="Menu btn-cls2-tree"
                                         pre="user"
                                         route="PAYSLIP_35"
                                         disabled>
                                       <li><u>PAYSLIP</u></li>
                                    </button>
                                 </div>
                              </div>
                           ';
                           break;
                        default:
                           echo '
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    <button type="button"
                                         class="Menu btn-cls2-tree"
                                         pre="user"
                                         route="PAYSLIP_21"
                                        >
                                       <li><u>PAYSLIP</u></li>
                                    </button>
                                 </div>
                              </div>
                           ';
                           break;
                     }
                  ?>
               </div>
            </div>
         </div>
         <div class="row margin-top" id="spms_canvas">
            <div class="col-xs-12">
               <div class="panel-top" id="SPMS_Header">STRATEGIC PERFORMANCE MANAGEMENT SYSTEM (SPMS)</div>
               <div class="panel-mid" style="border-bottom: 1px solid black;" id="SPMS_menu">
                  <!-- <div class="row margin-top">
                     <div class="col-xs-12">
                        <button type="button"
                             class="Menu btn-cls2-tree"
                             pre="spms"
                             route="_IPCR_14"
                            >
                           <li><u>INDIVIDUAL PERFORMANCE COMMITMENT AND REVIEW [IPCR]</u></li>
                        </button>
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        <button type="button"
                             class="Menu btn-cls2-tree"
                             pre="spms"
                             route="IPAR"
                            >
                           <li><u>INDIVIDUAL PERFORMANCE ACCOMPLISHMENT AND REVIEW [IPAR]</u></li>
                        </button>
                     </div>
                  </div> -->
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        <button type="button"
                             class="Menu btn-cls2-tree"
                             pre="spms"
                             route="_PerformanceCommitmentReview"
                            >
                           <li><u>PERFORMANCE REVIEW</u></li>
                        </button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row margin-top" id="ldms_canvas" style="display: ;">
            <div class="col-xs-12">
               <div class="panel-top" id="LDMS_Header">LEARNING AND DEVELOPMENT (LDMS)</div>
               <div class="panel-mid" style="border-bottom: 1px solid black;" id="LDMS_menu">
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        <button type="button"
                             class="Menu btn-cls2-tree"
                             pre="ldms"
                             route="_trainings_and_programs"
                            >
                           <li><u>TRAININGS AND PROGRAMS</u></li>
                        </button>
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        <button type="button"
                             class="Menu btn-cls2-tree"
                             pre="user"
                             route="_applied_trainings"
                            >
                           <li><u>LIST OF APPLIED TRAININGS</u></li>
                        </button>
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        <button type="button"
                             class="Menu btn-cls2-tree"
                             pre="user"
                             route="_REAP"
                            >
                           <li><u>RE-ENTRY ACTION PLAN</u></li>
                        </button>
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        <button type="button"
                             class="Menu btn-cls2-tree"
                             pre="user"
                             route="_LAP"
                            >
                           <li><u>LEARNING APPLICATION PLAN</u></li>
                        </button>
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        <button type="button"
                             class="Menu btn-cls2-tree"
                             pre="user"
                             route="_EvaluationForm"
                            >
                           <li><u>EVALUATION FORM</u></li>
                        </button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-12">
               <div class="panel-top" id="myrequest">
                  MY REQUEST(S)
               </div>
               <div class="panel-mid" id="myrequestView" style="border-bottom: 1px solid black;">
                  <div class="row" style="margin:0px;">
                     <div class="col-xs-12">
                        <?php
                           include 'inc/inc_employees_req_list.e2e.php';
                        ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-12">
               <div class="panel-top" id="Leaves_Header">Summary Of Leaves</div>
               <div class="panel-mid" style="border-bottom: 1px solid black;" id="Leaves_menu">
                  <?php
                     include 'inc/inc_emp_leave_balances.php';
                  ?>
               </div>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-12">
               <?php
                  $emp_row = FindFirst("employees","WHERE RefId = $EmpRefId","`BiometricsID`, `CompanyRefId`");
                  if ($emp_row) {
                     $CompanyRefId = $emp_row["CompanyRefId"];
                     $BioID = $emp_row["BiometricsID"];
                     switch ($CompanyRefId) {
                        case '2':
                           include 'inc/inc_emp_notif_2.e2e.php';
                           break;
                        case '14':
                           include 'inc/inc_emp_notif_14.e2e.php';
                           break;
                     }
                  }
               ?>
            </div>
         </div>
      </div>
      <div class="mypanel margin-top" id="panelChangePW" style="display:none">
         <div class="panel-top">Change Password</div>
         <div class="panel-mid">
            <div class="row">
               <div class="col-xs-6" style="margin-left: 20px;">
                  <div class="row">
                     <div class="form-group">
                        <label>Current Password:</label>
                        <input type="password" name="currentToken" id="currentToken" class="form-input">
                     </div>
                  </div>
                  <?php entryAlert("cuPW","Wrong Current Password !!!"); ?>
                  <div class="row">
                     <div class="form-group">
                        <label>New Password:</label>
                        <input type="password" name="newToken" id="newToken" class="form-input">
                     </div>
                  </div>
                  <div class="row">
                     <div class="form-group">
                        <label>Re-Type Password:</label>
                        <input type="password" name="reToken" id="reToken" class="form-input">
                     </div>
                  </div>
                  <?php entryAlert("rePW","Characters Mismacthed In New Password !!!"); ?>
                  <div class="row">
                     <div class="form-group">
                        <button type="button" 
                        class="btn-cls2-sea"
                        id="btnChangeNow" name="btnChangeNow">Change Now
                        </button>
                        <button
                        type="button" 
                        class="btn-cls2-red"
                        id="btnChangeCancel" name="btnChangeCancel">Cancel
                        </button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="panel-bottom"></div>
      </div>
   </div>   
</div>