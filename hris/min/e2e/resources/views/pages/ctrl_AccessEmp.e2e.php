<?php
   session_start();
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
	require "conn.e2e.php";
   $p_SysUserRefId = getvalue("sysuserrefid");
   $task = getvalue("task");
if ($task == "loadAccessEmp")
{
?>

   <link href="<?php echo path("datatables/jquery.dataTables.min.css"); ?>" rel="stylesheet">
   <script type="text/javascript" src="<?php echo path("datatables/jquery.dataTables.min.js"); ?>"></script>
   <script language="JavaScript">
      $(document).ready(function() {
         $("#chk_All").click(function (){
            $("input[class*='saveFields--']").prop('checked',$("#chk_All").is(":checked"));
         });
         $('#gridTable').DataTable();
         var table = $('#gridTable').DataTable();
         // Sort by columns 1
         table
            .order([ 0, 'desc' ])
            .draw();
      });
   </script>
   <input type="hidden" id="hSysUserRefId" value="<?php echo $p_SysUserRefId; ?>">
   <table class="table table-order-column table-striped table-bordered table-hover" id="gridTable">
      <thead>
         <tr>
            <th class="txt-center"><input type="checkbox" id="chk_All" title="Check All"></th>
            <th>Employees Name</th>
            <th></th>
         </tr>
      </thead>
      <tbody>
      <?php
         $empRefId = getvalue("empRefId");
         $rsEmployee = f_Find("Employees","order by LastName, FirstName, MiddleName");
         $j = $k = 0;
         if ($rsEmployee) {
            while ($row = mysqli_fetch_assoc($rsEmployee)) {
               $qry = "WHERE SysUserRefId = ".$p_SysUserRefId;
               $qry .= " AND EmployeesRefId = ".$row['RefId'];
               $rsAccessEmployees = FindFirst("AccessEmployees",$qry,"*");
               if ($rsAccessEmployees)
               {
                  $j++;
                  echo '
                  <tr>
                     <td>&nbsp;</td>
                     <td>'.$j.' - ['.$row['RefId'].'] '.$row['LastName'].", ".$row['FirstName']." ".$row['MiddleName'].'</td>
                     <td>
                        <a style="text-decoration:none;cursor:pointer" onclick="deleteRecord('.$rsAccessEmployees["RefId"].');"
                           title="Delete This Record">
                           <img src="'.img("delete.png").'">
                        </a>
                     </td>
                  </tr>';
               } else {
                  $k++;
                  echo '
                  <tr>
                     <td class="txt-center">
                        <input type="checkbox" class="saveFields--" id="chk_'.$row['RefId'].'" value="'.$row['RefId'].'">
                     </td>
                     <td>'.$k.' - ['.$row['RefId'].'] '.$row['LastName'].", ".$row['FirstName']." ".$row['MiddleName'].'</td>
                     <td></td>
                  </tr>';
               }
            }
         }
      ?>
      </tbody>
   </table>

<?php
}
?>

<?php $conn->close(); ?>