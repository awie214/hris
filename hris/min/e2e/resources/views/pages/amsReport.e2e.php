<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style>
            option.optHeader {
               background-color:#444444;
               color:#ffffff;
               font-size:11pt;
               font-weight:600;
            }
            option {
               padding-left:20px;
            }
      </style>
      <link rel="stylesheet" href="<?php echo path("js/autocomplete/css/jquery-ui.css"); ?>" type="text/css" />
      <script type="text/javascript" src="<?php echo path("js/autocomplete/jquery-ui.js") ?>"></script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php
               doTitleBar("REPORTS");
               spacer(5);
            ?>
            <div class="mypanel ">
               <div class="panel-mid">
                     <div class="margin-top" id="rptCriteria">
                        <div class="row">
                           <div class="col-sm-1"></div>
                           <div class="col-sm-10">
                              <?php spacer(10); ?>
                              <p>
                                 <label>Choose Kind Of Report:</label>
                                 <select class="form-input rptCriteria--" id="ReportKind" name="drpReportKind" style="width:50%;font-family:12pt;">
                                    <option value="XX" disabled class="optHeader">COMPANY REPORTS / FORM</option>
                                    <?php
                                       $crit = "WHERE `CompanyRefId` = $CompanyId ";
                                       //$crit .= "AND `BranchRefId` = $BranchId "; 
                                       /*$crit .= "AND `CompanyCode` = '$CompanyCode' ";*/
                                       $crit .= "AND `SystemRefId` = 2"; 
                                       echo $crit;
                                       $rs = SelectEach("reports",$crit);
                                       $htm = "yes";
                                       if ($rs) {
                                          while ($r = mysqli_fetch_array($rs)) {
                                             echo    
                                             '<option for="'.$r["For"].'" own="'.$r["Owner"].'" ext="'.$r["ExtType"].'" value="'.$r["Filename"].'" rptname="'.$r["Name"].'">'.$r["Name"].'</option>'."\n";
                                          }   
                                       }
                                    ?>
                                    <option value="XX" disabled class="optHeader">DEFAULT REPORTS / FORM</option>
                                    <?php
                                       $crit = "WHERE `CompanyRefId` = 0 ";
                                       $crit .= "AND `BranchRefId` = 0 ";
                                       $crit .= "AND `SystemRefId` = '2'";
                                       $rs = SelectEach("reports",$crit);
                                       $htm = "yes";
                                       if ($rs) {
                                          while ($r = mysqli_fetch_array($rs)) {
                                             echo
                                             '<option for="'.$r["For"].'" ext="'.$r["ExtType"].'" own="'.$r["Owner"].'" value="'.$r["Filename"].'" rptname="'.$r["Name"].'">'.$r["Name"].'</option>'."\n";
                                          }
                                       }
                                    ?>
                                 </select>
                              </p>
                              <?php
                                 bar();
                                 echo "<h5><label>Search Criteria</label></h5>";
                                 require_once "incUtilitiesJS.e2e.php";
                                 require_once "incEmpSearchCriteria.e2e.php";
                                 bar();
                                 //echo "<h5><label>Date's Criteria</label></h5>";
                              ?>
                              <div id="CritHolder">
                                 <!-- ######################################################### -->
                                 <div class="row margin-top padd5 dateCrit" id="LeaveDateCriterias">
                                    <div class="col-sm-12">
                                       <div class="row">
                                          <div class="col-xs-6">
                                             <label>Application Date</label><br>
                                             <p>
                                                <span class="label">From:</span>
                                                <input type="text" class="form-input date-- rptCriteria--" name="txtAppDateFrom"
                                                   placeholder="Lower Date">
                                                <span class="label">To:</span>
                                                <input type="text" class="form-input date-- rptCriteria--" name="txtAppDateTo"
                                                   placeholder="Higher Date">
                                                <a href="javascript:clearFields('txtAppDateFrom,txtAppDateTo');" class="clearflds--">
                                                   <i class="fa fa-minus-square-o" aria-hidden="true" title="Clear Entry"></i>
                                                </a>
                                             </p>
                                          </div>
                                          <div class="col-xs-6">
                                             <label>Filing Date:</label><br>
                                             <p>
                                                <span class="label">From:</span>
                                                <input type="text" class="form-input date-- rptCriteria--" name="txtFilingDateFrom"
                                                   placeholder="Lower Date">
                                                <span class="label">To:</span>
                                                <input type="text" class="form-input date-- rptCriteria--" name="txtFilingDateTo"
                                                   placeholder="Higher Date">
                                                <a href="javascript:clearFields('txtFilingDateFrom,txtFilingDateTo');" class="clearflds--">
                                                   <i class="fa fa-minus-square-o" aria-hidden="true" title="Clear Entry"></i>
                                                </a>
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- ######################################################### -->
                                 <div class="row margin-top">
                                    <div class="col-xs-2">
                                       <p>
                                          <label>For The Month of:</label><br>
                                          <select name="txtAttendanceMonth" class="form-input rptCriteria--">
                                             <?php
                                                $arr_month =[
                                                              "January",
                                                              "February",
                                                              "March",
                                                              "April",
                                                              "May",
                                                              "June",
                                                              "July",
                                                              "August",
                                                              "September",
                                                              "October",
                                                              "November",
                                                              "December"
                                                            ];
                                                $curr_month = date("F",time());
                                                foreach ($arr_month as $key => $value) {
                                                   $idx = $key+1;
                                                   if ($idx <= 9) $idx = "0".$idx;
                                                   if ($curr_month == $value) {
                                                      $selected = "selected";
                                                   } else {
                                                      $selected = "";
                                                   }
                                                   echo '<option value="'.$idx.'" '.$selected.'>'.$value.'</option>';
                                                }
                                             ?>
                                          </select>
                                       </p>
                                    </div>
                                    <div class="col-xs-2">
                                       <p>
                                          <label>For The Year of:</label>
                                          <select name="txtAttendanceYear" class="form-input rptCriteria--">
                                             <?php
                                                $backyear  = date("Y",time());
                                                for ($i=0; $i <= 3 ; $i++) { 
                                                   echo '<option value="'.($backyear-$i).'">'.($backyear-$i).'</option>';
                                                }
                                             ?>
                                          </select>
                                       </p>
                                    </div>
                                    <div class="col-xs-2">
                                       <p>
                                          <label>Start Date:</label>
                                          <br>
                                          <input type="text" name="txtStartDate" id="txtStartDate" class="form-input rptCriteria-- date--">
                                       </p>
                                    </div>
                                    <div class="col-xs-2">
                                       <p>
                                          <label>End Date:</label>
                                          <br>
                                          <input type="text" name="txtEndDate" id="txtEndDate" class="form-input rptCriteria-- date--">
                                       </p>
                                    </div>
                                 </div>
                                 <!-- ######################################################### -->
                                 <div class="row margin-top dateCrit" id="CTODateCriterias">
                                    <div class="col-xs-12 txt-right">
                                       CTODateCriterias
                                    </div>
                                 </div>
                                 <!-- ######################################################### -->
                                 <div class="margin-top dateCrit" id="AuthorityDateCriterias">
                                    <div class="row">
                                       <div class="col-xs-12">
                                          <label>Application Date</label><br>
                                          <p>
                                             <span class="label">From:</span>
                                             <input type="text" class="form-input date-- rptCriteria--" name="txtAppDateFrom"
                                                placeholder="Lower Date">
                                             <?php
                                                timePicker("txtTimeFrom","");
                                             ?>

                                             &nbsp;&nbsp;&nbsp;
                                             <span class="label">To:</span>
                                             <input type="text" class="form-input date-- rptCriteria--" name="txtAppDateTo"
                                                placeholder="Higher Date">
                                             <?php
                                                timePicker("txtTimeTo","");
                                             ?>
                                             <a href="javascript:clearFields('txtAppDateFrom,txtAppDateTo,txtTimeFrom,txtTimeFrom_mode,txtTimeTo,txtTimeTo_mode');" class="clearflds--">
                                                <i class="fa fa-minus-square-o" aria-hidden="true" title="Clear Entry"></i>
                                             </a>
                                          </p>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-xs-6">
                                          <label>Filing Date:</label><br>
                                          <p>
                                             <span class="label">From:</span>
                                             <input type="text" class="form-input date-- rptCriteria--" name="txtFilingDateFrom"
                                                placeholder="Lower Date">
                                             <span class="label">To:</span>
                                             <input type="text" class="form-input date-- rptCriteria--" name="txtFilingDateTo"
                                                placeholder="Higher Date">
                                             <a href="javascript:clearFields('txtFilingDateFrom,txtFilingDateTo');" class="clearflds--">
                                                <i class="fa fa-minus-square-o" aria-hidden="true" title="Clear Entry"></i>
                                             </a>
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <?php
                                 bar();
                                 echo "<h5><label>Report Options</label></h5>";
                              ?>
                              <div class="row margin-top">
                                 <div class="col-xs-12 txt-center">
                                    <span class="label">Sort By:</span>
                                    <select class="form-input rptCriteria--" name="drpSortBy" id="drpSortBy" style="width:150px;">
                                       <option value="LastName" selected>Last Name</option>
                                       <option value="FirstName">First Name</option>
                                       <option value="BirthDate">Date of Birth</option>
                                    </select>
                                    <label style="margin-left:10px;margin-right:10px;">|</label>
                                    <input type="checkbox" name="chkRptSummary" class="showCol--">&nbsp;<span class="label">Show Report Summary</span>
                                 </div>
                              </div>
                              <?php
                                 spacer(10);
                                 bar();
                              ?>
                              <div>
                                 <div style="text-align:center;">
                                    <button type="button"
                                         class="btn-cls4-sea trnbtn"
                                         id="btnGENERATE" name="btnGENERATE">
                                       <i class="fa fa-file" aria-hidden="true"></i>
                                       &nbsp;GENERATE REPORT
                                    </button>
                                    <button type="button"
                                         class="btn-cls4-red trnbtn"
                                         id="btnEXIT" name="btnEXIT">
                                       <i class="fa fa-times" aria-hidden="true"></i>
                                       &nbsp;EXIT
                                    </button>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-1"></div>
                        </div>
                     </div>
               </div>
               <div class="panel-bottom"></div>
            </div>
            <?php
               footer();
               include "varHidden.e2e.php";
               doHidden("hRptFile","rptSummaryAttendance","");
               doHidden("hRptScrn","YES","");
            ?>
            <!-- Modal -->
            <div class="modal fade border0" id="prnModal" role="dialog">
               <div class="modal-dialog border0" style="padding:0px;width:97%;height:92%;">

                  <div class="mypanel border0" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Print Now" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php modalEmpLkUp(); ?>
         </div>
      </form>

   </body>
   <script type="text/javascript">
      $(document).ready(function () {
         EmployeeAutoComplete("employees","signatory");
         EmployeeAutoComplete("employees","prepared_by");
         EmployeeAutoComplete("employees","corrected_by");
         $("#signatory").blur(function () {
            var value = $(this).val();
            arr = value.split("-");
            $("#signatory_refid").val(arr[0]);
            $(this).val(arr[1]);
         });
         $("#prepared_by").blur(function () {
            var value = $(this).val();
            arr = value.split("-");
            $("#prepared_by_refid").val(arr[0]);
            $(this).val(arr[1]);
         });
         $("#corrected_by").blur(function () {
            var value = $(this).val();
            arr = value.split("-");
            $("#corrected_by_refid").val(arr[0]);
            $(this).val(arr[1]);
         });
      });
   </script>
</html>