<?php
   $tabTitle = ["Personal Information","Employment Classification","Attendance Reference"];
   $tabFile = ["inc_ams_PersonalInfo","inc_ams_EmpClassification","inc_ams_AttendanceRef"];
?>

<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script type="text/javascript" src="<?php echo jsCtrl("ctrl_amsEmployee"); ?>"></script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar ($paramTitle); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div class="panel-top margin-top">DETAILS</div>
                     <div class="panel-mid">
                        <div class="row margin-top">
                           <div class="col-xs-3">
                              <?php employeeSelector();?>
                           </div>
                           <div class="col-xs-9" id="RecordDetails">
                              <div class="row" id="updateRow">
                                 <div class="col-xs-12">
                                    <button type="button" class="btn-cls4-sea" id="btnUpdate" name="btnUpdate">UPDATE</button>
                                    <input type="hidden" name="fn" value="updateAMSEmployee">
                                    <input type="hidden" name="RefId" value="">
                                 </div>
                              </div>
                              <div class="row" style="margin-top:5px;">
                                 <div class="col-xs-12">
                                    <input type="hidden" name="hTabIdx" value="1">
                                    <div class="btn-group btn-group-sm">
                                       <?php
                                          $idx = 0;
                                          $active = "";
                                          for ($j=0;$j<count($tabTitle);$j++) {
                                             $idx = $j + 1;
                                             /*if ($idx == 1) $active = "elActive";
                                                       else $active = "";*/
                                             echo
                                             '<button type="button" name="btnTab_'.$idx.'" class="btn btn-default '.$active.'">'.$tabTitle[$j].'</button>';
                                          }
                                       ?>
                                    </div>
                                    <div class="row">
                                       <div class="col-xs-12">
                                          <?php
                                             spacer(10);
                                             $idx = 0;
                                             for ($j=0;$j<count($tabTitle);$j++) {
                                                $idx = $j + 1;
                                                fTabs($idx,$tabTitle[$j],$tabFile[$j]);
                                             }
                                          ?>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               doHidden("hController","ctrl_amsEmployee","");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>