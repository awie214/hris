<script>
   $(document).ready(function () {
      $(".newDataLibrary").hide();
      $(".createSelect--").css("width","100%");
   });
</script>
<div id="empbonus">
      <div class="mypanel">
            <div class="panel-top">List</div>
            <div class="panel-mid" style="max-height:200px;overflow:auto;">
               <div class="row">
                  <table border="1" width="99%" align="center">
                     <tr  class="txt-center" style="padding:5px;">
                           <td><label>Deduction Name</label></td>
                           <td><label>Amount</label></td>
                           <td><label>Date Start</label></td>
                           <td><label>Date End</label></td>
                           <td><label>Terminated</label></td>
                     </tr>
                     <?php
                     $rs = SelectEach("pms_employeesbonus","");
                     if ($rs) {
                           $recordNum = mysqli_num_rows($rs);
                           while ($row = mysqli_fetch_array($rs)) {
                     ?>
                           <tr>
                              <td><?php echo $row["FullName"]?></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                           </tr>
                     <?php 
                           }
                     }
                     ?>
                  </table>
               </div>
            </div>
            <div class="panel-bottom"></div>   
      </div>

   </div>
   <?php spacer(5);?>
      <div class="mypanel">
         <div class="panel-top">Detail</div>
         <div class="panel-mid">
            <div class="row">
               <div class="col-xs-12">
                  <input type="checkbox" class="enabler--" name="chkEnabledbonus"  for="empbonus">
                  <label id="enable">Enable Fields</label>
               </div> 
            </div>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <div class="row margin-top">
                     <div class="col-xs-2 txt-right" class="label">
                        <label>Covered Date:</label>
                     </div>
                     <div class="col-xs-2">
                        <input type="text" name="CoveredDate" class="saveFields-- form-input date--">
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-2 txt-right" class="label">
                        <label>Deduction Name:</label>
                     </div>
                     <div class="col-xs-4">
                        <input type="text" name="BIDeductionName" class="saveFields-- form-input">
                     </div>
                     <div class="col-xs-2 txt-right" class="label">
                        <label>Tax Type:</label>
                     </div>
                     <div class="col-xs-2">
                        <select class="saveFields-- form-input" name="BITaxType">
                           <option></option>
                           <option>Taxable</option>
                           <option>Non-Taxable</option>
                        </select>
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-2 txt-right" class="label">
                        <label>Amount:</label>
                     </div>
                     <div class="col-xs-2">
                        <input type="text" name="Amount" class="saveFields-- form-input">
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="panel-bottom"></div>
      </div>
   </div>
</div>