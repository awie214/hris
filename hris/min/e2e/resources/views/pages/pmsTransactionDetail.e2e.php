<?php
   session_start();
   include_once "constant.e2e.php";
   include_once pathClass.'0620functions.e2e.php';
   include_once pathClass.'SysFunctions.e2e.php';
   $EmpRefId = getvalue("EmpRefId");
   $Employees = FindFirst("employees","WHERE RefId = $EmpRefId","*");
   //print_r($Employees);
   if ($Employees) {
      //echo json_encode($Employees);
      //$Emp = json_encode($Employees);
      $EmpInfo = FindFirst("empinformation","WHERE RefId = $EmpRefId","*");
      //print_r($EmpInfo);
      //$Info = json_encode($EmpInfo);
      if ($EmpInfo) {
         $EmpInfo['PositionRefId'] = getRecord("position",$EmpInfo['PositionRefId'],"Name");
         $EmpInfo['PositionItemRefId'] = getRecord("positionitem",$EmpInfo['PositionItemRefId'],"Name");
         $EmpInfo['OfficeRefId'] = getRecord("office",$EmpInfo['OfficeRefId'],"Name");
         $EmpInfo['DepartmentRefId'] = getRecord("department",$EmpInfo['DepartmentRefId'],"Name");
         $EmpInfo['DivisionRefId'] = getRecord("division",$EmpInfo['DivisionRefId'],"Name");
         $EmpInfo['EmpStatusRefId'] = getRecord("empstatus",$EmpInfo['EmpStatusRefId'],"Name");
         $EmpInfo['SalaryGradeRefId'] = getRecord("salarygrade",$EmpInfo['SalaryGradeRefId'],"Name");
         $EmpInfo['StepIncrementRefId'] = getRecord("stepincrement",$EmpInfo['StepIncrementRefId'],"Name");
         $EmpInfo['PayRateRefId'] = getRecord("payrate",$EmpInfo['PayRateRefId'],"Name");
         echo json_encode(array_merge($Employees,$EmpInfo));
      }  else {
         echo "No Record information";
      }
   } else {
      echo "No Record";
   }
?>