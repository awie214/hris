<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script src="<?php echo jsCtrl("ctrl_pmsFileManager"); ?>"></script>
   </head>
   <body>
      <form name="xForm" method="post" id="xForm" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php
               doTitleBar ($paramTitle);
            ?>
               <div class="row">
                  <div class="col-xs-12" id="bodyContent">
                  <?php spacer(5) ?>
                        <div class="row" class="amsTabsHolder">
                           <div class="col-xs-4 bgTab padd5">
                              <div class="dropdown">
                                 <div class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;MISCELLANEOUS
                                 </div>
                                 <ul class="dropdown-menu">
                                    <li>
                                       <a href="javascript:void(0);" class="subMenu" memof="pms" pre="pms" route="FileManager" id="mBenefits">
                                       <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Benefits</a>
                                    </li>
                                    <li>
                                       <a href="javascript:void(0);" class="subMenu" memof="pms" pre="pms" route="FileManager" id="mAdjustments">
                                       <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Adjustments</a>
                                    </li>
                                    <li>
                                       <a href="javascript:void(0);" class="subMenu" memof="pms" pre="pms" route="FileManager" id="mDeductions">
                                       <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Deductions</a>
                                    </li>
                                    <li>
                                       <a href="javascript:void(0);" class="subMenu" memof="pms" pre="pms" route="FileManager" id="mLoans">
                                       <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Loans</a>
                                    </li>
                                    <hr>
                                    <li>
                                       <a href="javascript:void(0);" class="subMenu" memof="pms" pre="pms" route="FileManager" id="mResponsibility">
                                       <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Responsibility Center</a>
                                    </li>
                                    <li>
                                       <a href="javascript:void(0);" class="subMenu" memof="pms" pre="pms" route="FileManager" id="mBank">
                                       <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Bank</a>
                                    </li>
                                    <li>
                                       <a href="javascript:void(0);" class="subMenu" memof="pms" pre="pms" route="FileManager" id="mBankBranch">
                                       <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Bank Branch</a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           <div class="col-xs-4 bgTab padd5">
                              <div class="dropdown">
                                 <div class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;GOVT. TABLE AND POLICIES
                                 </div>
                                 <ul class="dropdown-menu margin-top10">
                                    <div>
                                       <label style="padding:5px;font-size:10pt;">PhilHealth Table and Policies</label>
                                    </div>
                                    <li>
                                       <a href="javascript:void(0);" class="subMenu" id="mPHILPolicy" pre="pms" route="FileManager">
                                       <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;PhilHealth Policy</a>
                                    </li>
                                    <li>
                                       <a href="javascript:void(0);" class="subMenu" id="mPHILTable" pre="pms" route="FileManager">
                                       <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;PhilHealth Table</a>
                                    </li>
                                    <?php bar();?>
                                    <div>
                                       <label style="padding:5px;font-size:10pt;">Pag-Ibig Table and Policies</label>
                                    </div>
                                    <li>
                                       <a href="javascript:void(0);" class="subMenu" id="mPAGIBIGPolicy" pre="pms" route="FileManager">
                                       <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Pag-Ibig Policy</a>
                                    </li>
                                    <li>
                                       <a href="javascript:void(0);" class="subMenu" id="mPAGIBIGTable" pre="pms" route="FileManager">
                                       <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Pag-Ibig Table</a>
                                    </li>
                                    <?php bar();?>
                                    <li>
                                       <a href="javascript:void(0);" class="subMenu" id="mGSIS" pre="pms" route="FileManager">
                                       <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;GSIS Policy</a>
                                    </li>
                                    <?php bar();?>
                                    <div>
                                       <label style="padding:5px;font-size:10pt;">Tax Table and Policies</label>
                                    </div>
                                    <li>
                                       <a href="javascript:void(0);" class="subMenu" id="mTaxTable" pre="pms" route="FileManager">
                                       <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Tax Table</a>
                                    </li>
                                    <li>
                                       <a href="javascript:void(0);" class="subMenu" id="mTaxStatus" pre="pms" route="FileManager">
                                       <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Tax Status Table</a>
                                    </li>
                                    <li>
                                       <a href="javascript:void(0);" class="subMenu" id="mAnnualTaxTable" pre="pms" route="FileManager">
                                       <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Annual Tax Table</a>
                                    </li>
                                    <li>
                                       <a href="javascript:void(0);" class="subMenu" id="mTaxPolicy" pre="pms" route="FileManager">
                                       <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Tax Policy</a>
                                    </li>
                                    <?php bar();?>
                                    <li>
                                       <a href="javascript:void(0);" class="subMenu" id="mWAGERATE" pre="pms" route="FileManager">
                                       <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Wage Rate</a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>

                        <?php
                        if (!empty($ScreenName)) {
                           if ($sql) {
                        ?>
                              <div>
                                 <div id="divList">
                                    <div class="row">
                                       <div class="col-xs-12 padd5" id="tbContent">
                                          <?php
                                          if (isset($childFile)) {
                                          ?>
                                          <div class="mypanel">
                                             <div class="panel-top"><span><?php echo strtoupper($ScreenName); ?></span> LIST</div>
                                             <div class="panel-mid">
                                                      <span id="spGridTable">
                                                         <?php
                                                           doGridTable($table,
                                                                        $gridTableHdr_arr,
                                                                        $gridTableFld_arr,
                                                                        $sql,
                                                                        $Action,
                                                                        $gridtable);
                                                         ?>
                                                      </span>
                                             </div>
                                             <div class="panel-bottom">
                                                <?php
                                                   btnINRECLO([true,true,false]);
                                                ?>
                                             </div>
                                          </div>
                                          <?php
                                          }
                                          ?>
                                       </div>
                                    </div>
                                 </div>
                                 <div id="divView">
                                    <?php
                                       if (isset($childFile)) {
                                       spacer(10);
                                    ?>
                                       <div class="mypanel">
                                          <div class="panel-top">
                                             <span id="ScreenMode">INSERTING NEW</span> <?php echo strtoupper($ScreenName); ?>
                                          </div>
                                          <div class="panel-mid-litebg">
                                             <?php require_once $childFile.".e2e.php"; ?>
                                          </div>
                                          <div class="panel-bottom">
                                             <?php btnSACABA([true,true,true]); ?>
                                          </div>
                                       </div>
                                    <?php
                                       }
                                    ?>
                                 </div>
                              </div>
                        <?php
                           } else {
                           // Tables
                           spacer(5);
                           require_once $childFile.".e2e.php";
                           }
                        }
                        ?>

                  </div>
               </div>
            <?php
               footer();
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>

   </body>
</html>