<?php
   require_once "constant.e2e.php";
   require_once "inc_model.e2e.php";
   require_once "incUtilitiesJS.e2e.php";
?>
   <div class="container" id="EntryScrn">
      <div class="row">
         <div class="col-xs-1"></div>
         <div class="col-xs-10">
            <?php if ($ScrnMode != 1) { ?>
               <div class="row">
                  <ul class="nav nav-pills">
                     <li class="active" style="font-size:12pt;font-weight:600;">
                        <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                        <?php echo $refid; ?>
                        </span></a>
                     </li>
                  </ul>
               </div>
            <?php } ?>
            <div class="row">
               <div class="col-xs-6">
                  <div class="row margin-top10">
                     <div class="form-group">
                        <label  for="inputs">CODE:</label><br>
                        <input class="form-input saveFields-- uCase-- mandatory" type="text" placeholder="Code" <?php if ($ScrnMode == 2) { echo "disabled"; } else { echo $disabled; }  ?>
                           id="inputs" name="char_Code" style='width:100%' autofocus>
                     </div>
                  </div>
                  <div class="row">
                     <div class="form-group">
                        <label  for="inputs"> NAME:</label><br>
                        <input class="form-input saveFields-- mandatory"
                               type="text" placeholder="name"
                               id="inputs" name="char_Name"
                               style='width:100%'>
                     </div>
                  </div>
                  <div class="row">

                        <div class="form-group">
                           <label> Effectivity Date:</label><br>
                           <input class="form-input saveFields-- date--"
                                  type="text" placeholder=""
                                  name="char_EffectivityDate"
                                  style='width:100%'>

                     </div>
                     <div class="col-xs-6">
                        <div class="row">
                           <div class="form-group">
                              <label  for="inputs"> Salary Bracket:</label><br>
                              <input class="form-input saveFields-- number--"
                                     type="text" placeholder=""
                                     id="inputs" name="char_SalaryBracket"
                                     style='width:100%'>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-6">
                              <div class="form-group">
                                 <label  for="inputs"> Monthly Salary Range From:</label><br>
                                 <input class="form-input saveFields-- number--"
                                        type="text" placeholder=""
                                        id="inputs" name="char_SalaryRangeFrom"
                                        style='width:100%'>
                              </div>
                           </div>
                           <div class="col-xs-6">
                              <div class="form-group">
                                 <label  for="inputs"> Monthly Salary Range To:</label><br>
                                 <input class="form-input saveFields-- number--"
                                        type="text" placeholder=""
                                        id="inputs" name="char_SalaryRangeTo"
                                        style='width:100%'>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="form-group">
                              <label  for="inputs"> Salary Base:</label><br>
                              <input class="form-input saveFields-- number--"
                                     type="text" placeholder=""
                                     id="inputs" name="char_SalaryBase"
                                     style='width:100%'>
                           </div>
                        </div>
                     </div>
                     <div class="col-xs-5" style="margin-left:10px;">
                        <div class="row">
                           <div class="form-group">
                              <label  for="inputs"> Total Monthly Contribution:</label><br>
                              <input class="form-input saveFields-- number--"
                                     type="text" placeholder=""
                                     id="inputs" name="char_MonthlyContribution"
                                     style='width:100%'>
                           </div>
                        </div>
                        <br>
                        <div class="row">
                           <div class="form-group">
                              <label  for="inputs"> Employee Share:</label><br>
                              <input class="form-input saveFields-- number--"
                                     type="text" placeholder=""
                                     id="inputs" name="char_EmployeeShare"
                                     style='width:100%'>
                           </div>
                        </div>
                        <div class="row">
                           <div class="form-group">
                              <label  for="inputs"> Employer Share:</label><br>
                              <input class="form-input saveFields-- number--"
                                     type="text" placeholder=""
                                     id="inputs" name="char_EmployerShare"
                                     style='width:100%'>
                           </div>
                        </div>


                     </div>
                  </div>
                  <div class="row">
                     <div class="form-group">
                        <label for="inputs">REMARKS:</label>
                        <textarea class="form-input saveFields--" rows="5" name="char_Remarks" <?php echo $disabled; ?>
                        placeholder="remarks"><?php echo $remarks; ?></textarea>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
         <div class="col-xs-1"></div>
   </div>