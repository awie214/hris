<?php 
   $module = module("AvailmentLeave"); 
   $m = date("m",time());
   $y = date("Y",time());
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript">
         $(document).ready(function () {
            $("#btnPRINT").click(function () {
               popPDS("AvailLeave","");
            });
            $("#h_spl").hide();
            $("#sint_LeavesRefId").change(function () {
               if ($("#sint_LeavesRefId option:selected").text().indexOf("Privilege Leave") >= 0) {
                  $("#h_spl").show();
               } else {
                  $("#h_spl").hide();
               }
            });
            $("[name='sint_LeavesRefId']").addClass("mandatory");
            <?php
               if (isset($_GET["errmsg"])) {
                  echo '$.notify("'.$_GET["errmsg"].'");';
               }
            ?>
            <?php
               echo '$("#month_").val("'.$m.'");';        
               echo '$("#year_").val("'.$y.'");';     
            ?>
         });
         function afterDelete(refid){
            alert("Record Successfully Deleted!!!");
            gotoscrn("amsAvailLeave","");
         }
         function selectMe(refid) {
            var cid = $("#hCompanyID").val();
            var rptFile = "";
            if (cid == 35) {
               rptFile = "rpt_Leave_Availment_35";
            } else {
               rptFile = "rpt_Leave_Availment";
            }
            /*
             else if (cid == 21) {
               rptFile = "rpt_Leave_Availment_21";
            }
            */
            $("#rptContent").attr("src","blank.htm");
            var url = "ReportCaller.e2e.php?file=" + rptFile;
            url += "&refid=" + refid;
            url += "&" + $("[name='hgParam']").val();
            $("#prnModal").modal();
            $("#rptContent").attr("src",url);
         }
      </script>
   </head>
   <body>
      <form name="xForm" method="post" action="postMultiple.e2e.php">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar($paramTitle); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                        <div id="divList">
                           <div class="mypanel">
                              <?php
                                 /*if (getvalue("errmsg") != "") {
                                    msg("System Info",getvalue("errmsg"));
                                 }*/
                                 //$_SESSION['ERR_MSG'] = $err;
                              ?>
                              <div class="panel-top margin-top">LIST</div>
                              <div class="panel-mid">
                                 <?php
                                    application_filter();
                                 ?>
                                 <span id="spGridTable">
                                    <?php
                                          $sizeCol = "col-sm-6";
                                          if ($UserCode == "COMPEMP") {
                                             $sizeCol = "col-sm-12";
                                             $gridTableHdr_arr = ["File Date", "Leave", "App. Date From", "App. Date To", "Force", "Status"];
                                             $gridTableFld_arr = ["FiledDate", "LeavesRefId", "ApplicationDateFrom", "ApplicationDateTo", "isForceLeave", "Status"];
                                             $sql = "SELECT * FROM $table WHERE CompanyRefId = $CompanyId AND BranchRefId = $BranchId AND EmployeesRefId = $EmployeesRefId ORDER BY FiledDate";
                                             $Action = [false,false,false,false];
                                          } else {
                                             $sql = "SELECT * FROM $table WHERE Month(FiledDate) = '$m' AND Year(FiledDate) = '$y' ORDER BY FiledDate";   
                                          }
                                          
                                          doGridTable($table,
                                                      $gridTableHdr_arr,
                                                      $gridTableFld_arr,
                                                      $sql,
                                                      $Action,
                                                      "gridTable");
                                    ?>
                                 </span>
                                 <?php
                                       btnINRECLO([true,true,false]);
                                       // echo '
                                       //    <button type="button"
                                       //         class="btn-cls4-lemon trnbtn"
                                       //         id="btnPRINT" name="btnPRINT">
                                       //       <i class="fa fa-print" aria-hidden="true"></i>&nbsp;
                                       //       PRINT
                                       //    </button>
                                       // ';
                                 ?>
                              </div>
                              <div class="panel-bottom"></div>
                           </div>
                        </div>
                        <div id="divView">
                           <div class="row">
                              <div class="col-xs-6">
                                 <div class="mypanel">
                                    <div class="panel-top">
                                       <span id="ScreenMode">AVAILING</span> <?php echo strtoupper($ScreenName); ?>
                                    </div>
                                    <div class="panel-mid">
                                       <div id="EntryScrn">
                                          <div class="row margin-top">
                                             <div class="<?php echo $sizeCol; ?>">
                                                   <div class="row" id="badgeRefId" style="display: none;">
                                                      <div class="col-xs-6">
                                                         <ul class="nav nav-pills">
                                                            <li class="active" style="font-size:12pt;font-weight:600;">
                                                               <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                                                               </span></a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                   <?php
                                                      $attr = ["br"=>true,
                                                               "type"=>"text",
                                                               "row"=>true,
                                                               "name"=>"date_FiledDate",
                                                               "col"=>"4",
                                                               "id"=>"FiledDate",
                                                               "label"=>"Date File",
                                                               "class"=>"saveFields-- mandatory date--",
                                                               "style"=>"",
                                                               "other"=>""];
                                                      $form->eform($attr);

                                                      echo
                                                      '<div class="row margin-top">
                                                      <div class="col-xs-12">
                                                      <label>Leave</label><br>';

                                                      createSelect("leaves",
                                                                   "sint_LeavesRefId",
                                                                   "",100,
                                                                   "Name",
                                                                   "Select Leave","required");

                                                      echo
                                                      '</div></div>';
                                                      echo '
                                                         <div class="row margin-top">
                                                            <div class="col-xs-12">
                                                               <select name="char_SPLType" id="h_spl" class="form-input saveFields--">
                                                                  <option value="PM">Personal Milestone</option>
                                                                  <option value="PO">Parental Obligation</option>
                                                                  <option value="Fil">Fillial</option>
                                                                  <option value="DomE">Domestic Emergencies</option>
                                                                  <option value="PTrn">Personal Transactions</option>
                                                                  <option value="C">Calamity</option>
                                                               </select>
                                                            </div>
                                                         </div>
                                                      ';
                                                      echo
                                                      '<div class="row margin-top">
                                                         <div class="col-xs-12">
                                                            <label>Application Date</label><br>
                                                            <div class="row">';
                                                               $attr = ["br"=>true,
                                                                        "type"=>"text",
                                                                        "row"=>false,
                                                                        "name"=>"date_ApplicationDateFrom",
                                                                        "col"=>"6",
                                                                        "id"=>"ApplicationDateFrom",
                                                                        "label"=>"From",
                                                                        "class"=>"saveFields-- mandatory date--",
                                                                        "style"=>"",
                                                                        "other"=>"required"];
                                                               $form->eform($attr);
                                                               $attr = ["br"=>true,
                                                                        "type"=>"text",
                                                                        "row"=>false,
                                                                        "name"=>"date_ApplicationDateTo",
                                                                        "col"=>"6",
                                                                        "id"=>"ApplicationDateTo",
                                                                        "label"=>"To",
                                                                        "class"=>"saveFields-- mandatory date--",
                                                                        "style"=>"",
                                                                        "other"=>"required"];
                                                               $form->eform($attr);
                                                      echo
                                                      '
                                                            </div>
                                                         </div>
                                                      </div>';
                                                   ?>
                                                   <div class="row margin-top" id="vl_detail">
                                                      <div class="col-xs-12">
                                                         <label>If Abroad (Specify):</label>
                                                         <input type="text" class="form-input saveFields--" name="char_VLPlace" id="char_VLPlace" placeholder="City, Country">
                                                      </div>
                                                   </div>
                                                   <?php
                                                      if (getvalue("hCompanyID") != "1000") {
                                                   ?>
                                                   <!-- <div class="margin-top">
                                                      <input id="fleave" type="checkbox" name="sint_isForceLeave" value="0" class="saveFields--">
                                                      <label for="fleave" >Force Leave</label>
                                                   </div> -->
                                                   <div class="row">
                                                      <div class="col-xs-12" id="is_fl">
                                                         <label>IS THIS FORCED LEAVE?</label>
                                                         <select name="sint_isForceLeave" id="sint_isForceLeave" class="form-input saveFields--">
                                                            <option value="">NO</option>
                                                            <option value="1">YES</option>
                                                         </select>
                                                      </div>
                                                   </div>
                                                   <?php
                                                      }
                                                   ?>
                                                   <!--<input type="hidden" name="sint_isForceLeave" value="0">-->
                                                   <!-- <div class="margin-top">
                                                      <input id="incSATSUN" type="checkbox" name="sint_IncludeSatSun" value="0" class="saveFields--">
                                                      <label for="incSATSUN">Include Saturday And Sunday</label>
                                                   </div> -->
                                                   <!--<input type="hidden" name="sint_IncludeSunSat" value="0">-->
                                                   <!--
                                                   <div class="margin-top">
                                                      <label>Balance:</label>
                                                      <input type="text" class="form-input number-- saveFields--" name="deci_Balance">
                                                   </div>
                                                   -->
                                                   <div class="row margin-top">
                                                      <div class="col-xs-12">
                                                         <div class="form-group">
                                                            <label class="control-label" for="inputs">Remarks:</label>
                                                            <textarea class="form-input saveFields--" rows="5" name="char_Remarks" placeholder="remarks"></textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                             </div>
                                             <div class="col-xs-6 padd5 adminUse--">
                                                <label>Employees Selected</label>
                                                <select multiple id="EmpSelected" name="EmpSelected" class="form-input saveFields--" style="height:250px;">
                                                </select>
                                                <p>Double Click the items to remove in the List</p>
                                                <input type="hidden" class="saveFields--" name="hEmpSelected">
                                             </div>
                                          </div>
                                       </div>
                                       <?php
                                          spacer(10);
                                          echo
                                          '<button type="submit" class="btn-cls4-sea"
                                                   name="btnLocSAVE" id="LocSAVE">
                                             <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                             &nbsp;Save
                                          </button>';
                                          btnSACABA([false,true,true]);
                                       ?>
                                    </div>
                                    <div class="panel-bottom"></div>
                                 </div>
                              </div>

                              <div class="col-xs-6 adminUse--">
                                 <div class="mypanel">
                                    <div class="panel-top">Employees List</div>
                                    <div class="panel-mid">
                                       <?php include_once "incEmpListSearchCriteria.e2e.php" ?>
                                       <!-- <a href="javascript:void(0);" title="Search Employees">
                                          <i class="fa fa-search" aria-hidden="true"></i>
                                       </a>-->
                                       <div id="empList" style="max-height:300px;overflow:auto;">
                                          <h4>No Employees Selected</h4>
                                       </div>
                                    </div>
                                    <div class="panel-bottom">
                                       <a href="javascript:void(0);" id="clearEmpSelected">Clear Employees Selected</a>&nbsp;
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varJSON.e2e.php";
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>