<?php
   $rowcount = false;
   $offer1 = $offer2 = $offer3 = $offer4 = $offer5 = 0;
   require_once "constant.e2e.php";
   require_once "inc_model.e2e.php";
   require_once "incUtilitiesJS.e2e.php";
   if ($rowcount) {
      $offer1 = $row["Offer1"];
      $offer2 = $row["Offer2"];
      $offer3 = $row["Offer3"];
      $offer4 = $row["Offer4"];
      $offer5 = $row["Offer5"];
   }
?>
   <div class="container" id="EntryScrn">
      <div class="row">
         <div class="col-sm-12">
            <?php if ($ScrnMode != 1) { ?>
               <div class="row">
                  <ul class="nav nav-pills">
                     <li class="active" style="font-size:12pt;font-weight:600;">
                        <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                        <?php echo $refid; ?>
                        </span></a>
                     </li>
                  </ul>
               </div>
            <?php

               }
            ?>
            <div class="row margin-top10">
               <div class="form-group">
                  <label class="control-label" for="inputs">CODE:</label><br>
                  <input class="form-input saveFields-- uCase--" type="text" placeholder="Code" <?php if ($ScrnMode == 2) { echo "disabled"; } else { echo $disabled; }  ?>
                     id="inputs" name="char_Code" style='width:75%' value="" autofocus>
               </div>
            </div>
            <div class="row">
               <div class="form-group">
                  <label class="control-label" for="inputs">NAME:</label><br>
                  <input class="form-input saveFields-- uCase-- mandatory alpha--" type="text" placeholder="name" <?php echo $disabled; ?>
                     id="inputs" name="char_Name" style='width:75%' value="">
               </div>
            </div>

            <div class="row">
               <div class="col-sm-12">
                  <div class="row">
                     <div class="col-sm-4">
                        <input type="checkbox" id="offer1" name="sint_Offer1" value="<?php echo $offer1; ?>"
                        <?php
                           echo $offer1;
                           if ($offer1 == 1) {
                              echo " checked";
                           }
                        ?>
                        class="saveFields--">&nbsp;&nbsp;
                        <label for="offer1">ELEMENTARY</label>
                     </div>
                     <div class="col-sm-4">
                        <input type="checkbox" id="offer2" name="sint_Offer2" value="<?php echo $offer2; ?>"
                        <?php
                           echo $offer2;
                           if ($offer2 == 1) {
                              echo " checked";
                           }
                        ?>
                        class="saveFields--">&nbsp;&nbsp;
                        <label for="offer2">SECONDARY</label>
                     </div>
                     <div class="col-sm-4">
                        <input type="checkbox" id="offer4" name="sint_Offer4" value="<?php echo $offer4; ?>"
                        <?php
                           if ($offer4 == 1) {
                              echo " checked";
                           }
                        ?>
                        class="saveFields--">&nbsp;&nbsp;
                        <label for="offer4">COLLEGE</label>
                     </div>
                  </div>

                  <div class="row">
                     <div class="col-sm-4">
                        <input type="checkbox" id="offer3" name="sint_Offer3" value="<?php echo $offer3; ?>"
                        <?php
                           if ($offer3 == 1) {
                              echo " checked";
                           }
                        ?>
                        class="saveFields--">&nbsp;&nbsp;
                        <label for="offer3">VOCATIONAL</label>
                     </div>
                     <div class="col-sm-4">
                        <input type="checkbox" id="offer5" name="sint_Offer5" value="<?php echo $offer5; ?>"
                        <?php
                           if ($offer5 == 1) {
                              echo " checked";
                           }
                        ?>
                        class="saveFields--">&nbsp;&nbsp;
                        <label for="offer5">GRADUATE STUDIES</label>
                     </div>
                  </div>

               </div>
            </div>

            <div class="row">
               <div class="form-group">
                  <label class="control-label" for="inputs">REMARKS:</label>
                  <textarea class="form-input saveFields--" rows="5" name="char_Remarks" <?php echo $disabled; ?>
                  placeholder="remarks"></textarea>
               </div>
            </div>
         </div>
      </div>
   </div>
<script type="text/javascript">
   $(document).ready(function () {
      $("#btnSAVE, #btnFM_SAVE").prop("disabled",true);
      $("[name*='sint_Offer']").each(function () {
         $(this).click(function () {
            if ($("[name*='sint_Offer']:checkbox:checked").length == 0) {
               $("#btnSAVE, #btnFM_SAVE").prop("disabled",true);
            } else {
               $("#btnSAVE, #btnFM_SAVE").prop("disabled",false);
            }
         });
      });

      <?php
         if (getvalue("levelType") > 0) {
            echo '
            $("[name*=\'sint_Offer\']").prop("disabled",true);
            $("[name=\'sint_Offer'.getvalue("levelType").'\']").prop("disabled",false);
            $("[name=\'sint_Offer'.getvalue("levelType").'\']").prop("checked",true);
            $("[name=\'sint_Offer'.getvalue("levelType").'\']").val(1);
            $("#btnFM_SAVE").prop("disabled",false);';

         }
      ?>




   });
</script>