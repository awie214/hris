<?php
   //session_start();
   include 'colors.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = 0;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   include 'incRptSortBy.e2e.php';
   $rs = SelectEach($table,$whereClause);
   if ($rs) $rowcount = mysqli_num_rows($rs);
   //if ($dbg) { echo "DBG >> ".$whereClause; }


   function doColumn($row,$s,$show) {
      echo '<tr>';
         if ($s == 0) {
            echo '<th nowrap>Employees Name</th>';
         } else {
            echo '<td nowrap style="padding-left:5px;">';
            if ($show) {
               echo $row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"].'</td>';
            } else {
               echo "&nbsp;</td>";
            }
         }
         if (getvalue ("chkChildFullName") == 'true') {
            if ($s==0) {
               echo '<th nowrap>Child Full Name</th>';
            } else {
               echo '<td nowrap style="padding-left:10px;">'.$GLOBALS["ChildFullName"].'</td>';
            }
         }
         if (getvalue ("chkChildBDate") == 'true') {
            if ($s==0) {
               echo '<th nowrap>Child Birth Date</th>';
            } else {
               echo '<td nowrap class="center--">'.$GLOBALS["ChildBirthDate"].'</td>';
            }
         }
         if (getvalue ("chkChildGender") == 'true') {
            if ($s==0) {
               echo '<th nowrap>Child Gender</th>';
            } else {
               echo '<td nowrap class="center--">'.$GLOBALS["ChildGender"].'</td>';
            }
         }

      echo '</tr>';
   }

   /*Start Here Date Validation --*/
   $errmsg = "";

   /*End Here - Date Validation*/

   $recordsCount = 0;
?>

<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader("Employees Child List");
         ?>
         <table border="1">
            <tr>
            <?php
               doColumn("",0,true);
            ?>
            </tr>
            <?php
               $empRefid = "";
               while ($row = mysqli_fetch_assoc($rs))
               {
                  $childCount = 0;
                  $empRefid = $row['RefId'];

                  $sql = "SELECT * FROM employeeschild where EmployeesRefId = $empRefid ORDER BY BirthDate";

                  $resultLChild = mysqli_query($conn,$sql) or die(mysqli_error($conn));
                  $numrow = mysqli_num_rows($resultLChild);
                  $ChildFullName = "";
                  $ChildBirthDate = "";
                  $ChildGender = "";
                  if ($numrow) {

                     while ($row_Child = mysqli_fetch_assoc($resultLChild)) {
                        $childCount++;
                        $ChildFullName = $row_Child["FullName"];
                        $ChildBirthDate = $row_Child["BirthDate"];
                        $ChildGender = $row_Child["Gender"];
                        if ($childCount == 1) {
                           doColumn($row,1,true);
                        } else {
                           doColumn($row,1,false);
                        }
                     }
                     $recordsCount++;
                  }
               }
               echo '</table>';
               echo "RECORD COUNT : ".$recordsCount;
            ?>
         <?php
            echo
            '<div>SEARCH CRITERIA:</div>';
            if ($searchCriteria == "") echo "<li>ALL RECORDS</li>";
            rptFooter();
         ?>
      </div>
   </body>
</html>