<?php
	include_once "constant.e2e.php";
   include_once pathClass.'0620functions.e2e.php';
   include_once pathClass.'SysFunctions.e2e.php';
   include_once pathClass.'0620TrnData.e2e.php';

   $file 					= $_FILES["char_File"]["name"];
   $EmployeesRefId 		= getvalue("sint_EmployeesRefId");
   $PositionRefId 		= getvalue("sint_PositionRefId");
   $DepartmentRefId 		= getvalue("sint_DepartmentRefId");
   $DivisionRefId 		= getvalue("sint_DivisionRefId");
   $PCRType 				= getvalue("char_PCRType");
   $Semester 				= getvalue("char_Semester");
   $Year 			 		= getvalue("date_YearPerformed");
   $OverallScore 			= getvalue("bint_OverallScore");
   $NumericalRating 		= getvalue("bint_NumericalRating");
   $Adjectival 			= getvalue("char_Adjectival");
   $table 					= "spms_pcr";
   $cid 						= getvalue("hCompanyID");
   $FileType 				= strtolower(pathinfo($file,PATHINFO_EXTENSION));
   $path 					= path."images/".$cid."/SPMS/".$EmployeesRefId;
   $newName 				= $PCRType."_".$file;
   $Flds = "`EmployeesRefId`, `PositionRefId`, `DivisionRefId`, `DepartmentRefId`, `PCRType`, `Semester`, `Year`,";
   $Flds .= " `Adjectival`, `OverallScore`, `NumericalRating`, `File`,";
   $Vals = "'$EmployeesRefId', '$PositionRefId', '$DivisionRefId', '$DepartmentRefId', '$PCRType', '$Semester', '$Year',";
   $Vals .= " '$Adjectival', '$OverallScore', '$NumericalRating', '$newName',";
   
   if (isset($_POST["submit"])) {
   	if($FileType != "pdf") {
		   header('Location: ' . $_SERVER['HTTP_REFERER']."&err=Invalid File Type.");
		} else {
			if (!file_exists($path)) {
			   mkdir($path, 0777);
			}
			if (!file_exists($path."/".$newName)) {
				if (move_uploaded_file($_FILES["char_File"]["tmp_name"], $path."/".$newName)) {
			      $save = f_SaveRecord("NEWSAVE",$table,$Flds,$Vals);
		         if (is_numeric($save)) {
		            header('Location: ' . $_SERVER['HTTP_REFERER']."&err=Successfully Saved.");
		         } else {
		            header('Location: ' . $_SERVER['HTTP_REFERER']."&err=Error in Saving.");
		         }
			   } else {
			      header('Location: ' . $_SERVER['HTTP_REFERER']."&err=Sorry, there was an error uploading your file.");
			   }	
			} else {
				header('Location: ' . $_SERVER['HTTP_REFERER']."&err=File Already Exist.");
			}
		}
   }
?>