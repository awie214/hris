<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>

   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post">
         <?php $sys->SysHdr($sys,"ldms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("Competency"); ?>
            <div class="container-fluid margin-top">
               <div class="mypanel">
                  <div class="row">
                     <div class="col-xs-12" id="div_CONTENT">
                        <div class="margin-top">
                           <div id="divList">
                              <span id="spGridTable">
                                 <?php
                                       $table = "competency";
                                       $gridTableHdr_arr = ["Name","Type"];
                                       $gridTableFld_arr = ["Name","Type"];
                                       $sql = "SELECT * FROM competency";
                                       $Action = [true,true,true,false];
                                       doGridTable($table,
                                                   $gridTableHdr_arr,
                                                   $gridTableFld_arr,
                                                   $sql,
                                                   $Action,
                                                   $_SESSION["module_gridTable_ID"]);
                                 ?>
                              </span>
                              <?php
                                 btnINRECLO([true,false,false]);
                              ?>
                           </div>
                           <div id="divView">
                              <div class="mypanel panel panel-default">
                                 <div class="panel-top">
                                    <span id="ScreenMode">INSERTING NEW COMPETENCY
                                 </div>
                                 <div class="panel-mid-litebg" id="EntryScrn">
                                    <div id="EntryScrn">
                                       <div class="row">
                                          <div class="col-xs-6">
                                             <div class="row">
                                                <div class="col-xs-12">
                                                   <label class="control-label" for="inputs">COMPETENCY TYPE:</label>
                                                   <select class="form-input saveFields-- mandatory" name="char_Type" id="char_Type">
                                                      <option value="">Select Type</option>
                                                      <option value="Foundation">Foundation</option>
                                                      <option value="Leadership">Leadership</option>
                                                      <option value="Organizational">Organizational</option>
                                                      <option value="Technical">Technical</option>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="row margin-top">
                                                <div class="col-xs-12">
                                                   <label class="control-label" for="inputs">OPERATIONAL DEFINITION:</label>
                                                   <textarea class="form-input saveFields--" name="char_Definition" id="char_Definition" rows="3"></textarea>
                                                </div>
                                             </div>
                                             <div class="row margin-top">
                                                <div class="col-xs-12">
                                                   <label class="control-label" for="inputs">COMPETENCY NAME:</label>
                                                   <input class="form-input saveFields-- mandatory" 
                                                          type="text"
                                                          name="char_Name"
                                                          id="char_Name">
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <br><br>
                                       <div class="row margin-top">
                                          <div class="col-xs-3">
                                             <div class="row">
                                                <div class="col-xs-12 text-center">
                                                   <label>LEVEL 1</label>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-xs-3">
                                             <div class="row">
                                                <div class="col-xs-12 text-center">
                                                   <label>LEVEL 2</label>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-xs-3">
                                             <div class="row">
                                                <div class="col-xs-12 text-center">
                                                   <label>LEVEL 3</label>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-xs-3">
                                             <div class="row">
                                                <div class="col-xs-12 text-center">
                                                   <label>LEVEL 4</label>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <?php
                                          for ($i=1; $i <= 10; $i++) { 
                                       ?>
                                       <div class="row margin-top">
                                          <div class="col-xs-3">
                                             <div class="row">
                                                <div class="col-xs-12 text-center">
                                                   <textarea class="form-input saveFields--" name="char_LevelOne<?php echo $i; ?>" id="char_LevelOne<?php echo $i; ?>" rows="3" style="resize: none;"></textarea>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-xs-3">
                                             <div class="row">
                                                <div class="col-xs-12 text-center">
                                                   <textarea class="form-input saveFields--" name="char_LevelTwo<?php echo $i; ?>" id="char_LevelTwo<?php echo $i; ?>" rows="3" style="resize: none;"></textarea>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-xs-3">
                                             <div class="row">
                                                <div class="col-xs-12 text-center">
                                                   <textarea class="form-input saveFields--" name="char_LevelThree<?php echo $i; ?>" id="char_LevelThree<?php echo $i; ?>" rows="3" style="resize: none;"></textarea>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-xs-3">
                                             <div class="row">
                                                <div class="col-xs-12 text-center">
                                                   <textarea class="form-input saveFields--" name="char_LevelFour<?php echo $i; ?>" id="char_LevelFour<?php echo $i; ?>" rows="3" style="resize: none;"></textarea>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <?php
                                          }
                                       ?>
                                    </div>
                                 </div>
                                 <div class="panel-bottom">
                                    <?php btnSACABA([true,true,true]); ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               $table = "competency";
               doHidden("paramTitle",getvalue("paramTitle"),"");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
   <script language="JavaScript">
      $(document).ready(function () {
         remIconDL();
      });
      function afterNewSave() {
         alert("Successfully Saved");
         gotoscrn("ldms_competency","");
      }
      function afterEditSave() {
         alert("Successfully Updated");
         gotoscrn("ldms_competency","");   
      }
   </script>
</html>



