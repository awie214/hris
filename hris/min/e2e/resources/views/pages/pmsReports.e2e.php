<?php
   include_once "constant.e2e.php";
   include_once pathClass.'0620functions.e2e.php';
   include_once pathClass.'SysFunctions.e2e.php';
   $sys = new SysFunctions();
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <script type="text/javascript">
         $(document).ready(function () {
            $("#checkAll").change(function(){
               if ($(this).is(':checked')) {
                  $(".showCol--").prop("checked",true);
               } else {
                  $(".showCol--").prop("checked",false);
               }
               $("[name='chkEmpName']").prop("checked",true);
            });
         });
      </script>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pms"); ?>
         <div class="container-fluid" id="mainScreen">
               <?php doTitleBar("REPORTS"); ?>
               <div class="margin-top" id="rptCriteria">
                  <div class="row">
                     <div class="col-sm-1"></div>
                     <div class="col-sm-10">
                        <?php spacer(10); ?>
                        <p>
                           <label>Choose Kind Of Report:</label>
                           <select class="form-input rptCriteria--" id="ReportKind" name="drpReportKind" style="width:50%;font-family:12pt;">
                              <?php
                                $crit = "WHERE `CompanyRefId` = $CompanyId ";
                                $crit .= "AND `BranchRefId` = $BranchId ";
                                $crit .= "AND `CompanyCode` = '$CompanyCode' ";
                                $crit .= "AND `SystemRefId` = '3'";
                                $rs = SelectEach("reports",$crit);
                                $htm = "yes";
                                if ($rs) {
                                   while ($r = mysqli_fetch_array($rs)) {
                                      echo
                                      '<option for="" ext="'.$r["ExtType"].'" own="'.$r["Owner"].'" value="'.$r["Filename"].'" rptname="'.$r["Name"].'">['.$r["Code"].'] - '.$r["Name"].'</option>'."\n";
                                   }
                                }
                              ?>

                              <option for="" ext="htm" own="E2E" value="rptPAYSLIP1">PIDS PaySlip</option>
                              <option for="" ext="htm" own="E2E" value="rptINITIALPAY">Initital Pay</option>
                              <option for="" ext="htm" own="E2E" value="rptTRANSMITAL">Transmital</option>
                              <option for="" ext="htm" own="E2E" value="rptOTPAY">Overtime Pay</option>
                              <option for="" ext="htm" own="E2E" value="rptPROMOTIONS">Promotions</option>
                              <option for="" ext="htm" own="E2E" value="rptPROVIDENT">Provident</option>
                              <option for="" ext="htm" own="E2E" value="rptPIDS">PIDS</option>
                              <option for="" ext="htm" own="E2E" value="rptSTEPINC">Step Increment</option>
                              <option for="" ext="htm" own="E2E" value="rptADJUSTMENT">Adjustment</option>
                              <option for="" ext="htm" own="E2E" value="rptLEAVE">Leave</option>
                              <option for="" ext="htm" own="E2E" value="rptRATA">RATA</option>

                              <option value="XX" disabled class="optHeader">Loan Reports</option>
                              <option for="LoanReports" ext="" value="rptLoanSummary">Loan Report Summary</option>

                              <option value="XX" disabled class="optHeader">Loan Report Detail</option>
                              <option for="LoanReportsDtl" own="E2E" ext="" value="rptLoanReportDetail">Loan Report Detail - Console Loan</option>
                              <option for="LoanReportsDtl" own="E2E" ext="" value="rptLoanReportDetail">Loan Report Detail - Policy Loan</option>
                              <option for="LoanReportsDtl" own="E2E" ext="" value="rptLoanReportDetail">Loan Report Detail - Pagibig Loan</option>
                              <option for="LoanReportsDtl" own="E2E" ext="" value="rptLoanReportDetail">Loan Report Detail - Emergency Loan</option>
                              <option for="LoanReportsDtl" own="E2E" ext="" value="rptLoanReportDetail">Loan Report Detail - Landbank Loan</option>
                              <option for="LoanReportsDtl" own="E2E" ext="" value="rptLoanBalance">Loan Report Balance</option>

                              <option value="XX" disabled class="optHeader">Payroll Register</option>
                              <option for="PayrollRegister" own="E2E" ext="" value="rptPayrollRegister">Payroll Register</option>

                              <option value="XX" disabled class="optHeader">Payslip</option>
                              <option for="PaySlip" own="E2E" ext="" value="rptPayslip">Payslip</option>

                              <option value="XX" disabled class="optHeader">Government Reports</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rptGSIS_RemittanceExcel">GSIS-Remittance Excel</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rptGSIS_LoanSummary">GSIS-Loan Summary</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rptGSIS_PremiumCertificate">GSIS-Premium Certificate</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rptGSIS_LoanCertificate">GSIS-Loan Certificate</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rptGSIS_ConsolidatedPremiumRemittances">GSIS-Consolidated Premium Remittances</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rptGSIS_ConsolidatedLoanRemittances">GSIS-Consolidated Loan Remittances</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rptPHILHEALTH_ContributionList">PHILHEALTH-Contribution List</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rptPHILHEALTH_Certificate">PHILHEALTH-Certificate</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rptPHILHEALTH_MonthlyDiskette">PHILHEALTH-Monthly Diskette</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rptPHILHEALTH_QuarterlyDiskette">PHILHEALTH-Quarterly Diskette</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rptPHILHEALTH_EmployersMonthlyReport">PHILHEALTH-RF-1 Employer's Monthly Report</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rptPHILHEALTH_EmployersQuarterlyReport">PHILHEALTH-RF-1 Employer's Quarterly Report</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rptPAGIBIG_ContributionList">PAGIBIG-Contribution List</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rptPAGIBIG_Diskette">PAGIBIG-Diskette</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rptPAGIBIG_Certificate">PAGIBIG-Certificate</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rptPAGIBIG_LoanCollectionList">PAGIBIG-Loan Collection List</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rptPAGIBIG_LoanDiskette">PAGIBIG-Loan Diskette</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rptPAGIBIG_LoanCertificate">PAGIBIG-Loan Certificate</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rptPAGIBIG_MembershipRemittanceForm">PAGIBIG-M1-1 Membership Remittance Form</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rptPAGIBIG_MonthlyRemittanceSchedule">PAGIBIG-P2-4 Monthly Remittance Schedule (Loan)</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rptTax">TAX</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rpt">ALPHALIST-(7.1) Employees Terminated before Dec.31</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rpt">ALPHALIST-(7.2) Employees whose Income Exempt from w/ Tax</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rpt">ALPHALIST-(7.3) Employees w/ No Previous Employers</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rpt">ALPHALIST-(7.4) Employees w/ Previous Employers</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rpt">ALPHALIST-(7.5) Minimum Wage Earning Employees</option>

                              <option value="XX" disabled class="optHeader">Bank Report</option>
                              <option for="BankRpt" own="E2E" ext="" value="rptBank">Bank Report</option>

                              <option value="XX" disabled class="optHeader">Allowances</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rptEmp_Rata">Employees Rata</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rptEME">EME</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rptEmp_COMMExpAllot">Communication Expenses Allotment</option>

                              <option for="GovtRpt" own="E2E" ext="" value="rpt_COE-COMPEN-PLANTILLA">Certificate of Employment and Compensation</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rpt_COE-JOBBERS-COMPEN">Certificate of Employment and Jobbers Compensation</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rpt_HalfYearEndAndCashGifts">PAYROLL- 1/2 YEAR-END BONUS AND 1/2 CASH GIFT</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rpt_YearEndAndCashGifts">PAYROLL TRANSFER - YEAR-END BONUS AND 1/2 CASH GIFT</option>

                              <option for="GovtRpt" own="E2E" ext="" value="rpt_payrollTransfer">Payroll Transfer</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rpt_InitialSalary">Initial Salary</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rpt_LastPaymentofSalary">Last Payment of Salary</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rpt_ClothingAllowance">PAYROLL SHEET FOR CLOTHING ALLOWANCE</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rpt_Monetization">REGULAR / 50% MONETIZATION OF EARNED VACATION/SICK LEAVE CREDITS</option>
                              <option for="GovtRpt" own="E2E" ext="" value="rpt_PEI">PERFORMANCE ENHANCEMENT INCENTIVE (PEI)</option>
                              <option for="GovtRpt" own="Agency" ext="" value="rpt_Payslip">PCC Payslip</option>
                           </select>
                        </p>

                        <?php
                           bar();
                           echo "<h5><label>Search Criteria</label></h5>";
                           require_once "incUtilitiesJS.e2e.php";
                           require_once "incEmpSearchCriteria.e2e.php";
                           bar();
                           echo "<h5><label>Date's Criteria</label></h5>";
                        ?>
                        <?php
                        bar();
                           echo "<h5><label>Report Options</label></h5>";
                        ?>
                        <div class="row margin-top">
                           <div class="col-xs-12 txt-center">
                              <span class="label">Sort By:</span>
                              <select class="form-input rptCriteria--" name="drpSortBy" id="drpSortBy" style="width:150px;">
                                 <option value="LastName" selected>Last Name</option>
                                 <option value="FirstName">First Name</option>
                                 <option value="BirthDate">Date of Birth</option>
                              </select>
                              <label style="margin-left:10px;margin-right:10px;">|</label>
                              <input type="checkbox" name="chkRptSummary" class="showCol--">&nbsp;<span class="label">Show Report Summary</span>
                           </div>
                        </div>
                        <?php
                        bar();
                           echo "<h5><label>Show Column</label></h5>";
                        ?>
                        <div class="row margin-top">
                           <div class="col-xs-12">
                              <div class="row">
                                 <div class="col-xs-3">
                                    <input type="checkbox" id="checkAll"/>&nbsp;<span class="label">Check All</span>
                                    <?php bar();?>
                                 </div>
                              </div>
                              <?php
                                 $ObjList =
                                 [
                                    [
                                      "chkEmpRefId|EmpRefId|checked|Employees Ref. Id",
                                      "chkEmpName|EmpName|checked disabled|Employees Name",
                                      "chkBirth|Birth|checked|BirthDate",
                                      "chkSex|Sex|checked|Sex"
                                    ],
                                    [
                                      "chkCStatus|CStatus|checked|Civil Status",
                                      "chkCitizenship|Citizenship||Citizenship",
                                      "chkPBirth|PBirth||Birth Place",
                                      "chkBlood|Blood||Blood Type"
                                    ],
                                    [
                                      "chkHeight|Height||Height(m)",
                                      "chkWeight|Weight||Weight(lbs)",
                                      "chkGSIS|GSIS||GSIS No.",
                                      "chkPAGIBIG|PAGIBIG||PAGIBIG No."
                                    ],
                                    [
                                      "chkResAddress|ResAddress||Residential Address",
                                      "chkResTelNo|ResTelNo||Residential Tel. No.",
                                      "chkPermAddress|PermAddress||Permanent Address",
                                      "chkPermTelNo|PermTelNo||Permanent Tel. No."
                                    ],
                                    [
                                      "chkEmail|Email||Email Address",
                                      "chkMobileNo|MobileNo|checked|Mobile No.",
                                      "chkAgencyEmpNo|AgencyEmpNo|checked|Agency Emp. No.",
                                      "chkTinNo|TinNo||TIN No."
                                    ],
                                    [
                                      "chkPHILHEALTHNo|PHILHEALTHNo||PHILHEALTH No.",
                                      "chkTrainStartDate|TrainStartDate||Training Start Date",
                                      "chkTrainEndDate|TrainEndDate||Training End Date",
                                      "chkEducLvl|LevelType||Education Level"
                                    ],
                                    [
                                      "chkStrWorkDate|StrWorkDate||Work Start Date",
                                      "chkEndWorkDate|StrEndDate||Work End Date",
                                    ]
                                 ];

                                 for ($j=0;$j<count($ObjList);$j++) {
                                    $RowElement = $ObjList[$j];
                                    echo
                                    '<div class="row margin-top">';
                                    for ($d=0;$d<count($RowElement);$d++) {
                                       $obj = explode("|",$RowElement[$d]);

                                       if ($obj[0] != "") {
                                          echo
                                          '<div class="col-xs-3 colopt">
                                             <input type="checkbox" id="'.$obj[1].'" name="'.$obj[0].'" class="showCol--" '.$obj[2].'>
                                             <label class="label" for="'.$obj[1].'">'.$obj[3].'</label>
                                          </div>';
                                       }

                                    }
                                    echo
                                    '</div>'."\n";
                                 }
                              ?>
                           </div>
                        </div>
                        <?php
                           spacer(10);
                           bar();
                        ?>
                        <div>
                           <div style="text-align:center;">
                              <button type="button"
                                   class="btn-cls4-sea trnbtn"
                                   id="btnGENERATE" name="btnGENERATE">
                                 <i class="fa fa-file" aria-hidden="true"></i>
                                 &nbsp;GENERATE REPORT
                              </button>
                              <button type="button"
                                   class="btn-cls4-red trnbtn"
                                   id="btnEXIT" name="btnEXIT">
                                 <i class="fa fa-times" aria-hidden="true"></i>
                                 &nbsp;EXIT
                              </button>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-1"></div>
                  </div>
               </div>
               <?php
                  footer();
                  include "varHidden.e2e.php";
                  doHidden("hRptFile","rptSummaryAttendance","");
                  doHidden("hRptScrn","YES","");
               ?>
               <!-- Modal -->
               <div class="modal fade border0" id="prnModal" role="dialog">
                  <div class="modal-dialog" style="height:90%;width:98%">
                     <div class="mypanel border0" style="height:100%;">
                        <div class="panel-top bgSilver">
                           <a href="#" data-toggle="tooltip" data-placement="top" title="Print Now" id="btnPRINTNOW">
                              <i class="fa fa-print" aria-hidden="true"></i>
                           </a>
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                     </div>
                  </div>
               </div>
               <!--
               <div class="modal fade border0" id="prnModal" role="dialog">
                  <div class="modal-dialog modal-lg">

                     <div class="mypanel border0" style="height:100%;">
                        <div class="panel-top bgSilver">
                           <a href="#" data-toggle="tooltip" data-placement="top" title="Print Now" id="btnPRINTNOW">
                              <i class="fa fa-print" aria-hidden="true"></i>
                           </a>
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                     </div>
                  </div>
               </div>
               -->
               <?php modalEmpLkUp(); ?>
         </div>
      </form>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_Dashboard"); ?>"></script>
   </body>
</html>



