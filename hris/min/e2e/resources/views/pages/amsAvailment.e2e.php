<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript">
      </script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar(getvalue("paramTitle")); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varHidden.e2e.php";
               doHidden("hRptFile","DashboardRpt","");
            ?>
         </div>
      </form>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_Dashboard"); ?>"></script>
   </body>
</html>