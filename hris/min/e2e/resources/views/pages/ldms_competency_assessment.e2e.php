<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>

   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post">
         <?php $sys->SysHdr($sys,"ldms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("Competency Assessment"); ?>
            <div class="container-fluid margin-top">
               <div class="mypanel">
                  <div class="row">
                     <div class="col-sm-3">
                        <?php
                           employeeSelector();
                        ?>
                     </div>
                     <div class="col-xs-9" id="div_CONTENT">
                        <div class="margin-top">
                           <div id="divList">
                              <div class="mypanel panel panel-default">
                                 <div class="panel-top">
                                    <span id="ScreenMode">INSERTING NEW COMPETENCY ASSESSMENT
                                 </div>
                                 <div class="panel-mid">
                                    <span id="spGridTable">
                                       <?php
                                             $table = "ldmscompetency_assessment";
                                             $gridTableHdr_arr = ["Employee Name","Competency Name","Year Conducted","Self Assessment"];
                                             $gridTableFld_arr = ["EmployeesRefId","LDMSCompetencyRefId","YearConducted","SelfAssessment"];
                                             $sql = "SELECT * FROM ldmscompetency_assessment";
                                             $Action = [true,true,true,false];
                                             doGridTable($table,
                                                         $gridTableHdr_arr,
                                                         $gridTableFld_arr,
                                                         $sql,
                                                         $Action,
                                                         $_SESSION["module_gridTable_ID"]);
                                       ?>
                                    </span>
                                 </div>
                                 <div class="panel-bottom">
                                    <?php
                                       btnINRECLO([true,false,false]);
                                    ?>
                                 </div>
                              </div>
                           </div>
                           <div id="divView">
                              <div class="mypanel panel panel-default">
                                 <div class="panel-top">
                                    <span id="ScreenMode">INSERTING NEW COMPETENCY ASSESSMENT
                                 </div>
                                 <div class="panel-mid-litebg" id="EntryScrn">
                                    <div class="container" id="EntryScrn">
                                       <div class="row">
                                          <div class="col-xs-4">
                                             <label class="control-label" for="inputs">Current Position:</label><br>
                                             <?php
                                                createSelect("Position",
                                                             "sint_PositionRefId",
                                                             "",100,"Name","Select Position","");
                                             ?>
                                          </div>
                                          <div class="col-xs-4">
                                             <label class="control-label" for="inputs">Current Department:</label><br>
                                             <?php
                                                createSelect("Department",
                                                             "sint_DepartmentRefId",
                                                             "",100,"Name","Select Department","");
                                             ?>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-xs-4">
                                             <input type="hidden" name="sint_EmployeesRefId" id="sint_EmployeesRefId" class="saveFields-- mandatory">
                                             <label class="control-label" for="inputs">Employee Name:</label><br>
                                             <input class="form-input" 
                                                    type="text"
                                                    name="employeename"
                                                    id="employeename">
                                          </div>
                                          <div class="col-xs-4">
                                             <label class="control-label" for="inputs">Year In Position:</label><br>
                                             <input class="form-input date--" 
                                                    type="text"
                                                    name="employeename"
                                                    id="employeename">
                                          </div>
                                       </div>
                                       <br><br><br>
                                       <div class="row">
                                          <div class="col-xs-6">
                                             <div class="row">
                                                <div class="form-group">
                                                   <label class="control-label" for="inputs">Competency:</label><br>
                                                   <?php
                                                      createSelect("ldmscompetency",
                                                                   "sint_LDMSCompetencyRefId",
                                                                   "",100,"Name","Select Competency","");
                                                   ?>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="form-group">
                                                   <label class="control-label" for="inputs">YEAR CONDUCTED:</label><br>
                                                   <select class="form-input saveFields-- mandatory" 
                                                          name="sint_YearConducted"
                                                          id="sint_YearConducted">
                                                          <?php
                                                            for ($i=(date("Y",time()) - 5); $i <= (date("Y",time()) + 5); $i++) { 
                                                               echo '<option value="'.$i.'">'.$i.'</option>';
                                                            }
                                                          ?>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="form-group">
                                                   <label class="control-label" for="inputs">SELF ASSESSMENT:</label><br>
                                                   <select class="form-input saveFields-- mandatory" 
                                                          name="sint_SelfAssessment"
                                                          id="sint_SelfAssessment">
                                                          <option value="1">1</option>
                                                          <option value="2">2</option>
                                                          <option value="3">3</option>
                                                          <option value="4">4</option>
                                                          <option value="5">5</option>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="form-group">
                                                   <label class="control-label" for="inputs">SUPERVISOR:</label><br>
                                                   <input class="form-input saveFields-- mandatory uCase--" 
                                                          type="text"
                                                          name="char_Supervisor"
                                                          id="char_Supervisor">
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="form-group">
                                                   <label class="control-label" for="inputs">NEED L&D INTERVENTION?:</label><br>
                                                   <select class="form-input saveFields-- mandatory" 
                                                          name="sint_IsIntervention"
                                                          id="sint_IsIntervention">
                                                          <option value="YES">YES</option>
                                                          <option value="NO">NO</option>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="form-group">
                                                   <label class="control-label" for="inputs">REMARKS:</label>
                                                   <textarea class="form-input saveFields--" 
                                                             rows="5" 
                                                             name="char_Remarks"
                                                             placeholder="remarks"></textarea>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel-bottom">
                                    <?php btnSACABA([true,true,true]); ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               $table = "ldmscompetency_assessment";
               doHidden("paramTitle",getvalue("paramTitle"),"");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
   <script language="JavaScript">
      $(document).ready(function () {
         remIconDL();
      });
      function afterNewSave() {
         alert("Successfully Saved");
         gotoscrn("ldms_competency_assessment","");
      }
      function afterEditSave() {
         alert("Successfully Updated");
         gotoscrn("ldms_competency_assessment","");   
      }
      function selectMe(emprefid) {
         $("[name='sint_EmployeesRefId']").val(emprefid);
         $.get("ldmsEmpDetail.e2e.php",
         {
            EmpRefId:emprefid,
            hCompanyID:$("#hCompanyID").val(),
            hBranchID:$("#hBranchID").val(),
            hEmpRefId:$("#hEmpRefId").val(),
            hUserRefId:$("#hUserRefId").val()
         },
         function(data,status) {
            if (status == "success") {
               try {
                  eval(data);
               } catch (e) {
                   if (e instanceof SyntaxError) {
                       alert(e.message);
                   }
               }
            }
         });
      }
   </script>
</html>



