<?php
   $ScrnMode = $code = $name = $remarks = $disabled = $msg = "";
   
   $mainTable = "";
   if (isset($_GET["hScrnMode"]))
      $ScrnMode = $_GET["hScrnMode"];
   else if (isset($_POST["hScrnMode"]))
      $ScrnMode = $_POST["hScrnMode"];

   if ($ScrnMode == 1) {
      $refid = "";
   } else if ($ScrnMode == 3 || $ScrnMode == 2) {
      session_start();
      require_once $_SESSION['Classes'].'0620functions.e2e.php';
      require_once "conn.e2e.php";
      $msg = getvalue("msg");
      $mainTable = getvalue("hTable");
      if ($ScrnMode == 3) $disabled = "disabled";
      $refid = getvalue("hRefId");
      if ($refid) {
         $criteria  = " WHERE RefId = $refid";
         $criteria .= " LIMIT 1";
         $recordSet = f_Find($mainTable,$criteria);
         $rowcount = mysqli_num_rows($recordSet);
         $row = array();
         $row = mysqli_fetch_assoc($recordSet);
         if ($rowcount) {
            $code = $row["Code"];
            $name = $row["Name"];
            $remarks = $row["Remarks"];
         }
      }
   }
?>