<?php
   if (isset($_POST["month_"])) {
      $m = $_POST["month_"];
   } else {
      $m = intval(date("m",time()));
   }
   if (isset($_POST["year_"])) {
      $y = $_POST["year_"];
   } else {
      $y = date("Y",time());
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>

   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post">
         <?php $sys->SysHdr($sys,"ldms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("Training Calendar"); ?>
            <div class="container-fluid margin-top">
               <div class="mypanel">
                  <div class="row" id="divList">
                     <div class="col-xs-12">
                        <div class="panel-top">
                           Training Calendar
                        </div>
                        <div class="panel-mid">
                           <div class="row">
                              <div class="col-xs-2">
                                 <select class="form-input" id="month_" name="month_">
                                    <option value="0">Select Month</option>
                                    <option value="1">January</option>
                                    <option value="2">February</option>
                                    <option value="3">March</option>
                                    <option value="4">April</option>
                                    <option value="5">May</option>
                                    <option value="6">June</option>
                                    <option value="7">July</option>
                                    <option value="8">August</option>
                                    <option value="9">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                 </select>
                              </div>
                              <div class="col-xs-2">
                                 <select class="form-input" id="year_" name="year_">
                                    <option value="0">Select Year</option>
                                    <?php
                                       $start   = date("Y",time()) - 1;
                                       $end     = date("Y",time());
                                       for ($a=$start; $a <= $end ; $a++) { 
                                          echo '<option value="'.$a.'">'.$a.'</option>';
                                       }
                                    ?>
                                 </select>
                              </div>
                              <div class="col-xs-3">
                                 <button type="submit" id="" class="btn-cls4-sea">VIEW</button>      
                              </div>
                           </div>
                           <br>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <table class="table table-order-column table-striped table-bordered table-hover">
                                    <thead>
                                       <tr>
                                          <th style="width: 30%;">Inclusive Date/s</th>
                                          <th style="width: 70%;">Training</th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <?php
                                          $where   = "WHERE Month(StartDate) = '$m' AND Year(StartDate) = '$y' AND Posted > '0'";
                                          $rs      = SelectEach("ldmslndprogram",$where); 
                                          if ($rs) {
                                             while ($row = mysqli_fetch_assoc($rs)) {
                                                $Name       = $row["Name"];
                                                $StartDate  = $row["StartDate"];
                                                $EndDate    = $row["EndDate"];
                                                if ($StartDate != "") {
                                                   if ($StartDate == $EndDate) {
                                                      $Date = date("F d, Y",strtotime($StartDate));
                                                   } else {
                                                      $Date = date("F d, Y",strtotime($StartDate))." to ".date("F d, Y",strtotime($EndDate));
                                                   }
                                                } else {
                                                   $Date = "Not Set";
                                                }
                                                
                                                echo '<tr>';
                                                echo '<td class="text-center">'.$Date.'</td>';
                                                echo '<td>';
                                                echo $Name."<br>";
                                                echo '<button type="button" class="btn-cls2-sea" onClick="viewParticipants('.$row["RefId"].');">List of Participants</button>';
                                                echo '<button type="button" class="btn-cls2-sea" onClick="viewInfo('.$row["RefId"].',3,\'\');">View Details</button>';
                                                echo '</td>';
                                                echo '</tr>';   
                                             }
                                          } else {
                                             echo '<tr>';
                                             echo '<td colspan="2">No Training Available</td>';
                                             echo '</tr>';
                                          }
                                       ?>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                        <div class="panel-bottom">
                           
                        </div>
                     </div>
                  </div>
                  <div id="divView">
                     <div class="mypanel panel panel-default">
                        <div class="panel-top">
                           <span id="ScreenMode">TRAINING/PROGRAM DETAILS
                        </div>
                        <div class="panel-mid-litebg" id="EntryScrn">
                           <div class="container" id="EntryScrn">
                              <div class="row">
                                 <div class="col-xs-8">
                                    <label class="control-label" for="inputs">Course Title:</label><br>
                                    <input class="form-input saveFields-- mandatory" 
                                           type="text"
                                           name="char_Name"
                                           id="char_Name">
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-8">
                                    <label class="control-label" for="inputs">Training Institution:</label><br>
                                    <input class="form-input saveFields-- mandatory" 
                                           type="text"
                                           name="char_TrainingInstitution"
                                           id="char_TrainingInstitution">
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-4">
                                    <label class="control-label" for="inputs">Start Date:</label><br>
                                    <input class="form-input saveFields-- mandatory date--" 
                                           type="text"
                                           name="date_StartDate"
                                           id="date_StartDate">
                                 </div>
                                 <div class="col-xs-4">
                                    <label class="control-label" for="inputs">End Date:</label><br>
                                    <input class="form-input saveFields-- mandatory date--" 
                                           type="text"
                                           name="date_EndDate"
                                           id="date_EndDate">
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    <input type="checkbox" name="sint_Resched" id="sint_Resched" value="0">
                                    &nbsp;&nbsp;&nbsp;If Reschedule:
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-4">
                                    <label class="control-label" for="inputs">Start Date:</label><br>
                                    <input class="form-input saveFields-- date--" 
                                           type="text"
                                           name="date_NewStartDate"
                                           id="date_NewStartDate">
                                 </div>
                                 <div class="col-xs-4">
                                    <label class="control-label" for="inputs">End Date:</label><br>
                                    <input class="form-input saveFields-- date--" 
                                           type="text"
                                           name="date_NewEndDate"
                                           id="date_NewEndDate">
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-8">
                                    <label class="control-label" for="inputs">Venue:</label><br>
                                    <input class="form-input saveFields--" 
                                           type="text"
                                           name="char_Venue"
                                           id="char_Venue">
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-4">
                                    <label class="control-label" for="inputs">Slots:</label><br>
                                    <input class="form-input saveFields--" 
                                           type="text"
                                           name="char_Slot"
                                           id="char_Slot">
                                 </div>
                                 <div class="col-xs-4">
                                    <label class="control-label" for="inputs">Cost:</label><br>
                                    <input class="form-input saveFields--" 
                                           type="text"
                                           name="char_Cost"
                                           id="char_Cost">
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-8">
                                    <label>COMPETENCY</label>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-4">
                                    <label class="control-label" for="inputs">Core:</label><br>
                                    <input class="form-input saveFields--" 
                                           type="text"
                                           name="char_Core"
                                           id="char_Core">
                                 </div>
                                 <div class="col-xs-3">
                                    <label class="control-label" for="inputs">Level:</label><br>
                                    <select class="form-input saveFields--" name="sint_CoreLevel" id="sint_CoreLevel" >
                                       <option value="">Select Level</option>
                                       <option value="1">Level 1</option>
                                       <option value="2">Level 2</option>
                                       <option value="3">Level 3</option>
                                       <option value="4">Level 4</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-4">
                                    <label class="control-label" for="inputs">Foundation:</label><br>
                                    <input class="form-input saveFields--" 
                                           type="text"
                                           name="char_Foundation"
                                           id="char_Foundation">
                                 </div>
                                 <div class="col-xs-3">
                                    <label class="control-label" for="inputs">Level:</label><br>
                                    <select class="form-input saveFields--" name="sint_FoundationLevel" id="sint_FoundationLevel" >
                                       <option value="">Select Level</option>
                                       <option value="1">Level 1</option>
                                       <option value="2">Level 2</option>
                                       <option value="3">Level 3</option>
                                       <option value="4">Level 4</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-4">
                                    <label class="control-label" for="inputs">Organizational:</label><br>
                                    <input class="form-input saveFields--" 
                                           type="text"
                                           name="char_Organizational"
                                           id="char_Organizational">
                                 </div>
                                 <div class="col-xs-3">
                                    <label class="control-label" for="inputs">Level:</label><br>
                                    <select class="form-input saveFields--" name="sint_OrganizationalLevel" id="sint_OrganizationalLevel" >
                                       <option value="">Select Level</option>
                                       <option value="1">Level 1</option>
                                       <option value="2">Level 2</option>
                                       <option value="3">Level 3</option>
                                       <option value="4">Level 4</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-4">
                                    <label class="control-label" for="inputs">Leadership:</label><br>
                                    <input class="form-input saveFields--" 
                                           type="text"
                                           name="char_Leadership"
                                           id="char_Leadership">
                                 </div>
                                 <div class="col-xs-3">
                                    <label class="control-label" for="inputs">Level:</label><br>
                                    <select class="form-input saveFields--" name="sint_LeadershipLevel" id="sint_LeadershipLevel" >
                                       <option value="">Select Level</option>
                                       <option value="1">Level 1</option>
                                       <option value="2">Level 2</option>
                                       <option value="3">Level 3</option>
                                       <option value="4">Level 4</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-4">
                                    <label class="control-label" for="inputs">Technical:</label><br>
                                    <input class="form-input saveFields--" 
                                           type="text"
                                           name="char_Technical"
                                           id="char_Technical">
                                 </div>
                                 <div class="col-xs-3">
                                    <label class="control-label" for="inputs">Level:</label><br>
                                    <select class="form-input saveFields--" name="sint_TechnicalLevel" id="sint_TechnicalLevel" >
                                       <option value="">Select Level</option>
                                       <option value="1">Level 1</option>
                                       <option value="2">Level 2</option>
                                       <option value="3">Level 3</option>
                                       <option value="4">Level 4</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-2">
                                    <label class="control-label" for="inputs">Deadline:</label><br>
                                    <input class="form-input saveFields-- mandatory date--" 
                                           type="text"
                                           name="date_Deadline"
                                           id="date_Deadline">
                                 </div>
                                 <div class="col-xs-2">
                                    <label class="control-label" for="inputs">Post?</label><br>
                                    <select class="form-input saveFields--" name="sint_Posted" id="sint_Posted">
                                       <option value="0">Training Calender</option>
                                       <option value="1">User Portal</option>
                                       <option value="2">Both</option>
                                    </select>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="panel-bottom">
                           <?php btnSACABA([true,true,true]); ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="participantModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div>
                     <div class="modal-content">
                        <div class="modal-header">
                           List of Participants
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                           <div class="row">
                              <div class="col-xs-12" style="padding: 10px;">
                                 <table class="table table-order-column table-striped table-bordered table-hover">
                                    <thead>
                                       <tr>
                                          <th>#</th>
                                          <th>Employee Name</th>
                                          <th>Employee ID</th>
                                       </tr>
                                    </thead>
                                    <tbody id="participant_content"></tbody>
                                 </table>   
                              </div>
                           </div>
                        </div>
                        <div class="modal-footer">
                           &nbsp;
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               $table = "ldmslndprogram";
               doHidden("paramTitle",getvalue("paramTitle"),"");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
   <script language="JavaScript">
      $(document).ready(function () {
         <?php
            if (isset($_POST["month_"])) {
               $m = $_POST["month_"];
            } else {
               $m = intval(date("m",time()));
            }

            if (isset($_POST["year_"])) {
               $y = $_POST["year_"];
            } else {
               $y = date("Y",time());
            }
            echo '$("#month_").val("'.$m.'");';        
            echo '$("#year_").val("'.$y.'");';        
         ?>
      });
      function viewParticipants(refid){
         var str = "";
         $("#participantModal").modal();
         $.get("ldms_transaction.e2e.php",
         {
            refid: refid,
            fn: "getParticipants"
         },
         function(data,status) {
            if (status == "success") {
               try {
                  data = data.trim();
                  if (data != "") {
                     var arr = JSON.parse(data);
                     for (var i = 0; i < arr.length; i++) {
                        var count      = i+1;
                        var LastName   = arr[i]["LastName"];
                        var FirstName  = arr[i]["FirstName"];
                        var AgencyId   = arr[i]["AgencyId"];
                        str += '<tr>';
                        str += '<td class="text-center">'+count+'</td>';
                        str += '<td>'+LastName + ", " + FirstName +'</td>';
                        str += '<td class="text-center">'+AgencyId+'</td>';
                        str += '</tr>';   
                     }   
                  } else {
                     str += '<tr>';
                     str += '<td colspan="3">No Participants in this Training</td>';
                     str += '</tr>';
                  }
                  $("#participant_content").html(str);
               } catch (e) {
                  if (e instanceof SyntaxError) {
                     alert(e.message);
                  }
               }
            }
         });
      }
   </script>
</html>



