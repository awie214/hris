<?php
   include_once "constant.e2e.php";
   include_once pathClass.'0620functions.e2e.php';
   include_once pathClass.'SysFunctions.e2e.php';
   $sys = new SysFunctions();
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript">
      </script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar ($paramTitle); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <?php
                        $whereClause = "";
                        $rs = SelectEach("employees",$whereClause);
                        if ($rs) $rowcount = mysqli_num_rows($rs);

                        $count_1 = 150;
                        $count_2 = 15;
                        $count_3 = 10;
                        $count_4 = 14;
                        $count_5 = 10;
                        $count_6 = 5;
                        $count_7 = 8;
                        $count_8 = 6;
                        $count_9 = 3;
                        $count_10 = 4;
                        $count_11 = 25;
                        $count_12 = 20;
                        $count_13 = 45;
                        $count_14 = 4;
                        $count_15 = 15;
                        $count_16 = 35;
                     ?>

                     <div class="row">
                        <div class="col-xs-6" style="padding:20px;">
                           <div style="border-radius:20px;height:100%;width:100%;background:var(--sea);color:#fff;">
                              <?php spacer(10) ?>
                              <div class="txt-center"><h4>TOTAL NUMBER OF EMPLOYEE</h4></div>
                              <div class="row">
                                 <div class="col-xs-6 txt-center" style="padding:0px 10px 20px 20px;">
                                    <div class="counts--" style="color:var(--sea);" onclick="showEmp(1,<?php echo $rowcount; ?>);">
                                       <h1><?php echo $rowcount; ?></h1>
                                       <div class="txt-center"><h5>TOTAL EMPLOYEES</h5></div>
                                    </div>
                                 </div>
                                 <div class="col-xs-6 txt-center" style="padding:0px 20px 20px 10px;">
                                    <div class="counts--" style="color:var(--sea);" onclick="showEmp(2,<?php echo $count_2; ?>);">
                                       <h1><?php echo $count_2; ?></h1>
                                       <div class="txt-center"><h5>NEWLY REGULAR</h5></div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-xs-6 txt-center" style="padding:0px 10px 20px 20px;">
                                    <div class="counts--" style="color:var(--sea);" onclick="showEmp(3,<?php echo $count_3; ?>);">
                                       <h1><?php echo $count_3; ?></h1>
                                       <div class="txt-center"><h5>END OF CONTRACT</h5></div>
                                    </div>
                                 </div>
                                 <div class="col-xs-6 txt-center" style="padding:0px 20px 20px 10px;">
                                    <div class="counts--" style="color:var(--sea);" onclick="showEmp(4,<?php echo $count_4; ?>);">
                                       <h1><?php echo $count_4; ?></h1>
                                       <div class="txt-center"><h5>RESIGNED / TERMINATED</h5></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="col-xs-6" style="padding:20px;">
                           <div style="border-radius:20px;height:100%;width:100%;background:var(--lemon);color:#fff;">
                              <?php spacer(10) ?>
                              <div class="txt-center"><h4>NOTIFICATION FOR</h4></div>
                              <div class="row">
                                 <div class="col-xs-6 txt-center" style="padding:0px 10px 20px 20px;">
                                    <div class="counts--" style="color:var(--lemon);" onclick="showEmp(5,<?php echo $count_5; ?>);">
                                       <h1><?php echo $count_5; ?></h1>
                                       <div class="txt-center"><h5>BIRTHDAY</h5></div>
                                    </div>
                                 </div>
                                 <div class="col-xs-6 txt-center" style="padding:0px 20px 20px 10px;">
                                    <div class="counts--" style="color:var(--lemon);" onclick="showEmp(6,<?php echo $count_6; ?>);">
                                       <h1><?php echo $count_6; ?></h1>
                                       <div class="txt-center"><h5>ANNIVERSARY</h5></div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-xs-6 txt-center" style="padding:0px 10px 20px 20px;">
                                    <div class="counts--" style="color:var(--lemon);" onclick="showEmp(7,<?php echo $count_7; ?>);">
                                       <h1><?php echo $count_7; ?></h1>
                                       <div class="txt-center"><h5>PROMOTED</h5></div>
                                    </div>
                                 </div>
                                 <div class="col-xs-6 txt-center" style="padding:0px 20px 20px 10px;">
                                    <div class="counts--" style="color:var(--lemon);" onclick="showEmp(8,<?php echo $count_8; ?>);">
                                       <h1><?php echo $count_8; ?></h1>
                                       <div class="txt-center"><h5>RETIREES</h5></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-6" style="padding:20px;">
                           <div style="border-radius:20px;height:100%;width:100%;background:var(--apple);color:#fff;">
                              <?php spacer(10) ?>
                              <div class="txt-center"><h4>STEP INCREMENT AND LOYALTY</h4></div>
                              <div class="row">
                                 <div class="col-xs-6 txt-center" style="padding:0px 10px 20px 20px;" onclick="showEmp(9,<?php echo $count_9; ?>);">
                                    <div class="counts--" style="color:var(--apple);">
                                       <h1><?php echo $count_9; ?></h1>
                                       <div class="txt-center"><h5>3 YEARS</h5></div>
                                    </div>
                                 </div>
                                 <div class="col-xs-6 txt-center" style="padding:0px 20px 20px 10px;" onclick="showEmp(10,<?php echo $count_10; ?>);">
                                    <div class="counts--" style="color:var(--apple);">
                                       <h1><?php echo $count_10; ?></h1>
                                       <div class="txt-center"><h5>4 - 10 YEARS</h5></div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-xs-6 txt-center" style="padding:0px 10px 20px 20px;" onclick="showEmp(11,<?php echo $count_11; ?>);">
                                    <div class="counts--" style="color:var(--apple);" >
                                       <h1><?php echo $count_11; ?></h1>
                                       <div class="txt-center"><h5>11 - 15 YEARS</h5></div>
                                    </div>
                                 </div>
                                 <div class="col-xs-6 txt-center" style="padding:0px 20px 20px 10px;" onclick="showEmp(12,<?php echo $count_12; ?>);">
                                    <div class="counts--" style="color:var(--apple);">
                                       <h1><?php echo $count_12; ?></h1>
                                       <div class="txt-center"><h5>16 - UP</h5></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="col-xs-6" style="padding:20px;">
                           <div style="border-radius:20px;height:100%;width:100%;background:var(--tree);color:#fff;">
                              <?php spacer(10) ?>
                              <div class="txt-center"><h4>EMPLOYEES DOCUMENTS / IDS(#) / etc.</h4></div>
                              <div class="row">
                                 <div class="col-xs-6 txt-center" style="padding:0px 10px 20px 20px;" onclick="showEmp(13,<?php echo $count_13; ?>);">
                                    <div class="counts--" style="color:var(--tree);">
                                       <h1><?php echo $count_13; ?></h1>
                                       <div class="txt-center"><h5>NO BIRTH CERTIFICATE</h5></div>
                                    </div>
                                 </div>
                                 <div class="col-xs-6 txt-center" style="padding:0px 20px 20px 10px;" onclick="showEmp(14,<?php echo $count_14; ?>);">
                                    <div class="counts--" style="color:var(--tree);">
                                       <h1><?php echo $count_14; ?></h1>
                                       <div class="txt-center"><h5>NO TOR</h5></div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-xs-6 txt-center" style="padding:0px 10px 20px 20px;" onclick="showEmp(15,<?php echo $count_15; ?>);">
                                    <div class="counts--" style="color:var(--tree);">
                                       <h1><?php echo $count_15; ?></h1>
                                       <div class="txt-center"><h5>No. of DUAL CITIZEN</h5></div>
                                    </div>
                                 </div>
                                 <div class="col-xs-6 txt-center" style="padding:0px 20px 20px 10px;" onclick="showEmp(16,<?php echo $count_16; ?>);">
                                    <div class="counts--" style="color:var(--tree);">
                                       <h1><?php echo $count_16; ?></h1>
                                       <div class="txt-center"><h5>with GRAD. STUDIES</h5></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade border0" id="prnModal" role="dialog">
               <div class="modal-dialog border0" style="padding:0px;width:97%;height:92%;">

                  <div class="mypanel border0" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Print Now" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>

            <?php
               footer();
               include "varHidden.e2e.php";
               doHidden("hRptFile","DashboardRpt","");
            ?>
         </div>
      </form>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_Dashboard"); ?>"></script>
   </body>
</html>



