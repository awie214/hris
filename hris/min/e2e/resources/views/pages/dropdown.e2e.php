<!DOCTYPE>
<html>
<head>
	<?php include_once $files["inc"]["pageHEAD"]; ?>
	<title></title>
	<script type="text/javascript">
		function load_dropdown(table,obj,batch,srchString) {
	         $.get("getDropDown.e2e.php",{
	            table:table,
	            obj:obj,
	            batch:batch,
	            srchString:srchString
	         },
	         function(data,status) {
	            if (status == "success") {
	               	try {
	               		$("#dropdown_title").html("LIST OF " + table);
	                	$("#dropdown_modal").modal();
	                  	$("#dropdown_details").html(data);
	               	} catch (e) {
	            	   	if (e instanceof SyntaxError) {
	                	    alert(e.message);
	        	        }
	      	        }
	           	}
	       	});
	   	}
	   	function insert_dropdown(refid,objname,name){
	      	$("#"+objname).val(name);
	      	$("[name='"+objname+"']").val(refid);
	    	$("#dropdown_modal").modal("hide");
	   	}
	</script>
</head>
<body>
	<div class="container">
		<br><br><br>
		<div class="row">
			<div class="col-xs-6">
				<input type="hidden" class="form-input saveFields--" value="" name="GradStudiesRefId_5_<?php echo $j; ?>">
				<input type="text" class="form-input" id="sint_CountryRefId" style="width: 80%;" placeholder="MANDATORY" readonly>
				<input type="hidden" class="form-input saveFields-- mandatory" name="sint_CountryRefId">
				<a href="javascript:void(0);"
				    onclick="load_dropdown('country','sint_CountryRefId',1,'');">
				   <i class="fa fa-search" aria-hidden="true"></i>
				</a>
				<span class="newDataLibrary">
				   <a href="javascript:void(0);"
				      onclick="popAddSchools(5,'chkGradStudies_5_<?php echo $j; ?>','sint_SchoolsRefId_5_<?php echo $j; ?>');"
				      id="FM_schools" class="FMModalNewRecord--" for="" title="Add New School for Grad. Studies">
				      <i class="fa fa-plus-square" aria-hidden="true"></i>
				   </a>
				</span>
			</div>
		</div>
	</div>
	<div class="modal fade" id="dropdown_modal" role="dialog">
      	<div class="modal-dialog modal-lg" style="width: 60%;">
         	<div class="mypanel">
            	<div class="panel-top" id="dropdown_title">
               		<button type="button" class="close" data-dismiss="modal">&times;</button>
            	</div>
            	<div class="panel-mid">
               		<div class="row">
                  		<div class="col-xs-12" id="dropdown_details"></div>
               		</div>
            	</div>
            	<div class="panel-bot">&nbsp;</div>
         	</div>
    	</div>
   	</div>
</body>
</html>