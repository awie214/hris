<?php
	include 'conn.e2e.php';
	include 'constant.e2e.php';
	include pathClass.'0620functions.e2e.php';
	include pathClass.'Mailer.e2e.php';
	//include 'upl/FnUpload.php';
	function saveFM($table,$flds,$vals,$name,$where = "") {
		include 'conn.e2e.php';
		$table 			= strtolower($table);
		$where 			= "WHERE Name = '$name' ".$where;
		$check_FM 		= FindFirst($table,$where,"RefId");
		if (is_numeric($check_FM)) {
			return $check_FM;
		} else {
			$date_today    	= date("Y-m-d",time());
	        $curr_time     	= date("H:i:s",time());
	        $trackingA_fld 	= "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
	        $trackingA_val 	= "'$date_today', '$curr_time', 'Admin', 'A'";
	        $flds   	   	= $flds.$trackingA_fld;
	        $vals   		= $vals.$trackingA_val;
	        $sql 			= "INSERT INTO $table ($flds) VALUES ($vals)";
	        $rs 			= mysqli_query($conn,$sql) or die(mysqli_error($conn));
	        if ($rs) {
	        	return mysqli_insert_id($conn);
	        } else {
	        	echo $rs."<br>$sql";
	        }	
		}
			
	}
	if (!isset($_GET["test"])) {
		echo '<h3>No Parameter</h3>';
		return false;
	}
	$test = $_GET["test"];
	
	if ($test == 1) {
		$sql = "SELECT * from employees inner join pms_employees WHERE employees.FirstName = pms_employees.firstname AND employees.LastName = pms_employees.lastname ORDER BY employees.LastName";
		$rs = mysqli_query($conn,$sql);
		echo '<table border=1 width="100%">';
		echo '
			<tr>
				<td>LastName</td>
				<td>FirstName</td>
				<td>AgencyId</td>
				<td>employee_number</td>
				<td>lastname</td>
				<td>firstname</td>
			</tr>
		';
		while ($row = mysqli_fetch_assoc($rs)) {
			$emprefid = $row["id"];
			$AgencyId = $row["AgencyId"];
			$LastName = $row["LastName"];
			$FirstName = $row["FirstName"];
			$employee_number = $row["employee_number"];
			$lastname = $row["lastname"];
			$firstname = $row["firstname"];
			$update = "UPDATE pms_employees SET employee_number = '$AgencyId' WHERE id='$emprefid'";
			$result = mysqli_query($conn,$update);
			echo '
				<tr>
					<td>'.$LastName.'</td>
					<td>'.$FirstName.'</td>
					<td>'.$AgencyId.'</td>
					<td>'.$employee_number.'</td>
					<td>'.$emprefid." -> ".$lastname.'</td>
					<td>'.$firstname.'</td>
				</tr>
			';
			
			if ($result) {
				echo "Update $FirstName employee number. -> $update<br>";
			} else {
				echo $update." -> ".$result."<br>";
			}
		}
		echo '</table>';
	} else if ($test == 2) {
		//update empinformation SET HiredDate = NULL, AssumptionDate = NULL, StartDate = NULL, EndDate = NULL, ResignedDate = NULL
		echo '<table border=1 width="100%">';
		echo '
			<tr>
				<td>Start Date</td>
				<td>End Date</td>
				<td>Position</td>
				<td>Salary Amount</td>
				<td>Salary Grade</td>
				<td>Step</td>
				<td>Appt Status</td>
			</tr>
		';
		$where = "WHERE employee_personal_information_sheet_id = 4";
		$where = "";
		$emp = SelectEach("employees","");
		$active = 0;
		$inactive = 0;
		if ($emp) {
			while ($emp_row = mysqli_fetch_assoc($emp)) {
				$emprefid = $emp_row["RefId"];
				$where = "WHERE employee_personal_information_sheet_id = '$emprefid'";
				$sql = "SELECT * FROM employee_work_experiences $where ORDER BY start_date DESC LIMIT 1";
				$rs = mysqli_query($conn,$sql);
				if ($rs) {
					while ($row = mysqli_fetch_assoc($rs)) {
						$start_date = $row["start_date"];
						$end_date = $row["end_date"];
						$position_title = $row["position_title"];
						$monthly_salary = $row["monthly_salary"];
						$salary_grade = intval($row["salary_grade"]);
						$step = intval($row["step"]);
						$status_of_appointment = $row["status_of_appointment"];
						if ($end_date == "") {
							$active++;
						} else {
							$inactive++;
						}
						$Fld = "";
						$Val = "";
						$FldnVal = "";
						if ($end_date != "") {
							$update = "Inactive = 1, ";
							$result_update = f_SaveRecord("EDITSAVE","employees",$update,$emprefid);
							if ($result != "") {
								echo $result."<br>";
							}
						}
						if (
							strtolower($status_of_appointment) == "contractual" || 
							strtolower($status_of_appointment) == "co-terminous w/ pascn" ||
							strtolower($status_of_appointment) == "coterminus"
						) {
							//$Fld .= "`StartDate`,`AssumptionDate`,";
							//$Val .= "'$start_date','$start_date',";
							$FldnVal .= "`StartDate` = '$start_date', `AssumptionDate` = '$start_date',";
							if ($end_date != "") {
								//$Fld .= "`EndDate`,";
								//$Val .= "'$end_date',";
								$FldnVal .= "`EndDate` = '$end_date',";
							}
						} else {
							//$Fld .= "HiredDate, AssumptionDate, ";
							//$Val .= "'$start_date','$start_date', ";
							$FldnVal .= "`HiredDate` = '$start_date', `AssumptionDate` = '$start_date', ";
							if ($end_date != "") {
								//$Fld .= "`ResignedDate`,";
								//$Val .= "'$end_date',";
								$FldnVal .= "`ResignedDate` = '$end_date',";
							}
						}
						if ($salary_grade > 0) {
							$SalaryGradeRefId = FindFirst("salarygrade","WHERE Name = '$salary_grade'","RefId");
							if ($SalaryGradeRefId) {
								//$Fld .= "`SalaryGradeRefId`,";
								//$Val .= "'$SalaryGradeRefId',";
								$FldnVal .= "`SalaryGradeRefId` = '$SalaryGradeRefId',";
							}
						}
						if ($step > 0) {
							$StepIncrementRefId = FindFirst("stepincrement","WHERE Name = 'STEP-$step'","RefId");
							if ($StepIncrementRefId) {
								//$Fld .= "`StepIncrementRefId`,";
								//$Val .= "'$StepIncrementRefId',";
								$FldnVal .= "`StepIncrementRefId` = '$StepIncrementRefId',";
							}
						}
						if (intval($monthly_salary) > 0) {
							//$Fld .= "`SalaryAmount`,";
							//$Val .= "'$monthly_salary',";
							$FldnVal .= "`SalaryAmount` = '$monthly_salary',";
						}
						if ($position_title != "") {
							$PositionRefId = saveFM("position","`Name`,","`$position_title`",$position_title);
							if (is_numeric($PositionRefId)) {
								//$Fld .= "`PositionRefId`,";
								//$Val .= "'$PositionRefId',";
								$FldnVal .= "`PositionRefId` = '$PositionRefId',";	
							}
						}
						if ($status_of_appointment != "") {
							$status_of_appointment = strtoupper($status_of_appointment);
							$EmpStatusRefId = saveFM("empstatus","`Name`,","`$status_of_appointment`",$status_of_appointment);
							if (is_numeric($EmpStatusRefId)) {
								//$Fld .= "`EmpStatusRefId`,";
								//$Val .= "'$EmpStatusRefId',";
								$FldnVal .= "`EmpStatusRefId` = '$EmpStatusRefId',";	
							}
						}
						$empinfo = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","RefId");
						if ($empinfo) {
							$result = f_SaveRecord("EDITSAVE","empinformation",$FldnVal,$empinfo,"System");
							if ($result != "") {
								echo "Error $emprefid.<br>";
							}
						}
						echo '
							<tr>
							<td>'.$start_date.'</td>
							<td>'.$end_date.'</td>
							<td>'.$position_title.'</td>
							<td>'.$monthly_salary.'</td>
							<td>'.$salary_grade.'</td>
							<td>'.$step.'</td>
							<td>'.$status_of_appointment.'</td>
							</tr>
						';
						
					}
				}		
			}
		}
		echo '
			<tr>
				<td>Active</td>
				<td>'.$active.'</td>
				<td>Inactive</td>
				<td>'.$inactive.'</td>
			</tr>
			';
		echo '</table>';
	} else if ($test == "3") {
		$rs = SelectEach("employees","WHERE (Inactive != 1 OR Inactive IS NULL)");
		if ($rs) {
			while ($row = mysqli_fetch_assoc($rs)) {
				$emprefid = $row["RefId"];
				$pms_empinfo = sync_pms($emprefid,2);
				$pms_salaryinfo = sync_pms($emprefid,3);
				if ($pms_empinfo != "") {
					echo "Error in Updating PMS Emp Info -> $pms_empinfo.<br>";
				} else {
					echo "Pms Emp Info Updated.<br>";
				}
				if ($pms_salaryinfo != "") {
					echo "Error in Updating PMS Salary Info -> $pms_salaryinfo.<br>";
				} else {
					echo "Pms Salary Info Updated.<br>";
				}
			}
		}
	} else if ($test == "4") {
		$rs = SelectEach("employees"," LIMIT 5");
		while ($row = mysqli_fetch_assoc($rs)) {
			$data["array"] = $row;
			echo "<pre>";
			echo json_encode(array_values($data["array"]),JSON_PRETTY_PRINT);
			echo "</pre>";
		}
		
	} else if ($test == "5") {
		function get_empid() {
			include 'conn.e2e.php';
			$sql = "SELECT AgencyId FROM employees WHERE AgencyId LIKE 'P20%' ORDER BY AgencyId DESC LIMIT 1";
			$rs = mysqli_query($conn,$sql);
			if ($rs) {
				while ($row = mysqli_fetch_assoc($rs)) {
					$empid = $row["AgencyId"];
					$empid_arr = explode("-", $empid);
					if (strlen($empid_arr[1]) > 3) {
						return $empid_arr[0]."-0".($empid_arr[1] + 1);
					} else {
						return $empid_arr[0]."-".($empid_arr[1] + 1);
					}
					
				}
			}	
		}
		//echo get_empid();
		
	} else if ($test == "6") {
		echo $_SERVER["DOCUMENT_ROOT"] . "/repo/hris/min/e2e/mdb/sample.mdb";
		/*$dbName = $_SERVER["DOCUMENT_ROOT"] . "/repo/hris/min/e2e/mdb/sample.mdb";
	   	$driver = 'MDBTools';
	   	$dataSourceName = "odbc:Driver=$driver;DBQ=$dbName;Uid=;Pwd=;";
	   	$connection = new PDO($dataSourceName);
	   	$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	   	var_dump($connection);*/
	   	
	   	/*$mdbFilename = $_SERVER["DOCUMENT_ROOT"] . "/repo/hris/min/e2e/mdb/sample.mdb";
	   	$user = "";
	   	$password = "";
	   	$connection = odbc_connect("Driver={Microsoft Access Driver (*.mdb)};Dbq=$mdbFilename", $user, $password);
	   	var_dump($connection);*/

	   	/*$db = $_SERVER["DOCUMENT_ROOT"] . "/repo/hris/min/e2e/mdb/sample.mdb";
		$connection = new COM('ADODB.Connection');
		$connection->Open("DRIVER={Driver do Microsoft Access (*.mdb)}; DBQ=$db");
		var_dump($connection);*/

	} else if ($test == "7") {
		$mconn = mysqli_connect("localhost","root","","mirdc_old");
		$sql = "SELECT * FROM tblservicerecord";
		$rs = mysqli_query($mconn,$sql);
		if ($rs) {
			$str = "";
			while ($row = mysqli_fetch_assoc($rs)) {
				$empid = $row["empNumber"];
				$start_date = $row["serviceFromDate"];
				$end_date = $row["serviceToDate"];
				$plantilla = $row["positionCode"];
				$position = $row["positionDesc"];
				$salary = $row["salary"];
				$agency = $row["stationAgency"];
				$trigger = $row["governService"];
				$separation = $row["separationDate"];
				$remarks = $row["remarks"];
				$lwop = $row["lwop"];
				$empstatus = $row["appointmentCode"];
				$check = "SELECT `appointmentDesc` FROM tblappointment WHERE appointmentCode = '$empstatus'";
				$chech_rs = mysqli_query($mconn,$check);
				if ($chech_rs) {
					$check_row = mysqli_fetch_assoc($chech_rs);
					$empstatus = $check_row["appointmentDesc"];
				}
				$division = $row["branch"];
				$trigger = strtolower($trigger);
				$str .= $trigger."|".$empid."|".$start_date."|".$end_date."|".$plantilla."|".$position."|".$salary."|".$agency;
				$str .= $separation."|".$remarks."|".$lwop."|".$empstatus."|".$division;
				echo $division."<br>";
			}
			$myfile = fopen("upl/csv/service_record_21.txt", "a") or die("Unable to open file!");
			fwrite($myfile,"\n".$str);
			fclose($myfile);
		}

	} else if ($test == "8") {

		//header("Content-type: application/vnd.ms-word");
		//header("Content-Disposition: attachment;Filename=document_name.doc");

		echo "<html>";
		echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">";
		echo "<body>";
		echo "<b>My first document</b>";
		echo "</body>";
		echo "</html>";
	} else if ($test == "9") {
		$rs = SelectEach("employees","ORDER BY AgencyId");
		if ($rs) {
			while ($row = mysqli_fetch_assoc($rs)) {
				$AgencyId 	= $row["AgencyId"];
				$FirstName 	= $row["FirstName"];
				$LastName 	= $row["LastName"];
				$MiddleName = $row["MiddleName"];
				$str = $AgencyId.",".$LastName.",".$FirstName.",".$MiddleName."<br>";
				echo $str;
			}
		}
	} else if ($test == "10") {
		$emprefid = "123";
		$from 		= "2018-10-29";
		$to 		= "2018-10-29";
		echo count_leave($emprefid,$from,$to);
	} else if ($test == "11") {
		include 'conn.e2e.php';
		mysqli_query($conn,"TRUNCATE pms_employees");
		mysqli_query($conn,"TRUNCATE pms_employee_information");
		mysqli_query($conn,"TRUNCATE pms_salaryinfo");
		$Sync     = file_get_contents("upl/json/pis_pms_sync_21.json");
        $Sync_arr = json_decode($Sync, true);
		$rs = SelectEach("employees","WHERE (Inactive = '0' OR Inactive IS NULL)");
		if ($rs) {
			while ($row = mysqli_fetch_assoc($rs)) {
				$pms_employees_Fld 	= "active, ";
				$pms_employees_Val 	= "1, ";
				$pms_employee_information_Fld = "";
				$pms_employee_information_Val = "";
				$pms_salaryinfo_Fld = "";
				$pms_salaryinfo_Val = "";
				$emprefid 			= $row["RefId"];
				foreach ($Sync_arr["pms_employees"] as $key => $value) {
					if ($row[$key] != "") {
						$pms_employees_Fld .= "`".$value."`,";
						$pms_employees_Val .= "'".realEscape($row[$key])."',";
					}
				}
				$empinfo_row = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
				if ($empinfo_row) {
					foreach ($Sync_arr["pms_employee_information"] as $nkey => $nvalue) {
						if ($empinfo_row[$nkey] != "") {
							$pms_employee_information_Fld .= "`".$nvalue."`,";
							$pms_employee_information_Val .= "'".$empinfo_row[$nkey]."',";
						}
					}
					foreach ($Sync_arr["pms_salaryinfo"] as $nkey => $nvalue) {
						if ($empinfo_row[$nkey] != "") {
							$pms_salaryinfo_Fld .= "`".$nvalue."`,";
							$pms_salaryinfo_Val .= "'".$empinfo_row[$nkey]."',";
						}
					}	
				}
				$save_pms_employee = savePMS("pms_employees",$pms_employees_Fld,$pms_employees_Val);
				if (is_numeric($save_pms_employee)) {
					echo "Successfully Save in PMS Employee.<br>";
					if ($pms_employee_information_Fld != "") {
						$pms_employee_information_Fld .= "`employee_id`,";
						$pms_employee_information_Val .= "'".$save_pms_employee."',";
						$save_pms_employee_information = savePMS("pms_employee_information",$pms_employee_information_Fld,$pms_employee_information_Val);
						if (is_numeric($save_pms_employee_information)) {
							echo "Successfully Save in PMS Employee Information.<br>";
						} else {
							echo "Error Saving in PMS Employee Information.<br>";
						}
					}
					if ($pms_salaryinfo_Fld != "") {
						$pms_salaryinfo_Fld .= "`employee_id`,";
						$pms_salaryinfo_Val .= "'".$save_pms_employee."',";
						$save_pms_salaryinfo = savePMS("pms_salaryinfo",$pms_salaryinfo_Fld,$pms_salaryinfo_Val);
						if (is_numeric($save_pms_salaryinfo)) {
							echo "Successfully Save in PMS Salary Info.<br>";
						} else {
							echo "Error Saving in PMS Salary Info.<br>";
						}
					}
				}
				
			}
		}
	} else if ($test == "12") {
		$rs = SelectEach("employees","");
		$count = 0;
		if ($rs) {
			while ($row = mysqli_fetch_assoc($rs)) {
				$emprefid = $row["RefId"];
				$FirstName = $row["FirstName"];
				$LastName = $row["LastName"];
				$AgencyId = $row["AgencyId"];
				$empinfo = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
				if ($empinfo) {
					
					$AssumptionDate = $empinfo["AssumptionDate"];
					if ($AssumptionDate == "") {
						$count++;
						echo $count.". ".$FirstName." ".$LastName.".<br>";
					}
				}
			}
		}
	} else if ($test == "13") {
		$obj = fopen("upl/csv/3rd_tranche.csv", "r");
		echo '{<br>';
		while(!feof($obj)) {
			$str = fgets($obj);
			if ($str != "") {
				$row = explode(",", $str);	
				
					echo '"'.$row[0].'": {<br>';
					echo '"1":"'.$row[1].'",<br>';
					echo '"2":"'.$row[2].'",<br>';
					echo '"3":"'.$row[3].'",<br>';
					echo '"4":"'.$row[4].'",<br>';
					echo '"5":"'.$row[5].'",<br>';
					echo '"6":"'.$row[6].'",<br>';
					echo '"7":"'.$row[7].'",<br>';
					echo '"8":"'.$row[8].'",<br>';
					echo '},<br>';
				
			}
		}
		echo '},<br>';
	} else if ($test == "14") {
		include 'conn.e2e.php';
		$rs = SelectEach("employees","");
		if ($rs) {
			while ($row = mysqli_fetch_assoc($rs)) {
				$refid = $row["RefId"];
				$AgencyId = $row["AgencyId"];
				$update = "UPDATE employees SET BiometricsID = '$AgencyId' WHERE RefId = '$refid'";
				$update_rs = mysqli_query($conn,$update);
				
			}
		}
	} else if ($test == 15) {
		include 'conn.e2e.php';
		$rs = SelectEach("employees","");
		if ($rs) {
			while ($row = mysqli_fetch_assoc($rs)) {
				$emprefid = $row["RefId"];
				$empid = $row["AgencyId"];
				$sql = "UPDATE employees SET UserName = '$empid' WHERE RefId = '$emprefid'";
				$result = mysqli_query($conn,$sql);
				if ($result) {
					echo "Updated Login of ".$row["LastName"]." ".$row["FirstName"]."<br>";
				}
			}
		}
	} else if ($test == 16) {
		$arr_month =[
          "January",
          "February",
          "March",
          "April",
          "May",
          "June",
          "July",
          "August",
          "September",
          "October",
          "November",
          "December"
        ];
		$emprefid 	= 30;
		$curr_month = date("m",time());
		$year  		= date("Y",time());
		echo '<table border="1" width="100%">';
		echo '
			<thead>
	            <tr align="center">
	                <th rowspan="2" style="width: 15%;">Period Covered</th>
	                <th rowspan="2" style="width: 15%;">Particular</th>
	                <th colspan="4">Vacation Leave</th>
	                <th colspan="4">Sick Leave</th>
	                <th rowspan="2">Remarks</th>
	            </tr>
	            <tr align="center">
	                <th>Earned</th>
	                <th>Absences<br>Undertime<br>With Pay</th>
	                <th>Balance</th>
	                <th>Absences<br>Undertime<br>W/O Pay</th>

	                <th>Earned</th>
	                <th>Absences<br>Undertime<br>With Pay</th>
	                <th>Balance</th>
	                <th>Absences<br>Undertime<br>W/O Pay</th>
	            </tr>
	        </thead>
	        <tbody>
		';
		$where_credits 	= "WHERE EmployeesRefId = '$emprefid' AND EffectivityYear = '$year'";
		$rsCredits 		= SelectEach("employeescreditbalance",$where_credits);
		if ($rsCredits) {
			while ($credits_row = mysqli_fetch_assoc($rsCredits)) {
				$balance 		= $credits_row["BeginningBalance"];
				$credit 		= $credits_row["NameCredits"];
				$credits_date 	= $credits_row["BegBalAsOfDate"];
				switch ($credit) {
					case 'VL':
						${"VL_".$emprefid} 	= $balance;
						$as_of 				= date("F d, Y",strtotime($credits_date));
						break;
					case 'SL':
						${"SL_".$emprefid} = $balance;
						break;
				}
			}
		} else {
			$as_of 				= date("F 01, Y",time());
			${"SL_".$emprefid} 	= 0;
			${"FL_".$emprefid} 	= 0;
			${"VL_".$emprefid} 	= 0;
			${"SPL_".$emprefid} = 0;
		}
		echo '<tr>';
			echo '<td>'.$as_of.'</td>';
			echo '<td>Balance Previous Year</td>';
			echo '<td></td>';
			echo '<td></td>';
			echo '<td>'.${"VL_".$emprefid}.'</td>';
			echo '<td></td>';
			echo '<td></td>';
			echo '<td></td>';
			echo '<td>'.${"SL_".$emprefid}.'</td>';
			echo '<td></td>';
			echo '<td></td>';
		echo '</tr>';
		for ($a=1; $a <= intval($curr_month); $a++) { 
			if ($a <= 9) {
				$selected_month = "0".$a;	
			} else {
				$selected_month = $a;
			}
			echo '<tr>';
				echo '<td>'.$arr_month[$a - 1].'</td>';
				echo '<td></td>';
				echo '<td></td>';
				echo '<td></td>';
				echo '<td>'.${"VL_".$emprefid}.'</td>';
				echo '<td></td>';
				echo '<td></td>';
				echo '<td></td>';
				echo '<td>'.${"SL_".$emprefid}.'</td>';
				echo '<td></td>';
				echo '<td></td>';
			echo '</tr>';



			${"data_arr".$emprefid."_".$a} = array();
			$where = "where EmployeesRefId = ".$emprefid." AND Status = 'Approved'";
            $where .= " AND MONTH(ApplicationDateFrom) = '$selected_month'";
            $where .= " AND YEAR(ApplicationDateFrom) = '$year'";
            $where .= " ORDER BY ApplicationDateFrom";
            $rsLeave = SelectEach("employeesleave",$where);
            if ($rsLeave) {
            	while ($leave_row = mysqli_fetch_assoc($rsLeave)) {
            		$leave_from = $leave_row["ApplicationDateFrom"];
            		$leave_to 	= $leave_row["ApplicationDateTo"];
            		$dfrom   	= date("d",strtotime($leave_from));
                  	$dto     	= date("d",strtotime($leave_to));
                  	$type    	= getRecord("leaves",$leave_row["LeavesRefId"],"Code");
                  	$leave_day  = count_leave($emprefid,$leave_from,$leave_to);
                  	${"data_arr".$emprefid."_".$a}[strtotime($leave_to)] = [
                  		"From"=>$leave_from,
                  		"To"=>$leave_to,
                  		"Code"=>$type,
                  		"Count"=>$leave_day
                  	];
            	}
            }
            $rsCTO 	= SelectEach("employeescto",$where);
            if ($rsCTO) {
            	while ($cto_row = mysqli_fetch_assoc($rsCTO)) {
            		$cto_from 	= $cto_row["ApplicationDateFrom"];
            		$cto_to 	= $cto_row["ApplicationDateTo"];
            		$cto_dfrom  = date("d",strtotime($cto_from));
                  	$cto_dto    = date("d",strtotime($cto_to));
                  	$cto_day 	= count_leave($emprefid,$cto_from,$cto_to);
            		$cto_hrs 	= $cto_row["Hours"] * $cto_day;
            		${"data_arr".$emprefid."_".$a}[strtotime($cto_to)] = [
                  		"From"=>$cto_from,
                  		"To"=>$cto_to,
                  		"Code"=>"CTO",
                  		"Count"=>$cto_hrs
                  	];
            	}
            }
            asort(${"data_arr".$emprefid."_".$a});
			foreach (${"data_arr".$emprefid."_".$a} as $key => $data) {
				$From 	= $data["From"];
				$To 	= $data["To"];
				$Type 	= $data["Code"];
				$Count 	= $data["Count"];
				if ($From == $To) {
					$covered_date = date("M d, Y",strtotime($To));
					if ($Type == "CTO") {
						$particular = "( ".$Count." Hours)";
					} else {
						$particular = "( ".$Count." day)";
					}
					
				} else {
					$dummy_from    	= date("d",strtotime($From));
					$dummy_to      	= date("d",strtotime($To));
					$dummy_month   	= date("M",strtotime($From));
					$dummy_year    	= date("Y",strtotime($To));
					$covered_date  	= $dummy_month." ".$dummy_from."-".$dummy_to.", ".$year;
					if ($Type == "CTO") {
						$particular = "( ".$Count." Hours)";
					} else {
						$particular = "( ".$Count." days)";
					}
				}
				$particular = $Type." ".$particular;
				if ($Type == "VL" || $Type == "SL") $Count = $Count * 1.25;
				
				if ($Type == "VL") {
					echo '<tr>';
						echo '<td>'.$covered_date.'</td>';
						echo '<td>'.$particular.'</td>';
						echo '<td></td>';
						echo '<td>'.$Count.'</td>';
						${"VL_".$emprefid} -= $Count;
						echo '<td>'.${"VL_".$emprefid}.'</td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td>'.$particular.'</td>';
					echo '</tr>';
				} else if ($Type == "SL") {
					echo '<tr>';
						echo '<td>'.$covered_date.'</td>';
						echo '<td>'.$particular.'</td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td>'.$Count.'</td>';
						${"SL_".$emprefid} -= $Count;
						echo '<td>'.${"SL_".$emprefid}.'</td>';
						echo '<td></td>';
						echo '<td>'.$particular.'</td>';
					echo '</tr>';
				} else {
					echo '<tr>';
						echo '<td>'.$covered_date.'</td>';
						echo '<td>'.$particular.'</td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td>'.$particular.'</td>';
					echo '</tr>';
				}
			}
			$where_dtr  = "WHERE EmployeesRefId = $emprefid AND Month = '$selected_month' AND Year = '$year'";
			$arr_empDTR = FindFirst("dtr_process",$where_dtr,"*");
			if ($arr_empDTR) {
				$Tardy_Deduction_EQ 	= $arr_empDTR["Tardy_Deduction_EQ"];
				$Undertime_Deduction_EQ = $arr_empDTR["Undertime_Deduction_EQ"];
				$Total_Absent_EQ 		= $arr_empDTR["Total_Absent_EQ"];
				$Total_Tardy_Hr 		= $arr_empDTR["Total_Tardy_Hr"];
				$Total_Undertime_Hr 	= $arr_empDTR["Total_Undertime_Hr"];
				$Total_Absent_Count 	= $arr_empDTR["Total_Absent_Count"];
				$VL_Earned 				= $arr_empDTR["VL_Earned"];
				$SL_Earned 				= $arr_empDTR["SL_Earned"];
				$UT_Tardy 				= $Total_Undertime_Hr + $Total_Tardy_Hr;
				if ($UT_Tardy > 0) {
					if ($Total_Undertime_Hr > $Total_Tardy_Hr) {
						$Deduct 			= $Undertime_Deduction_EQ;
						$Deduction_notes	= "UT ".convertToHoursMins($Total_Undertime_Hr);
					} else {
						$Deduct 			= $Tardy_Deduction_EQ;
						$Deduction_notes	= "Tardy ".convertToHoursMins($Total_Tardy_Hr);
					}
					echo '<tr>';
						echo '<td></td>';
						echo '<td>'.$Deduction_notes.'</td>';
						echo '<td></td>';
						echo '<td>'.$Deduct.'</td>';
						${"VL_".$emprefid} -= $Deduct;
						echo '<td>'.${"VL_".$emprefid}.'</td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td></td>';
					echo '</tr>';
				}
				echo '<tr>';
					echo '<td></td>';
					echo '<td></td>';
					echo '<td>'.$VL_Earned.'</td>';
					echo '<td></td>';
					${"VL_".$emprefid} += $VL_Earned;
					echo '<td>'.${"VL_".$emprefid}.'</td>';
					echo '<td></td>';
					echo '<td>'.$SL_Earned.'</td>';
					echo '<td></td>';
					${"SL_".$emprefid} += $SL_Earned;
					echo '<td>'.${"SL_".$emprefid}.'</td>';
					echo '<td></td>';
					echo '<td></td>';
				echo '</tr>';
				echo '<tr><td colspan="11" style="background: gray;">&nbsp;</td></tr>';

			}
		}
		echo '</tbody>
			</table>';
	} else if ($test == "17") {
		$rs = SelectEach("employees","WHERE Inactive != 1 OR Inactive IS NULL ORDER BY LastName");
		if ($rs) {
			while ($row = mysqli_fetch_assoc($rs)) {
				$AgencyId 	= $row["AgencyId"];
				$LastName 	= $row["LastName"];
				$FirstName 	= $row["FirstName"];
				$MiddleName = $row["MiddleName"];
				echo $AgencyId.",".$LastName.",".$FirstName.",".$MiddleName."<br>";
			}
		}
	} else if ($test == "18") {
		$sql = "SELECT * FROM pms_employees WHERE active = 0";
		$rs = mysqli_query($conn,$sql);
		if ($rs) {
			$count = 0;
			while ($row = mysqli_fetch_assoc($rs)) {
				$count++;
				$lastname = $row["lastname"];
				$firstname = $row["firstname"];
				echo $count.". ".$lastname."., ".$firstname."<br>";
			}
		}
	} else if ($test == "19") {
		$lwop = "3.35";
		$where = "WHERE NoOfDaysLeaveWOP <= '$lwop'";
		$lwop_earnings = FindLast("leavecreditsearnedwopay",$where,"*");
		$earnings = $lwop_earnings["LeaveCreditsEarned"];
		$lwop_eq = $lwop_earnings["NoOfDaysLeaveWOP"];
		if ($lwop > $earnings) {
			$borrow = $lwop - $lwop_eq;
		} else {
			$borrow = 0;
		}
		echo $borrow;
		
	} else if ($test == "20") {
		$count = 0;
		$rs = SelectEach("employeesmovement","");
		if ($rs) {
			while ($row = mysqli_fetch_assoc($rs)) {
				$refid 				= $row["RefId"];
				$ApptStatusRefId 	= $row["ApptStatusRefId"];
				$EmpStatusRefId 	= $row["EmpStatusRefId"];
				$Appt 				= getRecord("apptstatus",$ApptStatusRefId,"Name");
				$Emp 				= getRecord("Empstatus",$EmpStatusRefId,"Name");
				switch (strtolower($Appt)) {
					case 'permanent':
						$New_EmpStatus = 2;
						break;
					case 'contractual':
						$New_EmpStatus = 3;
						break;
					default:
						$New_EmpStatus = 0;
						break;
				}
				if ($New_EmpStatus > 0) {
					$fldnval = "EmpStatusRefId = '$New_EmpStatus',";
					$result  = f_SaveRecord("EDITSAVE","employeesmovement",$fldnval,$refid,"SYSTEM");
					if ($result != "") {
						echo "Error: ".$refid;
						return false;
					} else {
						$count++;
						echo $count.". Updated Record.<br>"; 
					}
				}
			}
		}
	} else if ($test == 21) {
		// We'll be outputting an excel file
		header('Content-type: application/vnd.ms-excel');

		// It will be called file.xls
		header('Content-Disposition: attachment; filename="file.xls"');
	} else if ($test == 22) {
		$arr_loans = array();
		$gsis_rowspan = 0;
		$pagibig_rowspan = 0;
		$gsis_td = "";
		$pagibig_td = "";
		echo '<table width="100%" border=1>';
		echo '<tr>';
			echo '<td rowspan="2">Employee Name</td>';
			$trn_group 	= pms_SelectEach("pms_loaninfo_transactions","WHERE month = '3' AND year = '2019' GROUP BY loan_id");
			if ($trn_group) {
				while ($trn_row = mysqli_fetch_assoc($trn_group)) {
					$loan_id = $trn_row["loan_id"];
					$loans_row 	 = pms_FindFirst("pms_loans","WHERE id = '$loan_id'","*");
					$loan_type 		= $loans_row["loan_type"];
					$loan_name 		= $loans_row["name"];
					if ($loan_type == "GSIS") {
						$gsis_td .= "<td align='center'>".$loan_name."</td>";
						$gsis_rowspan++;
					} else {
						$pagibig_td .= "<td align='center'>".$loan_name."</td>";
						$pagibig_rowspan++;
					}
					$arr_loans[$loan_id."_id"] = $loan_id;
					//subtotal
					${$loan_id."_pagibig_loan"} = 0;
					${$loan_id."_gsis_loan"} = 0;
				}
			}
			if ($gsis_rowspan > 0) {
				echo '<td colspan="'.$gsis_rowspan.'" align="center">GSIS</td>';	
			}
			if ($pagibig_rowspan > 0) {
				echo '<td colspan="'.$pagibig_rowspan.'" align="center">PAGIBIG</td>';	
			}
		echo '</tr>';
		echo '<tr>';
		if ($gsis_rowspan > 0) {
			echo $gsis_td;
		}
		if ($pagibig_rowspan > 0) {
			echo $pagibig_td;
		}
		echo '</tr>';
		$dummy_office 	= "";
		$loan_count 	= 1;
		$where_trn 		= "WHERE month = '3' AND year = '2019'";
		$trn_rs 		= pms_SelectEach("pms_transactions",$where_trn." ORDER BY office_id");
		if ($trn_rs) {
			while ($trn_row = mysqli_fetch_assoc($trn_rs)) {
				$emprefid 			= $trn_row["employee_id"];
				$office_id 			= $trn_row["office_id"];
				$position_id 		= $trn_row["position_id"];
				$where_benefit_trn 	= "WHERE employee_id = '$emprefid'";
				$where_benefit_trn 	.= " AND month = '3' AND year = '2019'";
				$office_name 		= pms_FindFirst("office","WHERE RefId = '$office_id'","Name");
				$emp_row 			= pms_FindFirst("pms_employees","WHERE id = '$emprefid'","*");


				if ($dummy_office == "") {
					echo '
						<tr>
							<td style="color: red;" colspan="'.(1 + ($gsis_rowspan + $pagibig_rowspan)).'">'.$office_name.'<br><br></td>
						</tr>
					';	
					$dummy_office = $office_id;
				} else {
					if ($dummy_office != $office_id) $dummy_office = ""; 
				}
				if ($emp_row) {
					$lastname 	= $emp_row["lastname"];
					$middlename = $emp_row["middlename"];
					$firstname	= $emp_row["firstname"];
					$fullname 	= $lastname.", ".$firstname;

					echo '<tr>';
					echo '<td>'.$fullname.'</td>';
					/*LOAN TRANSACTION*/
					foreach ($arr_loans as $key => $value) {
						$loan_amount = pms_FindFirst("pms_loaninfo_transactions",$where_benefit_trn." AND loan_id = '$value'","amount");
						if ($loan_amount) {
							${$value."_pagibig_loan"} += $loan_amount;
							echo '<td align="right">'.$loan_amount.'</td>';
						} else {
							echo '<td align="right"></td>';
						}
					}
					echo '</tr>';
				}
				if ($dummy_office != $office_id) {
					echo '<tr>';
					echo '<td style="color: blue;"><br>SUBTOTAL<br></td>';
					foreach ($arr_loans as $akey => $avalue) {
						echo '<td align="right">'.${$avalue."_pagibig_loan"}.'</td>';
					}
					echo '</tr>';
					$loan_rs 	= pms_SelectEach("pms_loans","");
					if ($loan_rs) {
						while ($loan_row = mysqli_fetch_assoc($loan_rs)) {
							${$loan_row["id"]."_pagibig_loan"} = 0;
							${$loan_row["id"]."_gsis_loan"} = 0;
						}
					}
				}
				
				// /*BENEFIT TRANSACTION*/
				// $benefit_trn_row 	= pms_SelectEach("pms_benefitinfo_transactions",
				// 									$where_benefit_trn,
				// 									"`benefit_id`, `amount`");
				// if ($benefit_trn_row) {
				// 	$benefit_id 	= $benefit_trn_row["benefit_id"];
				// 	$benefit_amount = $benefit_trn_row["amount"];
				// }

				

				// /*DEDUCTION TRANSACTION*/
				// $deduction_trn_row 		= pms_SelectEach("pms_deductioninfo_transactions",
				// 									$where_benefit_trn,
				// 									"`deduction_id`, `amount`");
				// if ($deduction_trn_row) {
				// 	$deduction_id 		= $deduction_trn_row["deduction_id"];
				// 	$deduction_amount 	= $deduction_trn_row["amount"];
				// }

			}
		}
		echo '</table>';
	} else if ($test == 23) {
		$start = "01";
		$end = "12";
		$coc_year = "2019";
		$year = "2019";
		$emprefid = 9;

		$vl       = 0;
        $sl       = 0;
        $ot       = 0;
        $spl      = 0;
        $fl       = 0;
        $new_vl   = 0;
        $new_sl   = 0;
        $lwop     = 0;

        $start    = intval($start);
        $end      = intval($end);
        if ($start <= 9) $start = "0".$start;
        if ($end <= 9) $end = "0".$end;

        $coc_where  = "WHERE EmployeesRefId = '$emprefid' AND EffectivityYear = '$coc_year' AND NameCredits = 'OT'";
        $COC        = FindFirst("employeescreditbalance",$coc_where,"BeginningBalance");
        if ($COC) {
           $ot = $COC;
        }
        $balance    = SelectEach("employeescreditbalance","WHERE EmployeesRefId = '$emprefid' AND EffectivityYear = '$year'");
        if ($balance) {
           while ($row_balance = mysqli_fetch_assoc($balance)) {
              $credit_name = $row_balance["NameCredits"];
              switch ($credit_name) {
                 case 'VL':
                    $vl = $row_balance["BeginningBalance"];
                    $fl = $row_balance["ForceLeave"];
                    break;
                 case 'SL':
                    $sl = $row_balance["BeginningBalance"];
                    break;
                 case 'SPL':
                    $spl = $row_balance["BeginningBalance"];
                    break;
              }
           }
        }
        echo $vl." -> ".$sl."<br>";
        if ($start <= $end) {
           $where = "WHERE EmployeesRefId = '$emprefid' AND Month >= '$start' AND Year = '$year' AND Month <= '$end' ORDER BY Month, Year";
           $rs = SelectEach("dtr_process",$where);
           if ($rs) {
              while ($row = mysqli_fetch_assoc($rs)) {
                 $where_monetization = "WHERE EmployeesRefId = '$emprefid'";
                 $where_monetization .= " AND MONTH(FiledDate) = '".$row["Month"]."'";
                 $where_monetization .= " AND YEAR(FiledDate) = '".$year."'";
                 $where_monetization .= " AND Status = 'Approved'";
                 $monetize   = FindFirst("employeesleavemonetization",$where_monetization,"*");
                 if ($monetize) {
                    $vl_monetize = $monetize["VLValue"];
                    $sl_monetize = $monetize["SLValue"];
                 } else {
                    $vl_monetize = $sl_monetize = 0;
                 }

                 $coc        = $row["Total_COC_Hr"];
                 $absent     = $row["Total_Absent_Count"];
                 $vl_earned  = $row["VL_Earned"];
                 $sl_earned  = $row["SL_Earned"];
                 $spl_used   = $row["SPL_Used"];
                 $coc_used   = $row["COC_Used"];
                 $tardy      = $row["Tardy_Deduction_EQ"];
                 $undertime  = $row["Undertime_Deduction_EQ"];
                 $vl_used    = $row["VL_Used"];
                 $sl_used    = $row["SL_Used"];

                 $ot         = $ot + $coc - $coc_used;
                 $spl        = $spl - $spl_used;
                 $deduction  = $tardy + $undertime + $absent + $vl_used + $vl_monetize;
                 
                 if ($new_vl == 0) {
                    $new_vl = $vl - $deduction;
                 } else {
                    $new_vl = $new_vl - $deduction;
                 }
                 if ($new_vl < 0) {
                    $lwop = abs($new_vl);
                    $new_vl = 0;
                 }
                 

                 if ($new_sl == 0) {
                    $new_sl = $sl - ($sl_used + $sl_monetize);
                 } else {
                    $new_sl = $new_sl - ($sl_used + $sl_monetize);
                 }

                 $new_vl += $vl_earned;
                 $new_sl += $sl_earned;
              }
           } else {
              $new_vl = $vl;
              $new_sl = $sl;
           }
        } else {
           $new_vl = $vl;
           $new_sl = $sl;
        }

        
        $arr["VL"]  = $new_vl;
        $arr["SL"]  = $new_sl;
        $arr["OT"]  = $ot;
        $arr["SPL"] = $spl;
        $arr["FL"]  = $fl;
        echo $new_vl." -> $new_sl -> $ot -> $spl -> $fl";
	} else if ($test == 24) {
		$img_loc = "../../../public/images/35/EmpDocument/PIC_30.png";
		$new_loc = "../../../public/images/35/EmployeesPhoto/PIC_30.png";
		copy($img_loc, $new_loc);
	} else if ($test == 25) {
		$sql = "SELECT AgencyId FROM employees ORDER BY AgencyId DESC LIMIT 1";
        $rs = mysqli_query($conn,$sql);
        if ($rs) {
           	while ($row = mysqli_fetch_assoc($rs)) {
              	$empid = $row["AgencyId"];
              	$count = substr($empid, 4) + 1;
              	$count = str_pad($count, 4, '0',STR_PAD_LEFT);
              	$new_empid = date("Y",time()).$count;
              	return $new_empid;
           	}
        }  
	} else if ($test == 26) {
		$str = "";
		for($d=1;$d<=31;$d++) {
			$y = "2019-04-".$d;
			$day = date("D",strtotime($y));	
			if ($day == "Sat" || $day == "Sun") {
				$day = "<span style='color:red;'>".$day."</span>";
				$str .= "."."day_".$d.", ";
			}
		}
		echo '
		<style type="css">
			'.$str.'.day_32 {
				background:gray;
			}
		</style>
		';
	} else if ($test == 27) {
		$third     	= file_get_contents(json."third_tranche.json");
		$second 	= file_get_contents(json."second_tranche.json");
		$third_arr  = json_decode($third, true);
		$second_arr = json_decode($second, true);
		$error 		= json_last_error_msg();
		$third_rate 	= $third_arr["7"]["1"];
		$second_rate 	= $second_arr["7"]["1"];
		//echo $second_rate." -> ".($third_rate - $second_rate)." -> 7-1<br>";
		echo "UPDATE pms_salaryinfo SET basic_amount_one = '$second_rate', basic_amount_two = '".($third_rate - $second_rate)."' WHERE salarygrade_id = '7' AND step_inc = 1;<br>";
		$third_rate 	= $third_arr["9"]["1"];
		$second_rate 	= $second_arr["9"]["1"];
		//echo $second_rate." -> ".($third_rate - $second_rate)." -> 9-1<br>";
		echo "UPDATE pms_salaryinfo SET basic_amount_one = '$second_rate', basic_amount_two = '".($third_rate - $second_rate)."' WHERE salarygrade_id = '8' AND step_inc = 1;<br>";
		$third_rate 	= $third_arr["11"]["1"];
		$second_rate 	= $second_arr["11"]["1"];
		//echo $second_rate." -> ".($third_rate - $second_rate)." -> 11-1<br>";
		echo "UPDATE pms_salaryinfo SET basic_amount_one = '$second_rate', basic_amount_two = '".($third_rate - $second_rate)."' WHERE salarygrade_id = '10' AND step_inc = 1;<br>";
		$third_rate 	= $third_arr["12"]["1"];
		$second_rate 	= $second_arr["12"]["1"];
		//echo $second_rate." -> ".($third_rate - $second_rate)." -> 12-1<br>";
		echo "UPDATE pms_salaryinfo SET basic_amount_one = '$second_rate', basic_amount_two = '".($third_rate - $second_rate)."' WHERE salarygrade_id = '11' AND step_inc = 1;<br>";
		$third_rate 	= $third_arr["14"]["1"];
		$second_rate 	= $second_arr["14"]["1"];
		//echo $second_rate." -> ".($third_rate - $second_rate)." -> 14-1<br>";
		echo "UPDATE pms_salaryinfo SET basic_amount_one = '$second_rate', basic_amount_two = '".($third_rate - $second_rate)."' WHERE salarygrade_id = '13' AND step_inc = 1;<br>";
		$third_rate 	= $third_arr["15"]["1"];
		$second_rate 	= $second_arr["15"]["1"];
		//echo $second_rate." -> ".($third_rate - $second_rate)." -> 15-1<br>";
		echo "UPDATE pms_salaryinfo SET basic_amount_one = '$second_rate', basic_amount_two = '".($third_rate - $second_rate)."' WHERE salarygrade_id = '14' AND step_inc = 1;<br>";
		$third_rate 	= $third_arr["18"]["1"];
		$second_rate 	= $second_arr["18"]["1"];
		//echo $second_rate." -> ".($third_rate - $second_rate)." -> 18-1<br>";
		echo "UPDATE pms_salaryinfo SET basic_amount_one = '$second_rate', basic_amount_two = '".($third_rate - $second_rate)."' WHERE salarygrade_id = '17' AND step_inc = 1;<br>";
		$third_rate 	= $third_arr["18"]["7"];
		$second_rate 	= $second_arr["18"]["7"];
		//echo $second_rate." -> ".($third_rate - $second_rate)." -> 18-7<br>";
		echo "UPDATE pms_salaryinfo SET basic_amount_one = '$second_rate', basic_amount_two = '".($third_rate - $second_rate)."' WHERE salarygrade_id = '17' AND step_inc = 7;<br>";
		$third_rate 	= $third_arr["19"]["1"];
		$second_rate 	= $second_arr["19"]["1"];
		//echo $second_rate." -> ".($third_rate - $second_rate)." -> 19-1<br>";
		echo "UPDATE pms_salaryinfo SET basic_amount_one = '$second_rate', basic_amount_two = '".($third_rate - $second_rate)."' WHERE salarygrade_id = '18' AND step_inc = 1;<br>";
		$third_rate 	= $third_arr["22"]["1"];
		$second_rate 	= $second_arr["22"]["1"];
		//echo $second_rate." -> ".($third_rate - $second_rate)." -> 22-1<br>";
		echo "UPDATE pms_salaryinfo SET basic_amount_one = '$second_rate', basic_amount_two = '".($third_rate - $second_rate)."' WHERE salarygrade_id = '21' AND step_inc = 1;<br>";
		$third_rate 	= $third_arr["24"]["8"];
		$second_rate 	= $second_arr["24"]["8"];
		//echo $second_rate." -> ".($third_rate - $second_rate)." -> 24-8<br>";
		echo "UPDATE pms_salaryinfo SET basic_amount_one = '$second_rate', basic_amount_two = '".($third_rate - $second_rate)."' WHERE salarygrade_id = '23' AND step_inc = 8;<br>";
		$third_rate 	= $third_arr["8"]["7"];
		$second_rate 	= $second_arr["8"]["7"];
		//echo $second_rate." -> ".($third_rate - $second_rate)." -> 8-7<br>";
		echo "UPDATE pms_salaryinfo SET basic_amount_one = '$second_rate', basic_amount_two = '".($third_rate - $second_rate)."' WHERE salarygrade_id = '33' AND step_inc = 7;<br>";
	} else if ($test == 28) {
		$start 		= 1;
		$end 		= 12;
		$emprefid 	= 66;
		$coc_year   = 2019;
		$year 		= 2019;
        $vl       = 0;
        $sl       = 0;
        $ot       = 0;
        $spl      = 0;
        $fl       = 0;
        $new_vl   = 0;
        $new_sl   = 0;
        $lwop     = 0;

        $start    = intval($start);
        $end      = intval($end);
        if ($start <= 9) $start = "0".$start;
        if ($end <= 9) $end = "0".$end;

        $coc_where  = "WHERE EmployeesRefId = '$emprefid' AND EffectivityYear = '$coc_year' AND NameCredits = 'OT'";
        $COC        = FindFirst("employeescreditbalance",$coc_where,"BeginningBalance");
        if ($COC) {
           $ot = $COC;
        }
        $balance    = SelectEach("employeescreditbalance","WHERE EmployeesRefId = '$emprefid' AND EffectivityYear = '$year'");
        if ($balance) {
           while ($row_balance = mysqli_fetch_assoc($balance)) {
              $credit_name = $row_balance["NameCredits"];
              switch ($credit_name) {
                 case 'VL':
                    $vl = $row_balance["BeginningBalance"];
                    $fl = $row_balance["ForceLeave"];
                    break;
                 case 'SL':
                    $sl = $row_balance["BeginningBalance"];
                    break;
                 case 'SPL':
                    $spl = $row_balance["BeginningBalance"];
                    break;
              }
           }
        }
        if ($start <= $end) {
           $where = "WHERE EmployeesRefId = '$emprefid' AND Month >= '$start' AND Year = '$year' AND Month <= '$end'";
           $rs = SelectEach("dtr_process",$where);
           if ($rs) {
              while ($row = mysqli_fetch_assoc($rs)) {
                 $where_monetization = "WHERE EmployeesRefId = '$emprefid'";
                 $where_monetization .= " AND MONTH(FiledDate) = '".$row["Month"]."'";
                 $where_monetization .= " AND YEAR(FiledDate) = '".$year."'";
                 $where_monetization .= " AND Status = 'Approved'";
                 $monetize   = FindFirst("employeesleavemonetization",$where_monetization,"*");
                 if ($monetize) {
                    $vl_monetize = $monetize["VLValue"];
                    $sl_monetize = $monetize["SLValue"];
                 } else {
                    $vl_monetize = $sl_monetize = 0;
                 }
                 $coc        = $row["Total_COC_Hr"];
                 $absent     = $row["Total_Absent_Count"];
                 $vl_earned  = $row["VL_Earned"];
                 $sl_earned  = $row["SL_Earned"];
                 $spl_used   = $row["SPL_Used"];
                 $coc_used   = $row["COC_Used"];
                 $tardy      = $row["Tardy_Deduction_EQ"];
                 $undertime  = $row["Undertime_Deduction_EQ"];
                 $vl_used    = $row["VL_Used"];
                 $sl_used    = $row["SL_Used"];
                 $fl_days    = $row["FL_Days"];
                 $fl_used    = 0;
                 if ($fl_days != "") {
                    $fl_arr = explode("|", $fl_days);
                    foreach ($fl_arr as $key => $value) {
                       if ($value != "") {
                          $fl_used++;
                       }
                    }
                 }

                 $ot         = $ot + $coc - $coc_used;
                 $spl        = $spl - $spl_used;
                 $deduction  = $tardy + $undertime + $absent + $vl_used + $vl_monetize;
                 echo $deduction." -> ".$row["Month"]."<br>";
                 if ($new_vl == 0) {
                    $new_vl = $vl - $deduction;
                 } else {
                    $new_vl = $new_vl - $deduction;
                 }
                 if ($new_vl < 0) {
                    $lwop = abs($new_vl);
                    $new_vl = 0;
                 }
                

                 if ($new_sl == 0) {
                    $new_sl = $sl - ($sl_used + $sl_monetize);
                 } else {
                    $new_sl = $new_sl - ($sl_used + $sl_monetize);
                 }

                 $new_vl += $vl_earned;
                 $new_sl += $sl_earned;
                 $fl -= $fl_used;
              }
           } else {
              $new_vl = $vl;
              $new_sl = $sl;
           }
        } else {
           $new_vl = $vl;
           $new_sl = $sl;
        }

        
        $arr["VL"]  = $new_vl;
        $arr["SL"]  = $new_sl;
        $arr["OT"]  = $ot;
        $arr["SPL"] = $spl;
        $arr["FL"]  = $fl;
        echo $new_vl." -> VL<br>";
        echo $new_sl." -> SL<br>";
	} else if ($test == 31) {
		$minute = "688";
		$total 	= getEquivalent($minute,"workinghrsconversion");
		$earned = FindFirst("slvlearneddaily","WHERE NoOfDays = '5'","VLEarned");
		echo $earned;
		//echo $total." -> ".$minute;
	} else if ($test == 32) {
		$mailer = email_send('Emman','emmanplacido.e2e@gmail.com','Leave');
	}
?>