<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript">
         $(document).ready(function () {
            remIconDL();
         });
      </script>
      <link rel="stylesheet" href="<?php echo path("js/autocomplete/css/jquery-ui.css"); ?>" type="text/css" />
      <script type="text/javascript" src="<?php echo path("js/autocomplete/jquery-ui.js") ?>"></script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post">
         <?php $sys->SysHdr($sys,"spms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("SPMS Report"); ?>
            <div class="container-fluid margin-top">
               <div class="mypanel">
                  <div class="row">
                     <div class="col-xs-12">
                        <div class="mypanel panel panel-default">
                           <div class="panel-top">
                              SPMS REPORT
                           </div>
                           <div class="panel-mid-litebg">
                              <div class="row">
                                 <div class="col-xs-4">
                                    <label>Period</label>
                                    <select class="form-input" name="period" id="period">
                                       <option value="">Select Period</option>
                                       <option value="January - June">January - June</option>
                                       <option value="July - December">July - December</option>
                                    </select>
                                 </div>
                                 <div class="col-xs-4">
                                    <label class="control-label" for="inputs">Division:</label><br>
                                    <?php
                                       createSelect("Division",
                                                    "sint_DivisionRefId",
                                                    "",100,"Name","Select Division","");
                                    ?>
                                 </div>
                                 <div class="col-xs-4">
                                    <label class="control-label" for="inputs">Department:</label><br>
                                    <?php
                                       createSelect("Department",
                                                    "sint_DepartmentRefId",
                                                    "",100,"Name","Select Department","");
                                    ?>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-4">
                                    <label>Employee Name</label>
                                    <input type="text" class="form-input" name="selected_emp" id="selected_emp">
                                    <input type="hidden" name="emprefid" id="emprefid">
                                 </div>
                                 <div class="col-xs-4">
                                    <label>Year Performed</label>
                                    <select class="form-input saveFields-- mandatory" 
                                           name="YearPerformed"
                                           id="YearPerformed">
                                           <?php
                                             for ($i=(date("Y",time()) - 5); $i <= (date("Y",time()) + 5); $i++) { 
                                                echo '<option value="'.$i.'">'.$i.'</option>';
                                             }
                                           ?>
                                    </select>
                                 </div>
                                 <div class="col-xs-4">
                                    <label class="control-label" for="inputs">Report Type:</label><br>
                                    <select class="form-input" name="report_type" id="report_type">
                                       <option value="spms_list_report">List Report</option>
                                       <option value="spms_bar_graph_report">Graph Report</option>
                                    </select>
                                 </div>
                              </div>
                           </div>
                           <div class="panel-bottom">
                              <button class="btn-cls4-tree" type="button" id="generate_report">GENERATE REPORT</button>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal fade border0" id="modalSPMS" role="dialog">
               <div class="modal-dialog border0" style="padding:0px;width:90%;height:92%;">
                  <div class="mypanel border0" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                              <i class="fa fa-print" aria-hidden="true"></i>
                           </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               doHidden("paramTitle",getvalue("paramTitle"),"");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
   <script type="text/javascript">
      $(document).ready(function () {
         EmployeeAutoComplete("employees","selected_emp");
         $("#selected_emp").blur(function () {
            var value = $(this).val();
            arr = value.split("-");
            $("#emprefid").val(arr[0]);
            $(this).val(arr[1]);
         });
         $("#generate_report").click(function () {
            var file       = $("#report_type").val();
            var period     = $("#period").val();
            var division   = $("#sint_DivisionRefId").val();
            var department = $("#sint_DepartmentRefId").val();
            var year       = $("#YearPerformed").val();
            var url        = "ReportCaller.e2e.php?file=" + file;   
            url += "&" + $("[name='hgParam']").val();
            url += "&ServiceRecordEmpRefId=" + emprefid;
            url += "&period=" + period;
            url += "&division=" + division;
            url += "&department=" + department;
            url += "&year=" + year;
            $("#rptContent").attr("src",url);
            $("#modalSPMS").modal();
         });
      });
   </script>
</html>



