
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript">
         $(document).ready(function () {
            remIconDL();
            $("#LocSAVE").click(function () {
               if (saveProceed() == 0) {
                  $(this).attr("type","submit"); 
               }
            });
            <?php
               if (isset($_GET["errmsg"])) {
                  echo '$.notify("'.$_GET["errmsg"].'");';
               }
            ?>
         });
         
         function afterDelete() {
            alert("Successfully Deleted");
            gotoscrn($("#hProg").val(),"");
         }
      </script>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_AfterTrn") ?>"></script>
   </head>
   <body>
      <form name="xForm" method="post" action="">
         <?php $sys->SysHdr($sys,"ldms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("Learning Action Plan"); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div id="divList">
                        <div class="mypanel">
                           <div class="panel-top">List of Learning Action Plan</div>
                           <div class="panel-mid">
                              <span id="spGridTable">
                                 <?php
                                    $table = "learning_application_plan";
                                    $sizeCol = "col-xs-6";
                                    $gridTableHdr_arr = ["Employees Name", "Course"];
                                    $gridTableFld_arr = ["EmployeesRefId", "LDMSLNDProgramRefId"];
                                    $sql = "SELECT * FROM `learning_application_plan`";
                                    $Action = [false,true,true,true];
                                    doGridTable($table,
                                                $gridTableHdr_arr,
                                                $gridTableFld_arr,
                                                $sql,
                                                $Action,
                                                "gridTable");
                                 ?>
                              </span>
                           </div>
                           <div class="panel-bottom">
                              <?php
                                 btnINRECLO([true,true,false]);
                              ?>
                           </div>
                        </div>
                     </div>
                     <div id="divView">
                        <div class="row" id="EntryScrn">
                           <div class="col-xs-12">
                              <div class="mypanel">
                                 <div class="panel-top">
                                    <span id="ScreenMode">ADD NEW Learning ACTION PLAN
                                 </div>
                                 <div class="panel-mid">
                                    <div class="row">
                                       <div class="col-xs-12">
                                          <div class="row margin-top">
                                             <div class="col-xs-6">
                                                <label>LEARNER:</label>
                                                <select class="form-input saveFields--" name="sint_EmployeesRefId" id="sint_EmployeesRefId">
                                                   <option value="">Select Learner</option>
                                                   <?php
                                                      $rsemp = SelectEach("employees","ORDER BY LastName");
                                                      if ($rsemp) {
                                                         while ($rowemp = mysqli_fetch_assoc($rsemp)) {
                                                            $refid = $rowemp["RefId"];
                                                            $LastName = $rowemp["LastName"];
                                                            $FirstName = $rowemp["FirstName"];
                                                            $MiddleName = $rowemp["MiddleName"];
                                                            echo '<option value="'.$refid.'">'.$LastName.', '.$FirstName.'</option>';
                                                         }
                                                      }
                                                   ?>
                                                </select>
                                             </div>
                                             <div class="col-xs-6">
                                                <label>TITLE OF INTERVENTIONS:</label>
                                                <select class="form-input saveFields--" name="sint_LDMSLNDProgramRefId" id="sint_LDMSLNDProgramRefId">
                                                   <option value="">Select Training</option>
                                                   <?php
                                                      $rs2 = SelectEach("ldmslndprogram","");
                                                      if ($rs) {
                                                         while ($row2 = mysqli_fetch_assoc($rs2)) {
                                                            $refid = $row2["RefId"];
                                                            $title = $row2["Name"];
                                                            echo '<option value="'.$refid.'">'.$title.'</option>';
                                                         }
                                                      }
                                                   ?>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-6">
                                                <label>Specific Competency Targets to Develop/Enhance:</label>
                                                <textarea class="form-input saveFields--" name="char_Objectives" id="char_Objectives" rows="3" style="resize: none;"></textarea>
                                             </div>
                                          </div>
                                          <?php bar(); ?>
                                          <div class="row margin-top">
                                             <div class="col-xs-12">
                                                <table border="1" width="100%" class="table">
                                                   <thead>
                                                      <tr>
                                                         <th style="width: 20%;">
                                                            Learning Goals
                                                         </th>
                                                         <th style="width: 20%;">
                                                            Current Status
                                                         </th>
                                                         <th style="width: 20%;">
                                                            Learning Strategies
                                                         </th>
                                                         <th style="width: 20%;">
                                                            Required Resources
                                                         </th>
                                                         <th style="width: 20%;">
                                                            Key Performance Indicator
                                                         </th>
                                                      </tr>
                                                   </thead>
                                                   <tbody>
                                                      <?php
                                                         for ($i=1; $i <=10 ; $i++) { 
                                                            echo '
                                                               <tr>
                                                                  <td>
                                                                     <input type="text" 
                                                                            class="form-input saveFields--" 
                                                                            name="char_Goals'.$i.'" 
                                                                            id="char_Goals'.$i.'">
                                                                  </td>
                                                                  <td>
                                                                     <input type="text" 
                                                                            class="form-input saveFields--" 
                                                                            name="char_Status'.$i.'" 
                                                                            id="char_Status'.$i.'">
                                                                  </td>
                                                                  <td>
                                                                     <input type="text" 
                                                                            class="form-input saveFields--" 
                                                                            name="char_Strategies'.$i.'" 
                                                                            id="char_Strategies'.$i.'">
                                                                  </td>
                                                                  <td>
                                                                     <input type="text" 
                                                                            class="form-input saveFields--" 
                                                                            name="char_Requirements'.$i.'" 
                                                                            id="char_Requirements'.$i.'">
                                                                  </td>
                                                                  <td>
                                                                     <input type="text" 
                                                                            class="form-input saveFields--" 
                                                                            name="char_Indicator'.$i.'" 
                                                                            id="char_Indicator'.$i.'">
                                                                  </td>
                                                               </tr>
                                                            ';
                                                         }
                                                      ?>
                                                   </tbody>
                                                </table>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-6">
                                                <label class="control-label" for="inputs">Head of Office:</label>
                                                <?php
                                                   createSelect("signatories",
                                                                "sint_Signatory1",
                                                                "",100,"Name","Select Signatory","",false);
                                                ?>
                                             </div>
                                             <div class="col-xs-6">
                                                <label class="control-label" for="inputs">Supervisor:</label>
                                                <?php
                                                   createSelect("signatories",
                                                                "sint_Signatory2",
                                                                "",100,"Name","Select Signatory","",false);
                                                ?>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel-bottom">
                                    <?php
                                       btnSACABA([true,true,true]);
                                    ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
   <script type="text/javascript">
      $(document).ready(function () {

      });
      function afterNewSave() {
         alert("Successfully Saved");
         gotoscrn($("#hProg").val(),"");
      }
      function selectMe(refid){
         $("#rptContent").attr("src","blank.htm");
         var rptFile = "rpt_LAP";
         var url = "ReportCaller.e2e.php?file=" + rptFile;
         url += "&refid=" + refid;
         url += "&" + $("[name='hgParam']").val();
         $("#prnModal").modal();
         $("#rptContent").attr("src",url);
      }
   </script>
</html>