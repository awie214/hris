<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_AfterTrn") ?>"></script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post">
         <?php $sys->SysHdr($sys,"ldms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("Individual Competency Data"); ?>
            <div class="container-fluid margin-top">
               <div class="mypanel">
                  <div class="row" id="divList">
                     <div class="col-xs-12">
                        <div class="mypanel">
                           <div class="panel-top">List of Individual Competency Data</div>
                           <div class="panel-mid">
                              <span id="spGridTable">
                                 <?php
                                    $Action           = [false,false,true,true];
                                    $table            = "employees_assessment";
                                    $sql              = "SELECT * FROM $table";
                                    $gridTableHdr_arr = ["Emp. Name","Position","Year"];
                                    $gridTableFld_arr = ["EmployeesRefId","PositionRefId","Year"];
                                    
                                    doGridTable($table,
                                                $gridTableHdr_arr,
                                                $gridTableFld_arr,
                                                $sql,
                                                $Action,
                                                "gridTable");
                                 ?>
                              </span>
                              
                           </div>
                           <div class="panel-bottom">
                              <?php
                                 btnINRECLO([true,true,false]);
                              ?>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row" id="divView">
                     <div class="col-xs-12" id="div_CONTENT">
                        <div class="margin-top">
                           <div id="">
                              <div class="mypanel panel panel-default">
                                 <div class="panel-top">
                                    <span id="ScreenMode">INDIVIDUAL COMPETENCY DATA
                                 </div>
                                 <div class="panel-mid-litebg" id="EntryScrn">
                                    <div id="EntryScrn">
                                       <div class="row margin-top">
                                          <div class="col-xs-4">
                                             <label class="control-label" for="inputs">Year Conducted:</label>
                                             <select class="form-input saveFields--" name="Year" id="Year">
                                                <option value="">Select Year</option>
                                                <?php
                                                   $from = date("Y",time()) - 5; 
                                                   $to = date("Y",time()) + 5; 
                                                   for ($i=$from; $i <= $to ; $i++) { 
                                                      if (date("Y",time()) == $i) {
                                                         echo '<option value="'.$i.'" selected>'.$i.'</option>';   
                                                      } else {
                                                         echo '<option value="'.$i.'">'.$i.'</option>';
                                                      }
                                                      
                                                   }
                                                ?>
                                             </select>
                                          </div>
                                          <div class="col-xs-4">
                                             <label>Employee Name:</label>
                                             <select class="form-input saveFields--" name="sint_EmployeesRefId" id="sint_EmployeesRefId">
                                                <option value="">Select Employee</option>
                                                <?php
                                                   $rsemp = SelectEach("employees","ORDER BY LastName");
                                                   if ($rsemp) {
                                                      while ($rowemp = mysqli_fetch_assoc($rsemp)) {
                                                         $refid = $rowemp["RefId"];
                                                         $LastName = $rowemp["LastName"];
                                                         $FirstName = $rowemp["FirstName"];
                                                         $MiddleName = $rowemp["MiddleName"];
                                                         echo '<option value="'.$refid.'">'.$LastName.', '.$FirstName.'</option>';
                                                      }
                                                   }
                                                ?>
                                             </select>
                                          </div>
                                          
                                       </div>
                                       <div class="row">
                                          <div class="col-xs-4">
                                             <label class="control-label" for="inputs">Position Title:</label><br>
                                             <?php
                                                createSelect("Position",
                                                             "sint_PositionRefId",
                                                             "",100,"Name","Select Position","");
                                             ?>
                                          </div>
                                          <div class="col-xs-4">
                                             <label class="control-label" for="inputs">Office/Area:</label><br>
                                             <?php
                                                createSelect("Office",
                                                             "sint_OfficeRefId",
                                                             "",100,"Name","Select Office","");
                                             ?>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-4">
                                             <label class="control-label" for="inputs">Profile Match:</label>
                                             <input type="text" class="form-input saveFields--" name="" id="">
                                          </div>
                                       </div>
                                       <br>
                                       <?php bar(); ?>
                                       <div class="row margin-top">
                                          <div class="col-xs-3 text-center">
                                             
                                          </div>
                                          <div class="col-xs-2 text-center">
                                             
                                          </div>
                                          <div class="col-xs-2 text-center">
                                             
                                          </div>
                                          <div class="col-xs-3 text-center">
                                             
                                          </div>
                                          <div class="col-xs-2 text-center">
                                             
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-12">
                                             <div class="panel-top">&nbsp;</div>
                                             <div class="panel-mid">
                                                <table class="table table-order-column table-striped table-bordered table-hover" border="1">
                                                   <thead>
                                                      <tr style="font-size: 8pt; font-weight: 400;">
                                                         <th style="width: 24.99%;">
                                                            <b>COMPETENCIES REQUIRED OF THE POSITION</b>
                                                         </th>
                                                         <th style="width: 16.66%;">
                                                            <b>REQUIRED<br>LEVEL OF<br>PROFICIENCY</b>
                                                         </th>
                                                         <th style="width: 24.99%;">
                                                            <b>ASSESSMENT RESULT</b>
                                                         </th>
                                                         <th style="width: 16.66%;">
                                                            <b>INTERVENTION<br>/DEVELOPMENT<br>REQUIREMENT</b>
                                                         </th>
                                                         <th style="width: 16.66%;">
                                                            <b>FOCUS BEHAVIOR</b>
                                                         </th>
                                                      </tr>
                                                   </thead>
                                                   <tbody id="competency_table"></tbody>
                                                </table>   
                                             </div>
                                             <div class="panel-bottom"></div>
                                          </div>
                                       </div> 
                                    </div>
                                 </div>
                                 <div class="panel-bottom">
                                    <input type="hidden" name="sint_EmployeesRefId" id="sint_EmployeesRefId" value="0">
                                    <button type="button" name="btn_save_competency" id="btn_save_competency" class="btn-cls4-sea">
                                       <i class="fa fa-save"></i>&nbsp;&nbsp;SAVE
                                    </button>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="rptModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                           <div class="row">
                              <div class="col-xs-12" style="padding: 10px;">
                                 <div class="row margin-top">
                                    <div class="col-xs-12">
                                       <label class="control-label" for="inputs">COMPETENCY TYPE:</label>
                                       <input class="form-input" 
                                              type="text"
                                              name="char_Type"
                                              id="char_Type" readonly>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-12">
                                       <label class="control-label" for="inputs">OPERATIONAL DEFINITION:</label>
                                       <textarea class="form-input" name="char_Definition" id="char_Definition" rows="3" disabled></textarea>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-12">
                                       <label class="control-label" for="inputs">COMPETENCY NAME:</label>
                                       <input class="form-input" 
                                              type="text"
                                              name="char_Name"
                                              id="char_Name" readonly>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-12">
                                       <table class="table table-order-column table-striped table-bordered table-hover">
                                          <thead>
                                             <tr>
                                                <th style="width: 10%;" class="text-center">
                                                   <input type="checkbox" name="chk_all" id="chk_all">
                                                </th>
                                                <th style="width: 90%;" class="text-center">
                                                   <span id="proficiency_level"></span>
                                                </th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <?php for ($i=1; $i <= 10; $i++) { ?>
                                                <tr>
                                                   <td class="text-center">
                                                      <input type="checkbox" name="chk_<?php echo $i; ?>" id="chk_<?php echo $i; ?>">
                                                   </td>
                                                   <td class="">
                                                      <span id="description_<?php echo $i; ?>"></span>
                                                   </td>
                                                </tr>
                                             <?php } ?>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       <label class="control-label" for="inputs">Assessment Result:</label>
                                       <select class="form-input" name="result" id="result">
                                          <option value="">Select Result</option>
                                          <option value="DEMONSTRATES SOME BEHAVIORS">DEMONSTRATES SOME BEHAVIORS</option>
                                          <option value="NEEDS TO DEMONSTRATE PRESCRIBED BEHAVIORS">NEEDS TO DEMONSTRATE PRESCRIBED BEHAVIORS</option>
                                          <option value="DEMONSTRATES MOST BEHAVIORS">DEMONSTRATES MOST BEHAVIORS</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       <label class="control-label" for="inputs">Intervention Requirement:</label>
                                       <select class="form-input" name="requirement" id="requirement">
                                          <option value="">Select Requirements</option>
                                          <option value="COACHING/EXPOSURE">COACHING/EXPOSURE</option>
                                          <option value="MAY REQUIRE FORMAL TRAINING/INTERVENTION OR REVISIT PROFILE">MAY REQUIRE FORMAL TRAINING/INTERVENTION OR REVISIT PROFILE</option>
                                          <option value="NONE">NONE</option>
                                          <option value="OTHER">OTHER</option>
                                       </select>
                                    </div>
                                    <div class="col-xs-6" id="other_requirements">
                                       <label class="control-label" for="inputs">Please specify others:</label>
                                       <input type="text" class="form-input" name="other_requirement" id="other_requirement">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="modal-footer">
                           <div class="row margin-top">
                              <div class="col-xs-12 text-left">
                                 <button class="btn-cls4-sea" id="save_competency" type="button">SAVE</button>
                                 <input type="hidden" name="behavior" id="behavior" value="">
                                 <input type="hidden" name="selected_competency" id="selected_competency" value="">
                                 <input type="hidden" name="idx_competency" id="idx_competency" value="">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               doHidden("paramTitle",getvalue("paramTitle"),"");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
   <script language="JavaScript">
      $(document).ready(function () {
         var behavior = "";
         $("#other_requirements").hide();
         $("#requirement").change(function () {
            var value = $(this).val();
            if (value == "OTHER") {
               $("#other_requirements").show();
            } else {
               $("#other_requirements").hide();
            }
         });
         $("#btn_save_competency").click(function () {
            var check = $("#competency_table").html();
            var year  = $("#Year").val();
            if (check == "") {
               $.notify("Create Individual Competency Data First.");
               return false;
            } else {
               if (year != "") {
                  //$.notify("Ready");
                  var idx = 0;
                  var obj = "";
                  $("[name*='refid_']").each(function () {
                     //$.notify($(this).attr("name"));
                     var idx           = $(this).attr("name").split("_")[1];
                     var result        = $("[name='result_" + idx + "']").val();
                     var requirement   = $("[name='requirement_" + idx + "']").val();
                     var behavior      = $("[name='behavior_" + idx + "']").val();
                     var refid         = $("[name='refid_" + idx + "']").val();
                     var level         = $("#level_" + idx).html();
                     obj += refid+"|"+result+"|"+requirement+"|"+behavior+"|"+level+"_";
                  });
                  $.post("ldms_transaction.e2e.php",
                  {
                     fn: "saveIDC",
                     obj: obj,
                     mode: "save",
                     emprefid: $("[name='sint_EmployeesRefId']").val(),
                     year: year,
                     position: $("[name='sint_PositionRefId']").val(),
                     office: $("[name='sint_OfficeRefId']").val(),
                  },
                  function(data,status) {
                     if (status == "success") {
                        try {
                           eval(data);
                        } catch (e) {
                           if (e instanceof SyntaxError) {
                              alert(e.message);
                           }
                        }
                     }
                  });
               } else {
                  $.notify("Select Year First");
               }
            }
         });
         $("[id*='chk_']").each(function () {
            $(this).click(function () {
               var id = $(this).attr("id").split("_")[1];
               if ($(this).is(":checked") == true) {
                  if ($("#description_" + id).html() != "") {
                     $(this).prop("checked",true);
                     if (id != "all") {
                        if (!$("#behavior").val().includes(id + " ")) {
                           behavior += id + " ";      
                        }
                        
                     }
                  }
               } else {
                  var str = id + " ";
                  behavior = behavior.replace(str,"");
                  $(this).prop("checked",false);
               }
               $("#behavior").val(behavior);
            });
         });
         $("#chk_all").click(function () {
            var behavior_all = "";
            if ($(this).is(":checked") == true) {
               $("[id*='chk_']").each(function () {
                  var id = $(this).attr("id").split("_")[1];
                  if ($("#description_" + id).html() != "") {
                     $(this).prop("checked",true);
                     if (id != "all") {
                        behavior_all += id + " ";   
                     }
                  }
               });
            } else {
               behavior_all = "";
               $("[id*='chk_']").prop("checked",false);
            }
            $("#behavior").val(behavior_all);
         });
         
         $("#save_competency").click(function () {
            var behavior      = $("#behavior").val();
            var refid         = $("#selected_competency").val();
            var requirement   = $("#requirement").val();
            var result        = $("#result").val();
            if (requirement == "OTHER") requirement = $("#other_requirement").val();
            saveCompetency(refid,behavior,requirement,result);
         });
         $("#sint_EmployeesRefId").change(function () {
            var value = $(this).val();
            selectEmp(value);
         });
      });
      function selectMe(refid) {
         $("#rptContent").attr("src","blank.htm");
         var rptFile = "rpt_ICD";
         var url = "ReportCaller.e2e.php?file=" + rptFile;
         url += "&refid=" + refid;
         url += "&" + $("[name='hgParam']").val();
         $("#rptModal").modal();
         $("#rptContent").attr("src",url);
      }
      function afterNewSave() {
         alert("Successfully Saved");
         gotoscrn("ldms_individual_competency_data","");
      }
      function recordExist() {
         alert("Already have an Assessment.");
         gotoscrn($("#hProg").val(),"");
      }
      function selectEmp(emprefid) {
         var str = "";
         $("#competency_table").html("");
         //$("[name='sint_EmployeesRefId']").val(emprefid);
         $.get("ldmsEmpDetail.e2e.php",
         {
            EmpRefId:emprefid,
            hCompanyID:$("#hCompanyID").val(),
            hBranchID:$("#hBranchID").val(),
            hEmpRefId:$("#hEmpRefId").val(),
            hUserRefId:$("#hUserRefId").val()
         },
         function(data,status) {
            if (status == "success") {
               try {
                  eval(data);
                  
                  $.get("ldms_transaction.e2e.php",
                  {
                     emprefid: emprefid,
                     fn: "getCompetencyPosition"
                  },
                  function(data,status) {
                     if (status == "success") {
                        try {
                           data = data.trim();
                           if (data != "") {
                              var count = 0;
                              var arr = JSON.parse(data);
                              for (var i = 0; i < arr.length; i++) {
                                 count++;
                                 var refid                  = arr[i]["RefId"];
                                 var competency_name        = getCompetencyName(arr[i]["CompetencyRefId"],refid);
                                 var competency_level       = arr[i]["Level"];
                                 var competency_type        = arr[i]["Type"];
                                 var competency_refid       = arr[i]["CompetencyRefId"];
                                 var competency_result      = arr[i]["Result"];
                                 var competency_requirement = arr[i]["Requirement"];
                                 var competency_behavior    = arr[i]["Behavior"];
                                 if (competency_behavior != null) {
                                    var behavior_arr           = competency_behavior.split(" ");
                                    competency_behavior        = "";
                                    for (var a = 0; a < behavior_arr.length; a++) {
                                       competency_behavior += behavior_arr[a] + " ";
                                    }   
                                 }
                                 
                                 if (competency_result == null) competency_result = "&nbsp;";
                                 if (competency_requirement == null) competency_requirement = "&nbsp;";
                                 if (competency_behavior == null) competency_behavior = "&nbsp;";

                                 str += '<tr style="color:black;">';
                                 str += '<td rowspan="2"><span id="name_'+refid+'">' + competency_name  + '</span><input type="hidden" value="' + competency_refid  + '"  name="refid_'+i+'"></td>';
                                 str += '<td rowspan="2" class=" text-center" id="level_'+i+'">' + competency_level  + '</td>';
                                 str += '<td class="text-center ">';
                                 str += '<button class="btn-cls4-sea" type="button" onClick="view('+competency_refid+','+competency_level+','+refid+','+i+');">VIEW</button>';
                                 str += '</td>';
                                 str += '<td rowspan="2" class="text-center "><span id="requirement_'+i+'">' + competency_requirement  + '</span><input type="hidden" value="' + competency_requirement  + '"  name="requirement_'+i+'"></td>';
                                 str += '<td rowspan="2" class="text-center "><span id="behavior_'+i+'">'+ competency_behavior +'</span><input type="hidden" value="'+ competency_behavior +'" name="behavior_'+i+'"></td>';
                                 str += '</tr>';
                                 str += '<tr style="color:black;">';
                                 str += '<td class="text-center"><span id="result_'+i+'">' + competency_result +'</span><input type="hidden" value="' + competency_result +'" name="result_'+i+'"></td>';
                                 str += '</tr>';

                              }
                           } else {
                              str += '<tr>';
                              str += '<td colspan="5">No Competency Found</td>'; 
                              str += '</tr>';
                           }
                           $("#competency_table").html(str);
                        } catch (e) {
                           if (e instanceof SyntaxError) {
                              alert(e.message);
                           }
                        }
                     }
                  });

               } catch (e) {
                   if (e instanceof SyntaxError) {
                       alert(e.message);
                   }
               }
            }
         });
      }
      function saveCompetency(refid,behavior,requirement,result){
         var idx = $("#idx_competency").val();
         $("#requirement_" + idx).html(requirement);
         $("#behavior_" + idx).html(behavior);
         $("#result_" + idx).html(result);

         $("[name='requirement_" + idx + "']").val(requirement);
         $("[name='behavior_" + idx + "']").val(behavior);
         $("[name='result_" + idx + "']").val(result);
         /*$.post("ldms_transaction.e2e.php",
         {
            refid: refid,
            fn: "saveCompetency",
            behavior: behavior,
            requirement: requirement,
            result: result
         },
         function(data,status) {
            if (status == "success") {
               try {
                  eval(data);
               } catch (e) {
                  if (e instanceof SyntaxError) {
                     alert(e.message);
                  }
               }
            }
         });*/
         $("#prnModal").modal('toggle');
      }
      function getCompetencyName(refid,obj_refid){
         var name = "";
         $.get("ldms_transaction.e2e.php",
         {
            refid: refid,
            fn: "getCompetencyName"
         },
         function(data,status) {
            if (status == "success") {
               try {
                  name = data.trim();
                  $("#name_" + obj_refid).html(name);
               } catch (e) {
                  if (e instanceof SyntaxError) {
                     alert(e.message);
                  }
               }
            }
         });
         
      }
      function view(refid,level,curr_refid,idx) {
         $("#prnModal").modal();
         $("#selected_competency", "#behavior, #idx_competency").val("");
         $("#selected_competency").val(curr_refid);
         $("#idx_competency").val(idx);
         $("#char_Type").val("");
         $("#char_Definition").val("");
         $("#char_Name").val("");
         $("#proficiency_level").html("Level");
         $("#result").val("");
         $("#requirement").val("");
         $("#behavior").val("");
         for (var a = 1; a <= 10; a++) {
            $("#description_" + a).html("");
            $("#chk_" + a).prop("checked",false);
         }
         $("#chk_all").prop("checked",false);
         var str = "";
         var level_word = "";
         $.get("ldms_transaction.e2e.php",
         {
            refid: refid,
            table: "competency",
            fn: "getCompetencyInfo"
         },
         function(data,status) {
            if (status == "success") {
               try {
                  data = data.trim();
                  if (data != "") {
                     var arr = JSON.parse(data);
                     var competency_name  = arr["Name"];
                     var competency_level = level;
                     var competency_type  = arr["Type"];
                     var competency_definition = arr["Definition"];

                     $("#char_Type").val(competency_type);
                     $("#char_Definition").val(competency_definition);
                     $("#char_Name").val(competency_name);
                     $("#proficiency_level").html("Level " + level);
                     switch (competency_level) {
                        case 1:
                           level_word = "One";
                           break;
                        case 2:
                           level_word = "Two";
                           break;
                        case 3:
                           level_word = "Three";
                           break;
                        case 4:
                           level_word = "Four";
                           break;
                     }
                     for (var i = 1; i <= 10; i++) {
                        str = "Level" + level_word + i;
                        $("#description_" + i).html(arr[str]);
                     }
                     $.get("ldms_transaction.e2e.php",
                     {
                        refid: curr_refid,
                        table: "position_competency",
                        fn: "getCompetencyInfo"
                     },
                     function(data,status) {
                        if (status == "success") {
                           try {
                              var new_arr = JSON.parse(data);
                              $("#result").val(new_arr["Result"]);
                              $("#requirement").val(new_arr["Requirement"]);
                              $("#behavior").val(new_arr["Behavior"]);
                              var behavior_arr = new_arr["Behavior"].split(" ");
                              for (var a = 1; a < behavior_arr.length; a++) {
                                 $("#chk_" + a).prop("checked",true);
                              }
                           } catch (e) {
                              if (e instanceof SyntaxError) {
                                 alert(e.message);
                              }
                           }
                        }
                     });   
                  }
                  
               } catch (e) {
                  if (e instanceof SyntaxError) {
                     alert(e.message);
                  }
               }
            }
         });
      }
      function deleteRecord(refid) {
         $.get("ldms_transaction.e2e.php",
         {
            refid: refid,
            fn: "deleteICD"
         },
         function(data,status) {
            if (status == "success") {
               try {
                  eval(data);
               } catch (e) {
                  if (e instanceof SyntaxError) {
                     alert(e.message);
                  }
               }
            }
         });
      }
   </script>
</html>



