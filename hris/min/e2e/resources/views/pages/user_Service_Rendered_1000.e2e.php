<?php
   require_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   require_once pathClass.'0620RptFunctions.e2e.php';
   require_once pathClass.'DTRFunction.e2e.php';
   require_once "conn.e2e.php";
   $emprefid = getvalue('hUserRefId');
   $whereClause = "WHERE RefId = '$emprefid'";
   $row = FindFirst("employees",$whereClause,"*");
   if ($row) {
      $LastName      = $row["LastName"];
      $FirstName     = $row["FirstName"];
      $MiddleName    = $row["MiddleName"];
      $MiddleInitial = substr($row["MiddleName"], 0,1);
      $ExtName       = $row["ExtName"];
      $FullName      = $FirstName." ".$MiddleInitial." ".$LastName." ".$ExtName;
   } else {
      $FullName      = "";
   }
   $emp_row = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
   if ($emp_row) {
      $appt    = $emp_row["ApptStatusRefId"];
      $PositionRefId = $emp_row["PositionRefId"];
      $PositionRefId = getRecord("Position",$PositionRefId,"Name");
      
   } else {
      $PositionRefId = "";
   }


   $year = date("Y",time());
   $arr_month =[
     "January",
     "February",
     "March",
     "April",
     "May",
     "June",
     "July",
     "August",
     "September",
     "October",
     "November",
     "December"
   ];
?>
<!DOCTYPE html>
<html>
   <head>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script type="text/javascript">
         $(document).ready(function () {
            $("#btnPrint").click(function () {
               var start = $("#start").val();
               var end   = $("#end").val();
               var head = $("head").html();
               
               printDiv('div_CONTENT',head);
            });
         });
      </script>
      <style type="text/css">
         th {
            text-align: center;
         }
      </style>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis") ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar ("Certificate of Service Rendered"); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12">
                     <button type="button" id="btnPrint" class="btn-cls4-lemon">PRINT</button>
                     <input type="text" class="date-- form-input" name="start" id="start" placeholder="FROM">
                     <input type="text" class="date-- form-input" name="end" id="end" placeholder="TO">
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-11" id="div_CONTENT">
                     <div class="row">
                        <div class="col-xs-12">
                           <div class="row">
                              <div class="col-xs-12 text-center">
                                 PHILIPPINE INSTITUTE FOR DEVELOPMENT STUDIES
                                 <br>
                                 18<sup>th</sup> Floor Three Cyberpod Centris, North Tower
                                 <br>
                                 EDSA cor. Quezon Ave., Quezon City
                                 <br>
                                 <div class="row">
                                    <div class="col-xs-8 text-center"></div>
                                    <div class="col-xs-4 text-center">
                                       <u>
                                          <?php echo date("F d, Y",time()); ?>
                                          <br>
                                          Date
                                       </u>
                                    </div>
                                 </div>
                                 
                                 <br><br>
                                 <b>
                                    CERTIFICATE OF SERVICES RENDERED
                                 </b>
                              </div>
                           </div>
                           <br><br>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-10" style="text-indent: 5%; text-align: justify;">
                                 <p>
                                    This is to certify that I have rendered my services in the Philippine Institute for Development Studies  (PIDS) as <b><u><?php echo $PositionRefId; ?></u></b> for the period <span id="range">________________________</span>.
                                 </p>
                              </div>
                           </div>
                           <br><br>
                           <div class="row margin-top">
                              <div class="col-xs-7"></div>
                              <div class="col-xs-5 text-center">
                                 <b><u><?php echo strtoupper($FullName); ?></u></b>
                                 <br>
                                 (Signature Over Printed Name)
                                 <br>
                                 <b><u><?php echo strtoupper($PositionRefId); ?></u></b>
                                 <br>
                                 Designation
                              </div>
                           </div>
                           <br><br>
                           <div class="row margin-top">
                              <div class="col-xs-4 text-center">
                                 Certified true & correct:
                                 <br><br>
                                 _________________________________
                                 <br>
                                 (Signature Over Printed Name)
                                 <br>
                                 _________________________________
                                 <br>
                                 Designation
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>