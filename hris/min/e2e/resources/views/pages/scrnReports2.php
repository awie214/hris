<?php
   $incRptScrn = getvalue("hRptScrn");
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.php"; ?>
      <script src="js/ctrl/ctrl_Report.js"></script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <div class="panel-top">
               <i class="fa fa-bars" aria-hidden="true" onclick="openNav();" class="mbar"></i>&nbsp;&nbsp;
               <?php
                  echo strtoupper(getvalue("paramTitle"));
                  if (getvalue("hRptScrn") != "") {
               ?>
               <button type="button" class="close" aria-label="Close" onclick="closeSCRN('model');">
                  <span aria-hidden="true" style="color:white;">&times;</span>
               </button>
               <?php } ?>
            </div>
            <div class="container-fluid margin-top10" id="paramScreen">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                  <?php if ($incRptScrn == "") { ?>
                     <div class="mypanel" id="ReportHead">
                        <div class="panel-top">Report Type</div>
                        <div class="panel-mid-litebg">
                           <div class="row margin-top">
                              <div class="col-xs-3">
                                 <div class="InfoMenu" id="mApplicantList">
                                    <img src="images/applicants.png">&nbsp;Report 1
                                 </div>
                                 <div class="InfoMenu" id="mEmpList">
                                    <img src="images/employees.png">&nbsp;Report 2
                                 </div>
                                 <div class="InfoMenu" id="mEmpChildList">
                                    <img src="images/baby.png">&nbsp;Report 3
                                 </div>
                                 <div class="InfoMenu" id="mEmpTrainingEduc">
                                    <img src="images/educ.png">&nbsp;Report 4
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  <?php } else { ?>
                     <div class="row">
                        <div class="col-xs-12" id="RptMainHolder">
                           <?php include $incRptScrn.".php"; ?>
                        </div>
                     </div>
                  <?php } ?>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varHidden.php";
            ?>
            <input type="hidden" id="hRptFile" value="<?php echo getvalue("hRptFile")?>">
         </div>
      </form>
   </body>
</html>