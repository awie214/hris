<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>

   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("GSIS ARA FORM E"); ?>
            <div class="container-fluid margin-top">
               <div class="mypanel">
                  <div class="row">
                     <div class="col-xs-12" id="div_CONTENT">
                        <div class="margin-top">
                           <div id="divList">
                              <div class="mypanel panel panel-default">
                                 <div class="panel-top">
                                    <span id="ScreenMode">LIST OF GSIS ARA FORM E
                                 </div>
                                 <div class="panel-mid">
                                    <span id="spGridTable">
                                       <?php
                                       $table = "gsis_ara";
                                       $gridTableHdr_arr = ["Last Name","First Name","Middle Name"];
                                       $gridTableFld_arr = ["LastNameFrom","FirstNameFrom","MiddleNameFrom"];
                                       $sql = "SELECT * FROM gsis_ara";
                                       $Action = [true,true,true,false];
                                       doGridTable($table,
                                                   $gridTableHdr_arr,
                                                   $gridTableFld_arr,
                                                   $sql,
                                                   $Action,
                                                   $_SESSION["module_gridTable_ID"]);
                                 ?>
                                    </span>
                                 </div>
                                 <div class="panel-bottom">
                                    <?php
                                       btnINRECLO([true,false,false]);
                                    ?>
                                 </div>
                              </div>
                           </div>
                           <div id="divView">
                              <!--  -->
                              <div class="mypanel panel panel-default">
                                 <div class="panel-top">
                                    <span id="ScreenMode">INSERTING NEW GSIS ARA FORM E
                                 </div>
                                 <div class="panel-mid-litebg" id="EntryScrn">
                                    <div id="EntryScrn">
                                       <div class="row">
                                          <div class="col-xs-12">
                                             <div class="row">
                                                <div class="col-xs-6">
                                                   <label class="control-label" for="inputs">Member BP Number:</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_MemberBPNumber"
                                                          id="char_MemberBPNumber">
                                                </div>
                                             </div>
                                             <br>
                                             <div class="row">
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Last Name (From):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_LastNameFrom"
                                                          id="char_LastNameFrom">
                                                </div>
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Last Name (To):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_LastNameTo"
                                                          id="char_LastNameTo">
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">First Name (From):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_FirstNameFrom"
                                                          id="char_FirstNameFrom">
                                                </div>
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">First Name (To):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_FirstNameTo"
                                                          id="char_FirstNameTo">
                                                </div>
                                             </div>
                                             
                                             <div class="row">
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Middle Name (From):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_MiddleNameFrom"
                                                          id="char_MiddleNameFrom">
                                                </div>
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Middle Name (To):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_MiddleNameTo"
                                                          id="char_MiddleNameTo">
                                                </div>
                                             </div>
                                             
                                             <div class="row">
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Ext Name (From):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_ExtNameFrom"
                                                          id="char_ExtNameFrom">
                                                </div>
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Ext Name (To):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_ExtNameTo"
                                                          id="char_ExtNameTo">
                                                </div>
                                             </div>
                                             
                                             <div class="row">
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Street (From):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_StreetFrom"
                                                          id="char_StreetFrom">
                                                </div>
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Street (To):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_StreetTo"
                                                          id="char_StreetTo">
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">City (From):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_CityFrom"
                                                          id="char_CityFrom">
                                                </div>
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">City (To):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_CityTo"
                                                          id="char_CityTo">
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Region (From):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_RegionFrom"
                                                          id="char_RegionFrom">
                                                </div>
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Region (To):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_RegionTo"
                                                          id="char_RegionTo">
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Country (From):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_CountryFrom"
                                                          id="char_CountryFrom">
                                                </div>
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Country (To):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_CountryTo"
                                                          id="char_CountryTo">
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">CivilStatus (From):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_CivilStatusFrom"
                                                          id="char_CivilStatusFrom">
                                                </div>
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">CivilStatus (To):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_CivilStatusTo"
                                                          id="char_CivilStatusTo">
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Postal Code (From):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_PostalCodeFrom"
                                                          id="char_PostalCodeFrom">
                                                </div>
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Postal Code (To):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_PostalCodeTo"
                                                          id="char_PostalCodeTo">
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Mobile No (From):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_MobileNoFrom"
                                                          id="char_MobileNoFrom">
                                                </div>
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Mobile No (To):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_MobileNoTo"
                                                          id="char_MobileNoTo">
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Email Address (From):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_EmailAddFrom"
                                                          id="char_EmailAddFrom">
                                                </div>
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Email Address (To):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_EmailAddTo"
                                                          id="char_EmailAddTo">
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Status of Employment (From):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_EmpStatusFrom"
                                                          id="char_EmpStatusFrom">
                                                </div>
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Status of Employment (To):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_EmpStatusTo"
                                                          id="char_EmpStatusTo">
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Date of Birth (From):</label><br>
                                                   <input class="form-input saveFields-- date--" 
                                                          type="text"
                                                          name="date_BirthDateFrom"
                                                          id="date_BirthDateFrom">
                                                </div>
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Date of Birth (To):</label><br>
                                                   <input class="form-input saveFields-- date--" 
                                                          type="text"
                                                          name="date_BirthDateTo"
                                                          id="date_BirthDateTo">
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Place of Birth (From):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_BirthPlaceFrom"
                                                          id="char_BirthPlaceFrom">
                                                </div>
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Place of Birth (To):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_BirthPlaceTo"
                                                          id="char_BirthPlaceTo">
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Position (From):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_PositionFrom"
                                                          id="char_PositionFrom">
                                                </div>
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Position (To):</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_PositionTo"
                                                          id="char_PositionTo">
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Effectivity Date (From):</label><br>
                                                   <input class="form-input saveFields-- date--" 
                                                          type="text"
                                                          name="date_EffectivityDateFrom"
                                                          id="date_EffectivityDateFrom">
                                                </div>
                                                <div class="col-xs-4">
                                                   <label class="control-label" for="inputs">Effectivity Date (To):</label><br>
                                                   <input class="form-input saveFields-- date--" 
                                                          type="text"
                                                          name="date_EffectivityDateTo"
                                                          id="date_EffectivityDateTo">
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-xs-8">
                                                   <label class="control-label" for="inputs">Remarks:</label>
                                                   <input class="form-input saveFields--" 
                                                          type="text"
                                                          name="char_Remarks"
                                                          id="char_Remarks">
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel-bottom">
                                    <?php btnSACABA([true,true,true]); ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               $table = "gsis_ara";
               doHidden("paramTitle",getvalue("paramTitle"),"");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
   <script language="JavaScript">
      $(document).ready(function () {
         remIconDL();
      });
      function afterNewSave() {
         alert("Successfully Saved");
         gotoscrn($("#hProg").val(),"");
      }
      
   </script>
</html>



