<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script type="text/javascript" src="<?php echo jsCtrl("ctrl_MasterFile"); ?>"></script>
      <script language="JavaScript">
         
         $(document).ready(function () {
            
         });
      </script>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar($modTitle); ?>
            <div class="container-fluid margin-top10">
               <div class="row">
                  <div class="col-xs-3">
                     <?php
                        employeeSelector();
                     ?>
                  </div>
                  <div class="col-xs-9" style="padding:0px;">
                     <div id="btnHolder">
                        <?php 
                           createButton("Print Preview","btnPrintMF","btn-cls4-lemon","fa-print",""); 
                        ?>
                        <a href="#" id="aSplitSettings">Split Settings</a>
                     </div>
                     <div id="RecordDetails" style="margin:0px;padding-left:0;margin-top:5px;">
                        <?php //include 'ctrl_201File_1.e2e.php'; ?>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               include "pds_modal.e2e.php";
               footer();
               doHidden("paramTitle",getvalue("paramTitle"),"");
               doHidden("hEmpRefId","","");
               include "varHidden.e2e.php";
            ?>
         </div>
         <?php 
            $templ->splitSettings();
         ?>
      </form>
   </body>
</html>







