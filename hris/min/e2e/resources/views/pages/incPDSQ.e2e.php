<?php
   function doYESNO($idx) {
      echo
      '<div class="row">
         <input type="hidden" class="HiddenEntry saveFields--" name="sint_'.$idx.'" value="">
         <div class="col-xs-6">
            <input type="radio" class="form-input" style="width:20px;" name="Ans_'.$idx.'" id="YAns_'.$idx.'" onclick="clickAnsYN(1,\''.$idx.'\');">&nbsp;&nbsp;
            <label for="YAns_'.$idx.'">YES</label>
         </div>
         <div class="col-xs-6">
            <input type="radio" class="form-input" style="width:20px;" name="Ans_'.$idx.'" id="NAns_'.$idx.'" onclick="clickAnsYN(0,\''.$idx.'\');" checked>&nbsp;&nbsp;
            <label for="NAns_'.$idx.'">NO</label>
         </div>
      </div>';
      if ($idx == "Q7c" || $idx == "Q7b") {
         echo '
            <div class="row">
               <div class="col-xs-12">
                  If YES, please specificy ID no. <input type="text" class="form-input saveFields--" name="char_'.$idx.'exp"/>
               </div>
            </div>';
      } else {
         if ($idx == "Q1a") {
            echo '
               <div class="row" style="display:none;">
                  <div class="col-xs-12">
                     If YES, give details <input type="text" class="form-input saveFields--" name="char_'.$idx.'exp"/>
                  </div>
               </div>
            ';
         } else {
            echo '
               <div class="row">
                  <div class="col-xs-12">
                     If YES, give details <input type="text" class="form-input saveFields--" name="char_'.$idx.'exp"/>
                  </div>
               </div>
            ';   
         }
      }
   }
?>
      <!--fldName="Q1a,Q1aexp,Q1b,Q1bexp,Q2a,Q2aexp,Q2b,Q2bexp,Q2DateFiled,Q2CaseStatus,Q3a,Q3aexp,Q4a,Q4aexp,Q5a,Q5aexp,Q5b,Q5bexp,Q6a,Q6aexp,Q7a,Q7aexp,Q7b,Q7bexp,Q7c,Q7cexp"-->
      <input type="checkbox" for="<?php echo $iFile; ?>" class="enabler--" id="chkPDSQ" name="chkPDSQ" refid="">
      <label for="chkPDSQ" class="btn-cls4-sea">EDIT PDSQ</label>
      <div class="row" id="EntryPDSQ">
         <input type="hidden" name="pdsqRefId" value="">
         <div class="col-xs-6">
            <div class="panel-top">
               34. Are you related by consanguinity or affinity to the appointing or recommending authority, or to the chief of bureau or office or to the person who has immediate supervision over you in the Office, Bureau or Department where you will be apppointed,
            </div>
            <div class="panel-mid-litebg">
               <div class="row">
                  <div class="col-xs-12">
                     a. within the third degree?
                  </div>
               </div>
               <?php doYESNO("Q1a"); ?>
               <div class="row">
                  <div class="col-xs-12">
                     b. within the fourth degree (for Local Government Unit - Career Employees)?
                  </div>
               </div>
               <?php doYESNO("Q1b"); ?>
            </div>
            <div class="panel-bottom"></div>

            <div class="panel-top margin-top">
               35.
            </div>
            <div class="panel-mid-litebg">
               <div class="row">
                  <div class="col-xs-12">
                     a. Have you ever been found guilty of any administrative offense?
                  </div>
               </div>
               <?php doYESNO("Q2a"); ?>
               <div class="row">
                  <div class="col-xs-12">
                     b. Have you been criminally charged before any court?
                  </div>
               </div>
               <?php doYESNO("Q2b"); ?>
               <div class="row margin-top">
                  <div class="col-xs-12">
                  <label>Date Filed:</label>
                  <input type="text" class="form-input date-- saveFields--" name="date_Q2DateFiled" id="date_Q2DateFiled" group="1" readonly>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-12">
                  <label>Status of Case/s:</label>
                  <input type="text" class="form-input saveFields--" name="char_Q2CaseStatus" id="char_Q2CaseStatus" group="1">
                  </div>
               </div>
            </div>
            <div class="panel-bottom"></div>

            <div class="panel-top margin-top">
               36.
            </div>
            <div class="panel-mid-litebg">
               <div class="row">
                  <div class="col-xs-12">
                     a. Have you ever been convicted of any crime or violation of any law, decree, ordinance or regulation by any court or tribunal?
                  </div>
               </div>
               <?php doYESNO("Q3a"); ?>
            </div>
            <div class="panel-bottom"></div>
         </div>
         <div class="col-xs-6">

            <div class="panel-top">
               37.
            </div>
            <div class="panel-mid-litebg">
               <div class="row">
                  <div class="col-xs-12">
                     a. Have you ever been separated from the service in any of the following modes: resignation, retirement, dropped from the rolls, dismissal, termination, end of term, finished contract or phased out (abolition) in the public or private sector?
                  </div>
               </div>
               <?php doYESNO("Q4a"); ?>
            </div>
            <div class="panel-bottom"></div>
            <div class="panel-top margin-top">
               38.
            </div>
            <div class="panel-mid-litebg">
               <div class="row">
                  <div class="col-xs-12">
                     a. Have you ever been a candidate in a national or local election held within the last year (except Barangay election)?
                  </div>
               </div>
               <?php doYESNO("Q5a"); ?>
               <div class="row">
                  <div class="col-xs-12">
                     b. Have you resigned from the government service during the three (3)-month period before the last election to promote/actively campaign for a national or local candidate?
                  </div>
               </div>
               <?php doYESNO("Q5b"); ?>
            </div>
            <div class="panel-bottom"></div>
            <div class="panel-top margin-top">
               39.
            </div>
            <div class="panel-mid-litebg">
               <div class="row">
                  <div class="col-xs-12">
                     a. Have you acquired the status of an immigrant or permanent resident of another country?
                  </div>
               </div>
               <?php doYESNO("Q6a"); ?>
            </div>
            <div class="panel-bottom"></div>
            <div class="panel-top margin-top">
               40. Pursuant to: (a) Indigenous People's Act (RA 8371); (b) Magna Carta for Disabled Persons (RA 7277); and (c) Solo Parents Welfare Act of 2000 (RA 8972), please answer the following items:
            </div>
            <div class="panel-mid-litebg">
               <div class="row">
                  <div class="col-xs-12">
                     a. Are you a member of any indigenous group?
                  </div>
               </div>
               <?php doYESNO("Q7a"); ?>
               <div class="row">
                  <div class="col-xs-12">
                     b. Are you a person with disability?
                  </div>
               </div>
               <?php doYESNO("Q7b"); ?>
               <div class="row">
                  <div class="col-xs-12">
                     b. Are you a solo parent?
                  </div>
               </div>
               <?php doYESNO("Q7c"); ?>
            </div>
            <div class="panel-bottom"></div>
         </div>
      </div>



