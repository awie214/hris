<script>
   $(document).ready(function () {
      $(".newDataLibrary").hide();
      $(".createSelect--").css("width","100%");
   });
</script>
<div class="mypanel" id="empdeductions">
   <div class="row margin-top" style="height:200px;overflow:auto;padding: 5px;">
      <table border="1" width="100%">
         <tr  class="txt-center" style="padding:5px;">
            <td><label>Deduction Name</label></td>
            <td><label>Amount</label></td>
            <td><label>Date Start</label></td>
            <td><label>Date End</label></td>
            <td><label>Terminated</label></td>
         </tr>
      <?php
         $rs = SelectEach("employeeschild","");
         if ($rs) {
            $recordNum = mysqli_num_rows($rs);
            while ($row = mysqli_fetch_array($rs)) {
      ?>
         <tr>
            <td><?php echo $row["FullName"]?></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
         </tr>
      <?php 
            }
         }
      ?>
      </table>
   </div>
   <?php spacer(20);?>
   <div class="mypanel">
      <div class="panel-top">Detail</div>
      <div class="panel-mid">
         <div class="row">
            <div class="col-xs-12">
               <input type="checkbox" class="enabler--" name="chkEnableddeductions"  for="empdeductions">
               <label id="enable">Enable Fields</label>
            </div> 
         </div>
         <div class="row margin-top">
            <div class="col-xs-12">
               <div class="row margin-top">
                  <div class="col-xs-2 txt-right" class="label">
                     <label>Deduction Name:</label>
                  </div>
                  <div class="col-xs-4">
                     <select class="saveFields-- form-input" name="DIDeductionName">
                        <option></option>
                        <option>Employee Association</option>
                        <option>Loyalty Card</option>
                        <option>Savings</option>
                        <option>Cooperative</option>
                     </select>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2 txt-right" class="label">
                     <label>Amount:</label>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="DIAmount" class="saveFields-- form-input number--">
                  </div>
                  <div class="col-xs-2"></div>
                  <div class="col-xs-2 txt-right" class="label">
                     <label>Date Start:</label>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="DIDateStart" class="saveFields-- form-input date--">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2 txt-right" class="label">
                     <label>Pay Rate:</label>
                  </div>
                  <div class="col-xs-2">
                     <?php $ftable = "payrate";
                        createSelect($ftable,"srch".$ftable."RefId","",100,"Name","Select DIPayrate","");
                     ?>
                  </div>
                  <div class="col-xs-2"></div>
                  <div class="col-xs-2 txt-right" class="label">
                     <label>Date End:</label>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="DateEnd" class="saveFields-- form-input date--">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2 txt-right" class="label">
                     <label>Pay Period:</label>
                  </div>
                  <div class="col-xs-2">
                     <select class="saveFields-- form-input" name="DIPayPeriod">
                        <option></option>
                        <option>Both</option>
                        <option>Monthly</option>
                        <option>Weekly</option>
                        <option>First Half</option>
                        <option>Second Half</option>
                     </select>
                  </div>
                  <div class="col-xs-2 txt-center">
                     <input type="checkbox" name="DITerminate">&nbsp;<label>Terminate</label>
                  </div>
                  <div class="col-xs-2" style="text-align: right;" class="label">
                     <label>Date Terminated:</label>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="DIDateTerminated" class="saveFields-- form-input date--">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="panel-bottom"></div>
   </div>
</div>