<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript">
      </script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar($paramTitle); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div class="mypanel">
                        <div class="panel-top">Employees Search</div>
                        <div class="panel-mid">
                                 <?php
                                    $EmpRefId = getvalue("txtRefId");
                                    $attr = ["empRefId"=>getvalue("txtRefId"),
                                             "empLName"=>getvalue("txtLName"),
                                             "empFName"=>getvalue("txtFName"),
                                             "empMName"=>getvalue("txtMidName")];
                                    $EmpRefId = EmployeesSearch($attr);
                                 ?>
                        </div>
                     </div>
                     <?php
                        if ($EmpRefId == 0 && $GLOBALS['UserCode'] == "COMPEMP") {
                           $EmpRefId = getvalue("hEmpRefId");
                        }
                        if (!($EmpRefId == ""))
                        {
                     ?>
                     <div class="panel-mid margin-top10" style="border-top:3px solid #222">
                           <?php
                              btnSACABA([true,false,true]);
                              spacer(10);
                           ?>
                           <div class="row margin-top">
                              <div class="col-xs-2">
                                 <span class="label">Code:</span>
                              </div>
                              <div class="col-xs-2">
                                 <input type="text" class="form-input saveFields--">
                              </div>
                              <div class="col-xs-2"></div>
                              <div class="col-xs-1">
                                 <input type="radio" name="schedType" id="schedTypeFix">&nbsp;Fix
                              </div>
                              <div class="col-xs-1">
                                 <input type="radio" name="schedType" id="schedTypeFlexi">&nbsp;Flexi
                              </div>
                              <div class="col-xs-1">
                                 <input type="radio" name="schedType" id="schedTypeCompress">&nbsp;Compress Work
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-2">
                                 <span class="label">Schedule:</span>
                              </div>
                              <div class="col-xs-2">
                                 <input type="text" class="form-input saveFields--">
                              </div>
                           </div>
                           <?php spacer (30);?>
                           <div class="row">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-1 txt-center"><span class="label">AM (In)</span></div>
                              <div class="col-xs-1 txt-center"><span class="label">MID (Out)</span></div>
                              <div class="col-xs-1 txt-center"><span class="label">MID (In)</span></div>
                              <div class="col-xs-1 txt-center"><span class="label">PM (Out)</span></div>
                              <div class="col-xs-1 txt-center"><span class="label">Strict MID</span></div>
                              <div class="col-xs-1 txt-center"><span class="label">Flexi MID Break</span></div>
                              <div class="col-xs-1 txt-center"><span class="label">Flexi</span></div>
                              <div class="col-xs-1 txt-center"><span class="label">Flexi Time</span></div>
                              <div class="col-xs-1 txt-center"><span class="label">Restday</span></div>
                              <div class="col-xs-1 txt-center"><span class="label">Whole Day</span></div>
                              <div class="col-xs-1 txt-center"><span class="label">Half Day</span></div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"><span class="label">Sunday</span></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="radio" name="sunday" id="sun_strictMID" class="form-input"></div>
                              <div class="col-xs-1"><input type="radio" name="sunday" id="sun_flexiMID" class="form-input"></div>
                              <div class="col-xs-1"><input type="radio" name="sunday" id="sun_flexi" class="form-input"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="radio" name="sunday" id="sun_restDay" class="form-input"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <input type="hidden" name="" id="sunday_flexi" value="" class="saveFields--">
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"><span class="label">Monday</span></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="radio" name="monday" id="mon_strictMID" class="form-input"></div>
                              <div class="col-xs-1"><input type="radio" name="monday" id="mon_flexiMID" class="form-input"></div>
                              <div class="col-xs-1"><input type="radio" name="monday" id="mon_flexi" class="form-input"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="radio" name="monday" id="mon_restDay" class="form-input"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <input type="hidden" name="" id="monday_flexi" value="" class="saveFields--">
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"><span class="label">Tuesday</span></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="radio" name="tuesday" id="tue_strictMID" class="form-input"></div>
                              <div class="col-xs-1"><input type="radio" name="tuesday" id="tue_flexiMID" class="form-input"></div>
                              <div class="col-xs-1"><input type="radio" name="tuesday" id="tue_flexi" class="form-input"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="radio" name="tuesday" id="tue_restDay" class="form-input"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <input type="hidden" name="" id="tuesday_flexi" value="" class="saveFields--">
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"><span class="label">Wednesday</span></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="radio" name="wednesday" id="wed_strictMID" class="form-input"></div>
                              <div class="col-xs-1"><input type="radio" name="wednesday" id="wed_flexiMID" class="form-input"></div>
                              <div class="col-xs-1"><input type="radio" name="wednesday" id="wed_flexi" class="form-input"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="radio" name="wednesday" id="wed_restDay" class="form-input"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <input type="hidden" name="" id="wednesday_flexi" value="" class="saveFields--">
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"><span class="label">Thursday</span></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="radio" name="thursday" id="thur_strictMID" class="form-input"></div>
                              <div class="col-xs-1"><input type="radio" name="thursday" id="thur_flexiMID" class="form-input"></div>
                              <div class="col-xs-1"><input type="radio" name="thursday" id="thur_flexi" class="form-input"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="radio" name="thursday" id="thur_restDay" class="form-input"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <input type="hidden" name="" id="thursday_flexi" value="" class="saveFields--">
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"><span class="label">Friday</span></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="radio" name="friday" id="fri_strictMID" class="form-input"></div>
                              <div class="col-xs-1"><input type="radio" name="friday" id="fri_flexiMID" class="form-input"></div>
                              <div class="col-xs-1"><input type="radio" name="friday" id="fri_flexi" class="form-input"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="radio" name="friday" id="fri_restDay" class="form-input"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <input type="hidden" name="" id="friday_flexi" value="" class="saveFields--">
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"><span class="label">Saturday</span></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="radio" name="saturday" id="sat_strictMID" class="form-input"></div>
                              <div class="col-xs-1"><input type="radio" name="saturday" id="sat_flexiMID" class="form-input"></div>
                              <div class="col-xs-1"><input type="radio" name="saturday" id="sat_flexi" class="form-input"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="radio" name="saturday" id="sat_restDay" class="form-input"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <input type="hidden" name="" id="saturday_flexi" value="" class="saveFields--">
                           </div>
                           <?php spacer(20);?>
                           <div class="row">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-1 txt-center"><span class="label">O.T (In)</span></div>
                              <div class="col-xs-1 txt-center"><span class="label">O.T Start Time</span></div>
                              <div class="col-xs-1 txt-center"><span class="label">Minimum O.T</span></div>
                              <div class="col-xs-1 txt-center"><span class="label">O.T Needs Aprroval</span></div>
                              <div class="col-xs-1 txt-center"><span class="label">Advance O.T</span></div>
                              <div class="col-xs-1 txt-center"><span class="label">Advance O.T Start Time</span></div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"><span class="label">Sunday</span></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="radio" name="sunday" id="sun_approval" class="form-input"></div>
                              <div class="col-xs-1"><input type="radio" name="sunday" id="sun_advanceOT" class="form-input"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <input type="hidden" name="" id="sunday_OT" value="" class="saveFields--">
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"><span class="label">Monday</span></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="radio" name="monday" id="mon_approval" class="form-input"></div>
                              <div class="col-xs-1"><input type="radio" name="monday" id="mon_advanceOT" class="form-input"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <input type="hidden" name="" id="monday_OT" value="" class="saveFields--">
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"><span class="label">Tuesday</span></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="radio" name="tuesday" id="tue_approval" class="form-input"></div>
                              <div class="col-xs-1"><input type="radio" name="" id="tue_advanceOT" class="form-input"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <input type="hidden" name="" id="tuesday_OT" value="" class="saveFields--">
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"><span class="label">Wednesday</span></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="radio" name="wednesday" id="wed_approval" class="form-input"></div>
                              <div class="col-xs-1"><input type="radio" name="wednesday" id="wed_advanceOT" class="form-input"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <input type="hidden" name="" id="wednesday_OT" value="" class="saveFields--">
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"><span class="label">Thursday</span></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="radio" name="thursday" id="thur_approval" class="form-input"></div>
                              <div class="col-xs-1"><input type="radio" name="thursday" id="thur_advanceOT" class="form-input"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <input type="hidden" name="" id="thursday_OT" value="" class="saveFields--">
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"><span class="label">Friday</span></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="radio" name="friday" id="fri_approval" class="form-input"></div>
                              <div class="col-xs-1"><input type="radio" name="friday" id="fri_advanceOT" class="form-input"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <input type="hidden" name="" id="friday_OT" value="" class="saveFields--">
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"><span class="label">Saturday</span></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <div class="col-xs-1"><input type="radio" name="saturday" id="sat_approval" class="form-input"></div>
                              <div class="col-xs-1"><input type="radio" name="saturday" id="sat_advanceOT" class="form-input"></div>
                              <div class="col-xs-1"><input type="text" class="form-input saveFields--"></div>
                              <input type="hidden" name="" id="saturday_OT" value="" class="saveFields--">
                           </div>
                     </div>
                     <?php
                        }
                        spacer (30);
                     ?>


                  </div>
               </div>
            </div>
            <?php
               footer();
               doHidden("paramTitle",$paramTitle,"");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>

   </body>
</html>