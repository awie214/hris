<?php
   //session_start();
   include 'colors.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   include 'incRptSortBy.e2e.php';
   $rs = SelectEach($table,$whereClause);
   if ($rs) $rowcount = mysqli_num_rows($rs);
   if ($dbg) { echo "DBG >> ".$whereClause; }

   function doColumn($row,$s) {
      echo '<tr>';
         if ($s == 0) {
            echo '<th nowrap>Employees Name</th>';
         }
         else {
            echo '<td nowrap valign="top" style="padding-left:5px;">';
            if (getvalue("chkEmpRefId") == 'true') {
               echo $row["RefId"]." ";
            }
            echo $row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"].'</td>';
         }
         if (getvalue("chkPosition") == 'true'){
            if ($s==0) {
            echo '<th nowrap>Position</th>';
            } else {
               echo '<td nowrap></td>';
            }
         }
         if (getvalue("chkSG") == 'true'){
            if ($s==0) {
            echo '<th nowrap>SG</th>';
            } else {
               echo '<td nowrap></td>';
            }
         }
         if (getvalue("chkSS") == 'true'){
            if ($s==0) {
            echo '<th nowrap>SS</th>';
            } else {
               echo '<td nowrap></td>';
            }
         }
         if (getvalue("chkEducation") == 'true'){
            if ($s==0) {
            echo '<th nowrap>Education</th>';
            } else {
               echo '<td nowrap valign="top" style="padding-left:10px;">'.$GLOBALS["Education"].'</td>';
            }
         }
         if (getvalue("chkTraining") == 'true'){
            if ($s==0) {
            echo '<th nowrap>Training</th>';
            } else {
               echo '<td nowrap valign="top">'.$GLOBALS["TrainingDes"].'</td>';
            }
         }
            //'.$row[""].'
      echo '</tr>';
   }

   $recordsCount = 0;
?>

<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader("Employees List of Training and Education");
         ?>
         <table border="1">
            <tr>
            <?php
               doColumn("",0);
            ?>
            </tr>
            <?php
               $empRefid = "";
               while ($row = mysqli_fetch_assoc($rs))
               {
                  $empRefid = $row['RefId'];

                  $sql = "SELECT * FROM employeestraining where EmployeesRefId = $empRefid ORDER BY StartDate";
                  $resultTraining = mysqli_query($conn,$sql) or die(mysqli_error($conn));
                  $numrow = mysqli_num_rows($resultTraining);
                  $TrainingDes = "";
                  if ($numrow) {
                     while ($row_Training = mysqli_fetch_assoc($resultTraining)) {
                        $TrainingDes .= "<div>".$row_Training["Description"]."</div>";
                     }
                  }
                  $sql = "SELECT * FROM employeeseduc where EmployeesRefId = $empRefid ORDER BY LevelType";
                  $resultEduc = mysqli_query($conn,$sql) or die(mysqli_error($conn));
                  $numrow = mysqli_num_rows($resultEduc);
                  $Education = "";
                  if ($numrow) {
                     while ($row_Education = mysqli_fetch_assoc($resultEduc)) {
                        $Education .= "<div>".getRecord("schools",
                                                        $row_Education["SchoolsRefId"],
                                                        "Name")." - (".$row_Education["YearGraduated"].")</div>";

                     }
                  }
                  if ($TrainingDes!="" || $Education != "") {
                     $recordsCount++;
                     doColumn($row,1);
                  }
               }
               echo '</table>';
               echo "RECORD COUNT : ".$recordsCount;
            ?>
         <?php
            echo
            '<div>SEARCH CRITERIA:</div>';
            if ($searchCriteria == "") echo "<li>ALL RECORDS</li>";
            rptFooter();
         ?>
      </div>
   </body>
</html>