<?php
   session_start();
   require_once "conn.e2e.php";
   require_once "constant.e2e.php";
   require_once pathClass.'0620functions.e2e.php';
   $CompanyId     = getvalue("hCompanyID");
   $BranchId      = getvalue("hBranchID");
?>
<div class="row padd10">
   <div class="col-xs-12">
      <?php

  
         $gridTableHdr_arr = ["Employees ID#","Last Name", "First Name"];
         $gridTableFld_arr = ["AgencyId","LastName", "FirstName"];

         $sql = "SELECT * FROM `employees` where CompanyRefId = $CompanyId and BranchRefId = $BranchId";
         $sql .= " and LastName != '' and FirstName != ''";
         $sql .= " AND (Inactive != 1 OR Inactive IS NULL)";

         if (!empty(getvalue("refid")) && getvalue("refid") != "undefined") {$sql .= " and RefId = ".getvalue("refid");}
         if (!empty(getvalue("lname")) && getvalue("lname") != "undefined") {$sql .= " and LastName like '".getvalue("lname")."%'";}
         if (!empty(getvalue("fname")) && getvalue("fname") != "undefined") {$sql .= " and FirstName like '".getvalue("fname")."%'";}
         if (!empty(getvalue("mname")) && getvalue("mname") != "undefined") {$sql .= " and MiddleName like '".getvalue("mname")."%'";}
         /*url += "&agency=" + $("[name='srchAgencyRefId']").val();
         url += "&position=" + $("[name='srchPositionRefId']").val();
         url += "&office=" + $("[name='srchOfficeRefId']").val();
         url += "&division=" + $("[name='srchDivisionRefId']").val();
         url += "&dept=" + $("[name='srchDepartmentRefId']").val();
         url += "&empstatus=" + $("[name='srchEmpStatusRefId']").val();*/
         $sql .= " ORDER BY LastName, FirstName LIMIT 200";
         doGridTable("employees",
                     $gridTableHdr_arr,
                     $gridTableFld_arr,
                     $sql,
                     [false,false,false,true],
                     "EmpLkUp_GridTable");
      ?>

   </div>
</div>


<script type="text/javascript">
   $('#EmpLkUp_GridTable').DataTable();
   var table = $('#EmpLkUp_GridTable').DataTable();
   // Sort by columns 1
   table
      .order([ 0, 'desc' ])
      .draw();
</script>