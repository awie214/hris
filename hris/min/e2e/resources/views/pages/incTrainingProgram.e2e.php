 
   <div class="mypanel">
      <div class="panel-top" for="TrainProg">
         <div class="row">
            <div class="col-sm-4 txt-center">
               <!-- 
               TITLE OF SEMINAR/CONFERENCE
               /WORKSHOP/SHORTCOURSES
               <br>(Write in full)
               -->
               TRAINING/SEMINAR/CONFERENCE<br>/WORKSHOP ATTENDED
            </div>
            <div class="col-sm-3 txt-center">
               <div class="row">
                  INCLUSIVE DATES
               </div>
               <div class="row">
                  <div class="col-sm-6 txt-center margin-top">
                     FROM
                  </div>
                  <div class="col-sm-6 txt-center margin-top">
                     TO
                  </div>
               </div>
            </div>
            <div class="col-sm-1 txt-center">
               NUMBERS OF HOURS
            </div>
            <div class="col-sm-4 txt-center">
               CONDUCTED SPONSORED BY<br>
               (Write in full)
            </div>
         </div>
         <hr>   
         <div class="row">   
            <div class="col-sm-3 txt-center" style="margin-top:10px;">
               L&D TYPE
            </div>
            <div class="col-sm-3 txt-center" style="margin-top:10px;">
               L&D CLASS
            </div>
            <div class="col-sm-6 txt-center" style="margin-top:10px;">
               DESCRIPTION
            </div>
         </div>
      </div>
      <div class="panel-mid-litebg" id="TrainProg">
         <?php
            $createEntry = 0;
            if (getvalue("hUserGroup") == "COMPEMP") {
               if (getvalue("hCompanyID") == 21) {
                  $rs = SelectEach("employeestraining","WHERE CompanyRefId = $CompanyId AND BranchRefId = $BranchId AND EmployeesRefId = $EmployeesRefId ORDER BY StartDate DESC, RefId DESC LIMIT 30");
               } else {
                  $rs = SelectEach("employeestraining","WHERE CompanyRefId = $CompanyId AND BranchRefId = $BranchId AND EmployeesRefId = $EmployeesRefId ORDER BY StartDate DESC, RefId DESC");
               }
               $j = 0;
               if ($rs) {
                  $createEntry = mysqli_num_rows($rs);
               } else {
                  $createEntry = 1;
               }
            } else {
               $createEntry = 1;
            }
            $disabled = "disabled";
            for ($j=1;$j<=$createEntry;$j++) {
         ?>

               <div id="EntryTrainingProg_<?php echo $j; ?>" class="entry201">
                  <input type="checkbox" id="TrainProg_<?php echo $j; ?>" name="chkTrainProg_<?php echo $j; ?>" 
                     class="enabler-- training_fpreview" refid="" 
                     fldName="bint_SeminarsRefId_<?php echo $j; ?>,
                     date_StartDate_<?php echo $j; ?>_TrainProg,
                     date_EndDate_<?php echo $j; ?>_TrainProg,
                     sint_NumofHrs_<?php echo $j; ?>_TrainProg,
                     bint_SponsorRefId_<?php echo $j; ?>,
                     bint_SeminarPlaceRefId_<?php echo $j; ?>,
                     bint_SeminarTypeRefId_<?php echo $j; ?>,
                     sint_SeminarClassRefId_<?php echo $j; ?>"
                     idx="<?php echo $j; ?>" 
                     unclick="CancelAddRow">
                  <input type="hidden" name="trainingRefId_<?php echo $j; ?>" >
                  <label for="TrainProg_<?php echo $j; ?>" class="btn-cls2-sea"><b>EDIT TRAINING PROGRAMS #<?php echo $j; ?></b></label>
                  <div class="row margin-top">
                     <div class="col-sm-4 tip--">
                        <?php
                           createSelect("Seminars",
                                       "bint_SeminarsRefId_".$j,
                                       "",100,"Name","Select Title of L&D",$disabled);
                        ?>
                        <span class="tipText">LEARNING AND DEVELOPMENT</span>
                     </div>
                     <div class="col-sm-3">
                        <div class="row">
                           <div class="tip-- pull-left" style="margin-right:5px;">
                              <input type="text" class="form-input date-- saveFields-- datefrom mandatory valDate--" 
                                 placeholder="Start Date" label-tip="Start Date of Seminar"
                                 id="SemStartDate_<?php echo $j; ?>" tabname="Training Programs" 
                                 name="date_StartDate_<?php echo $j."_TrainProg"; ?>" <?php echo $disabled; ?> readonly>
                              <span class="tipText">Start Date of Learning and Development</span>
                           </div>   
                           <div class="tip-- pull-left" style="margin-right:5px;">
                              <input type="text" class="form-input date-- saveFields-- dateto mandatory valDate--" for="SemStartDate_<?php echo $j; ?>" 
                                 tabname="Training Programs" placeholder="End Date" id="SemEndDate_<?php echo $j; ?>" tabname="Training Programs" 
                                 name="date_EndDate_<?php echo $j."_TrainProg"; ?>" <?php echo $disabled; ?> readonly>
                                 <span class="tipText">End Date of Learning and Development</span>
                           </div>
                           <?php doChkPresent($j,8); ?>   
                        </div>
                     </div>
                     <div class="col-sm-1 tip--">
                        <input type="text" class="form-input number-- saveFields-- nonzero" tabname="Training Programs" placeholder="Hrs.?"
                           id="NumHrs_<?php echo $j; ?>" name="sint_NumofHrs_<?php echo $j."_TrainProg"; ?>" <?php echo $disabled; ?>>
                        <span class="tipText">Number of Hours</span>
                     </div>
                     <div class="col-sm-4 tip--">
                        <?php
                           createSelect("Sponsor",
                                       "bint_SponsorRefId_".$j,
                                       "",100,"Name","Select L&D Sponsor",$disabled);
                        ?>
                        <span class="tipText">Learning and Development Sponsor</span>
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-sm-3 tip--">
                        <?php
                           createSelect("SeminarType",
                                       "bint_SeminarTypeRefId_".$j,
                                       "",100,"Name","Select Type of L&D",$disabled);
                        ?>
                        <span class="tipText">Learning and Development Type</span>
                     </div>
                     <div class="col-sm-3 tip--">
                        <?php
                           createSelect("SeminarClass",
                                       "sint_SeminarClassRefId_".$j,
                                       "",100,"Name","Select L&D Classification",$disabled);
                        ?>
                        <span class="tipText">Learning and Development Classification</span>
                     </div>
                     <div class="col-sm-6 tip--">
                        <input type="text" class="form-input" 
                        placeholder="Description" 
                        tabname="Training Programs" 
                        name="char_Description_<?php echo $j; ?>" <?php echo $disabled; ?> readonly>
                        <span class="tipText">Description</span>
                     </div>
                  </div>
               </div>
         <?php 
            } 
         ?>
      </div>
      <div class="panel-bottom bgSilver"><a href="javascript:void(0);" class="addRow" id="addRowTraining">Add Row</a></div>
   </div>
<script type="text/javascript">
   $("#TrainProg  select, #TrainProg [name*='date_']").addClass("mandatory");
   $("#TrainProg .saveFields--").attr("tabname","Training Programs");
   $("#TrainProg #FM_SeminarClass").hide();
   $("#TrainProg #FM_SeminarType").hide();
   $("[name*='bint_SeminarsRefId_']").attr("onchange","getSeminarDes(this.value,$(this).attr('name'))");
   function getSeminarDes(val,name) {
      var id = "";
      id = name.split("_")[2];
      $.get("trn.e2e.php",
      {
         fn:"getSeminarDes",
         val:val,
         idx:id
      },
      function(data,status){
         if (status == 'success') {
            eval(data);
         }
      });
   }
</script>