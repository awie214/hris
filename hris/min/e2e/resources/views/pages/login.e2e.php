<?php
   session_start();
   require_once "conn.e2e.php";
   require_once "constant.e2e.php";
   require_once pathClass."0620functions.e2e.php";
   require_once pathClass."SysFunctions.e2e.php";

   $_SESSION["cssFile"] = "CSS_PIS".f_encode(date("Ymd",time())).".css";
   $sys = new SysFunctions();
   $sys->destroy_css("CSS_PIS");
   $css = ["armyBlue1","armyBlue2","bgPIS"];
   //$mynewfile = $sys->css_create($css);
   //$_SESSION["cssFilePath"] = $mynewfile;
/**
 * Copy a file, or recursively copy a folder and its contents
 * @author      Aidan Lister <aidan@php.net>
 * @version     1.0.1
 * @link        http://aidanlister.com/2004/04/recursively-copying-directories-in-php/
 * @param       string   $source    Source path
 * @param       string   $dest      Destination path
 * @param       int      $permissions New folder creation permissions
 * @return      bool     Returns true on success, false on failure
 */
function xcopy($source, $dest, $permissions = 0755) {
    // Check for symlinks
    if (is_link($source)) {
        return symlink(readlink($source), $dest);
    }

    // Simple copy for a file
    if (is_file($source)) {
        return copy($source, $dest);
    }

    // Make destination directory
    if (!is_dir($dest)) {
        mkdir($dest, $permissions);
    }

    // Loop through the folder
    $dir = dir($source);
    while (false !== $entry = $dir->read()) {
        // Skip pointers
        if ($entry == '.' || $entry == '..') {
            continue;
        }

        // Deep copy directories
        xcopy("$source/$entry","$dest/$entry",$permissions);
    }

    // Clean up
    $dir->close();
    return true;
}

   $path = "../../../public/";
   $pw = getvalue('hToken');
   $u  = getvalue('txtUser');
   $module = getvalue("hModule");
   $Terminal = getMAC();
   $rowCompany = FindFirst("company","","*");


   $criteria  = " WHERE UserName = '$u'";
  $criteria .= " AND Password = '$pw'";
   $criteria .= " LIMIT 1";
   $recordSet = f_Find("sysuser",$criteria);
   $from = "";
   $Proceed = false;
   if ($recordSet) {
      $Proceed = true;
      $from = "sysuser";
   } else {
      $criteria  = " WHERE UserName = '$u'";
      $criteria .= " AND pw = '$pw'";
      $criteria .= " LIMIT 1";
      $recordSet = f_Find("employees",$criteria);
      if ($recordSet) {
         $Proceed = true;
         $from = "employees";
      }
   }

   if ($Proceed) {
      $rowcount = mysqli_num_rows($recordSet);
      $row = mysqli_fetch_assoc($recordSet);
      if ($from == "employees") {
         $EmployeesRefId = $row["RefId"];
      } else {
         $EmployeesRefId = $row["EmployeesRefId"];
         $userRefId = $row["RefId"];
      }

      $t = time();
      $session = $t.date("Ymd",$t);
      $session = f_encode($session);
      if ($rowcount) {
         $date_today = date("Y-m-d",time());
         $curr_time  = date("H:i:s",time());
         $trackingA_fld = "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
         $trackingA_val = "'$date_today', '$curr_time', 'PHP', 'A'";

         $sql  = "";
         $sql .= "UPDATE `$from` SET Session = '$session',";
         $sql .= "isLogin = 1, ";
         $sql .= "Terminal = '$Terminal'";
         if ($from == "sysuser"){
            $sql .= ", LastLoginTime = '$curr_time', ";
            $sql .= "LastLoginDate = '$date_today'";
         }

         $sql .= $criteria;
         if ($conn->query($sql) === TRUE) {

            $login = fopen($sys->publicPath("syslog/userLogin_".date("Ymd",time()).".log"), "a+");

            if ($from == "employees") {
               $UserCode = "COMPEMP";
               $_SESSION['sess_user_refid'.$u]  = 0;
               $_SESSION['sess_ulvl'.$u]        = 0;
               $_SESSION['SysGroupRefId'.$u]    = 0;
            } else {
               $UserCode = FFirstRefId("SysGroup",$row["SysGroupRefId"],"Code"); 
               $rowEmployees = FFirstRefId("employees",$EmployeesRefId,"*");
               $_SESSION['sess_user_refid'.$u]  = $row["RefId"];
               $_SESSION['sess_ulvl'.$u]        = $row["Level"];
               $_SESSION['SysGroupRefId'.$u]    = $row["SysGroupRefId"];
            }

            $_SESSION['AUTH_PAGE'] = "init";
            $CompanyId  = $rowCompany["RefId"];
            $BranchId   = $rowCompany["BranchRefId"];
            $CompCode   = $rowCompany["Code"];

            $gPARAM  = "hSess=".$session;
            $gPARAM .= "&hUser=".$u;
            $gPARAM .= "&hUserRefId=".$row["RefId"];
            //$gPARAM .= "&hUserRefId=".$EmployeesRefId;
            $gPARAM .= "&hCompanyID=$CompanyId";
            $gPARAM .= "&hBranchID=$BranchId";
            $gPARAM .= "&hsysgrp=".$row["SysGroupRefId"];
            $gPARAM .= "&hucode=$UserCode";
            $gPARAM .= "&hUserGroup=$UserCode";
            $gPARAM .= "&ccode=$CompCode";
            $gPARAM .= "&hEmpRefId=".$EmployeesRefId;
            $gPARAM .= "&h=Secret";
            $gPARAM .= "&loginT=".date("Y-m-d")." ".date("h:i:s A");
            
            $_SESSION['user']                = $u;
            $_SESSION['isLoggedIn']          = true;
            $_SESSION['pageSession'.$u]      = "init";
            $_SESSION['path']                = $path;
            $_SESSION['idx']                 = $CompanyId;
            $_SESSION['sess_Session'.$u]     = $session;
            $_SESSION['sess_gParam'.$u]      = $gPARAM;
            $_SESSION['sess_user'.$u]        = $u;
            $_SESSION['EmployeesRefId'.$u]   = $EmployeesRefId;
            $_SESSION['UserCode'.$u]         = $UserCode;
            $_SESSION['CompanyCode'.$u]      = $CompCode;
            $_SESSION['sess_deptid'.$u]      = $rowEmployees["DeptRefId"];
            $_SESSION['sess_logintime'.$u]   = date("h:i:s A");
            $_SESSION['sess_logindate'.$u]   = date("Y-m-d");
            $_SESSION['userFullName'.$u]     = $rowEmployees["LastName"].", ".$rowEmployees["FirstName"];
            $_SESSION['EmployeesImage'.$u]   = $rowEmployees["PicFilename"];
            $_SESSION['MAC'.$u]              = $Terminal;
            $_SESSION['Module'.$u]           = $module;

            $flds   = "`EmployeesRefId`, `LoginDate`, `LoginTime`, `LoginAs`, `Terminal`,".$trackingA_fld;
            $values = "'$EmployeesRefId', '$date_today', '$curr_time', '$from', '$Terminal',".$trackingA_val;
            if (!($conn->query("INSERT INTO `employeeslogin` ($flds) VALUES ($values)") === TRUE)) {
               fwrite($login,"Err !!!: ".$conn->error);
               fwrite($login,"\n");
            }
            $dateLOG = date("mdY")." ".date("h:i:s A");
            fwrite($login,$dateLOG."--".str_pad($u,15," ")."--".$gPARAM."\n");
            fclose($login);
            header ("Location: GlobalCaller.e2e.php?".$gPARAM."&file=scrnWelcome&auth=init");
         } else {
            echo "Error updating record: " . $conn->error;
         }
      }
   } else {
      header ("Location: idx.e2e.php?login_error=yes&msg=$Msg");
   }
  $conn->close();
?>