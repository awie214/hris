<?php
   session_start();
	require_once "conn.e2e.php";
	require_once $_SESSION["Classes"]."0620functions.e2e.php";
   require_once $_SESSION["Classes"]."SysFunctions.e2e.php";
   $sys = new SysFunctions();



   function setobj() {
      echo '$("#txtEmpIDNO").val("");'."\n";
      echo '$("[name=\'token\']").val("");'."\n";
      echo '$("[name=\'btnPunch\']").prop("disabled",true);'."\n";
      echo '$("#txtEmpIDNO").select();'."\n";
      echo '$("#txtEmpIDNO").focus();'."\n";
   }

   $path  = "../../../public/";
   $u     = getvalue('val1');
	$pw    = getvalue('val2');
   $token = getvalue('token');
   $KindOfEntry = getvalue('kindEntry');

   $criteria  = " WHERE AgencyId = '$u'";
	$criteria .= " AND pw = '$pw'";
   $criteria .= " LIMIT 1";
   $recordSet = f_Find("employees",$criteria);
   if ($recordSet) {
      $rowcount = mysqli_num_rows($recordSet);
      $row = mysqli_fetch_assoc($recordSet);
      $t = time();
      if ($rowcount) {
         $t = time();
         $empRefId       = $row["RefId"];
         $date_today     = date("Y-m-d",$t);
         $datetoday      = date("Ymd",$t);
         $curr_time      = date("H:i:s",$t);
         $currtime_Arr   = explode(":",$curr_time);
         $AttendanceTime = $t;
         $Terminal       = getMAC();
         $LastUpdateBy   = $row["AgencyId"];
         $flds = "`CompanyRefId`, `BranchRefId`, `EmployeesRefId`, `Terminal`, `AttendanceDate`, `AttendanceTime`, `KindOfEntry`, `LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
         $values = "'1000', '1', '$empRefId', '$Terminal', '$date_today', '$AttendanceTime', '$KindOfEntry', '$date_today', '$curr_time', '$LastUpdateBy', 'A'";
         $sql = "INSERT INTO `employeesattendance` ($flds) VALUES ($values)";

         $criteria = "where EmployeesRefId != $empRefId AND AttendanceDate = '$date_today' ORDER BY AttendanceTime DESC LIMIT 1";
         $rsLastPunch = f_Find("employeesattendance",$criteria);
         if ($rsLastPunch) {
            $rowLastPunch = $rsLastPunch->fetch_assoc();
            $empPicLastPunch = FindFirst("employees","where RefId = ".$rowLastPunch["EmployeesRefId"],"PicFilename");
            echo '$("#prevPic").attr("src","../../../public/images/'.$empRefId.'/EmployeesPhoto/'.$empPicLastPunch.'");'."\n";
            echo '$("#LastPunch").html("'.getLabel_AttendanceEntry($rowLastPunch['KindOfEntry'])." - ".date("H:i:s",$rowLastPunch['AttendanceTime']).'");'
            ."\n";
         } else {
            echo '$("#prevPic").attr("src","../../../public/images/nopic.png");'."\n";
            echo '$("#LastPunch").html("&nbsp;");'."\n";
         }
         if ($conn->query($sql) === TRUE) {
            $login = fopen($sys->publicPath('syslog/Attendance_'.$datetoday.'.log'), 'a+');
            $dateLOG = date("mdY")." ".date("h:i:s A");
            fwrite($login,$dateLOG."--".str_pad($u,15," ")."--".$values."\n");
            fclose($login);
            $criteria = "where EmployeesRefId = $empRefId AND AttendanceDate = '$date_today' ORDER BY AttendanceTime DESC";
            $EmpAttendanceToday = f_Find("employeesattendance",$criteria);
            echo '$("#EmpPic").attr("src","../../../public/images/'.$row["CompanyRefId"].'/EmployeesPhoto/'.$row["PicFilename"].'");';
            echo "\n";
            $htm  = '<table border="1" class="bgSilver" style="width:80%;">';
            $htm .= '<caption><strong>'.$LastUpdateBy.' PUNCHED LIST</strong></caption>';
            $htm .= '<tr class="bgSea"><th class="padd5 txt-center">Kind Of Entry</th><th class="padd5 txt-center">Time</th></tr>';
            while ($rowAttendance = $EmpAttendanceToday->fetch_assoc()) {
               echo '$("[name=\'btnPunch'.$rowAttendance['KindOfEntry'].'\']").prop("disabled",true);'."\n";
               $Entry = getLabel_AttendanceEntry($rowAttendance['KindOfEntry']);
               $htm .='<tr><td class="padd5 txt-center">'.$Entry.'</td><td class="padd5 txt-center">'.date("H:i:s A",$rowAttendance["AttendanceTime"]).'</td></tr>';
            }
            $htm .= '</table>';
            echo "$('#PunchedList').html('".$htm."');"."\n";
            setobj();
            echo '$("#SuccessMSG").show();'."\n";
            echo '$("#SuccessMSG2").html("<strong>Success!!!</strong> '.$LastUpdateBy.' Punched.");'."\n";
         } else {
            $msg .= " Saving Error: ".$conn->error;
            if (isset($_SESSION['debugging'])) {
               if ($_SESSION['debugging']) {
                  $msg .= " --- ".$sql;
               }
            }
         }
      } else {
         //echo 'console.log("-- dbg -- NO ROWS !!!");';
         setobj();
         echo 'alert("-- dbg -- MISMATCHED NO ROWS!!!");';
      }

   } else {
      //echo 'console.log("-- dbg -- NO RS !!!");';
      setobj();
      echo 'alert("-- dbg -- MISMATCHED NO RS!!!");';
   }
   $conn->close();
?>