<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'SysFunctions.e2e.php';
   $sys = new SysFunctions();


   $tabTitle = ["Qualification Standard","Initial Interview","Examination Result","2nd Interview","Final Interview/Assessment"];
   $tabFile = ["incQualificationStandard","incInitialInterview","incExamResult","inc2ndResult","incFinalResult"];
?>

<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script type="text/javascript" src="<?php echo jsCtrl("ctrl_Evaluate"); ?>"></script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"rms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar(getvalue("paramTitle")); ?>
            <div class="container-fluid">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div>
                        <button type="button"
                             class="btn-cls4-sea"
                             id="btnUpdate" name="btnUpdate">
                           UPDATE
                        </button>
                        <button type="button"
                             class="btn-cls4-lemon"
                             id="btnPRINT" name="btnPRINT">
                           <i class="fa fa-print" aria-hidden="true"></i>&nbsp;
                           PRINT
                        </button>
                     </div>
                     <div class="mypanel">
                        <div class="row panel-top margin-top">EVALUATE</div>
                        <div class="row panel-mid">
                           <div class="col-xs-12">
                              <div class="row">
                                 <div id="spGridTable">
                                    <?php
                                          $sql = "SELECT * FROM `".strtolower($table)."` ORDER BY RefId Desc LIMIT 500";
                                          doGridTable($table,
                                                      $gridTableHdr_arr,
                                                      $gridTableFld_arr,
                                                      $sql,
                                                      [false,false,false,true],
                                                      $_SESSION["module_gridTable_ID"]);
                                    ?>
                                 </div>
                              </div>
                           </div>
                           <div class="col-xs-7">
                              <div class="row">
                                 <div class="col-xs-12">
                                    <input type="hidden" name="hTabIdx" value="1">
                                    <div class="btn-group btn-group-sm">
                                       <?php
                                          $idx = 0;
                                          $active = "";
                                          for ($j=0;$j<count($tabTitle);$j++) {
                                             $idx = $j + 1;
                                             /*if ($idx == 1) $active = "elActive";
                                                       else $active = "";*/
                                             echo
                                             '<button type="button" name="btnTab_'.$idx.'" class="btn btn-default '.$active.'">'.$tabTitle[$j].'</button>';
                                          }
                                       ?>
                                    </div>
                                    <div class="row">
                                       <div class="col-xs-12">
                                          <?php
                                             spacer(10);
                                             $idx = 0;
                                             for ($j=0;$j<count($tabTitle);$j++) {
                                                $idx = $j + 1;
                                                fTabs($idx,$tabTitle[$j],$tabFile[$j]);
                                             }
                                          ?>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row panel-bottom"></div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include_once ("varHidden.e2e.php");
            ?>
         </div>
      </form>
   </body>
</html>




