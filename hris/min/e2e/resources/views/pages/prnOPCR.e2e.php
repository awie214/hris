<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style>
         td {
            vertical-align:center;
            text-align:center;
         }
         
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader("Office Performance Commitment and Review");

         ?>
         <table border="1">
            <tr>
               <td>Reviewed By</td>
               <td>Date</td>
               <td>Approved By</td>
               <td>Date</td>
            </tr>
            <tr>
               <td>JUAN DELA CRUZ</td>
               <td></td>
               <td>JOSE MERCADO</td>
               <td></td>
            </tr>
            <tr>
               <td>Manager,Corporate Planning Department</td>
               <td></td>
               <td>Chief Operating Officer</td>
               <td></td>
            </tr>
         </table>
         <table border="1" style="margin-top:20px;width:100%;">
            <tr>
               <td rowspan="2">STRATEGIC OBJECTIVES/FUNCTIONS</td>
               <td colspan="2" rowspan="2">SUCCESS INDICATOR</td>
               <td rowspan="2">Actual Accomplishments</td>
               <td colspan="4">RATING</td>
               <td rowspan="2">REMARKS</td>
            </tr>
            <tr>
               <td>Q1</td>
               <td>E2</td>
               <td>T3</td>
               <td>A4</td>
            </tr>
            <tr>
               <td>STRATEGIC OBJECTIVES</td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
            </tr>
            <tr>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
            </tr>
         </table>
         <?php   
            rptFooter();
         ?>
      </div>
   </body>
</html>