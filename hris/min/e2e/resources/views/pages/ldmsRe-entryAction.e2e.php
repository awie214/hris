<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <style type="text/css">
         th {
            vertical-align: top;
            text-align: center;
            font-size: 8pt;
            padding: 5px;
            background: #d9d9d9;
         }
         .td-input {
            padding: 2px;
         }
      </style>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"ldms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("RE-ENTRY APPLICATION PLAN"); ?>
            <div class="container-fluid margin-top">  
               <div class="mypanel">
                  <div class="row">
                     <div class="col-sm-3">
                        <?php
                           employeeSelector();
                        ?>
                     </div>
                     <div class="col-xs-9" style="padding: 5px;">
                        <div class="row">
                           <div class="col-xs-12">
                              <button type="button" id="ldms_edit" name="ldms_edit" class="btn-cls4-sea">
                                 <i class="fa fa-edit"></i>&nbsp;EDIT
                              </button>
                              <button type="button" id="ldms_print" name="ldms_print" class="btn-cls4-lemon">
                                 <i class="fa fa-print"></i>&nbsp;PRINT
                              </button>
                           </div>
                        </div>
                        <div class="row margin-top">
                           <div class="col-xs-12">
                              <div class="panel-top margin-top">
                                 RE-ENTRY APPLICATION PLAN
                              </div>
                              <div class="panel-mid" style="padding: 10px;">
                                 <div class="row">
                                    <div class="col-xs-6">
                                       <label>LEARNER:</label>
                                       <input class="form-input" type="text" name="learner" id="learner" readonly>
                                    </div>
                                    <div class="col-xs-6">
                                       <label>DATE CONDUCTED:</label>
                                       <br>
                                       <input class="form-input date--" type="text" name="date_conducted" id="date_conducted">
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       <label>OFFICE:</label>
                                       <input class="form-input" type="text" name="office" id="office" readonly>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-12">
                                       <label>TITLE OF INTERVENTIONS:</label>
                                       <input class="form-input" type="text" name="intervention_title" id="intervention_title">
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-12">
                                       <?php bar(); ?>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-12">
                                       <label>REAP TITLE:</label>
                                       <input class="form-input" type="text" name="reap_title" id="reap_title">
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-12">
                                       <label>OBJECTIVES:</label>
                                       <input class="form-input" type="text" name="objectives" id="objectives">
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-12">
                                       <label>DURATION:</label>
                                       <input class="form-input" type="text" name="duration" id="duration">
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-12">
                                       <label>EXPECTED OUTPUTS:</label>
                                       <input class="form-input" type="text" name="outputs" id="outputs">
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-12">
                                       <label>SUCCESS INDICATORS:</label>
                                       <input class="form-input" type="text" name="success_indicator" id="success_indicator">
                                    </div>
                                 </div>
                                 <br>
                                 <div class="row margin-top">
                                    <div class="col-xs-12">
                                       <table border="1" width="100%">
                                          <thead>
                                             <tr>
                                                <th style="width: 25%;">
                                                   Specific Actions
                                                </th>
                                                <th style="width: 25%;">
                                                   Resources
                                                </th>
                                                <th style="width: 25%;">
                                                   Target Date
                                                </th>
                                                <th style="width: 25%;">
                                                   Status/Remarks
                                                </th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <?php
                                                for ($i=1; $i <=5 ; $i++) { 
                                                   echo '
                                                      <tr id="tbl_row_'.$i.'">
                                                         <td class="td-input">
                                                            <input type="text" 
                                                                   class="form-input" 
                                                                   name="actions_'.$i.'" 
                                                                   id="actions_'.$i.'">
                                                         </td>
                                                         <td class="td-input">
                                                            <input type="text" 
                                                                   class="form-input" 
                                                                   name="resources_'.$i.'" 
                                                                   id="resources_'.$i.'">
                                                         </td>
                                                         <td class="td-input">
                                                            <input type="text" 
                                                                   class="form-input" 
                                                                   name="target_date_'.$i.'" 
                                                                   id="target_date_'.$i.'">
                                                         </td>
                                                         <td class="td-input">
                                                            <input type="text" 
                                                                   class="form-input" 
                                                                   name="remarks_'.$i.'" 
                                                                   id="remarks_'.$i.'">
                                                         </td>
                                                      </tr>
                                                   ';
                                                }
                                             ?>
                                          </tbody>
                                          <tfoot>
                                             <tr>
                                                <td colspan="5" class="td-input">
                                                   <button type="button" class="btn-cls4-tree" id="add_row">
                                                      Add row
                                                   </button>
                                                   <button type="button" class="btn-cls4-red" id="delete_row">
                                                      Delete last row
                                                   </button>
                                                </td>
                                             </tr>
                                          </tfoot>
                                       </table>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       <label>BUDGETARY REQUIREMENTS</label>
                                       <input type="text" name="budget_requirement" id="budget_requirement" class="form-input">
                                    </div>
                                 </div>
                              </div>
                              <div class="panel-bottom">
                                 <input type="hidden" name="emprefid" id="emprefid">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               doHidden("paramTitle",getvalue("paramTitle"),"");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
   <script type="text/javascript">
      $(document).ready(function () {
         $("#ldms_print").click(function () {
            var refid = $("#emprefid").val();
            if (refid == "") {
               $.notify("No Employee Selected");
               return false;
            }
            $("#rptContent").attr("src","blank.htm");
            var rptFile = "rpt_ldms_reap";
            var url = "ReportCaller.e2e.php?file=" + rptFile;
            url += "&refid=" + refid;
            url += "&" + $("[name='hgParam']").val();
            $("#prnModal").modal();
            $("#rptContent").attr("src",url);
         });
         $("#add_row").click(function () {
            var last = "";
            var str = "";
            $("[id*='tbl_row_']").each(function () {
               last = $(this).attr("id").split("_")[2];
            });
            var idx = parseInt(last) + 1;
            
            str += '<tr id="tbl_row_' + idx + '">';
            str += '<td class="td-input">';
            str += '<input type="text" class="form-input" name="actions_' + idx + '" id="actions_' + idx + '">';
            str += '</td>';
            str += '<td class="td-input">';
            str += '<input type="text" class="form-input" name="resources_' + idx + '" id="resources_' + idx + '">';
            str += '</td>';
            str += '<td class="td-input">';
            str += '<input type="text" class="form-input" name="target_date_' + idx + '" id="target_date_' + idx + '">';
            str += '</td>';
            str += '<td align="center">';
            str += '<input type="text" class="form-input" name="remarks_' + idx + '" id="remarks_' + idx + '">';
            str += '</td>';
            str += '</tr>';
            $("#tbl_row_" + last +":last").after(str);
         });
         $("[id*='delete_row']").each(function () {
            $(this).click(function () {
               var last = "";
               $("[id*='tbl_row_']").each(function () {
                  last = $(this).attr("id").split("_")[2];
               });
               $("#tbl_row_" + last).remove();
            });
         });
      });
      function selectMe(emprefid) {
         $("#emprefid").val(emprefid);
      }
   </script>
</html>



