<?php
   require_once $_SESSION["Classes"]."0620functions.e2e.php";
   require_once $_SESSION["Classes"]."SysFunctions.e2e.php";
   $sys = new SysFunctions();
   $tabTitle = ["Payroll Summary","Attendance","Benefits/Allowance Info","Contribution","Loans Information","Deduction Information","Bonus Information"];
   $tabFile = ["inc_pms_trnpayrollSummary","inc_pms_trnattendance","inc_pms_trnbenefits","inc_pms_trncontribution","inc_pms_trnLoansInfo","inc_pms_trndeduction","inc_pms_trnbonus"];
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script type="text/javascript" src="<?php echo jsCtrl("ctrl_pmsTransaction"); ?>"></script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" id="e2eForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar ($paramTitle); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-sm-3 margin-top">
                     <?php employeeSelector();?>
                  </div>
                  <div class="col-sm-9">
                     <div class="row">
                        <div class="panel-top margin-top">
                           SELECTED EMPLOYEE: <span></span><span></span>
                        </div>
                        <div class="panel-mid">
                           <div class="row margin-top">
                              <div class="col-sm-12">
                                 <div class="row">
                                    <div class="col-sm-3 label">
                                       <label>DATE:</label>
                                    </div>
                                    <div class="col-sm-6">
                                       <input type="text" class="form-input date-- saveFields--">
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-sm-3 label">
                                       <label>PERIOD:</label>
                                    </div>
                                    <div class="col-sm-6">
                                       <div class="row">
                                          <select class="form-input" name="drpPeriod">
                                             <option value="monthly">MONTHLY</option>
                                             <option value="semi-monthly">SEMI-MONTHLY</option>
                                          </select>
                                       </div>
                                       <div class="row margin-top" id="half">
                                          <select class="form-input" name="drpHalf">
                                             <option value="1">FIRST HALF</option>
                                             <option value="2">SECOND HALF</option>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-sm-12">
                                       <button type="button" class="btn-cls4-sea trnbtn"
                                                name="btnSAVE" id="btnSAVE" disabled>
                                          <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                          &nbsp;PROCESS
                                       </button>
                                    </div>
                                 </div>

                              </div>
                           </div>
                        </div>
                        <div class="panel-bottom"></div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-sm-12" id="div_CONTENT">
                           <div class="container-fluid margin-top">
                              <?php if (!$EmpRefId) { ?>
                              <div class="row">
                                 <div class="col-sm-12" id="div_CONTENT">
                                    <div class="row" style="margin-top:5px;width:100%;">

                                       <div class="col-sm-12">
                                          <input type="hidden" name="hTabIdx" value="1">
                                          <div class="btn-group btn-group-sm">
                                             <?php
                                                $idx = 0;
                                                $active = "";
                                                for ($j=0;$j<count($tabTitle);$j++) {
                                                   $idx = $j + 1;
                                                   /*if ($idx == 1) $active = "elActive";
                                                             else $active = "";*/
                                                   echo
                                                   '<button type="button" name="btnTab_'.$idx.'" class="btn btn-default '.$active.'">'.$tabTitle[$j].'</button>';
                                                }
                                             ?>
                                          </div>
                                          <div class="row">
                                             <div class="col-sm-12">
                                                <?php
                                                   spacer(10);
                                                   $idx = 0;
                                                   for ($j=0;$j<count($tabTitle);$j++) {
                                                      $idx = $j + 1;
                                                      fTabs($idx,$tabTitle[$j],$tabFile[$j]);
                                                   }
                                                ?>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <?php
                              } else {
                                 alert("Information","NO Employees Selected!!!");
                                 //<input type="button" value="try" id="try">
                              }
                              ?>

                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">

                  </div>
               </div>
            <?php
               /*$EmpRefId = getvalue("txtRefId");
               $attr = ["empRefId"=>getvalue("txtRefId"),
                        "empLName"=>getvalue("txtLName"),
                        "empFName"=>getvalue("txtFName"),
                        "empMName"=>getvalue("txtMidName")];
               $EmpRefId = EmployeesSearch($attr);
               spacer(5);*/
            ?>

            </div>
            <?php
               footer();
               include "varHidden.e2e.php";
               doHidden("hRptFile","DashboardRpt","");
            ?>
         </div>
      </form>
   </body>
</html>



