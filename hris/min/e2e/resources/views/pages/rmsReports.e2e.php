<?php
   $incRptScrn="";
   $incRptScrn=getvalue("hRptScrn");
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>

   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <div class="row panel-top" style="padding-left:0px;border-radius:0;">
               <div class="col-xs-6">
                  <a href="javascript:void(0)" class="mbar" id="titleBarIcons" onclick="openNav();">
                     <i class="fa fa-chevron-left" aria-hidden="true"></i>
                  </a>&nbsp;
                  <?php echo strtoupper(getvalue("paramTitle")); ?>
               </div>
               <div class="col-xs-5">
                  Today is <span id="TimeDate"></span>
               </div>
               <div class="col-xs-1 txt-right">
                  <?php if (getvalue("hRptScrn") != "") { ?>
                     <button type="button" class="close" aria-label="Close" onclick="closeSCRN('model');">
                        <span aria-hidden="true" style="color:white;">&times;</span>
                     </button>
                  <?php } ?>
                  <a href="#" id="mHome" style="color:white;">
                     <i class="fa fa-home" aria-hidden="true" title="Home/Exit Module"></i>
                  </a>
               </div>
            </div>
            <div class="container-fluid margin-top10" id="rptCriteria">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div class="mypanel" id="ReportHead">
                        <div class="panel-top">Report Type</div>
                        <div class="panel-mid-litebg">
                           <label>Choose Kind Of Report:</label>
                           <p>
                              <select class="form-input rptCriteria-- " id="ReportKind" sys="pis" name="drpReportKind" style="width:50%;font-family:12pt;">
                                 <!--
                                 <option for="" ext="" own="E2E" value="rptEmployeesMovement">CES Executive Movement Report</option>
                                 <option for="" ext="" own="E2E" value="rptNonCESPosition">Non-CES Position</option>
                                 <option for="" ext="" own="E2E" value="rptOccupancy">Occupancy Report</option>
                                 <option for="" ext="" own="E2E" value="rptOfficialPendingCases">Officials with pending cases</option>
                                 <option for="" ext="" own="E2E" value="rptVacantCESPosition">Vacant CES Position</option>
                                 <option for="" ext="" own="E2E" value="rptFM_GSIS_OPS_UMR_01_FORM_A">FM-GSIS-OPS-UMR-01 FORM A</option>
                                 <option for="" ext="" own="E2E" value="rptFM_GSIS_OPS_UMR_02_FORM_B">FM-GSIS-OPS-UMR-01 FORM B</option>
                                 <option for="" ext="" own="E2E" value="rptFM_GSIS_OPS_UMR_03_FORM_C">FM-GSIS-OPS-UMR-01 FORM C</option>
                                 <option for="" ext="" own="E2E" value="rptFM_GSIS_OPS_UMR_04_FORM_D">FM-GSIS-OPS-UMR-01 FORM D</option>
                                 <option for="" ext="" own="E2E" value="rptFM_GSIS_OPS_UMR_05_FORM_E">FM-GSIS-OPS-UMR-01 FORM E</option>
                                 <option for="modelEmpReport" ext="" own="E2E" value="EmployeesListRpt">Employee List</option>
                                 <option for="modelEmpChildList" ext="" own="E2E" value="EmpChildRpt">Employees Child List</option>
                                 <option for="modelEmpTrainingEduc" ext="" own="E2E" value="EmpTrainingEducRpt">Employees Training and Education</option>
                                 <option for="modelReportScrn" ext="" own="E2E" value="NOSARpt">Notice of Salary Adjustment</option>
                                 <option for="modelReportScrn" ext="" own="E2E" value="NOSIRpt">Notice of Step Increment</option>
                                 <option for="modelReportScrn" ext="" own="E2E" value="ServiceRecordsRpt">Employees Service Record</option>
                                 <option for="" ext="" own="Agency" value="Form_Medical_Certificate">Medical Certificate</option>
                                 <option for="" ext="" own="E2E" value="Form_EmpMovement">Employee Movement</option>
                                 -->
                                 <?php
                                    $crit = "WHERE `CompanyRefId` = $CompanyId ";
                                    $crit .= "AND `BranchRefId` = $BranchId ";
                                    $crit .= "AND `CompanyCode` = '$CompanyCode' ";
                                    $crit .= "AND `SystemRefId` = '6'";
                                    $rs = SelectEach("reports",$crit);
                                    $htm = "yes";
                                    if ($rs) {
                                       while ($r = mysqli_fetch_array($rs)) {
                                          echo
                                          '<option for="" ext="'.$r["ExtType"].'" own="'.$r["Owner"].'" value="'.$r["Filename"].'" rptname="'.$r["Name"].'">['.$r["Code"].'] - '.$r["Name"].'</option>'."\n";
                                       }
                                    }

                                 ?>
                                 <?php
                                    $crit = "WHERE `CompanyRefId` = 0 ";
                                    $crit .= "AND `BranchRefId` = 0 ";
                                    $crit .= "AND `SystemRefId` = '6'";
                                    $rs = SelectEach("reports",$crit);
                                    $htm = "yes";
                                    if ($rs) {
                                       while ($r = mysqli_fetch_array($rs)) {
                                          echo
                                          '<option for="" ext="'.$r["ExtType"].'" own="'.$r["Owner"].'" value="'.$r["Filename"].'" rptname="'.$r["Name"].'">'.$r["Name"].'</option>'."\n";
                                       }
                                    }
                                    echo $crit;
                                 ?>
                              </select>
                           </p>
                           <?php
                              bar();
                              echo "<h5><label>Search Criteria</label></h5>";
                              require_once "incEmpSearchCriteria.e2e.php";
                              bar();
                           ?>
                           <div class="row">
                              <div class="col-xs-12" id="RptMainHolder">
                              </div>
                           </div>
                        </div>
                        <div class="panel-bottom"></div>
                     </div>
                  </div>
               </div>
               <?php spacer(10)?>
               <div>
                  <div style="text-align:center;">
                     <button type="button"
                          class="btn-cls4-sea trnbtn"
                          id="btnGENERATE" name="btnGENERATE">
                        <i class="fa fa-file" aria-hidden="true"></i>
                        &nbsp;GENERATE REPORT
                     </button>
                     <button type="button"
                          class="btn-cls4-red trnbtn"
                          id="btnEXIT" name="btnEXIT">
                        <i class="fa fa-times" aria-hidden="true"></i>
                        &nbsp;EXIT
                     </button>
                  </div>
               </div>
               <!-- Modal -->
               <div class="modal fade border0" id="prnModal" role="dialog">
                  <div class="modal-dialog border0" style="padding:0px;width:15in;height:92%;">
                     <div class="mypanel border0" style="height:100%;">
                        <div class="panel-top bgSilver">
                           <a href="#" data-toggle="tooltip" data-placement="top" title="Print Now" id="btnPRINTNOW">
                              <i class="fa fa-print" aria-hidden="true"></i>
                           </a>
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                     </div>
                  </div>
               </div>
               <?php modalEmpLkUp(); ?>
            </div>
            <?php
               footer();
               include "varHidden.e2e.php";
            ?>
            <input type="hidden" id="hRptFile" value="<?php echo getvalue("hRptFile")?>">
         </div>
      </form>
   </body>
</html>
