<?php
	include 'pms_conn_2.e2e.php';
	include 'conn.e2e.php';
	$emprefid = $_GET["hEmpRefId"];
	$hris_sql = "SELECT * FROM employees WHERE RefId = '$emprefid'";
	$hris_rs  = mysqli_query($conn,$hris_sql);

	function pms_GetName($table,$refid,$fld) {
		include 'pms_conn_2.e2e.php';
		$sql = "SELECT `$fld` FROM $table WHERE id = '$refid'";
		$rs  = mysqli_query($pms,$sql);
		if ($rs) {
			$row = mysqli_fetch_assoc($rs);
			return $row;
		} else {
			return false;
		}
	}


	function pms_GetLoan($emprefid,$name,$Ftable,$InfoTable,$FColumn,$rtnVal) {
		$refid = pms_FindFirst($Ftable,"WHERE name = '$name'");
		if ($refid) {
			$id = $refid["id"];
		} else {
			$id = 0;
		}
		if ($id > 0) {
			$pms_info = pms_FindFirst($InfoTable,"WHERE employee_id = '$emprefid' AND $FColumn = '$id'");
			if ($pms_info) {
				return $pms_info[$rtnVal];
			} else {
				return 0;
			}
		} else {
			return 0;
		}
	}
   $start_month         = date("Y-m"."-01",time());
   $first_week          = "01-07";
   $second_week         = "08-15";
   $third_week          = "16-22";
   $fourth_week         = "23-".cal_days_in_month(CAL_GREGORIAN,date("m",time()),date("Y",time()));

   $SalaryAmount        = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","SalaryAmount");
   $PositionRefId       = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","PositionRefId");
   $DepartmentRefId     = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","DepartmentRefId");
   $Position            = getRecord("position",$PositionRefId,"Name");
   $Department          = getRecord("Department",$DepartmentRefId,"Name");
	$BASIC 					= 0;
	$PERA_id 				= 0;
	$PERA 					= 0;
	$Overtime 				= 0;
	$Uniform_Allowance 	= 0;
	$CNA_PEI 				= 0;
	$Opt_Ins 				= 0;
	$Genesis 				= 0;
	$Hosp_Plus 				= 0;
   $Division            = "";
   $PositionItem        = "";
   $Office              = "";
	if ($hris_rs) {
		$hris_row 	= mysqli_fetch_assoc($hris_rs);
		$AgencyId 	= $hris_row["AgencyId"];
		$FirstName 	= $hris_row["FirstName"];
		$LastName 	= $hris_row["LastName"];
		$MiddleName = $hris_row["MiddleName"];
		$FullName   = $LastName.", ".$FirstName." ".$MiddleName;
	}
   $weekly_salary = $SalaryAmount / 4;
?>
<!DOCTYPE>
<html>
<head>
	<title></title>
	<?php include_once $files["inc"]["pageHEAD"]; ?>
	<script type="text/javascript">
      $(document).ready(function () {
         $("#btnPrint").click(function () {
            var head = $("head").html();
            printDiv('div_CONTENT',head);
         });
      });
   </script>
   <style type="text/css">
      @media print {
         #canvas {
            width: 50%;
            font-size: 8pt;
         }
         @page {
            size: a4;
         }
      }
   </style>
</head>
<body onload = "indicateActiveModules();">
   <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
      <?php $sys->SysHdr($sys,"pis"); ?>
      <div class="container-fluid" id="mainScreen">
         <?php doTitleBar ("PAYROLL"); ?>
         <div class="container-fluid margin-top">
            <button type="button" id="btnPrint" class="btn-cls4-lemon">PRINT</button>
            <div class="row">
               <div class="col-xs-6" id="div_CONTENT">
                  <div class="container-fluid rptBody" id="canvas">
                     <div class="row">
                     	<div class="col-xs-12">
                     		<b style="font-size: 12pt;">
                              OFFICE OF THE VICE PRESIDENT
                              <br>
                              OF THE PHILIPPINES
                              <br>
                              PAYSLIP
                              <br>    
                           </b>

                           <span style="font-size: 8pt;">
                              FOR THE MONTH OF <?php echo date("F Y",time()); ?>
                           </span>
                           <br><br><br>
                     	</div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           <?php echo $Department; ?>
                        </div>
                     </div>
                     <div class="row margin-top">
                     	<div class="col-xs-12">
                     		<?php echo $AgencyId." - ".$FullName; ?>
                     	</div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           Position: <?php echo $Position; ?>
                        </div>
                     </div>
                     <div class="row margin-top">
                     	<div class="col-xs-12">
                     		Monthly Salary: <b><?php echo number_format($SalaryAmount,2); ?></b>
                     	</div>
                     </div>
                     <div class="row margin-top" style="border: 1px solid black;">
                        <div class="col-xs-12">
                           <div class="row">
                              <div class="col-xs-4">
                                 <b>PAY PERIOD</b>
                              </div>
                              <div class="col-xs-4">
                                 <b>PARTICULARS</b>
                              </div>
                              <div class="col-xs-4">
                                 <b>AMOUNT</b>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row" style="border-left: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; padding: 10px;">
                        <div class="col-xs-12">
                           <div class="row">
                              <div class="col-xs-12">
                                 <b><?php echo date("F "."07",time()); ?></b>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 <b>Pay Period Salary</b>
                              </div>
                              <div class="col-xs-4 text-right">
                                 <?php echo number_format(($weekly_salary),2); ?></b>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 <b>Total Amount of Deduction</b>
                              </div>
                              <div class="col-xs-4 text-right">
                                 
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-2"></div>
                              <div class="col-xs-6">
                                 Leave Without Pay (LWOP)
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-2"></div>
                              <div class="col-xs-6">
                                 PHILHEALTH Contribution
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-2"></div>
                              <div class="col-xs-6">
                                 HDMF Pag Ibig Contribution
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-2"></div>
                              <div class="col-xs-6">
                                 GSIS Contribution
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-2"></div>
                              <div class="col-xs-6">
                                 GSIS Optional Insurance Premium
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-2"></div>
                              <div class="col-xs-6">
                                 GSIS Optional Insurance Premium2
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-2"></div>
                              <div class="col-xs-6">
                                 GSIS Cash Advance Loan
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-2"></div>
                              <div class="col-xs-6">
                                 GSIS Educhild
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-2"></div>
                              <div class="col-xs-6">
                                 GSIS Genesis Plus
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-2"></div>
                              <div class="col-xs-6">
                                 GSIS Educational Assistance Loan
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-2"></div>
                              <div class="col-xs-6">
                                 GSIS Policy Loan
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-2"></div>
                              <div class="col-xs-6">
                                 GSIS Emergency Loan
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-2"></div>
                              <div class="col-xs-6">
                                 GSIS UMID Cash Advance
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-2"></div>
                              <div class="col-xs-6">
                                 GSIS Consoloan
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-2"></div>
                              <div class="col-xs-6">
                                 HDMF Housing Loan
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-2"></div>
                              <div class="col-xs-6">
                                 HDMF Multipurpose Loan
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-2"></div>
                              <div class="col-xs-6">
                                 HDMF Calamity Loan
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-2"></div>
                              <div class="col-xs-6">
                                 HDMF Housing Loan/LMLS Loan
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-2"></div>
                              <div class="col-xs-6">
                                 NHMFC
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-2"></div>
                              <div class="col-xs-6">
                                 PLDT/Cellphone
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-2"></div>
                              <div class="col-xs-6">
                                 Withholding Tax
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-8">
                              </div>
                              <div class="col-xs-4" style="border-bottom: 1px solid black;">
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 <b>Net Pay for the Period</b>
                              </div>
                              <div class="col-xs-4 text-right">
                                 <b><?php echo number_format($weekly_salary,2); ?></b>
                              </div>
                           </div>
                           <br><br>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <b><?php echo date("F "."15",time()); ?></b>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 <b>Pay Period Salary</b>
                              </div>
                              <div class="col-xs-4 text-right">
                                 <?php echo number_format($weekly_salary,2); ?>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 <b>PERA</b>
                              </div>
                              <div class="col-xs-4 text-right" style="border-bottom: 1px solid black;">
                                 <?php echo number_format("1000",2); ?>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 <b>Net Pay for the Period</b>
                              </div>
                              <div class="col-xs-4 text-right">
                                 <b><?php echo number_format(($weekly_salary + 1000),2); ?></b>
                              </div>
                           </div>
                           <br><br>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <b><?php echo date("F "."22",time()); ?></b>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 <b>Pay Period Salary</b>
                              </div>
                              <div class="col-xs-4 text-right" style="border-bottom: 1px solid black;">
                                 <?php echo number_format($weekly_salary,2); ?>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 <b>Net Pay for the Period</b>
                              </div>
                              <div class="col-xs-4 text-right">
                                 <b><?php echo number_format($weekly_salary,2); ?></b>
                              </div>
                           </div>
                           <br><br>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <b><?php echo date("F "."31",time()); ?></b>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 <b>Pay Period Salary</b>
                              </div>
                              <div class="col-xs-4 text-right">
                                 <?php echo number_format($weekly_salary,2); ?>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 <b>PERA</b>
                              </div>
                              <div class="col-xs-4 text-right" style="border-bottom: 1px solid black;">
                                 <?php echo number_format("1000",2); ?>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 <b>Net Pay for the Period</b>
                              </div>
                              <div class="col-xs-4 text-right">
                                 <b><?php echo number_format(($weekly_salary + 1000),2); ?></b>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row" style="border-left: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; padding: 10px;">
                        <div class="col-xs-12">
                           <div class="row">
                              <div class="col-xs-6" style="text-indent: 20px;">
                                 <b>Net Pay for the Month</b>
                              </div>
                              <div class="col-xs-6 text-right">
                                 <b>P <?php echo number_format(($SalaryAmount + 2000),2); ?></b>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php
            footer();
            include "varHidden.e2e.php";
         ?>
      </div>
   </form>
</body>
</html>