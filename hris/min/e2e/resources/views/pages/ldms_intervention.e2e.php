<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>

   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post">
         <?php $sys->SysHdr($sys,"ldms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("Learning And Development Intervention"); ?>
            <div class="container-fluid margin-top">
               <div class="mypanel">
                  <div>
                     <div class="row">
                        <div class="col-xs-12" id="div_CONTENT">
                           <div class="margin-top">
                              <div id="divList">
                                 <div class="panel-top">
                                    List of Trainings & Programs
                                 </div>
                                 <div class="panel-mid">
                                    <span id="spGridTable">
                                       <?php
                                             $table = "ldmslndprogram";
                                             $gridTableHdr_arr = ["Course Title","Trainings Institution","Start Date","End Date","Venue","Cost"];
                                             $gridTableFld_arr = ["Name","TrainingInstitution","StartDate","EndDate","Venue","Cost"];
                                             $sql = "SELECT * FROM ldmslndprogram";
                                             $Action = [true,true,true,false];
                                             doGridTable($table,
                                                         $gridTableHdr_arr,
                                                         $gridTableFld_arr,
                                                         $sql,
                                                         $Action,
                                                         $_SESSION["module_gridTable_ID"]);
                                       ?>
                                    </span>   
                                 </div>
                                 <div class="panel-bottom">
                                    <?php
                                       btnINRECLO([true,false,false]);
                                    ?>
                                 </div>
                                 
                              </div>
                              <div id="divView">
                                 <div class="mypanel panel panel-default">
                                    <div class="panel-top">
                                       <span id="ScreenMode">INSERTING NEW INTERVENTION
                                    </div>
                                    <div class="panel-mid-litebg" id="EntryScrn">
                                       <div class="container" id="EntryScrn">
                                          <div class="row margin-top">
                                             <div class="col-xs-4">
                                                <label class="control-label" for="inputs">Semester:</label><br>
                                                <select class="form-input saveFields--" name="char_Semester" id="char_Semester">
                                                   <option value="">Select Semester</option>
                                                   <option value="January to June">January to June</option>
                                                   <option value="July to December">July to December</option>
                                                </select>
                                             </div>
                                             <div class="col-xs-4">
                                                <label class="control-label" for="inputs">Year:</label><br>
                                                <select class="form-input saveFields--" name="sint_Year" id="sint_Year">
                                                   <option value="">Select Year</option>
                                                   <?php
                                                      $from = date("Y",time()) - 5; 
                                                      $to = date("Y",time()) + 5; 
                                                      for ($i=$from; $i <= $to ; $i++) { 
                                                         if (date("Y",time()) == $i) {
                                                            echo '<option value="'.$i.'" selected>'.$i.'</option>';   
                                                         } else {
                                                            echo '<option value="'.$i.'">'.$i.'</option>';
                                                         }
                                                         
                                                      }
                                                   ?>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-8">
                                                <label class="control-label" for="inputs">Course Title:</label><br>
                                                <input class="form-input saveFields-- mandatory" 
                                                       type="text"
                                                       name="char_Name"
                                                       id="char_Name">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-8">
                                                <label class="control-label" for="inputs">Facilitator/Conducted by?</label><br>
                                                <?php
                                                   createSelect("provider",
                                                                "sint_ProviderRefId",
                                                                "",100,"Name","Select Facilitator","");
                                                ?>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-8">
                                                <label class="control-label" for="inputs">Training Institution:</label><br>
                                                <input class="form-input saveFields-- mandatory" 
                                                       type="text"
                                                       name="char_TrainingInstitution"
                                                       id="char_TrainingInstitution">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-8">
                                                <label class="control-label" for="inputs">Training Type:</label><br>
                                                <input class="form-input saveFields-- mandatory" 
                                                       type="text"
                                                       name="char_TrainingType"
                                                       id="char_TrainingType">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-2">
                                                <label class="control-label" for="inputs">Start Date:</label><br>
                                                <input class="form-input saveFields-- mandatory date--" 
                                                       type="text"
                                                       name="date_StartDate"
                                                       id="date_StartDate">
                                             </div>
                                             <div class="col-xs-2">
                                                <label class="control-label" for="inputs">End Date:</label><br>
                                                <input class="form-input saveFields-- mandatory date--" 
                                                       type="text"
                                                       name="date_EndDate"
                                                       id="date_EndDate">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-2">
                                                <label>Whole Day?</label>
                                                <select class="form-input saveFields--" name="sint_WholeDay" id="sint_WholeDay">
                                                   <option value="">Select Time</option>
                                                   <option value="0">NO</option>
                                                   <option value="1">YES</option>
                                                </select>
                                             </div>
                                             <div class="col-xs-2 time_canvas">
                                                <label class="control-label" for="inputs">Start Time:</label><br>
                                                <?php drpTime("sint_StartTime",8,30,0); ?>
                                             </div>
                                             <div class="col-xs-2 time_canvas">
                                                <label class="control-label" for="inputs">End Time:</label><br>
                                                <?php drpTime("sint_EndTime",8,30,0); ?>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-12">
                                                <input type="checkbox" name="sint_Resched" id="sint_Resched" value="0">
                                                &nbsp;&nbsp;&nbsp;If Reschedule:
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-2">
                                                <label class="control-label" for="inputs">Start Date:</label><br>
                                                <input class="form-input saveFields-- date--" 
                                                       type="text"
                                                       name="date_NewStartDate"
                                                       id="date_NewStartDate">
                                             </div>
                                             <div class="col-xs-2">
                                                <label class="control-label" for="inputs">End Date:</label><br>
                                                <input class="form-input saveFields-- date--" 
                                                       type="text"
                                                       name="date_NewEndDate"
                                                       id="date_NewEndDate">
                                             </div>
                                          </div>
                                          
                                          <div class="row margin-top">
                                             <div class="col-xs-8">
                                                <label class="control-label" for="inputs">Venue:</label><br>
                                                <input class="form-input saveFields--" 
                                                       type="text"
                                                       name="char_Venue"
                                                       id="char_Venue">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-4">
                                                <label class="control-label" for="inputs">Slots:</label><br>
                                                <input class="form-input saveFields--" 
                                                       type="text"
                                                       name="char_Slot"
                                                       id="char_Slot">
                                             </div>
                                             <div class="col-xs-4">
                                                <label class="control-label" for="inputs">Cost Per Employee:</label><br>
                                                <input class="form-input saveFields--" 
                                                       type="text"
                                                       name="char_Cost"
                                                       id="char_Cost">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-8">
                                                <label class="control-label" for="inputs">Proposed Attendees:</label><br>
                                                <input class="form-input saveFields--" 
                                                       type="text"
                                                       name="char_ProposedAttendees"
                                                       id="char_ProposedAttendees">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-4">
                                                <label class="control-label" for="inputs">Approved Budget:</label><br>
                                                <input class="form-input saveFields--" 
                                                       type="text"
                                                       name="deci_ApprovedBudget"
                                                       id="deci_ApprovedBudget">
                                             </div>
                                          </div>
<!--                                           <div class="row margin-top">
                                             <div class="col-xs-8">
                                                <label>COMPETENCY</label>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-4">
                                                <label class="control-label" for="inputs">Core:</label><br>
                                                <?php
                                                   createSelect2("competency","char_Core","",100,"Name","Select Competency","","WHERE Type = 'Core'",false);
                                                ?>
                                             </div>
                                             <div class="col-xs-3">
                                                <label class="control-label" for="inputs">Level:</label><br>
                                                <select class="form-input saveFields--" name="sint_CoreLevel" id="sint_CoreLevel" >
                                                   <option value="">Select Level</option>
                                                   <option value="1">Level 1</option>
                                                   <option value="2">Level 2</option>
                                                   <option value="3">Level 3</option>
                                                   <option value="4">Level 4</option>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-4">
                                                <label class="control-label" for="inputs">Foundation:</label><br>
                                                <?php
                                                   createSelect2("competency","char_Foundation","",100,"Name","Select Competency","","WHERE Type = 'Foundation'",false);
                                                ?>
                                             </div>
                                             <div class="col-xs-3">
                                                <label class="control-label" for="inputs">Level:</label><br>
                                                <select class="form-input saveFields--" name="sint_FoundationLevel" id="sint_FoundationLevel" >
                                                   <option value="">Select Level</option>
                                                   <option value="1">Level 1</option>
                                                   <option value="2">Level 2</option>
                                                   <option value="3">Level 3</option>
                                                   <option value="4">Level 4</option>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-4">
                                                <label class="control-label" for="inputs">Organizational:</label><br>
                                                <?php
                                                   createSelect2("competency","char_Organizational","",100,"Name","Select Competency","","WHERE Type = 'Organizational'",false);
                                                ?>
                                             </div>
                                             <div class="col-xs-3">
                                                <label class="control-label" for="inputs">Level:</label><br>
                                                <select class="form-input saveFields--" name="sint_OrganizationalLevel" id="sint_OrganizationalLevel" >
                                                   <option value="">Select Level</option>
                                                   <option value="1">Level 1</option>
                                                   <option value="2">Level 2</option>
                                                   <option value="3">Level 3</option>
                                                   <option value="4">Level 4</option>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-4">
                                                <label class="control-label" for="inputs">Leadership:</label><br>
                                                <?php
                                                   createSelect2("competency","char_Leadership","",100,"Name","Select Competency","","WHERE Type = 'Leadership'",false);
                                                ?>
                                             </div>
                                             <div class="col-xs-3">
                                                <label class="control-label" for="inputs">Level:</label><br>
                                                <select class="form-input saveFields--" name="sint_LeadershipLevel" id="sint_LeadershipLevel" >
                                                   <option value="">Select Level</option>
                                                   <option value="1">Level 1</option>
                                                   <option value="2">Level 2</option>
                                                   <option value="3">Level 3</option>
                                                   <option value="4">Level 4</option>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-4">
                                                <label class="control-label" for="inputs">Technical:</label><br>
                                                <?php
                                                   createSelect2("competency","char_Technical","",100,"Name","Select Competency","","WHERE Type = 'Technical'",false);
                                                ?>
                                             </div>
                                             <div class="col-xs-3">
                                                <label class="control-label" for="inputs">Level:</label><br>
                                                <select class="form-input saveFields--" name="sint_TechnicalLevel" id="sint_TechnicalLevel" >
                                                   <option value="">Select Level</option>
                                                   <option value="1">Level 1</option>
                                                   <option value="2">Level 2</option>
                                                   <option value="3">Level 3</option>
                                                   <option value="4">Level 4</option>
                                                </select>
                                             </div>
                                          </div> -->
                                          <div class="row margin-top">
                                             <div class="col-xs-2">
                                                <label class="control-label" for="inputs">Deadline:</label><br>
                                                <input class="form-input saveFields-- mandatory date--" 
                                                       type="text"
                                                       name="date_Deadline"
                                                       id="date_Deadline">
                                             </div>
                                             <div class="col-xs-2">
                                                <label class="control-label" for="inputs">Post?</label><br>
                                                <select class="form-input saveFields--" name="sint_Posted" id="sint_Posted">
                                                   <option value="0">Training Calender</option>
                                                   <option value="1">User Portal</option>
                                                   <option value="2">Both</option>
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel-bottom">
                                       <?php btnSACABA([true,true,true]); ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  
               </div>
            </div>
            <?php
               footer();
               $table = "ldmslndprogram";
               doHidden("paramTitle",getvalue("paramTitle"),"");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
   <script language="JavaScript">
      $(document).ready(function () {
         remIconDL();
         $("[class*='time_canvas']").hide();
         $("#sint_WholeDay").change(function () {
            var value = $(this).val();
            if (value == 0) {
               $("[class*='time_canvas']").show();
            } else {
               $("[class*='time_canvas']").hide();
            }
         });
         $("#sint_ProviderRefId").change(function () {
            $.get("ldms_transaction.e2e.php",
            {
               fn:"getFacilitatorInstitution",
               refid : $(this).val()
            },
            function(data,status){
               if (status == 'success') {
                  eval(data);
               }
            });
         });
      });
      function afterNewSave() {
         alert("Successfully Saved");
         gotoscrn("ldms_intervention","");
      }
      function afterEditSave() {
         alert("Successfully Updated");
         gotoscrn("ldms_intervention","");   
      }
   </script>
</html>



