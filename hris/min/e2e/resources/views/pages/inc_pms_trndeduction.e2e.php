<?php
   function insertIconDeduction() {
      echo
      '<a href="javascript:void(0);">
         <i class="fa fa-plus-square" aria-hidden="true" id="deductionInsert" title="INSERT NEW" style="color:white;"></i>
      </a>';
   }
   function dobtnDeduction() {
      echo
      '<hr>
      <div class="row">
         <div class="col-xs-12 txt-center">';
               createButton("Save","btnLocSaveItem","btn-cls4-sea","fa-floppy-o","");
               createButton("Cancel","btnLocCancelItem","btn-cls4-red","fa-undo","");
         echo
         '</div>
      </div>';
   }

?>
<script>
   $(document).ready(function () {
      $("#deductionInsert").click(function () {
         $("#hmode").val("ADD");
         $(".saveFields--").val("");
         $("#deductionTableSet").modal();
      });
   });
</script>
<div class="mypanel">
   <div class="row margin-top" id="newBenefits">
      <div class="col-xs-12">
         <div class="panel-top">
            <?php insertIconDeduction(); ?> <label>INSERT DEDUCTION</label>
         </div> 
         <div class="panel-mid">
            <?php
               $table = "employees";
               $tableHdr = ["Deduction Name","Amount","Date Start","Date End","Terminated"];
               $tableFld = ["","","","",""];
               $sql = "SELECT * FROM `$table` WHERE RefId = 0 ORDER BY RefId Desc LIMIT 100";
               $action = ["true","true","true"];
               doGridTable($table,
                           $tableHdr,
                           $tableFld,
                           $sql,
                           $action,
                           "Deduction");
            ?>
         </div>
         <div class="panel-bottom"></div>
      </div>
   </div>
</div>
<!--modal-->
<div class="modal fade modalFieldEntry--" id="deductionTableSet" role="dialog">
   <div class="modal-dialog" style="width:75%;">
      <div class="mypanel" style="height:100%;">
         <div class="panel-top bgSea">
            <span id="modalTitle" style="font-size:11pt;">NEW DEDUCTION INFORMATION</span>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="panel-mid">
            <div class="row" style="padding:10px;">
               <div class="col-xs-12">
                  <div class="row">
                     <div class="col-xs-3 label">
                        <label>Deduction Name:</label>
                     </div>
                     <div class="col-xs-6">
                        <select name="DeductionName" class="form-input" autofocus>
                           <option></option>
                           <option>Employee Association</option>
                        </select>
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-3 label">
                        <label>Amount:</label>
                     </div>
                     <div class="col-xs-3">
                        <input name="DIAmount" type="text" class="form-input number--">
                     </div>
                     <div class="col-xs-3 txt-center">
                        <input name="DIAdjustment" type="checkbox">&nbsp;
                        <label>Adjustment</label>
                     </div>
                     <div class="col-xs-2 label">
                        <label>Date Start:</label>
                     </div>
                     <div class="col-xs-1">
                        <input name="DIDateStart" type="text" class="form-input date--">
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-3 label">
                        <label>Pay Rate:</label>
                     </div>
                     <div class="col-xs-3">
                        <select name="DIPayRate" class="form-input">
                           <option></option>
                           <option>Monthly Rate</option>
                        </select>
                     </div>
                     <div class="col-xs-3 txt-center">
                     </div>
                     <div class="col-xs-2 label">
                        <label>Date End:</label>
                     </div>
                     <div class="col-xs-1">
                        <input name="DIDateEnd" type="text" class="form-input date--">
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-3 label">
                        <label>Pay Period:</label>
                     </div>
                     <div class="col-xs-3">
                        <select name="DIPayPeriod" class="form-input">
                           <option></option>
                           <option>First Half</option>
                        </select>
                     </div>
                     <div class="col-xs-3 txt-center">
                        <input name="DITerminated" type="checkbox">&nbsp;<label>Terminated</label>
                     </div>
                     <div class="col-xs-2 label">
                        <label>Date Terminated:</label>
                     </div>
                     <div class="col-xs-1">
                        <input name="DIDateTerminated" type="text" class="form-input date--">
                     </div>
                  </div>
                  <?php dobtnDeduction(); ?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>