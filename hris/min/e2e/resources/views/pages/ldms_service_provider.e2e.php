<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>

   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post">
         <?php $sys->SysHdr($sys,"ldms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("Service Provider"); ?>
            <div class="container-fluid margin-top">
               <div class="mypanel">
                  <div class="row">
                     <div class="col-xs-12" id="div_CONTENT">
                        <div class="margin-top">
                           <div id="divList">
                              <span id="spGridTable">
                                 <?php
                                       $table = "provider";
                                       $gridTableHdr_arr = ["Facilitator","Training Institution"];
                                       $gridTableFld_arr = ["Name","TrainingInstitution"];
                                       $sql = "SELECT * FROM provider";
                                       $Action = [true,true,true,false];
                                       doGridTable($table,
                                                   $gridTableHdr_arr,
                                                   $gridTableFld_arr,
                                                   $sql,
                                                   $Action,
                                                   $_SESSION["module_gridTable_ID"]);
                                 ?>
                              </span>
                              <?php
                                 btnINRECLO([true,false,false]);
                              ?>
                           </div>
                           <div id="divView">
                              <div class="mypanel panel panel-default">
                                 <div class="panel-top">
                                    <span id="ScreenMode">INSERTING NEW DATA
                                 </div>
                                 <div class="panel-mid-litebg" id="EntryScrn">
                                    <div id="EntryScrn">
                                       <div class="row">
                                          <div class="col-xs-6">
                                             <label>Facilitator</label>
                                             <input type="text" class="form-input saveFields--" name="char_Name" id="char_Name">
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-6">
                                             <label>Training Institution:</label>
                                             <input type="text" class="form-input saveFields--" name="char_TrainingInstitution" id="char_TrainingInstitution">
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-6">
                                             <label>Address</label>
                                             <input type="text" class="form-input saveFields--" name="char_Address" id="char_Address">
                                          </div>
                                       </div>
                                       
                                       <div class="row margin-top">
                                          <div class="col-xs-6">
                                             <label>Education:</label>
                                             <input type="text" class="form-input saveFields--" name="char_Education" id="char_Education">
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-6">
                                             <label>Expertise:</label>
                                             <input type="text" class="form-input saveFields--" name="char_Expertise" id="char_Expertise">
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-6">
                                             <label>Experience:</label>
                                             <input type="text" class="form-input saveFields--" name="char_Experience" id="char_Experience">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel-bottom">
                                    <?php btnSACABA([true,true,true]); ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               $table = "provider";
               doHidden("paramTitle",getvalue("paramTitle"),"");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
   <script language="JavaScript">
      $(document).ready(function () {
         remIconDL();
         $("#sint_LDMSLNDProgramRefId").change(function () {
            var value = $(this).val();
            $.get("ldms_transaction.e2e.php",
            {
               refid: value,
               fn: "getFacilitator"
            },
            function(data,status) {
               if (status == "success") {
                  try {
                     eval(data);
                  } catch (e) {
                     if (e instanceof SyntaxError) {
                        alert(e.message);
                     }
                  }
               }
            });
         });
      });
      function afterNewSave() {
         alert("Successfully Saved");
         gotoscrn($("#hProg").val(),"");
      }
      function afterEditSave() {
         alert("Successfully Updated");
         gotoscrn($("#hProg").val(),"");   
      }
   </script>
</html>



