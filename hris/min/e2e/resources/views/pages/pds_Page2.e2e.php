<?php
/*
------------------------------------------------------------------------
    File        : pds_Page2.php
    Purpose     :
    Syntax      : PHP / HTML
    Description :
    Author(s)   : Erwin Mendoza / emendoza0620@gmail.com
    Created     :
    Notes       :
------------------------------------------------------------------------
Developer   Date     Changes
--------------------------------------------------------------------------

--------------------------------------------------------------------------*/
function splitEligib() {
   pdsFooter(true,13);                  
   echo
   '</tbody>';
   echo    
   '</table>';
   footer_signdate(false);
   echo
   '</div>
   <div class="page-- nextpage" id="page2" style="position:relative">
   <table border=1 cellspacing=0 cellpadding=0>';
   eligibTHEAD();
   echo 
   '<tbody>';
}

function splitWorkExp() {
   pdsFooter(true,13);                  
   echo
   '</tbody>';
   echo    
   '</table>';
   footer_signdate(false);
   echo
   '</div>
   <div class="page-- nextpage" id="page2" style="position:relative">
   <table border=1 cellspacing=0 cellpadding=0>';
   workexpTHEAD();
   echo 
   '<tbody>';
}
   $EmpEligibility_split = false;
   $EmpWorkExp_split = false;
   $splitRowPerPage = 35;
?>
      <div class="page-- nextpage" id="page2" style="position:relative;">
         <table border=1 cellspacing=0 cellpadding=0 title="page 2">
            <?php eligibTHEAD(); ?>
            <tbody>
            <?php
               $lineCount = 0;
               $rowCount_EmpEligibility = 0;
               if ($rsEmpEligibility) {
                  $rowCount_EmpEligibility = mysqli_num_rows($rsEmpEligibility); 
                  if ($split["Eligibility"] != ".") {
                     $pds["rowEligibility"] = explode(".",$split["Eligibility"])[1];
                     $EmpEligibility_split = true;
                     if ($rowCount_EmpEligibility - $pds["rowEligibility"] <= 0) {
                        $EmpEligibility_split = false;
                     }
                  } else {
                     if ($rowCount_EmpEligibility > $pds["rowEligibility"]) {
                        $EmpEligibility_split = true;
                     }
                  }
               } else {
                  if ($split["Eligibility"] != ".") {
                     $pds["rowEligibility"] = explode(".",$split["Eligibility"])[1];
                  }
               }
               
               for ($j=1;$j<=$pds["rowEligibility"];$j++) {
            ?>
                  <tr align="center" class="trHeight">
                     <td colspan=5 align="left">
                        <span class="answer ELIGIBILITY" id="page2_A<?php echo $j + 3?>"><?php if ($diag) { echo "page2_A".($j + 3);}?></span>
                     </td>
                     <td>
                        <span class="answer ELIGIBILITY" id="page2_F<?php echo $j + 3?>"><?php if ($diag) { echo "page2_F".($j + 3);}?></span>
                     </td>
                     <td colspan=2>
                        <span class="answer ELIGIBILITY" id="page2_G<?php echo $j + 3?>"><?php if ($diag) { echo "page2_G".($j + 3);}?></span>
                     </td>
                     <td colspan=3>
                        <span class="answer ELIGIBILITY" id="page2_I<?php echo $j + 3?>"><?php if ($diag) { echo "page2_I".($j + 3);}?></span>
                     </td>
                     <td>
                        <span class="answer ELIGIBILITY" id="page2_L<?php echo $j + 3?>"><?php if ($diag) { echo "page2_L".($j + 3);}?></span>
                     </td>
                     <td>
                        <span class="answer ELIGIBILITY" id="page2_M<?php echo $j + 3?>"><?php if ($diag) { echo "page2_M".($j + 3);}?></span>
                     </td>
                  </tr>
            <?php
               }
               rowContinuation(13);
            ?>
            </tbody>
            <!--<tfoot>
               <tr style="border-top:2px solid #000;border-bottom:2px solid #000;">
                  <td colspan="13" class="bgGrayLabel" align="center" style="color:#FF0000;">
                     <i>(Continue on separate sheet if necessary)</i>
                  </td>
               </tr>
            </tfoot>-->
         </table>
         <table border=1 cellspacing=0 cellpadding=0>
            <?php workexpTHEAD(); ?>
            <tbody>
            <?php
               $rowCount_EmpWorkExp = 0;
               if ($rsEmpWorkExp) {
                  $rowCount_EmpWorkExp = mysqli_num_rows($rsEmpWorkExp);
                  if ($split["WorkExp"] != ".") {
                     $pds["rowWorkExperience"] = explode(".",$split["WorkExp"])[1];
                     $EmpWorkExp_split = true;
                     if ($rowCount_EmpWorkExp - $pds["rowWorkExperience"] <= 0) {
                        $EmpWorkExp_split = false;
                     }
                  } else {
                     if ($rowCount_EmpWorkExp > $pds["rowWorkExperience"]) {
                        $EmpWorkExp_split = true;
                     }
                  }
               } else {
                  if ($split["WorkExp"] != ".") {
                     $pds["rowWorkExperience"] = explode(".",$split["WorkExp"])[1];
                  }
               }
               
               for ($j=1;$j<=$pds["rowWorkExperience"];$j++)
               {
            ?>
                  <tr class="trHeight">
                        <td colspan=3>
                           <div class="newRow">
                              <div class="col-6">
                                 <div style="border-right: 1px solid black; height: <?php echo $pds["trHeight"]; ?>; text-align: center;">
                                    <span class="answer WORKEXPERIENCE" id="page2_O<?php echo $j + 5?>"></span>
                                 </div>
                              </div>
                              <div class="col-6" style="text-align: center;">
                                 <div>
                                    <span class="answer WORKEXPERIENCE" id="page2_Q<?php echo $j + 5?>"></span>
                                 </div>
                              </div>
                           </div>
                        </td>
                        <td colspan=3>
                           <span class="answer WORKEXPERIENCE" id="page2_R<?php echo $j + 5?>"></span>
                        </td>
                        <td colspan=3>
                           <span class="answer WORKEXPERIENCE" id="page2_U<?php echo $j + 5?>"></span>
                        </td>
                        <td align="right">
                           <span class="answer WORKEXPERIENCE" id="page2_X<?php echo $j + 5?>"></span>
                        </td>
                        <td>
                           <span class="answer WORKEXPERIENCE" id="page2_Y<?php echo $j + 5?>"></span>
                        </td>
                        <td>
                           <span class="answer WORKEXPERIENCE" id="page2_Z<?php echo $j + 5?>"></span>
                        </td>
                        <td align="center"> 
                           <span class="answer WORKEXPERIENCE" id="page2_AA<?php echo $j + 5?>"></span>
                        </td>
                  </tr>
            <?php
               }
               rowContinuation(13);
            ?>
            </tbody>
         </table>
         <?php footer_signdate(false) ?>
      </div> 
<?php 
   if ($EmpEligibility_split || $EmpWorkExp_split) {
      $rows_Count = 0;
?>
      <div class="page-- nextpage" id="page2_1" style="position:relative;">
      <?php 
         if ($EmpEligibility_split) { 
            echo '<table border=1 cellspacing=0 cellpadding=1>';
            eligibTHEAD();
            echo '<tbody>';
            for ($j=1;$j<=$rowCount_EmpEligibility - $pds["rowEligibility"];$j++) {
               $rows_Count++;
               if ($split["Eligibility"] != ".") {
                  if (count(explode(".",$split["Eligibility"])) == 3) {
                     $splitRowPerPage = explode(".",$split["Eligibility"])[2];
                  } 
                  if ($rows_Count > $splitRowPerPage) {
                     $rows_Count = 1; 
                     splitEligib();
                  }  
               }
               else { 
                  if ($rows_Count > $pds["rowPerPage"]) {
                     $rows_Count = 1;
                     splitEligib();
                  }
               }
      ?>    
               <tr align="center" class="trHeight">
                  <td colspan=5 align="left">
                     <span class="answer ELIGIBILITY" id="page2_1_A<?php echo $j + 3?>"></span>
                  </td>
                  <td>
                     <span class="answer ELIGIBILITY" id="page2_1_F<?php echo $j + 3?>"></span>
                  </td>
                  <td colspan=2>
                     <span class="answer ELIGIBILITY" id="page2_1_G<?php echo $j + 3?>"></span>
                  </td>
                  <td colspan=3>
                     <span class="answer ELIGIBILITY" id="page2_1_I<?php echo $j + 3?>"></span>
                  </td>
                  <td>
                     <span class="answer ELIGIBILITY" id="page2_1_L<?php echo $j + 3?>"></span>
                  </td>
                  <td>
                     <span class="answer ELIGIBILITY" id="page2_1_M<?php echo $j + 3?>"></span>
                  </td>
               </tr>
      <?php 
            }
            echo '</tbody></table>';
         }
         if ($EmpWorkExp_split) { 
            echo '<table border=1 cellspacing=0 cellpadding=1>';
            workexpTHEAD();
            echo '<tbody>';
            for ($j=1;$j<=$rowCount_EmpWorkExp - $pds["rowWorkExperience"];$j++) {
               $rows_Count++;
               if ($split["WorkExp"] != ".") {
                  if (count(explode(".",$split["WorkExp"])) == 3) {
                     $splitRowPerPage = explode(".",$split["WorkExp"])[2];
                  } 
                  if ($rows_Count > $splitRowPerPage) {
                     $rows_Count = 1; 
                     splitWorkExp();
                  }  
               }
               else {
                  if ($rows_Count > $pds["rowPerPage"]) {
                     $rows_Count = 1;
                     splitWorkExp();
                  }
               }
      ?>
               <tr class="trHeight">
                  <td colspan=3>
                     <div class="newRow">
                        <div class="col-6">
                           <div style="border-right: 1px solid black; height: <?php echo $pds["trHeight"]; ?>; text-align: center;">
                              <span class="answer WORKEXPERIENCE" id="page2_1_O<?php echo $j + 5?>"></span>
                           </div>
                        </div>
                        <div class="col-6" style="text-align: center;">
                           <div>
                              <span class="answer WORKEXPERIENCE" id="page2_1_Q<?php echo $j + 5?>"></span>
                           </div>
                        </div>
                     </div>
                  </td>
                  <td colspan=3>
                     <span class="answer WORKEXPERIENCE" id="page2_1_R<?php echo $j + 5?>"></span>
                  </td>
                  <td colspan=3>
                     <span class="answer WORKEXPERIENCE" id="page2_1_U<?php echo $j + 5?>"></span>
                  </td>
                  <td align="right">
                     <span class="answer WORKEXPERIENCE" id="page2_1_X<?php echo $j + 5?>"></span>
                  </td>
                  <td>
                     <span class="answer WORKEXPERIENCE" id="page2_1_Y<?php echo $j + 5?>"></span>
                  </td>
                  <td>
                     <span class="answer WORKEXPERIENCE" id="page2_1_Z<?php echo $j + 5?>"></span>
                  </td>
                  <td align="center"> 
                     <span class="answer WORKEXPERIENCE" id="page2_1_AA<?php echo $j + 5?>"></span>
                  </td>
               </tr>
      <?php 
            }
            echo '</tbody></table>';
         }
         footer_signdate(false);
      ?> 
      </div>  
<?php
   }
?>      

