ALTER TABLE `attendance_request`
	ADD COLUMN `Signatory1` INT(11) NULL DEFAULT NULL AFTER `Status`,
	ADD COLUMN `Signatory2` INT(11) NULL DEFAULT NULL AFTER `Signatory1`;