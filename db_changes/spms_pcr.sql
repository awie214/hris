DROP TABLE IF EXISTS `spms_pcr`;
CREATE TABLE IF NOT EXISTS `spms_pcr` (
  `RefId` int(50) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` int(50) DEFAULT NULL,
  `PositionRefId` int(50) DEFAULT NULL,
  `DivisionRefId` varchar(300) DEFAULT NULL,
  `DepartmentRefId` varchar(300) DEFAULT NULL,
  `PCRType` varchar(300) DEFAULT NULL,
  `Semester` varchar(300) DEFAULT NULL,
  `Year` varchar(300) DEFAULT NULL,
  `Average` varchar(300) DEFAULT NULL,
  `Rating` varchar(300) DEFAULT NULL,
  `Adjectival` varchar(300) DEFAULT NULL,
  `OverallScore` varchar(300) DEFAULT NULL,
  `NumericalRating` varchar(300) DEFAULT NULL,
  `File` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;