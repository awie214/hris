-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.37-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table pids_02_11.modules
CREATE TABLE IF NOT EXISTS `modules` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `SystemRefId` int(10) DEFAULT NULL,
  `ScrnId` varchar(100) COLLATE utf32_bin DEFAULT NULL,
  `UseFor` int(10) DEFAULT NULL,
  `Code` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Ordinal` int(6) DEFAULT NULL,
  `Name` varchar(300) COLLATE utf32_bin DEFAULT NULL,
  `Icons` varchar(50) COLLATE utf32_bin DEFAULT NULL,
  `Filename` varchar(100) COLLATE utf32_bin DEFAULT NULL,
  `Route` varchar(100) COLLATE utf32_bin DEFAULT NULL,
  `Description` varchar(300) COLLATE utf32_bin DEFAULT NULL,
  `Remarks` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Data` varchar(1) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf32 COLLATE=utf32_bin COMMENT='System Modules';

-- Dumping data for table pids_02_11.modules: ~5 rows (approximately)
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` (`RefId`, `SystemRefId`, `ScrnId`, `UseFor`, `Code`, `Ordinal`, `Name`, `Icons`, `Filename`, `Route`, `Description`, `Remarks`, `LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`) VALUES
	(1, 4, '1549938417', NULL, 'COMP MANUAL', 1, 'Competency Manual', NULL, '_competency_manual', 'ldms', NULL, NULL, '2019-02-12', '10:39:15', 'admin', 'E'),
	(2, 4, '1549941668', NULL, 'COMP ASSESSEMENT', 2, 'Competency Assessment', NULL, '_competency_assessment', 'ldms', NULL, NULL, '2019-02-12', '11:22:09', 'admin', 'A'),
	(3, 4, '1549950179', NULL, 'LND', 3, 'Learning And Development Program', NULL, '_learning_and_developement_program', 'ldms', NULL, NULL, '2019-02-12', '15:27:43', 'admin', 'E'),
	(4, 4, '1549950179', NULL, 'LNDI', 4, 'Learning And Development Intervention', NULL, '_learning_and_development_intervention', 'ldms', NULL, NULL, '2019-02-12', '13:51:47', 'admin', 'A'),
	(5, 4, '1549950179', NULL, 'RSO', 5, 'Return Service Obligation', NULL, '_return_service_obligation', 'ldms', NULL, NULL, '2019-02-12', '13:52:47', 'admin', 'A'),
	(6, 5, '1550029330', NULL, 'SPMSRPT', 5, 'SPMS Report', NULL, '_report', 'spms', NULL, NULL, '2019-02-13', '11:42:59', 'admin', 'A'),
	(7, 4, '1550372636', NULL, 'LDMS_RPT', 7, 'LDMS Reports', NULL, '_report', 'ldms', NULL, NULL, '2019-02-17', '11:04:28', 'admin', 'A');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
