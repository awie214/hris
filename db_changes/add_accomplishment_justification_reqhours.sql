ALTER TABLE `overtime_request`
	ADD COLUMN `HoursReq` INT(4) NULL DEFAULT NULL AFTER `ToTime`,
	ADD COLUMN `Accomplishment` VARCHAR(300) NULL DEFAULT NULL AFTER `HoursReq`,
	ADD COLUMN `Justification` VARCHAR(300) NULL DEFAULT NULL AFTER `Accomplishment`;