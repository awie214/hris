CREATE TABLE IF NOT EXISTS `usermanagement` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `EmployeesRefId` int(10) DEFAULT NULL,
  `DivisionRefId` int(10) DEFAULT NULL,
  `SystemAccess` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Data` varchar(1) CHARACTER SET latin1 DEFAULT NULL,
  `Remarks` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf32 COLLATE=utf32_bin;