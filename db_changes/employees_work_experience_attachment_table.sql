-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.31-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5289
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table pids.employees_work_experience_attachments
CREATE TABLE IF NOT EXISTS `employees_work_experience_attachments` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(11) DEFAULT NULL,
  `BranchRefId` int(11) DEFAULT NULL,
  `EmployeesRefId` int(11) DEFAULT NULL,
  `WorkExperienceRefId` int(11) DEFAULT NULL,
  `PositionRefId` int(11) DEFAULT NULL,
  `OfficeRefId` int(11) DEFAULT NULL,
  `AgencyRefId` int(11) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `Supervisor` varchar(300) DEFAULT NULL,
  `Location` varchar(300) DEFAULT NULL,
  `Accomplishments` varchar(1500) DEFAULT NULL,
  `Duties` varchar(1500) DEFAULT NULL,
  `LastUpdateBy` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
