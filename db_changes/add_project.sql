CREATE TABLE IF NOT EXISTS `project` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(10) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `empinformation`
  ADD COLUMN `ProjectRefId` BIGINT(20) NULL DEFAULT '0';