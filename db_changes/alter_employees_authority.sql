ALTER TABLE `employeesauthority`
	ADD COLUMN `ActivityNature` VARCHAR(300) NULL DEFAULT NULL AFTER `ControlNumber`,
	ADD COLUMN `Sponsor` VARCHAR(300) NULL DEFAULT NULL AFTER `ActivityNature`;